package com.solvve.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.solvve.domain.PageResult;
import com.solvve.dto.role.*;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.util.TestObjectsFactoryController;
import com.solvve.domain.Role;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.RoleService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({RoleController.class, TestObjectsFactoryController.class})
public class RoleControllerTest extends BaseControllerTest {

    @MockBean
    private RoleService roleService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetRole() throws Exception {

        RoleReadDTO role = testObjectsFactoryController.createRoleReadDTO();

        Mockito.when(roleService.getRole(role.getId())).thenReturn(role);

        String resultJson = mvc.perform(get("/api/v1/roles/{id}", role.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReadDTO actualRole = objectMapper.readValue(resultJson, RoleReadDTO.class);
        Assertions.assertThat(actualRole).isEqualToComparingFieldByField(role);

        Mockito.verify(roleService).getRole(role.getId());
    }

    @Test
    public void testGetRoles() throws Exception {
        RoleFilter roleFilter = new RoleFilter();
        roleFilter.setFilmId(UUID.randomUUID());
        roleFilter.setPersonId(UUID.randomUUID());

        RoleReadDTO read = testObjectsFactoryController.createRoleReadDTO();

        PageResult<RoleReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(read));
        Mockito.when(roleService.getRoles(roleFilter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/roles")
                .param("filmId", roleFilter.getFilmId().toString())
                .param("personId", roleFilter.getPersonId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<RoleReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {});
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetRoleExtended() throws Exception {

        RoleReadExtendedDTO role = new RoleReadExtendedDTO();
        role.setId(UUID.randomUUID());
        role.setText("Kevin Lomax");

        Mockito.when(roleService.getRoleExtended(role.getId())).thenReturn(role);

        String resultJson = mvc.perform(get("/api/v1/roles/{id}/extended", role.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReadExtendedDTO actualRole = objectMapper.readValue(resultJson, RoleReadExtendedDTO.class);
        Assertions.assertThat(actualRole).isEqualToComparingFieldByField(role);

        Mockito.verify(roleService).getRoleExtended(role.getId());
    }

    @Test
    public void testGetActorRoles() throws Exception {

        List<RoleReadDTO> roles = List.of(testObjectsFactoryController.createRoleReadDTO(),
                testObjectsFactoryController.createRoleReadDTO(),
                testObjectsFactoryController.createRoleReadDTO());

        UUID actorId = UUID.randomUUID();

        Mockito.when(roleService.getActorRoles(actorId)).thenReturn(roles);

        String resultJson = mvc.perform(get("/api/v1/actor/{actorId}/roles", actorId.toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReadDTO[] arrRoles = objectMapper.readValue(resultJson, RoleReadDTO[].class);
        Assertions.assertThat(roles).containsExactlyInAnyOrder(arrRoles);
    }

    @Test
    public void testGetRoleWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Role.class, wrongId);
        Mockito.when(roleService.getRole(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/roles/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateRole() throws Exception {

        UUID actorId = UUID.randomUUID();
        UUID filmId = UUID.randomUUID();

        RoleCreateDTO create = new RoleCreateDTO();
        create.setText("Kevin Lomax");
        create.setActorId(actorId);
        create.setFilmId(filmId);

        RoleReadDTO read = testObjectsFactoryController.createRoleReadDTO();

        Mockito.when(roleService.createRole(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/roles")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReadDTO actualRole = objectMapper.readValue(resultJson, RoleReadDTO.class);
        Assertions.assertThat(actualRole).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testAddRoleToActor() throws Exception {

        UUID actorId = UUID.randomUUID();
        UUID roleId = UUID.randomUUID();

        RoleReadDTO read = testObjectsFactoryController.createRoleReadDTO();
        List<RoleReadDTO> expectedRoles = List.of(read);

        Mockito.when(roleService.addRoleToActor(actorId, roleId)).thenReturn(expectedRoles);

        String resultJson = mvc.perform(post("/api/v1/actor/{actorId}/roles/{id}", actorId, roleId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<RoleReadDTO> actualRoles = objectMapper.readValue(resultJson,
                new TypeReference<List<RoleReadDTO>>() {
                });
        Assert.assertEquals(expectedRoles, actualRoles);
    }

    @Test
    public void testPatchRole() throws Exception {

        RolePatchDTO patchDTO = testObjectsFactoryController.createRolePatchDTO();

        RoleReadDTO read = testObjectsFactoryController.createRoleReadDTO();

        Mockito.when(roleService.patchRole(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/roles/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReadDTO actualAdmin = objectMapper.readValue(resultJson, RoleReadDTO.class);
        Assert.assertEquals(read, actualAdmin);
    }

    @Test
    public void testUpdateRole() throws Exception {

        RolePutDTO putDTO = testObjectsFactoryController.createRolePutDTO();

        RoleReadDTO read = testObjectsFactoryController.createRoleReadDTO();

        Mockito.when(roleService.updateRole(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/roles/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReadDTO actualAdmin = objectMapper.readValue(resultJson, RoleReadDTO.class);
        Assert.assertEquals(read, actualAdmin);
    }

    @Test
    public void testDeleteRole() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/roles/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(roleService).deleteRole(id);
    }

    @Test
    public void testRemoveRoleFromActor() throws Exception {

        UUID actorId = UUID.randomUUID();
        UUID roleId = UUID.randomUUID();

        List<RoleReadDTO> expectedRoles = List.of();

        Mockito.when(roleService.removeRoleFromActor(actorId, roleId)).thenReturn(expectedRoles);

        String resultJson = mvc.perform(delete("/api/v1/actor/{actorId}/roles/{id}", actorId, roleId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<RoleReadDTO> actualRoles = objectMapper.readValue(resultJson,
                new TypeReference<List<RoleReadDTO>>() {
                });
        Assert.assertEquals(expectedRoles, actualRoles);
    }

    @Test
    public void testGetRolesWithPagingAndSorting() throws Exception {
        RoleFilter roleFilter = new RoleFilter();
        roleFilter.setFilmId(UUID.randomUUID());
        roleFilter.setPersonId(UUID.randomUUID());

        RoleReadDTO read = testObjectsFactoryController.createRoleReadDTO();

        int page = 1;
        int size = 25;

        PageResult<RoleReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(new Long(100));
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "text"));
        Mockito.when(roleService.getRoles(roleFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/roles")
                .param("filmId", roleFilter.getFilmId().toString())
                .param("personId", roleFilter.getPersonId().toString())
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "text,desc"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<RoleReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {});
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetRolesWithBigPage() throws Exception {
        RoleFilter roleFilter = new RoleFilter();
        roleFilter.setFilmId(UUID.randomUUID());
        roleFilter.setPersonId(UUID.randomUUID());

        RoleReadDTO read = testObjectsFactoryController.createRoleReadDTO();

        int page = 1;
        int size = 999999;

        PageResult<RoleReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(new Long(100));
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(page, maxPageSize, Sort.by(Sort.Direction.DESC, "text"));
        Mockito.when(roleService.getRoles(roleFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/roles")
                .param("filmId", roleFilter.getFilmId().toString())
                .param("personId", roleFilter.getPersonId().toString())
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "text,desc"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<RoleReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {});
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testCreateRoleValidationFailed() throws Exception {

        RoleCreateDTO create = new RoleCreateDTO();

        RoleReadDTO read = testObjectsFactoryController.createRoleReadDTO();

        Mockito.when(roleService.createRole(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/roles/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(roleService, Mockito.never()).createRole(ArgumentMatchers.any());
    }
}

