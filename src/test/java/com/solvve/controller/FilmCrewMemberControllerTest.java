package com.solvve.controller;

import com.solvve.domain.Film;
import com.solvve.dto.crewmember.*;
import com.solvve.dto.film.FilmReadDTO;
import com.solvve.service.FilmCrewMemberService;
import com.solvve.util.TestObjectsFactoryController;
import com.solvve.domain.CrewMember;
import com.solvve.exception.EntityNotFoundException;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({FilmCrewMemberController.class, TestObjectsFactoryController.class})
public class FilmCrewMemberControllerTest extends BaseControllerTest {

    @MockBean
    private FilmCrewMemberService filmCrewMemberService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetFilmCrewMembers() throws Exception {

        List<CrewMemberReadDTO> crewMembers = List.of(testObjectsFactoryController.createCrewMemberReadDTO(),
                                                      testObjectsFactoryController.createCrewMemberReadDTO());
        FilmReadDTO film = testObjectsFactoryController.createFilmReadDTO();

        Mockito.when(filmCrewMemberService.getFilmCrewMembers(film.getId())).thenReturn(crewMembers);

        String resultJson = mvc.perform(get("/api/v1/film/{filmId}/crew-members", film.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewMemberReadDTO[] arrCrewMembers = objectMapper.readValue(resultJson, CrewMemberReadDTO[].class);
        Assertions.assertThat(crewMembers).containsExactlyInAnyOrder(arrCrewMembers);
    }

    @Test
    public void testGetFilmCrewMember() throws Throwable {

        CrewMemberReadDTO crewMember = testObjectsFactoryController.createCrewMemberReadDTO();
        FilmReadDTO film = testObjectsFactoryController.createFilmReadDTO();

        Mockito.when(filmCrewMemberService.getFilmCrewMember(film.getId(),
                crewMember.getId())).thenReturn(crewMember);

        String resultJson = mvc.perform(get("/api/v1/film/{filmId}/crew-members/{crewMemberId}",
                film.getId(), crewMember.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewMemberReadDTO actualCrewMember = objectMapper.readValue(resultJson, CrewMemberReadDTO.class);
        Assertions.assertThat(actualCrewMember).isEqualToComparingFieldByField(crewMember);

        Mockito.verify(filmCrewMemberService).getFilmCrewMember(film.getId(), crewMember.getId());
    }

    @Test
    public void testGetFilmCrewMemberWrongId() throws Throwable {

        UUID wrongId = UUID.randomUUID();
        FilmReadDTO film = testObjectsFactoryController.createFilmReadDTO();

        EntityNotFoundException exception = new EntityNotFoundException(CrewMember.class, wrongId,
                Film.class, film.getId());
        Mockito.when(filmCrewMemberService.getFilmCrewMember(film.getId(), wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/film/{filmId}/crew-members/{crewMemberId}",
                film.getId(), wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testPatchFilmCrewMember() throws Throwable {

        CrewMemberPatchDTO patchDTO = testObjectsFactoryController.createCrewMemberPatchDTO();
        CrewMemberReadDTO read = testObjectsFactoryController.createCrewMemberReadDTO();
        FilmReadDTO filmReadDTO = testObjectsFactoryController.createFilmReadDTO();

        Mockito.when(filmCrewMemberService.patchFilmCrewMember(filmReadDTO.getId(), read.getId(),
                patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/film/{filmId}/crew-members/{crewMemberId}",
                filmReadDTO.getId(), read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewMemberReadDTO actualCrewMember = objectMapper.readValue(resultJson, CrewMemberReadDTO.class);
        Assert.assertEquals(read, actualCrewMember);
    }

    @Test
    public void testUpdateFilmCrewMember() throws Throwable{

        CrewMemberPutDTO putDTO = testObjectsFactoryController.createCrewMemberPutDTO();
        CrewMemberReadDTO read = testObjectsFactoryController.createCrewMemberReadDTO();
        FilmReadDTO filmReadDTO = testObjectsFactoryController.createFilmReadDTO();

        Mockito.when(filmCrewMemberService.updateFilmCrewMember(filmReadDTO.getId(), read.getId(),
                putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/film/{filmId}/crew-members/{crewMemberId}",
                filmReadDTO.getId(), read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewMemberReadDTO actualAdmin = objectMapper.readValue(resultJson, CrewMemberReadDTO.class);
        Assert.assertEquals(read, actualAdmin);
    }
}
