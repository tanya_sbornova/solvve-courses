package com.solvve.controller;

import com.solvve.domain.Film;
import com.solvve.domain.ReviewFilm;
import com.solvve.dto.film.FilmReadDTO;
import com.solvve.dto.reviewfilm.ReviewFilmCreateDTO;
import com.solvve.dto.reviewfilm.ReviewFilmPatchDTO;
import com.solvve.dto.reviewfilm.ReviewFilmPutDTO;
import com.solvve.dto.reviewfilm.ReviewFilmReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.service.FilmReviewFilmService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({FilmReviewFilmController.class, TestObjectsFactoryController.class})
public class FilmReviewFilmControllerTest extends BaseControllerTest {

    @MockBean
    private FilmReviewFilmService reviewFilmService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetFilmReviewsFilm() throws Exception {

        List<ReviewFilmReadDTO> reviewsFilm = List.of(testObjectsFactoryController.createReviewFilmReadDTO(),
                                                      testObjectsFactoryController.createReviewFilmReadDTO());
        UUID filmId = UUID.randomUUID();

        Mockito.when(reviewFilmService.getFilmReviewsFilm(filmId)).thenReturn(reviewsFilm);

        String resultJson = mvc.perform(get("/api/v1/film/{filmId}/review-films", filmId.toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewFilmReadDTO[] arrReviewsFilm = objectMapper.readValue(resultJson, ReviewFilmReadDTO[].class);
        Assertions.assertThat(reviewsFilm).containsExactlyInAnyOrder(arrReviewsFilm);
    }

    @Test
    public void testGetFilmReviewsFilmOfCurrentRegisteredUser() throws Exception {

        List<ReviewFilmReadDTO> reviewsFilm = List.of(testObjectsFactoryController.createReviewFilmReadDTO(),
                testObjectsFactoryController.createReviewFilmReadDTO());
        UUID filmId = UUID.randomUUID();
        UUID registeredUserId = UUID.randomUUID();

        Mockito.when(reviewFilmService.getFilmReviewsFilmOfCurrentRegisteredUser(registeredUserId, filmId))
                .thenReturn(reviewsFilm);

        String resultJson = mvc.perform(get("/api/v1/registered-user/{registeredUserId}/film/{filmId}/review-films",
                registeredUserId.toString(), filmId.toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewFilmReadDTO[] arrReviewsFilm = objectMapper.readValue(resultJson, ReviewFilmReadDTO[].class);
        Assertions.assertThat(reviewsFilm).containsExactlyInAnyOrder(arrReviewsFilm);
    }

    @Test
    public void testGetFilmReviewFilm() throws Throwable {

        ReviewFilmReadDTO reviewFilm = testObjectsFactoryController.createReviewFilmReadDTO();
        UUID filmId = UUID.randomUUID();

        Mockito.when(reviewFilmService.getFilmReviewFilm(filmId,
                reviewFilm.getId())).thenReturn(reviewFilm);

        String resultJson = mvc.perform(get("/api/v1/film/{filmId}/review-films/{reviewFilmId}",
                filmId.toString(),
                reviewFilm.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewFilmReadDTO actualReviewFilm = objectMapper.readValue(resultJson, ReviewFilmReadDTO.class);
        Assertions.assertThat(actualReviewFilm).isEqualToComparingFieldByField(reviewFilm);

        Mockito.verify(reviewFilmService).getFilmReviewFilm(filmId, reviewFilm.getId());
    }

    @Test
    public void testGetFilmReviewFilmWrongId() throws Throwable {

        UUID wrongId = UUID.randomUUID();
        FilmReadDTO film = testObjectsFactoryController.createFilmReadDTO();

        EntityNotFoundException exception = new EntityNotFoundException(ReviewFilm.class, wrongId,
                Film.class, film.getId());
        Mockito.when(reviewFilmService.getFilmReviewFilm(film.getId(), wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/film/{filmId}/review-films/{reviewFilmId}",
                film.getId().toString(), wrongId.toString()))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateFilmReviewFilm() throws Exception {

        ReviewFilmCreateDTO create = testObjectsFactoryController.createReviewFilmCreateDTO();
        ReviewFilmReadDTO read = testObjectsFactoryController.createReviewFilmReadDTO();
        UUID filmId = UUID.randomUUID();

        Mockito.when(reviewFilmService.createFilmReviewFilm(filmId, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/film/{filmId}/review-films",
                filmId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewFilmReadDTO actualReviewFilm = objectMapper.readValue(resultJson, ReviewFilmReadDTO.class);
        Assertions.assertThat(actualReviewFilm).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchFilmReviewFilm() throws Throwable {

        ReviewFilmPatchDTO patchDTO = testObjectsFactoryController.createReviewFilmPatchDTO();
        ReviewFilmReadDTO read = testObjectsFactoryController.createReviewFilmReadDTO();
        UUID filmId = UUID.randomUUID();

        Mockito.when(reviewFilmService.patchFilmReviewFilm(filmId, read.getId(),
                patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/film/{filmId}/review-films/{reviewFilmId}",
                filmId.toString(), read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewFilmReadDTO actualReviewFilm = objectMapper.readValue(resultJson, ReviewFilmReadDTO.class);
        Assert.assertEquals(read, actualReviewFilm);
    }

    @Test
    public void testUpdateFilmReviewFilm() throws Throwable{

        ReviewFilmPutDTO putDTO = testObjectsFactoryController.createReviewFilmPutDTO();
        ReviewFilmReadDTO read = testObjectsFactoryController.createReviewFilmReadDTO();
        UUID filmId = UUID.randomUUID();

        Mockito.when(reviewFilmService.updateFilmReviewFilm(filmId, read.getId(),
                putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/film/{filmId}/review-films/{reviewFilmId}",
                filmId.toString(), read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewFilmReadDTO actualAdmin = objectMapper.readValue(resultJson, ReviewFilmReadDTO.class);
        Assert.assertEquals(read, actualAdmin);
    }

    @Test
    public void testDeleteFilmReviewFilm() throws Exception {

        ReviewFilmReadDTO read = testObjectsFactoryController.createReviewFilmReadDTO();
        FilmReadDTO filmReadDTO = testObjectsFactoryController.createFilmReadDTO();

        mvc.perform(delete("/api/v1/film/{filmId}/review-films/{reviewFilmId}",
                filmReadDTO.getId().toString(), read.getId().toString()))
                .andExpect(status().isOk());

        Mockito.verify(reviewFilmService).deleteFilmReviewFilm(filmReadDTO.getId(), read.getId());
    }

    @Test
    public void testCreateReviewFilmValidationFailed() throws Exception {

        ReviewFilmCreateDTO create = new ReviewFilmCreateDTO();
        ReviewFilmReadDTO read = testObjectsFactoryController.createReviewFilmReadDTO();
        UUID filmId = UUID.randomUUID();

        Mockito.when(reviewFilmService.createFilmReviewFilm(filmId, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/film/{filmId}/review-films",
                filmId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(reviewFilmService, Mockito.never()).createFilmReviewFilm(ArgumentMatchers.any(),
                ArgumentMatchers.any());
    }
}
