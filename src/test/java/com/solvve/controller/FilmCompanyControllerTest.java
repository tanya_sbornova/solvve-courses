package com.solvve.controller;

import com.solvve.domain.Film;
import com.solvve.dto.company.*;
import com.solvve.dto.film.FilmReadDTO;
import com.solvve.service.FilmCompanyService;
import com.solvve.util.TestObjectsFactoryController;
import com.solvve.domain.Company;
import com.solvve.exception.EntityNotFoundException;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({FilmCompanyController.class, TestObjectsFactoryController.class})
public class FilmCompanyControllerTest extends BaseControllerTest {

    @MockBean
    private FilmCompanyService filmCompanyService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetFilmCompanies() throws Exception {

        List<CompanyReadDTO> companies = List.of(testObjectsFactoryController.createCompanyReadDTO(),
                testObjectsFactoryController.createCompanyReadDTO());
        FilmReadDTO film = testObjectsFactoryController.createFilmReadDTO();

        Mockito.when(filmCompanyService.getFilmCompanies(film.getId())).thenReturn(companies);

        String resultJson = mvc.perform(get("/api/v1/film/{filmId}/companies", film.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CompanyReadDTO[] arrCompanies = objectMapper.readValue(resultJson, CompanyReadDTO[].class);
        Assertions.assertThat(companies).containsExactlyInAnyOrder(arrCompanies);
    }

    @Test
    public void testGetFilmCompany() throws Throwable {

        CompanyReadDTO company = testObjectsFactoryController.createCompanyReadDTO();
        FilmReadDTO film = testObjectsFactoryController.createFilmReadDTO();

        Mockito.when(filmCompanyService.getFilmCompany(film.getId(),
                company.getId())).thenReturn(company);

        String resultJson = mvc.perform(get("/api/v1/film/{filmId}/companies/{companyId}",
                film.getId(), company.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CompanyReadDTO actualCompany = objectMapper.readValue(resultJson, CompanyReadDTO.class);
        Assertions.assertThat(actualCompany).isEqualToComparingFieldByField(company);

        Mockito.verify(filmCompanyService).getFilmCompany(film.getId(), company.getId());
    }

    @Test
    public void testGetFilmCompanyWrongId() throws Throwable {

        UUID wrongId = UUID.randomUUID();
        FilmReadDTO film = testObjectsFactoryController.createFilmReadDTO();

        EntityNotFoundException exception = new EntityNotFoundException(Company.class, wrongId,
                Film.class, film.getId());
        Mockito.when(filmCompanyService.getFilmCompany(film.getId(), wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/film/{filmId}/companies/{companyId}",
                film.getId(), wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }
}
