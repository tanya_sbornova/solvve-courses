package com.solvve.controller;

import com.solvve.domain.NewsLikeDislike;
import com.solvve.dto.newslikedislike.NewsLikeDislikePatchDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikePutDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikeReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.NewsLikeDislikeService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({NewsLikeDislikeController.class, TestObjectsFactoryController.class})
public class NewsLikeDislikeControllerTest extends BaseControllerTest {

    @MockBean
    private NewsLikeDislikeService newsLikeDislikeService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetNewsLikeDislike() throws Exception {

        NewsLikeDislikeReadDTO newsLikeDislike = testObjectsFactoryController.createNewsLikeDislikeReadDTO();

        Mockito.when(newsLikeDislikeService.getNewsLikeDislike(newsLikeDislike.getId())).
                thenReturn(newsLikeDislike);

        String resultJson = mvc.perform(get("/api/v1/news-like-dislikes/{id}",
                newsLikeDislike.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        NewsLikeDislikeReadDTO actualNewsLikeDislike = objectMapper.readValue(resultJson,
                NewsLikeDislikeReadDTO.class);
        Assertions.assertThat(actualNewsLikeDislike).isEqualToComparingFieldByField(newsLikeDislike);

        Mockito.verify(newsLikeDislikeService).getNewsLikeDislike(newsLikeDislike.getId());
    }

    @Test
    public void testGetNewsLikeDislikeWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(NewsLikeDislike.class, wrongId);
        Mockito.when(newsLikeDislikeService.getNewsLikeDislike(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/news-like-dislikes/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testPatchNewsLikeDislike() throws Exception {

        NewsLikeDislikePatchDTO patchDTO = new NewsLikeDislikePatchDTO();
        patchDTO.setIsLike(false);

        NewsLikeDislikeReadDTO read = testObjectsFactoryController.createNewsLikeDislikeReadDTO();

        Mockito.when(newsLikeDislikeService.patchNewsLikeDislike(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/news-like-dislikes/{id}",
                read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsLikeDislikeReadDTO actualNewsLikeDislike = objectMapper.readValue(resultJson,
                NewsLikeDislikeReadDTO.class);
        Assert.assertEquals(read, actualNewsLikeDislike);
    }

    @Test
    public void testUpdateNewsLikeDislike() throws Exception {

        NewsLikeDislikePutDTO putDTO = new NewsLikeDislikePutDTO();
        putDTO.setIsLike(false);

        NewsLikeDislikeReadDTO read = testObjectsFactoryController.createNewsLikeDislikeReadDTO();

        Mockito.when(newsLikeDislikeService.updateNewsLikeDislike(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/news-like-dislikes/{id}",
                read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsLikeDislikeReadDTO actualNewsLikeDislike = objectMapper.readValue(resultJson,
                NewsLikeDislikeReadDTO.class);
        Assert.assertEquals(read, actualNewsLikeDislike);
    }

    @Test
    public void testDeleteNewsLikeDislike() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/news-like-dislikes/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(newsLikeDislikeService).deleteNewsLikeDislike(id);
    }
}
