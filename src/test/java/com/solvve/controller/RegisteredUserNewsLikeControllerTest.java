package com.solvve.controller;

import com.solvve.domain.NewsLikeDislike;
import com.solvve.domain.RegisteredUser;
import com.solvve.dto.newslikedislike.NewsLikeDislikeCreateDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikePatchDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikePutDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikeReadDTO;
import com.solvve.dto.registereduser.RegisteredUserReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.service.RegisteredUserNewsLikeService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({RegisteredUserNewsLikeController.class, TestObjectsFactoryController.class})
public class RegisteredUserNewsLikeControllerTest extends BaseControllerTest {

    @MockBean
    private RegisteredUserNewsLikeService registeredUserNewsLikeDislikeService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetRegisteredUserNewLikes() throws Exception {

        List<NewsLikeDislikeReadDTO> newsLikeDislikes = List.of(testObjectsFactoryController.
                createNewsLikeDislikeReadDTO());
        RegisteredUserReadDTO registeredUser = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserNewsLikeDislikeService.getRegisteredUserNewsLikes(registeredUser.getId())).
                thenReturn(newsLikeDislikes);

        String resultJson = mvc.perform(get("/api/v1/registered-user/{registeredUserId}/news-like-dislike",
                registeredUser.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsLikeDislikeReadDTO[] arrNewsLikeDislikes = objectMapper.readValue(resultJson,
                NewsLikeDislikeReadDTO[].class);
        Assertions.assertThat(newsLikeDislikes).containsExactlyInAnyOrder(arrNewsLikeDislikes);
    }

    @Test
    public void testGetRegisteredUserNewLike() throws Throwable {

        NewsLikeDislikeReadDTO newsLikeDislike = testObjectsFactoryController.createNewsLikeDislikeReadDTO();

        Mockito.when(registeredUserNewsLikeDislikeService.
                getRegisteredUserNewsLike(newsLikeDislike.getRegisteredUserId(),
                newsLikeDislike.getId())).thenReturn(newsLikeDislike);

        String resultJson = mvc.perform(get(
                "/api/v1/registered-user/{registeredUserId}/news-like-dislike/{newsLikeDislikeId}",
                newsLikeDislike.getRegisteredUserId().toString(),
                newsLikeDislike.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsLikeDislikeReadDTO actualNewsLikeDislike = objectMapper.readValue(resultJson,
                NewsLikeDislikeReadDTO.class);
        Assertions.assertThat(actualNewsLikeDislike).isEqualToComparingFieldByField(newsLikeDislike);

        Mockito.verify(registeredUserNewsLikeDislikeService).getRegisteredUserNewsLike(newsLikeDislike.
                getRegisteredUserId(), newsLikeDislike.getId());
    }

    @Test
    public void testGetRegisteredUserNewLikeWrongId() throws Throwable {

        UUID wrongId = UUID.randomUUID();
        RegisteredUserReadDTO registeredUser = testObjectsFactoryController.createRegisteredUserReadDTO();

        EntityNotFoundException exception = new EntityNotFoundException(NewsLikeDislike.class, wrongId,
                RegisteredUser.class, registeredUser.getId());
        Mockito.when(registeredUserNewsLikeDislikeService.getRegisteredUserNewsLike(registeredUser.getId(),
                wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get(
                "/api/v1/registered-user/{registeredUserId}/news-like-dislike/{newsLikeDislikeId}",
                registeredUser.getId().toString(), wrongId.toString()))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateRegisteredUserNewLike() throws Exception {

        UUID newsId = UUID.randomUUID();
        NewsLikeDislikeCreateDTO create = testObjectsFactoryController.createNewsLikeDislikeCreateDTO();
        create.setNewsId(newsId);
        NewsLikeDislikeReadDTO read = testObjectsFactoryController.createNewsLikeDislikeReadDTO();
        read.setNewsId(newsId);
        UUID registeredUserId = UUID.randomUUID();

        Mockito.when(registeredUserNewsLikeDislikeService.createRegisteredUserNewsLike(registeredUserId,
                create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/registered-user/{registeredUserId}/news-like-dislike",
                registeredUserId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsLikeDislikeReadDTO actualNewsLikeDislike = objectMapper.readValue(resultJson,
                NewsLikeDislikeReadDTO.class);
        Assertions.assertThat(actualNewsLikeDislike).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchRegisteredUserNewLike() throws Throwable {

        NewsLikeDislikePatchDTO patchDTO = testObjectsFactoryController.createNewsLikeDislikePatchDTO();
        NewsLikeDislikeReadDTO read = testObjectsFactoryController.createNewsLikeDislikeReadDTO();
        RegisteredUserReadDTO registeredUserReadDTO = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserNewsLikeDislikeService.patchRegisteredUserNewsLike(registeredUserReadDTO.getId(),
                read.getId(),
                patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch(
                "/api/v1/registered-user/{registeredUserId}/news-like-dislike/{newsLikeDislikeId}",
                registeredUserReadDTO.getId().toString(), read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsLikeDislikeReadDTO actualNewsLikeDislike = objectMapper.readValue(resultJson,
                NewsLikeDislikeReadDTO.class);
        Assert.assertEquals(read, actualNewsLikeDislike);
    }

    @Test
    public void testUpdateRegisteredUserNewLike() throws Throwable{

        NewsLikeDislikePutDTO putDTO = testObjectsFactoryController.createNewsLikeDislikePutDTO();
        NewsLikeDislikeReadDTO read = testObjectsFactoryController.createNewsLikeDislikeReadDTO();
        RegisteredUserReadDTO registeredUserReadDTO = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserNewsLikeDislikeService.updateRegisteredUserNewsLike(registeredUserReadDTO.getId(),
                read.getId(),
                putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put(
                "/api/v1/registered-user/{registeredUserId}/news-like-dislike/{newsLikeDislikeId}",
                registeredUserReadDTO.getId().toString(), read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsLikeDislikeReadDTO actualAdmin = objectMapper.readValue(resultJson, NewsLikeDislikeReadDTO.class);
        Assert.assertEquals(read, actualAdmin);
    }

    @Test
    public void testDeleteRegisteredUserNewLike() throws Exception {

        NewsLikeDislikeReadDTO read = testObjectsFactoryController.createNewsLikeDislikeReadDTO();
        RegisteredUserReadDTO registeredUserReadDTO = testObjectsFactoryController.createRegisteredUserReadDTO();

        mvc.perform(delete("/api/v1/registered-user/{registeredUserId}/news-like-dislike/{newsLikeDislikeId}",
                registeredUserReadDTO.getId().toString(), read.getId().toString()))
                .andExpect(status().isOk());

        Mockito.verify(registeredUserNewsLikeDislikeService).
                deleteRegisteredUserNewsLike(registeredUserReadDTO.getId(), read.getId());
    }

    @Test
    public void testCreateNewsLikeDislikeValidationFailed() throws Exception {

        NewsLikeDislikeCreateDTO create = new  NewsLikeDislikeCreateDTO();
        NewsLikeDislikeReadDTO read = testObjectsFactoryController.createNewsLikeDislikeReadDTO();
        UUID registeredUserId = UUID.randomUUID();

        Mockito.when(registeredUserNewsLikeDislikeService.createRegisteredUserNewsLike(registeredUserId,
                create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/registered-user/{registeredUserId}/news-like-dislike",
                registeredUserId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(registeredUserNewsLikeDislikeService, Mockito.never())
                .createRegisteredUserNewsLike(ArgumentMatchers.any(), ArgumentMatchers.any());
    }
}
