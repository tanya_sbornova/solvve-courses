package com.solvve.controller;

import com.solvve.dto.person.*;
import com.solvve.util.TestObjectsFactoryController;
import com.solvve.domain.Person;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.PersonService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({PersonController.class, TestObjectsFactoryController.class})
public class PersonControllerTest extends BaseControllerTest {

    @MockBean
    private PersonService personService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetPerson() throws Exception {

        PersonReadDTO person = testObjectsFactoryController.createPersonReadDTO();

        Mockito.when(personService.getPerson(person.getId())).thenReturn(person);

        String resultJson = mvc.perform(get("/api/v1/persons/{id}", person.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        PersonReadDTO actualPerson = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assertions.assertThat(actualPerson).isEqualToComparingFieldByField(person);

        Mockito.verify(personService).getPerson(person.getId());
    }

    @Test
    public void testGetPersonWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Person.class, wrongId);
        Mockito.when(personService.getPerson(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/persons/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreatePerson() throws Exception {

        PersonCreateDTO create = testObjectsFactoryController.createPersonCreateDTO();
        PersonReadDTO read = testObjectsFactoryController.createPersonReadDTO();

        Mockito.when(personService.createPerson(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/persons/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PersonReadDTO actualPerson = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assertions.assertThat(actualPerson).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchPerson() throws Exception {

        PersonPatchDTO patchDTO = testObjectsFactoryController.createPersonPatchDTO();

        PersonReadDTO read = testObjectsFactoryController.createPersonReadDTO();

        Mockito.when(personService.patchPerson(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/persons/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PersonReadDTO actualAdmin = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assert.assertEquals(read, actualAdmin);
    }

    @Test
    public void testUpdatePerson() throws Exception {

        PersonPutDTO putDTO = testObjectsFactoryController.createPersonPutDTO();

        PersonReadDTO read = testObjectsFactoryController.createPersonReadDTO();

        Mockito.when(personService.updatePerson(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/persons/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PersonReadDTO actualAdmin = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assert.assertEquals(read, actualAdmin);
    }

    @Test
    public void testDeletePerson() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/persons/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(personService).deletePerson(id);
    }
}


