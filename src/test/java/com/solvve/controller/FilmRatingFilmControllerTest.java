package com.solvve.controller;

import com.solvve.domain.Film;
import com.solvve.domain.RatingFilm;
import com.solvve.dto.film.FilmReadDTO;
import com.solvve.dto.ratingfilm.RatingFilmCreateDTO;
import com.solvve.dto.ratingfilm.RatingFilmPatchDTO;
import com.solvve.dto.ratingfilm.RatingFilmPutDTO;
import com.solvve.dto.ratingfilm.RatingFilmReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.service.FilmRatingFilmService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({FilmRatingFilmController.class, TestObjectsFactoryController.class})
public class FilmRatingFilmControllerTest extends BaseControllerTest {

    @MockBean
    private FilmRatingFilmService ratingFilmService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetFilmRatingFilms() throws Exception {


        List<RatingFilmReadDTO> ratingFilms = List.of(testObjectsFactoryController.createRatingFilmReadDTO(),
                                                      testObjectsFactoryController.createRatingFilmReadDTO(),
                                                      testObjectsFactoryController.createRatingFilmReadDTO());
        FilmReadDTO film = testObjectsFactoryController.createFilmReadDTO();

        Mockito.when(ratingFilmService.getFilmRatingFilms(film.getId())).thenReturn(ratingFilms);

        String resultJson = mvc.perform(get("/api/v1/film/{filmId}/rating-films", film.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RatingFilmReadDTO[] arrRatingFilms = objectMapper.readValue(resultJson, RatingFilmReadDTO[].class);

        Assertions.assertThat(ratingFilms).containsExactlyInAnyOrder(arrRatingFilms);
    }

    @Test
    public void testGetFilmRatingFilm() throws Throwable {

        RatingFilmReadDTO ratingFilm = testObjectsFactoryController.createRatingFilmReadDTO();

        Mockito.when(ratingFilmService.getFilmRatingFilm(ratingFilm.getFilmId(),
                ratingFilm.getId())).thenReturn(ratingFilm);

        String resultJson = mvc.perform(get("/api/v1/film/{filmId}/rating-films/{ratingFilmId}",
                ratingFilm.getFilmId().toString(), ratingFilm.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RatingFilmReadDTO actualRatingFilm = objectMapper.readValue(resultJson, RatingFilmReadDTO.class);
        Assertions.assertThat(actualRatingFilm).isEqualToComparingFieldByField(ratingFilm);

        Mockito.verify(ratingFilmService).getFilmRatingFilm(ratingFilm.getFilmId(), ratingFilm.getId());
    }

    @Test
    public void testGetFilmRatingFilmWrongId() throws Throwable {

        UUID wrongId = UUID.randomUUID();
        FilmReadDTO film = testObjectsFactoryController.createFilmReadDTO();

        EntityNotFoundException exception = new EntityNotFoundException(RatingFilm.class, wrongId,
                Film.class, film.getId());
        Mockito.when(ratingFilmService.getFilmRatingFilm(film.getId(), wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/film/{filmId}/rating-films/{ratingFilmId}",
                film.getId().toString(), wrongId.toString()))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateFilmRatingFilm() throws Exception {

        RatingFilmCreateDTO create = testObjectsFactoryController.createRatingFilmCreateDTO();
        RatingFilmReadDTO read = testObjectsFactoryController.createRatingFilmReadDTO();
        UUID filmId = UUID.randomUUID();

        Mockito.when(ratingFilmService.createFilmRatingFilm(filmId, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/film/{filmId}/rating-films",
                filmId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RatingFilmReadDTO actualRatingFilm = objectMapper.readValue(resultJson, RatingFilmReadDTO.class);
        Assertions.assertThat(actualRatingFilm).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchFilmRatingFilm() throws Throwable {

        RatingFilmPatchDTO patchDTO = testObjectsFactoryController.createRatingFilmPatchDTO();
        RatingFilmReadDTO read = testObjectsFactoryController.createRatingFilmReadDTO();
        FilmReadDTO filmReadDTO = testObjectsFactoryController.createFilmReadDTO();

        Mockito.when(ratingFilmService.patchFilmRatingFilm(filmReadDTO.getId(), read.getId(),
                patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/film/{filmId}/rating-films/{ratingFilmId}",
                filmReadDTO.getId().toString(), read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RatingFilmReadDTO actualRatingFilm = objectMapper.readValue(resultJson, RatingFilmReadDTO.class);
        Assert.assertEquals(read, actualRatingFilm);
    }

    @Test
    public void testUpdateFilmRatingFilm() throws Throwable{

        RatingFilmPutDTO putDTO = testObjectsFactoryController.createRatingFilmPutDTO();
        RatingFilmReadDTO read = testObjectsFactoryController.createRatingFilmReadDTO();
        FilmReadDTO filmReadDTO = testObjectsFactoryController.createFilmReadDTO();

        Mockito.when(ratingFilmService.updateFilmRatingFilm(filmReadDTO.getId(), read.getId(),
                putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/film/{filmId}/rating-films/{ratingFilmId}",
                filmReadDTO.getId().toString(), read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RatingFilmReadDTO actualAdmin = objectMapper.readValue(resultJson, RatingFilmReadDTO.class);
        Assert.assertEquals(read, actualAdmin);
    }

    @Test
    public void testDeleteFilmRatingFilm() throws Exception {

        RatingFilmReadDTO read = testObjectsFactoryController.createRatingFilmReadDTO();
        FilmReadDTO filmReadDTO = testObjectsFactoryController.createFilmReadDTO();

        mvc.perform(delete("/api/v1/film/{filmId}/rating-films/{ratingFilmId}",
                filmReadDTO.getId().toString(), read.getId().toString()))
                .andExpect(status().isOk());

        Mockito.verify(ratingFilmService).deleteFilmRatingFilm(filmReadDTO.getId(), read.getId());
    }

    @Test
    public void testCreateRatingFilmValidationFailed() throws Exception {

        RatingFilmCreateDTO create = new RatingFilmCreateDTO();
        RatingFilmReadDTO read = testObjectsFactoryController.createRatingFilmReadDTO();
        UUID filmId = UUID.randomUUID();

        Mockito.when(ratingFilmService.createFilmRatingFilm(filmId, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/film/{filmId}/rating-films",
                filmId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(ratingFilmService, Mockito.never()).createFilmRatingFilm(ArgumentMatchers.any(),
                ArgumentMatchers.any());
    }
}
