package com.solvve.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.solvve.dto.company.*;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.util.TestObjectsFactoryController;
import com.solvve.domain.Company;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.CompanyService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({CompanyController.class, TestObjectsFactoryController.class})
public class CompanyControllerTest extends BaseControllerTest {

    @MockBean
    private CompanyService companyService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetCompany() throws Exception {

        CompanyReadDTO company = testObjectsFactoryController.createCompanyReadDTO();

        Mockito.when(companyService.getCompany(company.getId())).thenReturn(company);

        String resultJson = mvc.perform(get("/api/v1/companies/{id}", company.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CompanyReadDTO actualCompany = objectMapper.readValue(resultJson, CompanyReadDTO.class);
        Assertions.assertThat(actualCompany).isEqualToComparingFieldByField(company);

        Mockito.verify(companyService).getCompany(company.getId());
    }

    @Test
    public void testGetCompanyWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Company.class, wrongId);
        Mockito.when(companyService.getCompany(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/companies/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetcompanys() throws Exception {

        List<CompanyReadDTO> companies = List.of(testObjectsFactoryController.createCompanyReadDTO(),
                testObjectsFactoryController.createCompanyReadDTO());

        Mockito.when(companyService.getCompanies()).thenReturn(companies);

        String resultJson = mvc.perform(get("/api/v1/companies"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CompanyReadDTO[] arrCompanys = objectMapper.readValue(resultJson, CompanyReadDTO[].class);
        Assertions.assertThat(companies).containsExactlyInAnyOrder(arrCompanys);
    }

    @Test
    public void testCreateCompany() throws Exception {

        UUID personId = UUID.randomUUID();

        CompanyCreateDTO create = testObjectsFactoryController.createCompanyCreateDTO();

        CompanyReadDTO read = testObjectsFactoryController.createCompanyReadDTO();

        Mockito.when(companyService.createCompany(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/companies/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CompanyReadDTO actualCompany = objectMapper.readValue(resultJson, CompanyReadDTO.class);
        Assertions.assertThat(actualCompany).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testAddCompanyToFilm() throws Exception {

        UUID filmId = UUID.randomUUID();
        UUID companyId = UUID.randomUUID();

        CompanyReadDTO read = testObjectsFactoryController.createCompanyReadDTO();
        List<CompanyReadDTO> expectedCompanys = List.of(read);

        Mockito.when(companyService.addCompanyToFilm(filmId, companyId)).thenReturn(expectedCompanys);

        String resultJson = mvc.perform(post("/api/v1/film/{filmId}/companies/{id}", filmId, companyId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<CompanyReadDTO> actualCompanys = objectMapper.readValue(resultJson,
                new TypeReference<List<CompanyReadDTO>>() {
                });
        Assert.assertEquals(expectedCompanys, actualCompanys);
    }

    @Test
    public void testDeleteCompany() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/companies/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(companyService).deleteCompany(id);
    }

    @Test
    public void testRemoveCompanyFromFilm() throws Exception {

        UUID filmId = UUID.randomUUID();
        UUID companyId = UUID.randomUUID();

        List<CompanyReadDTO> expectedCompanys = List.of();

        Mockito.when(companyService.removeCompanyFromFilm(filmId, companyId)).thenReturn(expectedCompanys);

        String resultJson = mvc.perform(delete("/api/v1/film/{filmId}/companies/{id}", filmId, companyId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<CompanyReadDTO> actualCompanys = objectMapper.readValue(resultJson,
                new TypeReference<List<CompanyReadDTO>>() {
                });
        Assert.assertEquals(expectedCompanys, actualCompanys);
    }

    @Test
    public void testCreateCompanyValidationFailed() throws Exception {

        CompanyCreateDTO create = new CompanyCreateDTO();

        CompanyReadDTO read = testObjectsFactoryController.createCompanyReadDTO();

        Mockito.when(companyService.createCompany(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/companies/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(companyService, Mockito.never()).createCompany(ArgumentMatchers.any());
    }
}

