package com.solvve.controller;

import com.solvve.domain.MisprintInfoRole;
import com.solvve.domain.RegisteredUser;
import com.solvve.dto.misprintinforole.MisprintInfoRoleReadDTO;
import com.solvve.dto.misprintinforole.MisprintInfoRoleCreateDTO;
import com.solvve.dto.registereduser.RegisteredUserReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.RegisteredUserMisprintInfoRoleService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({RegisteredUserMisprintInfoRoleController.class, TestObjectsFactoryController.class})
public class RegisteredUserMisprintInfoRoleControllerTest extends BaseControllerTest {

    @MockBean
    private RegisteredUserMisprintInfoRoleService registeredUserMisprintInfoRoleService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetRegisteredMisprintInfoRoleLikes() throws Exception {

        List<MisprintInfoRoleReadDTO> misprintInfoRoles = List.of(testObjectsFactoryController.
                createMisprintInfoRoleReadDTO());
        RegisteredUserReadDTO registeredUser = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserMisprintInfoRoleService.getRegisteredUserMisprintInfoRoles(registeredUser.getId())).
                thenReturn(misprintInfoRoles);

        String resultJson = mvc.perform(get("/api/v1/registered-user/{registeredUserId}/misprint-info-roles",
                registeredUser.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MisprintInfoRoleReadDTO[] arrMisprintInfoRoles = objectMapper.readValue(resultJson,
                MisprintInfoRoleReadDTO[].class);
        Assertions.assertThat(misprintInfoRoles).containsExactlyInAnyOrder(arrMisprintInfoRoles);
    }

    @Test
    public void testGetRegisteredMisprintInfoRoleLike() throws Throwable {

        MisprintInfoRoleReadDTO misprintInfoRole = testObjectsFactoryController.createMisprintInfoRoleReadDTO();

        Mockito.when(registeredUserMisprintInfoRoleService.
                getRegisteredUserMisprintInfoRole(misprintInfoRole.getRegisteredUserId(),
                        misprintInfoRole.getId())).thenReturn(misprintInfoRole);

        String resultJson = mvc.perform(get(
                "/api/v1/registered-user/{registeredUserId}/misprint-info-roles/{misprintInfoRoleId}",
                misprintInfoRole.getRegisteredUserId().toString(),
                misprintInfoRole.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MisprintInfoRoleReadDTO actualMisprintInfoRole = objectMapper.readValue(resultJson,
                MisprintInfoRoleReadDTO.class);
        Assertions.assertThat(actualMisprintInfoRole).isEqualToComparingFieldByField(misprintInfoRole);

        Mockito.verify(registeredUserMisprintInfoRoleService).getRegisteredUserMisprintInfoRole(misprintInfoRole.
                getRegisteredUserId(), misprintInfoRole.getId());
    }

    @Test
    public void testGetRegisteredMisprintInfoRoleLikeWrongId() throws Throwable {

        UUID wrongId = UUID.randomUUID();
        RegisteredUserReadDTO registeredUser = testObjectsFactoryController.createRegisteredUserReadDTO();

        EntityNotFoundException exception = new EntityNotFoundException(MisprintInfoRole.class, wrongId,
                RegisteredUser.class, registeredUser.getId());
        Mockito.when(registeredUserMisprintInfoRoleService.getRegisteredUserMisprintInfoRole(registeredUser.getId(),
                wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get(
                "/api/v1/registered-user/{registeredUserId}/misprint-info-roles/{misprintInfoRoleId}",
                registeredUser.getId().toString(), wrongId.toString()))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateRegisteredMisprintInfoRoleLike() throws Exception {

        MisprintInfoRoleCreateDTO create = testObjectsFactoryController.createMisprintInfoRoleCreateDTO();
        MisprintInfoRoleReadDTO read = testObjectsFactoryController.createMisprintInfoRoleReadDTO();
        UUID registeredUserId = UUID.randomUUID();

        Mockito.when(registeredUserMisprintInfoRoleService.createRegisteredUserMisprintInfoRole(registeredUserId,
                create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/registered-user/{registeredUserId}/misprint-info-roles",
                registeredUserId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MisprintInfoRoleReadDTO actualMisprintInfoRole = objectMapper.readValue(resultJson,
                MisprintInfoRoleReadDTO.class);
        Assertions.assertThat(actualMisprintInfoRole).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testDeleteRegisteredMisprintInfoRoleLike() throws Exception {

        MisprintInfoRoleReadDTO read = testObjectsFactoryController.createMisprintInfoRoleReadDTO();
        RegisteredUserReadDTO registeredUserReadDTO = testObjectsFactoryController.createRegisteredUserReadDTO();

        mvc.perform(delete("/api/v1/registered-user/{registeredUserId}/misprint-info-roles/{misprintInfoRoleId}",
                registeredUserReadDTO.getId().toString(), read.getId().toString()))
                .andExpect(status().isOk());

        Mockito.verify(registeredUserMisprintInfoRoleService).
                deleteRegisteredUserMisprintInfoRole(registeredUserReadDTO.getId(), read.getId());
    }
}

