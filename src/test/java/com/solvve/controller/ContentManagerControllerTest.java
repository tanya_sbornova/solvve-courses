package com.solvve.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.solvve.domain.ContentManager;
import com.solvve.domain.PageResult;
import com.solvve.domain.UserRoleType;
import com.solvve.domain.UserStatus;
import com.solvve.dto.contentmanager.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.service.ContentManagerService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({ContentManagerController.class, TestObjectsFactoryController.class})
public class ContentManagerControllerTest extends BaseControllerTest {

    @MockBean
    private ContentManagerService contentManagerService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetContentManager() throws Exception {

        ContentManagerReadDTO contentManager = testObjectsFactoryController.createContentManagerReadDTO();

        Mockito.when(contentManagerService.getContentManager(contentManager.getId())).
                thenReturn(contentManager);

        String resultJson = mvc.perform(get("/api/v1/content-managers/{id}", contentManager.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ContentManagerReadDTO actualContentManager =
                objectMapper.readValue(resultJson, ContentManagerReadDTO.class);
        Assertions.assertThat(actualContentManager).isEqualToComparingFieldByField(contentManager);

        Mockito.verify(contentManagerService).getContentManager(contentManager.getId());
    }

    @Test
    public void testGetContentManagerWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(ContentManager.class, wrongId);
        Mockito.when(contentManagerService.getContentManager(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/content-managers/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateContentManager() throws Exception {

        ContentManagerCreateDTO create = testObjectsFactoryController.createContentManagerCreateDTO();

        ContentManagerReadDTO read = testObjectsFactoryController.createContentManagerReadDTO();

        Mockito.when(contentManagerService.createContentManager(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/content-managers/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ContentManagerReadDTO actualContentManager =
                objectMapper.readValue(resultJson, ContentManagerReadDTO.class);
        Assertions.assertThat(actualContentManager).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testCreateContentManagerValidationFailed() throws Exception {

        ContentManagerCreateDTO create = new ContentManagerCreateDTO();

        ContentManagerReadDTO read = testObjectsFactoryController.createContentManagerReadDTO();

        Mockito.when(contentManagerService.createContentManager(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/content-managers/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(contentManagerService, Mockito.never()).createContentManager(ArgumentMatchers.any());
    }

    @Test
    public void testPatchContentManager() throws Exception {

        ContentManagerPatchDTO patchDTO = new ContentManagerPatchDTO();
        patchDTO.setEncodedPassword("12345");
        patchDTO.setUserRole(UserRoleType.CONTENT_MANAGER);
        patchDTO.setUserStatus(UserStatus.BLOCKED);

        ContentManagerReadDTO read = testObjectsFactoryController.createContentManagerReadDTO();

        Mockito.when(contentManagerService.patchContentManager(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/content-managers/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ContentManagerReadDTO actualContentManager =
                objectMapper.readValue(resultJson, ContentManagerReadDTO.class);
        Assert.assertEquals(read, actualContentManager);
    }

    @Test
    public void testUpdateContentManager() throws Exception {

        ContentManagerPutDTO putDTO = new ContentManagerPutDTO();
        putDTO.setUserRole(UserRoleType.CONTENT_MANAGER);
        putDTO.setUserStatus(UserStatus.BLOCKED);

        ContentManagerReadDTO read = testObjectsFactoryController.createContentManagerReadDTO();

        Mockito.when(contentManagerService.updateContentManager(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/content-managers/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ContentManagerReadDTO actualContentManager =
                objectMapper.readValue(resultJson, ContentManagerReadDTO.class);
        Assert.assertEquals(read, actualContentManager);
    }

    @Test
    public void testDeleteContentManager() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/content-managers/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(contentManagerService).deleteContentManager(id);
    }

    @Test
    public void testGetContentManagersWithPagingAndSorting() throws Exception {
        ContentManagerFilter contentManagerFilter = new ContentManagerFilter();
        contentManagerFilter.setEmail("test1@mail.ru");
        contentManagerFilter.setUserStatus(UserStatus.NEW);

        ContentManagerReadDTO read = testObjectsFactoryController.createContentManagerReadDTO();

        int page = 1;
        int size = 25;

        PageResult<ContentManagerReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(new Long(100));
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "email"));
        Mockito.when(contentManagerService.getContentManagers(contentManagerFilter, pageRequest))
                .thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/content-managers")
                .param("email", contentManagerFilter.getEmail().toString())
                .param("userStatus", contentManagerFilter.getUserStatus().toString())
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "email,asc")
        )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<ContentManagerReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {});
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetContentManagersWithBigPage() throws Exception {
        ContentManagerFilter contentManagerFilter = new ContentManagerFilter();
        contentManagerFilter.setEmail("test1@mail.ru");
        contentManagerFilter.setUserStatus(UserStatus.NEW);

        ContentManagerReadDTO read = testObjectsFactoryController.createContentManagerReadDTO();

        int page = 0;
        int size = 999999;

        PageResult<ContentManagerReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(new Long(100));
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(page, maxPageSize, Sort.by(Sort.Direction.ASC, "email"));
        Mockito.when(contentManagerService.getContentManagers(contentManagerFilter, pageRequest))
                .thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/content-managers")
                .param("email", contentManagerFilter.getEmail().toString())
                .param("userStatus", contentManagerFilter.getUserStatus().toString())
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "email,asc")
        )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<ContentManagerReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {});
        Assert.assertEquals(resultPage, actualPage);
    }
}


