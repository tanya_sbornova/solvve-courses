package com.solvve.controller;

import com.solvve.domain.Admin;
import com.solvve.domain.UserRoleType;
import com.solvve.domain.UserStatus;
import com.solvve.dto.admin.AdminCreateDTO;
import com.solvve.dto.admin.AdminPatchDTO;
import com.solvve.dto.admin.AdminPutDTO;
import com.solvve.dto.admin.AdminReadDTO;
import com.solvve.exception.ControllerValidationException;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.service.AdminService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({AdminController.class, TestObjectsFactoryController.class})
public class AdminControllerTest extends BaseControllerTest {

    @MockBean
    private AdminService adminService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetAdmin() throws Exception {

        AdminReadDTO admin = testObjectsFactoryController.createAdminReadDTO();

        Mockito.when(adminService.getAdmin(admin.getId())).thenReturn(admin);

        String resultJson = mvc.perform(get("/api/v1/admins/{id}", admin.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        AdminReadDTO actualAdmin = objectMapper.readValue(resultJson, AdminReadDTO.class);
        Assertions.assertThat(actualAdmin).isEqualToComparingFieldByField(admin);

        Mockito.verify(adminService).getAdmin(admin.getId());
    }

    @Test
    public void testGetAdminWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Admin.class, wrongId);
        Mockito.when(adminService.getAdmin(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/admins/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateAdmin() throws Exception {

        AdminCreateDTO create = testObjectsFactoryController.createAdminCreateDTO();

        AdminReadDTO read = testObjectsFactoryController.createAdminReadDTO();

        Mockito.when(adminService.createAdmin(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/admins/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        AdminReadDTO actualAdmin = objectMapper.readValue(resultJson, AdminReadDTO.class);
        Assertions.assertThat(actualAdmin).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchAdmin() throws Exception {

        AdminPatchDTO patchDTO = new AdminPatchDTO();
        patchDTO.setEncodedPassword("12345");
        patchDTO.setUserRole(UserRoleType.ADMIN);
        patchDTO.setUserStatus(UserStatus.BLOCKED);

        AdminReadDTO read = testObjectsFactoryController.createAdminReadDTO();

        Mockito.when(adminService.patchAdmin(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/admins/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        AdminReadDTO actualAdmin = objectMapper.readValue(resultJson, AdminReadDTO.class);
        Assert.assertEquals(read, actualAdmin);
    }

    @Test
    public void testPutAdmin() throws Exception {

        AdminPutDTO putDTO = new AdminPutDTO();
        putDTO.setUserRole(UserRoleType.ADMIN);
        putDTO.setUserStatus(UserStatus.BLOCKED);

        AdminReadDTO read = testObjectsFactoryController.createAdminReadDTO();

        Mockito.when(adminService.updateAdmin(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/admins/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        AdminReadDTO actualAdmin = objectMapper.readValue(resultJson, AdminReadDTO.class);
        Assert.assertEquals(read, actualAdmin);
    }

    @Test
    public void testDeleteAdmin() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/admins/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(adminService).deleteAdmin(id);
    }

    @Test
    public void testCreateAdminValidationFailed() throws Exception {

        AdminCreateDTO create = new AdminCreateDTO();

        AdminReadDTO read = testObjectsFactoryController.createAdminReadDTO();

        Mockito.when(adminService.createAdmin(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/admins/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(adminService, Mockito.never()).createAdmin(ArgumentMatchers.any());
    }

    @Test
    public void testCreateAdminWrongDates() throws Exception {

        AdminCreateDTO create = testObjectsFactoryController.createAdminCreateDTO();

        create.setStartAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        create.setFinishAt(create.getStartAt().minus(30, ChronoUnit.MINUTES));

        String resultJson = mvc.perform(post("/api/v1/admins/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();
        System.out.println(resultJson);
        ErrorInfo errorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assert.assertTrue(errorInfo.getMessage().contains("startAt"));
        Assert.assertTrue(errorInfo.getMessage().contains("finishAt"));
        Assert.assertEquals(ControllerValidationException.class, errorInfo.getExceptionClass());
        Mockito.verify(adminService, Mockito.never()).createAdmin(ArgumentMatchers.any());
    }
}