package com.solvve.controller;

import com.solvve.domain.ReviewRoleLikeDislike;
import com.solvve.domain.RegisteredUser;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikeCreateDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePatchDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePutDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikeReadDTO;
import com.solvve.dto.registereduser.RegisteredUserReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.service.RegisteredUserReviewRoleLikeService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({RegisteredUserReviewRoleLikeController.class, TestObjectsFactoryController.class})
public class RegisteredUserReviewRoleLikeControllerTest extends BaseControllerTest {

    @MockBean
    private RegisteredUserReviewRoleLikeService registeredUserReviewRoleLikeDislikeService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetRegisteredUserNewLikes() throws Exception {

        List<ReviewRoleLikeDislikeReadDTO> reviewRoleLikeDislikes =
                List.of(testObjectsFactoryController.createReviewRoleLikeDislikeReadDTO());
        RegisteredUserReadDTO registeredUser = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserReviewRoleLikeDislikeService.
                getRegisteredUserReviewRoleLikes(registeredUser.getId())).thenReturn(reviewRoleLikeDislikes);

        String resultJson = mvc.perform(get("/api/v1/registered-user/{registeredUserId}/review-role-like-dislike",
                registeredUser.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewRoleLikeDislikeReadDTO[] arrReviewRoleLikeDislikes =
                objectMapper.readValue(resultJson, ReviewRoleLikeDislikeReadDTO[].class);
        Assertions.assertThat(reviewRoleLikeDislikes).containsExactlyInAnyOrder(arrReviewRoleLikeDislikes);
    }

    @Test
    public void testGetRegisteredUserNewLike() throws Throwable {

        ReviewRoleLikeDislikeReadDTO reviewRoleLikeDislike = testObjectsFactoryController.
                createReviewRoleLikeDislikeReadDTO();
        RegisteredUserReadDTO registeredUser = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserReviewRoleLikeDislikeService.
                getRegisteredUserReviewRoleLike(registeredUser.getId(),
                        reviewRoleLikeDislike.getId())).thenReturn(reviewRoleLikeDislike);

        String resultJson = mvc.perform(get(
                "/api/v1/registered-user/{registeredUserId}/review-role-like-dislike/{reviewRoleLikeDislikeId}",
                registeredUser.getId().toString(),
                reviewRoleLikeDislike.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewRoleLikeDislikeReadDTO actualReviewRoleLikeDislike = objectMapper.
                readValue(resultJson, ReviewRoleLikeDislikeReadDTO.class);
        Assertions.assertThat(actualReviewRoleLikeDislike).isEqualToComparingFieldByField(reviewRoleLikeDislike);

        Mockito.verify(registeredUserReviewRoleLikeDislikeService).
                getRegisteredUserReviewRoleLike(registeredUser.getId(),
                        reviewRoleLikeDislike.getId());
    }

    @Test
    public void testGetRegisteredUserNewLikeWrongId() throws Throwable {

        UUID wrongId = UUID.randomUUID();
        RegisteredUserReadDTO registeredUser = testObjectsFactoryController.createRegisteredUserReadDTO();

        EntityNotFoundException exception = new EntityNotFoundException(ReviewRoleLikeDislike.class, wrongId,
                RegisteredUser.class, registeredUser.getId());
        Mockito.when(registeredUserReviewRoleLikeDislikeService.
                getRegisteredUserReviewRoleLike(registeredUser.getId(), wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get(
                "/api/v1/registered-user/{registeredUserId}/review-role-like-dislike/{reviewRoleLikeDislikeId}",
                registeredUser.getId().toString(), wrongId.toString()))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateRegisteredUserNewLike() throws Exception {

        ReviewRoleLikeDislikeCreateDTO create = testObjectsFactoryController.createReviewRoleLikeDislikeCreateDTO();
        ReviewRoleLikeDislikeReadDTO read = testObjectsFactoryController.createReviewRoleLikeDislikeReadDTO();
        UUID registeredUserId = UUID.randomUUID();

        Mockito.when(registeredUserReviewRoleLikeDislikeService.
                createRegisteredUserReviewRoleLike(registeredUserId, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/registered-user/{registeredUserId}/review-role-like-dislike",
                registeredUserId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewRoleLikeDislikeReadDTO actualReviewRoleLikeDislike = objectMapper.
                readValue(resultJson, ReviewRoleLikeDislikeReadDTO.class);
        Assertions.assertThat(actualReviewRoleLikeDislike).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchRegisteredUserNewLike() throws Throwable {

        ReviewRoleLikeDislikePatchDTO patchDTO = testObjectsFactoryController.createReviewRoleLikeDislikePatchDTO();
        ReviewRoleLikeDislikeReadDTO read = testObjectsFactoryController.createReviewRoleLikeDislikeReadDTO();
        RegisteredUserReadDTO registeredUserReadDTO = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserReviewRoleLikeDislikeService.
                patchRegisteredUserReviewRoleLike(registeredUserReadDTO.getId(), read.getId(),
                        patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch(
                "/api/v1/registered-user/{registeredUserId}/review-role-like-dislike/{reviewRoleLikeDislikeId}",
                registeredUserReadDTO.getId().toString(), read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewRoleLikeDislikeReadDTO actualReviewRoleLikeDislike = objectMapper.readValue(resultJson,
                ReviewRoleLikeDislikeReadDTO.class);
        Assert.assertEquals(read, actualReviewRoleLikeDislike);
    }

    @Test
    public void testUpdateRegisteredUserNewLike() throws Throwable{

        ReviewRoleLikeDislikePutDTO putDTO = testObjectsFactoryController.createReviewRoleLikeDislikePutDTO();
        ReviewRoleLikeDislikeReadDTO read = testObjectsFactoryController.createReviewRoleLikeDislikeReadDTO();
        RegisteredUserReadDTO registeredUserReadDTO = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserReviewRoleLikeDislikeService.
                updateRegisteredUserReviewRoleLike(registeredUserReadDTO.getId(), read.getId(),
                        putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put(
                "/api/v1/registered-user/{registeredUserId}/review-role-like-dislike/{reviewRoleLikeDislikeId}",
                registeredUserReadDTO.getId().toString(), read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewRoleLikeDislikeReadDTO actualAdmin = objectMapper.
                readValue(resultJson, ReviewRoleLikeDislikeReadDTO.class);
        Assert.assertEquals(read, actualAdmin);
    }

    @Test
    public void testDeleteRegisteredUserNewLike() throws Exception {

        ReviewRoleLikeDislikeReadDTO read = testObjectsFactoryController.createReviewRoleLikeDislikeReadDTO();
        RegisteredUserReadDTO registeredUserReadDTO = testObjectsFactoryController.createRegisteredUserReadDTO();

        mvc.perform(delete(
                "/api/v1/registered-user/{registeredUserId}/review-role-like-dislike/{reviewRoleLikeDislikeId}",
                registeredUserReadDTO.getId().toString(), read.getId().toString()))
                .andExpect(status().isOk());

        Mockito.verify(registeredUserReviewRoleLikeDislikeService).
                deleteRegisteredUserReviewRoleLike(registeredUserReadDTO.getId(), read.getId());
    }

    @Test
    public void testCreateReviewRoleLikeDislikeValidationFailed() throws Exception {

        ReviewRoleLikeDislikeCreateDTO create = new ReviewRoleLikeDislikeCreateDTO();
        ReviewRoleLikeDislikeReadDTO read = testObjectsFactoryController.createReviewRoleLikeDislikeReadDTO();
        UUID registeredUserId = UUID.randomUUID();

        Mockito.when(registeredUserReviewRoleLikeDislikeService.
                createRegisteredUserReviewRoleLike(registeredUserId, create)).thenReturn(read);

        String resultJson = mvc.perform(post(
                "/api/v1/registered-user/{registeredUserId}/review-role-like-dislike",
                registeredUserId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(registeredUserReviewRoleLikeDislikeService, Mockito.never())
                .createRegisteredUserReviewRoleLike(ArgumentMatchers.any(), ArgumentMatchers.any());
    }
}

