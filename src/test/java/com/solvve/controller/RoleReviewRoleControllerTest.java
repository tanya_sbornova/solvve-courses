package com.solvve.controller;

import com.solvve.domain.Role;
import com.solvve.domain.ReviewRole;
import com.solvve.dto.role.RoleReadDTO;
import com.solvve.dto.reviewrole.ReviewRoleCreateDTO;
import com.solvve.dto.reviewrole.ReviewRolePatchDTO;
import com.solvve.dto.reviewrole.ReviewRolePutDTO;
import com.solvve.dto.reviewrole.ReviewRoleReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.service.RoleReviewRoleService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({RoleReviewRoleController.class, TestObjectsFactoryController.class})
public class RoleReviewRoleControllerTest extends BaseControllerTest {

    @MockBean
    private RoleReviewRoleService reviewRoleService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetRoleReviewRoles() throws Exception {

        List<ReviewRoleReadDTO> reviewRoles = List.of(testObjectsFactoryController.createReviewRoleReadDTO(),
                                                      testObjectsFactoryController.createReviewRoleReadDTO(),
                                                      testObjectsFactoryController.createReviewRoleReadDTO());
        UUID roleId = UUID.randomUUID();

        Mockito.when(reviewRoleService.getRoleReviewRoles(roleId)).thenReturn(reviewRoles);

        String resultJson = mvc.perform(get("/api/v1/role/{roleId}/review-roles", roleId.toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewRoleReadDTO[] arrReviewRoles = objectMapper.readValue(resultJson, ReviewRoleReadDTO[].class);
        Assertions.assertThat(reviewRoles).containsExactlyInAnyOrder(arrReviewRoles);
    }

    @Test
    public void testGetRoleReviewsRoleOfCurrentRegisteredUser() throws Exception {

        List<ReviewRoleReadDTO> reviewsRole = List.of(testObjectsFactoryController.createReviewRoleReadDTO(),
                testObjectsFactoryController.createReviewRoleReadDTO());
        UUID roleId = UUID.randomUUID();
        UUID registeredUserId = UUID.randomUUID();

        Mockito.when(reviewRoleService.getRoleReviewsRoleOfCurrentRegisteredUser(registeredUserId, roleId))
                .thenReturn(reviewsRole);

        String resultJson = mvc.perform(get("/api/v1/registered-user/{registeredUserId}/role/{roleId}/review-roles",
                registeredUserId.toString(), roleId.toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewRoleReadDTO[] arrReviewsFilm = objectMapper.readValue(resultJson, ReviewRoleReadDTO[].class);
        Assertions.assertThat(reviewsRole).containsExactlyInAnyOrder(arrReviewsFilm);
    }

    @Test
    public void testGetRoleReviewRole() throws Throwable {

        ReviewRoleReadDTO reviewRole = testObjectsFactoryController.createReviewRoleReadDTO();
        UUID roleId = UUID.randomUUID();

        Mockito.when(reviewRoleService.getRoleReviewRole(roleId,
                reviewRole.getId())).thenReturn(reviewRole);

        String resultJson = mvc.perform(get("/api/v1/role/{roleId}/review-roles/{reviewRoleId}",
                roleId.toString(),
                reviewRole.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewRoleReadDTO actualReviewRole = objectMapper.readValue(resultJson, ReviewRoleReadDTO.class);
        Assertions.assertThat(actualReviewRole).isEqualToComparingFieldByField(reviewRole);

        Mockito.verify(reviewRoleService).getRoleReviewRole(roleId, reviewRole.getId());
    }

    @Test
    public void testGetRoleReviewRoleWrongId() throws Throwable {

        UUID wrongId = UUID.randomUUID();
        RoleReadDTO role = testObjectsFactoryController.createRoleReadDTO();

        EntityNotFoundException exception = new EntityNotFoundException(ReviewRole.class, wrongId,
                Role.class, role.getId());
        Mockito.when(reviewRoleService.getRoleReviewRole(role.getId(), wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/role/{roleId}/review-roles/{reviewRoleId}",
                role.getId().toString(), wrongId.toString()))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateRoleReviewRole() throws Exception {

        ReviewRoleCreateDTO create = testObjectsFactoryController.createReviewRoleCreateDTO();
        ReviewRoleReadDTO read = testObjectsFactoryController.createReviewRoleReadDTO();
        UUID roleId = UUID.randomUUID();

        Mockito.when(reviewRoleService.createRoleReviewRole(roleId, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/role/{roleId}/review-roles",
                roleId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewRoleReadDTO actualReviewRole = objectMapper.readValue(resultJson, ReviewRoleReadDTO.class);
        Assertions.assertThat(actualReviewRole).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchRoleReviewRole() throws Throwable {

        ReviewRolePatchDTO patchDTO = testObjectsFactoryController.createReviewRolePatchDTO();
        ReviewRoleReadDTO read = testObjectsFactoryController.createReviewRoleReadDTO();
        UUID roleId = UUID.randomUUID();

        Mockito.when(reviewRoleService.patchRoleReviewRole(roleId, read.getId(),
                patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/role/{roleId}/review-roles/{reviewRoleId}",
                roleId.toString(), read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewRoleReadDTO actualReviewRole = objectMapper.readValue(resultJson, ReviewRoleReadDTO.class);
        Assert.assertEquals(read, actualReviewRole);
    }

    @Test
    public void testUpdateRoleReviewRole() throws Throwable{

        ReviewRolePutDTO putDTO = testObjectsFactoryController.createReviewRolePutDTO();
        ReviewRoleReadDTO read = testObjectsFactoryController.createReviewRoleReadDTO();
        UUID roleId = UUID.randomUUID();

        Mockito.when(reviewRoleService.updateRoleReviewRole(roleId, read.getId(),
                putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/role/{roleId}/review-roles/{reviewRoleId}",
                roleId.toString(), read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewRoleReadDTO actualAdmin = objectMapper.readValue(resultJson, ReviewRoleReadDTO.class);
        Assert.assertEquals(read, actualAdmin);
    }

    @Test
    public void testDeleteRoleReviewRole() throws Exception {

        ReviewRoleReadDTO read = testObjectsFactoryController.createReviewRoleReadDTO();
        RoleReadDTO roleReadDTO = testObjectsFactoryController.createRoleReadDTO();

        mvc.perform(delete("/api/v1/role/{roleId}/review-roles/{reviewRoleId}",
                roleReadDTO.getId().toString(), read.getId().toString()))
                .andExpect(status().isOk());

        Mockito.verify(reviewRoleService).deleteRoleReviewRole(roleReadDTO.getId(), read.getId());
    }

    @Test
    public void testCreateReviewRoleValidationFailed() throws Exception {

        ReviewRoleCreateDTO create = new ReviewRoleCreateDTO();
        ReviewRoleReadDTO read = testObjectsFactoryController.createReviewRoleReadDTO();
        UUID roleId = UUID.randomUUID();

        Mockito.when(reviewRoleService.createRoleReviewRole(roleId, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/role/{roleId}/review-roles",
                roleId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(reviewRoleService, Mockito.never()).createRoleReviewRole(ArgumentMatchers.any(),
                ArgumentMatchers.any());
    }
}
