package com.solvve.controller;

import com.solvve.dto.department.*;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.util.TestObjectsFactoryController;
import com.solvve.domain.Department;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.DepartmentService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({DepartmentController.class, TestObjectsFactoryController.class})
public class DepartmentControllerTest extends BaseControllerTest {

    @MockBean
    private DepartmentService departmentService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetDepartment() throws Exception {

        DepartmentReadDTO department = testObjectsFactoryController.createDepartmentReadDTO();

        Mockito.when(departmentService.getDepartment(department.getId())).thenReturn(department);

        String resultJson = mvc.perform(get("/api/v1/departments/{id}", department.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        DepartmentReadDTO actualDepartment = objectMapper.readValue(resultJson, DepartmentReadDTO.class);
        Assertions.assertThat(actualDepartment).isEqualToComparingFieldByField(department);

        Mockito.verify(departmentService).getDepartment(department.getId());
    }

    @Test
    public void testGetDepartmentWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Department.class, wrongId);
        Mockito.when(departmentService.getDepartment(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/departments/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateDepartment() throws Exception {

        DepartmentCreateDTO create = testObjectsFactoryController.createDepartmentCreateDTO();
        DepartmentReadDTO read = testObjectsFactoryController.createDepartmentReadDTO();

        Mockito.when(departmentService.createDepartment(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/departments")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        DepartmentReadDTO actualDepartment = objectMapper.readValue(resultJson, DepartmentReadDTO.class);
        Assertions.assertThat(actualDepartment).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testDeleteDepartment() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/departments/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(departmentService).deleteDepartment(id);
    }
}
