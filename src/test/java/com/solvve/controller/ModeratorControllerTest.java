package com.solvve.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.solvve.domain.Moderator;
import com.solvve.domain.PageResult;
import com.solvve.domain.UserStatus;
import com.solvve.dto.moderator.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.service.ModeratorService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({ModeratorController.class, TestObjectsFactoryController.class})
public class ModeratorControllerTest extends BaseControllerTest {

    @MockBean
    private ModeratorService moderatorService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetModerator() throws Exception {

        ModeratorReadDTO moderator = testObjectsFactoryController.createModeratorReadDTO();

        Mockito.when(moderatorService.getModerator(moderator.getId())).thenReturn(moderator);

        String resultJson = mvc.perform(get("/api/v1/moderators/{id}", moderator.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        ModeratorReadDTO actualModerator = objectMapper.readValue(resultJson, ModeratorReadDTO.class);
        Assertions.assertThat(actualModerator).isEqualToComparingFieldByField(moderator);

        Mockito.verify(moderatorService).getModerator(moderator.getId());
    }

    @Test
    public void testGetModeratorWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Moderator.class, wrongId);
        Mockito.when(moderatorService.getModerator(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/moderators/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateModerator() throws Exception {

        ModeratorCreateDTO create = testObjectsFactoryController.createModeratorCreateDTO();

        ModeratorReadDTO read = testObjectsFactoryController.createModeratorReadDTO();

        Mockito.when(moderatorService.createModerator(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/moderators/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ModeratorReadDTO actualModerator = objectMapper.readValue(resultJson, ModeratorReadDTO.class);
        Assertions.assertThat(actualModerator).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchModerator() throws Exception {

        ModeratorPatchDTO patchDTO = testObjectsFactoryController.generateObject(ModeratorPatchDTO.class);

        ModeratorReadDTO read = testObjectsFactoryController.createModeratorReadDTO();

        Mockito.when(moderatorService.patchModerator(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/moderators/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ModeratorReadDTO actualModerator = objectMapper.readValue(resultJson, ModeratorReadDTO.class);
        Assert.assertEquals(read, actualModerator);
    }

    @Test
    public void testUpdateModerator() throws Exception {

        ModeratorPutDTO putDTO = testObjectsFactoryController.generateObject(ModeratorPutDTO.class);

        ModeratorReadDTO read = testObjectsFactoryController.createModeratorReadDTO();

        Mockito.when(moderatorService.updateModerator(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/moderators/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ModeratorReadDTO actualModerator = objectMapper.readValue(resultJson, ModeratorReadDTO.class);
        Assert.assertEquals(read, actualModerator);
    }

    @Test
    public void testDeleteModerator() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/moderators/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(moderatorService).deleteModerator(id);
    }

    @Test
    public void testCreateModeratorValidationFailed() throws Exception {

        ModeratorCreateDTO create = new ModeratorCreateDTO();

        ModeratorReadDTO read = testObjectsFactoryController.createModeratorReadDTO();

        Mockito.when(moderatorService.createModerator(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/moderators/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(moderatorService, Mockito.never()).createModerator(ArgumentMatchers.any());
    }

    @Test
    public void testGetModeratorsWithPagingAndSorting() throws Exception {
        ModeratorFilter moderatorFilter = new ModeratorFilter();
        moderatorFilter.setEmail("test1@mail.ru");
        moderatorFilter.setUserStatus(UserStatus.NEW);

        ModeratorReadDTO read = testObjectsFactoryController.createModeratorReadDTO();

        int page = 1;
        int size = 25;

        PageResult<ModeratorReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(new Long(100));
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "email"));
        Mockito.when(moderatorService.getModerators(moderatorFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/moderators")
                .param("email", moderatorFilter.getEmail().toString())
                .param("userStatus", moderatorFilter.getUserStatus().toString())
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "email,asc")
        )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<ModeratorReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {});
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetModeratorsWithBigPage() throws Exception {
        ModeratorFilter moderatorFilter = new ModeratorFilter();
        moderatorFilter.setEmail("test1@mail.ru");
        moderatorFilter.setUserStatus(UserStatus.NEW);

        ModeratorReadDTO read = testObjectsFactoryController.createModeratorReadDTO();

        int page = 0;
        int size = 999999;

        PageResult<ModeratorReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(new Long(100));
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(page, maxPageSize, Sort.by(Sort.Direction.ASC, "email"));
        Mockito.when(moderatorService.getModerators(moderatorFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/moderators")
                .param("email", moderatorFilter.getEmail().toString())
                .param("userStatus", moderatorFilter.getUserStatus().toString())
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "email,asc")
        )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<ModeratorReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {});
        Assert.assertEquals(resultPage, actualPage);
    }
}
