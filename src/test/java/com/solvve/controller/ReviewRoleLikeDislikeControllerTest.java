package com.solvve.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solvve.domain.ReviewRoleLikeDislike;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePatchDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePutDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikeReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.ReviewRoleLikeDislikeService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({ReviewRoleLikeDislikeController.class, TestObjectsFactoryController.class})
public class ReviewRoleLikeDislikeControllerTest extends BaseControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @MockBean
    private ReviewRoleLikeDislikeService reviewRoleLikeDislikeService;

    @Test
    public void testGetReviewRoleLikeDislike() throws Exception {

        ReviewRoleLikeDislikeReadDTO reviewRoleLikeDislike =
                testObjectsFactoryController.createReviewRoleLikeDislikeReadDTO();

        Mockito.when(reviewRoleLikeDislikeService.getReviewRoleLikeDislike(reviewRoleLikeDislike.getId())).
                thenReturn(reviewRoleLikeDislike);

        String resultJson = mvc.perform(get("/api/v1/review-role-like-dislikes/{id}",
                reviewRoleLikeDislike.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        ReviewRoleLikeDislikeReadDTO actualReviewRoleLikeDislike = objectMapper.readValue(resultJson,
                ReviewRoleLikeDislikeReadDTO.class);
        Assertions.assertThat(actualReviewRoleLikeDislike).
                isEqualToComparingFieldByField(reviewRoleLikeDislike);

        Mockito.verify(reviewRoleLikeDislikeService).getReviewRoleLikeDislike(reviewRoleLikeDislike.getId());
    }

    @Test
    public void testGetReviewRoleLikeDislikeWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(ReviewRoleLikeDislike.class, wrongId);
        Mockito.when(reviewRoleLikeDislikeService.getReviewRoleLikeDislike(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/review-role-like-dislikes/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testPatchReviewRoleLikeDislike() throws Exception {

        ReviewRoleLikeDislikePatchDTO patchDTO = new ReviewRoleLikeDislikePatchDTO();
        patchDTO.setIsLike(false);

        ReviewRoleLikeDislikeReadDTO read = testObjectsFactoryController.createReviewRoleLikeDislikeReadDTO();

        Mockito.when(reviewRoleLikeDislikeService.patchReviewRoleLikeDislike(read.getId(), patchDTO)).
                thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/review-role-like-dislikes/{id}",
                read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewRoleLikeDislikeReadDTO actualReviewRoleLikeDislike = objectMapper.readValue(resultJson,
                ReviewRoleLikeDislikeReadDTO.class);
        Assert.assertEquals(read, actualReviewRoleLikeDislike);
    }

    @Test
    public void testUpdateReviewRoleLikeDislike() throws Exception {

        ReviewRoleLikeDislikePutDTO putDTO = new ReviewRoleLikeDislikePutDTO();
        putDTO.setIsLike(false);

        ReviewRoleLikeDislikeReadDTO read = testObjectsFactoryController.createReviewRoleLikeDislikeReadDTO();

        Mockito.when(reviewRoleLikeDislikeService.updateReviewRoleLikeDislike(read.getId(),
                putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/review-role-like-dislikes/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewRoleLikeDislikeReadDTO actualReviewRoleLikeDislike = objectMapper.readValue(resultJson,
                ReviewRoleLikeDislikeReadDTO.class);
        Assert.assertEquals(read, actualReviewRoleLikeDislike);
    }

    @Test
    public void testDeleteReviewRoleLikeDislike() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/review-role-like-dislikes/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(reviewRoleLikeDislikeService).deleteReviewRoleLikeDislike(id);
    }
}

