package com.solvve.controller;

import com.solvve.domain.News;
import com.solvve.domain.NewsType;
import com.solvve.dto.news.NewsCreateDTO;
import com.solvve.dto.news.NewsPatchDTO;
import com.solvve.dto.news.NewsPutDTO;
import com.solvve.dto.news.NewsReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.service.NewsService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({NewsController.class, TestObjectsFactoryController.class})
public class NewsControllerTest extends BaseControllerTest {

    @MockBean
    private NewsService newsService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetNews() throws Exception {

        NewsReadDTO news = testObjectsFactoryController.createNewsReadDTO();

        Mockito.when(newsService.getNews(news.getId())).thenReturn(news);

        String resultJson = mvc.perform(get("/api/v1/news/{id}", news.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        NewsReadDTO actualNews = objectMapper.readValue(resultJson, NewsReadDTO.class);
        Assertions.assertThat(actualNews).isEqualToComparingFieldByField(news);

        Mockito.verify(newsService).getNews(news.getId());
    }

    @Test
    public void testGetNewsWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(News.class, wrongId);
        Mockito.when(newsService.getNews(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/news/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateNews() throws Exception {

        NewsCreateDTO create = testObjectsFactoryController.createNewsCreateDTO();
        NewsReadDTO read = testObjectsFactoryController.createNewsReadDTO();

        Mockito.when(newsService.createNews(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/news/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsReadDTO actualNews = objectMapper.readValue(resultJson, NewsReadDTO.class);
        Assertions.assertThat(actualNews).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchNews() throws Exception {

        NewsPatchDTO patchDTO = testObjectsFactoryController.createNewsPatchDTO();

        NewsReadDTO read = testObjectsFactoryController.createNewsReadDTO();

        Mockito.when(newsService.patchNews(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/news/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsReadDTO actualNews = objectMapper.readValue(resultJson, NewsReadDTO.class);
        Assert.assertEquals(read, actualNews);
    }

    @Test
    public void testUpdateNews() throws Exception {

        NewsPutDTO putDTO = testObjectsFactoryController.createNewsPutDTO();

        NewsReadDTO read = testObjectsFactoryController.createNewsReadDTO();

        Mockito.when(newsService.updateNews(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/news/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsReadDTO actualNews = objectMapper.readValue(resultJson, NewsReadDTO.class);
        Assert.assertEquals(read, actualNews);
    }

    @Test
    public void testDeleteNews() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/news/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(newsService).deleteNews(id);
    }

    @Test
    public void testCreateNewsValidationFailed() throws Exception {

        NewsCreateDTO create = new NewsCreateDTO();

        NewsReadDTO read = testObjectsFactoryController.createNewsReadDTO();

        Mockito.when(newsService.createNews(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/news/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(newsService, Mockito.never()).createNews(ArgumentMatchers.any());
    }
}

