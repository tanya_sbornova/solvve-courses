package com.solvve.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solvve.domain.ReviewFilmLikeDislike;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePatchDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePutDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.ReviewFilmLikeDislikeService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({ReviewFilmLikeDislikeController.class, TestObjectsFactoryController.class})
public class ReviewFilmLikeDislikeControllerTest extends BaseControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @MockBean
    private ReviewFilmLikeDislikeService reviewFilmLikeDislikeService;

    @Test
    public void testGetReviewFilmLikeDislike() throws Exception {

        ReviewFilmLikeDislikeReadDTO reviewFilmLikeDislike =
                testObjectsFactoryController.createReviewFilmLikeDislikeReadDTO();

        Mockito.when(reviewFilmLikeDislikeService.getReviewFilmLikeDislike(reviewFilmLikeDislike.getId())).
                thenReturn(reviewFilmLikeDislike);

        String resultJson = mvc.perform(get("/api/v1/review-film-like-dislikes/{id}",
                reviewFilmLikeDislike.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        ReviewFilmLikeDislikeReadDTO actualReviewFilmLikeDislike =
                objectMapper.readValue(resultJson, ReviewFilmLikeDislikeReadDTO.class);
        Assertions.assertThat(actualReviewFilmLikeDislike).
                isEqualToComparingFieldByField(reviewFilmLikeDislike);

        Mockito.verify(reviewFilmLikeDislikeService).getReviewFilmLikeDislike(reviewFilmLikeDislike.getId());
    }

    @Test
    public void testGetReviewFilmLikeDislikeWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(ReviewFilmLikeDislike.class, wrongId);
        Mockito.when(reviewFilmLikeDislikeService.getReviewFilmLikeDislike(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/review-film-like-dislikes/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testPatchReviewFilmLikeDislike() throws Exception {

        ReviewFilmLikeDislikePatchDTO patchDTO = new ReviewFilmLikeDislikePatchDTO();
        patchDTO.setIsLike(false);

        ReviewFilmLikeDislikeReadDTO read = testObjectsFactoryController.createReviewFilmLikeDislikeReadDTO();

        Mockito.when(reviewFilmLikeDislikeService.patchReviewFilmLikeDislike(read.getId(),
                patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/review-film-like-dislikes/{id}",
                read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewFilmLikeDislikeReadDTO actualReviewFilmLikeDislike =
                objectMapper.readValue(resultJson, ReviewFilmLikeDislikeReadDTO.class);
        Assert.assertEquals(read, actualReviewFilmLikeDislike);
    }

    @Test
    public void testUpdateReviewFilmLikeDislike() throws Exception {

        ReviewFilmLikeDislikePutDTO putDTO = new ReviewFilmLikeDislikePutDTO();
        putDTO.setIsLike(false);

        ReviewFilmLikeDislikeReadDTO read = testObjectsFactoryController.createReviewFilmLikeDislikeReadDTO();

        Mockito.when(reviewFilmLikeDislikeService.updateReviewFilmLikeDislike(read.getId(),
                putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/review-film-like-dislikes/{id}",
                read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewFilmLikeDislikeReadDTO actualReviewFilmLikeDislike = objectMapper.readValue(resultJson,
                ReviewFilmLikeDislikeReadDTO.class);
        Assert.assertEquals(read, actualReviewFilmLikeDislike);
    }

    @Test
    public void testDeleteReviewFilmLikeDislike() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/review-film-like-dislikes/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(reviewFilmLikeDislikeService).deleteReviewFilmLikeDislike(id);
    }
}

