package com.solvve.controller;

import com.solvve.domain.RatingFilm;
import com.solvve.dto.ratingfilm.RatingFilmPatchDTO;
import com.solvve.dto.ratingfilm.RatingFilmPutDTO;
import com.solvve.dto.ratingfilm.RatingFilmReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.RatingFilmService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({RatingFilmController.class, TestObjectsFactoryController.class})
public class RatingFilmControllerTest extends BaseControllerTest {

    @MockBean
    private RatingFilmService ratingFilmService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetRatingFilm() throws Exception {

        RatingFilmReadDTO ratingFilm = testObjectsFactoryController.createRatingFilmReadDTO();

        Mockito.when(ratingFilmService.getRatingFilm(ratingFilm.getId())).thenReturn(ratingFilm);

        String resultJson = mvc.perform(get("/api/v1/rating-films/{id}", ratingFilm.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        RatingFilmReadDTO actualRatingFilm = objectMapper.readValue(resultJson, RatingFilmReadDTO.class);
        Assertions.assertThat(actualRatingFilm).isEqualToComparingFieldByField(ratingFilm);

        Mockito.verify(ratingFilmService).getRatingFilm(ratingFilm.getId());
    }

    @Test
    public void testGetRatingFilmWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(RatingFilm.class, wrongId);
        Mockito.when(ratingFilmService.getRatingFilm(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/rating-films/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testPatchRatingFilm() throws Exception {

        RatingFilmPatchDTO patchDTO = new RatingFilmPatchDTO();
        patchDTO.setRating(5.5);

        RatingFilmReadDTO read = testObjectsFactoryController.createRatingFilmReadDTO();

        Mockito.when(ratingFilmService.patchRatingFilm(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/rating-films/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RatingFilmReadDTO actualRatingFilm = objectMapper.readValue(resultJson, RatingFilmReadDTO.class);
        Assert.assertEquals(read, actualRatingFilm);
    }

    @Test
    public void testUpdateRatingFilm() throws Exception {

        RatingFilmPutDTO putDTO = new RatingFilmPutDTO();
        putDTO.setRating(5.5);

        RatingFilmReadDTO read = testObjectsFactoryController.createRatingFilmReadDTO();

        Mockito.when(ratingFilmService.updateRatingFilm(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/rating-films/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RatingFilmReadDTO actualRatingFilm = objectMapper.readValue(resultJson, RatingFilmReadDTO.class);
        Assert.assertEquals(read, actualRatingFilm);
    }

    @Test
    public void testDeleteRatingFilm() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/rating-films/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(ratingFilmService).deleteRatingFilm(id);
    }

}