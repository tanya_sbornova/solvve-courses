package com.solvve.controller;

import com.solvve.domain.ProblemReview;
import com.solvve.domain.RegisteredUser;
import com.solvve.dto.problemreview.ProblemReviewCreateDTO;
import com.solvve.dto.problemreview.ProblemReviewPatchDTO;
import com.solvve.dto.problemreview.ProblemReviewPutDTO;
import com.solvve.dto.problemreview.ProblemReviewReadDTO;
import com.solvve.dto.registereduser.RegisteredUserReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.service.RegisteredUserProblemReviewService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({RegisteredUserProblemReviewController.class, TestObjectsFactoryController.class})
public class RegisteredUserProblemReviewControllerTest extends BaseControllerTest {

    @MockBean
    private RegisteredUserProblemReviewService registeredUserProblemReviewService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetRegisteredProblemReviews() throws Exception {

        List<ProblemReviewReadDTO> problemReviews = List.of(testObjectsFactoryController.
                createProblemFilmReviewReadDTO());
        RegisteredUserReadDTO registeredUser = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserProblemReviewService.getRegisteredUserProblemReviews(registeredUser.getId())).
                thenReturn(problemReviews);

        String resultJson = mvc.perform(get("/api/v1/registered-user/{registeredUserId}/problem-reviews",
                registeredUser.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ProblemReviewReadDTO[] arrProblemReviews = objectMapper.readValue(resultJson,
                ProblemReviewReadDTO[].class);
        Assertions.assertThat(problemReviews).containsExactlyInAnyOrder(arrProblemReviews);
    }

    @Test
    public void testGetRegisteredProblemReview() throws Throwable {

        ProblemReviewReadDTO problemReview = testObjectsFactoryController.createProblemFilmReviewReadDTO();

        Mockito.when(registeredUserProblemReviewService.
                getRegisteredUserProblemReview(problemReview.getRegisteredUserId(),
                        problemReview.getId())).thenReturn(problemReview);

        String resultJson = mvc.perform(get(
                "/api/v1/registered-user/{registeredUserId}/problem-reviews/{problemReviewId}",
                problemReview.getRegisteredUserId().toString(),
                problemReview.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ProblemReviewReadDTO actualProblemReview = objectMapper.readValue(resultJson,
                ProblemReviewReadDTO.class);
        Assertions.assertThat(actualProblemReview).isEqualToComparingFieldByField(problemReview);

        Mockito.verify(registeredUserProblemReviewService).getRegisteredUserProblemReview(problemReview.
                getRegisteredUserId(), problemReview.getId());
    }

    @Test
    public void testGetRegisteredProblemReviewWrongId() throws Throwable {

        UUID wrongId = UUID.randomUUID();
        RegisteredUserReadDTO registeredUser = testObjectsFactoryController.createRegisteredUserReadDTO();

        EntityNotFoundException exception = new EntityNotFoundException(ProblemReview.class, wrongId,
                RegisteredUser.class, registeredUser.getId());
        Mockito.when(registeredUserProblemReviewService.getRegisteredUserProblemReview(registeredUser.getId(),
                wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get(
                "/api/v1/registered-user/{registeredUserId}/problem-reviews/{problemReviewId}",
                registeredUser.getId().toString(), wrongId.toString()))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateRegisteredProblemReview() throws Exception {

        ProblemReviewCreateDTO create = testObjectsFactoryController.createProblemFilmReviewCreateDTO();
        ProblemReviewReadDTO read = testObjectsFactoryController.createProblemFilmReviewReadDTO();
        UUID registeredUserId = UUID.randomUUID();

        Mockito.when(registeredUserProblemReviewService.createRegisteredUserProblemReview(registeredUserId,
                create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/registered-user/{registeredUserId}/problem-reviews",
                registeredUserId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ProblemReviewReadDTO actualProblemReview = objectMapper.readValue(resultJson,
                ProblemReviewReadDTO.class);
        Assertions.assertThat(actualProblemReview).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchRegisteredProblemReview() throws Throwable {

        ProblemReviewPatchDTO patchDTO = testObjectsFactoryController.createProblemReviewPatchDTO();
        ProblemReviewReadDTO read = testObjectsFactoryController.createProblemFilmReviewReadDTO();
        RegisteredUserReadDTO registeredUserReadDTO = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserProblemReviewService.patchRegisteredUserProblemReview(registeredUserReadDTO.getId(),
                read.getId(),
                patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch(
                "/api/v1/registered-user/{registeredUserId}/problem-reviews/{problemReviewId}",
                registeredUserReadDTO.getId().toString(), read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ProblemReviewReadDTO actualProblemReview = objectMapper.readValue(resultJson,
                ProblemReviewReadDTO.class);
        Assert.assertEquals(read, actualProblemReview);
    }

    @Test
    public void testUpdateRegisteredProblemReview() throws Throwable{

        ProblemReviewPutDTO putDTO = testObjectsFactoryController.createProblemReviewPutDTO();
        ProblemReviewReadDTO read = testObjectsFactoryController.createProblemFilmReviewReadDTO();
        RegisteredUserReadDTO registeredUserReadDTO = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserProblemReviewService.
                updateRegisteredUserProblemReview(registeredUserReadDTO.getId(),
                read.getId(),
                putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put(
                "/api/v1/registered-user/{registeredUserId}/problem-reviews/{problemReviewId}",
                registeredUserReadDTO.getId().toString(), read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ProblemReviewReadDTO actualAdmin = objectMapper.readValue(resultJson, ProblemReviewReadDTO.class);
        Assert.assertEquals(read, actualAdmin);
    }

    @Test
    public void testDeleteRegisteredProblemReview() throws Exception {

        ProblemReviewReadDTO read = testObjectsFactoryController.createProblemFilmReviewReadDTO();
        RegisteredUserReadDTO registeredUserReadDTO = testObjectsFactoryController.createRegisteredUserReadDTO();

        mvc.perform(delete("/api/v1/registered-user/{registeredUserId}/problem-reviews/{problemReviewId}",
                registeredUserReadDTO.getId().toString(), read.getId().toString()))
                .andExpect(status().isOk());

        Mockito.verify(registeredUserProblemReviewService).
                deleteRegisteredUserProblemReview(registeredUserReadDTO.getId(), read.getId());
    }

    @Test
    public void testCreateProblemReviewValidationFailed() throws Exception {

        ProblemReviewCreateDTO create = new ProblemReviewCreateDTO();
        ProblemReviewReadDTO read = testObjectsFactoryController.createProblemFilmReviewReadDTO();
        UUID registeredUserId = UUID.randomUUID();

        Mockito.when(registeredUserProblemReviewService
                .createRegisteredUserProblemReview(registeredUserId, create)).thenReturn(read);

        String resultJson = mvc.perform(post(
                "/api/v1/registered-user/{registeredUserId}/problem-reviews",
                registeredUserId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(registeredUserProblemReviewService, Mockito.never())
                .createRegisteredUserProblemReview(ArgumentMatchers.any(), ArgumentMatchers.any());
    }
}
