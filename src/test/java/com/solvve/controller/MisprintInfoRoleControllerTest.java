package com.solvve.controller;

import com.solvve.domain.MisprintInfoRole;
import com.solvve.dto.misprintinforole.MisprintInfoRoleReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.MisprintInfoRoleService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({MisprintInfoRoleController.class, TestObjectsFactoryController.class})
public class MisprintInfoRoleControllerTest extends BaseControllerTest {

    @MockBean
    private MisprintInfoRoleService misprintInfoService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetMisprintInfoRole() throws Exception {

        MisprintInfoRoleReadDTO misprintInfo = testObjectsFactoryController.createMisprintInfoRoleReadDTO();

        Mockito.when(misprintInfoService.getMisprintInfoRole(misprintInfo.getId())).thenReturn(misprintInfo);

        String resultJson = mvc.perform(get("/api/v1/misprint-info-roles/{id}", misprintInfo.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        MisprintInfoRoleReadDTO actualMisprintInfoRole = objectMapper.readValue(resultJson,
                MisprintInfoRoleReadDTO.class);
        Assertions.assertThat(actualMisprintInfoRole).isEqualToComparingFieldByField(misprintInfo);

        Mockito.verify(misprintInfoService).getMisprintInfoRole(misprintInfo.getId());
    }

    @Test
    public void testGetMisprintInfoRoleWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(MisprintInfoRole.class, wrongId);
        Mockito.when(misprintInfoService.getMisprintInfoRole(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/misprint-info-roles/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testDeleteMisprintInfoRole() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/misprint-info-roles/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(misprintInfoService).deleteMisprintInfoRole(id);
    }
}


