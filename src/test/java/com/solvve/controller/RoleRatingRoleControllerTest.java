package com.solvve.controller;

import com.solvve.domain.Role;
import com.solvve.domain.RatingRole;
import com.solvve.dto.role.RoleReadDTO;
import com.solvve.dto.ratingrole.RatingRoleCreateDTO;
import com.solvve.dto.ratingrole.RatingRolePatchDTO;
import com.solvve.dto.ratingrole.RatingRolePutDTO;
import com.solvve.dto.ratingrole.RatingRoleReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.service.RoleRatingRoleService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({RoleRatingRoleController.class, TestObjectsFactoryController.class})
public class RoleRatingRoleControllerTest extends BaseControllerTest {

    @MockBean
    private RoleRatingRoleService roleRatingRoleService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetRoleRatingRoles() throws Exception {

        List<RatingRoleReadDTO> ratingRoles = List.of(testObjectsFactoryController.createRatingRoleReadDTO(),
                                                      testObjectsFactoryController.createRatingRoleReadDTO(),
                                                      testObjectsFactoryController.createRatingRoleReadDTO());
        RoleReadDTO role = testObjectsFactoryController.createRoleReadDTO();

        Mockito.when(roleRatingRoleService.getRoleRatingRoles(role.getId())).thenReturn(ratingRoles);

        String resultJson = mvc.perform(get("/api/v1/role/{roleId}/rating-roles", role.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RatingRoleReadDTO[] arrRatingRoles = objectMapper.readValue(resultJson, RatingRoleReadDTO[].class);
        Assertions.assertThat(ratingRoles).containsExactlyInAnyOrder(arrRatingRoles);
    }

    @Test
    public void testGetRoleRatingRole() throws Throwable {

        RatingRoleReadDTO ratingRole = testObjectsFactoryController.createRatingRoleReadDTO();

        Mockito.when(roleRatingRoleService.getRoleRatingRole(ratingRole.getRoleId(),
                ratingRole.getId())).thenReturn(ratingRole);

        String resultJson = mvc.perform(get("/api/v1/role/{roleId}/rating-roles/{ratingRoleId}",
                ratingRole.getRoleId().toString(), ratingRole.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RatingRoleReadDTO actualRatingRole = objectMapper.readValue(resultJson, RatingRoleReadDTO.class);
        Assertions.assertThat(actualRatingRole).isEqualToComparingFieldByField(ratingRole);

        Mockito.verify(roleRatingRoleService).getRoleRatingRole(ratingRole.getRoleId(), ratingRole.getId());
    }

    @Test
    public void testGetRoleRatingRoleWrongId() throws Throwable {

        UUID wrongId = UUID.randomUUID();
        RoleReadDTO role = testObjectsFactoryController.createRoleReadDTO();

        EntityNotFoundException exception = new EntityNotFoundException(RatingRole.class, wrongId,
                Role.class, role.getId());
        Mockito.when(roleRatingRoleService.getRoleRatingRole(role.getId(), wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/role/{roleId}/rating-roles/{ratingRoleId}",
                role.getId().toString(), wrongId.toString()))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateRoleRatingRole() throws Exception {

        RatingRoleCreateDTO create = testObjectsFactoryController.createRatingRoleCreateDTO();
        RatingRoleReadDTO read = testObjectsFactoryController.createRatingRoleReadDTO();
        UUID roleId = UUID.randomUUID();

        Mockito.when(roleRatingRoleService.createRoleRatingRole(roleId, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/role/{roleId}/rating-roles",
                roleId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RatingRoleReadDTO actualRatingRole = objectMapper.readValue(resultJson, RatingRoleReadDTO.class);
        Assertions.assertThat(actualRatingRole).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchRoleRatingRole() throws Throwable {

        RatingRolePatchDTO patchDTO = testObjectsFactoryController.createRatingRolePatchDTO();
        RatingRoleReadDTO read = testObjectsFactoryController.createRatingRoleReadDTO();
        RoleReadDTO roleReadDTO = testObjectsFactoryController.createRoleReadDTO();

        Mockito.when(roleRatingRoleService.patchRoleRatingRole(roleReadDTO.getId(), read.getId(),
                patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/role/{roleId}/rating-roles/{ratingRoleId}",
                roleReadDTO.getId().toString(), read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RatingRoleReadDTO actualRatingRole = objectMapper.readValue(resultJson, RatingRoleReadDTO.class);
        Assert.assertEquals(read, actualRatingRole);
    }

    @Test
    public void testUpdateRoleRatingRole() throws Throwable{

        RatingRolePutDTO putDTO = testObjectsFactoryController.createRatingRolePutDTO();
        RatingRoleReadDTO read = testObjectsFactoryController.createRatingRoleReadDTO();
        RoleReadDTO roleReadDTO = testObjectsFactoryController.createRoleReadDTO();

        Mockito.when(roleRatingRoleService.updateRoleRatingRole(roleReadDTO.getId(), read.getId(),
                putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/role/{roleId}/rating-roles/{ratingRoleId}",
                roleReadDTO.getId().toString(), read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RatingRoleReadDTO actualAdmin = objectMapper.readValue(resultJson, RatingRoleReadDTO.class);
        Assert.assertEquals(read, actualAdmin);
    }

    @Test
    public void testDeleteRoleReviewRole() throws Exception {

        RatingRoleReadDTO read = testObjectsFactoryController.createRatingRoleReadDTO();
        RoleReadDTO roleReadDTO = testObjectsFactoryController.createRoleReadDTO();

        mvc.perform(delete("/api/v1/role/{roleId}/rating-roles/{ratingRoleId}",
                roleReadDTO.getId().toString(), read.getId().toString()))
                .andExpect(status().isOk());

        Mockito.verify(roleRatingRoleService).deleteRoleRatingRole(roleReadDTO.getId(), read.getId());
    }

    @Test
    public void testCreateRatingRoleValidationFailed() throws Exception {

        RatingRoleCreateDTO create = new RatingRoleCreateDTO();
        RatingRoleReadDTO read = testObjectsFactoryController.createRatingRoleReadDTO();
        UUID roleId = UUID.randomUUID();

        Mockito.when(roleRatingRoleService.createRoleRatingRole(roleId, create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/role/{roleId}/rating-roles",
                roleId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(roleRatingRoleService, Mockito.never()).createRoleRatingRole(ArgumentMatchers.any(),
                ArgumentMatchers.any());
    }
}

