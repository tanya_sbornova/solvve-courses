package com.solvve.controller;

import com.solvve.domain.ProblemReview;
import com.solvve.domain.ProblemType;
import com.solvve.dto.problemreview.ProblemReviewCreateDTO;
import com.solvve.dto.problemreview.ProblemReviewPatchDTO;
import com.solvve.dto.problemreview.ProblemReviewPutDTO;
import com.solvve.dto.problemreview.ProblemReviewReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.ProblemReviewService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({ProblemReviewController.class, TestObjectsFactoryController.class})
public class ProblemReviewControllerTest extends BaseControllerTest {

    @MockBean
    private ProblemReviewService problemReviewService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetProblemReview() throws Exception {

        ProblemReviewReadDTO problemReview = testObjectsFactoryController.createProblemReviewReadDTO();

        Mockito.when(problemReviewService.getProblemReview(problemReview.getId())).thenReturn(problemReview);

        String resultJson = mvc.perform(get("/api/v1/problem-reviews/{id}", problemReview.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        ProblemReviewReadDTO actualProblemReview =
                objectMapper.readValue(resultJson, ProblemReviewReadDTO.class);
        Assertions.assertThat(actualProblemReview).isEqualToComparingFieldByField(problemReview);

        Mockito.verify(problemReviewService).getProblemReview(problemReview.getId());
    }

    @Test
    public void testGetProblemReviewWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(ProblemReview.class, wrongId);
        Mockito.when(problemReviewService.getProblemReview(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/problem-reviews/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetProblemReviews() throws Exception {

        List<ProblemReviewReadDTO> problemReviews = List.of(testObjectsFactoryController.createProblemReviewReadDTO(),
                testObjectsFactoryController.createProblemReviewReadDTO());

        Mockito.when(problemReviewService.getProblemReviews()).thenReturn(problemReviews);

        String resultJson = mvc.perform(get("/api/v1/problem-reviews"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ProblemReviewReadDTO[] arrProblemReviews = objectMapper.readValue(resultJson, ProblemReviewReadDTO[].class);
        Assertions.assertThat(problemReviews).containsExactlyInAnyOrder(arrProblemReviews);
    }

    @Test
    public void testCreateProblemReview() throws Exception {

        ProblemReviewCreateDTO create = new ProblemReviewCreateDTO();
        create.setProblemType(ProblemType.FILTHY_LANGUAGE);

        ProblemReviewReadDTO read = testObjectsFactoryController.createProblemReviewReadDTO();

        Mockito.when(problemReviewService.createProblemReview(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/problem-reviews/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ProblemReviewReadDTO actualProblemReview =
                objectMapper.readValue(resultJson, ProblemReviewReadDTO.class);
        Assertions.assertThat(actualProblemReview).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchProblemReview() throws Exception {

        ProblemReviewPatchDTO patchDTO = new ProblemReviewPatchDTO();
        patchDTO.setProblemType(ProblemType.FILTHY_LANGUAGE);

        ProblemReviewReadDTO read = testObjectsFactoryController.createProblemReviewReadDTO();

        Mockito.when(problemReviewService.patchProblemReview(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/problem-reviews/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ProblemReviewReadDTO actualProblemReview =
                objectMapper.readValue(resultJson, ProblemReviewReadDTO.class);
        Assert.assertEquals(read, actualProblemReview);
    }

    @Test
    public void testUpdateProblemReview() throws Exception {

        ProblemReviewPutDTO putDTO = new ProblemReviewPutDTO();
        putDTO.setProblemType(ProblemType.FILTHY_LANGUAGE);

        ProblemReviewReadDTO read = testObjectsFactoryController.createProblemReviewReadDTO();

        Mockito.when(problemReviewService.updateProblemReview(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/problem-reviews/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ProblemReviewReadDTO actualProblemReview =
                objectMapper.readValue(resultJson, ProblemReviewReadDTO.class);
        Assert.assertEquals(read, actualProblemReview);
    }

    @Test
    public void testDeleteProblemReview() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/problem-reviews/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(problemReviewService).deleteProblemReview(id);
    }
}
