package com.solvve.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.solvve.domain.PageResult;
import com.solvve.domain.ReviewRole;
import com.solvve.domain.ReviewStatus;
import com.solvve.dto.reviewrole.ReviewRoleFilter;
import com.solvve.dto.reviewrole.ReviewRolePatchDTO;
import com.solvve.dto.reviewrole.ReviewRolePutDTO;
import com.solvve.dto.reviewrole.ReviewRoleReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.ReviewRoleService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({ReviewRoleController.class, TestObjectsFactoryController.class})
public class ReviewRoleControllerTest extends BaseControllerTest {

    @MockBean
    private ReviewRoleService reviewRoleService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetReviewRole() throws Exception {

        ReviewRoleReadDTO reviewRole = testObjectsFactoryController.createReviewRoleReadDTO();

        Mockito.when(reviewRoleService.getReviewRole(reviewRole.getId())).thenReturn(reviewRole);

        String resultJson = mvc.perform(get("/api/v1/review-roles/{id}", reviewRole.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        ReviewRoleReadDTO actualReviewRole = objectMapper.readValue(resultJson, ReviewRoleReadDTO.class);
        Assertions.assertThat(actualReviewRole).isEqualToComparingFieldByField(reviewRole);

        Mockito.verify(reviewRoleService).getReviewRole(reviewRole.getId());
    }

    @Test
    public void testGetReviewRoleWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(ReviewRole.class, wrongId);
        Mockito.when(reviewRoleService.getReviewRole(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/review-roles/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testPatchReviewRole() throws Exception {

        ReviewRolePatchDTO patchDTO = new ReviewRolePatchDTO();
        patchDTO.setText("The best");
        patchDTO.setStatus(ReviewStatus.MODERATED);

        ReviewRoleReadDTO read = testObjectsFactoryController.createReviewRoleReadDTO();

        Mockito.when(reviewRoleService.patchReviewRole(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/review-roles/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewRoleReadDTO actualReviewRole = objectMapper.readValue(resultJson, ReviewRoleReadDTO.class);
        Assert.assertEquals(read, actualReviewRole);
    }

    @Test
    public void testUpdateReviewRole() throws Exception {

        ReviewRolePutDTO putDTO = new ReviewRolePutDTO();
        putDTO.setText("The best");
        putDTO.setStatus(ReviewStatus.MODERATED);

        ReviewRoleReadDTO read = testObjectsFactoryController.createReviewRoleReadDTO();

        Mockito.when(reviewRoleService.updateReviewRole(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/review-roles/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewRoleReadDTO actualReviewRole = objectMapper.readValue(resultJson, ReviewRoleReadDTO.class);
        Assert.assertEquals(read, actualReviewRole);
    }

    @Test
    public void testDeleteReviewRole() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/review-roles/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(reviewRoleService).deleteReviewRole(id);
    }

    @Test
    public void testGetReviewRolesWithPagingAndSorting() throws Exception {
        ReviewRoleFilter reviewRoleFilter = new ReviewRoleFilter();
        reviewRoleFilter.setStatus(ReviewStatus.FIXED);
        reviewRoleFilter.setIsSpoiler(false);
        reviewRoleFilter.setRegisteredUserId(UUID.randomUUID());
        reviewRoleFilter.setRoleId(UUID.randomUUID());

        ReviewRoleReadDTO read = testObjectsFactoryController.createReviewRoleReadDTO();

        int page = 1;
        int size = 25;

        PageResult<ReviewRoleReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(new Long(100));
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "email"));
        Mockito.when(reviewRoleService.getReviewRoles(reviewRoleFilter, pageRequest))
                .thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/review-roles")
                .param("status", reviewRoleFilter.getStatus().toString())
                .param("isSpoiler", reviewRoleFilter.getIsSpoiler().toString())
                .param("registeredUserId", reviewRoleFilter.getRegisteredUserId().toString())
                .param("roleId", reviewRoleFilter.getRoleId().toString())
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "email,asc")
        )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<ReviewRoleReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {});
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetReviewRolesWithBigPage() throws Exception {
        ReviewRoleFilter reviewRoleFilter = new ReviewRoleFilter();
        reviewRoleFilter.setStatus(ReviewStatus.FIXED);
        reviewRoleFilter.setIsSpoiler(false);
        reviewRoleFilter.setRegisteredUserId(UUID.randomUUID());
        reviewRoleFilter.setRoleId(UUID.randomUUID());

        ReviewRoleReadDTO read = testObjectsFactoryController.createReviewRoleReadDTO();

        int page = 0;
        int size = 999999;

        PageResult<ReviewRoleReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(new Long(100));
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(page, maxPageSize, Sort.by(Sort.Direction.ASC, "email"));
        Mockito.when(reviewRoleService.getReviewRoles(reviewRoleFilter, pageRequest))
                .thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/review-roles")
                .param("status", reviewRoleFilter.getStatus().toString())
                .param("isSpoiler", reviewRoleFilter.getIsSpoiler().toString())
                .param("registeredUserId", reviewRoleFilter.getRegisteredUserId().toString())
                .param("roleId", reviewRoleFilter.getRoleId().toString())
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "email,asc")
        )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<ReviewRoleReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {});
        Assert.assertEquals(resultPage, actualPage);
    }
}
