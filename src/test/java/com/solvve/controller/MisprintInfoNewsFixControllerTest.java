package com.solvve.controller;

import com.solvve.domain.MisprintInfoNews;
import com.solvve.domain.MisprintStatus;
import com.solvve.dto.misprintinfofix.MisprintInfoFixDTO;
import com.solvve.dto.misprintinfofix.MisprintInfoFixReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.EntityUnprocessableException;
import com.solvve.service.MisprintInfoCommonFixService;
import com.solvve.service.MisprintInfoFixService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({MisprintInfoFixController.class, TestObjectsFactoryController.class})
public class MisprintInfoNewsFixControllerTest extends BaseControllerTest {

    @MockBean
    private MisprintInfoFixService misprintInfoService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testFixMisprintInfoNewsWrongId() throws Exception {

        MisprintInfoFixDTO patch = new MisprintInfoFixDTO();
        UUID contentManagerId = UUID.randomUUID();
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(MisprintInfoFixService.class, wrongId);
        Mockito.when(misprintInfoService.fixContent(contentManagerId, wrongId, patch)).thenThrow(exception);

        String resultJson = mvc.perform(post(
                "/api/v1/content-manager/{contentManagerId}/misprints-info/{misprintInfoNewsId}/fix",
                contentManagerId.toString(), wrongId.toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testFixMisprintInfoNewsWrongStatus() throws Exception {

        MisprintInfoFixDTO patch = new MisprintInfoFixDTO();
        UUID misprintInfonewsId = UUID.randomUUID();
        UUID contentManagerId = UUID.randomUUID();

        EntityUnprocessableException exception = new EntityUnprocessableException(MisprintInfoNews.class,
                misprintInfonewsId, MisprintStatus.CANCELLED);
        Mockito.when(misprintInfoService.fixContent(contentManagerId, misprintInfonewsId, patch)).
                thenThrow(exception);

        String resultJson = mvc.perform(post(
                "/api/v1/content-manager/{contentManagerId}/misprints-info/{misprintInfoNewsId}/fix",
                contentManagerId.toString(), misprintInfonewsId.toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testFixMisprintInfoNewsUserVersion() throws Exception {

        UUID newsId = UUID.randomUUID();
        UUID registeredUserId = UUID.randomUUID();
        UUID contentManagerId = UUID.randomUUID();
        MisprintInfoFixDTO patch = new MisprintInfoFixDTO();

        MisprintInfoFixReadDTO misprintInfoNews = testObjectsFactoryController.
                createMPInfoNewsReadDTOWithParam(newsId, 10,16, "reveals", registeredUserId, contentManagerId);

        List<MisprintInfoFixReadDTO> expectedMisprintsRead = new ArrayList<>();
        expectedMisprintsRead.add(misprintInfoNews);
        expectedMisprintsRead.add(testObjectsFactoryController.createMPInfoNewsReadDTOWithParam(newsId,
                13,13, "e", registeredUserId, contentManagerId));
        expectedMisprintsRead.add(testObjectsFactoryController.createMPInfoNewsReadDTOWithParam(newsId,
                13,15,"veal", registeredUserId, contentManagerId));

        Mockito.when(misprintInfoService.fixContent(contentManagerId, misprintInfoNews.getId(), patch)).
                thenReturn(expectedMisprintsRead);

        String resultJson = mvc.perform(post(
                "/api/v1/content-manager/{contentManagerId}/misprints-info/{misprintInfoNewsId}/fix",
                contentManagerId.toString(), misprintInfoNews.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MisprintInfoFixReadDTO[] arrMisprintsRead = objectMapper.readValue(resultJson,
                MisprintInfoFixReadDTO[].class);
        Assertions.assertThat(expectedMisprintsRead).containsExactlyInAnyOrder(arrMisprintsRead);
    }

    @Test
    public void testFixMisprintInfoNewsCMVersion() throws Exception {

        UUID newsId = UUID.randomUUID();
        UUID registeredUserId = UUID.randomUUID();
        UUID contentManagerId = UUID.randomUUID();
        MisprintInfoFixDTO patch = new MisprintInfoFixDTO();
        patch.setText("NEO");

        MisprintInfoFixReadDTO misprintInfoNews = testObjectsFactoryController.
                createMPInfoNewsReadDTOWithParam(newsId, 10,16, "reveals", registeredUserId, contentManagerId);

        List<MisprintInfoFixReadDTO> expectedMisprintsRead = new ArrayList<>();
        expectedMisprintsRead.add(misprintInfoNews);
        expectedMisprintsRead.add(testObjectsFactoryController.createMPInfoNewsReadDTOWithParam(newsId,
                13,13, "e", registeredUserId, contentManagerId));
        expectedMisprintsRead.add(testObjectsFactoryController.createMPInfoNewsReadDTOWithParam(newsId,
                13,15,"veal", registeredUserId, contentManagerId));

        Mockito.when(misprintInfoService.fixContent(contentManagerId, misprintInfoNews.getId(), patch)).
                thenReturn(expectedMisprintsRead);

        String resultJson = mvc.perform(post(
                "/api/v1/content-manager/{contentManagerId}/misprints-info/{misprintInfoNewsId}/fix",
                contentManagerId.toString(), misprintInfoNews.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MisprintInfoFixReadDTO[] arrMisprintsRead = objectMapper.readValue(resultJson,
                MisprintInfoFixReadDTO[].class);
        Assertions.assertThat(expectedMisprintsRead).containsExactlyInAnyOrder(arrMisprintsRead);
    }
}
