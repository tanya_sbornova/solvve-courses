package com.solvve.controller;

import com.solvve.domain.RatingRole;
import com.solvve.dto.ratingrole.RatingRolePatchDTO;
import com.solvve.dto.ratingrole.RatingRolePutDTO;
import com.solvve.dto.ratingrole.RatingRoleReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.RatingRoleService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({RatingRoleController.class, TestObjectsFactoryController.class})
public class RatingRoleControllerTest extends BaseControllerTest {

    @MockBean
    private RatingRoleService ratingRoleService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetRatingRole() throws Exception {

        RatingRoleReadDTO ratingRole = testObjectsFactoryController.createRatingRoleReadDTO();

        Mockito.when(ratingRoleService.getRatingRole(ratingRole.getId())).thenReturn(ratingRole);

        String resultJson = mvc.perform(get("/api/v1/rating-roles/{id}", ratingRole.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        RatingRoleReadDTO actualRatingRole = objectMapper.readValue(resultJson, RatingRoleReadDTO.class);
        Assertions.assertThat(actualRatingRole).isEqualToComparingFieldByField(ratingRole);

        Mockito.verify(ratingRoleService).getRatingRole(ratingRole.getId());
    }

    @Test
    public void testGetRatingRoleWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(RatingRole.class, wrongId);
        Mockito.when(ratingRoleService.getRatingRole(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/rating-roles/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testPatchRatingRole() throws Exception {

        RatingRolePatchDTO patchDTO = new RatingRolePatchDTO();
        patchDTO.setRating(5.5);

        RatingRoleReadDTO read = testObjectsFactoryController.createRatingRoleReadDTO();

        Mockito.when(ratingRoleService.patchRatingRole(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/rating-roles/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RatingRoleReadDTO actualRatingRole = objectMapper.readValue(resultJson, RatingRoleReadDTO.class);
        Assert.assertEquals(read, actualRatingRole);
    }

    @Test
    public void testUpdateRatingRole() throws Exception {

        RatingRolePutDTO putDTO = new RatingRolePutDTO();
        putDTO.setRating(5.5);

        RatingRoleReadDTO read = testObjectsFactoryController.createRatingRoleReadDTO();

        Mockito.when(ratingRoleService.updateRatingRole(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/rating-roles/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RatingRoleReadDTO actualRatingRole = objectMapper.readValue(resultJson, RatingRoleReadDTO.class);
        Assert.assertEquals(read, actualRatingRole);
    }

    @Test
    public void testDeleteRatingRole() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/rating-roles/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(ratingRoleService).deleteRatingRole(id);
    }
}
