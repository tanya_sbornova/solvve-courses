package com.solvve.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.solvve.domain.Genre;
import com.solvve.domain.FilmGenre;
import com.solvve.dto.genre.GenreCreateDTO;
import com.solvve.dto.genre.GenrePatchDTO;
import com.solvve.dto.genre.GenrePutDTO;
import com.solvve.dto.genre.GenreReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.service.GenreService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({GenreController.class, TestObjectsFactoryController.class})
public class GenreControllerTest extends BaseControllerTest {

    @MockBean
    private GenreService genreService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetGenre() throws Exception {

        GenreReadDTO genre = testObjectsFactoryController.createGenreReadDTO();

        Mockito.when(genreService.getGenre(genre.getId())).thenReturn(genre);

        String resultJson = mvc.perform(get("/api/v1/genres/{id}", genre.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        GenreReadDTO actualGenre = objectMapper.readValue(resultJson, GenreReadDTO.class);
        Assertions.assertThat(actualGenre).isEqualToComparingFieldByField(genre);

        Mockito.verify(genreService).getGenre(genre.getId());
    }

    @Test
    public void testGetGenres() throws Exception {

        List<GenreReadDTO> genres = List.of(testObjectsFactoryController.createGenreReadDTO(),
                testObjectsFactoryController.createGenreReadDTO());

        Mockito.when(genreService.getGenres()).thenReturn(genres);

        String resultJson = mvc.perform(get("/api/v1/genres"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        GenreReadDTO[] arrGenres = objectMapper.readValue(resultJson, GenreReadDTO[].class);
        Assertions.assertThat(genres).containsExactlyInAnyOrder(arrGenres);
    }

    @Test
    public void testGetFilmGenres() throws Exception {

        List<GenreReadDTO> genres = List.of(testObjectsFactoryController.createGenreReadDTO(),
                testObjectsFactoryController.createGenreReadDTO(),
                testObjectsFactoryController.createGenreReadDTO());

        UUID filmId = UUID.randomUUID();

        Mockito.when(genreService.getFilmGenres(filmId)).thenReturn(genres);

        String resultJson = mvc.perform(get("/api/v1/film/{filmId}/genres", filmId.toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        GenreReadDTO[] arrGenres = objectMapper.readValue(resultJson, GenreReadDTO[].class);
        Assertions.assertThat(genres).containsExactlyInAnyOrder(arrGenres);
    }

    @Test
    public void testGetGenreWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Genre.class, wrongId);
        Mockito.when(genreService.getGenre(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/genres/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateGenre() throws Exception {

        GenreCreateDTO create = new GenreCreateDTO();
        create.setName(FilmGenre.SCI_FI);

        GenreReadDTO read = testObjectsFactoryController.createGenreReadDTO();

        Mockito.when(genreService.createGenre(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/genres/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        GenreReadDTO actualGenre = objectMapper.readValue(resultJson, GenreReadDTO.class);
        Assertions.assertThat(actualGenre).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testAddGenreToFilm() throws Exception {

        UUID filmId = UUID.randomUUID();
        UUID genreId = UUID.randomUUID();

        GenreReadDTO read = new GenreReadDTO();
        read.setId(genreId);
        read.setName(FilmGenre.DRAMA);
        List<GenreReadDTO> expectedGenres = List.of(read);

        Mockito.when(genreService.addGenreToFilm(filmId, genreId)).thenReturn(expectedGenres);

        String resultJson = mvc.perform(post("/api/v1/film/{filmId}/genres/{id}", filmId, genreId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<GenreReadDTO> actualGenres = objectMapper.readValue(resultJson,
                new TypeReference<List<GenreReadDTO>>() {
                });
        Assert.assertEquals(expectedGenres, actualGenres);
    }

    @Test
    public void testPatchGenre() throws Exception {

        GenrePatchDTO patchDTO = new GenrePatchDTO();
        patchDTO.setName(FilmGenre.SCI_FI);

        GenreReadDTO read = testObjectsFactoryController.createGenreReadDTO();

        Mockito.when(genreService.patchGenre(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/genres/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        GenreReadDTO actualGenre = objectMapper.readValue(resultJson, GenreReadDTO.class);
        Assert.assertEquals(read, actualGenre);
    }

    @Test
    public void testUpdateGenre() throws Exception {

        GenrePutDTO putDTO = new GenrePutDTO();
        putDTO.setName(FilmGenre.SCI_FI);

        GenreReadDTO read = testObjectsFactoryController.createGenreReadDTO();

        Mockito.when(genreService.updateGenre(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/genres/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        GenreReadDTO actualGenre = objectMapper.readValue(resultJson, GenreReadDTO.class);
        Assert.assertEquals(read, actualGenre);
    }

    @Test
    public void testDeleteGenre() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/genres/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(genreService).deleteGenre(id);
    }

    @Test
    public void testRemoveGenreFromFilm() throws Exception {

        UUID filmId = UUID.randomUUID();
        UUID genreId = UUID.randomUUID();

        List<GenreReadDTO> expectedGenres = List.of();

        Mockito.when(genreService.removeGenreFromFilm(filmId, genreId)).thenReturn(expectedGenres);

        String resultJson = mvc.perform(delete("/api/v1/film/{filmId}/genres/{id}", filmId, genreId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<GenreReadDTO> actualGenres = objectMapper.readValue(resultJson,
                new TypeReference<List<GenreReadDTO>>() {
                });
        Assert.assertEquals(expectedGenres, actualGenres);
    }

    @Test
    public void testCreateGenreValidationFailed() throws Exception {

        GenreCreateDTO create = new GenreCreateDTO();

        GenreReadDTO read = testObjectsFactoryController.createGenreReadDTO();

        Mockito.when(genreService.createGenre(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/genres/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(genreService, Mockito.never()).createGenre(ArgumentMatchers.any());
    }
}