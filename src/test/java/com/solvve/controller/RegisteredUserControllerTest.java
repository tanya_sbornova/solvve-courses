package com.solvve.controller;

import com.solvve.domain.RegisteredUser;
import com.solvve.dto.registereduser.RegisteredUserCreateDTO;
import com.solvve.dto.registereduser.RegisteredUserPatchDTO;
import com.solvve.dto.registereduser.RegisteredUserPutDTO;
import com.solvve.dto.registereduser.RegisteredUserReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.service.RegisteredUserService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({RegisteredUserController.class, TestObjectsFactoryController.class})
public class RegisteredUserControllerTest extends BaseControllerTest {

    @MockBean
    private RegisteredUserService registeredUserService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetRegisteredUser() throws Exception {

        RegisteredUserReadDTO registeredUser = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserService.getRegisteredUser(registeredUser.getId())).
                thenReturn(registeredUser);

        String resultJson = mvc.perform(get("/api/v1/registered-users/{id}", registeredUser.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        RegisteredUserReadDTO actualActor = objectMapper.readValue(resultJson, RegisteredUserReadDTO.class);
        Assertions.assertThat(actualActor).isEqualToComparingFieldByField(registeredUser);

        Mockito.verify(registeredUserService).getRegisteredUser(registeredUser.getId());
    }

    @Test
    public void testGetRegisteredUserWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(RegisteredUser.class, wrongId);
        Mockito.when(registeredUserService.getRegisteredUser(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/registered-users/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateRegisteredUser() throws Exception {

        RegisteredUserCreateDTO create = testObjectsFactoryController.createRegisteredUserCreateDTO();

        RegisteredUserReadDTO read = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserService.createRegisteredUser(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/registered-users/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegisteredUserReadDTO actualRegisteredUser =
                objectMapper.readValue(resultJson, RegisteredUserReadDTO.class);
        Assertions.assertThat(actualRegisteredUser).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchRegisteredUser() throws Exception {

        RegisteredUserPatchDTO patchDTO = testObjectsFactoryController.generateObject(RegisteredUserPatchDTO.class);

        RegisteredUserReadDTO read = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserService.patchRegisteredUser(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/registered-users/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegisteredUserReadDTO actualRegisteredUser =
                objectMapper.readValue(resultJson, RegisteredUserReadDTO.class);
        Assert.assertEquals(read, actualRegisteredUser);
    }

    @Test
    public void testUpdateRegisteredUser() throws Exception {

        RegisteredUserPutDTO putDTO = testObjectsFactoryController.generateObject(RegisteredUserPutDTO.class);

        RegisteredUserReadDTO read = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserService.updateRegisteredUser(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/registered-users/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegisteredUserReadDTO actualRegisteredUser =
                objectMapper.readValue(resultJson, RegisteredUserReadDTO.class);
        Assert.assertEquals(read, actualRegisteredUser);
    }

    @Test
    public void testDeleteRegisteredUser() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/registered-users/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(registeredUserService).deleteRegisteredUser(id);
    }

    @Test
    public void testCreateRegisteredUserValidationFailed() throws Exception {

        RegisteredUserCreateDTO create = new RegisteredUserCreateDTO();

        RegisteredUserReadDTO read = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserService.createRegisteredUser(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/registered-users/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(registeredUserService, Mockito.never()).createRegisteredUser(ArgumentMatchers.any());
    }
}
