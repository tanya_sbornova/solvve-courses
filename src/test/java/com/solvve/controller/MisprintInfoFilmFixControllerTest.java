package com.solvve.controller;

import com.solvve.domain.MisprintInfoFilm;
import com.solvve.domain.MisprintStatus;
import com.solvve.dto.misprintinfofix.MisprintInfoFixDTO;
import com.solvve.dto.misprintinfofix.MisprintInfoFixReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.EntityWrongStatusException;
import com.solvve.service.MisprintInfoFixService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({MisprintInfoFixController.class, TestObjectsFactoryController.class})
public class MisprintInfoFilmFixControllerTest extends BaseControllerTest {

    @MockBean
    private MisprintInfoFixService misprintInfoService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testFixMisprintInfoFilmWrongId() throws Exception {

        MisprintInfoFixDTO patch = new MisprintInfoFixDTO();
        UUID wrongId = UUID.randomUUID();
        UUID contentManagerId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(MisprintInfoFixService.class, wrongId);
        Mockito.when(misprintInfoService.fixContent(contentManagerId, wrongId, patch)).thenThrow(exception);

        String resultJson = mvc.perform(post(
                "/api/v1/content-manager/{contentManagerId}/misprints-info/{misprintInfoFilmId}/fix",
                contentManagerId.toString(), wrongId.toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testFixMisprintInfoFilmWrongStatus() throws Exception {

        MisprintInfoFixDTO patch = new MisprintInfoFixDTO();
        UUID misprintInfoFilmId = UUID.randomUUID();
        UUID contentManagerId = UUID.randomUUID();

        EntityWrongStatusException exception = new EntityWrongStatusException(MisprintInfoFilm.class,
                misprintInfoFilmId, MisprintStatus.CANCELLED, MisprintStatus.NEED_TO_FIX);
        Mockito.when(misprintInfoService.fixContent(contentManagerId, misprintInfoFilmId, patch)).
                thenThrow(exception);
        String resultJson = mvc.perform(post(
                "/api/v1/content-manager/{contentManagerId}/misprints-info/{misprintInfoFilmId}/fix",
                contentManagerId.toString(),
                misprintInfoFilmId.toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testFixMisprintInfoFilmUserVersion() throws Exception {

        UUID filmId = UUID.randomUUID();
        UUID registeredUserId = UUID.randomUUID();
        UUID contentManagerId = UUID.randomUUID();
        MisprintInfoFixDTO patch = new MisprintInfoFixDTO();

        MisprintInfoFixReadDTO misprintInfoFilm = testObjectsFactoryController.
                createMPInfoFilmReadDTOWithParam(filmId, 11,18, "a Virtual", registeredUserId, contentManagerId);


        List<MisprintInfoFixReadDTO> expectedMisprintsRead = new ArrayList<>();
        expectedMisprintsRead.add(misprintInfoFilm);
        expectedMisprintsRead.add(testObjectsFactoryController.createMPInfoFilmReadDTOWithParam(filmId,
                12,18, "a Virtual", registeredUserId, contentManagerId));
        expectedMisprintsRead.add(testObjectsFactoryController.createMPInfoFilmReadDTOWithParam(filmId,
                12,16,"a Virtual", registeredUserId, contentManagerId));

        Mockito.when(misprintInfoService.fixContent(contentManagerId, misprintInfoFilm.getId(), patch)).
                thenReturn(expectedMisprintsRead);

        String resultJson = mvc.perform(post(
                "/api/v1/content-manager/{contentManagerId}/misprints-info/{misprintInfoFilmId}/fix",
                contentManagerId.toString(), misprintInfoFilm.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MisprintInfoFixReadDTO[] arrMisprintsRead = objectMapper.readValue(resultJson,
                MisprintInfoFixReadDTO[].class);
        Assertions.assertThat(expectedMisprintsRead).containsExactlyInAnyOrder(arrMisprintsRead);
    }

    @Test
    public void testFixMisprintInfoFilmCMVersion() throws Exception {

        UUID filmId = UUID.randomUUID();
        UUID registeredUserId = UUID.randomUUID();
        UUID contentManagerId = UUID.randomUUID();
        MisprintInfoFixDTO patch = new MisprintInfoFixDTO();
        patch.setText("a fiction");

        MisprintInfoFixReadDTO misprintInfoFilm = testObjectsFactoryController.
                createMPInfoFilmReadDTOWithParam(filmId, 11,18, "a Virtual", registeredUserId, contentManagerId);


        List<MisprintInfoFixReadDTO> expectedMisprintsRead = new ArrayList<>();
        expectedMisprintsRead.add(misprintInfoFilm);
        expectedMisprintsRead.add(testObjectsFactoryController.createMPInfoFilmReadDTOWithParam(filmId,
                12,18, "a Virtual", registeredUserId, contentManagerId));
        expectedMisprintsRead.add(testObjectsFactoryController.createMPInfoFilmReadDTOWithParam(filmId,
                12,16,"a Virtual", registeredUserId, contentManagerId));

        Mockito.when(misprintInfoService.fixContent(contentManagerId, misprintInfoFilm.getId(), patch)).
                thenReturn(expectedMisprintsRead);

        String resultJson = mvc.perform(post(
                "/api/v1/content-manager/{contentManagerId}/misprints-info/{misprintInfoFilmId}/fix",
                contentManagerId.toString(), misprintInfoFilm.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MisprintInfoFixReadDTO[] arrMisprintsRead = objectMapper.readValue(resultJson,
                MisprintInfoFixReadDTO[].class);
        Assertions.assertThat(expectedMisprintsRead).containsExactlyInAnyOrder(arrMisprintsRead);
    }
}
