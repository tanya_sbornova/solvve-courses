package com.solvve.controller;

import com.solvve.domain.MisprintInfoFilm;
import com.solvve.dto.misprintinfofilm.MisprintInfoFilmReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.MisprintInfoFilmService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({MisprintInfoFilmController.class, TestObjectsFactoryController.class})
public class MisprintInfoFilmControllerTest extends BaseControllerTest {

    @MockBean
    private MisprintInfoFilmService misprintInfoService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetMisprintInfoFilm() throws Exception {

        MisprintInfoFilmReadDTO misprintInfo = testObjectsFactoryController.createMisprintInfoFilmReadDTO();

        Mockito.when(misprintInfoService.getMisprintInfoFilm(misprintInfo.getId())).thenReturn(misprintInfo);

        String resultJson = mvc.perform(get("/api/v1/misprint-info-films/{id}", misprintInfo.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        MisprintInfoFilmReadDTO actualMisprintInfoFilm = objectMapper.readValue(resultJson,
                MisprintInfoFilmReadDTO.class);
        Assertions.assertThat(actualMisprintInfoFilm).isEqualToComparingFieldByField(misprintInfo);

        Mockito.verify(misprintInfoService).getMisprintInfoFilm(misprintInfo.getId());
    }

    @Test
    public void testGetMisprintInfoFilmWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(MisprintInfoFilm.class, wrongId);
        Mockito.when(misprintInfoService.getMisprintInfoFilm(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/misprint-info-films/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testDeleteMisprintInfoFilm() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/misprint-info-films/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(misprintInfoService).deleteMisprintInfoFilm(id);
    }

}

