package com.solvve.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.solvve.domain.Film;
import com.solvve.domain.FilmGenre;
import com.solvve.domain.FilmStatus;
import com.solvve.domain.PageResult;
import com.solvve.dto.film.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.FilmService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({FilmController.class, TestObjectsFactoryController.class})
public class FilmControllerTest extends BaseControllerTest {

    @MockBean
    private FilmService filmService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetFilm() throws Exception {

        FilmReadDTO film = testObjectsFactoryController.createFilmReadDTO();

        Mockito.when(filmService.getFilm(film.getId())).thenReturn(film);

        String resultJson = mvc.perform(get("/api/v1/films/{id}", film.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        FilmReadDTO actualFilm = objectMapper.readValue(resultJson, FilmReadDTO.class);
        Assertions.assertThat(actualFilm).isEqualToComparingFieldByField(film);

        Mockito.verify(filmService).getFilm(film.getId());
    }

    @Test
    public void testGetFilmWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Film.class, wrongId);
        Mockito.when(filmService.getFilm(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/films/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetFilmWrongTypeId() throws Exception {

        int wrongId = 123;
        Map mapErrorInfo = new HashMap<String, String>() {{
            put("status", "BAD_REQUEST");
            put("exceptionClass",
                    "org.springframework.web.method.annotation.MethodArgumentTypeMismatchException");
            put("message", "id should be of type java.util.UUID");
        }};

        String resultJson = mvc.perform(get("/api/v1/films/{id}", wrongId))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        Map mapJson = objectMapper.readValue(resultJson, Map.class);

        Assert.assertEquals(mapJson, mapErrorInfo);
        Assert.assertTrue(resultJson.contains("id should be of type java.util.UUID"));
    }

    @Test
    public void testGetFilmExtended() throws Exception {

        FilmReadExtendedDTO filmReadExtendedDTO =
                testObjectsFactoryController.createFilmReadExtendedDTO();

        Mockito.when(filmService.getFilmExtended(filmReadExtendedDTO.getId())).
                thenReturn(filmReadExtendedDTO);

        String resultJson = mvc.perform(get("/api/v1/films/{id}/extended",
                filmReadExtendedDTO.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        FilmReadExtendedDTO actualFilm = objectMapper.readValue(resultJson,
                FilmReadExtendedDTO.class);

        Assertions.assertThat(actualFilm).isEqualToComparingFieldByField(filmReadExtendedDTO);

        Mockito.verify(filmService).getFilmExtended(filmReadExtendedDTO.getId());
    }

    @Test
    public void testCreateFilm() throws Exception {

        FilmCreateDTO create = new FilmCreateDTO();
        create.setTitle("The Aeronauts");
        create.setText("The most significant balloon flight");
        create.setDatePrime(LocalDate.of(2019, 11, 4));
        create.setBudget(50000);
        create.setStatus(FilmStatus.RELEASED);

        FilmReadDTO read = testObjectsFactoryController.createFilmReadDTO();

        Mockito.when(filmService.createFilm(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/films")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        FilmReadDTO actualFilm = objectMapper.readValue(resultJson, FilmReadDTO.class);

        Assertions.assertThat(actualFilm).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchFilm() throws Exception {

        FilmPatchDTO patchDTO = new FilmPatchDTO();
        patchDTO.setTitle("The Aeronauts");
        patchDTO.setText("The most significant balloon flight");
        patchDTO.setDatePrime(LocalDate.of(2019, 11, 4));
        patchDTO.setBudget(50000);
        patchDTO.setStatus(FilmStatus.RELEASED);

        FilmReadDTO read = testObjectsFactoryController.createFilmReadDTO();

        Mockito.when(filmService.patchFilm(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/films/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        FilmReadDTO actualFilm = objectMapper.readValue(resultJson, FilmReadDTO.class);

        Assertions.assertThat(actualFilm).isEqualToComparingFieldByField(read);
        Assert.assertEquals(read, actualFilm);
    }

    @Test
    public void testUpdateFilm() throws Exception {

        FilmPutDTO putDTO = new FilmPutDTO();
        putDTO.setTitle("The Aeronauts");
        putDTO.setText("The most significant balloon flight");
        putDTO.setDatePrime(LocalDate.of(2019, 11, 4));
        putDTO.setBudget(50000);
        putDTO.setStatus(FilmStatus.RELEASED);

        FilmReadDTO read = testObjectsFactoryController.createFilmReadDTO();

        Mockito.when(filmService.updateFilm(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/films/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        FilmReadDTO actualFilm = objectMapper.readValue(resultJson, FilmReadDTO.class);

        Assertions.assertThat(actualFilm).isEqualToComparingFieldByField(read);
        Assert.assertEquals(read, actualFilm);
    }

    @Test
    public void testDeleteFilm() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/films/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(filmService).deleteFilm(id);
    }

    @Test
    public void testGetFilms() throws Exception {
        FilmFilter filmFilter = new FilmFilter();
        filmFilter.setBudgetFrom(40000);
        filmFilter.setBudgetTo(50000);
        filmFilter.setDatePrimeFrom(LocalDate.of(2018, 3, 21)); //
        filmFilter.setDatePrimeTo(LocalDate.of(2020, 3, 21));   //
        filmFilter.setStatus(FilmStatus.NOT_RELEASED);
        filmFilter.setGenre(FilmGenre.DRAMA);

        FilmReadDTO read = testObjectsFactoryController.createFilmReadDTO();

        PageResult<FilmReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(read));
        Mockito.when(filmService.getFilms(filmFilter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/films")
                .param("genre", filmFilter.getGenre().toString())
                .param("datePrimeFrom", filmFilter.getDatePrimeFrom().toString())
                .param("datePrimeTo", filmFilter.getDatePrimeTo().toString())
                .param("budgetFrom", filmFilter.getBudgetFrom().toString())
                .param("budgetTo", filmFilter.getBudgetTo().toString())
                .param("status", filmFilter.getStatus().toString())
                )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        PageResult<FilmReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {});
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetFilmsWithPagingAndSorting() throws Exception {
        FilmFilter filmFilter = new FilmFilter();
        filmFilter.setBudgetFrom(40000);
        filmFilter.setBudgetTo(50000);
        filmFilter.setDatePrimeFrom(LocalDate.of(2018, 3, 21));
        filmFilter.setDatePrimeTo(LocalDate.of(2020, 3, 21));
        filmFilter.setStatus(FilmStatus.NOT_RELEASED);
        filmFilter.setGenre(FilmGenre.DRAMA);

        FilmReadDTO read = testObjectsFactoryController.createFilmReadDTO();

        int page = 1;
        int size = 25;

        PageResult<FilmReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(new Long(100));
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "title"));
        Mockito.when(filmService.getFilms(filmFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/films")
                .param("genre", filmFilter.getGenre().toString())
                .param("datePrimeFrom", filmFilter.getDatePrimeFrom().toString())
                .param("datePrimeTo", filmFilter.getDatePrimeTo().toString())
                .param("budgetFrom", filmFilter.getBudgetFrom().toString())
                .param("budgetTo", filmFilter.getBudgetTo().toString())
                .param("status", filmFilter.getStatus().toString())
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "title,desc")
        )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<FilmReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {});
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetFilmsWithBigPage() throws Exception {
        FilmFilter filmFilter = new FilmFilter();
        filmFilter.setBudgetFrom(40000);
        filmFilter.setBudgetTo(50000);
        filmFilter.setDatePrimeFrom(LocalDate.of(2018, 3, 21));
        filmFilter.setDatePrimeTo(LocalDate.of(2020, 3, 21));
        filmFilter.setStatus(FilmStatus.NOT_RELEASED);
        filmFilter.setGenre(FilmGenre.DRAMA);

        FilmReadDTO read = testObjectsFactoryController.createFilmReadDTO();

        int page = 0;
        int size = 999999;

        PageResult<FilmReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(new Long(100));
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(page, maxPageSize, Sort.by(Sort.Direction.DESC, "title"));
        Mockito.when(filmService.getFilms(filmFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/films")
                .param("genre", filmFilter.getGenre().toString())
                .param("datePrimeFrom", filmFilter.getDatePrimeFrom().toString())
                .param("datePrimeTo", filmFilter.getDatePrimeTo().toString())
                .param("budgetFrom", filmFilter.getBudgetFrom().toString())
                .param("budgetTo", filmFilter.getBudgetTo().toString())
                .param("status", filmFilter.getStatus().toString())
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "title,desc")
        )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<FilmReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {});
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetFilmsLeaderBoard() throws Exception {
        int filmsCount = 10;
        List<FilmInLeaderBoardReadDTO> expectedFilmInLeaderBoard = new ArrayList<>();
        for (int i = 1; i < filmsCount; i++) {
            expectedFilmInLeaderBoard.add(testObjectsFactoryController.createFilmInLeaderBoardReadDTO());
        }

        Mockito.when(filmService.getFilmsLeaderBoard()).thenReturn(expectedFilmInLeaderBoard);

        String resultJson = mvc.perform(get("/api/v1/films/leader-board"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        FilmInLeaderBoardReadDTO[] filmInLeaderBoards = objectMapper.readValue(resultJson,
                FilmInLeaderBoardReadDTO[].class);
        Assertions.assertThat(expectedFilmInLeaderBoard).containsExactlyInAnyOrder(filmInLeaderBoards);

    }

    private Instant createInstant(int year, int month, int day) {
        return  LocalDateTime.of(year, month, day, 0, 0 ).toInstant(ZoneOffset.UTC);
    }

}
