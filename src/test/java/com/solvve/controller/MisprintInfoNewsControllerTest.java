package com.solvve.controller;

import com.solvve.domain.MisprintInfoNews;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.MisprintInfoNewsService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({MisprintInfoNewsController.class, TestObjectsFactoryController.class})
public class MisprintInfoNewsControllerTest extends BaseControllerTest {

    @MockBean
    private MisprintInfoNewsService misprintInfoService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetMisprintInfoNews() throws Exception {

        MisprintInfoNewsReadDTO misprintInfo = testObjectsFactoryController.createMisprintInfoNewsReadDTO();

        Mockito.when(misprintInfoService.getMisprintInfoNews(misprintInfo.getId())).thenReturn(misprintInfo);

        String resultJson = mvc.perform(get("/api/v1/misprint-info-news/{id}", misprintInfo.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        MisprintInfoNewsReadDTO actualMisprintInfoNews = objectMapper.readValue(resultJson,
                MisprintInfoNewsReadDTO.class);
        Assertions.assertThat(actualMisprintInfoNews).isEqualToComparingFieldByField(misprintInfo);

        Mockito.verify(misprintInfoService).getMisprintInfoNews(misprintInfo.getId());
    }

    @Test
    public void testGetMisprintInfoNewsWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(MisprintInfoNews.class, wrongId);
        Mockito.when(misprintInfoService.getMisprintInfoNews(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/misprint-info-news/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetMisprintsInfoNews() throws Exception {

        List<MisprintInfoNewsReadDTO> misprintsInfoNews =
                List.of(testObjectsFactoryController.createMisprintInfoNewsReadDTO(),
                testObjectsFactoryController.createMisprintInfoNewsReadDTO());

        Mockito.when(misprintInfoService.getMisprintsInfoNewsNeedToFix()).thenReturn(misprintsInfoNews);

        String resultJson = mvc.perform(get("/api/v1/misprint-info-news/need-to-fix"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MisprintInfoNewsReadDTO[] arrMisprintsInfoNews =
                objectMapper.readValue(resultJson, MisprintInfoNewsReadDTO[].class);
        Assertions.assertThat(misprintsInfoNews).containsExactlyInAnyOrder(arrMisprintsInfoNews);
    }

    @Test
    public void testDeleteMisprintInfoNews() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/misprint-info-news/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(misprintInfoService).deleteMisprintInfoNews(id);
    }
}



