package com.solvve.controller;

import com.solvve.domain.MisprintInfoFilm;
import com.solvve.domain.RegisteredUser;
import com.solvve.dto.misprintinfofilm.MisprintInfoFilmReadDTO;
import com.solvve.dto.misprintinfofilm.MisprintInfoFilmCreateDTO;
import com.solvve.dto.registereduser.RegisteredUserReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.RegisteredUserMisprintInfoFilmService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({RegisteredUserMisprintInfoFilmController.class, TestObjectsFactoryController.class})
public class RegisteredUserMisprintInfoFilmControllerTest extends BaseControllerTest {

    @MockBean
    private RegisteredUserMisprintInfoFilmService registeredUserMisprintInfoFilmService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetRegisteredMisprintInfoFilmLikes() throws Exception {

        List<MisprintInfoFilmReadDTO> misprintInfoFilms = List.of(testObjectsFactoryController.
                createMisprintInfoFilmReadDTO());
        RegisteredUserReadDTO registeredUser = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserMisprintInfoFilmService.getRegisteredUserMisprintInfoFilms(registeredUser.getId())).
                thenReturn(misprintInfoFilms);

        String resultJson = mvc.perform(get("/api/v1/registered-user/{registeredUserId}/misprint-info-films",
                registeredUser.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MisprintInfoFilmReadDTO[] arrMisprintInfoFilms = objectMapper.readValue(resultJson,
                MisprintInfoFilmReadDTO[].class);
        Assertions.assertThat(misprintInfoFilms).containsExactlyInAnyOrder(arrMisprintInfoFilms);
    }

    @Test
    public void testGetRegisteredMisprintInfoFilmLike() throws Throwable {

        MisprintInfoFilmReadDTO misprintInfoFilm = testObjectsFactoryController.createMisprintInfoFilmReadDTO();

        Mockito.when(registeredUserMisprintInfoFilmService.
                getRegisteredUserMisprintInfoFilm(misprintInfoFilm.getRegisteredUserId(),
                        misprintInfoFilm.getId())).thenReturn(misprintInfoFilm);

        String resultJson = mvc.perform(get(
                "/api/v1/registered-user/{registeredUserId}/misprint-info-films/{misprintInfoFilmId}",
                misprintInfoFilm.getRegisteredUserId().toString(),
                misprintInfoFilm.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MisprintInfoFilmReadDTO actualMisprintInfoFilm = objectMapper.readValue(resultJson,
                MisprintInfoFilmReadDTO.class);
        Assertions.assertThat(actualMisprintInfoFilm).isEqualToComparingFieldByField(misprintInfoFilm);

        Mockito.verify(registeredUserMisprintInfoFilmService).getRegisteredUserMisprintInfoFilm(misprintInfoFilm.
                getRegisteredUserId(), misprintInfoFilm.getId());
    }

    @Test
    public void testGetRegisteredMisprintInfoFilmLikeWrongId() throws Throwable {

        UUID wrongId = UUID.randomUUID();
        RegisteredUserReadDTO registeredUser = testObjectsFactoryController.createRegisteredUserReadDTO();

        EntityNotFoundException exception = new EntityNotFoundException(MisprintInfoFilm.class, wrongId,
                RegisteredUser.class, registeredUser.getId());
        Mockito.when(registeredUserMisprintInfoFilmService.getRegisteredUserMisprintInfoFilm(registeredUser.getId(),
                wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get(
                "/api/v1/registered-user/{registeredUserId}/misprint-info-films/{misprintInfoFilmId}",
                registeredUser.getId().toString(), wrongId.toString()))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateRegisteredMisprintInfoFilmLike() throws Exception {

        MisprintInfoFilmCreateDTO create = testObjectsFactoryController.createMisprintInfoFilmCreateDTO();
        MisprintInfoFilmReadDTO read = testObjectsFactoryController.createMisprintInfoFilmReadDTO();
        UUID registeredUserId = UUID.randomUUID();

        Mockito.when(registeredUserMisprintInfoFilmService.createRegisteredUserMisprintInfoFilm(registeredUserId,
                create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/registered-user/{registeredUserId}/misprint-info-films",
                registeredUserId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MisprintInfoFilmReadDTO actualMisprintInfoFilm = objectMapper.readValue(resultJson,
                MisprintInfoFilmReadDTO.class);
        Assertions.assertThat(actualMisprintInfoFilm).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testDeleteRegisteredMisprintInfoFilmLike() throws Exception {

        MisprintInfoFilmReadDTO read = testObjectsFactoryController.createMisprintInfoFilmReadDTO();
        RegisteredUserReadDTO registeredUserReadDTO = testObjectsFactoryController.createRegisteredUserReadDTO();

        mvc.perform(delete("/api/v1/registered-user/{registeredUserId}/misprint-info-films/{misprintInfoFilmId}",
                registeredUserReadDTO.getId().toString(), read.getId().toString()))
                .andExpect(status().isOk());

        Mockito.verify(registeredUserMisprintInfoFilmService).
                deleteRegisteredUserMisprintInfoFilm(registeredUserReadDTO.getId(), read.getId());
    }
}

