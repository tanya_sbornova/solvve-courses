package com.solvve.controller;

import com.solvve.dto.actor.*;
import com.solvve.dto.person.PersonReadDTO;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.util.TestObjectsFactoryController;
import com.solvve.domain.Actor;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.ActorService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({ActorController.class, TestObjectsFactoryController.class})
public class ActorControllerTest extends BaseControllerTest {

    @MockBean
    private ActorService actorService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetActor() throws Exception {

        ActorReadDTO actor = testObjectsFactoryController.createActorReadDTO();

        Mockito.when(actorService.getActor(actor.getId())).thenReturn(actor);

        String resultJson = mvc.perform(get("/api/v1/actors/{id}", actor.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorReadDTO actualActor = objectMapper.readValue(resultJson, ActorReadDTO.class);
        Assertions.assertThat(actualActor).isEqualToComparingFieldByField(actor);

        Mockito.verify(actorService).getActor(actor.getId());
    }

    @Test
    public void testGetActorExtended() throws Exception {

        ActorReadExtendedDTO actorReadExtendedDTO =
                testObjectsFactoryController.createActorReadExtendedDTO();

        Mockito.when(actorService.getActorExtended(actorReadExtendedDTO.getId())).
                thenReturn(actorReadExtendedDTO);

        String resultJson = mvc.perform(get("/api/v1/actors/{id}/extended",
                actorReadExtendedDTO.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorReadExtendedDTO actualActor = objectMapper.readValue(resultJson,
                ActorReadExtendedDTO.class);

        Assertions.assertThat(actualActor).isEqualToComparingFieldByField(actorReadExtendedDTO);

        Mockito.verify(actorService).getActorExtended(actorReadExtendedDTO.getId());
    }

    @Test
    public void testGetPersonActor() throws Throwable {

        ActorReadDTO actor = testObjectsFactoryController.createActorReadDTO();

        Mockito.when(actorService.getPersonActor(actor.getPersonId(),
                actor.getId())).thenReturn(actor);

        String resultJson = mvc.perform(get("/api/v1/person/{personId}/actors/{actorId}",
                actor.getPersonId(), actor.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorReadDTO actualCrewMember = objectMapper.readValue(resultJson, ActorReadDTO.class);
        Assertions.assertThat(actualCrewMember).isEqualToComparingFieldByField(actor);

        Mockito.verify(actorService).getPersonActor(actor.getPersonId(), actor.getId());
    }

    @Test
    public void testGetActorWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Actor.class, wrongId);
        Mockito.when(actorService.getActor(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/actors/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateActor() throws Exception {

        ActorCreateDTO create = testObjectsFactoryController.createActorCreateDTO();
        ActorReadDTO read = testObjectsFactoryController.createActorReadDTO();

        Mockito.when(actorService.createActor(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/actors")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorReadDTO actualActor = objectMapper.readValue(resultJson, ActorReadDTO.class);
        Assertions.assertThat(actualActor).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testCreatePersonActor() throws Exception {

        ActorCreateDTO create = testObjectsFactoryController.createActorCreateDTO();
        ActorReadDTO read = testObjectsFactoryController.createActorReadDTO();
        PersonReadDTO personReadDTO = testObjectsFactoryController.createPersonReadDTO();

        Mockito.when(actorService.createPersonActor(personReadDTO.getId(), create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/person/{personId}/actors", personReadDTO.getId())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorReadDTO actualActor = objectMapper.readValue(resultJson, ActorReadDTO.class);
        Assertions.assertThat(actualActor).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testDeleteActor() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/actors/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(actorService).deleteActor(id);
    }

    @Test
    public void testCreateActorValidationFailed() throws Exception {

        ActorCreateDTO create = new ActorCreateDTO();

        ActorReadDTO read = testObjectsFactoryController.createActorReadDTO();

        Mockito.when(actorService.createActor(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/actors/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(actorService, Mockito.never()).createActor(ArgumentMatchers.any());
    }

    @Test
    public void testGetActorsLeaderBoard() throws Exception {
        int filmsCount = 10;
        List<ActorInLeaderBoardReadDTO> expectedActorInLeaderBoard = new ArrayList<>();
        for (int i = 1; i < filmsCount; i++) {
            expectedActorInLeaderBoard.add(testObjectsFactoryController.createActorInLeaderBoardReadDTO());
        }

        Mockito.when(actorService.getActorsLeaderBoard()).thenReturn(expectedActorInLeaderBoard);

        String resultJson = mvc.perform(get("/api/v1/actors/leader-board"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorInLeaderBoardReadDTO[] filmInLeaderBoards = objectMapper.readValue(resultJson,
                ActorInLeaderBoardReadDTO[].class);
        Assertions.assertThat(expectedActorInLeaderBoard).containsExactlyInAnyOrder(filmInLeaderBoards);

    }
}

