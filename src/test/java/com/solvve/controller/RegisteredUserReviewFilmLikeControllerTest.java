package com.solvve.controller;

import com.solvve.domain.ReviewFilmLikeDislike;
import com.solvve.domain.RegisteredUser;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeCreateDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePatchDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePutDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeReadDTO;
import com.solvve.dto.registereduser.RegisteredUserReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.service.RegisteredUserReviewFilmLikeService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({RegisteredUserReviewFilmLikeController.class, TestObjectsFactoryController.class})
public class RegisteredUserReviewFilmLikeControllerTest extends BaseControllerTest {

    @MockBean
    private RegisteredUserReviewFilmLikeService registeredUserReviewFilmLikeDislikeService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetRegisteredUserFilmLikes() throws Exception {

        List<ReviewFilmLikeDislikeReadDTO> reviewFilmLikeDislikes =
                List.of(testObjectsFactoryController.createReviewFilmLikeDislikeReadDTO());
        RegisteredUserReadDTO registeredUser = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserReviewFilmLikeDislikeService.
                getRegisteredUserReviewFilmLikes(registeredUser.getId())).thenReturn(reviewFilmLikeDislikes);

        String resultJson = mvc.perform(get("/api/v1/registered-user/{registeredUserId}/review-film-like-dislike",
                registeredUser.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewFilmLikeDislikeReadDTO[] arrReviewFilmLikeDislikes =
                objectMapper.readValue(resultJson, ReviewFilmLikeDislikeReadDTO[].class);
        Assertions.assertThat(reviewFilmLikeDislikes).containsExactlyInAnyOrder(arrReviewFilmLikeDislikes);
    }

    @Test
    public void testGetRegisteredUserFilmLike() throws Throwable {

        ReviewFilmLikeDislikeReadDTO reviewFilmLikeDislike = testObjectsFactoryController.
                createReviewFilmLikeDislikeReadDTO();
        RegisteredUserReadDTO registeredUser = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserReviewFilmLikeDislikeService.
                getRegisteredUserReviewFilmLike(registeredUser.getId(),
                        reviewFilmLikeDislike.getId())).thenReturn(reviewFilmLikeDislike);

        String resultJson = mvc.perform(get(
                "/api/v1/registered-user/{registeredUserId}/review-film-like-dislike/{reviewFilmLikeDislikeId}",
                registeredUser.getId().toString(),
                reviewFilmLikeDislike.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewFilmLikeDislikeReadDTO actualReviewFilmLikeDislike = objectMapper.
                readValue(resultJson, ReviewFilmLikeDislikeReadDTO.class);
        Assertions.assertThat(actualReviewFilmLikeDislike).isEqualToComparingFieldByField(reviewFilmLikeDislike);

        Mockito.verify(registeredUserReviewFilmLikeDislikeService).
                getRegisteredUserReviewFilmLike(registeredUser.getId(),
                        reviewFilmLikeDislike.getId());
    }

    @Test
    public void testGetRegisteredUserFilmLikeWrongId() throws Throwable {

        UUID wrongId = UUID.randomUUID();
        RegisteredUserReadDTO registeredUser = testObjectsFactoryController.createRegisteredUserReadDTO();

        EntityNotFoundException exception = new EntityNotFoundException(ReviewFilmLikeDislike.class, wrongId,
                RegisteredUser.class, registeredUser.getId());
        Mockito.when(registeredUserReviewFilmLikeDislikeService.
                getRegisteredUserReviewFilmLike(registeredUser.getId(), wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get(
                "/api/v1/registered-user/{registeredUserId}/review-film-like-dislike/{reviewFilmLikeDislikeId}",
                registeredUser.getId().toString(), wrongId.toString()))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateRegisteredUserFilmLike() throws Exception {

        ReviewFilmLikeDislikeCreateDTO create = testObjectsFactoryController.createReviewFilmLikeDislikeCreateDTO();
        ReviewFilmLikeDislikeReadDTO read = testObjectsFactoryController.createReviewFilmLikeDislikeReadDTO();
        UUID registeredUserId = UUID.randomUUID();

        Mockito.when(registeredUserReviewFilmLikeDislikeService.
                createRegisteredUserReviewFilmLike(registeredUserId, create)).thenReturn(read);

        String resultJson = mvc.perform(post(
                "/api/v1/registered-user/{registeredUserId}/review-film-like-dislike",
                registeredUserId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewFilmLikeDislikeReadDTO actualReviewFilmLikeDislike = objectMapper.
                readValue(resultJson, ReviewFilmLikeDislikeReadDTO.class);
        Assertions.assertThat(actualReviewFilmLikeDislike).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchRegisteredUserFilmLike() throws Throwable {

        ReviewFilmLikeDislikePatchDTO patchDTO = testObjectsFactoryController.createReviewFilmLikeDislikePatchDTO();
        ReviewFilmLikeDislikeReadDTO read = testObjectsFactoryController.createReviewFilmLikeDislikeReadDTO();
        RegisteredUserReadDTO registeredUserReadDTO = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserReviewFilmLikeDislikeService.
                patchRegisteredUserReviewFilmLike(registeredUserReadDTO.getId(), read.getId(),
                patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch(
                "/api/v1/registered-user/{registeredUserId}/review-film-like-dislike/{reviewFilmLikeDislikeId}",
                registeredUserReadDTO.getId().toString(), read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewFilmLikeDislikeReadDTO actualReviewFilmLikeDislike = objectMapper.readValue(resultJson,
                ReviewFilmLikeDislikeReadDTO.class);
        Assert.assertEquals(read, actualReviewFilmLikeDislike);
    }

    @Test
    public void testUpdateRegisteredUserFilmLike() throws Throwable{

        ReviewFilmLikeDislikePutDTO putDTO = testObjectsFactoryController.createReviewFilmLikeDislikePutDTO();
        ReviewFilmLikeDislikeReadDTO read = testObjectsFactoryController.createReviewFilmLikeDislikeReadDTO();
        RegisteredUserReadDTO registeredUserReadDTO = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserReviewFilmLikeDislikeService.
                updateRegisteredUserReviewFilmLike(registeredUserReadDTO.getId(), read.getId(),
                putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put(
                "/api/v1/registered-user/{registeredUserId}/review-film-like-dislike/{reviewFilmLikeDislikeId}",
                registeredUserReadDTO.getId().toString(), read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewFilmLikeDislikeReadDTO actualAdmin = objectMapper.
                readValue(resultJson, ReviewFilmLikeDislikeReadDTO.class);
        Assert.assertEquals(read, actualAdmin);
    }

    @Test
    public void testDeleteRegisteredUserFilmLike() throws Exception {

        ReviewFilmLikeDislikeReadDTO read = testObjectsFactoryController.createReviewFilmLikeDislikeReadDTO();
        RegisteredUserReadDTO registeredUserReadDTO = testObjectsFactoryController.createRegisteredUserReadDTO();

        mvc.perform(delete(
                "/api/v1/registered-user/{registeredUserId}/review-film-like-dislike/{reviewFilmLikeDislikeId}",
                registeredUserReadDTO.getId().toString(), read.getId().toString()))
                .andExpect(status().isOk());

        Mockito.verify(registeredUserReviewFilmLikeDislikeService).
                deleteRegisteredUserReviewFilmLike(registeredUserReadDTO.getId(), read.getId());
    }

    @Test
    public void testCreateReviewFilmLikeDislikeValidationFailed() throws Exception {

        ReviewFilmLikeDislikeCreateDTO create = new ReviewFilmLikeDislikeCreateDTO();
        ReviewFilmLikeDislikeReadDTO read = testObjectsFactoryController.createReviewFilmLikeDislikeReadDTO();
        UUID registeredUserId = UUID.randomUUID();
        Mockito.when(registeredUserReviewFilmLikeDislikeService.
                createRegisteredUserReviewFilmLike(registeredUserId, create)).thenReturn(read);

        String resultJson = mvc.perform(post(
                "/api/v1/registered-user/{registeredUserId}/review-film-like-dislike",
                registeredUserId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(registeredUserReviewFilmLikeDislikeService, Mockito.never())
                .createRegisteredUserReviewFilmLike(ArgumentMatchers.any(), ArgumentMatchers.any());
    }
}
