package com.solvve.controller.integration;

import com.solvve.BaseTest;
import com.solvve.domain.*;
import com.solvve.dto.film.FilmCreateDTO;
import com.solvve.dto.film.FilmReadDTO;
import com.solvve.dto.reviewfilm.ReviewFilmCreateDTO;
import com.solvve.dto.reviewfilm.ReviewFilmPatchDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeCreateDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeReadDTO;
import com.solvve.repository.AdminRepository;
import com.solvve.repository.ContentManagerRepository;
import com.solvve.repository.ModeratorRepository;
import com.solvve.repository.RegisteredUserRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.dbunit.util.Base64;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SecurityIntegrationTest extends BaseTest {

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @LocalServerPort
    private int serverPort;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    RestTemplate restTemplate;

    @Test
    public void testHealthNoSecurity() {

        ResponseEntity<Void> response = restTemplate.getForEntity("http://localhost:" + serverPort
                + "/health", Void.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetFilmsNoSecurity() {

        Assertions.assertThatThrownBy(() -> restTemplate.exchange("http://localhost:" + serverPort + "/api/v1/films",
                HttpMethod.GET, HttpEntity.EMPTY, new ParameterizedTypeReference<Void>() {
                }))
                .isInstanceOf(HttpClientErrorException.class)
                .extracting("statusCode").isEqualTo(HttpStatus.UNAUTHORIZED);

    }

    @Test
    public void testGetFilms() {
        String email = "test@gmail.com";
        String password = "pass123";

        ContentManager cm = new ContentManager();
        cm.setEmail(email);
        cm.setEncodedPassword(passwordEncoder.encode(password));
        cm.setUserRole(UserRoleType.CONTENT_MANAGER);
        cm.setUserStatus(UserStatus.ACTIVE);
        contentManagerRepository.save(cm);


        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<PageResult<FilmReadDTO>> response = restTemplate.exchange("http://localhost:" + serverPort
                        + "/api/v1/films", HttpMethod.GET, httpEntity, new ParameterizedTypeReference<>() {
                });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetFilmsWrongPassword() {
        String email = "test@gmail.com";
        String password = "pass123";

        Admin user = new Admin();
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));
        user.setUserRole(UserRoleType.ADMIN);
        user.setUserStatus(UserStatus.ACTIVE);
        adminRepository.save(user);


        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, "wrong password"));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange("http://localhost:" + serverPort + "/api/v1/films",
                HttpMethod.GET, httpEntity, new ParameterizedTypeReference<Void>() {
                }))
                .isInstanceOf(HttpClientErrorException.class)
                .extracting("statusCode").isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testGetFilmsWrongUser() {
        String email = "test@gmail.com";
        String password = "pass123";

        Admin user = new Admin();
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));
        user.setUserRole(UserRoleType.ADMIN);
        user.setUserStatus(UserStatus.ACTIVE);
        adminRepository.save(user);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue("wrong email", password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange("http://localhost:" + serverPort + "/api/v1/films",
                HttpMethod.GET, httpEntity, new ParameterizedTypeReference<Void>() {
                }))
                .isInstanceOf(HttpClientErrorException.class)
                .extracting("statusCode").isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testGetFilmsNoSession() {
        String email = "test@gmail.com";
        String password = "pass123";

        RegisteredUser user = new RegisteredUser();
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));
        user.setUserRole(UserRoleType.REGISTERED_USER);
        user.setUserStatus(UserStatus.ACTIVE);
        registeredUserRepository.save(user);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<PageResult<FilmReadDTO>> response = restTemplate.exchange("http://localhost:" + serverPort
                        + "/api/v1/films",
                HttpMethod.GET, httpEntity, new ParameterizedTypeReference<>() {
                });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNull(response.getHeaders().get("Set-Cookie"));
    }

    @Test
    public void testGetFilmsRegisteredUser() {
        String email = "test@gmail.com";
        String password = "pass123";

        RegisteredUser user = new RegisteredUser();
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));
        user.setUserRole(UserRoleType.REGISTERED_USER);
        user.setUserStatus(UserStatus.ACTIVE);
        registeredUserRepository.save(user);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<PageResult<FilmReadDTO>> response =
                restTemplate.exchange("http://localhost:" + serverPort + "/api/v1/films",
                        HttpMethod.GET, httpEntity, new ParameterizedTypeReference<>() {
                        });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetFilmsContentManager() {
        String email = "test@gmail.com";
        String password = "pass123";

        ContentManager user = new ContentManager();
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));
        user.setUserRole(UserRoleType.CONTENT_MANAGER);
        user.setUserStatus(UserStatus.ACTIVE);
        contentManagerRepository.save(user);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<PageResult<FilmReadDTO>> response =
                restTemplate.exchange("http://localhost:" + serverPort + "/api/v1/films",
                        HttpMethod.GET, httpEntity, new ParameterizedTypeReference<>() {
                        });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetFilmsWrongRole() {
        String email = "test@gmail.com";
        String password = "pass123";

        Moderator user = new Moderator();
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));
        user.setUserRole(UserRoleType.MODERATOR);
        user.setUserStatus(UserStatus.ACTIVE);
        moderatorRepository.save(user);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange("http://localhost:" + serverPort + "/api/v1/films",
                HttpMethod.GET, httpEntity, new ParameterizedTypeReference<PageResult<FilmReadDTO>>() {
                }))
                .isInstanceOf(HttpClientErrorException.class)
                .extracting("statusCode").isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void testCreateFilmContentManager() {
        String email = "test@gmail.com";
        String password = "pass123";

        ContentManager user = new ContentManager();
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));
        user.setUserRole(UserRoleType.CONTENT_MANAGER);
        user.setUserStatus(UserStatus.ACTIVE);
        contentManagerRepository.save(user);

        FilmCreateDTO filmCreateDTO = testObjectsFactory.generateObject(FilmCreateDTO.class);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));

        HttpEntity<?> httpEntity = new HttpEntity<>(filmCreateDTO, headers);

        ResponseEntity<FilmReadDTO> response =
                restTemplate.exchange("http://localhost:" + serverPort + "/api/v1/films",
                        HttpMethod.POST, httpEntity, FilmReadDTO.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testCreateFilmContentManagerWrongRole() {
        String email = "test@gmail.com";
        String password = "pass123";

        ContentManager user = new ContentManager();
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));
        user.setUserRole(UserRoleType.REGISTERED_USER);
        user.setUserStatus(UserStatus.ACTIVE);
        contentManagerRepository.save(user);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));

        FilmCreateDTO filmCreateDTO = testObjectsFactory.generateObject(FilmCreateDTO.class);
        HttpEntity<?> httpEntity = new HttpEntity<>(filmCreateDTO, headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange("http://localhost:" + serverPort + "/api/v1/films",
                HttpMethod.POST, httpEntity, new ParameterizedTypeReference<PageResult<FilmReadDTO>>() {
                }))
                .isInstanceOf(HttpClientErrorException.class)
                .extracting("statusCode").isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void testGetRegisteredUserReviewFilmLikesNoRoles() {
        String email = "test@gmail.com";
        String password = "pass123";

        RegisteredUser user = new RegisteredUser();
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));
        user.setUserStatus(UserStatus.ACTIVE);
        RegisteredUser userAfterSave = registeredUserRepository.save(user);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange("http://localhost:" + serverPort
                        + "/api/v1/registered-user/" + userAfterSave.getId() + "/review-film-like-dislike",
                HttpMethod.GET, httpEntity, new ParameterizedTypeReference<List<ReviewFilmLikeDislikeReadDTO>>() {
                }))
                .isInstanceOf(HttpClientErrorException.class)
                .extracting("statusCode").isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testGetRegisteredUserReviewFilmLikesWrongRole() {
        String email = "test@gmail.com";
        String password = "pass123";

        RegisteredUser user = new RegisteredUser();
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));
        user.setUserRole(UserRoleType.CONTENT_MANAGER);
        user.setUserStatus(UserStatus.ACTIVE);
        RegisteredUser userAfterSave = registeredUserRepository.save(user);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange("http://localhost:" + serverPort
                        + "/api/v1/registered-user/" + userAfterSave.getId() + "/review-film-like-dislike",
                HttpMethod.GET, httpEntity, new ParameterizedTypeReference<List<ReviewFilmLikeDislikeReadDTO>>() {
                }))
                .isInstanceOf(HttpClientErrorException.class)
                .extracting("statusCode").isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void testGetRegisteredUserReviewFilmLikesWrongRegisteredUser() {
        String email1 = "test@gmail.com";
        String email2 = "test2@gmail.com";
        String password = "pass123";

        RegisteredUser user1 = new RegisteredUser();
        user1.setEmail(email1);
        user1.setEncodedPassword(passwordEncoder.encode("abc"));
        user1.setUserRole(UserRoleType.REGISTERED_USER);
        user1.setUserStatus(UserStatus.ACTIVE);
        RegisteredUser user1AfterSave = registeredUserRepository.save(user1);

        RegisteredUser user2 = new RegisteredUser();
        user2.setEmail(email2);
        user2.setEncodedPassword(passwordEncoder.encode(password));
        user2.setUserRole(UserRoleType.REGISTERED_USER);
        user2.setUserStatus(UserStatus.ACTIVE);
        registeredUserRepository.save(user2);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email2, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange("http://localhost:" + serverPort
                        + "/api/v1/registered-user/" + user1AfterSave.getId() + "/review-film-like-dislike",
                HttpMethod.GET, httpEntity, new ParameterizedTypeReference<List<ReviewFilmLikeDislikeReadDTO>>() {
                }))
                .isInstanceOf(HttpClientErrorException.class)
                .extracting("statusCode").isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void testGetRegisteredUserReviewFilmLikesRegisteredUser() {
        String email = "test@gmail.com";
        String password = "pass123";

        RegisteredUser user = new RegisteredUser();
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));
        user.setUserRole(UserRoleType.REGISTERED_USER);
        user.setUserStatus(UserStatus.ACTIVE);
        user = registeredUserRepository.save(user);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        String url = "http://localhost:" + serverPort + "/api/v1/registered-user/"
                + user.getId() + "/review-film-like-dislike";

        ResponseEntity<List<ReviewFilmLikeDislikeReadDTO>> response =
                restTemplate.exchange(url,
                        HttpMethod.GET, httpEntity, new ParameterizedTypeReference<>() {
                        });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetRegisteredUserReviewFilmLikeRegisteredUser() {
        String email = "test@gmail.com";
        String password = "pass123";

        ReviewFilmLikeDislike reviewFilmLikeDislike = testObjectsFactory.createReviewFilmLikeDislike();
        RegisteredUser user = reviewFilmLikeDislike.getRegisteredUser();
        user.setUserRole(UserRoleType.REGISTERED_USER);
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));
        user = registeredUserRepository.save(user);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        String url = "http://localhost:" + serverPort + "/api/v1/registered-user/" + user.getId()
                + "/review-film-like-dislike/" + reviewFilmLikeDislike.getId();

        ResponseEntity<ReviewFilmLikeDislikeReadDTO> response =
                restTemplate.exchange(url, HttpMethod.GET, httpEntity, new ParameterizedTypeReference<>() {
                });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testCreateRegisteredUserReviewFilmLikesWrongRegisteredUser() {
        String email1 = "test@gmail.com";
        String email2 = "test2@gmail.com";
        String password = "pass123";

        RegisteredUser user1 = new RegisteredUser();
        user1.setEmail(email1);
        user1.setEncodedPassword(passwordEncoder.encode("abc"));
        user1.setUserRole(UserRoleType.REGISTERED_USER);
        user1.setUserStatus(UserStatus.ACTIVE);
        RegisteredUser user1AfterSave = registeredUserRepository.save(user1);

        RegisteredUser user2 = new RegisteredUser();
        user2.setEmail(email2);
        user2.setEncodedPassword(passwordEncoder.encode(password));
        user2.setUserRole(UserRoleType.REGISTERED_USER);
        user2.setUserStatus(UserStatus.ACTIVE);
        registeredUserRepository.save(user2);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email2, password));

        ReviewFilmLikeDislikeCreateDTO filmCreateDTO =
                testObjectsFactory.generateObject(ReviewFilmLikeDislikeCreateDTO.class);
        HttpEntity<?> httpEntity = new HttpEntity<>(filmCreateDTO, headers);

        String url = "http://localhost:" + serverPort + "/api/v1/registered-user/"
                + user1AfterSave.getId() + "/review-film-like-dislike";

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(url,
                HttpMethod.POST, httpEntity, new ParameterizedTypeReference<List<ReviewFilmLikeDislikeReadDTO>>() {
                }))
                .isInstanceOf(HttpClientErrorException.class)
                .extracting("statusCode").isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void testCreateRegisteredUserReviewFilmLikesWrongRole() {
        String email = "test@gmail.com";
        String password = "pass123";

        RegisteredUser user = new RegisteredUser();
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));
        user.setUserRole(UserRoleType.MODERATOR);
        user.setUserStatus(UserStatus.ACTIVE);
        RegisteredUser userAfterSave = registeredUserRepository.save(user);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));

        ReviewFilmLikeDislikeCreateDTO filmCreateDTO =
                testObjectsFactory.generateObject(ReviewFilmLikeDislikeCreateDTO.class);
        HttpEntity<?> httpEntity = new HttpEntity<>(filmCreateDTO, headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange("http://localhost:" + serverPort
                        + "/api/v1/registered-user/" + userAfterSave.getId() + "/review-film-like-dislike",
                HttpMethod.POST, httpEntity, new ParameterizedTypeReference<List<ReviewFilmLikeDislikeReadDTO>>() {
                }))
                .isInstanceOf(HttpClientErrorException.class)
                .extracting("statusCode").isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void testCreateRegisteredUserReviewFilmLikesRegisteredUser() {
        String email = "test@gmail.com";
        String password = "pass123";

        RegisteredUser user = new RegisteredUser();
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));
        user.setUserRole(UserRoleType.REGISTERED_USER);
        user.setUserStatus(UserStatus.ACTIVE);
        RegisteredUser userAfterSave = registeredUserRepository.save(user);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));

        ReviewFilmLikeDislikeCreateDTO createDTO = testObjectsFactory.createReviewFilmLikeDislikeCreateDTO();

        HttpEntity<?> httpEntity = new HttpEntity<>(createDTO, headers);

        ResponseEntity<ReviewFilmLikeDislikeReadDTO> response =
                restTemplate.exchange("http://localhost:" + serverPort + "/api/v1/registered-user/"
                                + userAfterSave.getId() + "/review-film-like-dislike",
                        HttpMethod.POST, httpEntity, new ParameterizedTypeReference<>() {
                        });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testCreateFilmReviewFilmRegisteredUser() {
        String email = "test@gmail.com";
        String password = "pass123";

        RegisteredUser user = new RegisteredUser();
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));
        user.setUserRole(UserRoleType.REGISTERED_USER);
        user.setUserStatus(UserStatus.ACTIVE);
        registeredUserRepository.save(user);

        Film film = testObjectsFactory.createFilm();

        String url = "http://localhost:" + serverPort + "/api/v1/film/" + film.getId() + "/review-films/";

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));

        ReviewFilmCreateDTO createDTO = testObjectsFactory.createReviewFilmCreateDTO();
        HttpEntity<?> httpEntity = new HttpEntity<>(createDTO, headers);

        ResponseEntity<ReviewFilmLikeDislikeReadDTO> response =
                restTemplate.exchange(url,
                        HttpMethod.POST, httpEntity, new ParameterizedTypeReference<>() {
                        });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testPatchFilmReviewFilmRegisteredUser() {
        String email = "test@gmail.com";
        String password = "pass123";

        RegisteredUser user = new RegisteredUser();
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));
        user.setUserRole(UserRoleType.REGISTERED_USER);
        user.setUserStatus(UserStatus.ACTIVE);
        registeredUserRepository.save(user);

        Film film = testObjectsFactory.createFilm();
        ReviewFilm reviewFilm = testObjectsFactory.createReviewFilm(film);
        ReviewFilmPatchDTO patchDTO = new ReviewFilmPatchDTO();
        patchDTO.setText("The best film");
        patchDTO.setStatus(ReviewStatus.MODERATED);

        String url = "http://localhost:" + serverPort + "/api/v1/film/" + film.getId() + "/review-films/"
                + reviewFilm.getId();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));

        HttpEntity<?> httpEntity = new HttpEntity<>(patchDTO, headers);

        ResponseEntity<ReviewFilmLikeDislikeReadDTO> response =
                restTemplate.exchange(url,
                        HttpMethod.PATCH, httpEntity, new ParameterizedTypeReference<>() {
                        });
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    private String getBasicAuthorizationHeaderValue(String userName, String password) {
        return "Basic " + new String(Base64.encodeBytes(String.format("%s:%s", userName, password).getBytes()));
    }
}
