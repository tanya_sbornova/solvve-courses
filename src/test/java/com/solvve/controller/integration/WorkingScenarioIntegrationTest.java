package com.solvve.controller.integration;

import com.solvve.BaseTest;
import com.solvve.domain.*;
import com.solvve.dto.actor.ActorCreateDTO;
import com.solvve.dto.actor.ActorReadDTO;
import com.solvve.dto.actor.ActorReadExtendedDTO;
import com.solvve.dto.contentmanager.ContentManagerCreateDTO;
import com.solvve.dto.contentmanager.ContentManagerPutDTO;
import com.solvve.dto.contentmanager.ContentManagerReadDTO;
import com.solvve.dto.crewmember.CrewMemberCreateDTO;
import com.solvve.dto.crewmember.CrewMemberReadDTO;
import com.solvve.dto.department.DepartmentCreateDTO;
import com.solvve.dto.department.DepartmentReadDTO;
import com.solvve.dto.film.FilmCreateDTO;
import com.solvve.dto.film.FilmReadDTO;
import com.solvve.dto.film.FilmReadExtendedDTO;
import com.solvve.dto.genre.GenreReadDTO;
import com.solvve.dto.misprintinfofix.MisprintInfoFixDTO;
import com.solvve.dto.misprintinfofix.MisprintInfoFixReadDTO;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsCreateDTO;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsReadDTO;
import com.solvve.dto.moderator.ModeratorCreateDTO;
import com.solvve.dto.moderator.ModeratorPutDTO;
import com.solvve.dto.moderator.ModeratorReadDTO;
import com.solvve.dto.news.NewsCreateDTO;
import com.solvve.dto.news.NewsReadDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikeCreateDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikeReadDTO;
import com.solvve.dto.person.PersonCreateDTO;
import com.solvve.dto.person.PersonReadDTO;
import com.solvve.dto.problemreview.ProblemReviewCreateDTO;
import com.solvve.dto.problemreview.ProblemReviewReadDTO;
import com.solvve.dto.ratingfilm.RatingFilmCreateDTO;
import com.solvve.dto.ratingfilm.RatingFilmReadDTO;
import com.solvve.dto.ratingrole.RatingRoleCreateDTO;
import com.solvve.dto.ratingrole.RatingRoleReadDTO;
import com.solvve.dto.registereduser.RegisteredUserCreateDTO;
import com.solvve.dto.registereduser.RegisteredUserPatchDTO;
import com.solvve.dto.registereduser.RegisteredUserPutDTO;
import com.solvve.dto.registereduser.RegisteredUserReadDTO;
import com.solvve.dto.reviewfilm.ReviewFilmCreateDTO;
import com.solvve.dto.reviewfilm.ReviewFilmFilter;
import com.solvve.dto.reviewfilm.ReviewFilmPatchDTO;
import com.solvve.dto.reviewfilm.ReviewFilmReadDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeCreateDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeReadDTO;
import com.solvve.dto.role.RoleCreateDTO;
import com.solvve.dto.role.RoleFilter;
import com.solvve.dto.role.RoleReadDTO;
import com.solvve.repository.AdminRepository;
import com.solvve.util.TestObjectsFactory;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.assertj.core.api.Assertions;
import org.dbunit.util.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@ActiveProfiles({"test"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Import(WorkingScenarioIntegrationTest.ScheduledTestConfig.class)
public class WorkingScenarioIntegrationTest extends BaseTest {

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @EnableScheduling
    static class ScheduledTestConfig {
    }

    @Autowired
    RestTemplate restTemplate;

    @LocalServerPort
    private int serverPort;

    @Test
    public void workingScenarioIntegrationTest() throws IOException, InterruptedException,
            org.json.simple.parser.ParseException {

        final String URL = "http://localhost:" + serverPort + "/api/v1/";

        String emailA1 = "a1@gmail.com";
        String passwordA1 = "pass12345";
        String emailM1 = "m1@gmail.com";
        String passwordM1 = "pass123";
        String emailCm1 = "c1@gmail.com";
        String passwordCm1 = "pass234";
        String emailUser1 = "u1@gmail.com";
        String passwordUser1 = "passQwerty1";
        String emailUser2 = "u2@gmail.com";
        String passwordUser2 = "passQwerty2";
        String emailUser3 = "u3@gmail.com";
        String passwordUser3 = "passQwerty3";

        Admin admin = new Admin();
        admin.setEmail(emailA1);
        admin.setEncodedPassword(passwordEncoder.encode(passwordA1));
        admin.setUserRole(UserRoleType.ADMIN);
        admin.setUserStatus(UserStatus.ACTIVE);
        adminRepository.save(admin);

        // FINAL_1
        // Регистрируется пользователь m1

        ModeratorCreateDTO createDTO = testObjectsFactory.generateObject(ModeratorCreateDTO.class);
        createDTO.setEmail(emailM1);
        createDTO.setEncodedPassword(passwordM1);

        HttpEntity<?> httpEntity1 = new HttpEntity<>(createDTO, null);

        ModeratorReadDTO moderatorRead =
                performRequest(URL + "moderators",
                        HttpMethod.POST, httpEntity1, ModeratorReadDTO.class);
        Assert.assertEquals(emailM1, moderatorRead.getEmail());
        Assert.assertEquals(UserStatus.NEW, moderatorRead.getUserStatus());
        Assert.assertNull(moderatorRead.getUserRole());

        // FINAL_2
        // a1 дает m1 роль модератора

        HttpHeaders headersA1 = new HttpHeaders();
        headersA1.add("Authorization", getBasicAuthorizationHeaderValue(emailA1, passwordA1));
        HttpEntity<?> httpEntity2 = new HttpEntity<>(headersA1);

        ResponseEntity<PageResult<ModeratorReadDTO>> response21 =
                performRequest(URL + "moderators", HttpMethod.GET, httpEntity2,
                        PageResult.class, ModeratorReadDTO.class);

        PageResult<ModeratorReadDTO> actualPage = response21.getBody();
        ModeratorReadDTO readDTO21 = actualPage.getData().get(0);

        Assert.assertEquals(UserStatus.NEW, readDTO21.getUserStatus());
        UUID moderatorId = readDTO21.getId();

        ModeratorPutDTO put = new ModeratorPutDTO();
        put.setUserRole(UserRoleType.MODERATOR);
        put.setUserStatus(UserStatus.ACTIVE);
        HttpEntity<?> httpEntity22 = new HttpEntity<>(put, headersA1);

        ModeratorReadDTO moderatorRead22 =
                performRequest(URL + "moderators/" + moderatorId,
                        HttpMethod.PUT, httpEntity22, ModeratorReadDTO.class);
        Assert.assertEquals(emailM1, moderatorRead22.getEmail());
        Assert.assertEquals(UserStatus.ACTIVE, moderatorRead22.getUserStatus());
        Assert.assertEquals(UserRoleType.MODERATOR, moderatorRead22.getUserRole());

        // FINAL_3
        // Регистрируется пользователь c1

        ContentManagerCreateDTO createCm = testObjectsFactory.generateObject(ContentManagerCreateDTO.class);
        createCm.setEmail(emailCm1);
        createCm.setEncodedPassword(passwordCm1);

        HttpEntity<?> httpEntity3 = new HttpEntity<>(createCm, null);

        ContentManagerReadDTO cmRead =
                performRequest(URL + "content-managers",
                        HttpMethod.POST, httpEntity3, ContentManagerReadDTO.class);
        Assert.assertEquals(emailCm1, cmRead.getEmail());
        Assert.assertEquals(UserStatus.NEW, cmRead.getUserStatus());
        Assert.assertNull(cmRead.getUserRole());

        // FINAL_4
        // a1 дает c1 роль контент менеджера

        HttpEntity<?> httpEntity4 = new HttpEntity<>(headersA1);

        ResponseEntity<PageResult<ContentManagerReadDTO>> response41 =
                performRequest(URL + "content-managers", HttpMethod.GET, httpEntity4,
                        PageResult.class, ContentManagerReadDTO.class);

        PageResult<ContentManagerReadDTO> actualPageCm = response41.getBody();
        ContentManagerReadDTO readDTO41 = actualPageCm.getData().get(0);
        Assert.assertEquals(UserStatus.NEW, readDTO41.getUserStatus());
        UUID cmId = readDTO41.getId();

        ContentManagerPutDTO putCm = new ContentManagerPutDTO();
        putCm.setUserRole(UserRoleType.CONTENT_MANAGER);
        putCm.setUserStatus(UserStatus.ACTIVE);
        HttpEntity<?> httpEntity42 = new HttpEntity<>(putCm, headersA1);

        ContentManagerReadDTO cmRead42 =
                performRequest(URL + "content-managers/" + cmId,
                        HttpMethod.PUT, httpEntity42, ContentManagerReadDTO.class);
        Assert.assertEquals(emailCm1, cmRead42.getEmail());
        Assert.assertEquals(UserStatus.ACTIVE, cmRead42.getUserStatus());
        Assert.assertEquals(UserRoleType.CONTENT_MANAGER, cmRead42.getUserRole());

        // FINAL_5
        // Регистрируются 3 обычных юзеров. Мужчины: u1, u2 и женщина u3

        RegisteredUserCreateDTO createUser = testObjectsFactory.generateObject(RegisteredUserCreateDTO.class);
        createUser.setEmail(emailUser1);
        createUser.setEncodedPassword(passwordUser1);
        createUser.setSex(Sex.MALE);

        HttpEntity<?> httpEntity5 = new HttpEntity<>(createUser, null);

        RegisteredUserReadDTO userRead51 =
                performRequest(URL + "registered-users",
                        HttpMethod.POST, httpEntity5, RegisteredUserReadDTO.class);
        Assert.assertEquals(emailUser1, userRead51.getEmail());
        Assert.assertEquals(UserStatus.ACTIVE, userRead51.getUserStatus());
        Assert.assertEquals(UserRoleType.REGISTERED_USER, userRead51.getUserRole());
        Assert.assertEquals(Sex.MALE, userRead51.getSex());

        createUser = testObjectsFactory.generateObject(RegisteredUserCreateDTO.class);
        createUser.setEmail(emailUser2);
        createUser.setEncodedPassword(passwordUser2);
        createUser.setSex(Sex.MALE);

        HttpEntity<?> httpEntity52 = new HttpEntity<>(createUser, null);

        RegisteredUserReadDTO userRead52 =
                performRequest(URL + "registered-users",
                        HttpMethod.POST, httpEntity52, RegisteredUserReadDTO.class);
        Assert.assertEquals(userRead52.getEmail(), emailUser2);
        Assert.assertEquals(userRead52.getUserStatus(), UserStatus.ACTIVE);
        Assert.assertEquals(userRead52.getUserRole(), UserRoleType.REGISTERED_USER);
        Assert.assertEquals(userRead52.getSex(), Sex.MALE);

        createUser = testObjectsFactory.generateObject(RegisteredUserCreateDTO.class);
        createUser.setEmail(emailUser3);
        createUser.setEncodedPassword(passwordUser3);
        createUser.setSex(Sex.FEMALE);

        HttpEntity<?> httpEntity53 = new HttpEntity<>(createUser, null);

        RegisteredUserReadDTO userRead53 =
                performRequest(URL + "registered-users",
                        HttpMethod.POST, httpEntity53, RegisteredUserReadDTO.class);
        Assert.assertEquals(emailUser3, userRead53.getEmail());
        Assert.assertEquals(UserStatus.ACTIVE, userRead53.getUserStatus());
        Assert.assertEquals(UserRoleType.REGISTERED_USER, userRead53.getUserRole());
        Assert.assertEquals(Sex.FEMALE, userRead53.getSex());

        // FINAL_6
        // u2 исправляет имя в своем профиле

        UUID regUserId2 = userRead52.getId();

        HttpHeaders headersUser2 = new HttpHeaders();
        headersUser2.add("Authorization", getBasicAuthorizationHeaderValue(emailUser2, passwordUser2));

        RegisteredUserPutDTO put6 = testObjectsFactory.generateObject(RegisteredUserPutDTO.class);
        put6.setProfileName("New name");
        put6.setUserRole(UserRoleType.REGISTERED_USER);
        HttpEntity<?> httpEntity6 = new HttpEntity<>(put6, headersUser2);

        RegisteredUserReadDTO regUserRead6 =
                performRequest(URL + "registered-users/" + regUserId2,
                        HttpMethod.PUT, httpEntity6, RegisteredUserReadDTO.class);
        Assert.assertEquals(put6.getProfileName(), regUserRead6.getProfileName());

        // FINAL_7
        // c1 создает фильм, персонажей, актеров, crew и т.д.

        String[] personNames = new String[6];
        LocalDate[] datesBirth = new LocalDate[6];
        String[] biographyFiles = new String[6];
        Country[] countries = new Country[6];
        String[] placesOfBirth = new String[6];
        String[] education = new String[6];
        Sex[] sex = new Sex[6];

        parseDataFile("Person", "personName", personNames);
        parseDataFile("Person", "dateBirth", datesBirth);
        parseDataFile("Person", "biographyFile", biographyFiles);
        parseDataFile("Person", "country", countries);
        parseDataFile("Person", "placeOfBirth", placesOfBirth);
        parseDataFile("Person", "education", education);
        parseDataFile("Person", "sex", sex);

        HttpHeaders headersCm1 = new HttpHeaders();
        headersCm1.add("Authorization", getBasicAuthorizationHeaderValue(emailCm1, passwordCm1));

        PersonCreateDTO personCreate;
        PersonReadDTO[] personsRead = new PersonReadDTO[6];
        HttpEntity<?> httpEntity71;
        for (int i = 0; i < 6; i++) {
            personCreate = new PersonCreateDTO();
            personCreate.setName(personNames[i]);
            personCreate.setDateBirth(datesBirth[i]);
            personCreate.setBiography(readFileContent(biographyFiles[i]));
            personCreate.setCountry(countries[i]);
            personCreate.setPlaceBirth(placesOfBirth[i]);
            personCreate.setEducation(education[i]);
            personCreate.setSex(sex[i]);

            httpEntity71 = new HttpEntity<>(personCreate, headersCm1);

            PersonReadDTO personReadDTO71 =
                    performRequest(URL + "persons",
                            HttpMethod.POST, httpEntity71, PersonReadDTO.class);
            Assertions.assertThat(personCreate).isEqualToComparingFieldByField(personReadDTO71);
            personsRead[i] = personReadDTO71;
        }
        DepartmentType[] departmentsType = {DepartmentType.PRODUCTION, DepartmentType.DIRECTING, DepartmentType.SOUND};

        DepartmentCreateDTO departmentCreate;
        DepartmentReadDTO[] departmentsRead = new DepartmentReadDTO[3];
        HttpEntity<?> httpEntity72;
        for (int i = 0; i < 3; i++) {
            departmentCreate = new DepartmentCreateDTO();
            departmentCreate.setDepartmentType(departmentsType[i]);
            httpEntity72 = new HttpEntity<>(departmentCreate, headersCm1);

            DepartmentReadDTO departmentReadDTO72 =
                    performRequest(URL + "departments",
                            HttpMethod.POST, httpEntity72, DepartmentReadDTO.class);
            Assert.assertEquals(departmentCreate.getDepartmentType(), departmentReadDTO72.getDepartmentType());
            departmentsRead[i] = departmentReadDTO72;
        }

        CrewMemberType[] crewMembersType = {CrewMemberType.DIRECTOR, CrewMemberType.DIRECTOR, CrewMemberType.COMPOSER};

        CrewMemberCreateDTO crewMemberCreate;
        CrewMemberReadDTO[] crewMembersRead = new CrewMemberReadDTO[3];
        HttpEntity<?> httpEntity73;
        for (int i = 0; i < 3; i++) {
            crewMemberCreate = new CrewMemberCreateDTO();
            crewMemberCreate.setCrewMemberType(crewMembersType[i]);
            crewMemberCreate.setPersonId(personsRead[i + 3].getId());
            crewMemberCreate.setDepartmentId(departmentsRead[i].getId());
            httpEntity73 = new HttpEntity<>(crewMemberCreate, headersCm1);

            CrewMemberReadDTO crewMemberReadDTO73 =
                    performRequest(URL + "crew-members",
                            HttpMethod.POST, httpEntity73, CrewMemberReadDTO.class);
            Assertions.assertThat(crewMemberCreate).isEqualToComparingFieldByField(crewMemberReadDTO73);
            crewMembersRead[i] = crewMemberReadDTO73;
        }

        FilmCreateDTO filmCreate = new FilmCreateDTO();
        filmCreate.setTitle("The Matrix");
        filmCreate.setText("Is about a Virtual Reality");
        filmCreate.setDescription(readFileContent("Matrix"));
        filmCreate.setBudget(63000);
        filmCreate.setStatus(FilmStatus.RELEASED);
        filmCreate.setDatePrime(LocalDate.of(1999, 3, 21));
        filmCreate.setForecastRating(null);
        filmCreate.setAverageRating(null);
        HttpEntity<?> httpEntity74 = new HttpEntity<>(filmCreate, headersCm1);

        FilmReadDTO filmReadDTO74 =
                performRequest(URL + "films",
                        HttpMethod.POST, httpEntity74, FilmReadDTO.class);
        Assertions.assertThat(filmCreate).isEqualToComparingFieldByField(filmReadDTO74);

        ResponseEntity<List<GenreReadDTO>> response75 =
                performRequest(URL + "genres", HttpMethod.GET, httpEntity74,
                        ArrayList.class, GenreReadDTO.class);

        List<GenreReadDTO> genresReadDTO75 = response75.getBody();
        UUID genreId = genresReadDTO75.stream().filter(u -> u.getName().equals(FilmGenre.SCI_FI))
                .findFirst().get().getId();

        ResponseEntity<List<GenreReadDTO>> response76 =
                performRequest(URL + "film/" + filmReadDTO74.getId()
                                + "/genres/" + genreId, HttpMethod.POST, httpEntity74,
                        ArrayList.class, GenreReadDTO.class);

        List<GenreReadDTO> genresReadDTO76 = response76.getBody();
        Assert.assertEquals(FilmGenre.SCI_FI, genresReadDTO76.get(0).getName());

        ActorCreateDTO actorCreate;
        ActorReadDTO[] actorsRead = new ActorReadDTO[3];
        for (int i = 0; i < 3; i++) {
            actorCreate = testObjectsFactory.generateObject(ActorCreateDTO.class);
            actorCreate.setPersonId(personsRead[i].getId());
            HttpEntity<?> httpEntity77 = new HttpEntity<>(actorCreate, headersCm1);

            ActorReadDTO actorReadDTO77 =
                    performRequest(URL + "actors",
                            HttpMethod.POST, httpEntity77, ActorReadDTO.class);
            Assert.assertEquals(actorCreate.getPersonId(), actorReadDTO77.getPersonId());
            actorsRead[i] = actorReadDTO77;
        }

        String[] rolesText = {"Neo", "Morpheus", "Trinity"};

        RoleCreateDTO roleCreate;
        for (int i = 0; i < 3; i++) {
            roleCreate = new RoleCreateDTO();
            roleCreate.setText(rolesText[i]);
            roleCreate.setActorId(actorsRead[i].getId());
            roleCreate.setFilmId(filmReadDTO74.getId());
            HttpEntity<?> httpEntity78 = new HttpEntity<>(roleCreate, headersCm1);

            RoleReadDTO roleReadDTO77 =
                    performRequest(URL + "roles",
                            HttpMethod.POST, httpEntity78, RoleReadDTO.class);
            Assertions.assertThat(roleCreate).isEqualToComparingFieldByField(roleReadDTO77);
        }

        // FINAL_8
        // c1 создает новость о фильме

        NewsCreateDTO newsCreate = new NewsCreateDTO();
        newsCreate.setTitle("About Matrix");
        newsCreate.setText("Brad Pitt rev_als he turned down 'The Matrix'");
        newsCreate.setNewsObjectId(filmReadDTO74.getId());
        newsCreate.setNewsType(NewsType.FILM);
        newsCreate.setCntLike(0);
        newsCreate.setCntDislike(0);
        HttpEntity<?> httpEntity8 = new HttpEntity<>(newsCreate, headersCm1);

        NewsReadDTO newsReadDTO8 =
                performRequest(URL + "news/",
                        HttpMethod.POST, httpEntity8, NewsReadDTO.class);
        Assertions.assertThat(newsCreate).isEqualToComparingFieldByField(newsReadDTO8);
        UUID newsId = newsReadDTO8.getId();

        // FINAL_9
        // u1 просматривает контент новости

        HttpHeaders headersUser1 = new HttpHeaders();
        headersUser1.add("Authorization", getBasicAuthorizationHeaderValue(emailUser1, passwordUser1));
        HttpEntity<?> httpEntity9 = new HttpEntity<>(headersUser1);

        Thread.sleep(3000);
        NewsReadDTO newsReadDTO9 =
                performRequest(URL + "news/" + newsReadDTO8.getId(),
                        HttpMethod.GET, httpEntity9, NewsReadDTO.class);
        Assertions.assertThat(newsCreate).isEqualToComparingFieldByField(newsReadDTO9);
        newsId = newsReadDTO9.getId();

        // FINAL_10
        // u1 и u2 отмечают, что новость им нравится

        UUID regUserId = userRead51.getId();

        NewsLikeDislikeCreateDTO newsLikeDislikeCreate = new NewsLikeDislikeCreateDTO();
        newsLikeDislikeCreate.setNewsId(newsId);
        newsLikeDislikeCreate.setIsLike(true);

        HttpEntity<?> httpEntity10 = new HttpEntity<>(newsLikeDislikeCreate, headersUser1);

        NewsLikeDislikeReadDTO newsReadDTO10 =
                performRequest(URL + "registered-user/" + regUserId + "/news-like-dislike",
                        HttpMethod.POST, httpEntity10, NewsLikeDislikeReadDTO.class);
        Assert.assertEquals(newsLikeDislikeCreate.getNewsId(), newsReadDTO10.getNewsId());
        Assert.assertEquals(newsLikeDislikeCreate.getIsLike(), newsReadDTO10.getIsLike());

        Thread.sleep(3000);

        HttpEntity<?> httpEntity101 = new HttpEntity<>(headersUser1);

        NewsReadDTO newsReadDTO101 =
                performRequest(URL + "/news/" + newsReadDTO10.getNewsId(),
                        HttpMethod.GET, httpEntity101, NewsReadDTO.class);
        Assert.assertEquals(Integer.valueOf(1), newsReadDTO101.getCntLike());
        Assert.assertEquals(Integer.valueOf(0), newsReadDTO101.getCntDislike());

        HttpEntity<?> httpEntity102 = new HttpEntity<>(newsLikeDislikeCreate, headersUser2);

        NewsLikeDislikeReadDTO newsReadDTO102 =
                performRequest(URL + "registered-user/" + regUserId2 + "/news-like-dislike",
                        HttpMethod.POST, httpEntity102, NewsLikeDislikeReadDTO.class);
        Assert.assertEquals(newsLikeDislikeCreate.getNewsId(), newsReadDTO102.getNewsId());
        Assert.assertEquals(newsLikeDislikeCreate.getIsLike(), newsReadDTO102.getIsLike());

        Thread.sleep(3000);

        HttpEntity<?> httpEntity103 = new HttpEntity<>(headersUser2);

        ResponseEntity<NewsReadDTO> response103 =
                restTemplate.exchange(URL + "/news/" + newsReadDTO10.getNewsId(),
                        HttpMethod.GET, httpEntity103, new ParameterizedTypeReference<>() {
                        });
        Assert.assertEquals(HttpStatus.OK, response103.getStatusCode());
        NewsReadDTO newsReadDTO103 = response103.getBody();
        Assert.assertEquals(Integer.valueOf(2), newsReadDTO103.getCntLike());
        Assert.assertEquals(Integer.valueOf(0), newsReadDTO103.getCntDislike());

        // FINAL_11
        // Пользователь u3 - ошибочно отмечает, что новость ей нравится, а потом отменяет оценку

        UUID regUserId3 = userRead53.getId();

        newsLikeDislikeCreate = new NewsLikeDislikeCreateDTO();
        newsLikeDislikeCreate.setNewsId(newsReadDTO9.getId());
        newsLikeDislikeCreate.setIsLike(true);

        HttpHeaders headersUser3 = new HttpHeaders();
        headersUser3.add("Authorization", getBasicAuthorizationHeaderValue(emailUser3, passwordUser3));
        HttpEntity<?> httpEntity11 = new HttpEntity<>(newsLikeDislikeCreate, headersUser3);

        NewsLikeDislikeReadDTO newsReadDTO11 =
                performRequest(URL + "registered-user/" + regUserId3 + "/news-like-dislike",
                        HttpMethod.POST, httpEntity11, NewsLikeDislikeReadDTO.class);
        Assert.assertEquals(newsLikeDislikeCreate.getNewsId(), newsReadDTO11.getNewsId());
        Assert.assertEquals(newsLikeDislikeCreate.getIsLike(), newsReadDTO11.getIsLike());

        Thread.sleep(3000);

        HttpEntity<?> httpEntity112 = new HttpEntity<>(headersUser3);

        NewsReadDTO newsReadDTO112 =
                performRequest(URL + "/news/" + newsReadDTO10.getNewsId(),
                        HttpMethod.GET, httpEntity112, NewsReadDTO.class);
        Assert.assertEquals(Integer.valueOf(3), newsReadDTO112.getCntLike());
        Assert.assertEquals(Integer.valueOf(0), newsReadDTO112.getCntDislike());

        ResponseEntity response113 =
                restTemplate.exchange(URL + "registered-user/" + regUserId3
                                + "/news-like-dislike/" + newsReadDTO11.getId(),
                        HttpMethod.DELETE, httpEntity112, new ParameterizedTypeReference<>() {
                        });
        Assert.assertEquals(HttpStatus.OK, response113.getStatusCode());

        Thread.sleep(3000);

        NewsReadDTO newsReadDTO114 =
                performRequest(URL + "/news/" + newsReadDTO10.getNewsId(),
                        HttpMethod.GET, httpEntity112, NewsReadDTO.class);
        Assert.assertEquals(Integer.valueOf(2), newsReadDTO114.getCntLike());
        Assert.assertEquals(Integer.valueOf(0), newsReadDTO114.getCntDislike());

        // FINAL_12
        // Пользователь u3 - отмечает, что новость ей не нравится

        newsLikeDislikeCreate = new NewsLikeDislikeCreateDTO();
        newsLikeDislikeCreate.setNewsId(newsReadDTO9.getId());
        newsLikeDislikeCreate.setIsLike(false);

        HttpEntity<?> httpEntity12 = new HttpEntity<>(newsLikeDislikeCreate, headersUser3);

        NewsLikeDislikeReadDTO newsReadDTO12 =
                performRequest(URL + "registered-user/" + regUserId3 + "/news-like-dislike",
                        HttpMethod.POST, httpEntity12, NewsLikeDislikeReadDTO.class);
        Assert.assertEquals(newsLikeDislikeCreate.getNewsId(), newsReadDTO12.getNewsId());
        Assert.assertEquals(newsLikeDislikeCreate.getIsLike(), newsReadDTO12.getIsLike());

        Thread.sleep(3000);

        HttpEntity<?> httpEntity122 = new HttpEntity<>(headersUser3);

        NewsReadDTO newsReadDTO122 =
                performRequest(URL + "/news/" + newsReadDTO10.getNewsId(),
                        HttpMethod.GET, httpEntity122, NewsReadDTO.class);
        Assert.assertEquals(Integer.valueOf(2), newsReadDTO122.getCntLike());
        Assert.assertEquals(Integer.valueOf(1), newsReadDTO122.getCntDislike());

        // FINAL_13
        // u3 помечает слово в новости, что в нем есть ошибка, без указания правильного варианта

        MisprintInfoNewsCreateDTO mpNewsCreate = new MisprintInfoNewsCreateDTO();
        mpNewsCreate.setStartIndex(10);
        mpNewsCreate.setEndIndex(17);
        mpNewsCreate.setIncorrectText("rev_als");
        mpNewsCreate.setContentId(newsReadDTO9.getId());
        mpNewsCreate.setObject(MisprintObject.NEWS);

        HttpEntity<?> httpEntity13 = new HttpEntity<>(mpNewsCreate, headersUser3);

        MisprintInfoNewsReadDTO mpNewsReadDTO13 =
                performRequest(URL + "registered-user/" + regUserId3 + "/misprint-info-news",
                        HttpMethod.POST, httpEntity13, MisprintInfoNewsReadDTO.class);
        Assertions.assertThat(mpNewsCreate).isEqualToIgnoringGivenFields(mpNewsReadDTO13, "status");
        Assert.assertEquals(MisprintStatus.NEED_TO_FIX, mpNewsReadDTO13.getStatus());

        // FINAL_14
        // с1 просматривает сигналы от пользователей и замечает новый сигнал

        HttpEntity<?> httpEntity14 = new HttpEntity<>(headersCm1);

        ResponseEntity<List<MisprintInfoNewsReadDTO>> response14 =
                performRequest(URL + "misprint-info-news/need-to-fix",
                        HttpMethod.GET, httpEntity14,
                        ArrayList.class, MisprintInfoNewsReadDTO.class);

        List<MisprintInfoNewsReadDTO> mpInfoNewsReadDTO14 = response14.getBody();

        Assert.assertEquals(1, mpInfoNewsReadDTO14.size());
        MisprintInfoNewsReadDTO misprintInfoNewsReadDTO = mpInfoNewsReadDTO14.get(0);
        Assert.assertEquals(MisprintStatus.NEED_TO_FIX, misprintInfoNewsReadDTO.getStatus());
        UUID mpInfoNewsId = misprintInfoNewsReadDTO.getId();

        // FINAL_15
        // c1 начинает рассмотрение сигнала и исправляет ошибку

        String correctText = "reveals";
        MisprintInfoFixDTO fixDTO = new MisprintInfoFixDTO();
        fixDTO.setText(correctText);
        fixDTO.setStatus(MisprintStatus.FIXED);

        HttpEntity httpEntityFix = new HttpEntity<>(fixDTO, headersCm1);

        String url = URL + "content-manager/" + cmRead.getId() + "/misprints-info/" + mpInfoNewsId + "/fix";

        ResponseEntity<List<MisprintInfoFixReadDTO>> response15 =
                performRequest(url,
                        HttpMethod.POST, httpEntityFix,
                        ArrayList.class, MisprintInfoFixReadDTO.class);

        List<MisprintInfoFixReadDTO> mpNewsReadDTO15 = response15.getBody();
        Assert.assertEquals(1, mpNewsReadDTO15.size());
        MisprintInfoFixReadDTO mpNewsRead15 = mpNewsReadDTO15.get(0);
        Assert.assertEquals(MisprintStatus.FIXED, mpNewsRead15.getStatus());
        Assert.assertEquals(cmRead.getId(), mpNewsRead15.getContentManagerId());
        Assert.assertEquals(correctText, mpNewsRead15.getText());

        // FINAL_16
        // c1 просматривает сигналы от пользователей - теперь список сигналов пуст

        HttpEntity<?> httpEntity16 = new HttpEntity<>(headersCm1);

        ResponseEntity<List<MisprintInfoNewsReadDTO>> response16 =
                performRequest(URL + "misprint-info-news/need-to-fix",
                        HttpMethod.GET, httpEntity16,
                        ArrayList.class, MisprintInfoNewsReadDTO.class);

        List<MisprintInfoNewsReadDTO> mpInfoNewsReadDTO16 = response16.getBody();
        Assert.assertEquals(0, mpInfoNewsReadDTO16.size());

        // FINAL_17
        // Незарегистрированный пользователь просматривает контент новости и также видит,
        // что двоим она понравилась и одному не понравилась

        HttpEntity<?> httpEntity17 = new HttpEntity<>(null);

        NewsReadDTO newsReadDTO17 =
                performRequest(URL + "unregistered-user/news/" + newsReadDTO8.getId(),
                        HttpMethod.GET, httpEntity17, NewsReadDTO.class);
        Assertions.assertThat(newsCreate).isEqualToIgnoringGivenFields(newsReadDTO17, "text", "cntLike", "cntDislike");
        Assert.assertEquals("Brad Pitt reveals he turned down 'The Matrix'", newsReadDTO17.getText());
        Assert.assertEquals(Integer.valueOf(2), newsReadDTO17.getCntLike());
        Assert.assertEquals(Integer.valueOf(1), newsReadDTO17.getCntDislike());

        // FINAL_18
        // u1 просматривает фильм

        HttpEntity<?> httpEntity18 = new HttpEntity<>(headersUser1);

        ResponseEntity<PageResult<FilmReadDTO>> response18 =
                performRequest(URL + "films", HttpMethod.GET, httpEntity18,
                        PageResult.class, FilmReadDTO.class);

        PageResult<FilmReadDTO> actualPage18 = response18.getBody();
        Assert.assertEquals(actualPage18.getData().size(), 1);
        FilmReadDTO filmReadDTO18 = actualPage18.getData().get(0);
        Assertions.assertThat(filmCreate).isEqualToComparingFieldByField(filmReadDTO18);
        UUID filmId = filmReadDTO18.getId();

        // FINAL_19
        // u1 пишет отзыв на фильм и ставит высокую оценку. В отзыве один из фрагментов помечается как спойлер

        ReviewFilmCreateDTO reviewFilmCreate = testObjectsFactory.generateObject(ReviewFilmCreateDTO.class);
        reviewFilmCreate.setText(readFileContent("MatrixReview"));
        reviewFilmCreate.setRegisteredUserId(regUserId);
        reviewFilmCreate.setIsSpoiler(true);

        HttpEntity<?> httpEntity191 = new HttpEntity<>(reviewFilmCreate, headersUser1);

        ReviewFilmReadDTO reviewFilmReadDTO191 =
                performRequest(URL + "film/" + filmId + "/review-films",
                        HttpMethod.POST, httpEntity191, ReviewFilmReadDTO.class);
        Assertions.assertThat(reviewFilmCreate).isEqualToIgnoringGivenFields(reviewFilmReadDTO191, "status");
        Assert.assertEquals(ReviewStatus.NEED_TO_MODERATE, reviewFilmReadDTO191.getStatus());

        HttpEntity<?> httpEntity192 = new HttpEntity<>(headersUser1);

        ResponseEntity<List<ReviewFilmReadDTO>> response192 =
                performRequest(URL + "registered-user/" + regUserId + "/film/" + filmId + "/review-films",
                        HttpMethod.GET, httpEntity192,
                        ArrayList.class, ReviewFilmReadDTO.class);

        ReviewFilmReadDTO reviewFilmReadDTO192 = response192.getBody().get(0);
        Assertions.assertThat(reviewFilmCreate).isEqualToIgnoringGivenFields(reviewFilmReadDTO192, "status");
        Assert.assertEquals(ReviewStatus.NEED_TO_MODERATE, reviewFilmReadDTO192.getStatus());

        RatingFilmCreateDTO ratingFilmCreate = new RatingFilmCreateDTO();
        ratingFilmCreate.setRating(9.0);
        ratingFilmCreate.setRegisteredUserId(regUserId);

        HttpEntity<?> httpEntity193 = new HttpEntity<>(ratingFilmCreate, headersUser1);

        RatingFilmReadDTO ratingFilmReadDTO193 =
                performRequest(URL + "film/" + filmId + "/rating-films",
                        HttpMethod.POST, httpEntity193, RatingFilmReadDTO.class);
        Assert.assertEquals(ratingFilmCreate.getRating(), ratingFilmReadDTO193.getRating());
        Assert.assertEquals(ratingFilmCreate.getRegisteredUserId(), ratingFilmReadDTO193.getRegisteredUserId());
        Thread.sleep(3000);

        // FINAL_20
        // u2 просматривает фильм, отзыв не виден, т.к. он пока на модерации

        HttpEntity<?> httpEntity20 = new HttpEntity<>(headersUser2);

        FilmReadExtendedDTO filmReadExtendedDTO20 =
                performRequest(URL + "films/" + filmId + "/extended",
                        HttpMethod.GET, httpEntity20, FilmReadExtendedDTO.class);
        Assertions.assertThat(filmCreate).isEqualToIgnoringGivenFields(filmReadExtendedDTO20, "averageRating");
        Assert.assertEquals(9.0, filmReadExtendedDTO20.getRatingFilms().get(0).getRating(), Double.MIN_NORMAL);
        Assertions.assertThat(filmReadExtendedDTO20.getGenres()).extracting("name").
                containsExactlyInAnyOrder(FilmGenre.SCI_FI);
        Assert.assertEquals(Double.valueOf(9.0), filmReadExtendedDTO20.getAverageRating());
        Assert.assertEquals(0, filmReadExtendedDTO20.getReviewFilms().size());

        // FINAL_21
        // u2 ставит оценку фильму

        RatingFilmCreateDTO ratingFilmCreate21 = new RatingFilmCreateDTO();
        ratingFilmCreate21.setRating(8.0);
        ratingFilmCreate21.setRegisteredUserId(regUserId2);

        HttpEntity<?> httpEntity210 = new HttpEntity<>(ratingFilmCreate21, headersUser2);

        RatingFilmReadDTO ratingFilmReadDTO21 =
                performRequest(URL + "film/" + filmId + "/rating-films",
                        HttpMethod.POST, httpEntity210, RatingFilmReadDTO.class);
        Assert.assertEquals(ratingFilmCreate21.getRating(), ratingFilmReadDTO21.getRating());
        Assert.assertEquals(ratingFilmCreate21.getRegisteredUserId(), ratingFilmReadDTO21.getRegisteredUserId());
        Thread.sleep(3000);

        HttpEntity<?> httpEntity211 = new HttpEntity<>(headersUser2);

        ResponseEntity<List<RatingFilmReadDTO>> response211 =
                performRequest(URL + "film/" + filmId + "/rating-films",
                        HttpMethod.GET, httpEntity211,
                        ArrayList.class, RatingFilmReadDTO.class);

        List<RatingFilmReadDTO> ratingsFilmReadDTO211 = response211.getBody();
        List<RatingFilmReadDTO> ratingsFilmReadDTO211ofRegUser2 = ratingsFilmReadDTO211.stream()
                .filter(rf -> rf.getRegisteredUserId().equals(regUserId2))
                .collect(Collectors.toList());
        Assertions.assertThat(ratingsFilmReadDTO211ofRegUser2).extracting("rating").
                containsExactlyInAnyOrder(ratingFilmCreate21.getRating());

        // FINAL_22
        // m1 просматривает неподтвержденные отзывы и замечает один

        ReviewFilmFilter reviewFilmFilter = new ReviewFilmFilter();
        reviewFilmFilter.setStatus(ReviewStatus.NEED_TO_MODERATE);

        HttpHeaders headersM1 = new HttpHeaders();
        headersM1.add("Authorization", getBasicAuthorizationHeaderValue(emailM1, passwordM1));
        HttpEntity<?> httpEntity220 = new HttpEntity<>(headersM1);

        ResponseEntity<PageResult<ReviewFilmReadDTO>> response220 =
                performRequest(URL + "review-films?status=" + reviewFilmFilter.getStatus(),
                        HttpMethod.GET, httpEntity220,
                        PageResult.class, ReviewFilmReadDTO.class);

        PageResult<ReviewFilmReadDTO> actualPage220 = response220.getBody();
        ReviewFilmReadDTO reviewFilmReadDTO220 = actualPage220.getData().get(0);
        Assert.assertEquals(1, actualPage220.getData().size());
        Assert.assertEquals(reviewFilmReadDTO191.getText(), reviewFilmReadDTO220.getText());
        UUID reviewFilmId = reviewFilmReadDTO220.getId();

        // FINAL_23
        // m1 начинает просмотр отзыва и подтверждает, что он хороший.
        // Пользователь u1 помечается как заслуживающий доверия

        ReviewFilmPatchDTO patch = new ReviewFilmPatchDTO();
        patch.setText(null);
        patch.setStatus(ReviewStatus.MODERATED);
        HttpEntity<?> httpEntity23 = new HttpEntity<>(patch, headersM1);

        ResponseEntity<ReviewFilmReadDTO> response23 =
                restTemplate.exchange(URL + "review-films/" + reviewFilmId,
                        HttpMethod.PATCH, httpEntity23, new ParameterizedTypeReference<>() {
                        });
        Assert.assertEquals(HttpStatus.OK, response23.getStatusCode());
        ReviewFilmReadDTO reviewFilmRead23 =
                performRequest(URL + "review-films/" + reviewFilmId,
                        HttpMethod.GET, httpEntity23, ReviewFilmReadDTO.class);
        Assert.assertEquals(reviewFilmReadDTO191.getText(), reviewFilmRead23.getText());
        Assert.assertEquals(ReviewStatus.MODERATED, reviewFilmRead23.getStatus());
        Assert.assertEquals(true, reviewFilmRead23.getIsSpoiler());

        RegisteredUserPatchDTO patch23 = testObjectsFactory.createRegisteredUserPatchDTO();
        patch23.setTrustLevel(5);

        HttpEntity<?> httpEntity232 = new HttpEntity<>(patch23, headersM1);

        RegisteredUserReadDTO regUserRead232 =
                performRequest(URL + "moderator/registered-users/" + regUserId,
                        HttpMethod.PATCH, httpEntity232, RegisteredUserReadDTO.class);
        Assert.assertEquals(regUserId, regUserRead232.getId());
        Assert.assertEquals(Integer.valueOf(5), regUserRead232.getTrustLevel());

        // FINAL_24
        // u3 просматривает фильм и ставит оценку. Она видит подтвержденный отзыв

        HttpEntity<?> httpEntity24 = new HttpEntity<>(headersUser3);

        FilmReadExtendedDTO filmReadExtendedDTO24 =
                performRequest(URL + "films/" + filmId + "/extended",
                        HttpMethod.GET, httpEntity24, FilmReadExtendedDTO.class);
        Assertions.assertThat(filmCreate).isEqualToIgnoringGivenFields(filmReadExtendedDTO24, "averageRating");
        Assertions.assertThat(filmReadExtendedDTO24.getGenres()).extracting("name").
                containsExactlyInAnyOrder(FilmGenre.SCI_FI);
        Assert.assertEquals(1, filmReadExtendedDTO24.getReviewFilms().size());
        Assert.assertEquals(reviewFilmCreate.getText(), filmReadExtendedDTO24.getReviewFilms().get(0).getText());
        Assert.assertEquals(Double.valueOf(8.5), filmReadExtendedDTO24.getAverageRating());

        RatingFilmCreateDTO ratingFilmCreate24 = testObjectsFactory.generateObject(RatingFilmCreateDTO.class);
        ratingFilmCreate24.setRating(10.0);
        ratingFilmCreate24.setRegisteredUserId(regUserId3);

        HttpEntity<?> httpEntity242 = new HttpEntity<>(ratingFilmCreate24, headersUser3);

        RatingFilmReadDTO ratingFilmReadDTO242 =
                performRequest(URL + "film/" + filmId + "/rating-films",
                        HttpMethod.POST, httpEntity242, RatingFilmReadDTO.class);
        Assert.assertEquals(ratingFilmCreate24.getRating(), ratingFilmReadDTO242.getRating());
        Assert.assertEquals(ratingFilmCreate24.getRegisteredUserId(), ratingFilmReadDTO242.getRegisteredUserId());
        Thread.sleep(3000);

        HttpEntity<?> httpEntity243 = new HttpEntity<>(headersUser3);

        ResponseEntity<List<RatingFilmReadDTO>> response243 =
                performRequest(URL + "film/" + filmId + "/rating-films",
                        HttpMethod.GET, httpEntity243,
                        ArrayList.class, RatingFilmReadDTO.class);

        List<RatingFilmReadDTO> ratingsFilmReadDTO243 = response243.getBody();
        List<RatingFilmReadDTO> ratingsFilmReadDTO243ofRegUser3 = ratingsFilmReadDTO243.stream()
                .filter(rf -> rf.getRegisteredUserId().equals(regUserId3))
                .collect(Collectors.toList());
        Assertions.assertThat(ratingsFilmReadDTO243ofRegUser3).extracting("rating").
                containsExactlyInAnyOrder(ratingFilmCreate24.getRating());

        // FINAL_25
        // u3 помечает, что отзыв ей нравится

        ReviewFilmLikeDislikeCreateDTO reviewFilmLikeCreate = new ReviewFilmLikeDislikeCreateDTO();
        reviewFilmLikeCreate.setReviewFilmId(reviewFilmId);
        reviewFilmLikeCreate.setIsLike(true);

        HttpEntity<?> httpEntity25 = new HttpEntity<>(reviewFilmLikeCreate, headersUser3);

        ReviewFilmLikeDislikeReadDTO reviewFilmLikeDislikeReadDTO25 =
                performRequest(URL + "registered-user/" + regUserId3 + "/review-film-like-dislike",
                        HttpMethod.POST, httpEntity25, ReviewFilmLikeDislikeReadDTO.class);
        Assert.assertEquals(reviewFilmLikeCreate.getReviewFilmId(), reviewFilmLikeDislikeReadDTO25.getReviewFilmId());
        Assert.assertEquals(reviewFilmLikeCreate.getIsLike(), reviewFilmLikeDislikeReadDTO25.getIsLike());

        // FINAL_26
        // Незарегистрированный пользователь просматривает фильм и видит его среднюю оценку

        HttpEntity<?> httpEntity26 = new HttpEntity<>(null);

        FilmReadExtendedDTO filmReadExtendedDTO26 =
                performRequest(URL + "unregistered-user/films/" + filmId + "/extended",
                        HttpMethod.GET, httpEntity26, FilmReadExtendedDTO.class);
        Assertions.assertThat(filmCreate).isEqualToIgnoringGivenFields(filmReadExtendedDTO26, "averageRating");
        Assertions.assertThat(filmReadExtendedDTO26.getGenres()).extracting("name").
                containsExactlyInAnyOrder(FilmGenre.SCI_FI);
        Assert.assertEquals(1, filmReadExtendedDTO26.getReviewFilms().size());
        Assert.assertEquals(reviewFilmCreate.getText(), filmReadExtendedDTO26.getReviewFilms().get(0).getText());
        Assert.assertEquals(Double.valueOf(9.0), filmReadExtendedDTO26.getAverageRating());

        // FINAL_27
        // c1 импортирует другой фильм, такой, чтобы один из актеров играл в первом, персонажей, актеров, crew и тд.

        HttpEntity<?> httpEntity27 = new HttpEntity<>(headersCm1);
        String movieExternalId = "245891";

        UUID film2Id =
                performRequest(URL + "movies/import/" + movieExternalId,
                        HttpMethod.POST, httpEntity27, UUID.class);

        FilmReadDTO filmReadDTO272 =
                performRequest(URL + "films/" + film2Id,
                        HttpMethod.GET, httpEntity27, FilmReadDTO.class);
        Assert.assertEquals("John Wick", filmReadDTO272.getTitle());
        Assert.assertEquals("Ex-hitman John Wick comes out of retirement to track down the gangsters "
                + "that took everything from him.", filmReadDTO272.getDescription());
        Assert.assertEquals(Integer.valueOf(20000000), filmReadDTO272.getBudget());
        Assert.assertEquals(FilmStatus.RELEASED, filmReadDTO272.getStatus());
        Assert.assertEquals(LocalDate.of(2014, 10, 22), filmReadDTO272.getDatePrime());

        RoleFilter roleFilter = new RoleFilter();
        roleFilter.setPersonId(personsRead[0].getId());
        roleFilter.setFilmId(film2Id);

        HttpEntity<?> httpEntity273 = new HttpEntity<>(headersCm1);

        ResponseEntity<PageResult<RoleReadDTO>> response273 =
                performRequest(URL + "roles?personId=" + roleFilter.getPersonId()
                                + "&filmId=" + roleFilter.getFilmId(), HttpMethod.GET, httpEntity273,
                        PageResult.class, RoleReadDTO.class);

        PageResult<RoleReadDTO> actualPage273 = response273.getBody();
        RoleReadDTO roleReadDTO273 = actualPage273.getData().get(0);
        Assert.assertEquals("John Wick", roleReadDTO273.getText());

        // FINAL_28
        // u1 ставит фильму низкую оценку, и пишет гневный отзыв

        RatingFilmCreateDTO ratingFilmCreate28 = new RatingFilmCreateDTO();
        ratingFilmCreate28.setRating(3.0);
        ratingFilmCreate28.setRegisteredUserId(regUserId);

        HttpEntity<?> httpEntity28 = new HttpEntity<>(ratingFilmCreate28, headersUser1);

        RatingFilmReadDTO ratingFilmReadDTO28 =
                performRequest(URL + "film/" + film2Id + "/rating-films",
                        HttpMethod.POST, httpEntity28, RatingFilmReadDTO.class);
        Assert.assertEquals(ratingFilmCreate28.getRating(), ratingFilmReadDTO28.getRating());
        Assert.assertEquals(ratingFilmCreate28.getRegisteredUserId(), ratingFilmReadDTO28.getRegisteredUserId());
        Thread.sleep(3000);

        ReviewFilmCreateDTO reviewFilmCreate282 = testObjectsFactory.generateObject(ReviewFilmCreateDTO.class);
        reviewFilmCreate282.setText(readFileContent("JohnWickReview"));
        reviewFilmCreate282.setRegisteredUserId(regUserId);
        reviewFilmCreate282.setIsSpoiler(false);

        HttpEntity<?> httpEntity282 = new HttpEntity<>(reviewFilmCreate282, headersUser1);

        ReviewFilmReadDTO reviewFilmReadDTO282 =
                performRequest(URL + "film/" + film2Id + "/review-films",
                        HttpMethod.POST, httpEntity282, ReviewFilmReadDTO.class);
        Assertions.assertThat(reviewFilmCreate282).isEqualToIgnoringGivenFields(reviewFilmReadDTO282, "status");
        Assert.assertEquals(reviewFilmReadDTO282.getStatus(), ReviewStatus.MODERATED);

        // FINAL_29
        // u2, u3 оценивают фильм. Отзыв виден сразу. Оба отправляют сигнал модератору, что в отзыве есть маты

        RatingFilmCreateDTO ratingFilmCreate29 = new RatingFilmCreateDTO();
        ratingFilmCreate29.setRating(8.5);
        ratingFilmCreate29.setRegisteredUserId(regUserId2);

        HttpEntity<?> httpEntity29 = new HttpEntity<>(ratingFilmCreate29, headersUser2);

        RatingFilmReadDTO ratingFilmReadDTO29 =

                performRequest(URL + "film/" + film2Id + "/rating-films",
                        HttpMethod.POST, httpEntity29, RatingFilmReadDTO.class);
        Assert.assertEquals(ratingFilmCreate29.getRating(), ratingFilmReadDTO29.getRating());
        Assert.assertEquals(ratingFilmCreate29.getRegisteredUserId(), ratingFilmReadDTO29.getRegisteredUserId());
        Thread.sleep(3000);

        HttpEntity<?> httpEntity292 = new HttpEntity<>(headersUser2);

        ResponseEntity<List<ReviewFilmReadDTO>> response292 =
                performRequest(URL + "film/" + film2Id + "/review-films",
                        HttpMethod.GET, httpEntity292,
                        ArrayList.class, ReviewFilmReadDTO.class);

        List<ReviewFilmReadDTO> reviewsFilmReadDTO292 = response292.getBody();

        ReviewFilmReadDTO reviewFilmReadDTO292 = reviewsFilmReadDTO292.get(0);
        Assert.assertEquals(reviewFilmCreate282.getText(), reviewFilmReadDTO292.getText());
        Assert.assertEquals(ReviewStatus.MODERATED, reviewFilmReadDTO292.getStatus());
        UUID reviewFilm2Id = reviewFilmReadDTO292.getId();

        ProblemReviewCreateDTO problemReviewCreate293 = new ProblemReviewCreateDTO();
        problemReviewCreate293.setProblemType(ProblemType.FILTHY_LANGUAGE);
        problemReviewCreate293.setReviewFilmId(reviewFilm2Id);
        problemReviewCreate293.setReviewRoleId(null);

        HttpEntity<?> httpEntity293 = new HttpEntity<>(problemReviewCreate293, headersUser2);

        ProblemReviewReadDTO problemReviewReadDTO293 =
                performRequest(URL + "registered-user/" + regUserId2 + "/problem-reviews",
                        HttpMethod.POST, httpEntity293, ProblemReviewReadDTO.class);
        Assert.assertEquals(problemReviewCreate293.getProblemType(), problemReviewReadDTO293.getProblemType());
        Assert.assertEquals(problemReviewCreate293.getReviewFilmId(), problemReviewReadDTO293.getReviewFilmId());

        RatingFilmCreateDTO ratingFilmCreate294 = new RatingFilmCreateDTO();
        ratingFilmCreate294.setRating(9.5);
        ratingFilmCreate294.setRegisteredUserId(regUserId3);

        HttpEntity<?> httpEntity294 = new HttpEntity<>(ratingFilmCreate294, headersUser3);

        RatingFilmReadDTO ratingFilmReadDTO294 =
                performRequest(URL + "film/" + film2Id + "/rating-films",
                        HttpMethod.POST, httpEntity294, RatingFilmReadDTO.class);
        Assert.assertEquals(ratingFilmCreate294.getRating(), ratingFilmReadDTO294.getRating());
        Assert.assertEquals(ratingFilmCreate294.getRegisteredUserId(), ratingFilmReadDTO294.getRegisteredUserId());
        Thread.sleep(3000);

        HttpEntity<?> httpEntity295 = new HttpEntity<>(headersUser2);

        ResponseEntity<List<ReviewFilmReadDTO>> response295 =
                performRequest(URL + "film/" + film2Id + "/review-films",
                        HttpMethod.GET, httpEntity295,
                        ArrayList.class, ReviewFilmReadDTO.class);

        List<ReviewFilmReadDTO> reviewsFilmReadDTO295 = response295.getBody();
        ReviewFilmReadDTO reviewFilmReadDTO295 = reviewsFilmReadDTO295.get(0);
        Assert.assertEquals(reviewFilmCreate282.getText(), reviewFilmReadDTO295.getText());
        Assert.assertEquals(ReviewStatus.MODERATED, reviewFilmReadDTO295.getStatus());
        reviewFilm2Id = reviewFilmReadDTO295.getId();

        ProblemReviewCreateDTO problemReviewCreate296 = new ProblemReviewCreateDTO();
        problemReviewCreate296.setProblemType(ProblemType.FILTHY_LANGUAGE);
        problemReviewCreate296.setReviewFilmId(reviewFilm2Id);
        problemReviewCreate296.setReviewRoleId(null);

        HttpEntity<?> httpEntity296 = new HttpEntity<>(problemReviewCreate296, headersUser3);

        ProblemReviewReadDTO problemReviewReadDTO296 =
                performRequest(URL + "registered-user/" + regUserId3 + "/problem-reviews",
                        HttpMethod.POST, httpEntity296, ProblemReviewReadDTO.class);
        Assert.assertEquals(problemReviewCreate296.getProblemType(), problemReviewReadDTO296.getProblemType());
        Assert.assertEquals(problemReviewCreate296.getReviewFilmId(), problemReviewReadDTO296.getReviewFilmId());

        // FINAL_30
        // m1 просматривает список сигналов к нему, у видит 2 новых сигнала

        HttpEntity<?> httpEntity30 = new HttpEntity<>(headersM1);

        ResponseEntity<List<ProblemReviewReadDTO>> response30 =
                performRequest(URL + "problem-reviews",
                        HttpMethod.GET, httpEntity30,
                        ArrayList.class, ProblemReviewReadDTO.class);

        List<ProblemReviewReadDTO> problemReviewsReadDTO30 = response30.getBody();
        Assert.assertEquals(problemReviewsReadDTO30.size(), 2);
        ProblemReviewReadDTO problemReviewReadDTO1 = problemReviewsReadDTO30.get(0);
        ProblemReviewReadDTO problemReviewReadDTO2 = problemReviewsReadDTO30.get(1);
        Assert.assertEquals(ProblemType.FILTHY_LANGUAGE, problemReviewReadDTO1.getProblemType());
        Assert.assertEquals(ProblemType.FILTHY_LANGUAGE, problemReviewReadDTO2.getProblemType());
        reviewFilm2Id = problemReviewReadDTO1.getReviewFilmId();

        // FINAL_31
        // m1 начинает рассмотрение одного из сигналов. Видит маты, удаляет обзор и банит u1

        HttpEntity<?> httpEntity31 = new HttpEntity<>(headersM1);

        ReviewFilmReadDTO reviewFilmReadDTO31 =
                performRequest(URL + "review-films/" + reviewFilm2Id,
                        HttpMethod.GET, httpEntity31, ReviewFilmReadDTO.class);
        Assert.assertEquals(reviewFilmReadDTO31.getText(), reviewFilmCreate282.getText());

        HttpEntity<?> httpEntity312 = new HttpEntity<>(headersM1);

        ResponseEntity response312 =
                restTemplate.exchange(URL + "review-films/" + reviewFilm2Id,
                        HttpMethod.DELETE, httpEntity312, new ParameterizedTypeReference<>() {
                        });
        Assert.assertEquals(HttpStatus.OK, response312.getStatusCode());

        RegisteredUserPutDTO put313 = new RegisteredUserPutDTO();
        put313.setTrustLevel(0);
        put313.setUserStatus(UserStatus.BLOCKED);
        put313.setUserRole(UserRoleType.BLOCKED);

        HttpEntity<?> httpEntity313 = new HttpEntity<>(put313, headersM1);

        RegisteredUserReadDTO regUserRead313 =
                performRequest(URL + "moderator/registered-users/" + regUserId,
                        HttpMethod.PUT, httpEntity313, RegisteredUserReadDTO.class);
        Assert.assertEquals(regUserId, regUserRead313.getId());
        Assert.assertEquals(Integer.valueOf(0), regUserRead313.getTrustLevel());
        Assert.assertEquals(UserStatus.BLOCKED, regUserRead313.getUserStatus());

        // FINAL_32
        // m1 просматривает список сигналов к модераторам - теперь этот список пуст

        HttpEntity<?> httpEntity32 = new HttpEntity<>(headersM1);

        ResponseEntity<List<ProblemReviewReadDTO>> response32 =
                performRequest(URL + "problem-reviews",
                        HttpMethod.GET, httpEntity32,
                        ArrayList.class, ProblemReviewReadDTO.class);

        List<ProblemReviewReadDTO> problemReviewsReadDTO32 = response32.getBody();
        Assert.assertEquals(0, problemReviewsReadDTO32.size());

        // FINAL_33
        // u2, u3 оценивают роли актера, являющегося общим для двух фильмов. Роли оцениваются в 2 фильмах

        roleFilter = new RoleFilter();
        roleFilter.setPersonId(personsRead[0].getId());
        roleFilter.setFilmId(filmId);

        HttpEntity<?> httpEntity33 = new HttpEntity<>(headersUser2);

        ResponseEntity<PageResult<RoleReadDTO>> response33 =
                performRequest(URL + "roles?personId=" + roleFilter.getPersonId()
                                + "&filmId=" + roleFilter.getFilmId(), HttpMethod.GET, httpEntity33,
                        PageResult.class, RoleReadDTO.class);

        PageResult<RoleReadDTO> actualPage33 = response33.getBody();
        RoleReadDTO roleReadDTO33 = actualPage33.getData().get(0);
        Assert.assertEquals("Neo", roleReadDTO33.getText());
        UUID role1Id = roleReadDTO33.getId();

        RatingRoleCreateDTO ratingRoleCreate332 = new RatingRoleCreateDTO();
        ratingRoleCreate332.setRating(9.0);
        ratingRoleCreate332.setRegisteredUserId(regUserId2);

        HttpEntity<?> httpEntity332 = new HttpEntity<>(ratingRoleCreate332, headersUser2);

        RatingRoleReadDTO ratingRoleReadDTO332 =
                performRequest(URL + "role/" + role1Id + "/rating-roles",
                        HttpMethod.POST, httpEntity332, RatingRoleReadDTO.class);
        Assert.assertEquals(ratingRoleCreate332.getRating(), ratingRoleReadDTO332.getRating());
        Assert.assertEquals(ratingRoleCreate332.getRegisteredUserId(), ratingRoleReadDTO332.getRegisteredUserId());
        Thread.sleep(3000);

        roleFilter = new RoleFilter();
        roleFilter.setPersonId(personsRead[0].getId());
        roleFilter.setFilmId(film2Id);

        HttpEntity<?> httpEntity333 = new HttpEntity<>(headersUser3);

        ResponseEntity<PageResult<RoleReadDTO>> response333 =
                performRequest(URL + "roles?personId=" + roleFilter.getPersonId()
                                + "&filmId=" + roleFilter.getFilmId(), HttpMethod.GET, httpEntity333,
                        PageResult.class, RoleReadDTO.class);

        PageResult<RoleReadDTO> actualPage333 = response333.getBody();
        RoleReadDTO roleReadDTO333 = actualPage333.getData().get(0);
        Assert.assertEquals("John Wick", roleReadDTO333.getText());
        UUID role2Id = roleReadDTO333.getId();

        RatingRoleCreateDTO ratingRoleCreate334 = new RatingRoleCreateDTO();
        ratingRoleCreate334.setRating(8.0);
        ratingRoleCreate334.setRegisteredUserId(regUserId2);

        HttpEntity<?> httpEntity334 = new HttpEntity<>(ratingRoleCreate334, headersUser2);

        RatingRoleReadDTO ratingRoleReadDTO334 =
                performRequest(URL + "role/" + role2Id + "/rating-roles",
                        HttpMethod.POST, httpEntity334, RatingRoleReadDTO.class);
        Assert.assertEquals(ratingRoleReadDTO334.getRating(), ratingRoleCreate334.getRating());
        Assert.assertEquals(ratingRoleReadDTO334.getRegisteredUserId(), ratingRoleCreate334.getRegisteredUserId());
        Thread.sleep(3000);

        roleFilter = new RoleFilter();
        roleFilter.setPersonId(personsRead[0].getId());
        roleFilter.setFilmId(filmId);

        HttpEntity<?> httpEntity335 = new HttpEntity<>(headersUser3);

        ResponseEntity<PageResult<RoleReadDTO>> response335 =
                performRequest(URL + "roles?personId=" + roleFilter.getPersonId()
                                + "&filmId=" + roleFilter.getFilmId(), HttpMethod.GET, httpEntity335,
                        PageResult.class, RoleReadDTO.class);

        PageResult<RoleReadDTO> actualPage335 = response335.getBody();
        RoleReadDTO roleReadDTO335 = actualPage335.getData().get(0);
        Assert.assertEquals("Neo", roleReadDTO335.getText());
        role1Id = roleReadDTO335.getId();

        RatingRoleCreateDTO ratingRoleCreate336 = new RatingRoleCreateDTO();
        ratingRoleCreate336.setRating(10.0);
        ratingRoleCreate336.setRegisteredUserId(regUserId2);

        HttpEntity<?> httpEntity336 = new HttpEntity<>(ratingRoleCreate336, headersUser3);

        RatingRoleReadDTO ratingRoleReadDTO336 =
                performRequest(URL + "role/" + role1Id + "/rating-roles",
                        HttpMethod.POST, httpEntity336, RatingRoleReadDTO.class);
        Assert.assertEquals(ratingRoleCreate336.getRating(), ratingRoleReadDTO336.getRating());
        Assert.assertEquals(ratingRoleCreate336.getRegisteredUserId(), ratingRoleReadDTO336.getRegisteredUserId());
        Thread.sleep(3000);

        roleFilter = new RoleFilter();
        roleFilter.setPersonId(personsRead[0].getId());
        roleFilter.setFilmId(film2Id);

        HttpEntity<?> httpEntity337 = new HttpEntity<>(headersUser3);

        ResponseEntity<PageResult<RoleReadDTO>> response337 =
                performRequest(URL + "roles?personId=" + roleFilter.getPersonId()
                                + "&filmId=" + roleFilter.getFilmId(), HttpMethod.GET, httpEntity337,
                        PageResult.class, RoleReadDTO.class);

        PageResult<RoleReadDTO> actualPage337 = response337.getBody();
        RoleReadDTO roleReadDTO337 = actualPage337.getData().get(0);
        Assert.assertEquals("John Wick", roleReadDTO337.getText());
        role2Id = roleReadDTO337.getId();

        RatingRoleCreateDTO ratingRoleCreate338 = new RatingRoleCreateDTO();
        ratingRoleCreate338.setRating(7.0);
        ratingRoleCreate338.setRegisteredUserId(regUserId2);

        HttpEntity<?> httpEntity338 = new HttpEntity<>(ratingRoleCreate338, headersUser3);

        RatingRoleReadDTO ratingRoleReadDTO338 =
                performRequest(URL + "role/" + role2Id + "/rating-roles",
                        HttpMethod.POST, httpEntity338, RatingRoleReadDTO.class);

        Assert.assertEquals(ratingRoleCreate338.getRating(), ratingRoleReadDTO338.getRating());
        Assert.assertEquals(ratingRoleCreate338.getRegisteredUserId(), ratingRoleReadDTO338.getRegisteredUserId());
        Thread.sleep(3000);

        // FINAL_34
        // u1 пытается оценить роль актера, но запрос выдает ему ошибку 403, т.к. он забанен

        RatingRoleCreateDTO ratingRoleCreate34 = new RatingRoleCreateDTO();
        ratingRoleCreate34.setRating(2.0);
        ratingRoleCreate34.setRegisteredUserId(regUserId);

        HttpEntity<?> httpEntity34 = new HttpEntity<>(ratingRoleCreate34, headersUser1);
        String url34 = URL + "role/" + role2Id + "/rating-roles";

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(url34,
                HttpMethod.POST, httpEntity34, new ParameterizedTypeReference<PageResult<FilmReadDTO>>() {
                }))
                .isInstanceOf(HttpClientErrorException.class)
                .extracting("statusCode").isEqualTo(HttpStatus.FORBIDDEN);

        // FINAL_35
        // Незарегистрированный пользователь заходит на страницу актера, являющегося общим для 2 фильмов,
        // читает информацию о нем и видит среднюю оценку по 2 фильмам в которых он играл.
        // Также видит среднюю оценку по ролям этого актера

        HttpEntity<?> httpEntity35 = new HttpEntity<>(null);

        ActorReadExtendedDTO actorReadExtendedDTO35 =
                performRequest(URL + "unregistered-user/actors/" + actorsRead[0].getId() + "/extended",
                        HttpMethod.GET, httpEntity35, ActorReadExtendedDTO.class);
        Assert.assertEquals(personsRead[0].getName(), actorReadExtendedDTO35.getPerson().getName());
        Assert.assertEquals(personsRead[0].getBiography(), actorReadExtendedDTO35.getPerson().getBiography());
        Assert.assertEquals(Double.valueOf(8.5), actorReadExtendedDTO35.getAverageRating());
        Assert.assertEquals(Double.valueOf(8.0), actorReadExtendedDTO35.getAverageRatingByFilm());
    }

    private <T> T performRequest(String url, HttpMethod httpMethod, HttpEntity httpEntity, Class<T> itemClass) {
        ResponseEntity<T> response =
                restTemplate.exchange(url, httpMethod, httpEntity, itemClass);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        T read = response.getBody();
        return read;
    }

    private <T> ResponseEntity<T> performRequest(String url, HttpMethod httpMethod, HttpEntity httpEntity,
                                                 Class responseClass, Class innerResponseClass) {

        ParameterizedType parameterizedType = TypeUtils.parameterize(responseClass, innerResponseClass);
        ResponseEntity<T> response =
                restTemplate.exchange(url, httpMethod, httpEntity,
                        ParameterizedTypeReference.forType(parameterizedType));
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        return response;
    }

    private String getBasicAuthorizationHeaderValue(String userName, String password) {
        return "Basic " + new String(Base64.encodeBytes(String.format("%s:%s", userName, password).getBytes()));
    }

    private String readFileContent(String fileName) throws IOException {

        String dataFilePath = "src/test/resources/";
        fileName = dataFilePath + "datafilms/" + fileName + ".txt";

        String content = new String(Files.readAllBytes(Paths.get(fileName)));
        return content;
    }

    private <T> JSONObject parseDataFile(String fileName, String dateType, T[] parsingResult)
            throws IOException, org.json.simple.parser.ParseException {

        String dataFilePath = "src/test/resources/";
        fileName = dataFilePath + "datafilms/" + fileName + ".txt";
        JSONObject testObject = null;

        FileReader reader = new FileReader(fileName);
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
        testObject = (JSONObject) jsonObject;
        JSONArray jsonArray = (JSONArray) jsonObject.get("personsDate");
        Iterator<String> iterator = jsonArray.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            iterator.next();
            jsonObject = (JSONObject) jsonArray.get(i);
            if (dateType == "country") {
                parsingResult[i] = (T) convertStringToCountryType((String) jsonObject.get(dateType));
            } else if (dateType == "sex") {
                parsingResult[i] = (T) convertStringToSexType((String) jsonObject.get(dateType));
            } else if (dateType == "dateBirth") {
                parsingResult[i] = (T) convertStringToLocalDate((String) jsonObject.get(dateType));
            } else {
                parsingResult[i] = (T) jsonObject.get(dateType);
            }
            i++;
        }
        reader.close();
        return testObject;
    }

    private Country convertStringToCountryType(String country) {
        return Country.valueOf(country);
    }

    private Sex convertStringToSexType(String sex) {
        return Sex.valueOf(sex);
    }

    private LocalDate convertStringToLocalDate(String date) {
        DateTimeFormatter DATEFORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate ld = LocalDate.parse(date, DATEFORMATTER);
        return ld;
    }
}

