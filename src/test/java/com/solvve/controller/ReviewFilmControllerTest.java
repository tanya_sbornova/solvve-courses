package com.solvve.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.solvve.domain.PageResult;
import com.solvve.domain.ReviewFilm;
import com.solvve.domain.ReviewStatus;
import com.solvve.dto.reviewfilm.ReviewFilmFilter;
import com.solvve.dto.reviewfilm.ReviewFilmPatchDTO;
import com.solvve.dto.reviewfilm.ReviewFilmPutDTO;
import com.solvve.dto.reviewfilm.ReviewFilmReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.ReviewFilmService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({ReviewFilmController.class, TestObjectsFactoryController.class})
public class ReviewFilmControllerTest extends BaseControllerTest {

    @MockBean
    private ReviewFilmService reviewFilmService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetReviewFilm() throws Exception {

        ReviewFilmReadDTO reviewFilm = testObjectsFactoryController.createReviewFilmReadDTO();

        Mockito.when(reviewFilmService.getReviewFilm(reviewFilm.getId())).thenReturn(reviewFilm);

        String resultJson = mvc.perform(get("/api/v1/review-films/{id}", reviewFilm.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        ReviewFilmReadDTO actualReviewFilm = objectMapper.readValue(resultJson, ReviewFilmReadDTO.class);
        Assertions.assertThat(actualReviewFilm).isEqualToComparingFieldByField(reviewFilm);

        Mockito.verify(reviewFilmService).getReviewFilm(reviewFilm.getId());
    }

    @Test
    public void testGetReviewFilmWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(ReviewFilm.class, wrongId);
        Mockito.when(reviewFilmService.getReviewFilm(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/review-films/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testPatchReviewFilm() throws Exception {

        ReviewFilmPatchDTO patchDTO = new ReviewFilmPatchDTO();
        patchDTO.setText("The best");
        patchDTO.setStatus(ReviewStatus.MODERATED);

        ReviewFilmReadDTO read = testObjectsFactoryController.createReviewFilmReadDTO();

        Mockito.when(reviewFilmService.patchReviewFilm(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/review-films/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewFilmReadDTO actualReviewFilm = objectMapper.readValue(resultJson, ReviewFilmReadDTO.class);
        Assert.assertEquals(read, actualReviewFilm);
    }

    @Test
    public void testUpdateReviewFilm() throws Exception {

        ReviewFilmPutDTO putDTO = new ReviewFilmPutDTO();
        putDTO.setText("The best");

        ReviewFilmReadDTO read = testObjectsFactoryController.createReviewFilmReadDTO();

        Mockito.when(reviewFilmService.updateReviewFilm(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/review-films/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewFilmReadDTO actualReviewFilm = objectMapper.readValue(resultJson, ReviewFilmReadDTO.class);
        Assert.assertEquals(read, actualReviewFilm);
    }

    @Test
    public void testDeleteReviewFilm() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/review-films/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(reviewFilmService).deleteReviewFilm(id);
    }

    @Test
    public void testGetReviewFilmsWithPagingAndSorting() throws Exception {
        ReviewFilmFilter reviewFilmFilter = new ReviewFilmFilter();
        reviewFilmFilter.setStatus(ReviewStatus.FIXED);
        reviewFilmFilter.setIsSpoiler(false);
        reviewFilmFilter.setRegisteredUserId(UUID.randomUUID());
        reviewFilmFilter.setFilmId(UUID.randomUUID());

        ReviewFilmReadDTO read = testObjectsFactoryController.createReviewFilmReadDTO();

        int page = 1;
        int size = 25;

        PageResult<ReviewFilmReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(new Long(100));
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "email"));
        Mockito.when(reviewFilmService.getReviewFilms(reviewFilmFilter, pageRequest))
                .thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/review-films")
                .param("status", reviewFilmFilter.getStatus().toString())
                .param("isSpoiler", reviewFilmFilter.getIsSpoiler().toString())
                .param("registeredUserId", reviewFilmFilter.getRegisteredUserId().toString())
                .param("filmId", reviewFilmFilter.getFilmId().toString())
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "email,asc")
        )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<ReviewFilmReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {});
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetReviewFilmsWithBigPage() throws Exception {
        ReviewFilmFilter reviewFilmFilter = new ReviewFilmFilter();
        reviewFilmFilter.setStatus(ReviewStatus.FIXED);
        reviewFilmFilter.setIsSpoiler(false);
        reviewFilmFilter.setRegisteredUserId(UUID.randomUUID());
        reviewFilmFilter.setFilmId(UUID.randomUUID());

        ReviewFilmReadDTO read = testObjectsFactoryController.createReviewFilmReadDTO();

        int page = 0;
        int size = 999999;

        PageResult<ReviewFilmReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements(new Long(100));
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(page, maxPageSize, Sort.by(Sort.Direction.ASC, "email"));
        Mockito.when(reviewFilmService.getReviewFilms(reviewFilmFilter, pageRequest))
                .thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/review-films")
                .param("status", reviewFilmFilter.getStatus().toString())
                .param("isSpoiler", reviewFilmFilter.getIsSpoiler().toString())
                .param("registeredUserId", reviewFilmFilter.getRegisteredUserId().toString())
                .param("filmId", reviewFilmFilter.getFilmId().toString())
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "email,asc")
        )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PageResult<ReviewFilmReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {});
        Assert.assertEquals(resultPage, actualPage);
    }
}

