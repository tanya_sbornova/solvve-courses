package com.solvve.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.solvve.dto.crewmember.*;
import com.solvve.exception.handler.ErrorInfo;
import com.solvve.util.TestObjectsFactoryController;
import com.solvve.domain.CrewMember;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.CrewMemberService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({CrewMemberController.class, TestObjectsFactoryController.class})
public class CrewMemberControllerTest extends BaseControllerTest {

    @MockBean
    private CrewMemberService crewMemberService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetCrewMember() throws Exception {

        CrewMemberReadDTO crewMember = testObjectsFactoryController.createCrewMemberReadDTO();

        Mockito.when(crewMemberService.getCrewMember(crewMember.getId())).thenReturn(crewMember);

        String resultJson = mvc.perform(get("/api/v1/crew-members/{id}", crewMember.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewMemberReadDTO actualCrewMember = objectMapper.readValue(resultJson, CrewMemberReadDTO.class);
        Assertions.assertThat(actualCrewMember).isEqualToComparingFieldByField(crewMember);

        Mockito.verify(crewMemberService).getCrewMember(crewMember.getId());
    }

    @Test
    public void testGetCrewMemberWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(CrewMember.class, wrongId);
        Mockito.when(crewMemberService.getCrewMember(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/crew-members/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetCrewMembers() throws Exception {

        List<CrewMemberReadDTO> crewMembers = List.of(testObjectsFactoryController.createCrewMemberReadDTO(),
                testObjectsFactoryController.createCrewMemberReadDTO());

        Mockito.when(crewMemberService.getCrewMembers()).thenReturn(crewMembers);

        String resultJson = mvc.perform(get("/api/v1/crew-members"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewMemberReadDTO[] arrCrewMembers = objectMapper.readValue(resultJson, CrewMemberReadDTO[].class);
        Assertions.assertThat(crewMembers).containsExactlyInAnyOrder(arrCrewMembers);
    }

    @Test
    public void testGetActorCrewMembers() throws Exception {

        List<CrewMemberReadDTO> crewMembers = List.of(testObjectsFactoryController.createCrewMemberReadDTO(),
                testObjectsFactoryController.createCrewMemberReadDTO(),
                testObjectsFactoryController.createCrewMemberReadDTO());

        UUID departmentId = UUID.randomUUID();

        Mockito.when(crewMemberService.getDepartmentCrewMembers(departmentId)).thenReturn(crewMembers);

        String resultJson = mvc.perform(get("/api/v1/department/{departmentId}/crewMembers", departmentId.toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewMemberReadDTO[] arrCrewMembers = objectMapper.readValue(resultJson, CrewMemberReadDTO[].class);
        Assertions.assertThat(crewMembers).containsExactlyInAnyOrder(arrCrewMembers);
    }

    @Test
    public void testCreateCrewMember() throws Exception {

        UUID personId = UUID.randomUUID();

        CrewMemberCreateDTO create = testObjectsFactoryController.createCrewMemberCreateDTO();
        create.setPersonId(personId);

        CrewMemberReadDTO read = testObjectsFactoryController.createCrewMemberReadDTO();
        read.setPersonId(personId);

        Mockito.when(crewMemberService.createCrewMember(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/crew-members/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewMemberReadDTO actualCrewMember = objectMapper.readValue(resultJson, CrewMemberReadDTO.class);
        Assertions.assertThat(actualCrewMember).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testAddCrewMemberToFilm() throws Exception {

        UUID filmId = UUID.randomUUID();
        UUID crewMemberId = UUID.randomUUID();

        CrewMemberReadDTO read = testObjectsFactoryController.createCrewMemberReadDTO();
        List<CrewMemberReadDTO> expectedCrewMembers = List.of(read);

        Mockito.when(crewMemberService.addCrewMemberToFilm(filmId, crewMemberId)).thenReturn(expectedCrewMembers);

        String resultJson = mvc.perform(post("/api/v1/film/{filmId}/crew-members/{id}", filmId, crewMemberId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<CrewMemberReadDTO> actualCrewMembers = objectMapper.readValue(resultJson,
                new TypeReference<List<CrewMemberReadDTO>>() {
                });
        Assert.assertEquals(expectedCrewMembers, actualCrewMembers);
    }

    @Test
    public void testAddCrewMemberToDepartment() throws Exception {

        UUID departmentId = UUID.randomUUID();
        UUID crewMemberId = UUID.randomUUID();

        CrewMemberReadDTO read = testObjectsFactoryController.createCrewMemberReadDTO();
        List<CrewMemberReadDTO> expectedCrewMembers = List.of(read);

        Mockito.when(crewMemberService.addCrewMemberToDepartment(departmentId, crewMemberId))
                .thenReturn(expectedCrewMembers);

        String resultJson = mvc.perform(post("/api/v1/department/{departmentId}/crew-members/{id}",
                departmentId, crewMemberId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<CrewMemberReadDTO> actualCrewMembers = objectMapper.readValue(resultJson,
                new TypeReference<List<CrewMemberReadDTO>>() {
                });
        Assert.assertEquals(expectedCrewMembers, actualCrewMembers);
    }

    @Test
    public void testPatchCrewMember() throws Exception {

        CrewMemberPatchDTO patchDTO = testObjectsFactoryController.createCrewMemberPatchDTO();

        CrewMemberReadDTO read = testObjectsFactoryController.createCrewMemberReadDTO();

        Mockito.when(crewMemberService.patchCrewMember(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/crew-members/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewMemberReadDTO actualCrewMember = objectMapper.readValue(resultJson, CrewMemberReadDTO.class);
        Assert.assertEquals(read, actualCrewMember);
    }

    @Test
    public void testUpdateCrewMember() throws Exception {

        CrewMemberPutDTO putDTO = testObjectsFactoryController.createCrewMemberPutDTO();

        CrewMemberReadDTO read = testObjectsFactoryController.createCrewMemberReadDTO();

        Mockito.when(crewMemberService.updateCrewMember(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/crew-members/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewMemberReadDTO actualСrewMember = objectMapper.readValue(resultJson, CrewMemberReadDTO.class);
        Assert.assertEquals(read, actualСrewMember);
    }

    @Test
    public void testDeleteCrewMember() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/crew-members/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(crewMemberService).deleteCrewMember(id);
    }

    @Test
    public void testRemoveCrewMemberFromFilm() throws Exception {

        UUID filmId = UUID.randomUUID();
        UUID crewMemberId = UUID.randomUUID();

        List<CrewMemberReadDTO> expectedCrewMembers = List.of();

        Mockito.when(crewMemberService.removeCrewMemberFromFilm(filmId, crewMemberId)).thenReturn(expectedCrewMembers);

        String resultJson = mvc.perform(delete("/api/v1/film/{filmId}/crew-members/{id}", filmId, crewMemberId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<CrewMemberReadDTO> actualCrewMembers = objectMapper.readValue(resultJson,
                new TypeReference<List<CrewMemberReadDTO>>() {
                });
        Assert.assertEquals(expectedCrewMembers, actualCrewMembers);
    }

    @Test
    public void testRemoveCrewMemberFromDepartment() throws Exception {

        UUID departmentId = UUID.randomUUID();
        UUID crewMemberId = UUID.randomUUID();

        List<CrewMemberReadDTO> expectedCrewMembers = List.of();

        Mockito.when(crewMemberService.removeCrewMemberFromDepartment(departmentId, crewMemberId))
                .thenReturn(expectedCrewMembers);

        String resultJson = mvc.perform(delete("/api/v1/department/{departmentId}/crew-members/{id}",
                departmentId, crewMemberId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<CrewMemberReadDTO> actualCrewMembers = objectMapper.readValue(resultJson,
                new TypeReference<List<CrewMemberReadDTO>>() {
                });
        Assert.assertEquals(expectedCrewMembers, actualCrewMembers);
    }


    @Test
    public void testCreateCrewMemberValidationFailed() throws Exception {

        CrewMemberCreateDTO create = new CrewMemberCreateDTO();

        CrewMemberReadDTO read = testObjectsFactoryController.createCrewMemberReadDTO();

        Mockito.when(crewMemberService.createCrewMember(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/crew-members/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(crewMemberService, Mockito.never()).createCrewMember(ArgumentMatchers.any());
    }
}
