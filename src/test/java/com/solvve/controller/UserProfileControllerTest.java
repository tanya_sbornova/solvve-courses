package com.solvve.controller;

import com.solvve.domain.FilmGenre;
import com.solvve.domain.UserProfile;
import com.solvve.dto.userprofile.UserProfileCreateDTO;
import com.solvve.dto.userprofile.UserProfilePatchDTO;
import com.solvve.dto.userprofile.UserProfilePutDTO;
import com.solvve.dto.userprofile.UserProfileReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.UserProfileService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(UserProfileController.class)
public class UserProfileControllerTest extends BaseControllerTest {

    @MockBean
    private UserProfileService userProfileService;

    @Test
    public void testGetUserProfile() throws Exception {

        UserProfileReadDTO userProfile = createUserProfileRead();

        Mockito.when(userProfileService.getUserProfile(userProfile.getId())).thenReturn(userProfile);

        String resultJson = mvc.perform(get("/api/v1/user-profiles/{id}", userProfile.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        UserProfileReadDTO actualUserProfile = objectMapper.readValue(resultJson, UserProfileReadDTO.class);
        Assertions.assertThat(actualUserProfile).isEqualToComparingFieldByField(userProfile);

        Mockito.verify(userProfileService).getUserProfile(userProfile.getId());
    }

    @Test
    public void testGetUserProfileWrongId() throws Exception {

        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(UserProfile.class, wrongId);
        Mockito.when(userProfileService.getUserProfile(wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get("/api/v1/user-profiles/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateUserProfile() throws Exception {

        UserProfileCreateDTO create = new UserProfileCreateDTO();
        create.setRatingReviews(5.2);
        create.setRatingActivity(6.7);
        create.setFavouriteGenre(FilmGenre.ROMANCE);

        UserProfileReadDTO read = createUserProfileRead();

        Mockito.when(userProfileService.createUserProfile(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/user-profiles/")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        UserProfileReadDTO actualUserProfile =
                objectMapper.readValue(resultJson, UserProfileReadDTO.class);
        Assertions.assertThat(actualUserProfile).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchUserProfile() throws Exception {

        UserProfilePatchDTO patchDTO = new UserProfilePatchDTO();
        patchDTO.setRatingReviews(5.2);
        patchDTO.setRatingActivity(6.7);
        patchDTO.setFavouriteGenre(FilmGenre.ROMANCE);

        UserProfileReadDTO read = createUserProfileRead();

        Mockito.when(userProfileService.patchUserProfile(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/user-profiles/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        UserProfileReadDTO actualUserProfile = objectMapper.readValue(resultJson, UserProfileReadDTO.class);
        Assert.assertEquals(read, actualUserProfile);
    }

    @Test
    public void testUpdateUserProfile() throws Exception {

        UserProfilePutDTO putDTO = new UserProfilePutDTO();
        putDTO.setRatingReviews(5.2);
        putDTO.setRatingActivity(6.7);
        putDTO.setFavouriteGenre(FilmGenre.ROMANCE);

        UserProfileReadDTO read = createUserProfileRead();

        Mockito.when(userProfileService.updateUserProfile(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/user-profiles/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        UserProfileReadDTO actualUserProfile = objectMapper.readValue(resultJson, UserProfileReadDTO.class);
        Assert.assertEquals(read, actualUserProfile);
    }

    @Test
    public void testDeleteUserProfile() throws Exception {

        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/user-profiles/{id}", id.toString()))
                .andExpect(status().isOk());

        Mockito.verify(userProfileService).deleteUserProfile(id);
    }

    private UserProfileReadDTO createUserProfileRead() {
        UserProfileReadDTO read = new UserProfileReadDTO();
        read.setId(UUID.randomUUID());
        read.setRatingReviews(5.2);
        read.setRatingActivity(6.7);
        read.setFavouriteGenre(FilmGenre.ROMANCE);
        return read;
    }
}
