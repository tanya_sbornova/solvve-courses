package com.solvve.controller;

import com.solvve.domain.MisprintInfoNews;
import com.solvve.domain.RegisteredUser;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsReadDTO;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsCreateDTO;
import com.solvve.dto.registereduser.RegisteredUserReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.RegisteredUserMisprintInfoNewsService;
import com.solvve.util.TestObjectsFactoryController;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest({RegisteredUserMisprintInfoNewsController.class, TestObjectsFactoryController.class})
public class RegisteredUserMisprintInfoNewsControllerTest extends BaseControllerTest {

    @MockBean
    private RegisteredUserMisprintInfoNewsService registeredUserMisprintInfoNewsService;

    @Autowired
    private TestObjectsFactoryController testObjectsFactoryController;

    @Test
    public void testGetRegisteredMisprintInfoNewsLikes() throws Exception {

        List<MisprintInfoNewsReadDTO> misprintInfoNews = List.of(testObjectsFactoryController.
                createMisprintInfoNewsReadDTO());
        RegisteredUserReadDTO registeredUser = testObjectsFactoryController.createRegisteredUserReadDTO();

        Mockito.when(registeredUserMisprintInfoNewsService.getRegisteredUserMisprintInfoNews(registeredUser.getId())).
                thenReturn(misprintInfoNews);

        String resultJson = mvc.perform(get("/api/v1/registered-user/{registeredUserId}/misprint-info-news",
                registeredUser.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MisprintInfoNewsReadDTO[] arrMisprintInfoNews = objectMapper.readValue(resultJson,
                MisprintInfoNewsReadDTO[].class);
        Assertions.assertThat(misprintInfoNews).containsExactlyInAnyOrder(arrMisprintInfoNews);
    }

    @Test
    public void testGetRegisteredMisprintInfoNewsLike() throws Throwable {

        MisprintInfoNewsReadDTO misprintInfoNews = testObjectsFactoryController.createMisprintInfoNewsReadDTO();

        Mockito.when(registeredUserMisprintInfoNewsService.
                getRegisteredUserMisprintInfoNews(misprintInfoNews.getRegisteredUserId(),
                        misprintInfoNews.getId())).thenReturn(misprintInfoNews);

        String resultJson = mvc.perform(get(
                "/api/v1/registered-user/{registeredUserId}/misprint-info-news/{misprintInfoNewsId}",
                misprintInfoNews.getRegisteredUserId().toString(),
                misprintInfoNews.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MisprintInfoNewsReadDTO actualMisprintInfoNews = objectMapper.readValue(resultJson,
                MisprintInfoNewsReadDTO.class);
        Assertions.assertThat(actualMisprintInfoNews).isEqualToComparingFieldByField(misprintInfoNews);

        Mockito.verify(registeredUserMisprintInfoNewsService).getRegisteredUserMisprintInfoNews(misprintInfoNews.
                getRegisteredUserId(), misprintInfoNews.getId());
    }

    @Test
    public void testGetRegisteredMisprintInfoNewsLikeWrongId() throws Throwable {

        UUID wrongId = UUID.randomUUID();
        RegisteredUserReadDTO registeredUser = testObjectsFactoryController.createRegisteredUserReadDTO();

        EntityNotFoundException exception = new EntityNotFoundException(MisprintInfoNews.class, wrongId,
                RegisteredUser.class, registeredUser.getId());
        Mockito.when(registeredUserMisprintInfoNewsService.getRegisteredUserMisprintInfoNews(registeredUser.getId(),
                wrongId)).thenThrow(exception);
        String resultJson = mvc.perform(get(
                "/api/v1/registered-user/{registeredUserId}/misprint-info-news/{misprintInfoNewsId}",
                registeredUser.getId().toString(), wrongId.toString()))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateRegisteredMisprintInfoNewsLike() throws Exception {

        MisprintInfoNewsCreateDTO create = testObjectsFactoryController.createMisprintInfoNewsCreateDTO();
        MisprintInfoNewsReadDTO read = testObjectsFactoryController.createMisprintInfoNewsReadDTO();
        UUID registeredUserId = UUID.randomUUID();

        Mockito.when(registeredUserMisprintInfoNewsService.createRegisteredUserMisprintInfoNews(registeredUserId,
                create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/registered-user/{registeredUserId}/misprint-info-news",
                registeredUserId.toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        MisprintInfoNewsReadDTO actualMisprintInfoNews = objectMapper.readValue(resultJson,
                MisprintInfoNewsReadDTO.class);
        Assertions.assertThat(actualMisprintInfoNews).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testDeleteRegisteredMisprintInfoNewsLike() throws Exception {

        MisprintInfoNewsReadDTO read = testObjectsFactoryController.createMisprintInfoNewsReadDTO();
        RegisteredUserReadDTO registeredUserReadDTO = testObjectsFactoryController.createRegisteredUserReadDTO();

        mvc.perform(delete("/api/v1/registered-user/{registeredUserId}/misprint-info-news/{misprintInfoNewsId}",
                registeredUserReadDTO.getId().toString(), read.getId().toString()))
                .andExpect(status().isOk());

        Mockito.verify(registeredUserMisprintInfoNewsService).
                deleteRegisteredUserMisprintInfoNews(registeredUserReadDTO.getId(), read.getId());
    }
}
