package com.solvve.client.themoviedb;

import com.solvve.BaseTest;
import com.solvve.client.themoviedb.dto.*;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class TheMovieDbClientTest extends BaseTest {

    @Autowired
    private TheMovieDbClient theMovieDbClient;

    @Test
    public void testGetMovieRu() {
        String movieId = "280";
        MovieReadDTO movie = theMovieDbClient.getMovie(movieId, "ru");
        Assert.assertEquals("Terminator 2: Judgment Day", movie.getOriginalTitle());
        Assert.assertEquals("Терминатор 2: Судный день", movie.getTitle());
        Assert.assertEquals("Война роботов и людей продолжается. Казалось, что человечество "
                + "обречено на полное уничтожение. Но благодаря своему лидеру Джону Коннору "
                + "у сопротивления появляется шанс победить. Не имея возможности убить Джона "
                + "в реальном времени, роботы отправляют в прошлое свою самую совершенную разработку "
                + "— терминатора-убийцу из жидкого металла, способного принимать любое обличье.",
                movie.getOverview());
    }

    @Test
    public void testGetMovieDefaultLanguage() {
        String movieId = "280";
        MovieReadDTO movie = theMovieDbClient.getMovie(movieId, null);
        Assert.assertEquals("Terminator 2: Judgment Day", movie.getOriginalTitle());
        Assert.assertEquals(movie.getOriginalTitle(), movie.getTitle());
        Assert.assertEquals("Nearly 10 years have passed since Sarah Connor was targeted"
                        + " for termination by a cyborg from the future. Now her son, John,"
                        + " the future leader of the resistance, is the target for a newer,"
                        + " more deadly terminator. Once again, the resistance has managed to send "
                        + "a protector back to attempt to save John and his mother Sarah.",
                movie.getOverview());
    }

    @Test
    public void testGetTopRatedMovies() {
        MoviesPageDTO moviesPage = theMovieDbClient.getTopRatedMovies();
        Assert.assertTrue(moviesPage.getTotalPages() > 0);
        Assert.assertTrue(moviesPage.getTotalResults() > 0);
        Assert.assertTrue(moviesPage.getResults().size() > 0);
        for (MovieReadShortDTO read : moviesPage.getResults()) {
            Assert.assertNotNull(read.getId());
            Assert.assertNotNull(read.getTitle());
        }
    }

    @Test
    public void testGetCreditsOfMovie() {
        String movieId = "280";
        MovieCreditsDTO readCredits = theMovieDbClient.getCreditsOfMovie(movieId , null);
        Assert.assertEquals("280", readCredits.getId());
        List<CastReadDTO> cast = readCredits.getCast();
        List<CrewReadDTO> crew = readCredits.getCrew();
        Assert.assertEquals(177, crew.size());
        Assert.assertEquals(57, cast.size());
        Assertions.assertThat(cast).extracting("character").contains("The Terminator");
        Assertions.assertThat(cast).extracting("name").contains("Arnold Schwarzenegger");
        Assertions.assertThat(crew).extracting("job").contains("Technical Advisor");
        Assertions.assertThat(crew).extracting("name").contains("Michael Albanese");
    }

    @Test
    public void testGetCreditsOfMovieActorAlreadyExist() {
        String movieId = "280";
        MovieCreditsDTO readCredits = theMovieDbClient.getCreditsOfMovie(movieId , null);
        List<CastReadDTO> cast = readCredits.getCast();
        List<CrewReadDTO> crew = readCredits.getCrew();
        Assertions.assertThat(cast).extracting("character").contains("The Terminator");
        Assertions.assertThat(cast).extracting("name").contains("Arnold Schwarzenegger");
        Assertions.assertThat(crew).extracting("job").contains("Technical Advisor");
        Assertions.assertThat(crew).extracting("name").contains("Michael Albanese");
    }

    @Test
    public void testGetCreditsOfMovieRoleAlreadyExist() {
        String movieId = "280";
        MovieCreditsDTO readCredits = theMovieDbClient.getCreditsOfMovie(movieId , null);
        List<CastReadDTO> cast = readCredits.getCast();
        List<CrewReadDTO> crew = readCredits.getCrew();
        Assertions.assertThat(cast).extracting("character").contains("The Terminator");
        Assertions.assertThat(cast).extracting("name").contains("Arnold Schwarzenegger");
        Assertions.assertThat(crew).extracting("job").contains("Technical Advisor");
        Assertions.assertThat(crew).extracting("name").contains("Michael Albanese");
    }

    @Test
    public void testGetCompany() {
        String companyId = "4";
        ProductionCompanyReadDTO company = theMovieDbClient.getCompany(companyId, null);
        Assert.assertEquals( "Paramount", company.getName());
        Assert.assertEquals("US", company.getOriginCountry());
    }
}
