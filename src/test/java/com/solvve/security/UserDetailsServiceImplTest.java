package com.solvve.security;

import com.solvve.BaseTest;
import com.solvve.domain.Admin;
import com.solvve.domain.RegisteredUser;
import com.solvve.repository.AdminRepository;
import com.solvve.repository.RegisteredUserRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.support.TransactionTemplate;

public class UserDetailsServiceImplTest extends BaseTest {

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testLoadUserByUsername() {

        Admin user = transactionTemplate.execute(status->{
            Admin u = testObjectsFactory.generateFlatEntityWithoutId(Admin.class);
            u.setUserRole(u.getUserRole());
            return adminRepository.save(u);
        });

        UserDetails userDetails = userDetailsService.loadUserByUsername(user.getEmail());
        Assert.assertEquals(user.getEmail(), userDetails.getUsername());
        Assert.assertEquals(user.getEncodedPassword(), userDetails.getPassword());
        Assert.assertFalse(userDetails.getAuthorities().isEmpty());
        Assertions.assertThat(userDetails.getAuthorities()).extracting("authority")
                .containsExactlyInAnyOrder(user.getUserRole().toString());
    }

    @Test
    public void testLoadRegisteredUserByUsername() {

        RegisteredUser user = transactionTemplate.execute(status->{
            RegisteredUser u = testObjectsFactory.generateFlatEntityWithoutId(RegisteredUser.class);
            u.setUserRole(u.getUserRole());
            return registeredUserRepository.save(u);
        });

        UserDetails userDetails = userDetailsService.loadUserByUsername(user.getEmail());
        Assert.assertEquals(user.getEmail(), userDetails.getUsername());
        Assert.assertEquals(user.getEncodedPassword(), userDetails.getPassword());
        Assert.assertFalse(userDetails.getAuthorities().isEmpty());
        Assertions.assertThat(userDetails.getAuthorities()).extracting("authority")
                .containsExactlyInAnyOrder(user.getUserRole().toString());
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testUserNotFound() {
        userDetailsService.loadUserByUsername("wrong name");
    }
}
