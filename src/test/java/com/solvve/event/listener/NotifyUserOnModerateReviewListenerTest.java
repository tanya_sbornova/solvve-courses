package com.solvve.event.listener;

import com.solvve.domain.ReviewStatus;
import com.solvve.event.ReviewStatusChangedEvent;
import com.solvve.service.UserNotificationService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class NotifyUserOnModerateReviewListenerTest {

    @MockBean
    UserNotificationService userNotificationService;

    @SpyBean
    private NotifyUserOnModerateReviewListener notifyUserOnModerateReviewListener;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Test
    public void testOnEvent() {

        ReviewStatusChangedEvent event = new ReviewStatusChangedEvent();
        event.setReviewId(UUID.randomUUID());
        event.setNewStatus(ReviewStatus.MODERATED);
        applicationEventPublisher.publishEvent(event);

        Mockito.verify(notifyUserOnModerateReviewListener, Mockito.timeout(500)).onEvent(event);
        Mockito.verify(userNotificationService, Mockito.timeout(500)).
                notifyOnReviewStatusChangedToModerated(event.getReviewId());
    }

    @Test
    public void testOnEventNotModerated() {

        for (ReviewStatus reviewStatus : ReviewStatus.values()) {
            if (reviewStatus == ReviewStatus.MODERATED) {
                continue;
            }
            ReviewStatusChangedEvent event = new ReviewStatusChangedEvent();
            event.setReviewId(UUID.randomUUID());
            event.setNewStatus(reviewStatus);
            applicationEventPublisher.publishEvent(event);

            Mockito.verify(notifyUserOnModerateReviewListener, Mockito.never()).onEvent(any());
            Mockito.verify(userNotificationService, Mockito.never()).notifyOnReviewStatusChangedToModerated(any());
        }
    }

    @Test
    public void testOnEventAsync() throws InterruptedException {

        ReviewStatusChangedEvent event = new ReviewStatusChangedEvent();
        event.setReviewId(UUID.randomUUID());
        event.setNewStatus(ReviewStatus.MODERATED);

        List<Integer> checkList = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(1);
        Mockito.doAnswer(invocationOnMock -> {
            Thread.sleep(500);
            checkList.add(2);
            latch.countDown();
            return null;
        }).when(userNotificationService).notifyOnReviewStatusChangedToModerated(event.getReviewId());

        applicationEventPublisher.publishEvent(event);
        checkList.add(1);
        latch.await();
        Mockito.verify(notifyUserOnModerateReviewListener).onEvent(event);
        Mockito.verify(userNotificationService).
                notifyOnReviewStatusChangedToModerated(event.getReviewId());
        Assert.assertEquals(Arrays.asList(1,2), checkList);
    }
}
