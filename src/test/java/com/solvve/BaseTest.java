package com.solvve;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = {"delete from external_system_import",
        "delete from misprint_info_common",
        "delete from problem_role",
        "delete from problem_film",
        "delete from problem_review",
        "delete from misprint_info",
        "delete from user_profile",
        "delete from news_like_dislike",
        "delete from review_film_like_dislike",
        "delete from review_role_like_dislike",
        "delete from review_film",
        "delete from review_role",
        "delete from review",
        "delete from rating_film",
        "delete from rating_role",
        "delete from registered_user",
        "delete from content_manager",
        "delete from moderator",
        "delete from admin",
        "delete from user_account",
        "delete from news",
        "delete from role",
        "delete from actor",
        "delete from crew_member",
        "delete from department",
        "delete from film",
        "delete from company",
        "delete from person"
}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public abstract class BaseTest {

}
