package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.RegisteredUser;
import com.solvve.domain.ReviewFilm;
import com.solvve.domain.ReviewStatus;
import com.solvve.dto.reviewfilm.ReviewFilmFilter;
import com.solvve.service.ReviewFilmService;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ReviewFilmRepositoryTest extends BaseTest {

    @Autowired
    private ReviewFilmRepository reviewFilmRepository;

    @Autowired
    private ReviewFilmService reviewFilmService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {

        Film film = testObjectsFactory.createFilm();
        ReviewFilm reviewFilm = testObjectsFactory.createReviewFilm(film);
        reviewFilm.setCreatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        reviewFilm = reviewFilmRepository.save(reviewFilm);

        Instant createdAtBeforeReload = reviewFilm.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        reviewFilm = reviewFilmRepository.findById(reviewFilm.getId()).get();

        Instant createdAtAfterReload = reviewFilm.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        Film film = testObjectsFactory.createFilm();
        ReviewFilm reviewFilm = testObjectsFactory.createReviewFilm(film);
        reviewFilm.setUpdatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        reviewFilm = reviewFilmRepository.save(reviewFilm);

        Instant updatedAtBeforeReload = reviewFilm.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        reviewFilm = reviewFilmRepository.findById(reviewFilm.getId()).get();

        Instant updatedAtAfterReload = reviewFilm.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(1000);
        reviewFilm.setIsSpoiler(true);
        reviewFilmRepository.save(reviewFilm);

        reviewFilm = reviewFilmRepository.findById(reviewFilm.getId()).get();
        Instant updatedAtAfterAnotherReload = reviewFilm.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {
        Film film = testObjectsFactory.createFilm();
        ReviewFilm ratingFilm = testObjectsFactory.createReviewFilm(film);
        assertNotNull(ratingFilm.getId());
        assertTrue(reviewFilmRepository.findById(ratingFilm.getId()).isPresent());
    }

    @Test
    public void testGetReviewFilmWithEmptyFilter() {
        Film film = testObjectsFactory.createFlatFilm();
        ReviewFilm rf1 = testObjectsFactory.createReviewFilm(film);
        ReviewFilm rf2 = testObjectsFactory.createReviewFilm(film);

        ReviewFilmFilter filter = new ReviewFilmFilter();
        Assertions.assertThat(reviewFilmService.getReviewFilms(filter, Pageable.unpaged()).getData())
                .extracting("id")
                .containsExactlyInAnyOrder(rf1.getId(), rf2.getId());
    }

    @Test
    public void testGetReviewFilmsByStatus() {
        Film film = testObjectsFactory.createFlatFilm();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        ReviewFilm rf1 = testObjectsFactory.createReviewFilm(ReviewStatus.FIXED, true,
                registeredUser, film);
        reviewFilmRepository.save(rf1);

        ReviewFilm rf2 = testObjectsFactory.createReviewFilm(ReviewStatus.NEED_TO_MODERATE,
                true, registeredUser, film);
        reviewFilmRepository.save(rf2);

        ReviewFilmFilter filter = new ReviewFilmFilter();
        filter.setStatus(ReviewStatus.FIXED);
        Assertions.assertThat(reviewFilmService.getReviewFilms(filter, Pageable.unpaged()).getData())
                .extracting("id")
                .containsExactlyInAnyOrder(rf1.getId());
    }

    @Test
    public void testGetReviewFilmsByIsSpoiler() {
        Film film = testObjectsFactory.createFlatFilm();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        ReviewFilm rf1 = testObjectsFactory.createReviewFilm(ReviewStatus.FIXED, true,
                registeredUser, film);
        reviewFilmRepository.save(rf1);

        ReviewFilm rf2 = testObjectsFactory.createReviewFilm(ReviewStatus.FIXED, false,
                registeredUser, film);
        reviewFilmRepository.save(rf2);

        ReviewFilmFilter filter = new ReviewFilmFilter();
        filter.setIsSpoiler(true);
        Assertions.assertThat(reviewFilmService.getReviewFilms(filter, Pageable.unpaged()).getData())
                .extracting("id")
                .containsExactlyInAnyOrder(rf1.getId());
    }

    @Test
    public void testGetReviewFilmsByRegisteredUserId() {
        Film film = testObjectsFactory.createFlatFilm();
        RegisteredUser registeredUser1 = testObjectsFactory.createRegisteredUser();
        ReviewFilm rf1 = testObjectsFactory.createReviewFilm(ReviewStatus.FIXED, false,
                registeredUser1, film);
        reviewFilmRepository.save(rf1);

        RegisteredUser registeredUser2 = testObjectsFactory.createRegisteredUser();
        ReviewFilm rf2 = testObjectsFactory.createReviewFilm(ReviewStatus.FIXED, false,
                registeredUser2, film);
        reviewFilmRepository.save(rf2);

        ReviewFilmFilter filter = new ReviewFilmFilter();
        filter.setRegisteredUserId(registeredUser2.getId());
        Assertions.assertThat(reviewFilmService.getReviewFilms(filter, Pageable.unpaged()).getData())
                .extracting("id")
                .containsExactlyInAnyOrder(rf2.getId());
    }

    @Test
    public void testGetReviewFilmsByFilmId() {
        Film film1 = testObjectsFactory.createFlatFilm();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        ReviewFilm rf1 = testObjectsFactory.createReviewFilm(ReviewStatus.FIXED, false,
                registeredUser, film1);
        reviewFilmRepository.save(rf1);

        Film film2 = testObjectsFactory.createFlatFilm();
        ReviewFilm rf2 = testObjectsFactory.createReviewFilm(ReviewStatus.FIXED, false,
                registeredUser, film2);
        reviewFilmRepository.save(rf2);

        ReviewFilmFilter filter = new ReviewFilmFilter();
        filter.setFilmId(film2.getId());
        Assertions.assertThat(reviewFilmService.getReviewFilms(filter, Pageable.unpaged()).getData())
                .extracting("id")
                .containsExactlyInAnyOrder(rf2.getId());
    }

    @Test
    public void testGetReviewFilmsByAllFilters() {
        Film film1 = testObjectsFactory.createFlatFilm();
        RegisteredUser registeredUser1 = testObjectsFactory.createRegisteredUser();
        ReviewFilm rf1 = testObjectsFactory.createReviewFilm(ReviewStatus.FIXED, false,
                registeredUser1, film1);
        reviewFilmRepository.save(rf1);

        Film film2 = testObjectsFactory.createFlatFilm();
        RegisteredUser registeredUser2 = testObjectsFactory.createRegisteredUser();
        ReviewFilm rf2 = testObjectsFactory.createReviewFilm(ReviewStatus.CANCELLED, true,
                registeredUser2, film2);
        reviewFilmRepository.save(rf2);

        ReviewFilmFilter filter = new ReviewFilmFilter();
        filter.setStatus(ReviewStatus.FIXED);
        filter.setIsSpoiler(false);
        filter.setRegisteredUserId(registeredUser1.getId());
        filter.setFilmId(film1.getId());
        Assertions.assertThat(reviewFilmService.getReviewFilms(filter, Pageable.unpaged()).getData())
                .extracting("id")
                .containsExactlyInAnyOrder(rf1.getId());
    }
}
