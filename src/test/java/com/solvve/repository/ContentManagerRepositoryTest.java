package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.ContentManager;
import com.solvve.domain.UserStatus;
import com.solvve.dto.contentmanager.ContentManagerFilter;
import com.solvve.service.ContentManagerService;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ContentManagerRepositoryTest extends BaseTest {

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Autowired
    private ContentManagerService contentManagerService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {

        ContentManager contentManager = testObjectsFactory.generateFlatEntityWithoutId(ContentManager.class);
        contentManager.setCreatedAt(LocalDateTime.of(2020,01,10,11,0, 0)
                .toInstant(ZoneOffset.UTC));

        contentManager = contentManagerRepository.save(contentManager);

        Instant createdAtBeforeReload = contentManager.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        contentManager = contentManagerRepository.findById(contentManager.getId()).get();

        Instant createdAtAfterReload = contentManager.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        ContentManager contentManager = testObjectsFactory.generateFlatEntityWithoutId(ContentManager.class);
        contentManager.setUpdatedAt(LocalDateTime.of(2020,01,10,11,0, 0)
                .toInstant(ZoneOffset.UTC));

        contentManager = contentManagerRepository.save(contentManager);

        Instant updatedAtBeforeReload = contentManager.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        contentManager = contentManagerRepository.findById(contentManager.getId()).get();

        Instant updatedAtAfterReload = contentManager.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(1000);
        contentManager.setEncodedPassword("tyuo999");
        contentManagerRepository.save(contentManager);

        contentManager = contentManagerRepository.findById(contentManager.getId()).get();
        Instant updatedAtAfterAnotherReload = contentManager.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {
        ContentManager contentManager = testObjectsFactory.generateFlatEntityWithoutId(ContentManager.class);
        contentManager = contentManagerRepository.save(contentManager);
        assertNotNull(contentManager.getId());
        assertTrue(contentManagerRepository.findById(contentManager.getId()).isPresent());
    }

    @Test
    public void testGetContentManagersWithEmptyFilter() {
        ContentManager m1 = testObjectsFactory.createContentManager();
        ContentManager m2 = testObjectsFactory.createContentManager();

        ContentManagerFilter filter = new ContentManagerFilter();
        Assertions.assertThat(contentManagerService.getContentManagers(filter, Pageable.unpaged()).getData())
                .extracting("id")
                .containsExactlyInAnyOrder(m1.getId(), m2.getId());
    }

    @Test
    public void testGetContentManagersByEmail() {
        ContentManager m1 = testObjectsFactory.generateFlatEntityWithoutId(ContentManager.class);
        m1.setEmail("test1@mail.ru");
        contentManagerRepository.save(m1);

        ContentManager m2 = testObjectsFactory.generateFlatEntityWithoutId(ContentManager.class);
        m2.setEmail("test2@mail.ru");
        contentManagerRepository.save(m2);

        ContentManagerFilter filter = new ContentManagerFilter();
        filter.setEmail("test1@mail.ru");
        Assertions.assertThat(contentManagerService.getContentManagers(filter, Pageable.unpaged()).getData())
                .extracting("id")
                .containsExactlyInAnyOrder(m1.getId());
    }

    @Test
    public void testGetContentManagersByUserStatus() {
        ContentManager m1 = testObjectsFactory.generateFlatEntityWithoutId(ContentManager.class);
        m1.setUserStatus(UserStatus.NEW);
        contentManagerRepository.save(m1);
        ContentManager m2 = testObjectsFactory.createContentManager();
        m2.setUserStatus(UserStatus.ACTIVE);
        contentManagerRepository.save(m2);

        ContentManagerFilter filter = new ContentManagerFilter();
        filter.setUserStatus(UserStatus.NEW);
        Assertions.assertThat(contentManagerService.getContentManagers(filter, Pageable.unpaged()).getData())
                .extracting("id")
                .containsExactlyInAnyOrder(m1.getId());
    }

    @Test
    public void testGetContentManagersByAllFilters() {
        ContentManager m1 = testObjectsFactory.generateFlatEntityWithoutId(ContentManager.class);
        m1.setEmail("test1@mail.ru");
        m1.setUserStatus(UserStatus.NEW);
        contentManagerRepository.save(m1);

        ContentManager m2 = testObjectsFactory.generateFlatEntityWithoutId(ContentManager.class);
        m2.setEmail("test2@mail.ru");
        m2.setUserStatus(UserStatus.BLOCKED);
        contentManagerRepository.save(m2);

        ContentManagerFilter filter = new ContentManagerFilter();
        filter.setEmail("test1@mail.ru");
        filter.setUserStatus(UserStatus.NEW);
        Assertions.assertThat(contentManagerService.getContentManagers(filter, Pageable.unpaged()).getData())
                .extracting("id")
                .containsExactlyInAnyOrder(m1.getId());
    }
}
