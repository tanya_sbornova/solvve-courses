package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.News;
import com.solvve.domain.NewsLikeDislike;
import com.solvve.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class NewsLikeDislikeRepositoryTest extends BaseTest {

    @Autowired
    private NewsLikeDislikeRepository newsLikeDislikeRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {

        NewsLikeDislike newsLikeDislike = testObjectsFactory.createNewsLikeDislike();
        newsLikeDislike.setCreatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        newsLikeDislike = newsLikeDislikeRepository.save(newsLikeDislike);

        Instant createdAtBeforeReload = newsLikeDislike.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        newsLikeDislike = newsLikeDislikeRepository.findById(newsLikeDislike.getId()).get();

        Instant createdAtAfterReload = newsLikeDislike.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        NewsLikeDislike newsLikeDislike = testObjectsFactory.createNewsLikeDislike();
        newsLikeDislike.setUpdatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));
        newsLikeDislike.setIsLike(false);

        newsLikeDislike = newsLikeDislikeRepository.save(newsLikeDislike);

        Instant updatedAtBeforeReload = newsLikeDislike.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        newsLikeDislike = newsLikeDislikeRepository.findById(newsLikeDislike.getId()).get();
        Thread.sleep(5000);
        Instant updatedAtAfterReload = newsLikeDislike.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(5000);
        newsLikeDislike.setIsLike(true);
        newsLikeDislikeRepository.save(newsLikeDislike);

        newsLikeDislike = newsLikeDislikeRepository.findById(newsLikeDislike.getId()).get();
        Instant updatedAtAfterAnotherReload = newsLikeDislike.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {

        NewsLikeDislike newsLikeDislike = testObjectsFactory.createNewsLikeDislike();
        newsLikeDislike = newsLikeDislikeRepository.save(newsLikeDislike);
        assertNotNull(newsLikeDislike.getId());
        assertTrue(newsLikeDislikeRepository.findById(newsLikeDislike.getId()).isPresent());
    }

    @Test
    public void testCalcCntLike() {

        News news = testObjectsFactory.createFilmNews(testObjectsFactory.createFlatFilm());
        testObjectsFactory.createNewsLikeDislike(news, true);
        testObjectsFactory.createNewsLikeDislike(news, true);
        testObjectsFactory.createNewsLikeDislike(news, false);

        Assert.assertEquals(Integer.valueOf(2), newsLikeDislikeRepository.calcCntOfNewsLike(news.getId()));
    }

    @Test
    public void testCalcCntDislike() {

        News news = testObjectsFactory.createFilmNews(testObjectsFactory.createFlatFilm());
        testObjectsFactory.createNewsLikeDislike(news, true);
        testObjectsFactory.createNewsLikeDislike(news, true);
        testObjectsFactory.createNewsLikeDislike(news, false);
        testObjectsFactory.createNewsLikeDislike(news, false);

        Assert.assertEquals(Integer.valueOf(2), newsLikeDislikeRepository.calcCntOfNewsDisLike(news.getId()));
    }

}