package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.Admin;
import com.solvve.domain.ContentManager;
import com.solvve.domain.UserRoleType;
import com.solvve.util.TestObjectsFactory;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AdminRepositoryTest extends BaseTest {

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {

        Admin admin = testObjectsFactory.generateFlatEntityWithoutId(Admin.class);
        admin.setCreatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        admin = adminRepository.save(admin);

        Instant createdAtBeforeReload = admin.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        admin = adminRepository.findById(admin.getId()).get();

        Instant createdAtAfterReload = admin.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        Admin admin = testObjectsFactory.generateFlatEntityWithoutId(Admin.class);
        admin.setUpdatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        admin = adminRepository.save(admin);

        Instant updatedAtBeforeReload = admin.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        admin = adminRepository.findById(admin.getId()).get();

        Instant updatedAtAfterReload = admin.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(1000);
        admin.setEncodedPassword("tyuo999");
        adminRepository.save(admin);

        admin = adminRepository.findById(admin.getId()).get();
        Instant updatedAtAfterAnotherReload = admin.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), CoreMatchers.is(true));
    }

    @Test
    public void testSave() {
        Admin admin = testObjectsFactory.generateFlatEntityWithoutId(Admin.class);
        admin = adminRepository.save(admin);
        assertNotNull(admin.getId());
        assertTrue(adminRepository.findById(admin.getId()).isPresent());
    }
}
