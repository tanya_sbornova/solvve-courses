package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.MisprintInfoFilm;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class MisprintInfoFilmRepositoryTest extends BaseTest {

    @Autowired
    private MisprintInfoFilmRepository misprintInfoRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testCreatedAtIsSet() {

        MisprintInfoFilm misprintInfo = testObjectsFactory.createMisprintInfoFilm();
        misprintInfo.setCreatedAt(LocalDateTime.of(2020, 01, 10, 11, 0, 0).
                toInstant(ZoneOffset.UTC));

        misprintInfo = misprintInfoRepository.save(misprintInfo);

        Instant createdAtBeforeReload = misprintInfo.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        misprintInfo = misprintInfoRepository.findById(misprintInfo.getId()).get();

        Instant createdAtAfterReload = misprintInfo.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        MisprintInfoFilm misprintInfo = testObjectsFactory.createMisprintInfoFilm();
        misprintInfo.setUpdatedAt(LocalDateTime.of(2020, 01, 10, 11, 0, 0).
                toInstant(ZoneOffset.UTC));

        misprintInfo = misprintInfoRepository.save(misprintInfo);

        Instant updatedAtBeforeReload = misprintInfo.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        misprintInfo = misprintInfoRepository.findById(misprintInfo.getId()).get();

        Instant updatedAtAfterReload = misprintInfo.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(1000);
        misprintInfo.setText("misprint");
        misprintInfoRepository.save(misprintInfo);

        misprintInfo = misprintInfoRepository.findById(misprintInfo.getId()).get();
        Instant updatedAtAfterAnotherReload = misprintInfo.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {
        MisprintInfoFilm misprintInfo = testObjectsFactory.createMisprintInfoFilm();
        misprintInfo = misprintInfoRepository.save(misprintInfo);
        assertNotNull(misprintInfo.getId());
        assertTrue(misprintInfoRepository.findById(misprintInfo.getId()).isPresent());
    }

    @Test
    public void testGetIdsOfMisprintInfoFilms() {
        Film film = testObjectsFactory.createFilm();
        Film film2 = testObjectsFactory.createFilm();
        List<UUID> expectedIdsOfMisprints = new ArrayList<>();
        MisprintInfoFilm misprintInfoFilm = testObjectsFactory.createMpInfoFilmWithParam(film,
                9, 19, "a Virtual", "a V_rtual");

        expectedIdsOfMisprints.add(misprintInfoFilm.getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoFilmWithParam(film, 11, 19, "Virtual",
                "V_rtual").getId());
        expectedIdsOfMisprints.add(testObjectsFactory
                .createMpInfoFilmWithParam(film, 11, 17, "Virt", "V_rt").getId());
        testObjectsFactory.createMpInfoFilmWithParam(film, 5, 17, "XXXXXXXXXXX", "");
        testObjectsFactory.createMpInfoFilmWithParam(film2, 9, 19, "a Virtual", "a V_rtual");

        inTransaction(() -> {
            Assertions.assertThat(misprintInfoRepository.getIdsOfMisprintInfoFilms(film.getId(), 9, 20))
                    .extracting("id").
                    containsExactlyInAnyOrder(expectedIdsOfMisprints.toArray());
        });
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }
}


