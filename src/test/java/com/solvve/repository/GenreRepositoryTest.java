package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.FilmGenre;
import com.solvve.domain.Genre;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class GenreRepositoryTest extends BaseTest {

    @Autowired
    private GenreRepository genreRepository;

    @Test
    public void testCreatedAtIsSet() {

        Genre genre = createGenre();
        genre.setCreatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        genre = genreRepository.save(genre);

        Instant createdAtBeforeReload = genre.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        genre = genreRepository.findById(genre.getId()).get();

        Instant createdAtAfterReload = genre.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        Genre genre = createGenre();
        genre.setUpdatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        genre = genreRepository.save(genre);

        Instant updatedAtBeforeReload = genre.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        genre = genreRepository.findById(genre.getId()).get();

        Instant updatedAtAfterReload = genre.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(1000);
        genre.setName(FilmGenre.ROMANCE);
        genreRepository.save(genre);

        genre = genreRepository.findById(genre.getId()).get();
        Instant updatedAtAfterAnotherReload = genre.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {
        Genre genre = createGenre();
        assertNotNull(genre.getId());
        assertTrue(genreRepository.findById(genre.getId()).isPresent());
    }

    private Genre createGenre() {
        Genre genre = new Genre();
        genre.setName(FilmGenre.DRAMA);
        return genreRepository.save(genre);
    }
}
