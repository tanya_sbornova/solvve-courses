package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.Actor;
import com.solvve.domain.Person;
import com.solvve.domain.RatingRole;
import com.solvve.domain.Role;
import com.solvve.dto.actor.ActorInLeaderBoardReadDTO;
import com.solvve.service.ActorService;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ActorRepositoryTest extends BaseTest {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private ActorService actorService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testCreatedAtIsSet() {

        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        actor.setCreatedAt(LocalDateTime.of(2020, 01, 10, 11, 0, 0).
                toInstant(ZoneOffset.UTC));

        actor = actorRepository.save(actor);

        Instant createdAtBeforeReload = actor.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        actor = actorRepository.findById(actor.getId()).get();

        Instant createdAtAfterReload = actor.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        actor.setUpdatedAt(LocalDateTime.of(2020, 01, 10, 11, 0, 0).
                toInstant(ZoneOffset.UTC));

        actor = actorRepository.save(actor);

        Instant updatedAtBeforeReload = actor.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        actor = actorRepository.findById(actor.getId()).get();

        Instant updatedAtAfterReload = actor.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(1000);
        actor.setAverageRating(5.8);
        actorRepository.save(actor);

        actor = actorRepository.findById(actor.getId()).get();
        Instant updatedAtAfterAnotherReload = actor.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {

        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        actor = actorRepository.save(actor);
        assertNotNull(actor.getId());
        assertTrue(actorRepository.findById(actor.getId()).isPresent());
    }

    @Test
    public void testGetPersonActors() {

        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        Actor actor2 = testObjectsFactory.createActor(person);

        List<Actor> res = actorRepository.findByPersonId(person.getId());

        Assertions.assertThat(res).extracting(Actor::getId).isEqualTo(Arrays.asList(actor.getId(),
                actor2.getId()));
    }

    @Test
    public void testGetPersonActorQuery() {

        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        Actor actor2 = testObjectsFactory.createActor(person);

        List<Actor> res = actorRepository.
                findActorsForPerson(person.getId());

        Assertions.assertThat(res).extracting(Actor::getId).
                isEqualTo(Arrays.asList(actor.getId(), actor2.getId()));
    }

    @Test
    public void testGetIdsOfActors() {
        Set<UUID> expectedIdsOfActors = new HashSet<>();
        Person person = testObjectsFactory.createPerson();
        expectedIdsOfActors.add(testObjectsFactory.createActor(person).getId());
        expectedIdsOfActors.add(testObjectsFactory.createActor(person).getId());
        expectedIdsOfActors.add(testObjectsFactory.createActor(person).getId());
        inTransaction(() -> {
            Assert.assertEquals(expectedIdsOfActors, actorRepository.getIdsOfActors().collect(Collectors.toSet()));
        });
    }

    @Test
    public void getActorsLeaderBoard() {
        int actorsCount = 30;
        Set<UUID> actorIds = new HashSet<>();
        for (int i = 0; i < actorsCount; i++) {
            Actor actor = testObjectsFactory.createActorWithRating(testObjectsFactory.createPerson());
            Role role = testObjectsFactory.createRole(actor, testObjectsFactory.createFlatFilm());
            testObjectsFactory.createRatingRole(role);
            testObjectsFactory.createRatingRole(role);
            RatingRole ratingRole = testObjectsFactory.createRatingRole(role);
            Role role2 = testObjectsFactory.createRole(actor, testObjectsFactory.createFlatFilm());
            testObjectsFactory.createRatingRole(role2);
            testObjectsFactory.createRatingRole(role2);
            testObjectsFactory.createRatingRole(role2);
            actorService.updateAverageRatingActorOfRole(actor.getId());
            actorIds.add(actor.getId());
        }
        List<ActorInLeaderBoardReadDTO> actorsLeaderBoard = actorRepository.getActorsLeaderBoard();
        Assertions.assertThat(actorsLeaderBoard)
                .isSortedAccordingTo(Comparator.comparing(ActorInLeaderBoardReadDTO::getAverageRating).reversed());
        Assert.assertEquals(actorIds,
                actorsLeaderBoard.stream().map(ActorInLeaderBoardReadDTO::getId).collect(Collectors.toSet()));

        for (ActorInLeaderBoardReadDTO ac : actorsLeaderBoard) {
            Assert.assertNotNull(ac.getName());
            Assert.assertNotNull(ac.getAverageRating());
        }
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }
}

