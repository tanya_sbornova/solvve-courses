package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.ReviewFilmLikeDislike;
import com.solvve.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ReviewFilmLikeDislikeRepositoryTest extends BaseTest {

    @Autowired
    private ReviewFilmLikeDislikeRepository reviewFilmLikeDislikeRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {

        ReviewFilmLikeDislike reviewFilmLikeDislike = testObjectsFactory.createReviewFilmLikeDislike();
        reviewFilmLikeDislike.setCreatedAt(LocalDateTime.of(2020,1,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        reviewFilmLikeDislike = reviewFilmLikeDislikeRepository.save(reviewFilmLikeDislike);

        Instant createdAtBeforeReload = reviewFilmLikeDislike.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        reviewFilmLikeDislike = reviewFilmLikeDislikeRepository.findById(reviewFilmLikeDislike.getId()).get();

        Instant createdAtAfterReload = reviewFilmLikeDislike.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        ReviewFilmLikeDislike reviewFilmLikeDislike = testObjectsFactory.createReviewFilmLikeDislike();
        reviewFilmLikeDislike.setUpdatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        reviewFilmLikeDislike = reviewFilmLikeDislikeRepository.save(reviewFilmLikeDislike);

        Instant updatedAtBeforeReload = reviewFilmLikeDislike.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        reviewFilmLikeDislike = reviewFilmLikeDislikeRepository.findById(reviewFilmLikeDislike.getId()).get();

        Instant updatedAtAfterReload = reviewFilmLikeDislike.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(5000);
        reviewFilmLikeDislike.setIsLike(true);
        reviewFilmLikeDislikeRepository.save(reviewFilmLikeDislike);

        reviewFilmLikeDislike = reviewFilmLikeDislikeRepository.findById(reviewFilmLikeDislike.getId()).get();
        Instant updatedAtAfterAnotherReload = reviewFilmLikeDislike.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {

        ReviewFilmLikeDislike reviewFilmLikeDislike = testObjectsFactory.createReviewFilmLikeDislike();
        reviewFilmLikeDislike = reviewFilmLikeDislikeRepository.save(reviewFilmLikeDislike);
        assertNotNull(reviewFilmLikeDislike.getId());
        assertTrue(reviewFilmLikeDislikeRepository.findById(reviewFilmLikeDislike.getId()).isPresent());
    }
}