package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.ProblemReview;
import com.solvve.domain.RegisteredUser;
import com.solvve.domain.ReviewFilmLikeDislike;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.hibernate.LazyInitializationException;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.UUID;

public class RepositoryHelperTest extends BaseTest {

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testGetReferenceIfExistExactlyReference() {

        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        RegisteredUser registeredUserReference = repositoryHelper.getReferenceIfExists(RegisteredUser.class,
                registeredUser.getId());

        Assert.assertTrue(registeredUser.getId().equals(registeredUserReference.getId()));

        Assertions.assertThatThrownBy(() -> {
            registeredUserReference.getName();
        }).isInstanceOf(LazyInitializationException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetReferenceIfExistNotFound() {
        repositoryHelper.getReferenceIfExists(RegisteredUser.class, UUID.randomUUID());
    }

    @Test
    public void testGetEntityRequired() {

        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        RegisteredUser registeredUserRequired = repositoryHelper.getEntityRequired(RegisteredUser.class,
                registeredUser.getId());
        Assert.assertEquals(registeredUser.getId(), registeredUserRequired.getId());
        Assert.assertEquals(registeredUser.getName(), registeredUserRequired.getName());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetEntityRequiredIfExistNotFound() {

        repositoryHelper.getEntityRequired(RegisteredUser.class, UUID.randomUUID());
    }

    @Test
    public void testGetEntityAssociationRequired() {

        ProblemReview problemReview = testObjectsFactory.createProblemFilmReview();
        RegisteredUser registeredUser = problemReview.getRegisteredUser();

        inTransaction(()-> {
            RegisteredUser ru = registeredUserRepository.findById(registeredUser.getId()).get();
            repositoryHelper.getEntityAssociationRequired(RegisteredUser.class, ProblemReview.class,
                ru.getId(), ru.getProblemReviews().get(0).getId());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetEntityAssociationRequiredIfExistNotFound() {

        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        repositoryHelper.getEntityAssociationRequired(RegisteredUser.class, ReviewFilmLikeDislike.class,
                registeredUser.getId(), UUID.randomUUID());
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status->{
            runnable.run();
        });
    }
}

