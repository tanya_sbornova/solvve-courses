package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.FilmGenre;
import com.solvve.domain.UserProfile;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class UserProfileRepositoryTest extends BaseTest {

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Test
    public void testCreatedAtIsSet() {

        UserProfile userProfile = createUserProfile();
        userProfile.setCreatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        userProfile = userProfileRepository.save(userProfile);

        Instant createdAtBeforeReload = userProfile.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        userProfile = userProfileRepository.findById(userProfile.getId()).get();

        Instant createdAtAfterReload = userProfile.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        UserProfile userProfile = createUserProfile();
        userProfile.setUpdatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        userProfile = userProfileRepository.save(userProfile);

        Instant updatedAtBeforeReload = userProfile.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        userProfile = userProfileRepository.findById(userProfile.getId()).get();

        Instant updatedAtAfterReload = userProfile.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);
        Thread.sleep(1000);
        userProfile.setRatingActivity(5.8);
        userProfileRepository.save(userProfile);

        userProfile = userProfileRepository.findById(userProfile.getId()).get();
        Instant updatedAtAfterAnotherReload = userProfile.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }


    @Test
    public void testSave() {
        UserProfile userProfile = new UserProfile();
        userProfile = userProfileRepository.save(userProfile);
        assertNotNull(userProfile.getId());
        assertTrue(userProfileRepository.findById(userProfile.getId()).isPresent());
    }

    private UserProfile createUserProfile() {
        UserProfile userProfile = new UserProfile();
        userProfile.setRatingReviews(5.2);
        userProfile.setRatingActivity(6.7);
        userProfile.setFavouriteGenre(FilmGenre.ROMANCE);
        return userProfileRepository.save(userProfile);
    }
}
