package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.MisprintInfoNews;
import com.solvve.domain.News;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class MisprintInfoNewsRepositoryTest extends BaseTest {

    @Autowired
    private MisprintInfoNewsRepository misprintInfoRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testCreatedAtIsSet() {

        MisprintInfoNews misprintInfo = testObjectsFactory.createMisprintInfoNews();
        misprintInfo.setCreatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        misprintInfo = misprintInfoRepository.save(misprintInfo);

        Instant createdAtBeforeReload = misprintInfo.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        misprintInfo = misprintInfoRepository.findById(misprintInfo.getId()).get();

        Instant createdAtAfterReload = misprintInfo.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        MisprintInfoNews misprintInfo = testObjectsFactory.createMisprintInfoNews();
        misprintInfo.setUpdatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        misprintInfo = misprintInfoRepository.save(misprintInfo);

        Instant updatedAtBeforeReload = misprintInfo.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        misprintInfo = misprintInfoRepository.findById(misprintInfo.getId()).get();

        Instant updatedAtAfterReload = misprintInfo.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(1000);
        misprintInfo.setText("misprint");
        misprintInfoRepository.save(misprintInfo);

        misprintInfo = misprintInfoRepository.findById(misprintInfo.getId()).get();
        Instant updatedAtAfterAnotherReload = misprintInfo.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {
        MisprintInfoNews misprintInfo = testObjectsFactory.createMisprintInfoNews();
        misprintInfo = misprintInfoRepository.save(misprintInfo);
        assertNotNull(misprintInfo.getId());
        assertTrue(misprintInfoRepository.findById(misprintInfo.getId()).isPresent());
    }

    @Test
    public void testGetIdsOfMisprintInfoNews() {
        Film film = testObjectsFactory.createFlatFilm();
        News news = testObjectsFactory.createFilmNews(film);
        News news2 = testObjectsFactory.createFilmNews(film);
        List<UUID> expectedIdsOfMisprints = new ArrayList<>();
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoNewsWithParam(news, 10,16,
                "reveal", "rev_al").getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoNewsWithParam(news, 13,13, "e", "_").getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoNewsWithParam(news, 13,16,"veal", "v_al").getId());
        testObjectsFactory.createMpInfoNewsWithParam(news, 5,16,"XXXXXXXXXXX", "");
        testObjectsFactory.createMpInfoNewsWithParam(news2, 13,16, "veal", "v_al");
        inTransaction(()-> {
            List<UUID> actualIdsOfMisprints = misprintInfoRepository.getIdsOfMisprintInfoNews(news.getId(), 10, 20).
                    collect(Collectors.toList());
            Assertions.assertThat(expectedIdsOfMisprints.toArray()).
                    containsExactlyInAnyOrder(actualIdsOfMisprints.toArray());
        });
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status->{
            runnable.run();
        });
    }
}

