package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.MisprintInfoCommon;
import com.solvve.domain.MisprintObject;
import com.solvve.domain.Role;
import com.solvve.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class MisprintInfoCommonFixRepositoryTest extends BaseTest {

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private MisprintInfoCommonRepository mpFixRepository;

    @Test
    public void testGetIdsOfMisprintInfoRole() {
        Role role = testObjectsFactory.createRole();
        Role role2 = testObjectsFactory.createRole();

        List<UUID> expectedIdsOfMisprints = new ArrayList<>();
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoCommonWithParam(role.getId(), MisprintObject.ROLE,
                0, 3, "Neo", "NEo").getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoCommonWithParam(role.getId(), MisprintObject.ROLE,
                1, 2, "e", "E").getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoCommonWithParam(role.getId(), MisprintObject.ROLE,
                1, 3, "eo", "Eo").getId());

        testObjectsFactory.createMpInfoRoleWithParam(role, 5, 16, "XXXXXXXXXXX", "");
        testObjectsFactory.createMpInfoRoleWithParam(role2, 1, 3, "eo", "Eo");

        List<MisprintInfoCommon> misprintsInfo = mpFixRepository.getMisprintsInfoCommon(role.getId(), "NEo",
                0, 3);

        List<UUID> misprintsInfoId = misprintsInfo.stream().map(x -> x.getId()).collect(Collectors.toList());
        Assert.assertEquals(expectedIdsOfMisprints, misprintsInfoId);
    }

}

