package com.solvve.repository;

 import com.solvve.BaseTest;
 import com.solvve.domain.ProblemReview;
 import com.solvve.domain.ProblemType;
 import com.solvve.util.TestObjectsFactory;
 import org.junit.Assert;
 import org.junit.Test;
 import org.springframework.beans.factory.annotation.Autowired;
 import java.time.Instant;
 import java.time.LocalDateTime;
 import java.time.ZoneOffset;
 import static org.hamcrest.CoreMatchers.is;
 import static org.junit.Assert.assertNotNull;
 import static org.junit.Assert.assertTrue;

 public class  ProblemReviewRepositoryTest extends BaseTest {

     @Autowired
     private ProblemReviewRepository problemReviewRepository;

     @Autowired
     private TestObjectsFactory testObjectsFactory;

     @Test
     public void testCreatedAtIsSet() {

         ProblemReview problemReview = testObjectsFactory.createProblemFilmReview();
         problemReview.setCreatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                 toInstant(ZoneOffset.UTC));

         problemReview = problemReviewRepository.save(problemReview);

         Instant createdAtBeforeReload = problemReview.getCreatedAt();
         Assert.assertNotNull(createdAtBeforeReload);
         problemReview = problemReviewRepository.findById(problemReview.getId()).get();

         Instant createdAtAfterReload = problemReview.getCreatedAt();
         Assert.assertNotNull(createdAtBeforeReload);
         Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
     }

     @Test
     public void testUpdatedAtIsSet() throws InterruptedException {

         ProblemReview problemReview = testObjectsFactory.createProblemFilmReview();
         problemReview.setUpdatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                 toInstant(ZoneOffset.UTC));

         problemReview = problemReviewRepository.save(problemReview);

         Instant updatedAtBeforeReload = problemReview.getUpdatedAt();
         Assert.assertNotNull(updatedAtBeforeReload);
         problemReview = problemReviewRepository.findById(problemReview.getId()).get();

         Instant updatedAtAfterReload = problemReview.getUpdatedAt();
         Assert.assertNotNull(updatedAtBeforeReload);
         Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

         Thread.sleep(1000);
         problemReview.setProblemType(ProblemType.SPOILER);
         problemReviewRepository.save(problemReview);

         problemReview = problemReviewRepository.findById(problemReview.getId()).get();
         Instant updatedAtAfterAnotherReload = problemReview.getUpdatedAt();
         Assert.assertNotNull(updatedAtAfterAnotherReload);
         Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
         Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
     }

     @Test
     public void testSave() {
         ProblemReview person = testObjectsFactory.createProblemFilmReview();
         person = problemReviewRepository.save(person);
         assertNotNull(person.getId());
         assertTrue(problemReviewRepository.findById(person.getId()).isPresent());
     }
 }
