package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.*;
import com.solvve.dto.role.RoleFilter;
import com.solvve.service.RoleService;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.support.TransactionTemplate;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class RoleRepositoryTest extends BaseTest {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleService roleService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testCreatedAtIsSet() {

        Role role = testObjectsFactory.createRole();
        role.setCreatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        role = roleRepository.save(role);

        Instant createdAtBeforeReload = role.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        role = roleRepository.findById(role.getId()).get();

        Instant createdAtAfterReload = role.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        Role role = testObjectsFactory.createRole();
        role.setUpdatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        role = roleRepository.save(role);

        Instant updatedAtBeforeReload = role.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        role = roleRepository.findById(role.getId()).get();

        Instant updatedAtAfterReload = role.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(1000);
        role.setText("Thomas Anderson");
        roleRepository.save(role);

        role = roleRepository.findById(role.getId()).get();
        Instant updatedAtAfterAnotherReload = role.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {

        Role role = testObjectsFactory.createRole();
        assertNotNull(role.getId());
        assertTrue(roleRepository.findById(role.getId()).isPresent());
    }

    @Test
    public void testFindByTextAndFilmId() {

        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        Film film = testObjectsFactory.createFilm();
        Role role =testObjectsFactory.createRole(actor, film);
        Role roleInRepo = roleRepository.findRoleByFilmIdAndNameRole(film.getId(), role.getText());
        assertNotNull(roleInRepo.getId());
        Assert.assertEquals(roleInRepo.getText(), role.getText());
    }

    @Test
    public void testGetCrewMemberFilmRoles() {

        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        Film film = testObjectsFactory.createFilm();
        Role role = testObjectsFactory.createRole(actor, film);
        Role role2 = testObjectsFactory.createRole(actor, film);

        List<Role> res = roleRepository.findByActorIdAndFilmId(actor.getId(), film.getId());

        Assertions.assertThat(res).extracting(Role::getId).isEqualTo(Arrays.asList(role.getId(),
                role2.getId()));
    }

    @Test
    public void testGetCrewMemberFilmQuery() {

        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        Film film = testObjectsFactory.createFilm();
        Role role = testObjectsFactory.createRole(actor, film);
        Role role2 = testObjectsFactory.createRole(actor, film);

        List<Role> res = roleRepository.findRolesForActorAndForFilm(actor.getId(), film.getId());

        Assertions.assertThat(res).
                extracting(Role::getId).isEqualTo(Arrays.asList(role.getId(), role2.getId()));
        Assertions.assertThat(res).extracting(Role::getId).
                containsExactlyInAnyOrder(role.getId(), role2.getId());
    }

    @Test
    public void testGetPersonFilmQuery() {

        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        Film film = testObjectsFactory.createFilm();
        Film film2 = testObjectsFactory.createFilm();
        Role role = testObjectsFactory.createRole(actor, film);
        Role role2 = testObjectsFactory.createRole(actor, film2);
        Role role3 = testObjectsFactory.createRole(actor, film2);

        List<Role> res = roleRepository.findRolesForActorAndForFilm(actor.getId(), film2.getId());

        Assertions.assertThat(res).extracting(Role::getId).
                isEqualTo(Arrays.asList(role2.getId(), role3.getId()));
        Assertions.assertThat(res).extracting(Role::getId).
                containsExactlyInAnyOrder(role2.getId(), role3.getId());
    }

    @Test
    public void testGetRolesWithEmptyFilter() {
        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        Film film = testObjectsFactory.createFilm();
        Film film2 = testObjectsFactory.createFilm();
        Role role = testObjectsFactory.createRole(actor, film);
        Role role2 = testObjectsFactory.createRole(actor, film2);

        RoleFilter filter = new RoleFilter();
        Assertions.assertThat(roleService.getRoles(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(role.getId(), role2.getId());
    }

    @Test
    public void testGetRolesByPerson() {
        Person person = testObjectsFactory.createPerson();
        Person person2 = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        Actor actor2 = testObjectsFactory.createActor(person2);
        Film film = testObjectsFactory.createFilm();
        Film film2 = testObjectsFactory.createFilm();
        Film film3 = testObjectsFactory.createFilm();
        Role role = testObjectsFactory.createRole(actor, film);
        Role role2 = testObjectsFactory.createRole(actor2, film2);
        Role role3 = testObjectsFactory.createRole(actor, film3);

        RoleFilter filter = new RoleFilter();
        filter.setPersonId(person.getId());
        Assertions.assertThat(roleService.getRoles(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(role.getId(), role3.getId());
    }

    @Test
    public void testGetRolesByFilm() {
        Person person = testObjectsFactory.createPerson();
        Person person2 = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        Actor actor2 = testObjectsFactory.createActor(person2);
        Film film = testObjectsFactory.createFilm();
        Film film2 = testObjectsFactory.createFilm();
        Film film3 = testObjectsFactory.createFilm();
        Role role = testObjectsFactory.createRole(actor, film);
        Role role2 = testObjectsFactory.createRole(actor2, film);
        Role role3 = testObjectsFactory.createRole(actor, film3);

        RoleFilter filter = new RoleFilter();
        filter.setFilmId(film.getId());
        Assertions.assertThat(roleService.getRoles(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(role.getId(), role2.getId());
    }

    @Test
    public void testGetRolesByAllFilters() {
        Person person = testObjectsFactory.createPerson();
        Person person2 = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        Actor actor2 = testObjectsFactory.createActor(person2);
        Film film = testObjectsFactory.createFilm();
        Film film2 = testObjectsFactory.createFilm();
        Film film3 = testObjectsFactory.createFilm();
        Role role = testObjectsFactory.createRole(actor, film);
        Role role2 = testObjectsFactory.createRole(actor2, film);
        Role role3 = testObjectsFactory.createRole(actor, film3);

        RoleFilter filter = new RoleFilter();
        filter.setPersonId(person.getId());
        filter.setFilmId(film.getId());
        Assertions.assertThat(roleService.getRoles(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(role.getId());
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status->{
            runnable.run();
        });
    }
}

