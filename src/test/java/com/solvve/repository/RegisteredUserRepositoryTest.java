package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.RegisteredUser;
import com.solvve.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class RegisteredUserRepositoryTest extends BaseTest {

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {

        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        registeredUser.setCreatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        registeredUser = registeredUserRepository.save(registeredUser);

        Instant createdAtBeforeReload = registeredUser.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        registeredUser = registeredUserRepository.findById(registeredUser.getId()).get();

        Instant createdAtAfterReload = registeredUser.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        registeredUser.setUpdatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        registeredUser = registeredUserRepository.save(registeredUser);

        Instant updatedAtBeforeReload = registeredUser.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        registeredUser = registeredUserRepository.findById(registeredUser.getId()).get();

        Instant updatedAtAfterReload = registeredUser.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(1000);
        registeredUser.setEncodedPassword("tyuo999");
        registeredUserRepository.save(registeredUser);

        registeredUser = registeredUserRepository.findById(registeredUser.getId()).get();
        Instant updatedAtAfterAnotherReload = registeredUser.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        registeredUser = registeredUserRepository.save(registeredUser);
        assertNotNull(registeredUser.getId());
        assertTrue(registeredUserRepository.findById(registeredUser.getId()).isPresent());
    }
}
