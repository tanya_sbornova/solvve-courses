package com.solvve.repository;

import com.solvve.BaseTest;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

@TestPropertySource(properties = "spring.liquibase.change-log=classpath:db/changelog/db.changelog-master.xml")
public class LiquibaseLoadDataTest extends BaseTest {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private RatingFilmRepository ratingFilmRepository;

    @Autowired
    private RatingRoleRepository ratingRoleRepository;

    @Autowired
    private ReviewFilmRepository reviewFilmRepository;

    @Autowired
    private ReviewRoleRepository reviewRoleRepository;

    @Autowired
    private ReviewFilmLikeDislikeRepository reviewFilmLikeDislikeRepository;

    @Autowired
    private ReviewRoleLikeDislikeRepository reviewRoleLikeDislikeRepository;

    @Autowired
    private NewsLikeDislikeRepository newsLikeDislikeRepository;

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Autowired
    private ProblemReviewRepository problemReviewRepository;

    @Autowired
    private MisprintInfoFilmRepository misprintInfoRepository;

    @Test
    public void testDataLoaded() {

        Assert.assertTrue(actorRepository.count() > 0);
        Assert.assertTrue(crewMemberRepository.count() > 0);
        Assert.assertTrue(personRepository.count() > 0);
        Assert.assertTrue(filmRepository.count() > 0);
        Assert.assertTrue(roleRepository.count() > 0);
        Assert.assertTrue(genreRepository.count() > 0);
        Assert.assertTrue(adminRepository.count() > 0);
        Assert.assertTrue(moderatorRepository.count() > 0);
        Assert.assertTrue(contentManagerRepository.count() > 0);
        Assert.assertTrue(registeredUserRepository.count() > 0);
        Assert.assertTrue(newsRepository.count() > 0);
        Assert.assertTrue(ratingFilmRepository.count() > 0);
        Assert.assertTrue(ratingRoleRepository.count() > 0);
        Assert.assertTrue(reviewFilmRepository.count() > 0);
        Assert.assertTrue(reviewRoleRepository.count() > 0);
        Assert.assertTrue(reviewFilmLikeDislikeRepository.count() > 0);
        Assert.assertTrue(reviewRoleLikeDislikeRepository.count() > 0);
        Assert.assertTrue(newsLikeDislikeRepository.count() > 0);
        Assert.assertTrue(userProfileRepository.count() > 0);
        Assert.assertTrue(problemReviewRepository.count() > 0);
        Assert.assertTrue(misprintInfoRepository.count() > 0);
        Assert.assertTrue(companyRepository.count() > 0);
        Assert.assertTrue(departmentRepository.count() > 0);
    }
}
