package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.MisprintInfoRole;
import com.solvve.domain.Role;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class MisprintInfoRoleRepositoryTest extends BaseTest {

    @Autowired
    private MisprintInfoRoleRepository misprintInfoRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testCreatedAtIsSet() {

        MisprintInfoRole misprintInfo = testObjectsFactory.createMisprintInfoRole();
        misprintInfo.setCreatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        misprintInfo = misprintInfoRepository.save(misprintInfo);

        Instant createdAtBeforeReload = misprintInfo.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        misprintInfo = misprintInfoRepository.findById(misprintInfo.getId()).get();

        Instant createdAtAfterReload = misprintInfo.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        MisprintInfoRole misprintInfo = testObjectsFactory.createMisprintInfoRole();
        misprintInfo.setUpdatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        misprintInfo = misprintInfoRepository.save(misprintInfo);

        Instant updatedAtBeforeReload = misprintInfo.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        misprintInfo = misprintInfoRepository.findById(misprintInfo.getId()).get();

        Instant updatedAtAfterReload = misprintInfo.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(1000);
        misprintInfo.setText("misprint");
        misprintInfoRepository.save(misprintInfo);

        misprintInfo = misprintInfoRepository.findById(misprintInfo.getId()).get();
        Instant updatedAtAfterAnotherReload = misprintInfo.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {
        MisprintInfoRole misprintInfo = testObjectsFactory.createMisprintInfoRole();
        misprintInfo = misprintInfoRepository.save(misprintInfo);
        assertNotNull(misprintInfo.getId());
        assertTrue(misprintInfoRepository.findById(misprintInfo.getId()).isPresent());
    }

}

