package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.News;
import com.solvve.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class NewsRepositoryTest extends BaseTest {

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testCreatedAtIsSet() {

        Film film = testObjectsFactory.createFlatFilm();
        News news = testObjectsFactory.createFilmNews(film);
        news.setCreatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        news = newsRepository.save(news);

        Instant createdAtBeforeReload = news.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        news = newsRepository.findById(news.getId()).get();

        Instant createdAtAfterReload = news.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        Film film = testObjectsFactory.createFlatFilm();
        News news = testObjectsFactory.createFilmNews(film);
        news.setUpdatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        news = newsRepository.save(news);

        Instant updatedAtBeforeReload = news.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        news = newsRepository.findById(news.getId()).get();

        Instant updatedAtAfterReload = news.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(1000);
        news.setTitle("is about ...");
        newsRepository.save(news);

        news = newsRepository.findById(news.getId()).get();
        Instant updatedAtAfterAnotherReload = news.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {
        Film film = testObjectsFactory.createFlatFilm();
        News news = testObjectsFactory.createFilmNews(film);
        assertNotNull(news.getId());
        assertTrue(newsRepository.findById(news.getId()).isPresent());
    }

    @Test
    public void testGetIdsOfNews() {

        Set<UUID> expectedIdsOfNews = new HashSet<>();
        expectedIdsOfNews.add(testObjectsFactory.createNews(testObjectsFactory.createRole()).getId());
        expectedIdsOfNews.add(testObjectsFactory.createNews(testObjectsFactory.createRole()).getId());
        expectedIdsOfNews.add(testObjectsFactory.createNews(testObjectsFactory.createRole()).getId());
        inTransaction(() -> {
            Assert.assertEquals(expectedIdsOfNews, newsRepository.getIdsOfNews().collect(Collectors.toSet()));
        });
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }
}
