package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.ReviewRoleLikeDislike;
import com.solvve.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ReviewRoleLikeDislikeRepositoryTest extends BaseTest {

    @Autowired
    private ReviewRoleLikeDislikeRepository reviewRoleLikeDislikeRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {

        ReviewRoleLikeDislike reviewRoleLikeDislike = testObjectsFactory.createReviewRoleLikeDislike();
        reviewRoleLikeDislike.setCreatedAt(LocalDateTime.of(2020,1,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        reviewRoleLikeDislike = reviewRoleLikeDislikeRepository.save(reviewRoleLikeDislike);

        Instant createdAtBeforeReload = reviewRoleLikeDislike.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        reviewRoleLikeDislike = reviewRoleLikeDislikeRepository.findById(reviewRoleLikeDislike.getId()).get();

        Instant createdAtAfterReload = reviewRoleLikeDislike.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        ReviewRoleLikeDislike reviewRoleLikeDislike = testObjectsFactory.createReviewRoleLikeDislike();
        reviewRoleLikeDislike.setUpdatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        reviewRoleLikeDislike = reviewRoleLikeDislikeRepository.save(reviewRoleLikeDislike);

        Instant updatedAtBeforeReload = reviewRoleLikeDislike.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        reviewRoleLikeDislike = reviewRoleLikeDislikeRepository.findById(reviewRoleLikeDislike.getId()).get();

        Instant updatedAtAfterReload = reviewRoleLikeDislike.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(2000);
        reviewRoleLikeDislike.setIsLike(true);
        reviewRoleLikeDislikeRepository.save(reviewRoleLikeDislike);

        reviewRoleLikeDislike = reviewRoleLikeDislikeRepository.findById(reviewRoleLikeDislike.getId()).get();
        Instant updatedAtAfterAnotherReload = reviewRoleLikeDislike.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {

        ReviewRoleLikeDislike reviewRoleLikeDislike = testObjectsFactory.createReviewRoleLikeDislike();
        reviewRoleLikeDislike = reviewRoleLikeDislikeRepository.save(reviewRoleLikeDislike);
        assertNotNull(reviewRoleLikeDislike.getId());
        assertTrue(reviewRoleLikeDislikeRepository.findById(reviewRoleLikeDislike.getId()).isPresent());
    }
}
