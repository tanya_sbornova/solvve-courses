package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.*;
import com.solvve.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class RatingFilmRepositoryTest extends BaseTest {

    @Autowired
    private RatingFilmRepository ratingFilmRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {

        Film film = testObjectsFactory.createFilm();
        RatingFilm ratingFilm = testObjectsFactory.createRatingFilm(film);
        ratingFilm.setCreatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        ratingFilm = ratingFilmRepository.save(ratingFilm);

        Instant createdAtBeforeReload = ratingFilm.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        ratingFilm = ratingFilmRepository.findById(ratingFilm.getId()).get();

        Instant createdAtAfterReload = ratingFilm.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        Film film = testObjectsFactory.createFilm();
        RatingFilm ratingFilm = testObjectsFactory.createRatingFilm(film);
        ratingFilm.setUpdatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        ratingFilm = ratingFilmRepository.save(ratingFilm);

        Instant updatedAtBeforeReload = ratingFilm.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        ratingFilm = ratingFilmRepository.findById(ratingFilm.getId()).get();

        Instant updatedAtAfterReload = ratingFilm.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(1000);
        ratingFilm.setRating(8.6);
        ratingFilmRepository.save(ratingFilm);

        ratingFilm = ratingFilmRepository.findById(ratingFilm.getId()).get();
        Instant updatedAtAfterAnotherReload = ratingFilm.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {
        Film film = testObjectsFactory.createFilm();
        RatingFilm ratingFilm = testObjectsFactory.createRatingFilm(film);
        assertNotNull(ratingFilm.getId());
        assertTrue(ratingFilmRepository.findById(ratingFilm.getId()).isPresent());
    }

    @Test
    public void testCalcAverageRating() {

        Film film = testObjectsFactory.createFlatFilm();
        testObjectsFactory.createRatingFilm(film, 7.0);
        testObjectsFactory.createRatingFilm(film, 8.0);
        testObjectsFactory.createRatingFilm(film, 9.0);

        Assert.assertEquals(8.0, ratingFilmRepository.calcAverageRatingOfFilm(film.getId()), Double.MIN_NORMAL);
    }

    @Test
    public void testCalcAverageRatingActorByFilm() {

        Film film = testObjectsFactory.createFlatFilm();
        testObjectsFactory.createRatingFilm(film, 7.0);
        testObjectsFactory.createRatingFilm(film, 8.0);
        testObjectsFactory.createRatingFilm(film, 9.0);
        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        Role role1 = testObjectsFactory.createRole(actor, film);
        roleRepository.save(role1);
        Film film2 = testObjectsFactory.createFlatFilm();
        testObjectsFactory.createRatingFilm(film, 6.0);
        testObjectsFactory.createRatingFilm(film, 7.0);
        testObjectsFactory.createRatingFilm(film, 8.0);
        Role role2 = testObjectsFactory.createRole(actor, film2);
        roleRepository.save(role2);

        Assert.assertEquals(7.5, ratingFilmRepository.calcAverageRatingActorByFilm(actor.getId()), Double.MIN_NORMAL);
    }

}
