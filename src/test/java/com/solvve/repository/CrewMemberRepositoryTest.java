package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.CrewMember;
import com.solvve.domain.CrewMemberType;
import com.solvve.domain.Film;
import com.solvve.domain.Person;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class CrewMemberRepositoryTest extends BaseTest {

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {

        CrewMember crewMember = testObjectsFactory.createCrewMember();
        crewMember.setCreatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        crewMember = crewMemberRepository.save(crewMember);

        Instant createdAtBeforeReload = crewMember.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        crewMember = crewMemberRepository.findById(crewMember.getId()).get();

        Instant createdAtAfterReload = crewMember.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        CrewMember crewMember = testObjectsFactory.createCrewMember();
        crewMember.setUpdatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        crewMember = crewMemberRepository.save(crewMember);

        Instant updatedAtBeforeReload = crewMember.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        crewMember = crewMemberRepository.findById(crewMember.getId()).get();

        Instant updatedAtAfterReload = crewMember.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(1000);
        crewMember.setCrewMemberType(CrewMemberType.COMPOSER);
        crewMemberRepository.save(crewMember);

        crewMember = crewMemberRepository.findById(crewMember.getId()).get();
        Instant updatedAtAfterAnotherReload = crewMember.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {
        Film film = testObjectsFactory.createFilm();
        CrewMember crewMember = testObjectsFactory.createCrewMember();
        film.setCrewMembers(Set.of(crewMember));
        filmRepository.save(film);
        crewMember = crewMemberRepository.save(crewMember);
        assertNotNull(crewMember.getId());
        assertTrue(crewMemberRepository.findById(crewMember.getId()).isPresent());
    }

    @Test
    public void testGetCrewMemberType() {

        Person person = testObjectsFactory.createPerson();
        testObjectsFactory.createCrewMember(person, CrewMemberType.CAMERAMEN);

        List<CrewMember> res = crewMemberRepository.findByCrewMemberType(CrewMemberType.CAMERAMEN);
        Assertions.assertThat(res).extracting(CrewMember::getCrewMemberType).
                isEqualTo(Arrays.asList(CrewMemberType.CAMERAMEN));

    }

    @Test
    public void testGetPersonCrewMembers() {

        Person person = testObjectsFactory.createPerson();
        CrewMember crewMember = testObjectsFactory.createCrewMember(person, CrewMemberType.DIRECTOR);
        CrewMember crewMember2 = testObjectsFactory.createCrewMember(person, CrewMemberType.DIRECTOR);

        List<CrewMember> res = crewMemberRepository.findByPersonIdAndCrewMemberType(person.getId(),
                CrewMemberType.DIRECTOR);

        Assertions.assertThat(res).extracting(CrewMember::getId).isEqualTo(Arrays.asList(crewMember.getId(),
                crewMember2.getId()));
    }

    @Test
    public void testGetPersonCrewMemberQuery() {

        Person person = testObjectsFactory.createPerson();
        CrewMember crewMember = testObjectsFactory.createCrewMember(person, CrewMemberType.DIRECTOR);
        CrewMember crewMember2 = testObjectsFactory.createCrewMember(person, CrewMemberType.DIRECTOR);

        List<CrewMember> res = crewMemberRepository.
                findCrewMembersForPerson(CrewMemberType.DIRECTOR, person.getId());

        Assertions.assertThat(res).extracting(CrewMember::getId).
                isEqualTo(Arrays.asList(crewMember.getId(), crewMember2.getId()));
    }
}
