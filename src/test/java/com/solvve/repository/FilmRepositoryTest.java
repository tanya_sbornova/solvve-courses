package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.FilmGenre;
import com.solvve.domain.FilmStatus;
import com.solvve.dto.film.FilmFilter;
import com.solvve.dto.film.FilmInLeaderBoardReadDTO;
import com.solvve.service.FilmService;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class FilmRepositoryTest extends BaseTest {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private FilmService filmService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testCreatedAtIsSet() {

        Film film = testObjectsFactory.createFilm();
        film.setCreatedAt(LocalDateTime.of(2020, 01, 10, 11, 0, 0).
                toInstant(ZoneOffset.UTC));

        film = filmRepository.save(film);

        Instant createdAtBeforeReload = film.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        film = filmRepository.findById(film.getId()).get();

        Instant createdAtAfterReload = film.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        Film film = testObjectsFactory.createFilm();
        film.setUpdatedAt(LocalDateTime.of(2020, 01, 10, 11, 0, 0).
                toInstant(ZoneOffset.UTC));

        film = filmRepository.save(film);

        Instant updatedAtBeforeReload = film.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        film = filmRepository.findById(film.getId()).get();

        Instant updatedAtAfterReload = film.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(1000);
        film.setBudget(90000);
        filmRepository.save(film);

        film = filmRepository.findById(film.getId()).get();
        Instant updatedAtAfterAnotherReload = film.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {
        Film film = testObjectsFactory.createFilm();
        assertNotNull(film.getId());
        assertTrue(filmRepository.findById(film.getId()).isPresent());
    }

    @Test
    public void testGetFilmsWithEmptyFilter() {
        Film f1 = testObjectsFactory.createFilm();
        Film f2 = testObjectsFactory.createFilm();

        FilmFilter filter = new FilmFilter();
        Assertions.assertThat(filmService.getFilms(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(f1.getId(), f2.getId());
    }

    @Test
    public void testGetFilmsByGenre() {
        Film f1 = testObjectsFactory.createFilm(FilmGenre.SCI_FI);
        Film f2 = testObjectsFactory.createFilm(FilmGenre.DRAMA);

        FilmFilter filter = new FilmFilter();
        filter.setGenre(FilmGenre.SCI_FI);
        Assertions.assertThat(filmService.getFilms(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(f1.getId());
    }

    @Test
    public void testGetFilmsByStatus() {
        Film f1 = testObjectsFactory.createFilm();
        f1.setStatus(FilmStatus.RELEASED);
        filmRepository.save(f1);
        Film f2 = testObjectsFactory.createFilm();
        f2.setStatus(FilmStatus.NOT_RELEASED);
        filmRepository.save(f2);

        FilmFilter filter = new FilmFilter();
        filter.setStatus(FilmStatus.NOT_RELEASED);
        Assertions.assertThat(filmService.getFilms(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(f2.getId());
    }

    @Test
    public void testGetFilmsByDatePrimeInterval() {
        Film f1 = testObjectsFactory.createFilm();
        f1.setDatePrime(LocalDate.of(1999,5,21));
        filmRepository.save(f1);
        Film f2 = testObjectsFactory.createFilm();
        f2.setDatePrime(LocalDate.of(2019,5,21));
        filmRepository.save(f2);

        FilmFilter filter = new FilmFilter();
        filter.setDatePrimeFrom(LocalDate.of(1998,3,21));
        filter.setDatePrimeTo(LocalDate.of(2000,3,21));
        Assertions.assertThat(filmService.getFilms(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(f1.getId());

        filter.setDatePrimeFrom(LocalDate.of(2018,3,21));
        filter.setDatePrimeTo(LocalDate.of(2020,3,21));
        Assertions.assertThat(filmService.getFilms(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(f2.getId());

    }

    @Test
    public void testGetFilmsByBudgetInterval() {
        Film f1 = testObjectsFactory.createFilm();
        f1.setBudget(35000);
        filmRepository.save(f1);
        Film f2 = testObjectsFactory.createFilm();
        f2.setBudget(45000);
        filmRepository.save(f2);

        FilmFilter filter = new FilmFilter();
        filter.setBudgetFrom(40000);
        filter.setBudgetTo(50000);
        Assertions.assertThat(filmService.getFilms(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(f2.getId());
    }

    @Test
    public void testGetFilmsByAllFilters() {
        Film f1 = testObjectsFactory.createFilm();
        f1.setStatus(FilmStatus.RELEASED);
        f1.setBudget(65000);
        f1.setDatePrime(LocalDate.of(1999,5,21));
        filmRepository.save(f1);
        Film f2 = testObjectsFactory.createFilm();

        FilmFilter filter = new FilmFilter();
        filter.setStatus(FilmStatus.RELEASED);
        filter.setBudgetFrom(60000);
        filter.setBudgetTo(70000);
        filter.setDatePrimeFrom(LocalDate.of(1999,1,1));
        filter.setDatePrimeTo(LocalDate.of(2000,1,1));
        Assertions.assertThat(filmService.getFilms(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(f1.getId());
    }

    @Test
    public void testGetIdsOfFilms() {

        Set<UUID> expectedIdsOfFilms = new HashSet<>();
        expectedIdsOfFilms.add(testObjectsFactory.createFilm().getId());
        expectedIdsOfFilms.add(testObjectsFactory.createFilm().getId());
        expectedIdsOfFilms.add(testObjectsFactory.createFilm().getId());
        inTransaction(() -> {
            Assert.assertEquals(expectedIdsOfFilms, filmRepository.getIdsOfFilms().collect(Collectors.toSet()));
        });
    }

    @Test
    public void testGetFilmsWithEmptyFilterWithPagingAndSorting() {
        Film f1 = testObjectsFactory.createFilm();
        f1.setTitle("Title2");
        filmRepository.save(f1);
        Film f2 = testObjectsFactory.createFilm();
        f2.setTitle("Title1");
        filmRepository.save(f2);

        FilmFilter filter = new FilmFilter();
        PageRequest pageRequest = PageRequest.of(0, 50, Sort.by(Sort.Direction.DESC, "title"));

        Assertions.assertThat(filmService.getFilms(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(f1.getId(), f2.getId()));
    }

    @Test
    public void getFilmsLeaderBoard() {
        int filmsCount = 20;
        Set<UUID> filmIds = new HashSet<>();
        for (int i = 1; i < filmsCount; i++) {
            Film film = testObjectsFactory.createFlatFilmWithRating();
            filmIds.add(film.getId());
            testObjectsFactory.createRatingFilm(film, Math.random() * (7.0 - 3.0 + 1) + 3.0);
            testObjectsFactory.createRatingFilm(film, Math.random() * (7.0 - 3.0 + 1) + 3.0);
            testObjectsFactory.createRatingFilm(film, 1.0);
            testObjectsFactory.createRatingFilm(film, 2.0);
            testObjectsFactory.createRatingFilm(film, 9.0);
            testObjectsFactory.createRatingFilm(film, 9.0);
            testObjectsFactory.createRatingFilm(film, 10.0);
            filmService.updateAverageRatingOfFilm(film.getId());
        }
        List<FilmInLeaderBoardReadDTO> filmsLeaderBoard = filmRepository.getFilmsLeaderBoard();
        Assertions.assertThat(filmsLeaderBoard).
                isSortedAccordingTo(Comparator.comparing(FilmInLeaderBoardReadDTO::getAverageRating).reversed());
        Assert.assertEquals(filmIds,
                filmsLeaderBoard.stream().map(FilmInLeaderBoardReadDTO::getId).collect(Collectors.toSet()));

        for(FilmInLeaderBoardReadDTO f: filmsLeaderBoard) {
            Assert.assertNotNull(f.getText());
            Assert.assertNotNull(f.getAverageRating());
            Assert.assertEquals(2, (long)f.getMinRatingsCount());
            Assert.assertEquals(3, (long)f.getMaxRatingsCount());
        }
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }

}

