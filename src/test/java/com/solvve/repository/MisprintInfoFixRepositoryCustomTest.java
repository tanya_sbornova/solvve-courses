package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.MisprintInfo;
import com.solvve.domain.MisprintObject;
import com.solvve.domain.Role;
import com.solvve.dto.misprintinfofix.MisprintInfoFixFilter;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MisprintInfoFixRepositoryCustomTest extends BaseTest {

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private MisprintInfoFixRepositoryCustomImpl mpFixRepository;

    @Test
    public void testGetMisprintsInfoWithEmptyFilter() {

        MisprintInfoFixFilter filter = new MisprintInfoFixFilter();

        Page<MisprintInfo> misprintsInfo = mpFixRepository.findByFilterMisprintsInfo(filter,
                MisprintObject.FILM, Pageable.unpaged());
        Assert.assertEquals(misprintsInfo.getTotalElements(), 0);
    }

    @Test
    public void testGetIdsOfMisprintInfoRole() {
        Role role = testObjectsFactory.createRole();
        Role role2 = testObjectsFactory.createRole();
        List<UUID> expectedIdsOfMisprints = new ArrayList<>();
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoRoleWithParam(role, 0, 3, "Neo", "NEo").getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoRoleWithParam(role, 1, 2, "e", "E").getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoRoleWithParam(role, 1, 3, "eo", "Eo").getId());
        testObjectsFactory.createMpInfoRoleWithParam(role, 5, 16, "XXXXXXXXXXX", "");
        testObjectsFactory.createMpInfoRoleWithParam(role2, 1, 3, "eo", "Eo");
        MisprintInfoFixFilter filter = createMisprintInfoFilter(role.getId(), "NEo", 0, 3);
        Page<MisprintInfo> misprintsInfo = mpFixRepository.findByFilterMisprintsInfo(filter,
                MisprintObject.ROLE, Pageable.unpaged());
        Assertions.assertThat(misprintsInfo).extracting("id").
                containsExactlyInAnyOrder(expectedIdsOfMisprints.toArray());
    }

    @Test
    public void testGetIdsOfMisprintInfoRole2() {
        Role role = testObjectsFactory.createRole();
        Role role2 = testObjectsFactory.createRole();
        List<UUID> expectedIdsOfMisprints = new ArrayList<>();
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoRoleWithParam(role, 0, 3, "Neo", "NEo").getId());
        testObjectsFactory.createMpInfoRoleWithParam(role, 1, 2, "e", "E");
        testObjectsFactory.createMpInfoRoleWithParam(role, 1, 3, "eo", "Eo");
        testObjectsFactory.createMpInfoRoleWithParam(role, 5, 16, "XXXXXXXXXXX", "");
        testObjectsFactory.createMpInfoRoleWithParam(role2, 1, 3, "eo", "Eo");
        MisprintInfoFixFilter filter = createMisprintInfoFilter(role.getId(), "NEo", null, null);
        Page<MisprintInfo> misprintsInfo = mpFixRepository.findByFilterMisprintsInfo(filter,
                MisprintObject.ROLE, Pageable.unpaged());
        Assertions.assertThat(misprintsInfo).extracting("id").
                containsExactlyInAnyOrder(expectedIdsOfMisprints.toArray());
    }

    @Test
    public void testGetIdsOfMisprintInfoRole3() {
        Role role = testObjectsFactory.createRole();
        Role role2 = testObjectsFactory.createRole();
        List<UUID> expectedIdsOfMisprints = new ArrayList<>();
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoRoleWithParam(role, 0, 3, "Neo", "NEo").getId());
        testObjectsFactory.createMpInfoRoleWithParam(role, 1, 2, "e", "E");
        testObjectsFactory.createMpInfoRoleWithParam(role, 1, 3, "eo", "Eo");
        testObjectsFactory.createMpInfoRoleWithParam(role, 5, 16, "XXXXXXXXXXX", "XXXXXXXXXXX").getId();
        testObjectsFactory.createMpInfoRoleWithParam(role2, 1, 3, "eo", "Eo");
        MisprintInfoFixFilter filter = createMisprintInfoFilter(role.getId(), "NEo", null, 3);
        Page<MisprintInfo> misprintsInfo = mpFixRepository.findByFilterMisprintsInfo(filter,
                MisprintObject.ROLE, Pageable.unpaged());
        Assertions.assertThat(misprintsInfo).extracting("id").
                containsExactlyInAnyOrder(expectedIdsOfMisprints.toArray());
    }

    @Test
    public void testGetIdsOfMisprintInfoRole4() {
        Role role = testObjectsFactory.createRole();
        Role role2 = testObjectsFactory.createRole();
        List<UUID> expectedIdsOfMisprints = new ArrayList<>();
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoRoleWithParam(role, 0, 3, "Neo", "NEo").getId());
        testObjectsFactory.createMpInfoRoleWithParam(role, 1, 2, "e", "E");
        testObjectsFactory.createMpInfoRoleWithParam(role, 1, 3, "eo", "Eo");
        testObjectsFactory.createMpInfoRoleWithParam(role, 5, 16, "XXXXXXXXXXX", "XXXXXXXXXXX");
        testObjectsFactory.createMpInfoRoleWithParam(role2, 1, 3, "eo", "Eo");
        MisprintInfoFixFilter filter = createMisprintInfoFilter(role.getId(), "NEo", 0, null);
        Page<MisprintInfo> misprintsInfo = mpFixRepository.findByFilterMisprintsInfo(filter,
                MisprintObject.ROLE, Pageable.unpaged());
        Assertions.assertThat(misprintsInfo).extracting("id").
                containsExactlyInAnyOrder(expectedIdsOfMisprints.toArray());
    }

    @Test
    public void testGetIdsOfMisprintInfoRole5() {
        Role role = testObjectsFactory.createRole();
        Role role2 = testObjectsFactory.createRole();
        List<UUID> expectedIdsOfMisprints = new ArrayList<>();
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoRoleWithParam(role, 0, 3, "Neo", "NEo").getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoRoleWithParam(role, 1, 2, "e", "E").getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoRoleWithParam(role, 1, 3, "eo", "Eo").getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoRoleWithParam(role, 5, 16, "XXXXXXXXXXX", "")
                .getId());
        testObjectsFactory.createMpInfoRoleWithParam(role2, 1, 3, "eo", "Eo");
        MisprintInfoFixFilter filter = createMisprintInfoFilter(role.getId(), null, 0, 3);
        Page<MisprintInfo> misprintsInfo = mpFixRepository.findByFilterMisprintsInfo(filter,
                MisprintObject.ROLE, Pageable.unpaged());
        Assertions.assertThat(misprintsInfo).extracting("id").
                containsExactlyInAnyOrder(expectedIdsOfMisprints.toArray());
    }

    private MisprintInfoFixFilter createMisprintInfoFilter(UUID contentId,
                                                           String incorrecText,
                                                           Integer startIndex,
                                                           Integer endIndex) {
        MisprintInfoFixFilter filter = new MisprintInfoFixFilter();
        filter.setIncorrectText(incorrecText);
        filter.setContentId(contentId);
        filter.setStartIndex(startIndex);
        filter.setEndIndex(endIndex);
        return filter;
    }
}
