package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.Actor;
import com.solvve.domain.Person;
import com.solvve.domain.RatingRole;
import com.solvve.domain.Role;
import com.solvve.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class RatingRoleRepositoryTest extends BaseTest {

    @Autowired
    private RatingRoleRepository ratingRoleRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {

        Role role = testObjectsFactory.createRole();
        RatingRole ratingRole = testObjectsFactory.createRatingRole(role);
        ratingRole.setCreatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        ratingRole = ratingRoleRepository.save(ratingRole);

        Instant createdAtBeforeReload = ratingRole.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        ratingRole = ratingRoleRepository.findById(ratingRole.getId()).get();

        Instant createdAtAfterReload = ratingRole.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        Role role = testObjectsFactory.createRole();
        RatingRole ratingRole = testObjectsFactory.createRatingRole(role);
        ratingRole.setUpdatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        ratingRole = ratingRoleRepository.save(ratingRole);

        Instant updatedAtBeforeReload = ratingRole.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        ratingRole = ratingRoleRepository.findById(ratingRole.getId()).get();

        Instant updatedAtAfterReload = ratingRole.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(1000);
        ratingRole.setRating(9.0);
        ratingRoleRepository.save(ratingRole);

        ratingRole = ratingRoleRepository.findById(ratingRole.getId()).get();
        Instant updatedAtAfterAnotherReload = ratingRole.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {
        Role role = testObjectsFactory.createRole();
        RatingRole ratingRole = testObjectsFactory.createRatingRole(role);
        assertNotNull(ratingRole.getId());
        assertTrue(ratingRoleRepository.findById(ratingRole.getId()).isPresent());
    }

    @Test
    public void testCalcAverageRatingActorOfRole() {

        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        Role role1 = testObjectsFactory.createRole("Neo", actor);
        RatingRole ratingRole1 = testObjectsFactory.createRatingRole(role1, 8.0);
        role1.setRatingRoles(List.of(ratingRole1));
        roleRepository.save(role1);
        Role role2 = testObjectsFactory.createRole("Kevin Lomax", actor);
        RatingRole ratingRole2 = testObjectsFactory.createRatingRole(role2, 8.5);
        role2.setRatingRoles(List.of(ratingRole2));
        roleRepository.save(role2);

        Assert.assertEquals(8.25, ratingRoleRepository.calcAverageRatingActorOfRole(actor.getId()), Double.MIN_NORMAL);
    }

}