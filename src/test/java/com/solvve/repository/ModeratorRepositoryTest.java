package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.Moderator;
import com.solvve.domain.UserStatus;
import com.solvve.dto.moderator.ModeratorFilter;
import com.solvve.service.ModeratorService;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ModeratorRepositoryTest extends BaseTest {

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Autowired
    private ModeratorService moderatorService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {

        Moderator moderator = testObjectsFactory.generateFlatEntityWithoutId(Moderator.class);
        moderator.setCreatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        moderator = moderatorRepository.save(moderator);

        Instant createdAtBeforeReload = moderator.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        moderator = moderatorRepository.findById(moderator.getId()).get();

        Instant createdAtAfterReload = moderator.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        Moderator moderator = testObjectsFactory.generateFlatEntityWithoutId(Moderator.class);
        moderator.setUpdatedAt(LocalDateTime.of(2020,01,10,11,0, 0).
                toInstant(ZoneOffset.UTC));

        moderator = moderatorRepository.save(moderator);

        Instant updatedAtBeforeReload = moderator.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        moderator = moderatorRepository.findById(moderator.getId()).get();

        Instant updatedAtAfterReload = moderator.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(1000);
        moderator.setEncodedPassword("tyuo999");
        moderatorRepository.save(moderator);

        moderator = moderatorRepository.findById(moderator.getId()).get();
        Instant updatedAtAfterAnotherReload = moderator.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {
        Moderator moderator = testObjectsFactory.generateFlatEntityWithoutId(Moderator.class);
        moderator = moderatorRepository.save(moderator);
        assertNotNull(moderator.getId());
        assertTrue(moderatorRepository.findById(moderator.getId()).isPresent());
    }

    @Test
    public void testGetModeratorsWithEmptyFilter() {
        Moderator m1 = testObjectsFactory.createModerator();
        Moderator m2 = testObjectsFactory.createModerator();

        ModeratorFilter filter = new ModeratorFilter();
        Assertions.assertThat(moderatorService.getModerators(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(m1.getId(), m2.getId());
    }

    @Test
    public void testGetModeratorsByEmail() {
        Moderator m1 = testObjectsFactory.generateFlatEntityWithoutId(Moderator.class);
        m1.setEmail("test1@mail.ru");
        moderatorRepository.save(m1);

        Moderator m2 = testObjectsFactory.generateFlatEntityWithoutId(Moderator.class);
        m2.setEmail("test2@mail.ru");
        moderatorRepository.save(m2);

        ModeratorFilter filter = new ModeratorFilter();
        filter.setEmail("test1@mail.ru");
        Assertions.assertThat(moderatorService.getModerators(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(m1.getId());
    }

    @Test
    public void testGetModeratorsByUserStatus() {
        Moderator m1 = testObjectsFactory.generateFlatEntityWithoutId(Moderator.class);
        m1.setUserStatus(UserStatus.NEW);
        moderatorRepository.save(m1);
        Moderator m2 = testObjectsFactory.createModerator();
        m2.setUserStatus(UserStatus.ACTIVE);
        moderatorRepository.save(m2);

        ModeratorFilter filter = new ModeratorFilter();
        filter.setUserStatus(UserStatus.NEW);
        Assertions.assertThat(moderatorService.getModerators(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(m1.getId());
    }

    @Test
    public void testGetModeratorsByAllFilters() {
        Moderator m1 = testObjectsFactory.generateFlatEntityWithoutId(Moderator.class);
        m1.setEmail("test1@mail.ru");
        m1.setUserStatus(UserStatus.NEW);
        moderatorRepository.save(m1);

        Moderator m2 = testObjectsFactory.generateFlatEntityWithoutId(Moderator.class);
        m2.setEmail("test2@mail.ru");
        m2.setUserStatus(UserStatus.BLOCKED);
        moderatorRepository.save(m2);

        ModeratorFilter filter = new ModeratorFilter();
        filter.setEmail("test1@mail.ru");
        filter.setUserStatus(UserStatus.NEW);
        Assertions.assertThat(moderatorService.getModerators(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(m1.getId());
    }

}
