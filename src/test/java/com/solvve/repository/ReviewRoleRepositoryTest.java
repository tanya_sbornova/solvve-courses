package com.solvve.repository;

import com.solvve.BaseTest;
import com.solvve.domain.RegisteredUser;
import com.solvve.domain.ReviewRole;
import com.solvve.domain.ReviewStatus;
import com.solvve.domain.Role;
import com.solvve.dto.reviewrole.ReviewRoleFilter;
import com.solvve.service.ReviewRoleService;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ReviewRoleRepositoryTest extends BaseTest {

    @Autowired
    private ReviewRoleRepository reviewRoleRepository;

    @Autowired
    private ReviewRoleService reviewRoleService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testCreatedAtIsSet() {

        Role role = testObjectsFactory.createRole();
        ReviewRole reviewRole = testObjectsFactory.createReviewRole(role);
        reviewRole.setCreatedAt(LocalDateTime.of(2020, 1, 10, 11, 0, 0).
                toInstant(ZoneOffset.UTC));

        reviewRole = reviewRoleRepository.save(reviewRole);

        Instant createdAtBeforeReload = reviewRole.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        reviewRole = reviewRoleRepository.findById(reviewRole.getId()).get();

        Instant createdAtAfterReload = reviewRole.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() throws InterruptedException {

        Role role = testObjectsFactory.createRole();
        ReviewRole reviewRole = testObjectsFactory.createReviewRole(role);
        reviewRole.setUpdatedAt(LocalDateTime.of(2020, 1, 10, 11, 0, 0).
                toInstant(ZoneOffset.UTC));

        reviewRole = reviewRoleRepository.save(reviewRole);

        Instant updatedAtBeforeReload = reviewRole.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);

        reviewRole = reviewRoleRepository.findById(reviewRole.getId()).get();

        Instant updatedAtAfterReload = reviewRole.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Thread.sleep(1000);
        reviewRole.setIsSpoiler(true);
        reviewRoleRepository.save(reviewRole);

        reviewRole = reviewRoleRepository.findById(reviewRole.getId()).get();
        Instant updatedAtAfterAnotherReload = reviewRole.getUpdatedAt();

        Assert.assertNotNull(updatedAtAfterAnotherReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterAnotherReload);
        Assert.assertThat(updatedAtAfterAnotherReload.isAfter(updatedAtBeforeReload), is(true));
    }

    @Test
    public void testSave() {
        Role role = testObjectsFactory.createRole();
        ReviewRole ratingRole = testObjectsFactory.createReviewRole(role);
        assertNotNull(ratingRole.getId());
        assertTrue(reviewRoleRepository.findById(ratingRole.getId()).isPresent());
    }

    @Test
    public void testGetReviewRoleWithEmptyFilter() {
        Role role = testObjectsFactory.createFlatRole();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        ReviewRole rr1 = testObjectsFactory.createReviewRole(ReviewStatus.FIXED, true,
                registeredUser, role);
        reviewRoleRepository.save(rr1);

        ReviewRole rr2 = testObjectsFactory.createReviewRole(ReviewStatus.NEED_TO_MODERATE, true,
                registeredUser, role);
        reviewRoleRepository.save(rr2);

        ReviewRoleFilter filter = new ReviewRoleFilter();
        Assertions.assertThat(reviewRoleService.getReviewRoles(filter, Pageable.unpaged()).getData())
                .extracting("id")
                .containsExactlyInAnyOrder(rr1.getId(), rr2.getId());
    }

    @Test
    public void testGetReviewRolesByStatus() {
        Role role = testObjectsFactory.createFlatRole();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        ReviewRole rr1 = testObjectsFactory.createReviewRole(ReviewStatus.FIXED, true,
                registeredUser, role);
        reviewRoleRepository.save(rr1);

        ReviewRole rr2 = testObjectsFactory.createReviewRole(ReviewStatus.NEED_TO_MODERATE, true,
                registeredUser, role);
        reviewRoleRepository.save(rr2);

        ReviewRoleFilter filter = new ReviewRoleFilter();
        filter.setStatus(ReviewStatus.FIXED);
        Assertions.assertThat(reviewRoleService.getReviewRoles(filter, Pageable.unpaged()).getData())
                .extracting("id")
                .containsExactlyInAnyOrder(rr1.getId());
    }

    @Test
    public void testGetReviewRolesByIsSpoiler() {
        Role role = testObjectsFactory.createFlatRole();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        ReviewRole rr1 = testObjectsFactory.createReviewRole(ReviewStatus.FIXED, true,
                registeredUser, role);
        reviewRoleRepository.save(rr1);

        ReviewRole rr2 = testObjectsFactory.createReviewRole(ReviewStatus.FIXED, false,
                registeredUser, role);
        reviewRoleRepository.save(rr2);

        ReviewRoleFilter filter = new ReviewRoleFilter();
        filter.setIsSpoiler(true);
        Assertions.assertThat(reviewRoleService.getReviewRoles(filter, Pageable.unpaged()).getData())
                .extracting("id")
                .containsExactlyInAnyOrder(rr1.getId());
    }

    @Test
    public void testGetreviewRolesByRegisteredUserId() {
        Role role = testObjectsFactory.createFlatRole();
        RegisteredUser registeredUser1 = testObjectsFactory.createRegisteredUser();
        ReviewRole rr1 = testObjectsFactory.createReviewRole(ReviewStatus.FIXED, false,
                registeredUser1, role);
        reviewRoleRepository.save(rr1);

        RegisteredUser registeredUser2 = testObjectsFactory.createRegisteredUser();
        ReviewRole rr2 = testObjectsFactory.createReviewRole(ReviewStatus.FIXED, false,
                registeredUser2, role);
        reviewRoleRepository.save(rr2);

        ReviewRoleFilter filter = new ReviewRoleFilter();
        filter.setRegisteredUserId(registeredUser2.getId());
        Assertions.assertThat(reviewRoleService.getReviewRoles(filter, Pageable.unpaged()).getData())
                .extracting("id")
                .containsExactlyInAnyOrder(rr2.getId());
    }

    @Test
    public void testGetReviewRolesByFilmId() {
        Role role = testObjectsFactory.createFlatRole();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        ReviewRole rr1 = testObjectsFactory.createReviewRole(ReviewStatus.FIXED, false,
                registeredUser, role);
        reviewRoleRepository.save(rr1);

        Role role2 = testObjectsFactory.createFlatRole();
        ReviewRole rr2 = testObjectsFactory.createReviewRole(ReviewStatus.FIXED, false,
                registeredUser, role2);
        reviewRoleRepository.save(rr2);

        ReviewRoleFilter filter = new ReviewRoleFilter();
        filter.setRoleId(role2.getId());
        Assertions.assertThat(reviewRoleService.getReviewRoles(filter, Pageable.unpaged()).getData())
                .extracting("id")
                .containsExactlyInAnyOrder(rr2.getId());
    }

    @Test
    public void testGetReviewRolesByAllFilters() {
        Role role1 = testObjectsFactory.createFlatRole();
        RegisteredUser registeredUser1 = testObjectsFactory.createRegisteredUser();
        ReviewRole rr1 = testObjectsFactory.createReviewRole(ReviewStatus.FIXED, false,
                registeredUser1, role1);
        reviewRoleRepository.save(rr1);

        Role role2 = testObjectsFactory.createFlatRole();
        RegisteredUser registeredUser2 = testObjectsFactory.createRegisteredUser();
        ReviewRole rr2 = testObjectsFactory.createReviewRole(ReviewStatus.CANCELLED, true,
                registeredUser2, role2);
        reviewRoleRepository.save(rr2);

        ReviewRoleFilter filter = new ReviewRoleFilter();
        filter.setStatus(ReviewStatus.FIXED);
        filter.setIsSpoiler(false);
        filter.setRegisteredUserId(registeredUser1.getId());
        filter.setRoleId(role1.getId());
        Assertions.assertThat(reviewRoleService.getReviewRoles(filter, Pageable.unpaged()).getData())
                .extracting("id")
                .containsExactlyInAnyOrder(rr1.getId());
    }
}
