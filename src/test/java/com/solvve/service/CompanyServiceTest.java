package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.Film;
import com.solvve.exception.LinkDuplicatedException;
import com.solvve.repository.FilmRepository;
import com.solvve.util.TestObjectsFactory;
import com.solvve.domain.Company;
import com.solvve.dto.company.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.CompanyRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.*;
import java.util.stream.Collectors;

public class CompanyServiceTest extends BaseTest {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void getCompanies() {

        List<Company> companies = new ArrayList<>();
        companyRepository.findAll().forEach(companies::add);

        List<UUID> companiesId = companies.stream().
                map(Company::getId).collect(Collectors.toList());

        List<CompanyReadDTO> companiesReadDTO = companyService.getCompanies();

        Assertions.assertThat(companiesReadDTO).extracting("id").
                containsExactlyInAnyOrder(companiesId.toArray());
    }

    @Test
    public void testGetCompany() {

        Company company = testObjectsFactory.createCompany();

        CompanyReadDTO readDTO = companyService.getCompany(company.getId());

        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(company);
    }

    @Test
    public void testGetSimpleCompany() {

        Company company = testObjectsFactory.createCompany();

        CompanyReadDTO readDTO = companyService.getCompany(company.getId());

        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(company);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetCompanyWrongId() {
        companyService.getCompany(UUID.randomUUID());
    }

    @Test
    public void testCreateCompany() {

        CompanyCreateDTO create = testObjectsFactory.createCompanyCreateDTO();

        CompanyReadDTO read = companyService.createCompany(create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Company company = companyRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(company);
    }

    @Test
    public void testAddCompanyToFilm() {

        Film film = testObjectsFactory.createFlatFilm();
        List<Company> companies = List.of(testObjectsFactory.createCompany());
        UUID companyId = companies.get(0).getId();

        List<CompanyReadDTO> companiesReadDTO = companyService.addCompanyToFilm(film.getId(), companyId);

        CompanyReadDTO expectedRead = new CompanyReadDTO();
        expectedRead.setId(companyId);
        expectedRead.setCountryName(companies.get(0).getCountryName());
        expectedRead.setName(companies.get(0).getName());
        expectedRead.setCreatedAt(companies.get(0).getCreatedAt());
        expectedRead.setUpdatedAt(companies.get(0).getUpdatedAt());
        Assertions.assertThat(companiesReadDTO).containsExactlyInAnyOrder(expectedRead);

        inTransaction(() -> {
            Film filmAfterSave = filmRepository.findById(film.getId()).get();
            Assertions.assertThat(filmAfterSave.getCompanies()).extracting(Company::getId)
                    .containsExactlyInAnyOrder(companyId);
        });
    }

    @Test
    public void testDuplicatedCompany() {

        Film film = testObjectsFactory.createFlatFilm();
        List<Company> companys = List.of(testObjectsFactory.createCompany());
        UUID companyId = companys.get(0).getId();

        companyService.addCompanyToFilm(film.getId(), companyId);
        Assertions.assertThatThrownBy(() -> {
            companyService.addCompanyToFilm(film.getId(), companyId);
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongFilmId() {

        UUID wrongFilmId = UUID.randomUUID();
        List<Company> companys = List.of(testObjectsFactory.createCompany(),
                testObjectsFactory.createCompany());
        UUID companyId = companys.get(0).getId();

        companyService.addCompanyToFilm(wrongFilmId, companyId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongCompanyId() {

        Film film = testObjectsFactory.createFlatFilm();
        UUID wrongCompanyId = UUID.randomUUID();
        companyService.addCompanyToFilm(film.getId(), wrongCompanyId);
    }

    @Test
    public void testDeleteCompany() {

        Company company = testObjectsFactory.createCompany();

        companyService.deleteCompany(company.getId());
        Assert.assertFalse(companyRepository.existsById(company.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteCompanyNotFound() {

        companyService.deleteCompany(UUID.randomUUID());
    }

    @Test
    public void testRemoveCompanyFromFilm() {

        Film film = testObjectsFactory.createFlatFilm();
        List<Company> companies = List.of(testObjectsFactory.createCompany(),
                testObjectsFactory.createCompany());
        UUID companyId = companies.get(0).getId();
        companyService.addCompanyToFilm(film.getId(), companyId);

        List<CompanyReadDTO> remainingCompanies = companyService
                .removeCompanyFromFilm(film.getId(), companyId);
        Assert.assertTrue(remainingCompanies.isEmpty());

        inTransaction(() -> {
            Film filmAfterRemove = filmRepository.findById(film.getId()).get();
            Assertions.assertThat(filmAfterRemove.getCompanies().isEmpty());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedCompany() {

        Film film = testObjectsFactory.createFlatFilm();
        List<Company> companys = List.of(testObjectsFactory.createCompany(),
                testObjectsFactory.createCompany());
        UUID companyId = companys.get(0).getId();

        companyService.removeCompanyFromFilm(film.getId(), companyId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedCompany() {

        Film film = testObjectsFactory.createFlatFilm();
        companyService.removeCompanyFromFilm(film.getId(), UUID.randomUUID());
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveCompanyValidation() {

        Company company = new Company();
        companyRepository.save(company);
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }
}
