package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.ReviewFilmLikeDislike;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePatchDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePutDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ReviewFilmLikeDislikeRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.util.UUID;

public class ReviewFilmLikeDislikeServiceTest extends BaseTest {

    @Autowired
    private ReviewFilmLikeDislikeRepository reviewFilmLikeDislikeRepository;

    @Autowired
    private ReviewFilmLikeDislikeService reviewFilmLikeDislikeService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetReviewFilmLikeDislike() {

        ReviewFilmLikeDislike reviewFilmLikeDislike = testObjectsFactory.createReviewFilmLikeDislike();

        ReviewFilmLikeDislikeReadDTO readDTO =
                reviewFilmLikeDislikeService.getReviewFilmLikeDislike(reviewFilmLikeDislike.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(reviewFilmLikeDislike,
                "reviewFilmId", "registeredUserId");
        Assert.assertEquals(readDTO.getReviewFilmId(), reviewFilmLikeDislike.getReviewFilm().getId());
        Assert.assertEquals(readDTO.getRegisteredUserId(), reviewFilmLikeDislike.getRegisteredUser().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetReviewFilmLikeDislikeWrongId() {

        reviewFilmLikeDislikeService.getReviewFilmLikeDislike(UUID.randomUUID());
    }

    @Test
    public void testPatchReviewFilmLikeDislike() {

        ReviewFilmLikeDislike reviewFilmLikeDislike = testObjectsFactory.createReviewFilmLikeDislike();

        ReviewFilmLikeDislikePatchDTO patch = new ReviewFilmLikeDislikePatchDTO();
        patch.setIsLike(false);

        ReviewFilmLikeDislikeReadDTO read =
                reviewFilmLikeDislikeService.patchReviewFilmLikeDislike(reviewFilmLikeDislike.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        reviewFilmLikeDislike = reviewFilmLikeDislikeRepository.findById(read.getId()).get();
        Assertions.assertThat(reviewFilmLikeDislike).isEqualToIgnoringGivenFields(read,
                "reviewFilm", "registeredUser");
        Assert.assertEquals(read.getReviewFilmId(), reviewFilmLikeDislike.getReviewFilm().getId());
        Assert.assertEquals(read.getRegisteredUserId(), reviewFilmLikeDislike.getRegisteredUser().getId());
    }

    @Test
    public void testPatchReviewFilmLikeDislikeEmptyPatch() {

        ReviewFilmLikeDislike reviewFilmLikeDislike = testObjectsFactory.createReviewFilmLikeDislike();

        ReviewFilmLikeDislikePatchDTO patch = new ReviewFilmLikeDislikePatchDTO();
        ReviewFilmLikeDislikeReadDTO read =
                reviewFilmLikeDislikeService.patchReviewFilmLikeDislike(reviewFilmLikeDislike.getId(), patch);

        Assert.assertNotNull(read.getIsLike());

        ReviewFilmLikeDislike reviewFilmLikeDislikeAfterUpdate =
                reviewFilmLikeDislikeRepository.findById(read.getId()).get();

        Assert.assertNotNull(reviewFilmLikeDislikeAfterUpdate.getIsLike());
        Assertions.assertThat(reviewFilmLikeDislike).isEqualToIgnoringGivenFields(read,
                "reviewFilm", "registeredUser");
        Assert.assertEquals(read.getReviewFilmId(), reviewFilmLikeDislike.getReviewFilm().getId());
        Assert.assertEquals(read.getRegisteredUserId(), reviewFilmLikeDislike.getRegisteredUser().getId());
    }

    @Test
    public void testPutReviewFilmLikeDislike() {

        ReviewFilmLikeDislike reviewFilmLikeDislike = testObjectsFactory.createReviewFilmLikeDislike();

        ReviewFilmLikeDislikePutDTO put = new ReviewFilmLikeDislikePutDTO();
        put.setIsLike(false);

        ReviewFilmLikeDislikeReadDTO read =
                reviewFilmLikeDislikeService.updateReviewFilmLikeDislike(reviewFilmLikeDislike.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        reviewFilmLikeDislike = reviewFilmLikeDislikeRepository.findById(read.getId()).get();
        Assertions.assertThat(reviewFilmLikeDislike).isEqualToIgnoringGivenFields(read,
                "reviewFilm", "registeredUser");
        Assert.assertEquals(read.getReviewFilmId(), reviewFilmLikeDislike.getReviewFilm().getId());
        Assert.assertEquals(read.getRegisteredUserId(), reviewFilmLikeDislike.getRegisteredUser().getId());
    }

    @Test
    public void testDeleteReviewFilmLikeDislike() {

        ReviewFilmLikeDislike reviewFilmLikeDislike = testObjectsFactory.createReviewFilmLikeDislike();

        reviewFilmLikeDislikeService.deleteReviewFilmLikeDislike(reviewFilmLikeDislike.getId());
        Assert.assertFalse(reviewFilmLikeDislikeRepository.existsById(reviewFilmLikeDislike.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteReviewFilmLikeDislikeNotFound() {

        reviewFilmLikeDislikeService.deleteReviewFilmLikeDislike(UUID.randomUUID());
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveReviewFilmValidation() {

        ReviewFilmLikeDislike reviewFilmLike = new ReviewFilmLikeDislike();
        reviewFilmLikeDislikeRepository.save(reviewFilmLike);
    }
}


