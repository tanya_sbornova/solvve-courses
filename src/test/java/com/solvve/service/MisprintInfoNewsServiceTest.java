package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.MisprintInfoNews;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.MisprintInfoNewsRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class MisprintInfoNewsServiceTest extends BaseTest {

    @Autowired
    private MisprintInfoNewsRepository misprintInfoRepository;

    @Autowired
    private MisprintInfoNewsService misprintInfoService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetMisprintInfoNews() {

        MisprintInfoNews misprintInfo = testObjectsFactory.createMisprintInfoNews();

        MisprintInfoNewsReadDTO readDTO = misprintInfoService.getMisprintInfoNews(misprintInfo.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(misprintInfo,
                "contentId", "registeredUserId", "contentManagerId");
        Assert.assertEquals(readDTO.getRegisteredUserId(), misprintInfo.getRegisteredUser().getId());
        Assert.assertEquals(readDTO.getContentId(), misprintInfo.getContent().getId());
    }

    @Test
    public void getCrewMembers() {

        List<MisprintInfoNews> misprintsInfo = new ArrayList<>();
        misprintInfoRepository.findAll().forEach(misprintsInfo::add);

        List<UUID> misprintsInfoId = misprintsInfo.stream().
                map(MisprintInfoNews::getId).collect(Collectors.toList());

        List<MisprintInfoNewsReadDTO> misprintsInfoReadDTO = misprintInfoService.getMisprintsInfoNewsNeedToFix();

        Assertions.assertThat(misprintsInfoReadDTO).extracting("id").
                containsExactlyInAnyOrder(misprintsInfoId.toArray());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetMisprintInfoNewsWrongId() {

        misprintInfoService.getMisprintInfoNews(UUID.randomUUID());
    }

    @Test
    public void testDeleteMisprintInfo() {

        MisprintInfoNews misprintInfo = testObjectsFactory.createMisprintInfoNews();

        misprintInfoService.deleteMisprintInfoNews(misprintInfo.getId());
        Assert.assertFalse(misprintInfoRepository.existsById(misprintInfo.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteMisprintInfoNotFound() {

        misprintInfoService.deleteMisprintInfoNews(UUID.randomUUID());
    }
}
