package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.RatingRole;
import com.solvve.domain.Role;
import com.solvve.dto.ratingrole.RatingRolePatchDTO;
import com.solvve.dto.ratingrole.RatingRolePutDTO;
import com.solvve.dto.ratingrole.RatingRoleReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.RatingRoleRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;
import java.util.UUID;

public class RatingRoleServiceTest extends BaseTest {

    @Autowired
    private RatingRoleRepository ratingRoleRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private RatingRoleService ratingRoleService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testGetRatingRole() {

        Role role = testObjectsFactory.createRole();
        RatingRole ratingRole = testObjectsFactory.createRatingRole(role);

        RatingRoleReadDTO readDTO = ratingRoleService.getRatingRole(ratingRole.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(ratingRole, "roleId", "registeredUserId");
        Assert.assertEquals(ratingRole.getRole().getId(), readDTO.getRoleId());
        Assert.assertEquals(ratingRole.getRegisteredUser().getId(), readDTO.getRegisteredUserId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRatingRoleWrongId() {

        ratingRoleService.getRatingRole(UUID.randomUUID());
    }

    @Test
    public void testPatchRatingRole() {

        Role role = testObjectsFactory.createRole();
        RatingRole ratingRole = testObjectsFactory.createRatingRole(role);

        RatingRolePatchDTO patch = new RatingRolePatchDTO();
        patch.setRating(7.6);

        RatingRoleReadDTO read = ratingRoleService.patchRatingRole(ratingRole.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        ratingRole = ratingRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(ratingRole, "roleId", "registeredUserId");
        Assert.assertEquals(ratingRole.getRole().getId(), read.getRoleId());
        Assert.assertEquals(ratingRole.getRegisteredUser().getId(), read.getRegisteredUserId());
    }

    @Test
    public void testPatchRatingRoleEmptyPatch() {

        Role role = testObjectsFactory.createRole();
        RatingRole ratingRole = testObjectsFactory.createRatingRole(role);
        RatingRolePatchDTO patch = new RatingRolePatchDTO();
        RatingRoleReadDTO read = ratingRoleService.patchRatingRole(ratingRole.getId(), patch);

        Assert.assertNotNull(read.getRating());

        RatingRole ratingRoleAfterUpdate = ratingRoleRepository.findById(read.getId()).get();
        Assert.assertNotNull(ratingRoleAfterUpdate.getRating());

        Assertions.assertThat(ratingRole).isEqualToIgnoringGivenFields(ratingRoleAfterUpdate,
                "role", "registeredUser");
        Assertions.assertThat(ratingRole.getRole()).isEqualToIgnoringGivenFields(ratingRoleAfterUpdate.getRole(),
                "ratingRoles", "reviewRoles", "actor", "film");
        Assertions.assertThat(ratingRole.getRegisteredUser()).
                isEqualToIgnoringGivenFields(ratingRoleAfterUpdate.getRegisteredUser(),
                        "reviewFilmLikeDislikes", "reviewRoleLikeDislikes", "newsLikeDislikes", "problemReviews",
                        "misprintInfoFilms", "misprintInfoRoles", "misprintInfoNews");
    }

    @Test
    public void testPutRatingRole() {

        Role role = testObjectsFactory.createRole();
        RatingRole ratingRole1 = testObjectsFactory.createRatingRole(role);

        RatingRolePutDTO put = new RatingRolePutDTO();
        put.setRating(8.4);

        RatingRoleReadDTO read = ratingRoleService.updateRatingRole(ratingRole1.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        inTransaction(()-> {
            RatingRole ratingRole = ratingRoleRepository.findById(read.getId()).get();
            Assertions.assertThat(ratingRole).isEqualToIgnoringGivenFields(read, "role", "registeredUser");
            Assert.assertEquals(ratingRole.getRole().getId(), read.getRoleId());
            Assert.assertEquals(ratingRole.getRegisteredUser().getId(), read.getRegisteredUserId());
        });
    }

    @Test
    public void testDeleteRatingRole() {

        Role role = testObjectsFactory.createRole();
        RatingRole ratingRole = testObjectsFactory.createRatingRole(role);

        ratingRoleService.deleteRatingRole(ratingRole.getId());
        Assert.assertFalse(ratingRoleRepository.existsById(ratingRole.getId()));
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveRatingRoleValidation() {

        RatingRole ratingRole = new RatingRole();
        ratingRoleRepository.save(ratingRole);
    }


    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status->{
            runnable.run();
        });
    }
}
