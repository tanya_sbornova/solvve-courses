package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.ProblemReview;
import com.solvve.domain.RegisteredUser;
import com.solvve.dto.problemreview.ProblemReviewCreateDTO;
import com.solvve.dto.problemreview.ProblemReviewPatchDTO;
import com.solvve.dto.problemreview.ProblemReviewPutDTO;
import com.solvve.dto.problemreview.ProblemReviewReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ProblemReviewRepository;
import com.solvve.repository.RegisteredUserRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class RegisteredUserProblemReviewServiceTest extends BaseTest {

    @Autowired
    private ProblemReviewRepository problemReviewRepository;

    @Autowired
    private RegisteredUserProblemReviewService registeredUserProblemReviewService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void getRegisteredUserProblemReviews() {

        ProblemReview problemReview1 = testObjectsFactory.createProblemFilmReview();
        RegisteredUser registeredUser = problemReview1.getRegisteredUser();

        List<UUID> problemReviewesId = transactionTemplate.execute(status -> {
            RegisteredUser registeredUser1 = registeredUserRepository.findById(registeredUser.getId()).get();
            return registeredUser1.getProblemReviews().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });
        List<ProblemReviewReadDTO> problemReviewesReadDTO = registeredUserProblemReviewService.
                getRegisteredUserProblemReviews(registeredUser.getId());

        Assertions.assertThat(problemReviewesReadDTO).extracting("id").
                containsExactlyInAnyOrder(problemReviewesId.toArray());
    }

    @Test
    public void getRegisteredUserProblemFilmReview() {

        ProblemReview problemReview = testObjectsFactory.createProblemFilmReview();
        RegisteredUser registeredUser = problemReview.getRegisteredUser();

        List<UUID> problemReviewesId = transactionTemplate.execute(status -> {
            RegisteredUser registeredUser1 = registeredUserRepository.findById(registeredUser.getId()).get();
            return registeredUser1.getProblemReviews().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });

        ProblemReviewReadDTO problemReviewReadDTO = registeredUserProblemReviewService.
                getRegisteredUserProblemReview(registeredUser.getId(),
                        problemReviewesId.get(0));
        Assertions.assertThat(problemReviewReadDTO).isEqualToIgnoringGivenFields(problemReview,
                "reviewFilmId", "reviewRoleId", "registeredUserId", "moderatorId");
        Assert.assertEquals(problemReview.getReviewFilm().getId(), problemReviewReadDTO.getReviewFilmId());
        Assert.assertEquals(problemReview.getRegisteredUser().getId(),
                problemReviewReadDTO.getRegisteredUserId());
    }

    @Test
    public void getRegisteredUserProblemRoleReview() {

        ProblemReview problemReview = testObjectsFactory.createProblemRoleReview();
        RegisteredUser registeredUser = problemReview.getRegisteredUser();

        List<UUID> problemReviewesId = transactionTemplate.execute(status -> {
            RegisteredUser registeredUser1 = registeredUserRepository.findById(registeredUser.getId()).get();
            return registeredUser1.getProblemReviews().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });

        ProblemReviewReadDTO problemReviewReadDTO = registeredUserProblemReviewService.
                getRegisteredUserProblemReview(registeredUser.getId(),
                        problemReviewesId.get(0));
        Assertions.assertThat(problemReviewReadDTO).isEqualToIgnoringGivenFields(problemReview,
                "reviewFilmId", "reviewRoleId", "registeredUserId", "moderatorId");
        Assert.assertEquals(problemReview.getReviewRole().getId(), problemReviewReadDTO.getReviewRoleId());
        Assert.assertEquals(problemReview.getRegisteredUser().getId(),
                problemReviewReadDTO.getRegisteredUserId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRegisteredUserNewsWrongId() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        registeredUserProblemReviewService.getRegisteredUserProblemReview(registeredUser.getId(), UUID.randomUUID());
    }

    @Test
    public void testCreateRegisteredUserProblemFilmReview() {

        ProblemReviewCreateDTO create = testObjectsFactory.createProblemReviewFilmCreateDTO();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        ProblemReviewReadDTO read = registeredUserProblemReviewService.
                createRegisteredUserProblemReview(registeredUser.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ProblemReview problemReview = problemReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(problemReview,
                "moderatorId", "registeredUserId", "reviewFilmId", "reviewRoleId");
        Assert.assertEquals(problemReview.getReviewFilm().getId(), read.getReviewFilmId());
        Assert.assertEquals(problemReview.getRegisteredUser().getId(), read.getRegisteredUserId());
    }

    @Test
    public void testCreateRegisteredUserProblemRoleReview() {

        ProblemReviewCreateDTO create = testObjectsFactory.createProblemReviewRoleCreateDTO();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        ProblemReviewReadDTO read = registeredUserProblemReviewService.
                createRegisteredUserProblemReview(registeredUser.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ProblemReview problemReview = problemReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(problemReview,
                "moderatorId", "registeredUserId", "reviewFilmId", "reviewRoleId");
        Assert.assertEquals(problemReview.getReviewRole().getId(), read.getReviewRoleId());
        Assert.assertEquals(problemReview.getRegisteredUser().getId(), read.getRegisteredUserId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateRegisteredUserProblemReviewWithWrongReview() {

        ProblemReviewCreateDTO create = testObjectsFactory.createProblemReviewWithWrongReviewFilm();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        ProblemReviewReadDTO read = registeredUserProblemReviewService.
                createRegisteredUserProblemReview(registeredUser.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ProblemReview reviewNews = problemReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(reviewNews).isEqualToIgnoringGivenFields(read, "registeredUser");
    }

    @Test
    public void testPatchRegisteredUserProblemReview() {

        ProblemReview problemReviewFilm1 = testObjectsFactory.createProblemFilmReview();
        ProblemReviewPatchDTO patch = testObjectsFactory.createProblemReviewPatchDTO();

        RegisteredUser registeredUser1 = problemReviewFilm1.getRegisteredUser();
        ProblemReview problemReviewFilm = transactionTemplate.execute(status -> {
            return registeredUserRepository.findById(registeredUser1.getId()).get().
                    getProblemReviews().get(0);
        });

        ProblemReviewReadDTO read = registeredUserProblemReviewService.
                patchRegisteredUserProblemReview(registeredUser1.getId(),
                        problemReviewFilm.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        ProblemReview problemReview2 = problemReviewRepository.
                 findById(problemReviewFilm1.getId()).get();

        Assertions.assertThat(problemReview2).isEqualToIgnoringGivenFields(read,
          "registeredUser", "moderator", "reviewFilm", "reviewRole", "createdAt", "updatedAt");
    }

    @Test
    public void testUpdateRegisteredUserProblemReview() {

        ProblemReview problemReview1 = testObjectsFactory.createProblemRoleReview();
        ProblemReviewPutDTO put = testObjectsFactory.createProblemReviewPutDTO();

        RegisteredUser registeredUser1 = problemReview1.getRegisteredUser();
        ProblemReview problemReview = transactionTemplate.execute(status -> {
            return registeredUserRepository.findById(registeredUser1.getId()).get().
                    getProblemReviews().get(0);
        });

        ProblemReviewReadDTO read = registeredUserProblemReviewService.
                updateRegisteredUserProblemReview(registeredUser1.getId(),
                        problemReview.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        ProblemReview problemReview2 = problemReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(problemReview2).isEqualToIgnoringGivenFields(read,
             "registeredUser", "moderator", "reviewFilm", "reviewRole", "createdAt", "updatedAt");
    }

    @Test
    public void testDeleteRegisteredUserProblemReview() {

        ProblemReview problemReview = testObjectsFactory.createProblemFilmReview();
        RegisteredUser registeredUser = problemReview.getRegisteredUser();
        UUID problemReviewUUID = problemReview.getId();

/*                UUID problemReviewUUID = transactionTemplate.execute(status-> {
            ProblemReview problemReview = registeredUserRepository.findById(registeredUser.getId()).get().
                    getProblemReviews().get(0);
            return problemReview.getId();
        });*/
        registeredUserProblemReviewService.deleteRegisteredUserProblemReview(registeredUser.getId(),
                problemReviewUUID);
        Assert.assertFalse(problemReviewRepository.existsById(problemReviewUUID));
    }

}
