package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.NewsLikeDislike;
import com.solvve.domain.RegisteredUser;
import com.solvve.dto.newslikedislike.NewsLikeDislikeCreateDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikePatchDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikePutDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikeReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.NewsLikeDislikeRepository;
import com.solvve.repository.RegisteredUserRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class RegisteredUserNewsLikeServiceTest extends BaseTest {

    @Autowired
    private NewsLikeDislikeRepository newsLikeDislikeRepository;

    @Autowired
    private RegisteredUserNewsLikeService registeredUserNewsLikeService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void getRegisteredUserNewsLikes() {

        NewsLikeDislike newsLikeDislike1 = testObjectsFactory.createNewsLikeDislike();
        RegisteredUser registeredUser = newsLikeDislike1.getRegisteredUser();

        List<UUID> newsLikeDislikesId = transactionTemplate.execute(status -> {
            RegisteredUser registeredUser1 = registeredUserRepository.findById(registeredUser.getId()).get();
            return registeredUser1.getNewsLikeDislikes().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });

        List<NewsLikeDislikeReadDTO> newsLikeDislikeReadDTO = registeredUserNewsLikeService.
                getRegisteredUserNewsLikes(registeredUser.getId());

        Assertions.assertThat(newsLikeDislikeReadDTO).extracting("id").
                containsExactlyInAnyOrder(newsLikeDislikesId.toArray());
    }

    @Test
    public void getRegisteredUserNewsLike() {

        NewsLikeDislike newsLikeDislike1 = testObjectsFactory.createNewsLikeDislike();
        RegisteredUser registeredUser = newsLikeDislike1.getRegisteredUser();

        List<UUID> newsLikeDislikesId = transactionTemplate.execute(status -> {
            RegisteredUser registeredUser1 = registeredUserRepository.findById(registeredUser.getId()).get();
            return registeredUser1.getNewsLikeDislikes().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });

        NewsLikeDislikeReadDTO newsLikeDislikeReadDTO = registeredUserNewsLikeService.
                getRegisteredUserNewsLike(registeredUser.getId(),
                        newsLikeDislikesId.get(0));
        Assertions.assertThat(newsLikeDislikeReadDTO.getId()).isEqualToIgnoringGivenFields(newsLikeDislikesId.get(0),
                "newsId", "registeredUserId");
        Assert.assertEquals(newsLikeDislike1.getNews().getId(), newsLikeDislikeReadDTO.getNewsId());
        Assert.assertEquals(newsLikeDislike1.getRegisteredUser().getId(),
                newsLikeDislikeReadDTO.getRegisteredUserId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRegisteredUserNewsWrongId() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        registeredUserNewsLikeService.getRegisteredUserNewsLike(registeredUser.getId(), UUID.randomUUID());
    }

    @Test
    public void testCreateRegisteredUserNews() {

        NewsLikeDislikeCreateDTO create = testObjectsFactory.createNewsLikeDislikeCreateDTO();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        NewsLikeDislikeReadDTO read = registeredUserNewsLikeService.
                createRegisteredUserNewsLike(registeredUser.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        NewsLikeDislike newsLikeDislike = newsLikeDislikeRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(newsLikeDislike, "newsId", "registeredUserId");
        Assert.assertEquals(read.getNewsId(), newsLikeDislike.getNews().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateRegisteredUserNewsWithWrongNews() {

        NewsLikeDislikeCreateDTO create = testObjectsFactory.createNewsLikeDislikeWithWrongNews();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        NewsLikeDislikeReadDTO read = registeredUserNewsLikeService.
                createRegisteredUserNewsLike(registeredUser.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        NewsLikeDislike reviewNews = newsLikeDislikeRepository.findById(read.getId()).get();
        Assertions.assertThat(reviewNews).isEqualToIgnoringGivenFields(read, "news", "registeredUser");
    }

    @Test
    public void testPatchRegisteredUserNews() {

        NewsLikeDislike newsLikeDislike1 = testObjectsFactory.createNewsLikeDislike();
        NewsLikeDislikePatchDTO patch = new NewsLikeDislikePatchDTO();
        patch.setIsLike(true);

        RegisteredUser registeredUser1 = newsLikeDislike1.getRegisteredUser();
        NewsLikeDislike newsLikeDislike = transactionTemplate.execute(status -> {
            return registeredUserRepository.findById(registeredUser1.getId()).get().
                    getNewsLikeDislikes().get(0);
        });

        NewsLikeDislikeReadDTO read = registeredUserNewsLikeService.
                patchRegisteredUserNewsLike(registeredUser1.getId(),
                        newsLikeDislike.getId(), patch);

        inTransaction(() -> {
            Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
            NewsLikeDislike newsLikeDislike2 = newsLikeDislikeRepository.
                    findById(newsLikeDislike1.getId()).get();
            Assertions.assertThat(newsLikeDislike2).isEqualToIgnoringGivenFields(read,
                    "registeredUser", "news", "createdAt", "updatedAt");
        });
    }

    @Test
    public void testUpdateRegisteredUserNews() {

        NewsLikeDislike news1 = testObjectsFactory.createNewsLikeDislike();
        NewsLikeDislikePutDTO put = new NewsLikeDislikePutDTO();
        put.setIsLike(true);

        RegisteredUser registeredUser1 = news1.getRegisteredUser();
        NewsLikeDislike news = transactionTemplate.execute(status -> {
            return registeredUserRepository.findById(registeredUser1.getId()).get().
                    getNewsLikeDislikes().get(0);
        });

        NewsLikeDislikeReadDTO read = registeredUserNewsLikeService.
                updateRegisteredUserNewsLike(registeredUser1.getId(),
                        news.getId(), put);

        inTransaction(() -> {
            Assertions.assertThat(put).isEqualToComparingFieldByField(read);
            NewsLikeDislike newsLikeDislike = newsLikeDislikeRepository.findById(read.getId()).get();
            Assertions.assertThat(newsLikeDislike).isEqualToIgnoringGivenFields(read,
                    "registeredUser", "news", "createdAt", "updatedAt");
        });
    }

    @Test
    public void testDeleteRegisteredUserNews() {

        NewsLikeDislike newsLikeDislike = testObjectsFactory.createNewsLikeDislike();
        RegisteredUser registeredUser = newsLikeDislike.getRegisteredUser();
        UUID newsLikeDislikeUUID = newsLikeDislike.getId();

        registeredUserNewsLikeService.deleteRegisteredUserNewsLike(registeredUser.getId(), newsLikeDislikeUUID);
        Assert.assertFalse(newsLikeDislikeRepository.existsById(newsLikeDislikeUUID));
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }
}

