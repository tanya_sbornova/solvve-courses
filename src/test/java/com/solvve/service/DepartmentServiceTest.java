package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.*;
import com.solvve.util.TestObjectsFactory;
import com.solvve.dto.department.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.DepartmentRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.*;

public class DepartmentServiceTest extends BaseTest {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testGetDepartment() {

        Department department = testObjectsFactory.createDepartment();

        DepartmentReadDTO readDTO = departmentService.getDepartment(department.getId());

        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(department);
    }

    @Test
    public void testGetSimpleDepartment() {

        Department department = testObjectsFactory.createDepartment();

        DepartmentReadDTO readDTO = departmentService.getDepartment(department.getId());

        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(department);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetDepartmentWrongId() {
        departmentService.getDepartment(UUID.randomUUID());
    }

    @Test
    public void testCreateDepartment() {

        DepartmentCreateDTO create = testObjectsFactory.createDepartmentCreateDTO();

        DepartmentReadDTO read = departmentService.createDepartment(create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        inTransaction(() -> {
            Department department = departmentRepository.findById(read.getId()).get();
            Assertions.assertThat(read).isEqualToComparingFieldByField(department);
        });
    }

    @Test
    public void testDeleteDepartment() {

        Department department = testObjectsFactory.createDepartment();

        departmentService.deleteDepartment(department.getId());
        Assert.assertFalse(departmentRepository.existsById(department.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteDepartmentNotFound() {

        departmentService.deleteDepartment(UUID.randomUUID());
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveDepartmentValidation() {

        Department department = new Department();
        departmentRepository.save(department);
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }
}