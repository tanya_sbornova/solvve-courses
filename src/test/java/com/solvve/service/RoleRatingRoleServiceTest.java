package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.RatingRole;
import com.solvve.domain.Role;
import com.solvve.dto.ratingrole.RatingRoleCreateDTO;
import com.solvve.dto.ratingrole.RatingRolePatchDTO;
import com.solvve.dto.ratingrole.RatingRolePutDTO;
import com.solvve.dto.ratingrole.RatingRoleReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.RatingRoleRepository;
import com.solvve.repository.RoleRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class RoleRatingRoleServiceTest extends BaseTest {

    @Autowired
    private RatingRoleRepository ratingRoleRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private RoleRatingRoleService roleRatingRoleService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test(expected = EntityNotFoundException.class)
    public void testGetRoleRatingRoleWrongId() {
        Role role = testObjectsFactory.createRole();
        roleRatingRoleService.getRoleRatingRole(role.getId(), UUID.randomUUID());
    }

    @Test
    public void getRoleRatingRoles() {

        Role role0 = testObjectsFactory.createRole();
        RatingRole ratingRole1 = testObjectsFactory.createRatingRole(role0);
        Role role1 = ratingRole1.getRole();

        List<UUID> ratingRolesId = transactionTemplate.execute(status -> {
            Role role = roleRepository.findById(role1.getId()).get();
            return role.getRatingRoles().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });

        List<RatingRoleReadDTO> ratingRolesReadDTO = roleRatingRoleService.getRoleRatingRoles(role1.getId());
        Assertions.assertThat(ratingRolesReadDTO).extracting("id").
                containsExactlyInAnyOrder(ratingRolesId.toArray());
    }

    @Test
    public void getRoleRatingRole()  {

        Role role1 = testObjectsFactory.createRole();
        RatingRole ratingRole1 = testObjectsFactory.createRatingRole(role1);
        Role role = ratingRole1.getRole();

        RatingRole ratingRole = transactionTemplate.execute(status -> {
            return roleRepository.findById(role.getId()).get().getRatingRoles().get(0);
        });

        RatingRoleReadDTO ratingRoleReadDTO = roleRatingRoleService.
                    getRoleRatingRole(role.getId(), ratingRole.getId());
        Assertions.assertThat(ratingRoleReadDTO).isEqualToIgnoringGivenFields(ratingRole,
                    "roleId", "registeredUserId");
    }

    @Test
    public void testCreateRoleRatingRole() {

        RatingRoleCreateDTO create = testObjectsFactory.createRatingRoleCreateDTO();
        Role role = testObjectsFactory.createRole();

        RatingRoleReadDTO read = roleRatingRoleService.createRoleRatingRole(role.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        RatingRole ratingRole = ratingRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(ratingRole, "roleId", "registeredUserId");
        Assertions.assertThat(read.getRoleId()).isEqualToComparingFieldByField(ratingRole.getRole().getId());
        Assert.assertEquals(read.getRoleId(), ratingRole.getRole().getId());
        Assert.assertEquals(read.getRegisteredUserId(), ratingRole.getRegisteredUser().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateRoleRatingRoleWithWrongUser() {

        RatingRoleCreateDTO create = testObjectsFactory.createRatingRoleWithWrongUser();
        Role role = testObjectsFactory.createRole();

        RatingRoleReadDTO read = roleRatingRoleService.createRoleRatingRole(role.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        RatingRole ratingRole = ratingRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(ratingRole).isEqualToIgnoringGivenFields(read, "role", "registeredUser");
    }

    @Test
    public void testPatchRoleRatingRole() throws Throwable {

        Role role1 = testObjectsFactory.createRole();
        RatingRolePatchDTO patch = new RatingRolePatchDTO();
        patch.setRating(7.6);

        RatingRole ratingRole = transactionTemplate.execute(status -> {
            Role role = roleRepository.findById(role1.getId()).get();
            return roleRepository.findById(role.getId()).get().getRatingRoles().get(0);
        });

        RatingRoleReadDTO read = roleRatingRoleService.patchRoleRatingRole(role1.getId(),
                    ratingRole.getId(), patch);

        inTransaction(()-> {
            Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
            RatingRole ratingRoleAfter = ratingRoleRepository.findById(ratingRole.getId()).get();
            Role role = ratingRoleAfter.getRole();
            Assertions.assertThat(ratingRoleAfter).isEqualToIgnoringGivenFields(read,
                    "registeredUser", "role", "createdAt", "updatedAt");
            Assert.assertEquals(read.getRoleId(), ratingRoleAfter.getRole().getId());
            Assert.assertEquals(read.getRegisteredUserId(), ratingRoleAfter.getRegisteredUser().getId());
        });
    }

    @Test
    public void testUpdateRoleRatingRole() throws Throwable {

        Role role0 = testObjectsFactory.createRole();
        RatingRole ratingRole1 = testObjectsFactory.createRatingRole(role0);
        RatingRolePutDTO put = new RatingRolePutDTO();
        put.setRating(7.6);
        Role role1 = ratingRole1.getRole();

       RatingRole ratingRole2 = transactionTemplate.execute(status -> {
           Role role = roleRepository.findById(role1.getId()).get();
           return roleRepository.findById(role.getId()).get().getRatingRoles().get(0);
       });

       RatingRoleReadDTO read = roleRatingRoleService.updateRoleRatingRole(role1.getId(),
                    ratingRole2.getId(), put);

        inTransaction(()-> {
            Assertions.assertThat(put).isEqualToComparingFieldByField(read);
            RatingRole ratingRole = ratingRoleRepository.findById(read.getId()).get();
            Assertions.assertThat(ratingRole).isEqualToIgnoringGivenFields(read,
                    "registeredUser", "role", "createdAt", "updatedAt");
            Assert.assertEquals(read.getRoleId(), ratingRole.getRole().getId());
            Assert.assertEquals(read.getRegisteredUserId(), ratingRole.getRegisteredUser().getId());
        });
    }

    @Test
    public void testDeleteRoleRatingRole() {

        Role role = testObjectsFactory.createRole();
        UUID ratingRoleUUID = transactionTemplate.execute(status-> {
            RatingRole ratingRole = roleRepository.findById(role.getId()).get().getRatingRoles().get(0);
            return ratingRole.getId();
        });
        roleRatingRoleService.deleteRoleRatingRole(role.getId(), ratingRoleUUID);
        Assert.assertFalse(ratingRoleRepository.existsById(ratingRoleUUID));
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status->{
            runnable.run();
        });
    }
}

