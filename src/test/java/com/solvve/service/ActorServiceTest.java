package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.*;
import com.solvve.repository.RoleRepository;
import com.solvve.util.TestObjectsFactory;
import com.solvve.dto.actor.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ActorRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.*;
import java.util.stream.Collectors;

public class ActorServiceTest extends BaseTest {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ActorService actorService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testGetActor() {

        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);

        ActorReadDTO readDTO = actorService.getActor(actor.getId());

        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(actor,
                "personId");

        Assert.assertEquals(readDTO.getPersonId(), actor.getPerson().getId());
    }

    @Test
    public void testGetSimpleActor() {

        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);

        ActorReadDTO readDTO = actorService.getActor(actor.getId());

        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(actor,
                "personId");
    }

    @Test
    public void testGetActorExtended() {

        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);

        ActorReadExtendedDTO readDTO = actorService.getActorExtended(actor.getId());

        inTransaction(() -> {
            Actor actor2 = actorRepository.findById(actor.getId()).get();
            Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(actor2, "roles", "person");
        });
    }

    @Test
    public void testGetSimpleActorExtended() {

        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);

        ActorReadExtendedDTO readDTO = actorService.getActorExtended(actor.getId());

        inTransaction(() -> {
            Actor actor2 = actorRepository.findById(actor.getId()).get();
            Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(actor2, "roles", "person");
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetActorWrongId() {
        actorService.getActor(UUID.randomUUID());
    }

    @Test
    public void getPersonActor() {

        Person person = testObjectsFactory.createPerson();
        List<Actor> actors = List.of(testObjectsFactory.createActor(person));

        ActorReadDTO actorReadDTO = actorService.
                getPersonActor(actors.get(0).getPerson().getId(), actors.get(0).getId());
        Assertions.assertThat(actorReadDTO).isEqualToIgnoringGivenFields(actors.get(0),
                "personId");
        Assert.assertEquals(actorReadDTO.getPersonId(), actors.get(0).getPerson().getId());
    }

    @Test
    public void testCreateActor() {

        ActorCreateDTO create = testObjectsFactory.createActorCreateDTO();

        ActorReadDTO read = actorService.createActor(create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        inTransaction(() -> {
            Actor actor = actorRepository.findById(read.getId()).get();
            Assertions.assertThat(read).isEqualToIgnoringGivenFields(actor,
                    "personId");
            Assert.assertEquals(read.getPersonId(), actor.getPerson().getId());
        });
    }

    @Test
    public void testCreatePersonActor() {

        ActorCreateDTO create = testObjectsFactory.createActorCreateDTO();
        Person person = testObjectsFactory.createPerson();
        create.setPersonId(person.getId());

        ActorReadDTO read = actorService.createPersonActor(person.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Actor actor = actorRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(actor, "personId");
        Assert.assertEquals(read.getPersonId(), actor.getPerson().getId());
    }

    @Test
    public void testDeleteActor() {

        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);

        actorService.deleteActor(actor.getId());
        Assert.assertFalse(actorRepository.existsById(actor.getId()));
    }

    @Test
    public void testDeletePersonActor() {

        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        actorService.deletePersonActor(actor.getPerson().getId(), actor.getId());
        Assert.assertFalse(actorRepository.existsById(actor.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteActorNotFound() {

        actorService.deleteActor(UUID.randomUUID());
    }

    @Test
    public void updateAverageRatingActorOfRole() {

        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        Role role = testObjectsFactory.createRole("Neo", actor);
        RatingRole ratingRole1 = testObjectsFactory.createRatingRole(role, 8.0);
        role = testObjectsFactory.createRole("Kevin Lomax", actor);
        RatingRole ratingRole2 = testObjectsFactory.createRatingRole(role, 8.5);
        role.setRatingRoles(List.of(ratingRole1, ratingRole2));
        roleRepository.save(role);

        actorService.updateAverageRatingActorOfRole(actor.getId());
        actor = actorRepository.findById(actor.getId()).get();

        Assert.assertEquals(8.25, actor.getAverageRating(), Double.MIN_NORMAL);
    }

    @Test
    public void updateAverageRatingActorByFilm() {

        Film film = testObjectsFactory.createFlatFilm();
        testObjectsFactory.createRatingFilm(film, 7.0);
        testObjectsFactory.createRatingFilm(film, 8.0);
        testObjectsFactory.createRatingFilm(film, 9.0);
        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        Role role1 = testObjectsFactory.createRole(actor, film);
        roleRepository.save(role1);
        Film film2 = testObjectsFactory.createFlatFilm();
        testObjectsFactory.createRatingFilm(film, 6.0);
        testObjectsFactory.createRatingFilm(film, 7.0);
        testObjectsFactory.createRatingFilm(film, 8.0);
        Role role2 = testObjectsFactory.createRole(actor, film2);
        roleRepository.save(role2);

        actorService.updateAverageRatingActorByFilm(actor.getId());

        actor = actorRepository.findById(actor.getId()).get();
        Assert.assertEquals(7.5, actor.getAverageRatingByFilm(), Double.MIN_NORMAL);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveActorValidation() {

        Actor actor = new Actor();
        actorRepository.save(actor);
    }

    @Test
    public void getActorsLeaderBoard() {
        int actorsCount = 30;
        Set<UUID> actorIds = new HashSet<>();
        for (int i = 1; i < actorsCount; i++) {
            Actor actor = testObjectsFactory.createActor(testObjectsFactory.createPerson());
            actorIds.add(actor.getId());
            Role role = testObjectsFactory.createRole(actor, testObjectsFactory.createFlatFilm());
            testObjectsFactory.createRatingRole(role);
            testObjectsFactory.createRatingRole(role);
            RatingRole ratingRole = testObjectsFactory.createRatingRole(role);
            Role role2 = testObjectsFactory.createRole(actor, testObjectsFactory.createFlatFilm());
            testObjectsFactory.createRatingRole(role2);
            testObjectsFactory.createRatingRole(role2);
            testObjectsFactory.createRatingRole(role2);
            actorService.updateAverageRatingActorOfRole(actor.getId());
        }
        List<ActorInLeaderBoardReadDTO> actorsLeaderBoard = actorRepository.getActorsLeaderBoard();
        Assertions.assertThat(actorsLeaderBoard).
                isSortedAccordingTo(Comparator.comparing(ActorInLeaderBoardReadDTO::getAverageRating).reversed());
        Assert.assertEquals(actorIds,
                actorsLeaderBoard.stream().map(ActorInLeaderBoardReadDTO::getId).collect(Collectors.toSet()));

        for(ActorInLeaderBoardReadDTO ac: actorsLeaderBoard) {
            Assert.assertNotNull(ac.getName());
            Assert.assertNotNull(ac.getAverageRating());
        }
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }
}

