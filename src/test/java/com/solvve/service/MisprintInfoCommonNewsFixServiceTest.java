package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.*;
import com.solvve.dto.misprintinfofix.MisprintInfoFixDTO;
import com.solvve.dto.misprintinfofix.MisprintInfoFixReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.EntityWrongStatusException;
import com.solvve.repository.NewsRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.*;

import static org.hamcrest.CoreMatchers.is;

public class MisprintInfoCommonNewsFixServiceTest extends BaseTest {

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private MisprintInfoCommonFixService misprintInfoService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test(expected = EntityNotFoundException.class)
    public void testFixMisprintInfoNewsWrongId() {
        ContentManager contentManager = testObjectsFactory.createContentManager();
        MisprintInfoFixDTO patch = new MisprintInfoFixDTO();
        misprintInfoService.fixContent(contentManager.getId(), UUID.randomUUID(), patch);
    }

    @Test(expected = EntityWrongStatusException.class)
    public void testFixMisprintInfoNewsWrongStatus() {
        MisprintInfoCommon misprintInfo = testObjectsFactory.createMisprintInfoCommonWrongStatus();
        ContentManager contentManager = testObjectsFactory.createContentManager();
        MisprintInfoFixDTO patch = new MisprintInfoFixDTO();
        misprintInfoService.fixContent(contentManager.getId(), misprintInfo.getId(), patch);
    }

    @Test
    public void testFixMisprintInfoNewsUserVersion() throws InterruptedException {

        Film film = testObjectsFactory.createFlatFilm();
        News news = testObjectsFactory.createFilmNews(film);
        news.setText("Brad Pitt rev_als he turned down 'The Matrix'");
        news = newsRepository.save(news);
        News news2 = testObjectsFactory.createFilmNews(film);
        ContentManager contentManager = testObjectsFactory.createContentManager();
        MisprintInfoFixDTO patch = new MisprintInfoFixDTO();
        MisprintInfoCommon misprintInfo = testObjectsFactory.createMpInfoCommonWithParam(news.getId(),
                MisprintObject.NEWS,
                10, 17,
                "reveals", "rev_als");

        List<UUID> expectedIdsOfMisprints = new ArrayList<>();
        expectedIdsOfMisprints.add(misprintInfo.getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoCommonWithParam(news.getId(), MisprintObject.NEWS,
                13, 14,
                "e", "_").getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoCommonWithParam(news.getId(), MisprintObject.NEWS,
                12, 16,
                "veal", "v_al").getId());
        testObjectsFactory.createMpInfoNewsWithParam(news, 5, 17, "XXXXXXXXXXX", "");
        testObjectsFactory.createMpInfoNewsWithParam(news2, 12, 16, "veal", "v_al");

        Instant dateBeforeTest = Instant.now();
        Thread.sleep(10);

        List<MisprintInfoFixReadDTO> mpNewsReads =
                misprintInfoService.fixContent(contentManager.getId(), misprintInfo.getId(), patch);

        Thread.sleep(10);
        Instant dateAfterTest = Instant.now();

        Assert.assertEquals(expectedIdsOfMisprints.size(), mpNewsReads.size());
        Assertions.assertThat(mpNewsReads).extracting("id").
                containsExactlyInAnyOrder(expectedIdsOfMisprints.toArray());
        Instant actualFixDate = mpNewsReads.get(0).getFixedDate();
        Assert.assertThat(dateBeforeTest.isBefore(actualFixDate), is(true));
        Assert.assertThat(dateAfterTest.isAfter(actualFixDate), is(true));
        mpNewsReads.forEach(mpNewsRead -> {
            Assert.assertEquals(mpNewsRead.getStatus(), MisprintStatus.FIXED);
            Assert.assertEquals(mpNewsRead.getContentManagerId(), contentManager.getId());
            Assert.assertEquals(mpNewsRead.getText(), misprintInfo.getText());
            Assert.assertThat(dateBeforeTest.isBefore(mpNewsRead.getFixedDate()), is(true));
            Assert.assertThat(dateAfterTest.isAfter(mpNewsRead.getFixedDate()), is(true));
            Assert.assertEquals(mpNewsRead.getFixedDate(), actualFixDate);
        });
        String adjustedText = prepareText(misprintInfo, news.getText(), misprintInfo.getText());
        News newsAfter = newsRepository.findById(news.getId()).get();
        Assert.assertEquals(newsAfter.getText(), adjustedText);
    }

    @Test
    public void testFixMisprintInfoNewsCMVersion() throws InterruptedException {

        Film film = testObjectsFactory.createFlatFilm();
        News news = testObjectsFactory.createFilmNews(film);
        news.setText("Brad Pitt rev_als he turned down 'The Matrix'");
        news = newsRepository.save(news);
        News news2 = testObjectsFactory.createFilmNews(film);
        ContentManager contentManager = testObjectsFactory.createContentManager();
        MisprintInfoFixDTO patch = new MisprintInfoFixDTO();
        patch.setText(" tells ");
        MisprintInfoCommon misprintInfo = testObjectsFactory.createMpInfoCommonWithParam(news.getId(),
                MisprintObject.NEWS,
                10, 17,
                "reveals", "rev_als");

        List<UUID> expectedIdsOfMisprints = new ArrayList<>();
        expectedIdsOfMisprints.add(misprintInfo.getId());
                expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoCommonWithParam(news.getId(),
                        MisprintObject.NEWS,13, 14,"e", "_").getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoCommonWithParam(news.getId(), MisprintObject.NEWS,
                12, 16,
                "veal", "v_al").getId());
        testObjectsFactory.createMpInfoNewsWithParam(news, 5, 17, "XXXXXXXXXXX", "");
        testObjectsFactory.createMpInfoNewsWithParam(news2, 12, 16, "veal", "v_al");

        Instant dateBeforeTest = Instant.now();
        Thread.sleep(10);
        List<MisprintInfoFixReadDTO> mpNewsReads =
                misprintInfoService.fixContent(contentManager.getId(), misprintInfo.getId(), patch);

        Thread.sleep(10);
        Instant dateAfterTest = Instant.now();

        Assert.assertEquals(expectedIdsOfMisprints.size(), mpNewsReads.size());
        Assertions.assertThat(mpNewsReads).extracting("id").
                containsExactlyInAnyOrder(expectedIdsOfMisprints.toArray());
        Instant actualFixDate = mpNewsReads.get(0).getFixedDate();
        Assert.assertThat(dateBeforeTest.isBefore(actualFixDate), is(true));
        Assert.assertThat(dateAfterTest.isAfter(actualFixDate), is(true));
        mpNewsReads.forEach(mpNewsRead -> {
            Assert.assertEquals(mpNewsRead.getStatus(), MisprintStatus.FIXED);
            Assert.assertEquals(mpNewsRead.getContentManagerId(), contentManager.getId());
            Assert.assertEquals(mpNewsRead.getText(), " tells ");
            Assert.assertEquals(mpNewsRead.getStatus(), MisprintStatus.FIXED);
            Assert.assertEquals(mpNewsRead.getFixedDate(), actualFixDate);
        });
        String adjustedText = prepareText(misprintInfo, news.getText(), " tells ");
        News newsAfter = newsRepository.findById(news.getId()).get();
        Assert.assertEquals(newsAfter.getText(), adjustedText);
    }

    private String prepareText(MisprintInfoCommon misprintInfo, String incorrectText,  String replacement) {

        StringBuilder builder = new StringBuilder(incorrectText);
        builder.replace(misprintInfo.getStartIndex(), misprintInfo.getEndIndex(), replacement);
        return builder.toString();
    }
}

