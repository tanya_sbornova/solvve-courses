package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.MisprintInfoNews;
import com.solvve.domain.MisprintStatus;
import com.solvve.domain.RegisteredUser;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsCreateDTO;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.MisprintInfoNewsRepository;
import com.solvve.repository.RegisteredUserRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class RegisteredUserMisprintInfoNewsServiceTest extends BaseTest {

    @Autowired
    private MisprintInfoNewsRepository misprintinfoNewsRepository;

    @Autowired
    private RegisteredUserMisprintInfoNewsService registeredUserMisprintInfoNewsService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void getRegisteredUserMisprintInfoNews1() {

        MisprintInfoNews misprintInfoNews1 = testObjectsFactory.createMisprintInfoNews();
        RegisteredUser registeredUser = misprintInfoNews1.getRegisteredUser();

        List<UUID> misprintInfoNewsId = transactionTemplate.execute(status -> {
            RegisteredUser registeredUser1 = registeredUserRepository.findById(registeredUser.getId()).get();
            return registeredUser1.getMisprintInfoNews().stream().
                    map(MisprintInfoNews::getId).collect(Collectors.toList());
        });
        List<MisprintInfoNewsReadDTO> misprintInfoNewsReadDTO = registeredUserMisprintInfoNewsService.
                getRegisteredUserMisprintInfoNews(registeredUser.getId());

        Assertions.assertThat(misprintInfoNewsReadDTO).extracting("id").
                containsExactlyInAnyOrder(misprintInfoNewsId.toArray());
    }

    @Test
    public void getRegisteredUserMisprintInfoNews2() {

        MisprintInfoNews misprintInfoNews1 = testObjectsFactory.createMisprintInfoNews();
        RegisteredUser registeredUser = misprintInfoNews1.getRegisteredUser();

        List<UUID> misprintInfoNewsId = transactionTemplate.execute(status -> {
            RegisteredUser registeredUser1 = registeredUserRepository.findById(registeredUser.getId()).get();
            return registeredUser1.getMisprintInfoNews().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });

        MisprintInfoNewsReadDTO misprintinfonewsReadDTO = registeredUserMisprintInfoNewsService.
                getRegisteredUserMisprintInfoNews(registeredUser.getId(),
                        misprintInfoNewsId.get(0));

        MisprintInfoNews misprintInfoNews = misprintinfoNewsRepository.findById(misprintInfoNewsId.get(0)).get();
        Assertions.assertThat(misprintinfonewsReadDTO).isEqualToIgnoringGivenFields(misprintInfoNews,
                "contentId", "registeredUserId", "contentManagerId");
        Assert.assertEquals(misprintInfoNews1.getContent().getId(), misprintinfonewsReadDTO.getContentId());
        Assert.assertEquals(misprintInfoNews1.getRegisteredUser().getId(),
                misprintinfonewsReadDTO.getRegisteredUserId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRegisteredUserNewsWrongId() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        registeredUserMisprintInfoNewsService.getRegisteredUserMisprintInfoNews(registeredUser.getId(),
                UUID.randomUUID());
    }

    @Test
    public void testCreateRegisteredUserMisprintInfoNews() {

        MisprintInfoNewsCreateDTO create = testObjectsFactory.createMisprintInfoNewsCreateDTO();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        MisprintInfoNewsReadDTO read = registeredUserMisprintInfoNewsService.
                createRegisteredUserMisprintInfoNews(registeredUser.getId(), create);

        Assertions.assertThat(create).isEqualToIgnoringGivenFields(read, "status");
        Assert.assertEquals(read.getStatus(), MisprintStatus.NEED_TO_FIX);
        Assert.assertNotNull(read.getId());

        MisprintInfoNews misprintinfoNews = misprintinfoNewsRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(misprintinfoNews,
                "contentId", "registeredUserId", "contentManagerId");
        Assert.assertEquals(misprintinfoNews.getContent().getId(), read.getContentId());
        Assert.assertEquals(misprintinfoNews.getRegisteredUser().getId(), read.getRegisteredUserId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateRegisteredUserMisprintInfoNewsWithWrongNews() {

        MisprintInfoNewsCreateDTO create = testObjectsFactory.createMisprintInfoNewsWithWrongNews();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        MisprintInfoNewsReadDTO read = registeredUserMisprintInfoNewsService.
                createRegisteredUserMisprintInfoNews(registeredUser.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        MisprintInfoNews misprintInfoNews = misprintinfoNewsRepository.findById(read.getId()).get();
        Assertions.assertThat(misprintInfoNews).isEqualToIgnoringGivenFields(read, "registeredUser");
    }

    @Test
    public void testDeleteRegisteredUserNews() {

        MisprintInfoNews misprintinfoNews = testObjectsFactory.createMisprintInfoNews();
        UUID misprintinfoNewsUUID = misprintinfoNews.getId();
        RegisteredUser registeredUser = misprintinfoNews.getRegisteredUser();

        registeredUserMisprintInfoNewsService.deleteRegisteredUserMisprintInfoNews(registeredUser.getId(),
                misprintinfoNewsUUID);
        Assert.assertFalse(misprintinfoNewsRepository.existsById(misprintinfoNewsUUID));
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }
}

