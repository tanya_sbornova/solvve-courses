package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.ContentManager;
import com.solvve.domain.UserStatus;
import com.solvve.dto.contentmanager.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ContentManagerRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.TransactionSystemException;

import java.util.Arrays;
import java.util.UUID;

public class ContentManagerServiceTest extends BaseTest {

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Autowired
    private ContentManagerService contentManagerService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetContentManager() {

        ContentManager contentManager = testObjectsFactory.createContentManager();

        ContentManagerReadDTO readDTO = contentManagerService.getContentManager(contentManager.getId());

        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(contentManager);

    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetContentManagerWrongId() {

        contentManagerService.getContentManager(UUID.randomUUID());
    }

    @Test
    public void testCreateContentManager() {

        ContentManagerCreateDTO create = testObjectsFactory.generateObject(ContentManagerCreateDTO.class);

        ContentManagerReadDTO read = contentManagerService.createContentManager(create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ContentManager contentManager = contentManagerRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(contentManager);
    }

    @Test
    public void testPatchContentManager() {

        ContentManager contentManager = testObjectsFactory.createContentManager();

        ContentManagerPatchDTO patch = testObjectsFactory.generateObject(ContentManagerPatchDTO.class);

        ContentManagerReadDTO read = contentManagerService.patchContentManager(contentManager.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        contentManager = contentManagerRepository.findById(read.getId()).get();
        Assertions.assertThat(contentManager).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchContentManagerEmptyPatch() {

        ContentManager contentManager = testObjectsFactory.createContentManager();

        ContentManagerPatchDTO patch = new ContentManagerPatchDTO();
        ContentManagerReadDTO read = contentManagerService.patchContentManager(contentManager.getId(),
                patch);

        Assert.assertNotNull(read.getEmail());
        Assert.assertNotNull(read.getUserRole());
        Assert.assertNotNull(read.getUserStatus());

        ContentManager contentManagerAfterUpdate = contentManagerRepository.findById(read.getId()).get();

        Assert.assertNotNull(contentManagerAfterUpdate.getEmail());
        Assert.assertNotNull(contentManagerAfterUpdate.getEncodedPassword());
        Assert.assertNotNull(contentManagerAfterUpdate.getUserRole());
        Assert.assertNotNull(contentManagerAfterUpdate.getUserStatus());

        Assertions.assertThat(contentManager).isEqualToComparingFieldByField(contentManagerAfterUpdate);
    }

    @Test
    public void testUpdateContentManager() {

        ContentManager contentManager = testObjectsFactory.createContentManager();

        ContentManagerPutDTO put = testObjectsFactory.generateObject(ContentManagerPutDTO.class);

        ContentManagerReadDTO read = contentManagerService.updateContentManager(contentManager.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        contentManager = contentManagerRepository.findById(read.getId()).get();
        Assertions.assertThat(contentManager).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testDeleteContentManager() {

        ContentManager contentManager = testObjectsFactory.createContentManager();

        contentManagerService.deleteContentManager(contentManager.getId());
        Assert.assertFalse(contentManagerRepository.existsById(contentManager.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteContentManagerNotFound() {

        contentManagerService.deleteContentManager(UUID.randomUUID());
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveContentManagerValidation() {

        ContentManager contentManager = new ContentManager();
        contentManagerRepository.save(contentManager);
    }

    @Test
    public void testGetContentManagersWithEmptyFilterWithPagingAndSorting() throws InterruptedException {
        ContentManager contentManager = testObjectsFactory.createContentManager();
        Thread.sleep(500);
        ContentManager contentManager2 = testObjectsFactory.createContentManager();

        ContentManagerFilter filter = new ContentManagerFilter();
        PageRequest pageRequest = PageRequest.of(0, 2, Sort.by(Sort.Direction.DESC, "createdAt"));

        Assertions.assertThat(contentManagerService.getContentManagers(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(contentManager2.getId(), contentManager.getId()));
    }

    @Test
    public void testGetContentManagersWithAllFiltersWithPagingAndSorting() throws InterruptedException {
        ContentManager m1 = testObjectsFactory.generateFlatEntityWithoutId(ContentManager.class);
        m1.setEmail("test1@mail.ru");
        m1.setUserStatus(UserStatus.NEW);
        contentManagerRepository.save(m1);
        ContentManager m2 = testObjectsFactory.generateFlatEntityWithoutId(ContentManager.class);
        m2.setEmail("test2@mail.ru");
        m2.setUserStatus(UserStatus.ACTIVE);
        contentManagerRepository.save(m2);

        ContentManagerFilter filter = new ContentManagerFilter();
        filter.setEmail("test1@mail.ru");
        filter.setUserStatus(UserStatus.NEW);
        PageRequest pageRequest = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "email"));

        Assertions.assertThat(contentManagerService.getContentManagers(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(m1.getId()));
    }

    @Test
    public void testGetContentManagersWithFilterByUserStatusWithPagingAndSorting() throws InterruptedException {
        ContentManager m1 = testObjectsFactory.generateFlatEntityWithoutId(ContentManager.class);
        m1.setEmail("test1@mail.ru");
        m1.setUserStatus(UserStatus.NEW);
        contentManagerRepository.save(m1);
        ContentManager m2 = testObjectsFactory.generateFlatEntityWithoutId(ContentManager.class);
        m2.setEmail("test2@mail.ru");
        m2.setUserStatus(UserStatus.NEW);
        contentManagerRepository.save(m2);
        ContentManager m3 = testObjectsFactory.generateFlatEntityWithoutId(ContentManager.class);
        m3.setEmail("test3@mail.ru");
        m3.setUserStatus(UserStatus.ACTIVE);
        contentManagerRepository.save(m3);

        ContentManagerFilter filter = new ContentManagerFilter();
        filter.setUserStatus(UserStatus.NEW);
        PageRequest pageRequest = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "email"));

        Assertions.assertThat(contentManagerService.getContentManagers(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(m1.getId(), m2.getId()));
    }
}
