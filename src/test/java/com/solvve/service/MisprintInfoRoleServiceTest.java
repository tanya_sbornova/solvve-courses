package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.MisprintInfoRole;
import com.solvve.dto.misprintinforole.MisprintInfoRoleReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.MisprintInfoRoleRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.UUID;

public class MisprintInfoRoleServiceTest extends BaseTest {

    @Autowired
    private MisprintInfoRoleRepository misprintInfoRepository;

    @Autowired
    private MisprintInfoRoleService misprintInfoService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetMisprintInfoRole() {

        MisprintInfoRole misprintInfo = testObjectsFactory.createMisprintInfoRole();

        MisprintInfoRoleReadDTO readDTO = misprintInfoService.getMisprintInfoRole(misprintInfo.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(misprintInfo,
                "contentId", "registeredUserId", "contentManagerId");
        Assert.assertEquals(readDTO.getRegisteredUserId(), misprintInfo.getRegisteredUser().getId());
        Assert.assertEquals(readDTO.getContentId(), misprintInfo.getContent().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetMisprintInfoRoleWrongId() {

        misprintInfoService.getMisprintInfoRole(UUID.randomUUID());
    }

    @Test
    public void testDeleteMisprintInfo() {

        MisprintInfoRole misprintInfo = testObjectsFactory.createMisprintInfoRole();

        misprintInfoService.deleteMisprintInfoRole(misprintInfo.getId());
        Assert.assertFalse(misprintInfoRepository.existsById(misprintInfo.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteMisprintInfoNotFound() {

        misprintInfoService.deleteMisprintInfoRole(UUID.randomUUID());
    }
}

