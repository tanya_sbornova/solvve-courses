package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.ReviewFilmLikeDislike;
import com.solvve.domain.RegisteredUser;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeCreateDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePatchDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePutDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ReviewFilmLikeDislikeRepository;
import com.solvve.repository.RegisteredUserRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class RegisteredUserReviewFilmLikeServiceTest extends BaseTest {

    @Autowired
    private ReviewFilmLikeDislikeRepository reviewFilmLikeDislikeRepository;

    @Autowired
    private RegisteredUserReviewFilmLikeService registeredUserReviewFilmLikeService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void getRegisteredUserReviewFilmLikes() {

        ReviewFilmLikeDislike reviewFilmLikeDislike1 = testObjectsFactory.createReviewFilmLikeDislike();
        RegisteredUser registeredUser = reviewFilmLikeDislike1.getRegisteredUser();

        List<UUID> reviewFilmLikeDislikesId = transactionTemplate.execute(status -> {
            RegisteredUser registeredUser1 = registeredUserRepository.findById(registeredUser.getId()).get();
            return registeredUser1.getReviewFilmLikeDislikes().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });

        List<ReviewFilmLikeDislikeReadDTO> reviewFilmLikeDislikesReadDTO = registeredUserReviewFilmLikeService.
                getRegisteredUserReviewFilmLikes(registeredUser.getId());

        Assertions.assertThat(reviewFilmLikeDislikesReadDTO).extracting("id").
                containsExactlyInAnyOrder(reviewFilmLikeDislikesId.toArray());
    }

    @Test
    public void getRegisteredUserReviewFilmLike() {

        ReviewFilmLikeDislike reviewFilmLikeDislike = testObjectsFactory.createReviewFilmLikeDislike();
        RegisteredUser registeredUser = reviewFilmLikeDislike.getRegisteredUser();

        List<UUID> reviewFilmLikeDislikesId = transactionTemplate.execute(status -> {
            RegisteredUser registeredUser1 = registeredUserRepository.findById(registeredUser.getId()).get();
            return registeredUser1.getReviewFilmLikeDislikes().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });

        ReviewFilmLikeDislikeReadDTO reviewFilmLikeDislikeReadDTO = registeredUserReviewFilmLikeService.
                getRegisteredUserReviewFilmLike(registeredUser.getId(),
                        reviewFilmLikeDislikesId.get(0));

        reviewFilmLikeDislike = reviewFilmLikeDislikeRepository.findById(reviewFilmLikeDislikesId.get(0)).get();

        Assertions.assertThat(reviewFilmLikeDislikeReadDTO).isEqualToIgnoringGivenFields(reviewFilmLikeDislike,
                "registeredUserId", "reviewFilmId");
        Assert.assertEquals(reviewFilmLikeDislike.getReviewFilm().getId(),
                reviewFilmLikeDislikeReadDTO.getReviewFilmId());
        Assert.assertEquals(reviewFilmLikeDislike.getRegisteredUser().getId(),
                reviewFilmLikeDislikeReadDTO.getRegisteredUserId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRegisteredUserReviewFilmWrongId() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        registeredUserReviewFilmLikeService.getRegisteredUserReviewFilmLike(registeredUser.getId(), UUID.randomUUID());
    }

    @Test
    public void testCreateRegisteredUserReviewFilm() {

        ReviewFilmLikeDislikeCreateDTO create = testObjectsFactory.createReviewFilmLikeDislikeCreateDTO();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        ReviewFilmLikeDislikeReadDTO read = registeredUserReviewFilmLikeService.
                createRegisteredUserReviewFilmLike(registeredUser.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ReviewFilmLikeDislike reviewFilmLikeDislike = reviewFilmLikeDislikeRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(reviewFilmLikeDislike,
                "reviewFilmId", "registeredUserId");
        Assert.assertEquals(read.getReviewFilmId(), reviewFilmLikeDislike.getReviewFilm().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateRegisteredUserReviewFilmWithWrongReviewFilm() {

        ReviewFilmLikeDislikeCreateDTO create = testObjectsFactory.createReviewFilmLikeDislikeWithWrongReviewFilm();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        ReviewFilmLikeDislikeReadDTO read = registeredUserReviewFilmLikeService.
                createRegisteredUserReviewFilmLike(registeredUser.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ReviewFilmLikeDislike reviewReviewFilm = reviewFilmLikeDislikeRepository.findById(read.getId()).get();
        Assertions.assertThat(reviewReviewFilm).isEqualToIgnoringGivenFields(read, "reviewFilm", "registeredUser");
    }

    @Test
    public void testPatchRegisteredUserReviewFilm() {

        ReviewFilmLikeDislike reviewFilmLikeDislike1 = testObjectsFactory.createReviewFilmLikeDislike();
        ReviewFilmLikeDislikePatchDTO patch = new ReviewFilmLikeDislikePatchDTO();
        patch.setIsLike(true);

        RegisteredUser registeredUser1 = reviewFilmLikeDislike1.getRegisteredUser();
        ReviewFilmLikeDislike reviewFilmLikeDislike = transactionTemplate.execute(status -> {
            return registeredUserRepository.findById(registeredUser1.getId()).get().
                    getReviewFilmLikeDislikes().get(0);
        });

        ReviewFilmLikeDislikeReadDTO read = registeredUserReviewFilmLikeService.
                patchRegisteredUserReviewFilmLike(registeredUser1.getId(),
                        reviewFilmLikeDislike.getId(), patch);

        inTransaction(()-> {
            Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
            ReviewFilmLikeDislike reviewFilmLikeDislike2 = reviewFilmLikeDislikeRepository.
                    findById(reviewFilmLikeDislike1.getId()).get();
            Assertions.assertThat(reviewFilmLikeDislike2).isEqualToIgnoringGivenFields(read,
                    "registeredUser", "reviewFilm", "createdAt", "updatedAt");
        });
    }

    @Test
    public void testUpdateRegisteredUserReviewFilm() {

        ReviewFilmLikeDislike reviewFilm1 = testObjectsFactory.createReviewFilmLikeDislike();
        ReviewFilmLikeDislikePutDTO put = new ReviewFilmLikeDislikePutDTO();
        put.setIsLike(true);

        RegisteredUser registeredUser1 = reviewFilm1.getRegisteredUser();
        ReviewFilmLikeDislike reviewFilm = transactionTemplate.execute(status -> {
            return registeredUserRepository.findById(registeredUser1.getId()).get().
                    getReviewFilmLikeDislikes().get(0);
        });

        ReviewFilmLikeDislikeReadDTO read = registeredUserReviewFilmLikeService.
                updateRegisteredUserReviewFilmLike(registeredUser1.getId(),
                        reviewFilm.getId(), put);

        inTransaction(()-> {
            Assertions.assertThat(put).isEqualToComparingFieldByField(read);
            ReviewFilmLikeDislike reviewFilmLikeDislike = reviewFilmLikeDislikeRepository.findById(read.getId()).get();
            Assertions.assertThat(reviewFilmLikeDislike).isEqualToIgnoringGivenFields(read,
                    "registeredUser", "reviewFilm", "createdAt", "updatedAt");
        });
    }

    @Test
    public void testDeleteRegisteredUserReviewFilm() {

        ReviewFilmLikeDislike roleLikeDislike = testObjectsFactory.createReviewFilmLikeDislike();
        RegisteredUser registeredUser = roleLikeDislike.getRegisteredUser();
        UUID reviewFilmLikeDislikeUUID = roleLikeDislike.getId();

        registeredUserReviewFilmLikeService.deleteRegisteredUserReviewFilmLike(registeredUser.getId(),
                reviewFilmLikeDislikeUUID);
        Assert.assertFalse(reviewFilmLikeDislikeRepository.existsById(reviewFilmLikeDislikeUUID));
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status->{
            runnable.run();
        });
    }
}
