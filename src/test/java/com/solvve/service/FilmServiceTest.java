package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.*;
import com.solvve.dto.crewmember.CrewMemberReadDTO;
import com.solvve.dto.film.*;
import com.solvve.dto.genre.GenreReadDTO;
import com.solvve.dto.ratingfilm.RatingFilmReadDTO;
import com.solvve.dto.reviewfilm.ReviewFilmReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.FilmRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

public class FilmServiceTest extends BaseTest {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private FilmService filmService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;
    
    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testGetFilm() {

        Film film = testObjectsFactory.createFilm();

        FilmReadDTO readDTO = filmService.getFilm(film.getId());

        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(film,
                "companyId");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetFilmWrongId() {
        filmService.getFilm(UUID.randomUUID());
    }

    @Test
    public void testGetFilmExtended() {

        Film film = testObjectsFactory.createFilm();

        List<UUID> crewMembersId = transactionTemplate.execute(status -> {
            Film film1 = filmRepository.findById(film.getId()).get();
            return film1.getCrewMembers().stream().
                    map(CrewMember::getId).collect(Collectors.toList());
        });

        List<UUID> ratingFilmsId = transactionTemplate.execute(status -> {
            Film film1 = filmRepository.findById(film.getId()).get();
            return film1.getRatingFilms().stream().
                    map(RatingFilm::getId).collect(Collectors.toList());
        });

        List<UUID> reviewFilmsId = transactionTemplate.execute(status -> {
            Film film1 = filmRepository.findById(film.getId()).get();
            return film1.getReviewFilms().stream().
                    map(ReviewFilm::getId).collect(Collectors.toList());
        });

        List<UUID> genresId = transactionTemplate.execute(status -> {
            Film film1 = filmRepository.findById(film.getId()).get();
            return film1.getGenres().stream().
                    map(Genre::getId).collect(Collectors.toList());
        });

        FilmReadExtendedDTO readDTO = filmService.getFilmExtended(film.getId());
        inTransaction(() -> {
            Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(film,
                    "crewMembers", "ratingFilms", "reviewFilms", "genres");

            Assert.assertEquals(crewMembersId,
                    readDTO.getCrewMembers().stream().map(CrewMemberReadDTO::getId).collect(Collectors.toList()));

            Assert.assertEquals(reviewFilmsId,
                    readDTO.getReviewFilms().stream().map(ReviewFilmReadDTO::getId).collect(Collectors.toList()));

            Assert.assertEquals(ratingFilmsId,
                    readDTO.getRatingFilms().stream().map(RatingFilmReadDTO::getId).collect(Collectors.toList()));

            Assert.assertEquals(genresId,
                    readDTO.getGenres().stream().map(GenreReadDTO::getId).collect(Collectors.toList()));
        });
    }

    @Test
    public void testCreateFilm() {

        FilmCreateDTO create = new FilmCreateDTO();
        create.setTitle("The Aeronauts");
        create.setText("The most significant balloon flight");
        create.setDatePrime(LocalDate.of(2019, 11, 4));
        create.setBudget(40000);
        create.setStatus(FilmStatus.RELEASED);

        FilmReadDTO read = filmService.createFilm(create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Film film = filmRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(film,
                "companyId");
    }

    @Test
    public void testPatchFilm() {

        Film film = testObjectsFactory.createFilm();

        FilmPatchDTO patch = new FilmPatchDTO();
        patch.setTitle("The Aeronauts");
        patch.setText("The most significant balloon flight");
        patch.setDatePrime(LocalDate.of(2019, 11, 4));
        patch.setBudget(40000);
        patch.setStatus(FilmStatus.RELEASED);

        FilmReadDTO read = filmService.patchFilm(film.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        film = filmRepository.findById(read.getId()).get();
        Assertions.assertThat(film).isEqualToIgnoringGivenFields(read,
                "ratingFilms", "reviewFilms", "crewMembers", "genres", "companies", "description");
    }

    @Test
    public void testPatchFilmEmptyPatch() {

        Film film = testObjectsFactory.createFilm();

        FilmPatchDTO patch = new FilmPatchDTO();

        FilmReadDTO read = filmService.patchFilm(film.getId(), patch);

        Assert.assertNotNull(read.getTitle());
        Assert.assertNotNull(read.getText());
        Assert.assertNotNull(read.getDatePrime());
        Assert.assertNotNull(read.getBudget());
        Assert.assertNotNull(read.getStatus());

        Film filmAfterUpdate = filmRepository.findById(read.getId()).get();

        Assert.assertNotNull(filmAfterUpdate.getTitle());
        Assert.assertNotNull(filmAfterUpdate.getText());
        Assert.assertNotNull(filmAfterUpdate.getDatePrime());
        Assert.assertNotNull(filmAfterUpdate.getBudget());
        Assert.assertNotNull(filmAfterUpdate.getStatus());

        Assertions.assertThat(film).isEqualToIgnoringGivenFields(filmAfterUpdate,
                "ratingFilms", "reviewFilms", "crewMembers", "genres", "companies");
    }

    @Test
    public void testPutFilm() {

        Film film = testObjectsFactory.createFilm();

        FilmPutDTO put = new FilmPutDTO();
        put.setTitle("The Aeronauts");
        put.setText("The most significant balloon flight");
        put.setDatePrime(LocalDate.of(2019, 11, 4));
        put.setBudget(40000);
        put.setStatus(FilmStatus.RELEASED);

        FilmReadDTO read = filmService.updateFilm(film.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        film = filmRepository.findById(read.getId()).get();
        Assertions.assertThat(film).isEqualToIgnoringGivenFields(read,
                "ratingFilms", "reviewFilms", "crewMembers", "genres", "companies", "description");
    }

    @Test
    public void testDeleteFilm() {

        Film film = testObjectsFactory.createFilm();

        filmService.deleteFilm(film.getId());
        Assert.assertFalse(filmRepository.existsById(film.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteContentManagerNotFound() {

        filmService.deleteFilm(UUID.randomUUID());
    }

    @Test
    public void updateAverageRatingOfFilm() {

        Film film = testObjectsFactory.createFlatFilm();
        testObjectsFactory.createRatingFilm(film, 7.0);
        testObjectsFactory.createRatingFilm(film, 8.0);
        testObjectsFactory.createRatingFilm(film, 9.0);

        filmService.updateAverageRatingOfFilm(film.getId());
        film = filmRepository.findById(film.getId()).get();

        Assert.assertEquals(8.0, film.getAverageRating(), Double.MIN_NORMAL);
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveFilmValidation() {

        Film film = new Film();
        filmRepository.save(film);
    }

    @Test
    public void getFilmsLeaderBoard() {
        int filmsCount = 100;
        Set<UUID> filmIds = new HashSet<>();
        for (int i = 1; i < filmsCount; i++) {
            Film film = testObjectsFactory.createFlatFilm();
            filmIds.add(film.getId());
            testObjectsFactory.createRatingFilm(film, Math.random() * (7.0 - 3.0 + 1) + 3.0);
            testObjectsFactory.createRatingFilm(film, Math.random() * (7.0 - 3.0 + 1) + 3.0);
            testObjectsFactory.createRatingFilm(film, 1.0);
            testObjectsFactory.createRatingFilm(film, 2.0);
            testObjectsFactory.createRatingFilm(film, 9.0);
            testObjectsFactory.createRatingFilm(film, 9.0);
            testObjectsFactory.createRatingFilm(film, 10.0);
            filmService.updateAverageRatingOfFilm(film.getId());
        }
        List<FilmInLeaderBoardReadDTO> filmsLeaderBoard = filmService.getFilmsLeaderBoard();
        Assertions.assertThat(filmsLeaderBoard).
                isSortedAccordingTo(Comparator.comparing(FilmInLeaderBoardReadDTO::getAverageRating).reversed());
        Assert.assertEquals(filmIds,
                filmsLeaderBoard.stream().map(FilmInLeaderBoardReadDTO::getId).collect(Collectors.toSet()));

        for(FilmInLeaderBoardReadDTO f: filmsLeaderBoard) {
            Assert.assertNotNull(f.getText());
            Assert.assertNotNull(f.getAverageRating());
            Assert.assertEquals(2, (long) f.getMinRatingsCount());
            Assert.assertEquals(3, (long) f.getMaxRatingsCount());
        }
    }

    @Test
    public void testGetFilmsWithAllFiltersWithPagingAndSorting() throws InterruptedException {
        Film m1 = testObjectsFactory.generateFlatEntityWithoutId(Film.class);
        m1.setBudget(45000);
        m1.setDatePrime(LocalDate.of(2019, 3, 21));
        m1.setStatus(FilmStatus.NOT_RELEASED);
        filmRepository.save(m1);
        Film m2 = testObjectsFactory.generateFlatEntityWithoutId(Film.class);
        filmRepository.save(m2);

        FilmFilter filter = new FilmFilter();
        filter.setBudgetFrom(40000);
        filter.setBudgetTo(50000);
        filter.setDatePrimeFrom(LocalDate.of(2018, 3, 21));
        filter.setDatePrimeTo(LocalDate.of(2020, 3, 21));
        filter.setStatus(FilmStatus.NOT_RELEASED);

        PageRequest pageRequest = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "title"));

        Assertions.assertThat(filmService.getFilms(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(m1.getId()));
    }

    private Instant createInstant(int year, int month, int day) {
        return LocalDateTime.of(year, month, day, 0, 0).toInstant(ZoneOffset.UTC);
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }
}
