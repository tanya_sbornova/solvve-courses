package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.CrewMember;
import com.solvve.domain.Film;
import com.solvve.dto.crewmember.CrewMemberPatchDTO;
import com.solvve.dto.crewmember.CrewMemberPutDTO;
import com.solvve.dto.crewmember.CrewMemberReadDTO;
import com.solvve.repository.CrewMemberRepository;
import com.solvve.repository.FilmRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class FilmCrewMemberServiceTest extends BaseTest {

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private FilmCrewMemberService filmCrewMemberService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Test
    public void getFilmCrewMembers() {

        Film film = testObjectsFactory.createFlatFilm();

        List<UUID> crewMembersId = transactionTemplate.execute(status -> {
            Film film1 = filmRepository.findById(film.getId()).get();
            return film1.getCrewMembers().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });

        List<CrewMemberReadDTO> crewMembersReadDTO = filmCrewMemberService.getFilmCrewMembers(film.getId());
        Assertions.assertThat(crewMembersReadDTO).extracting("id").
                containsExactlyInAnyOrder(crewMembersId.toArray());
    }

    @Test
    public void getFilmCrewMember()  {

        Film film = testObjectsFactory.createFilm();

        CrewMemberReadDTO crewMemberReadDTO = filmCrewMemberService.
                    getFilmCrewMember(film.getId(), film.getCrewMembers().stream().findFirst().get().getId());
        Assertions.assertThat(crewMemberReadDTO).isEqualToIgnoringGivenFields((film.getCrewMembers()).
                        stream().findFirst().get(),
                    "personId", "departmentId");
    }

    @Test
    public void testPatchFilmCrewMember() {

        Film film = testObjectsFactory.createFilm();
        CrewMemberPatchDTO patch = testObjectsFactory.createCrewMemberPatchDTO();
        UUID cmId = film.getCrewMembers().stream().findFirst().get().getId();

        CrewMemberReadDTO read = filmCrewMemberService.patchFilmCrewMember(film.getId(),
                cmId, patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
        CrewMember crewMember = crewMemberRepository.findById(read.getId()).get();
        Assertions.assertThat(crewMember).isEqualToIgnoringGivenFields(read,
                "person", "department", "films");
        Assert.assertEquals(read.getPersonId(), crewMember.getPerson().getId());
    }

    @Test
    public void testUpdateFilmCrewMember() {

        Film film = testObjectsFactory.createFilm();
        CrewMemberPutDTO put = testObjectsFactory.createCrewMemberPutDTO();

        CrewMemberReadDTO read = transactionTemplate.execute(status-> {

            CrewMemberReadDTO read1 = filmCrewMemberService.updateFilmCrewMember(film.getId(),
                    film.getCrewMembers().stream().findFirst().get().getId(), put);
            return read1;
        });

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);
        CrewMember crewMember = crewMemberRepository.findById(read.getId()).get();
        Assertions.assertThat(crewMember).isEqualToIgnoringGivenFields(read,
                    "person", "department", "films", "updatedAt");
        Assert.assertEquals(read.getPersonId(), crewMember.getPerson().getId());
    }
}
