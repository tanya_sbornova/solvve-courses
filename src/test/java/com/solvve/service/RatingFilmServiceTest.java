package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.RatingFilm;
import com.solvve.dto.ratingfilm.RatingFilmPatchDTO;
import com.solvve.dto.ratingfilm.RatingFilmPutDTO;
import com.solvve.dto.ratingfilm.RatingFilmReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.RatingFilmRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;
import java.util.UUID;

public class RatingFilmServiceTest extends BaseTest {

    @Autowired
    private RatingFilmRepository ratingFilmRepository;

    @Autowired
    private RatingFilmService ratingFilmService;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testGetRatingFilm() {

        Film film = testObjectsFactory.createFilm();
        RatingFilm ratingFilm = testObjectsFactory.createRatingFilm(film);

        RatingFilmReadDTO readDTO = ratingFilmService.getRatingFilm(ratingFilm.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(ratingFilm, "filmId", "registeredUserId");
        Assert.assertEquals(ratingFilm.getFilm().getId(), readDTO.getFilmId());
        Assert.assertEquals(ratingFilm.getRegisteredUser().getId(), readDTO.getRegisteredUserId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRatingFilmWrongId() {
        ratingFilmService.getRatingFilm(UUID.randomUUID());
    }

    @Test
    public void testPatchRatingFilm() {

        Film film = testObjectsFactory.createFilm();
        RatingFilm ratingFilm = testObjectsFactory.createRatingFilm(film);
        RatingFilmPatchDTO patch = new RatingFilmPatchDTO();
        patch.setRating(7.6);

        RatingFilmReadDTO read = ratingFilmService.patchRatingFilm(ratingFilm.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        ratingFilm = ratingFilmRepository.findById(read.getId()).get();
        Assertions.assertThat(ratingFilm).isEqualToIgnoringGivenFields(read,
                "film", "registeredUser");
        Assert.assertEquals(ratingFilm.getFilm().getId(), read.getFilmId());
        Assert.assertEquals(ratingFilm.getRegisteredUser().getId(), read.getRegisteredUserId());
    }

    @Test
    public void testPatchRatingFilmEmptyPatch() {

        Film film = testObjectsFactory.createFilm();
        RatingFilm ratingFilm = testObjectsFactory.createRatingFilm(film);

        RatingFilmPatchDTO patch = new RatingFilmPatchDTO();
        RatingFilmReadDTO read = ratingFilmService.patchRatingFilm(ratingFilm.getId(), patch);

        Assert.assertNotNull(read.getRating());

        RatingFilm ratingFilmAfterUpdate = ratingFilmRepository.findById(read.getId()).get();

        Assert.assertNotNull(ratingFilmAfterUpdate.getRating());

        Assertions.assertThat(ratingFilm).isEqualToIgnoringGivenFields(ratingFilmAfterUpdate,
                "film", "registeredUser");
        Assertions.assertThat(ratingFilm.getFilm()).isEqualToIgnoringGivenFields(ratingFilmAfterUpdate.getFilm(),
                "ratingFilms", "reviewFilms", "crewMembers", "genres", "companies");
        Assertions.assertThat(ratingFilm.getRegisteredUser()).
                isEqualToIgnoringGivenFields(ratingFilmAfterUpdate.getRegisteredUser(),
                        "reviewFilmLikeDislikes", "reviewRoleLikeDislikes", "newsLikeDislikes", "problemReviews",
                        "misprintInfoFilms", "misprintInfoRoles", "misprintInfoNews");
    }

    @Test
    public void testUpdateRatingFilm() {

        Film film = testObjectsFactory.createFilm();
        RatingFilm ratingFilm1 = testObjectsFactory.createRatingFilm(film);

        RatingFilmPutDTO put = new RatingFilmPutDTO();
        put.setRating(8.4);

        RatingFilmReadDTO read = ratingFilmService.updateRatingFilm(ratingFilm1.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        inTransaction(()-> {
            RatingFilm ratingFilm = ratingFilmRepository.findById(read.getId()).get();
            Film filmAfter = filmRepository.findById(read.getFilmId()).get();
            Assert.assertEquals(ratingFilm.getFilm().getId(), read.getFilmId());
            Assert.assertEquals(ratingFilm.getRegisteredUser().getId(), read.getRegisteredUserId());
        });
    }

    @Test
    public void testDeleteRatingFilm() {

        Film film = testObjectsFactory.createFilm();
        RatingFilm ratingFilm = testObjectsFactory.createRatingFilm(film);

        ratingFilmService.deleteRatingFilm(ratingFilm.getId());
        Assert.assertFalse(ratingFilmRepository.existsById(ratingFilm.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteRatingFilmNotFound() {

        ratingFilmService.deleteRatingFilm(UUID.randomUUID());
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveRatingFilmValidation() {

        RatingFilm ratingFilm = new RatingFilm();
        ratingFilmRepository.save(ratingFilm);
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status->{
            runnable.run();
        });
    }
}



