package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.ContentManager;
import com.solvve.domain.Film;
import com.solvve.domain.MisprintInfoFilm;
import com.solvve.domain.MisprintStatus;
import com.solvve.dto.misprintinfofix.MisprintInfoFixDTO;
import com.solvve.dto.misprintinfofix.MisprintInfoFixReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.EntityWrongStatusException;
import com.solvve.repository.FilmRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.time.Instant;
import java.util.*;

import static org.hamcrest.CoreMatchers.is;

public class MisprintInfoFilmFixServiceTest extends BaseTest {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private MisprintInfoFixService misprintInfoService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test(expected = EntityNotFoundException.class)
    public void testFixContentWrongId()  {
        ContentManager contentManager = testObjectsFactory.createContentManager();
        MisprintInfoFixDTO put = new MisprintInfoFixDTO();
        misprintInfoService.fixContent(contentManager.getId(), UUID.randomUUID(), put);
    }

    @Test(expected = EntityWrongStatusException.class)
    public void testFixContentWrongStatus()  {
        MisprintInfoFilm misprintInfoFilm = testObjectsFactory.createMisprintInfoFilmWrongStatus();
        ContentManager contentManager = testObjectsFactory.createContentManager();
        MisprintInfoFixDTO put = new MisprintInfoFixDTO();
        misprintInfoService.fixContent(contentManager.getId(), misprintInfoFilm.getId(), put);
    }

    @Test
    public void testFixContentUserVersion() throws InterruptedException {
        Film film = testObjectsFactory.createFilm();
        film.setText("Is about a V_rtual Reality");
        film = filmRepository.save(film);
        Film film2 = testObjectsFactory.createFilm();
        MisprintInfoFixDTO put = new MisprintInfoFixDTO();
        ContentManager contentManager = testObjectsFactory.createContentManager();
        MisprintInfoFilm misprintInfoFilm = testObjectsFactory.createMpInfoFilmWithParam(film, 9,19,
                "a Virtual", "a V_rtual");
        List<UUID> expectedIdsOfMisprints = new ArrayList<>();
        expectedIdsOfMisprints.add(misprintInfoFilm.getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoFilmWithParam(film, 11,19,
                "Virtual", "V_rtual").getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoFilmWithParam(film, 11,16,"Virt", "V_rt").getId());
        testObjectsFactory.createMpInfoFilmWithParam(film, 5,17,"XXXXXXXXXXX", "");
        testObjectsFactory.createMpInfoFilmWithParam(film2, 9,19, "a Virtual", "a V_rtual");

        Instant dateBeforeTest = Instant.now();
        Thread.sleep(10);

        List<MisprintInfoFixReadDTO> mpFilmReads = misprintInfoService.fixContent(contentManager.getId(),
                misprintInfoFilm.getId(), put);

        Thread.sleep(10);
        Instant dateAfterTest = Instant.now();

        Assert.assertEquals(expectedIdsOfMisprints.size(), mpFilmReads.size());
        Assertions.assertThat(mpFilmReads).extracting("id").
                containsExactlyInAnyOrder(expectedIdsOfMisprints.toArray());
        Instant actualFixDate = mpFilmReads.get(0).getFixedDate();
        Assert.assertThat(dateBeforeTest.isBefore(actualFixDate), is(true));
        Assert.assertThat(dateAfterTest.isAfter(actualFixDate), is(true));
        mpFilmReads.forEach(mpFilmRead->{
            Assert.assertEquals(mpFilmRead.getStatus(), MisprintStatus.FIXED);
            Assert.assertEquals(mpFilmRead.getContentManagerId(), contentManager.getId());
            Assert.assertEquals(mpFilmRead.getText(), misprintInfoFilm.getText());
            Assert.assertEquals(mpFilmRead.getFixedDate(), actualFixDate);
        });
        String adjustedText = prepareText(misprintInfoFilm,  misprintInfoFilm.getText());
        Film filmAfter = filmRepository.findById(film.getId()).get();
        Assert.assertEquals(filmAfter.getText(), adjustedText);
    }

    @Test
    public void testFixContentCMVersion() throws InterruptedException {

        Film film = testObjectsFactory.createFilm();
        film.setText("Is about a V_rtual Reality");
        film = filmRepository.save(film);
        Film film2 = testObjectsFactory.createFilm();
        MisprintInfoFixDTO put = new MisprintInfoFixDTO();
        ContentManager contentManager = testObjectsFactory.createContentManager();
        put.setText("a fiction");
        MisprintInfoFilm misprintInfoFilm = testObjectsFactory.createMpInfoFilmWithParam(film,
                9,19, "a Virtual", "a V_rtual");

        List<UUID> expectedIdsOfMisprints = new ArrayList<>();
        expectedIdsOfMisprints.add(misprintInfoFilm.getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoFilmWithParam(film, 11,19, "Virtual",
                "V_rtual").getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoFilmWithParam(film, 11,17,"Virt", "V_rt").getId());
        testObjectsFactory.createMpInfoFilmWithParam(film, 5,17,"XXXXXXXXXXX", "");
        testObjectsFactory.createMpInfoFilmWithParam(film2, 9,19, "a Virtual", "a V_rtual");

        Instant dateBeforeTest = Instant.now();
        Thread.sleep(5);

        List<MisprintInfoFixReadDTO> mpFilmReads =
                misprintInfoService.fixContent(contentManager.getId(), misprintInfoFilm.getId(), put);

        Thread.sleep(5);
        Instant dateAfterTest = Instant.now();

        Assert.assertEquals(expectedIdsOfMisprints.size(), mpFilmReads.size());
        Assertions.assertThat(mpFilmReads).extracting("id").
                containsExactlyInAnyOrder(expectedIdsOfMisprints.toArray());
        Instant actualFixDate = mpFilmReads.get(0).getFixedDate();
        Assert.assertThat(dateBeforeTest.isBefore(actualFixDate), is(true));
        Assert.assertThat(dateAfterTest.isAfter(actualFixDate), is(true));
        mpFilmReads.forEach(mpFilmRead->{
            Assert.assertEquals(mpFilmRead.getStatus(), MisprintStatus.FIXED);
            Assert.assertEquals(mpFilmRead.getContentManagerId(), contentManager.getId());
            Assert.assertEquals(mpFilmRead.getText(), "a fiction");
            Assert.assertEquals(mpFilmRead.getFixedDate(), actualFixDate);
        });
        String adjustedText = prepareText(misprintInfoFilm,  "a fiction");
        Film filmAfter = filmRepository.findById(film.getId()).get();
        Assert.assertEquals(filmAfter.getText(), adjustedText);
    }

    @Test
    public void testFixContentWithDelay() {
        Film film = testObjectsFactory.createFilm();
        film.setText("Is about a Virtual Reality");
        film = filmRepository.save(film);
        Film film2 = testObjectsFactory.createFilm();
        MisprintInfoFixDTO put = new MisprintInfoFixDTO();
        ContentManager contentManager = testObjectsFactory.createContentManager();
        MisprintInfoFilm misprintInfoFilm = testObjectsFactory.createMpInfoFilmWithParam(film, 9,19,
                "a Virtual", "a V_rtual");
        List<UUID> expectedIdsOfMisprints = List.of(misprintInfoFilm.getId());
        testObjectsFactory.createMpInfoFilmWithParam(film, 11,19,
               "Virtual", "V_rtual").getId();
        testObjectsFactory.createMpInfoFilmWithParam(film, 11,16,"Virt", "V_rt").getId();
        testObjectsFactory.createMpInfoFilmWithParam(film, 5,17,"XXXXXXXXXXX", "");
        testObjectsFactory.createMpInfoFilmWithParam(film2, 9,19, "a Virtual", "a V_rtual");

        List<MisprintInfoFixReadDTO> mpFilmReads = misprintInfoService.fixContent(contentManager.getId(),
                misprintInfoFilm.getId(), put);

        Assert.assertEquals(mpFilmReads.size(), 1);
        Assertions.assertThat(mpFilmReads).extracting("id").
                containsExactlyInAnyOrder(expectedIdsOfMisprints.toArray());
        Assert.assertEquals(mpFilmReads.get(0).getStatus(), MisprintStatus.CANCELLED);
        String adjustedText = prepareText(misprintInfoFilm,  misprintInfoFilm.getText());
        Film filmAfter = filmRepository.findById(film.getId()).get();
        Assert.assertNotEquals(filmAfter.getText(), adjustedText);
    }

    private String prepareText(MisprintInfoFilm misprintInfo,  String replacement) {
        String description = misprintInfo.getContent().getText();
        StringBuilder builder = new StringBuilder(description);
        builder.replace(misprintInfo.getStartIndex(), misprintInfo.getEndIndex(), replacement);
        return builder.toString();
    }
}

