package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.*;
import com.solvve.dto.misprintinfofix.MisprintInfoFixDTO;
import com.solvve.dto.misprintinfofix.MisprintInfoFixReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.EntityWrongStatusException;
import com.solvve.repository.NewsRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.*;

import static org.hamcrest.CoreMatchers.is;

public class MisprintInfoNewsFixServiceTest extends BaseTest {

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private MisprintInfoFixService misprintInfoService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test(expected = EntityNotFoundException.class)
    public void testFixMisprintInfoNewsWrongId() {
        ContentManager contentManager = testObjectsFactory.createContentManager();
        MisprintInfoFixDTO patch = new MisprintInfoFixDTO();
        misprintInfoService.fixContent(contentManager.getId(), UUID.randomUUID(), patch);
    }

    @Test(expected = EntityWrongStatusException.class)
    public void testFixMisprintInfoNewsWrongStatus() {
        MisprintInfoNews misprintInfoNews = testObjectsFactory.createMisprintInfoNewsWrongStatus();
        ContentManager contentManager = testObjectsFactory.createContentManager();
        MisprintInfoFixDTO patch = new MisprintInfoFixDTO();
        misprintInfoService.fixContent(contentManager.getId(), misprintInfoNews.getId(), patch);
    }

    @Test
    public void testFixMisprintInfoNewsUserVersion() throws InterruptedException {

        Film film = testObjectsFactory.createFlatFilm();
        News news = testObjectsFactory.createFilmNews(film);
        News news2 = testObjectsFactory.createFilmNews(film);
        ContentManager contentManager = testObjectsFactory.createContentManager();
        MisprintInfoFixDTO patch = new MisprintInfoFixDTO();
        patch.setText(null);
        MisprintInfoNews misprintInfoNews = testObjectsFactory.createMpInfoNewsWithParam(news, 10, 17,
                "reveals", "rev_als");
        List<UUID> expectedIdsOfMisprints = new ArrayList<>();
        expectedIdsOfMisprints.add(misprintInfoNews.getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoNewsWithParam(news, 13, 14, "e", "_").getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoNewsWithParam(news, 12, 16, "veal", "v_al").getId());
        testObjectsFactory.createMpInfoNewsWithParam(news, 5, 17, "XXXXXXXXXXX", "");
        testObjectsFactory.createMpInfoNewsWithParam(news2, 12, 16, "veal", "v_al");

        Instant dateBeforeTest = Instant.now();
        Thread.sleep(10);

        List<MisprintInfoFixReadDTO> mpNewsReads =
                misprintInfoService.fixContent(contentManager.getId(), misprintInfoNews.getId(), patch);

        Thread.sleep(10);
        Instant dateAfterTest = Instant.now();

        Assert.assertEquals(expectedIdsOfMisprints.size(), mpNewsReads.size());
        Assertions.assertThat(mpNewsReads).extracting("id").
                containsExactlyInAnyOrder(expectedIdsOfMisprints.toArray());
        Instant actualFixDate = mpNewsReads.get(0).getFixedDate();
        Assert.assertThat(dateBeforeTest.isBefore(actualFixDate), is(true));
        Assert.assertThat(dateAfterTest.isAfter(actualFixDate), is(true));
        mpNewsReads.forEach(mpNewsRead -> {
            Assert.assertEquals(mpNewsRead.getStatus(), MisprintStatus.FIXED);
            Assert.assertEquals(mpNewsRead.getContentManagerId(), contentManager.getId());
            Assert.assertEquals(mpNewsRead.getText(), misprintInfoNews.getText());
            Assert.assertThat(dateBeforeTest.isBefore(mpNewsRead.getFixedDate()), is(true));
            Assert.assertThat(dateAfterTest.isAfter(mpNewsRead.getFixedDate()), is(true));
            Assert.assertEquals(mpNewsRead.getFixedDate(), actualFixDate);
        });
        String adjustedText = prepareText(misprintInfoNews, misprintInfoNews.getText());
        News newsAfter = newsRepository.findById(news.getId()).get();
        Assert.assertEquals(newsAfter.getText(), adjustedText);
    }

    @Test
    public void testFixMisprintInfoNewsCMVersion() throws InterruptedException {

        Film film = testObjectsFactory.createFlatFilm();
        News news = testObjectsFactory.createFilmNews(film);
        News news2 = testObjectsFactory.createFilmNews(film);
        ContentManager contentManager = testObjectsFactory.createContentManager();
        MisprintInfoFixDTO patch = new MisprintInfoFixDTO();
        patch.setText(" tells ");
        MisprintInfoNews misprintInfoNews = testObjectsFactory.createMpInfoNewsWithParam(news, 10, 17,
                "reveals", "rev_als");

        List<UUID> expectedIdsOfMisprints = new ArrayList<>();
        expectedIdsOfMisprints.add(misprintInfoNews.getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoNewsWithParam(news, 13, 14, "e", "_").getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoNewsWithParam(news, 12, 16, "veal", "v_al").getId());
        testObjectsFactory.createMpInfoNewsWithParam(news, 5, 17, "XXXXXXXXXXX", "");
        testObjectsFactory.createMpInfoNewsWithParam(news2, 12, 16, "veal", "v_al");

        Instant dateBeforeTest = Instant.now();
        Thread.sleep(10);
        List<MisprintInfoFixReadDTO> mpNewsReads =
                misprintInfoService.fixContent(contentManager.getId(), misprintInfoNews.getId(), patch);

        Thread.sleep(10);
        Instant dateAfterTest = Instant.now();

        Assert.assertEquals(expectedIdsOfMisprints.size(), mpNewsReads.size());
        Assertions.assertThat(mpNewsReads).extracting("id").
                containsExactlyInAnyOrder(expectedIdsOfMisprints.toArray());
        Instant actualFixDate = mpNewsReads.get(0).getFixedDate();
        Assert.assertThat(dateBeforeTest.isBefore(actualFixDate), is(true));
        Assert.assertThat(dateAfterTest.isAfter(actualFixDate), is(true));
        mpNewsReads.forEach(mpNewsRead -> {
            Assert.assertEquals(mpNewsRead.getStatus(), MisprintStatus.FIXED);
            Assert.assertEquals(mpNewsRead.getContentManagerId(), contentManager.getId());
            Assert.assertEquals(mpNewsRead.getText(), " tells ");
            Assert.assertEquals(mpNewsRead.getStatus(), MisprintStatus.FIXED);
            Assert.assertEquals(mpNewsRead.getFixedDate(), actualFixDate);
        });
        String adjustedText = prepareText(misprintInfoNews, " tells ");
        News newsAfter = newsRepository.findById(news.getId()).get();
        Assert.assertEquals(newsAfter.getText(), adjustedText);
    }

    private String prepareText(MisprintInfoNews misprintInfo, String replacement) {
        String description = misprintInfo.getContent().getText();
        StringBuilder builder = new StringBuilder(description);
        builder.replace(misprintInfo.getStartIndex(), misprintInfo.getEndIndex(), replacement);
        return builder.toString();
    }
}
