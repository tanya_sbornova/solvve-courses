package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.FilmGenre;
import com.solvve.domain.UserProfile;
import com.solvve.dto.userprofile.UserProfileCreateDTO;
import com.solvve.dto.userprofile.UserProfilePatchDTO;
import com.solvve.dto.userprofile.UserProfilePutDTO;
import com.solvve.dto.userprofile.UserProfileReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.UserProfileRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.UUID;

public class UserProfileServiceTest extends BaseTest {

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Autowired
    private UserProfileService userProfileService;

    @Test
    public void testGetUserProfile() {

        UserProfile userProfile = createUserProfile();

        UserProfileReadDTO readDTO = userProfileService.getUserProfile(userProfile.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(userProfile);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetUserProfileWrongId() {

        userProfileService.getUserProfile(UUID.randomUUID());
    }

    @Test
    public void testCreateUserProfile() {

        UserProfileCreateDTO create = new UserProfileCreateDTO();
        create.setRatingReviews(5.2);
        create.setRatingActivity(6.7);
        create.setFavouriteGenre(FilmGenre.ROMANCE);

        UserProfileReadDTO read = userProfileService.createUserProfile(create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        UserProfile userProfile = userProfileRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(userProfile);
    }

    @Test
    public void testPatchUserProfile() {

        UserProfile userProfile = createUserProfile();

        UserProfilePatchDTO patch = new UserProfilePatchDTO();
        patch.setRatingReviews(5.2);
        patch.setRatingActivity(6.7);
        patch.setFavouriteGenre(FilmGenre.ROMANCE);

        UserProfileReadDTO read = userProfileService.patchUserProfile(userProfile.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        userProfile = userProfileRepository.findById(read.getId()).get();
        Assertions.assertThat(userProfile).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchUserProfileEmptyPatch() {

        UserProfile userProfile = createUserProfile();

        UserProfilePatchDTO patch = new UserProfilePatchDTO();
        UserProfileReadDTO read = userProfileService.patchUserProfile(userProfile.getId(), patch);

        Assert.assertNotNull(read.getRatingReviews());
        Assert.assertNotNull(read.getRatingActivity());
        Assert.assertNotNull(read.getFavouriteGenre());

        UserProfile userProfileAfterUpdate = userProfileRepository.findById(read.getId()).get();

        Assert.assertNotNull(userProfileAfterUpdate.getRatingReviews());
        Assert.assertNotNull(userProfileAfterUpdate.getRatingActivity());
        Assert.assertNotNull(userProfileAfterUpdate.getFavouriteGenre());

        Assertions.assertThat(userProfile).isEqualToComparingFieldByField(userProfileAfterUpdate);
    }

    @Test
    public void testPutUserProfile() {

        UserProfile userProfile = createUserProfile();

        UserProfilePutDTO put = new UserProfilePutDTO();
        put.setRatingReviews(5.2);
        put.setRatingActivity(6.7);
        put.setFavouriteGenre(FilmGenre.ROMANCE);

        UserProfileReadDTO read = userProfileService.updateUserProfile(userProfile.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        userProfile = userProfileRepository.findById(read.getId()).get();
        Assertions.assertThat(userProfile).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testDeleteUserProfile() {

        UserProfile userProfile = createUserProfile();

        userProfileService.deleteUserProfile(userProfile.getId());
        Assert.assertFalse(userProfileRepository.existsById(userProfile.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteUserProfileNotFound() {

        userProfileService.deleteUserProfile(UUID.randomUUID());
    }

    private UserProfile createUserProfile() {
        UserProfile userProfile = new UserProfile();
        userProfile.setRatingReviews(5.2);
        userProfile.setRatingActivity(6.7);
        userProfile.setFavouriteGenre(FilmGenre.ROMANCE);
        return userProfileRepository.save(userProfile);
    }
}

