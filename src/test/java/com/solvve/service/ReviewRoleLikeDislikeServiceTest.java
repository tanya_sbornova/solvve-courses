package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.ReviewRoleLikeDislike;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePatchDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePutDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikeReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ReviewRoleLikeDislikeRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.util.UUID;

public class ReviewRoleLikeDislikeServiceTest extends BaseTest {

    @Autowired
    private ReviewRoleLikeDislikeRepository reviewRoleLikeDislikeRepository;

    @Autowired
    private ReviewRoleLikeDislikeService reviewRoleLikeDislikeService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetReviewRoleLikeDislike() {

        ReviewRoleLikeDislike reviewRoleLikeDislike = testObjectsFactory.createReviewRoleLikeDislike();

        ReviewRoleLikeDislikeReadDTO readDTO =
                reviewRoleLikeDislikeService.getReviewRoleLikeDislike(reviewRoleLikeDislike.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(reviewRoleLikeDislike,
                "reviewRoleId", "registeredUserId");
        Assert.assertEquals(readDTO.getReviewRoleId(), reviewRoleLikeDislike.getReviewRole().getId());
        Assert.assertEquals(readDTO.getRegisteredUserId(), reviewRoleLikeDislike.getRegisteredUser().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetReviewRoleLikeDislikeWrongId() {

        reviewRoleLikeDislikeService.getReviewRoleLikeDislike(UUID.randomUUID());
    }

    @Test
    public void testPatchReviewRoleLikeDislike() {

        ReviewRoleLikeDislike reviewRoleLikeDislike = testObjectsFactory.createReviewRoleLikeDislike();

        ReviewRoleLikeDislikePatchDTO patch = new ReviewRoleLikeDislikePatchDTO();
        patch.setIsLike(false);

        ReviewRoleLikeDislikeReadDTO read =
                reviewRoleLikeDislikeService.patchReviewRoleLikeDislike(reviewRoleLikeDislike.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        reviewRoleLikeDislike = reviewRoleLikeDislikeRepository.findById(read.getId()).get();
        Assertions.assertThat(reviewRoleLikeDislike).isEqualToIgnoringGivenFields(read,
                "reviewRole", "registeredUser");
        Assert.assertEquals(read.getReviewRoleId(), reviewRoleLikeDislike.getReviewRole().getId());
        Assert.assertEquals(read.getRegisteredUserId(), reviewRoleLikeDislike.getRegisteredUser().getId());
    }

    @Test
    public void testPatchReviewRoleLikeDislikeEmptyPatch() {

        ReviewRoleLikeDislike reviewRoleLikeDislike = testObjectsFactory.createReviewRoleLikeDislike();

        ReviewRoleLikeDislikePatchDTO patch = new ReviewRoleLikeDislikePatchDTO();
        ReviewRoleLikeDislikeReadDTO read =
                reviewRoleLikeDislikeService.patchReviewRoleLikeDislike(reviewRoleLikeDislike.getId(), patch);

        Assert.assertNotNull(read.getIsLike());

        ReviewRoleLikeDislike reviewRoleLikeDislikeAfterUpdate =
                reviewRoleLikeDislikeRepository.findById(read.getId()).get();

        Assert.assertNotNull(reviewRoleLikeDislikeAfterUpdate.getIsLike());

        Assert.assertNotNull(reviewRoleLikeDislikeAfterUpdate.getIsLike());
        Assertions.assertThat(reviewRoleLikeDislike).isEqualToIgnoringGivenFields(read,
                "reviewRole", "registeredUser");
        Assert.assertEquals(read.getReviewRoleId(), reviewRoleLikeDislike.getReviewRole().getId());
        Assert.assertEquals(read.getRegisteredUserId(), reviewRoleLikeDislike.getRegisteredUser().getId());
    }

    @Test
    public void testPutReviewRoleLikeDislike() {

        ReviewRoleLikeDislike reviewRoleLikeDislike = testObjectsFactory.createReviewRoleLikeDislike();

        ReviewRoleLikeDislikePutDTO put = new ReviewRoleLikeDislikePutDTO();
        put.setIsLike(false);

        ReviewRoleLikeDislikeReadDTO read =
                reviewRoleLikeDislikeService.updateReviewRoleLikeDislike(reviewRoleLikeDislike.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        reviewRoleLikeDislike = reviewRoleLikeDislikeRepository.findById(read.getId()).get();
        Assertions.assertThat(reviewRoleLikeDislike).isEqualToIgnoringGivenFields(read,
                "reviewRole", "registeredUser");
        Assert.assertEquals(read.getReviewRoleId(), reviewRoleLikeDislike.getReviewRole().getId());
        Assert.assertEquals(read.getRegisteredUserId(), reviewRoleLikeDislike.getRegisteredUser().getId());
    }

    @Test
    public void testDeleteReviewRoleLikeDislike() {

        ReviewRoleLikeDislike reviewRoleLikeDislike = testObjectsFactory.createReviewRoleLikeDislike();

        reviewRoleLikeDislikeService.deleteReviewRoleLikeDislike(reviewRoleLikeDislike.getId());
        Assert.assertFalse(reviewRoleLikeDislikeRepository.existsById(reviewRoleLikeDislike.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteReviewRoleLikeDislikeNotFound() {

        reviewRoleLikeDislikeService.deleteReviewRoleLikeDislike(UUID.randomUUID());
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveReviewRoleValidation() {

        ReviewRoleLikeDislike reviewRoleLike = new ReviewRoleLikeDislike();
        reviewRoleLikeDislikeRepository.save(reviewRoleLike);
    }
}


