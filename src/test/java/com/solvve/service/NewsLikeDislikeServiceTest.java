package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.NewsLikeDislike;
import com.solvve.dto.newslikedislike.NewsLikeDislikePatchDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikePutDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikeReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.NewsLikeDislikeRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.util.UUID;

public class NewsLikeDislikeServiceTest extends BaseTest {

    @Autowired
    private NewsLikeDislikeRepository newsLikeDislikeRepository;

    @Autowired
    private NewsLikeDislikeService newsLikeDislikeService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetNewsLikeDislike() {

        NewsLikeDislike newsLikeDislike = testObjectsFactory.createNewsLikeDislike();

        NewsLikeDislikeReadDTO readDTO =
                newsLikeDislikeService.getNewsLikeDislike(newsLikeDislike.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(newsLikeDislike, "newsId", "registeredUserId");
        Assert.assertEquals(readDTO.getRegisteredUserId(), newsLikeDislike.getRegisteredUser().getId());
        Assert.assertEquals(readDTO.getNewsId(), newsLikeDislike.getNews().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetNewsLikeDislikeWrongId() {

        newsLikeDislikeService.getNewsLikeDislike(UUID.randomUUID());
    }

    @Test
    public void testPatchNewsLikeDislike() {

        NewsLikeDislike newsLikeDislike = testObjectsFactory.createNewsLikeDislike();

        NewsLikeDislikePatchDTO patch = new NewsLikeDislikePatchDTO();
        patch.setIsLike(false);

        NewsLikeDislikeReadDTO read = newsLikeDislikeService.
                patchNewsLikeDislike(newsLikeDislike.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        newsLikeDislike = newsLikeDislikeRepository.findById(read.getId()).get();
        Assertions.assertThat(newsLikeDislike).isEqualToIgnoringGivenFields(read, "news", "registeredUser");
        Assert.assertEquals(read.getRegisteredUserId(), newsLikeDislike.getRegisteredUser().getId());
        Assert.assertEquals(read.getNewsId(), newsLikeDislike.getNews().getId());
    }

    @Test
    public void testPatchNewsLikeDislikeEmptyPatch() {

        NewsLikeDislike newsLikeDislike = testObjectsFactory.createNewsLikeDislike();

        NewsLikeDislikePatchDTO patch = new NewsLikeDislikePatchDTO();
        NewsLikeDislikeReadDTO read = newsLikeDislikeService.
                patchNewsLikeDislike(newsLikeDislike.getId(), patch);

        Assert.assertNotNull(read.getIsLike());

        NewsLikeDislike newsLikeDislikeAfterUpdate =
                newsLikeDislikeRepository.findById(read.getId()).get();

        Assert.assertNotNull(newsLikeDislikeAfterUpdate.getIsLike());

        Assertions.assertThat(newsLikeDislike).isEqualToIgnoringGivenFields(newsLikeDislikeAfterUpdate,
                "news", "registeredUser");
        Assertions.assertThat(newsLikeDislike.getNews()).isEqualToIgnoringGivenFields(newsLikeDislike.getNews(),
                "film", "role", "contentManager");
        Assertions.assertThat(newsLikeDislike.getRegisteredUser()).
                isEqualToIgnoringGivenFields(newsLikeDislikeAfterUpdate.getRegisteredUser(),
                        "reviewFilmLikeDislikes", "reviewRoleLikeDislikes", "newsLikeDislikes", "problemReviews",
                        "misprintInfoFilms", "misprintInfoRoles", "misprintInfoNews");
    }

    @Test
    public void testUpdateNewsLikeDislike() {

        NewsLikeDislike newsLikeDislike = testObjectsFactory.createNewsLikeDislike();

        NewsLikeDislikePutDTO put = new NewsLikeDislikePutDTO();
        put.setIsLike(false);

        NewsLikeDislikeReadDTO read = newsLikeDislikeService.
                updateNewsLikeDislike(newsLikeDislike.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        newsLikeDislike = newsLikeDislikeRepository.findById(read.getId()).get();
        Assertions.assertThat(newsLikeDislike).isEqualToIgnoringGivenFields(read, "news", "registeredUser");
        Assert.assertEquals(read.getRegisteredUserId(), newsLikeDislike.getRegisteredUser().getId());
        Assert.assertEquals(read.getNewsId(), newsLikeDislike.getNews().getId());
    }

    @Test
    public void testDeleteNewsLikeDislike() {

        NewsLikeDislike newsLikeDislike = testObjectsFactory.createNewsLikeDislike();

        newsLikeDislikeService.deleteNewsLikeDislike(newsLikeDislike.getId());
        Assert.assertFalse(newsLikeDislikeRepository.existsById(newsLikeDislike.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteNewsLikeDislikeNotFound() {

        newsLikeDislikeService.deleteNewsLikeDislike(UUID.randomUUID());
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveNewsLikeDislikeValidation() {

        NewsLikeDislike news = new NewsLikeDislike();
        newsLikeDislikeRepository.save(news);
    }
}

