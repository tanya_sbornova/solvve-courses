package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.Role;
import com.solvve.repository.RoleRepository;
import com.solvve.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

public class RoleFilmLinkServiceTest extends BaseTest {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testLink() {

        Film film = testObjectsFactory.createFilm();

        Role role = testObjectsFactory.createRole();
        role.setFilm(film);
        roleRepository.save(role);

        Role savedRole = roleRepository.findById(role.getId()).get();

        Assert.assertNotNull(savedRole.getFilm());
    }

    @Test
    public void testCreateRoleAndLink() {

        Film film = testObjectsFactory.createFilm();

        Role role = testObjectsFactory.createRole();
        role.setFilm(film);
        role = roleRepository.save(role);

        Role savedRole = roleRepository.findById(role.getId()).get();

        Assert.assertNotNull(savedRole.getFilm());
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }
}
