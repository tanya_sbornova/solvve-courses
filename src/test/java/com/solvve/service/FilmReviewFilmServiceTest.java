package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.RegisteredUser;
import com.solvve.domain.ReviewFilm;
import com.solvve.domain.ReviewStatus;
import com.solvve.dto.reviewfilm.ReviewFilmCreateDTO;
import com.solvve.dto.reviewfilm.ReviewFilmPatchDTO;
import com.solvve.dto.reviewfilm.ReviewFilmPutDTO;
import com.solvve.dto.reviewfilm.ReviewFilmReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.ReviewFilmRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class FilmReviewFilmServiceTest extends BaseTest {

    @Autowired
    private ReviewFilmRepository reviewFilmRepository;

    @Autowired
    private FilmReviewFilmService filmReviewFilmService;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;
    
    @Test
    public void getFilmReviewsFilm() {

        Film film = testObjectsFactory.createFilm();
        ReviewFilm reviewFilm1 = testObjectsFactory.createReviewFilm(film);

        List<UUID> reviewFilmsId = transactionTemplate.execute(status -> {
            Film film1 = filmRepository.findById(film.getId()).get();
            return film1.getReviewFilms().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });

        List<ReviewFilmReadDTO> reviewFilmsReadDTO = filmReviewFilmService.getFilmReviewsFilm(film.getId());

        Assertions.assertThat(reviewFilmsReadDTO).extracting("id").
                containsExactlyInAnyOrder(reviewFilmsId.toArray());
    }

    @Test
    public void getFilmReviewsFilmOfCurrentRegisteredUser() {

        Film film = testObjectsFactory.createFilm();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        RegisteredUser registeredUser2 = testObjectsFactory.createRegisteredUser();
        testObjectsFactory.createReviewFilm(ReviewStatus.NEED_TO_MODERATE, false,
                registeredUser, film);
        testObjectsFactory.createReviewFilm(ReviewStatus.FIXED, false,
                registeredUser, film);
        testObjectsFactory.createReviewFilm(ReviewStatus.MODERATED, true,
                registeredUser2, film);

        List<UUID> reviewFilmsId = transactionTemplate.execute(status -> {
            Film film1 = filmRepository.findById(film.getId()).get();
            return film1.getReviewFilms().stream()
                    .filter(rv -> rv.getRegisteredUser().getId().equals(registeredUser.getId()))
                    .map(x -> x.getId()).collect(Collectors.toList());
        });

        List<ReviewFilmReadDTO> reviewFilmsReadDTO = filmReviewFilmService
                .getFilmReviewsFilmOfCurrentRegisteredUser(registeredUser.getId(), film.getId());

        Assertions.assertThat(reviewFilmsReadDTO).extracting("id").
                containsExactlyInAnyOrder(reviewFilmsId.toArray());
    }

    @Test
    public void getFilmReviewFilm() {

        Film film = testObjectsFactory.createFilm();
        ReviewFilm reviewFilm1 = testObjectsFactory.createReviewFilm(film);

        ReviewFilm reviewFilm = transactionTemplate.execute(status -> {
            Film filmIn = filmRepository.findById(film.getId()).get();
            return   filmIn.getReviewFilms().get(0);
        });
        ReviewFilmReadDTO reviewFilmReadDTO = filmReviewFilmService.
                    getFilmReviewFilm(film.getId(), reviewFilm.getId());
        Assertions.assertThat(reviewFilmReadDTO).isEqualToIgnoringGivenFields(reviewFilm,
                    "filmId", "registeredUserId");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetFilmReviewFilmWrongId() {
        Film film = testObjectsFactory.createFilm();
        filmReviewFilmService.getFilmReviewFilm(film.getId(), UUID.randomUUID());
    }

    @Test
    public void testCreateFilmReviewFilm() {

        ReviewFilmCreateDTO create = testObjectsFactory.createReviewFilmCreateDTO();
        Film film = testObjectsFactory.createFilm();

        ReviewFilmReadDTO read = filmReviewFilmService.createFilmReviewFilm(film.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ReviewFilm reviewFilm = reviewFilmRepository.findById(read.getId()).get();
        Assertions.assertThat(reviewFilm).isEqualToIgnoringGivenFields(read, "film", "registeredUser");
        Assert.assertEquals(read.getFilmId(), reviewFilm.getFilm().getId());
        Assert.assertEquals(read.getRegisteredUserId(), reviewFilm.getRegisteredUser().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateFilmReviewFilmWithWrongUser() {

        ReviewFilmCreateDTO create = testObjectsFactory.createReviewFilmWithWrongUser();
        Film film = testObjectsFactory.createFilm();

        ReviewFilmReadDTO read = filmReviewFilmService.createFilmReviewFilm(film.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ReviewFilm reviewFilm = reviewFilmRepository.findById(read.getId()).get();
        Assertions.assertThat(reviewFilm).isEqualToIgnoringGivenFields(read, "film", "registeredUser");
    }

    @Test
    public void testPatchFilmReviewFilm() {

        Film film = testObjectsFactory.createFilm();
        ReviewFilm reviewFilm1 = testObjectsFactory.createReviewFilm(film);
        ReviewFilmPatchDTO patch = new ReviewFilmPatchDTO();
        patch.setText("The best film");
        patch.setStatus(ReviewStatus.MODERATED);

        ReviewFilmReadDTO read = filmReviewFilmService.patchFilmReviewFilm(film.getId(),
                reviewFilm1.getId(), patch);
        inTransaction(()-> {
            Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
            ReviewFilm reviewFilm = reviewFilmRepository.findById(reviewFilm1.getId()).get();
            Assertions.assertThat(reviewFilm).isEqualToIgnoringGivenFields(read,
                    "registeredUser", "film");
            Assert.assertEquals(read.getFilmId(), reviewFilm.getFilm().getId());
            Assert.assertEquals(read.getRegisteredUserId(), reviewFilm.getRegisteredUser().getId());
        });
    }

    @Test
    public void testUpdateFilmReviewFilm() {

        Film film = testObjectsFactory.createFilm();
        ReviewFilm reviewFilm1 = testObjectsFactory.createReviewFilm(film);
        ReviewFilmPutDTO put = new ReviewFilmPutDTO();
        put.setText("The best");
        put.setStatus(ReviewStatus.FIXED);

        ReviewFilm reviewFilm2 = transactionTemplate.execute(status -> {
            Film filmIn = filmRepository.findById(film.getId()).get();
            return   filmIn.getReviewFilms().get(0);
        });

        ReviewFilmReadDTO read = filmReviewFilmService.updateFilmReviewFilm(film.getId(),
                reviewFilm2.getId(), put);

        inTransaction(()-> {
            Assertions.assertThat(put).isEqualToComparingFieldByField(read);
            ReviewFilm reviewFilm = reviewFilmRepository.findById(read.getId()).get();
            Assertions.assertThat(reviewFilm).isEqualToIgnoringGivenFields(read,
                    "registeredUser", "film", "createdAt", "updatedAt");
            Assert.assertEquals(read.getFilmId(), reviewFilm.getFilm().getId());
            Assert.assertEquals(read.getRegisteredUserId(), reviewFilm.getRegisteredUser().getId());
        });
    }

    @Test
    public void testDeleteFilmReviewFilm() {

        Film film = testObjectsFactory.createFilm();
        UUID reviewFilmUUID = transactionTemplate.execute(status-> {
            ReviewFilm reviewFilm = filmRepository.findById(film.getId()).get().getReviewFilms().get(0);
            return reviewFilm.getId();
        });
        filmReviewFilmService.deleteFilmReviewFilm(film.getId(), reviewFilmUUID);
        Assert.assertFalse(reviewFilmRepository.existsById(reviewFilmUUID));
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status->{
            runnable.run();
        });
    }
}

