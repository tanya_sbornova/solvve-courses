package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.exception.LinkDuplicatedException;
import com.solvve.util.TestObjectsFactory;
import com.solvve.domain.*;
import com.solvve.dto.role.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ActorRepository;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.RoleRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.*;
import java.util.stream.Collectors;

public class RoleServiceTest extends BaseTest {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private RoleService roleService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testGetRole() {

        Role role = testObjectsFactory.createRole();
        RoleReadDTO readDTO = roleService.getRole(role.getId());

        inTransaction(()-> {
            Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(role,
                        "filmId", "actorId");
            Film film = filmRepository.findById(readDTO.getFilmId()).get();
            Assertions.assertThat(film).isEqualToIgnoringGivenFields(role.getFilm(), "crewMembers",
                    "ratingFilms", "reviewFilms", "genres", "companies");
            Actor actor = actorRepository.findById(readDTO.getActorId()).get();
            Assertions.assertThat(actor).isEqualToIgnoringGivenFields(role.getActor(), "roles", "person");
        });
    }

    @Test
    public void testGetRoleExtended() {

        Role role = testObjectsFactory.createRole();
        RoleReadExtendedDTO readDTO = roleService.getRoleExtended(role.getId());

        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(role,
                    "actor", "film");
        Assertions.assertThat(readDTO.getFilm()).isEqualToIgnoringGivenFields(role.getFilm(), "companyId");
        Assertions.assertThat(readDTO.getActor()).isEqualToIgnoringGivenFields(role.getActor(),
                    "rolesId", "personId");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRoleWrongId() {
        roleService.getRole(UUID.randomUUID());
    }

    @Test
    public void getActorRoles() {

        Actor actor = testObjectsFactory.createActor(testObjectsFactory.createPerson());

        Set<UUID> rolesId = transactionTemplate.execute(status -> {
            Actor actor1 = actorRepository.findById(actor.getId()).get();
            return actor1.getRoles().stream().
                    map(x -> x.getId()).collect(Collectors.toSet());
        });

        List<RoleReadDTO> rolesReadDTO = roleService.getActorRoles(actor.getId());

        Assertions.assertThat(rolesReadDTO).extracting("id").
                containsExactlyInAnyOrder(rolesId.toArray());
    }

    @Test
    public void testCreateRole() {

        Person person = testObjectsFactory.createPerson();
        RoleCreateDTO create = new RoleCreateDTO();
        create.setText("Neo");
        create.setActorId(testObjectsFactory.createActor(person).getId());
        Film film = testObjectsFactory.createFilm();
        create.setFilmId(film.getId());

        RoleReadDTO read = roleService.createRole(create);

        inTransaction(()-> {

            Assertions.assertThat(create).isEqualToComparingFieldByField(read);
            Assert.assertNotNull(read.getId());

            Role role = roleRepository.findById(read.getId()).get();
            Assertions.assertThat(read).isEqualToIgnoringGivenFields(role,
                        "filmId", "actorId");
            Film film1 = filmRepository.findById(read.getFilmId()).get();
            Assertions.assertThat(film1).isEqualToComparingFieldByField(role.getFilm());
            Actor actor = actorRepository.findById(read.getActorId()).get();
            Assertions.assertThat(actor).isEqualToComparingFieldByField(role.getActor());
        });
    }

    @Test
    public void testAddRoleToActor() {

        Actor actor = testObjectsFactory.createActor(testObjectsFactory.createPerson());
        Role role = testObjectsFactory.createRole();
        UUID roleId = role.getId();

        List<RoleReadDTO> rolesReadDTO = roleService.addRoleToActor(actor.getId(), roleId);

        RoleReadDTO expectedRead = new RoleReadDTO();
        expectedRead.setId(roleId);
        expectedRead.setText(role.getText());
        expectedRead.setActorId(actor.getId());
        expectedRead.setFilmId(role.getFilm().getId());
        expectedRead.setCreatedAt(role.getCreatedAt());
        expectedRead.setUpdatedAt(role.getUpdatedAt());
        Assertions.assertThat(rolesReadDTO).containsExactlyInAnyOrder(expectedRead);

        inTransaction(() -> {
            Actor actorAfterSave = actorRepository.findById(actor.getId()).get();
            Assertions.assertThat(actorAfterSave.getRoles()).extracting(Role::getId)
                    .containsExactlyInAnyOrder(roleId);
        });
    }

    @Test
    public void testDuplicatedRole() {

        Actor actor = testObjectsFactory.createActor(testObjectsFactory.createPerson());
        Role role = testObjectsFactory.createRole();
        UUID roleId = role.getId();

        roleService.addRoleToActor(actor.getId(), roleId);
        Assertions.assertThatThrownBy(() -> {
            roleService.addRoleToActor(actor.getId(), roleId);
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test
    public void testPatchRole() {

        Role role = testObjectsFactory.createRole();

        RolePatchDTO patch = testObjectsFactory.createRolePatchDTO();

        RoleReadDTO read = roleService.patchRole(role.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        role = roleRepository.findById(read.getId()).get();
        Assertions.assertThat(role).isEqualToIgnoringGivenFields(read,
                "actor", "film", "ratingRoles", "reviewRoles");
        Assert.assertEquals(read.getFilmId(), role.getFilm().getId());
        Assert.assertEquals(read.getActorId(), role.getActor().getId());
    }

    @Test
    public void testPutRole() {

        Role role = testObjectsFactory.createRole();

        RolePutDTO put = testObjectsFactory.createRolePutDTO();

        RoleReadDTO read = roleService.updateRole(role.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        role = roleRepository.findById(read.getId()).get();
        Assertions.assertThat(role).isEqualToIgnoringGivenFields(read,
                "actor", "film", "ratingRoles", "reviewRoles");
        Assert.assertEquals(read.getFilmId(), role.getFilm().getId());
        Assert.assertEquals(read.getActorId(), role.getActor().getId());
    }

    @Test
    public void testPatchRoleEmptyPatch() {

        Role role = testObjectsFactory.createRole();
        RolePatchDTO patch = new RolePatchDTO();
        RoleReadDTO read = roleService.patchRole(role.getId(), patch);

        inTransaction(()-> {
            Assert.assertNotNull(read.getText());
            Role roleAfterUpdate = roleRepository.findById(read.getId()).get();
            Assert.assertNotNull(roleAfterUpdate.getText());
            Assertions.assertThat(role).isEqualToIgnoringGivenFields(roleAfterUpdate,
                    "ratingRoles", "reviewRoles", "actor", "film");
        });
    }

    @Test
    public void testDeleteRole() {

        Role role = testObjectsFactory.createRole();

        roleService.deleteRole(role.getId());
        Assert.assertFalse(roleRepository.existsById(role.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteRoleNotFound() {

        roleService.deleteRole(UUID.randomUUID());
    }

    @Test
    public void testRemoveRoleFromActor() {

        Actor actor = testObjectsFactory.createActor(testObjectsFactory.createPerson());
        Role role = testObjectsFactory.createRole();
        UUID roleId = role.getId();

        roleService.addRoleToActor(actor.getId(), roleId);

        List<RoleReadDTO> remainingRoles = roleService.removeRoleFromActor(actor.getId(), roleId);
        Assert.assertTrue(remainingRoles.isEmpty());

        inTransaction(() -> {
            Actor actorAfterRemove = actorRepository.findById(actor.getId()).get();
            Assertions.assertThat(actorAfterRemove.getRoles().isEmpty());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedGenre() {

        Actor actor = testObjectsFactory.createActor(testObjectsFactory.createPerson());
        Role role = testObjectsFactory.createRole();
        UUID roleId = role.getId();

        roleService.removeRoleFromActor(actor.getId(), roleId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedGenre() {

        Actor actor = testObjectsFactory.createActor(testObjectsFactory.createPerson());
        roleService.removeRoleFromActor(actor.getId(), UUID.randomUUID());
    }

    @Test
    public void testGetRolesWithEmptyFilterWithPagingAndSorting() throws InterruptedException {
        Role role = testObjectsFactory.createRole();
        Thread.sleep(1000);
        Role role2 = testObjectsFactory.createRole();

        RoleFilter filter = new RoleFilter();
        PageRequest pageRequest = PageRequest.of(0, 2, Sort.by(Sort.Direction.DESC, "createdAt"));

        Assertions.assertThat(roleService.getRoles(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(role2.getId(), role.getId()));
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveRoleValidation() {

        Role role = new Role();
        roleRepository.save(role);
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status->{
            runnable.run();
        });
    }
}
