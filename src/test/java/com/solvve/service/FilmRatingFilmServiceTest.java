package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.RatingFilm;
import com.solvve.dto.ratingfilm.RatingFilmCreateDTO;
import com.solvve.dto.ratingfilm.RatingFilmPatchDTO;
import com.solvve.dto.ratingfilm.RatingFilmPutDTO;
import com.solvve.dto.ratingfilm.RatingFilmReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.RatingFilmRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class FilmRatingFilmServiceTest extends BaseTest {

    @Autowired
    private RatingFilmRepository ratingFilmRepository;

    @Autowired
    private FilmRatingFilmService filmRatingFilmService;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void getFilmRatingFilms() {

        Film film = testObjectsFactory.createFilm();
        RatingFilm ratingFilm = testObjectsFactory.createRatingFilm(film);

        List<UUID> ratingFilmsId = transactionTemplate.execute(status -> {
            Film film1 = filmRepository.findById(film.getId()).get();
            return film1.getRatingFilms().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });

        List<RatingFilmReadDTO> ratingFilmsReadDTO = filmRatingFilmService.
                getFilmRatingFilms(ratingFilm.getFilm().getId());
        Assertions.assertThat(ratingFilmsReadDTO).extracting("id").
                containsExactlyInAnyOrder(ratingFilmsId.toArray());
    }

    @Test
    public void getFilmRatingFilm() {

        Film film = testObjectsFactory.createFilm();
        RatingFilm ratingFilm = testObjectsFactory.createRatingFilm(film);

        RatingFilm ratingFilm2 = transactionTemplate.execute(status -> {
            Film filmIn = filmRepository.findById(film.getId()).get();
            return   filmIn.getRatingFilms().get(0);
        });

        RatingFilmReadDTO ratingFilmReadDTO = filmRatingFilmService.
                getFilmRatingFilm(film.getId(), ratingFilm2.getId());
        Assertions.assertThat(ratingFilmReadDTO).isEqualToIgnoringGivenFields(ratingFilm2,
                "filmId", "registeredUserId");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetFilmRatingFilmWrongId() {
        Film film = testObjectsFactory.createFilm();
        filmRatingFilmService.getFilmRatingFilm(film.getId(), UUID.randomUUID());
    }

    @Test
    public void testCreateFilmRatingFilm() {

        RatingFilmCreateDTO create = testObjectsFactory.createRatingFilmCreateDTO();
        Film film = testObjectsFactory.createFilm();

        RatingFilmReadDTO read = filmRatingFilmService.createFilmRatingFilm(film.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        RatingFilm ratingFilm = ratingFilmRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(ratingFilm, "filmId", "registeredUserId");
        Assertions.assertThat(read.getFilmId()).isEqualToComparingFieldByField(ratingFilm.getFilm().getId());
        Assert.assertEquals(read.getFilmId(), ratingFilm.getFilm().getId());
        Assert.assertEquals(read.getRegisteredUserId(), ratingFilm.getRegisteredUser().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateFilmRatingFilmWithWrongUser() {

        RatingFilmCreateDTO create = testObjectsFactory.createRatingFilmWithWrongUser();
        Film film = testObjectsFactory.createFilm();
        RatingFilmReadDTO read = filmRatingFilmService.createFilmRatingFilm(film.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        RatingFilm reviewFilm = ratingFilmRepository.findById(read.getId()).get();
        Assertions.assertThat(reviewFilm).isEqualToIgnoringGivenFields(read, "film", "registeredUser");
    }

    @Test
    public void testPatchFilmRatingFilm() {

        Film film1 = testObjectsFactory.createFilm();
        RatingFilm ratingFilm1 = testObjectsFactory.createRatingFilm(film1);
        RatingFilmPatchDTO patch = new RatingFilmPatchDTO();
        patch.setRating(7.6);

        RatingFilmReadDTO read = filmRatingFilmService.patchFilmRatingFilm(film1.getId(),
                ratingFilm1.getId(), patch);

        inTransaction(()-> {
            Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
            RatingFilm ratingFilm = ratingFilmRepository.findById(ratingFilm1.getId()).get();
            Assertions.assertThat(ratingFilm).isEqualToIgnoringGivenFields(read,
                    "registeredUser", "film", "createdAt", "updatedAt");
            Assert.assertEquals(read.getFilmId(), ratingFilm.getFilm().getId());
            Assert.assertEquals(read.getRegisteredUserId(), ratingFilm.getRegisteredUser().getId());
        });
    }

    @Test
    public void testUpdateFilmRatingFilm() {

        Film film1 = testObjectsFactory.createFilm();
        RatingFilm ratingFilm1 = testObjectsFactory.createRatingFilm(film1);
        RatingFilmPutDTO put = new RatingFilmPutDTO();
        put.setRating(7.6);

        RatingFilm ratingFilm2 =  transactionTemplate.execute(status -> {
            Film film = filmRepository.findById(film1.getId()).get();
            return filmRepository.findById(film.getId()).get().getRatingFilms().get(0);
        });

        RatingFilmReadDTO read = filmRatingFilmService.updateFilmRatingFilm(film1.getId(),
                ratingFilm2.getId(), put);

        inTransaction(()-> {
            Assertions.assertThat(put).isEqualToComparingFieldByField(read);
            RatingFilm ratingFilm = ratingFilmRepository.findById(read.getId()).get();
            Assertions.assertThat(ratingFilm).isEqualToIgnoringGivenFields(read,
                    "registeredUser", "film", "createdAt", "updatedAt");
            Assert.assertEquals(read.getFilmId(), ratingFilm.getFilm().getId());
            Assert.assertEquals(read.getRegisteredUserId(), ratingFilm.getRegisteredUser().getId());
        });
    }

    @Test
    public void testDeleteFilmRatingFilm() {

        Film film = testObjectsFactory.createFilm();
        UUID ratingFilmUUID = transactionTemplate.execute(status-> {
            RatingFilm ratingFilm = filmRepository.findById(film.getId()).get().getRatingFilms().get(0);
            return ratingFilm.getId();
        });
        filmRatingFilmService.deleteFilmRatingFilm(film.getId(), ratingFilmUUID);
        Assert.assertFalse(ratingFilmRepository.existsById(ratingFilmUUID));
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status->{
            runnable.run();
        });
    }
}
