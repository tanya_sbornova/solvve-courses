package com.solvve.service;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;
import com.github.database.rider.spring.api.DBRider;
import com.solvve.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

@DBRider
public class ActorServiceDbRiderTest extends BaseTest {

    @Autowired
    private ActorService actorService;

    @Test
    @DataSet(value = "/datasets/testUpdateAverageRatingActorOfRole.xml")
    @ExpectedDataSet(value = "/datasets/testUpdateAverageRatingActorOfRole_result.xml")
    public void testUpdateAverageRatingActorOfRole() {

        UUID actorId = UUID.fromString("c81b7c44-492b-11ea-b77f-2e728ce88125");
        actorService.updateAverageRatingActorOfRole(actorId);
    }

}
