package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.RegisteredUser;
import com.solvve.dto.registereduser.RegisteredUserCreateDTO;
import com.solvve.dto.registereduser.RegisteredUserPatchDTO;
import com.solvve.dto.registereduser.RegisteredUserPutDTO;
import com.solvve.dto.registereduser.RegisteredUserReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.RegisteredUserRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.text.ParseException;
import java.util.UUID;

public class RegisteredUserServiceTest extends BaseTest {

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private RegisteredUserService registeredUserService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetRegisteredUser() {

        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        registeredUser = registeredUserRepository.save(registeredUser);

        RegisteredUserReadDTO readDTO = registeredUserService.getRegisteredUser(registeredUser.getId());

        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(registeredUser);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRegisteredUserWrongId() {
        registeredUserService.getRegisteredUser(UUID.randomUUID());
    }

    @Test
    public void testCreateRegisteredUser() {

        RegisteredUserCreateDTO create = testObjectsFactory.generateObject(RegisteredUserCreateDTO.class);

        RegisteredUserReadDTO read = registeredUserService.createRegisteredUser(create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        RegisteredUser registeredUser = registeredUserRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(registeredUser);
    }

    @Test
    public void testPatchRegisteredUser() {

        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        RegisteredUserPatchDTO patch = testObjectsFactory.generateObject(RegisteredUserPatchDTO.class);

        RegisteredUserReadDTO read = registeredUserService.patchRegisteredUser(registeredUser.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        registeredUser = registeredUserRepository.findById(read.getId()).get();
        Assertions.assertThat(registeredUser).isEqualToIgnoringGivenFields(read,
                "reviewFilmLikeDislikes", "reviewRoleLikeDislikes", "newsLikeDislikes", "problemReviews",
                "misprintInfoFilms", "misprintInfoRoles", "misprintInfoNews");
    }

    @Test
    public void testPatchRegisteredUserEmptyPatch() throws ParseException {

        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        RegisteredUserPatchDTO patch = new RegisteredUserPatchDTO();
        RegisteredUserReadDTO read = registeredUserService.patchRegisteredUser(registeredUser.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        RegisteredUser registeredUserAfterUpdate = registeredUserRepository.findById(read.getId()).get();

        Assertions.assertThat(registeredUserAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(registeredUser).isEqualToIgnoringGivenFields(registeredUserAfterUpdate,
                "reviewFilmLikeDislikes", "reviewRoleLikeDislikes", "newsLikeDislikes", "problemReviews",
                "misprintInfoFilms", "misprintInfoRoles", "misprintInfoNews");
    }

    @Test
    public void testPutRegisteredUser() {

        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        RegisteredUserPutDTO put = testObjectsFactory.generateObject(RegisteredUserPutDTO.class);

        RegisteredUserReadDTO read = registeredUserService.updateRegisteredUser(registeredUser.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        registeredUser = registeredUserRepository.findById(read.getId()).get();
        Assertions.assertThat(registeredUser).isEqualToIgnoringGivenFields(read,
                "reviewFilmLikeDislikes", "reviewRoleLikeDislikes", "newsLikeDislikes", "problemReviews",
                "misprintInfoFilms", "misprintInfoRoles", "misprintInfoNews");
    }

    @Test
    public void testDeleteRegisteredUser() {

        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        registeredUserService.deleteRegisteredUser(registeredUser.getId());
        Assert.assertFalse(registeredUserRepository.existsById(registeredUser.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteRegisteredUserNotFound() {

        registeredUserService.deleteRegisteredUser(UUID.randomUUID());
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveRegisteredUserValidation() {

        RegisteredUser registeredUser = new RegisteredUser();
        registeredUserRepository.save(registeredUser);
    }
}


