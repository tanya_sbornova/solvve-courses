package com.solvve.service;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;
import com.github.database.rider.spring.api.DBRider;
import com.solvve.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

@DBRider
public class FilmServiceDbRiderTest extends BaseTest {

    @Autowired
    private FilmService filmService;

    @Test
    @DataSet(value = "/datasets/testUpdateAverageRatingOfFilm.xml")
    @ExpectedDataSet(value = "/datasets/testUpdateAverageRatingOfFilm_result.xml")
    public void testUpdateAverageRatingOfFilm() {

        UUID filmId = UUID.fromString("c81b8194-492b-11ea-b77f-2e728ce88125");
        filmService.updateAverageRatingOfFilm(filmId);
    }
}
