package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.Film;
import com.solvve.dto.company.CompanyReadDTO;
import com.solvve.repository.FilmRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class FilmCompanyServiceTest extends BaseTest {

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private FilmCompanyService filmCompanyService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private FilmRepository filmRepository;

    @Test
    public void getFilmCompanies() {

        Film film = testObjectsFactory.createFlatFilm();

        List<UUID> companiesId = transactionTemplate.execute(status -> {
            Film film1 = filmRepository.findById(film.getId()).get();
            return film1.getCompanies().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });

        List<CompanyReadDTO> companiesReadDTO = filmCompanyService.getFilmCompanies(film.getId());
        Assertions.assertThat(companiesReadDTO).extracting("id").
                containsExactlyInAnyOrder(companiesId.toArray());
    }

    @Test
    public void getFilmCompany()  {

        Film film = testObjectsFactory.createFilm();

        CompanyReadDTO companyReadDTO = filmCompanyService.
                getFilmCompany(film.getId(), film.getCompanies().stream().findFirst().get().getId());
        Assertions.assertThat(companyReadDTO).isEqualToComparingFieldByField((film.getCompanies()).
                        stream().findFirst().get());
    }
}
