package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.MisprintInfoFilm;
import com.solvve.domain.RegisteredUser;
import com.solvve.dto.misprintinfofilm.MisprintInfoFilmCreateDTO;
import com.solvve.dto.misprintinfofilm.MisprintInfoFilmReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.MisprintInfoFilmRepository;
import com.solvve.repository.RegisteredUserRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class RegisteredUserMisprintInfoFilmServiceTest extends BaseTest {

    @Autowired
    private MisprintInfoFilmRepository misprintInfoFilmRepository;

    @Autowired
    private RegisteredUserMisprintInfoFilmService registeredUserMisprintInfoFilmService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void getRegisteredUserMisprintInfoFilms() {

        MisprintInfoFilm misprintInfoFilm1 = testObjectsFactory.createMisprintInfoFilm();
        RegisteredUser registeredUser = misprintInfoFilm1.getRegisteredUser();

        List<UUID> misprintInfoFilmsId = transactionTemplate.execute(status -> {
            RegisteredUser registeredUser1 = registeredUserRepository.findById(registeredUser.getId()).get();
            return registeredUser1.getMisprintInfoFilms().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });

        List<MisprintInfoFilmReadDTO> misprintInfoFilmsReadDTO = registeredUserMisprintInfoFilmService.
                    getRegisteredUserMisprintInfoFilms(registeredUser.getId());

        Assertions.assertThat(misprintInfoFilmsReadDTO).extracting("id").
                containsExactlyInAnyOrder(misprintInfoFilmsId.toArray());
    }

    @Test
    public void getRegisteredUserMisprintInfoFilm() {

        MisprintInfoFilm misprintinfofilm = testObjectsFactory.createMisprintInfoFilm();
        RegisteredUser registeredUser = misprintinfofilm.getRegisteredUser();

        List<UUID> misprintInfoFilmsId = transactionTemplate.execute(status -> {
            RegisteredUser registeredUser1 = registeredUserRepository.findById(registeredUser.getId()).get();
            return registeredUser1.getMisprintInfoFilms().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });

        MisprintInfoFilmReadDTO misprintInfoFilmReadDTO = registeredUserMisprintInfoFilmService.
                    getRegisteredUserMisprintInfoFilm(registeredUser.getId(),
                            misprintInfoFilmsId.get(0));

        MisprintInfoFilm misprintInfoFilm = misprintInfoFilmRepository.findById(misprintInfoFilmsId.get(0)).get();
        Assertions.assertThat(misprintInfoFilmReadDTO).isEqualToIgnoringGivenFields(misprintInfoFilm,
                    "contentId", "registeredUserId", "contentManagerId");
        Assert.assertEquals(misprintinfofilm.getContent().getId(), misprintInfoFilmReadDTO.getContentId());
        Assert.assertEquals(misprintinfofilm.getRegisteredUser().getId(),
                misprintInfoFilmReadDTO.getRegisteredUserId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRegisteredUserNewsWrongId() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        registeredUserMisprintInfoFilmService.
                getRegisteredUserMisprintInfoFilm(registeredUser.getId(), UUID.randomUUID());
    }

    @Test
    public void testCreateRegisteredUserMisprintInfoFilm() {

        MisprintInfoFilmCreateDTO create = testObjectsFactory.createMisprintInfoFilmCreateDTO();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        MisprintInfoFilmReadDTO read = registeredUserMisprintInfoFilmService.
                createRegisteredUserMisprintInfoFilm(registeredUser.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        MisprintInfoFilm misprintinfofilm = misprintInfoFilmRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(misprintinfofilm,
                "contentId", "registeredUserId", "contentManagerId");
        Assert.assertEquals(misprintinfofilm.getContent().getId(), read.getContentId());
        Assert.assertEquals(misprintinfofilm.getRegisteredUser().getId(), read.getRegisteredUserId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateRegisteredUserMisprintInfoFilmWithWrongFilm() {

        MisprintInfoFilmCreateDTO create = testObjectsFactory.createMisprintInfoFilmWithWrongFilm();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        MisprintInfoFilmReadDTO read = registeredUserMisprintInfoFilmService.
                createRegisteredUserMisprintInfoFilm(registeredUser.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        MisprintInfoFilm misprintInfoFilm = misprintInfoFilmRepository.findById(read.getId()).get();
        Assertions.assertThat(misprintInfoFilm).isEqualToIgnoringGivenFields(read, "registeredUser");
    }

    @Test
    public void testDeleteRegisteredUserFilm() {

        MisprintInfoFilm misprintinfofilm = testObjectsFactory.createMisprintInfoFilm();
        UUID misprintinfofilmUUID = misprintinfofilm.getId();
        RegisteredUser registeredUser = misprintinfofilm.getRegisteredUser();
/*        UUID misprintinfofilmUUID = transactionTemplate.execute(status-> {
            MisprintInfoFilm misprintinfofilm = registeredUserRepository.findById(registeredUser.getId()).get().
                    getMisprintInfoFilms().get(0);
            return misprintinfofilm.getId();
        });*/
        registeredUserMisprintInfoFilmService.deleteRegisteredUserMisprintInfoFilm(registeredUser.getId(),
                misprintinfofilmUUID);
        Assert.assertFalse(misprintInfoFilmRepository.existsById(misprintinfofilmUUID));
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status->{
            runnable.run();
        });
    }
}
