package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.Moderator;
import com.solvve.domain.UserStatus;
import com.solvve.dto.moderator.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ModeratorRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.TransactionSystemException;

import java.util.Arrays;
import java.util.UUID;

public class ModeratorServiceTest extends BaseTest {

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Autowired
    private ModeratorService moderatorService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetModerator() {

        Moderator moderator = testObjectsFactory.createModerator();

        ModeratorReadDTO readDTO = moderatorService.getModerator(moderator.getId());

        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(moderator);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetModeratorWrongId() {
        moderatorService.getModerator(UUID.randomUUID());
    }

    @Test
    public void testCreateModerator() {

        ModeratorCreateDTO create = testObjectsFactory.generateObject(ModeratorCreateDTO.class);

        ModeratorReadDTO read = moderatorService.createModerator(create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Moderator moderator = moderatorRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(moderator);
    }

    @Test
    public void testPatchModerator() {

        Moderator moderator = testObjectsFactory.createModerator();

        ModeratorPatchDTO patch = testObjectsFactory.generateObject(ModeratorPatchDTO.class);

        ModeratorReadDTO read = moderatorService.patchModerator(moderator.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        moderator = moderatorRepository.findById(read.getId()).get();
        Assertions.assertThat(moderator).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testUpdateModerator() {

        Moderator moderator = testObjectsFactory.createModerator();

        ModeratorPutDTO put = testObjectsFactory.generateObject(ModeratorPutDTO.class);

        ModeratorReadDTO read = moderatorService.updateModerator(moderator.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        moderator = moderatorRepository.findById(read.getId()).get();
        Assertions.assertThat(moderator).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchModeratorEmptyPatch() {

        Moderator moderator = testObjectsFactory.createModerator();

        ModeratorPatchDTO patch = new ModeratorPatchDTO();
        ModeratorReadDTO read = moderatorService.patchModerator(moderator.getId(), patch);

        Assert.assertNotNull(read.getEmail());
        Assert.assertNotNull(read.getUserRole());
        Assert.assertNotNull(read.getUserStatus());

        Moderator moderatorAfterUpdate = moderatorRepository.findById(read.getId()).get();

        Assert.assertNotNull(moderatorAfterUpdate.getEmail());
        Assert.assertNotNull(moderatorAfterUpdate.getEncodedPassword());
        Assert.assertNotNull(moderatorAfterUpdate.getUserRole());
        Assert.assertNotNull(moderatorAfterUpdate.getUserStatus());

        Assertions.assertThat(moderator).isEqualToComparingFieldByField(moderatorAfterUpdate);
    }

    @Test
    public void testDeleteModerator() {

        Moderator admin = testObjectsFactory.createModerator();

        moderatorService.deleteModerator(admin.getId());
        Assert.assertFalse(moderatorRepository.existsById(admin.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteModeratorNotFound() {

        moderatorService.deleteModerator(UUID.randomUUID());
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveModeratorValidation() {

        Moderator moderator = new Moderator();
        moderatorRepository.save(moderator);
    }

    @Test
    public void testGetModeratorsWithEmptyFilterWithPagingAndSorting() throws InterruptedException {
        Moderator moderator = testObjectsFactory.createModerator();
        Thread.sleep(500);
        Moderator moderator2 = testObjectsFactory.createModerator();

        ModeratorFilter filter = new ModeratorFilter();
        PageRequest pageRequest = PageRequest.of(0, 2, Sort.by(Sort.Direction.DESC, "createdAt"));

        Assertions.assertThat(moderatorService.getModerators(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(moderator2.getId(), moderator.getId()));
    }

    @Test
    public void testGetModeratorsWithAllFiltersWithPagingAndSorting() throws InterruptedException {
        Moderator m1 = testObjectsFactory.generateFlatEntityWithoutId(Moderator.class);
        m1.setEmail("test1@mail.ru");
        m1.setUserStatus(UserStatus.NEW);
        moderatorRepository.save(m1);
        Moderator m2 = testObjectsFactory.generateFlatEntityWithoutId(Moderator.class);
        m2.setEmail("test2@mail.ru");
        m2.setUserStatus(UserStatus.ACTIVE);
        moderatorRepository.save(m2);

        ModeratorFilter filter = new ModeratorFilter();
        filter.setEmail("test1@mail.ru");
        filter.setUserStatus(UserStatus.NEW);
        PageRequest pageRequest = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "email"));

        Assertions.assertThat(moderatorService.getModerators(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(m1.getId()));
    }

    @Test
    public void testGetModeratorsWithFilterByUserStatusWithPagingAndSorting() throws InterruptedException {
        Moderator m1 = testObjectsFactory.generateFlatEntityWithoutId(Moderator.class);
        m1.setEmail("test1@mail.ru");
        m1.setUserStatus(UserStatus.NEW);
        moderatorRepository.save(m1);
        Moderator m2 = testObjectsFactory.generateFlatEntityWithoutId(Moderator.class);
        m2.setEmail("test2@mail.ru");
        m2.setUserStatus(UserStatus.NEW);
        moderatorRepository.save(m2);
        Moderator m3 = testObjectsFactory.generateFlatEntityWithoutId(Moderator.class);
        m3.setEmail("test3@mail.ru");
        m3.setUserStatus(UserStatus.ACTIVE);
        moderatorRepository.save(m3);

        ModeratorFilter filter = new ModeratorFilter();
        filter.setUserStatus(UserStatus.NEW);
        PageRequest pageRequest = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "email"));

        Assertions.assertThat(moderatorService.getModerators(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(m1.getId(), m2.getId()));
    }
}

