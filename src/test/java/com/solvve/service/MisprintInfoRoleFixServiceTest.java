package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.ContentManager;
import com.solvve.domain.Role;
import com.solvve.domain.MisprintInfoRole;
import com.solvve.domain.MisprintStatus;
import com.solvve.dto.misprintinfofix.MisprintInfoFixDTO;
import com.solvve.dto.misprintinfofix.MisprintInfoFixReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.EntityWrongStatusException;
import com.solvve.repository.RoleRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.time.Instant;
import java.util.*;

import static org.hamcrest.CoreMatchers.is;

public class MisprintInfoRoleFixServiceTest extends BaseTest {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private MisprintInfoFixService misprintInfoService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test(expected = EntityNotFoundException.class)
    public void testFixMisprintInfoRoleWrongId() {
        ContentManager contentManager = testObjectsFactory.createContentManager();
        MisprintInfoFixDTO put = new MisprintInfoFixDTO();
        misprintInfoService.fixContent(contentManager.getId(), UUID.randomUUID(), put);
    }

    @Test(expected = EntityWrongStatusException.class)
    public void testFixMisprintInfoRoleWrongStatus() {
        MisprintInfoRole misprintInfoRole = testObjectsFactory.createMisprintInfoRoleWrongStatus();
        ContentManager contentManager = testObjectsFactory.createContentManager();
        MisprintInfoFixDTO put = new MisprintInfoFixDTO();
        misprintInfoService.fixContent(contentManager.getId(), misprintInfoRole.getId(), put);
    }

    @Test
    public void testFixMisprintInfoRoleUserVersion() throws InterruptedException {

        Role role = testObjectsFactory.createRole();
        role.setText("NEo");
        role = roleRepository.save(role);
        Role role2 = testObjectsFactory.createRole();
        ContentManager contentManager = testObjectsFactory.createContentManager();
        MisprintInfoFixDTO put = new MisprintInfoFixDTO();
        MisprintInfoRole misprintInfoRole = testObjectsFactory.createMpInfoRoleWithParam(role, 0,3, "Neo", "NEo");

        List<UUID> expectedIdsOfMisprints = new ArrayList<>();
        expectedIdsOfMisprints.add(misprintInfoRole.getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoRoleWithParam(role, 1,2, "e", "E").getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoRoleWithParam(role, 1,3,"eo", "Eo").getId());
        testObjectsFactory.createMpInfoRoleWithParam(role, 5,16,"XXXXXXXXXXX", "");
        testObjectsFactory.createMpInfoRoleWithParam(role2, 1,3, "eo", "Eo");

        Instant dateBeforeTest = Instant.now();
        Thread.sleep(10);
        List<MisprintInfoFixReadDTO> mpRoleReads = misprintInfoService.fixContent(contentManager.getId(),
                misprintInfoRole.getId(), put);

        Thread.sleep(10);
        Instant dateAfterTest = Instant.now();

        Assert.assertEquals(expectedIdsOfMisprints.size(), mpRoleReads.size());
        Assertions.assertThat(mpRoleReads).extracting("id").
                containsExactlyInAnyOrder(expectedIdsOfMisprints.toArray());
        Instant actualFixDate = mpRoleReads.get(0).getFixedDate();
        Assert.assertThat(dateBeforeTest.isBefore(actualFixDate), is(true));
        Assert.assertThat(dateAfterTest.isAfter(actualFixDate), is(true));
        mpRoleReads.forEach(mpRoleRead->{
            Assert.assertEquals(mpRoleRead.getStatus(), MisprintStatus.FIXED);
            Assert.assertEquals(mpRoleRead.getContentManagerId(), contentManager.getId());
            Assert.assertEquals(mpRoleRead.getText(), misprintInfoRole.getText());
            Assert.assertEquals(mpRoleRead.getFixedDate(), actualFixDate);
        });
        String adjustedText = prepareText(misprintInfoRole,  misprintInfoRole.getText());
        Role roleAfter = roleRepository.findById(role.getId()).get();
        Assert.assertEquals(roleAfter.getText(), adjustedText);
    }

    @Test
    public void testFixMisprintInfoRoleCMVersion() throws InterruptedException {

        Role role = testObjectsFactory.createRole();
        role.setText("NEo");
        role = roleRepository.save(role);
        Role role2 = testObjectsFactory.createRole();
        ContentManager contentManager = testObjectsFactory.createContentManager();
        MisprintInfoFixDTO put = new MisprintInfoFixDTO();
        put.setText("NEO");
        MisprintInfoRole misprintInfoRole = testObjectsFactory.createMpInfoRoleWithParam(role, 0,3, "Neo", "NEo");

        List<UUID> expectedIdsOfMisprints = new ArrayList<>();
        expectedIdsOfMisprints.add(misprintInfoRole.getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoRoleWithParam(role, 1,2, "e", "E").getId());
        expectedIdsOfMisprints.add(testObjectsFactory.createMpInfoRoleWithParam(role, 1,3,"eo", "Eo").getId());
        testObjectsFactory.createMpInfoRoleWithParam(role, 5,16,"XXXXXXXXXXX", "");
        testObjectsFactory.createMpInfoRoleWithParam(role2, 1,3, "eo", "Eo");

        Instant dateBeforeTest = Instant.now();
        Thread.sleep(10);

        List<MisprintInfoFixReadDTO> mpRoleReads = misprintInfoService.fixContent(contentManager.getId(),
                misprintInfoRole.getId(), put);

        Thread.sleep(10);
        Instant dateAfterTest = Instant.now();

        Assert.assertEquals(expectedIdsOfMisprints.size(), mpRoleReads.size());
        Assertions.assertThat(mpRoleReads).extracting("id").
                containsExactlyInAnyOrder(expectedIdsOfMisprints.toArray());
        Instant actualFixDate = mpRoleReads.get(0).getFixedDate();
        Assert.assertThat(dateBeforeTest.isBefore(actualFixDate), is(true));
        Assert.assertThat(dateAfterTest.isAfter(actualFixDate), is(true));
        mpRoleReads.forEach(mpRoleRead->{
            Assert.assertEquals(mpRoleRead.getStatus(), MisprintStatus.FIXED);
            Assert.assertEquals(mpRoleRead.getContentManagerId(), contentManager.getId());
            Assert.assertEquals(mpRoleRead.getText(), "NEO");
            Assert.assertEquals(mpRoleRead.getFixedDate(), actualFixDate);
        });
        String adjustedText = prepareText(misprintInfoRole,  "NEO");
        Role roleAfter = roleRepository.findById(role.getId()).get();
        Assert.assertEquals(roleAfter.getText(), adjustedText);
    }

    private String prepareText(MisprintInfoRole misprintInfo,  String replacement) {
        String description = misprintInfo.getContent().getText();
        StringBuilder builder = new StringBuilder(description);
        builder.replace(misprintInfo.getStartIndex(), misprintInfo.getEndIndex(), replacement);
        return builder.toString();
    }
}
