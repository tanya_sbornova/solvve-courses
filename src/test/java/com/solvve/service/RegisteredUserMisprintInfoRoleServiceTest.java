package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.MisprintInfoRole;
import com.solvve.domain.RegisteredUser;
import com.solvve.dto.misprintinforole.MisprintInfoRoleCreateDTO;
import com.solvve.dto.misprintinforole.MisprintInfoRoleReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.MisprintInfoRoleRepository;
import com.solvve.repository.RegisteredUserRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class RegisteredUserMisprintInfoRoleServiceTest extends BaseTest {

    @Autowired
    private MisprintInfoRoleRepository misprintInfoRoleRepository;

    @Autowired
    private RegisteredUserMisprintInfoRoleService registeredUserMisprintInfoRoleService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void getRegisteredUserMisprintInfoRoles() {

        MisprintInfoRole misprintInfoRole1 = testObjectsFactory.createMisprintInfoRole();
        RegisteredUser registeredUser = misprintInfoRole1.getRegisteredUser();

        List<UUID> misprintInfoRolesId = transactionTemplate.execute(status -> {
            RegisteredUser registeredUser1 = registeredUserRepository.findById(registeredUser.getId()).get();
            return registeredUser1.getMisprintInfoRoles().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });
        List<MisprintInfoRoleReadDTO> misprintInfoRolesReadDTO = registeredUserMisprintInfoRoleService.
                getRegisteredUserMisprintInfoRoles(registeredUser.getId());

        Assertions.assertThat(misprintInfoRolesReadDTO).extracting("id").
                containsExactlyInAnyOrder(misprintInfoRolesId.toArray());
    }

    @Test
    public void getRegisteredUserMisprintInfoRole() {

        MisprintInfoRole misprintInfoRole = testObjectsFactory.createMisprintInfoRole();
        RegisteredUser registeredUser = misprintInfoRole.getRegisteredUser();

        List<UUID> misprintInfoRolesId = transactionTemplate.execute(status -> {
            RegisteredUser registeredUser1 = registeredUserRepository.findById(registeredUser.getId()).get();
            return registeredUser1.getMisprintInfoRoles().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });

        MisprintInfoRoleReadDTO misprintinforoleReadDTO = registeredUserMisprintInfoRoleService.
                getRegisteredUserMisprintInfoRole(registeredUser.getId(),
                        misprintInfoRolesId.get(0));

        misprintInfoRole = misprintInfoRoleRepository.findById(misprintInfoRolesId.get(0)).get();

        Assertions.assertThat(misprintinforoleReadDTO).isEqualToIgnoringGivenFields(misprintInfoRole,
                "contentId", "registeredUserId", "contentManagerId");
        Assert.assertEquals(misprintInfoRole.getContent().getId(), misprintinforoleReadDTO.getContentId());
        Assert.assertEquals(misprintInfoRole.getRegisteredUser().getId(),
                misprintinforoleReadDTO.getRegisteredUserId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRegisteredUserRoleWrongId() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        registeredUserMisprintInfoRoleService.getRegisteredUserMisprintInfoRole(registeredUser.getId(),
                UUID.randomUUID());
    }

    @Test
    public void testCreateRegisteredUserMisprintInfoRole() {

        MisprintInfoRoleCreateDTO create = testObjectsFactory.createMisprintInfoRoleCreateDTO();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        MisprintInfoRoleReadDTO read = registeredUserMisprintInfoRoleService.
                createRegisteredUserMisprintInfoRole(registeredUser.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        MisprintInfoRole misprintinfoRole = misprintInfoRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(misprintinfoRole,
                "contentId", "registeredUserId", "contentManagerId");
        Assert.assertEquals(misprintinfoRole.getContent().getId(), read.getContentId());
        Assert.assertEquals(misprintinfoRole.getRegisteredUser().getId(), read.getRegisteredUserId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateRegisteredUserMisprintInfoRoleWithWrongRole() {

        MisprintInfoRoleCreateDTO create = testObjectsFactory.createMisprintInfoRoleWithWrongRole();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        MisprintInfoRoleReadDTO read = registeredUserMisprintInfoRoleService.
                createRegisteredUserMisprintInfoRole(registeredUser.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        MisprintInfoRole misprintInfoRole = misprintInfoRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(misprintInfoRole).isEqualToIgnoringGivenFields(read, "registeredUser");
    }

    @Test
    public void testDeleteRegisteredUserRole() {

        MisprintInfoRole misprintinfoRole = testObjectsFactory.createMisprintInfoRole();
        UUID misprintinfoRoleUUID = misprintinfoRole.getId();
        RegisteredUser registeredUser = misprintinfoRole.getRegisteredUser();

        registeredUserMisprintInfoRoleService.deleteRegisteredUserMisprintInfoRole(registeredUser.getId(),
                misprintinfoRoleUUID);
        Assert.assertFalse(misprintInfoRoleRepository.existsById(misprintinfoRoleUUID));
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status->{
            runnable.run();
        });
    }
}

