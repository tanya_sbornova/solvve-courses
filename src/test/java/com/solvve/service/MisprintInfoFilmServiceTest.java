package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.MisprintInfoFilm;
import com.solvve.dto.misprintinfofilm.MisprintInfoFilmReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.MisprintInfoFilmRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.UUID;

public class MisprintInfoFilmServiceTest extends BaseTest {

    @Autowired
    private MisprintInfoFilmRepository misprintInfoRepository;

    @Autowired
    private MisprintInfoFilmService misprintInfoService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetMisprintInfoFilm() {

        MisprintInfoFilm misprintInfo = testObjectsFactory.createMisprintInfoFilm();

        MisprintInfoFilmReadDTO readDTO = misprintInfoService.getMisprintInfoFilm(misprintInfo.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(misprintInfo,
                "contentId", "registeredUserId", "contentManagerId");
        Assert.assertEquals(readDTO.getRegisteredUserId(), misprintInfo.getRegisteredUser().getId());
        Assert.assertEquals(readDTO.getContentId(), misprintInfo.getContent().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetMisprintInfoFilmWrongId() {

        misprintInfoService.getMisprintInfoFilm(UUID.randomUUID());
    }

    @Test
    public void testDeleteMisprintInfo() {

        MisprintInfoFilm misprintInfo = testObjectsFactory.createMisprintInfoFilm();

        misprintInfoService.deleteMisprintInfoFilm(misprintInfo.getId());
        Assert.assertFalse(misprintInfoRepository.existsById(misprintInfo.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteMisprintInfoNotFound() {

        misprintInfoService.deleteMisprintInfoFilm(UUID.randomUUID());
    }
}


