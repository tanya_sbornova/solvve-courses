package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.Admin;
import com.solvve.dto.admin.AdminCreateDTO;
import com.solvve.dto.admin.AdminPatchDTO;
import com.solvve.dto.admin.AdminPutDTO;
import com.solvve.dto.admin.AdminReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.AdminRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.util.UUID;

public class AdminServiceTest extends BaseTest {

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private AdminService adminService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetAdmin() {

        Admin admin = testObjectsFactory.createAdmin();

        AdminReadDTO readDTO = adminService.getAdmin(admin.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(admin, "startAt", "finishAt");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetAdminWrongId() {

        adminService.getAdmin(UUID.randomUUID());
    }

    @Test
    public void testCreateAdmin() {

        AdminCreateDTO create = testObjectsFactory.generateObject(AdminCreateDTO.class);

        AdminReadDTO read = adminService.createAdmin(create);

        Assertions.assertThat(create).isEqualToIgnoringGivenFields(read, "startAt", "finishAt");
        Assert.assertNotNull(read.getId());

        Admin admin = adminRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(read, "startAt", "finishAt");
    }

    @Test
    public void testPatchAdmin() {

        Admin admin = testObjectsFactory.createAdmin();

        AdminPatchDTO patch = testObjectsFactory.generateObject(AdminPatchDTO.class);

        AdminReadDTO read = adminService.patchAdmin(admin.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        admin = adminRepository.findById(read.getId()).get();
        Assertions.assertThat(admin).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchAdminEmptyPatch() {

        Admin admin = testObjectsFactory.createAdmin();
        AdminPatchDTO patch = new AdminPatchDTO();
        AdminReadDTO read = adminService.patchAdmin(admin.getId(), patch);

        Assert.assertNotNull(read.getEmail());
        Assert.assertNotNull(read.getUserRole());
        Assert.assertNotNull(read.getUserStatus());

        Admin adminAfterUpdate = adminRepository.findById(read.getId()).get();

        Assert.assertNotNull(adminAfterUpdate.getEmail());
        Assert.assertNotNull(adminAfterUpdate.getEncodedPassword());
        Assert.assertNotNull(adminAfterUpdate.getEncodedPassword());
        Assert.assertNotNull(adminAfterUpdate.getUserStatus());

        Assertions.assertThat(admin).isEqualToComparingFieldByField(adminAfterUpdate);
    }

    @Test
    public void testPutAdmin() {

        Admin admin = testObjectsFactory.createAdmin();

        AdminPutDTO put = testObjectsFactory.generateObject(AdminPutDTO.class);

        AdminReadDTO read = adminService.updateAdmin(admin.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        admin = adminRepository.findById(read.getId()).get();
        Assertions.assertThat(admin).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testDeleteAdmin() {

        Admin admin = testObjectsFactory.createAdmin();

        adminService.deleteAdmin(admin.getId());
        Assert.assertFalse(adminRepository.existsById(admin.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteAdminNotFound() {

        adminService.deleteAdmin(UUID.randomUUID());
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveAdminValidation() {

        Admin admin = new Admin();
        adminRepository.save(admin);
    }

}
