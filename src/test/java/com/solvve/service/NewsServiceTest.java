package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.News;
import com.solvve.domain.NewsType;
import com.solvve.dto.news.NewsCreateDTO;
import com.solvve.dto.news.NewsPatchDTO;
import com.solvve.dto.news.NewsPutDTO;
import com.solvve.dto.news.NewsReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.NewsRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.util.UUID;

public class NewsServiceTest extends BaseTest {

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private NewsService newsService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetFilmNews() {

        Film film = testObjectsFactory.createFlatFilm();
        News news = testObjectsFactory.createFilmNews(film);

        NewsReadDTO readDTO = newsService.getNews(news.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(news);
    }

    @Test
    public void testGetRoleNews() {

        Film film = testObjectsFactory.createFlatFilm();
        News news = testObjectsFactory.createFilmNews(film);

        NewsReadDTO readDTO = newsService.getNews(news.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(news);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetNewsWrongId() {

        newsService.getNews(UUID.randomUUID());
    }

    @Test
    public void testCreateFilmNews() {

        NewsCreateDTO create = new NewsCreateDTO();
        create.setTitle("About Matrix");
        create.setText("Brad Pitt reveals he turned down 'The Matrix'");
        create.setNewsType(NewsType.FILM);
        create.setCntLike(0);
        create.setCntDislike(0);

        NewsReadDTO read = newsService.createNews(create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        News news = newsRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(news);
    }

    @Test
    public void testCreateRoleNews() {

        NewsCreateDTO create = new NewsCreateDTO();
        create.setTitle("About Brad Pitt");
        create.setText("Brad Pitt reveals he turned down 'The Matrix'");
        create.setNewsType(NewsType.ROLE);
        create.setCntLike(0);
        create.setCntDislike(0);

        NewsReadDTO read = newsService.createNews(create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        News news = newsRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(news);
    }

    @Test
    public void testPatchNews() {

        Film film = testObjectsFactory.createFlatFilm();
        News news = testObjectsFactory.createFilmNews(film);

        NewsPatchDTO patch = new NewsPatchDTO();
        patch.setTitle("About Matrix");
        patch.setText("Brad Pitt reveals he turned down 'The Matrix'");

        NewsReadDTO read = newsService.patchNews(news.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        news = newsRepository.findById(read.getId()).get();
        Assertions.assertThat(news).isEqualToIgnoringGivenFields(read);
    }

    @Test
    public void testPatchNewsEmptyPatch() {

        Film film = testObjectsFactory.createFlatFilm();
        News news = testObjectsFactory.createFilmNews(film);

        NewsPatchDTO patch = new NewsPatchDTO();
        NewsReadDTO read = newsService.patchNews(news.getId(), patch);

        Assert.assertNotNull(read.getText());
        Assert.assertNotNull(read.getTitle());

        News newsAfterUpdate = newsRepository.findById(read.getId()).get();

        Assert.assertNotNull(newsAfterUpdate.getText());
        Assert.assertNotNull(newsAfterUpdate.getTitle());

        Assertions.assertThat(news).isEqualToComparingFieldByField(newsAfterUpdate);
    }

    @Test
    public void testUpdateNews() {

        Film film = testObjectsFactory.createFlatFilm();
        News news = testObjectsFactory.createFilmNews(film);

        NewsPutDTO put = new NewsPutDTO();
        put.setTitle("About Matrix");
        put.setText("Brad Pitt reveals he turned down 'The Matrix'");
        NewsReadDTO read = newsService.updateNews(news.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        news = newsRepository.findById(read.getId()).get();
        Assertions.assertThat(news).isEqualToIgnoringGivenFields(read);
    }

    @Test
    public void testDeleteNews() {

        Film film = testObjectsFactory.createFlatFilm();
        News news = testObjectsFactory.createFilmNews(film);

        newsService.deleteNews(news.getId());
        Assert.assertFalse(newsRepository.existsById(news.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteNewsNotFound() {

        newsService.deleteNews(UUID.randomUUID());
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveNewsValidation() {

        News news = new News();
        newsRepository.save(news);
    }

    @Test
    public void updateCntLikeDislike() {

        News news = testObjectsFactory.createFilmNews(testObjectsFactory.createFlatFilm());
        testObjectsFactory.createNewsLikeDislike(news, true);
        testObjectsFactory.createNewsLikeDislike(news, true);
        testObjectsFactory.createNewsLikeDislike(news, true);
        testObjectsFactory.createNewsLikeDislike(news, false);
        testObjectsFactory.createNewsLikeDislike(news, false);

        newsService.updateCntLikeDislike(news.getId());
        news = newsRepository.findById(news.getId()).get();

        Assert.assertEquals(Integer.valueOf(3), news.getCntLike());
        Assert.assertEquals(Integer.valueOf(2), news.getCntDislike());
    }
}
