package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.ProblemReview;
import com.solvve.domain.ProblemType;
import com.solvve.dto.problemreview.ProblemReviewPatchDTO;
import com.solvve.dto.problemreview.ProblemReviewPutDTO;
import com.solvve.dto.problemreview.ProblemReviewReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ProblemReviewRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class ProblemReviewServiceTest extends BaseTest {

    @Autowired
    private ProblemReviewRepository problemReviewRepository;

    @Autowired
    private ProblemReviewService problemReviewService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetProblemFilmReview() {

        ProblemReview problemReview = testObjectsFactory.createProblemFilmReview();

        ProblemReviewReadDTO readDTO = problemReviewService.getProblemReview(problemReview.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(problemReview, "registeredUserId",
                "moderatorId", "reviewFilmId", "reviewRoleId");
        Assert.assertEquals(problemReview.getRegisteredUser().getId(), readDTO.getRegisteredUserId());
        Assert.assertEquals(problemReview.getReviewFilm().getId(), readDTO.getReviewFilmId());
    }

    @Test
    public void testGetProblemRoleReview() {

        ProblemReview problemReview = testObjectsFactory.createProblemRoleReview();

        ProblemReviewReadDTO readDTO = problemReviewService.getProblemReview(problemReview.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(problemReview, "registeredUserId",
                "moderatorId", "reviewFilmId", "reviewRoleId");
        Assert.assertEquals(problemReview.getRegisteredUser().getId(), readDTO.getRegisteredUserId());
        Assert.assertEquals(problemReview.getReviewRole().getId(), readDTO.getReviewRoleId());
    }

    @Test
    public void getProblemReviews() {

        List<ProblemReview> problemReviews = List.of(testObjectsFactory.createProblemFilmReview(),
                testObjectsFactory.createProblemFilmReview(),
                testObjectsFactory.createProblemRoleReview());

        List<UUID> problemReviewsId = problemReviews.stream().
                map(x -> x.getId()).collect(Collectors.toList());

        List<ProblemReviewReadDTO> problemReviewsReadDTO = problemReviewService.getProblemReviews();

        Assertions.assertThat(problemReviewsReadDTO).extracting("id").
                containsExactlyInAnyOrder(problemReviewsId.toArray());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetProblemReviewWrongId() {

        problemReviewService.getProblemReview(UUID.randomUUID());
    }

    @Test
    public void testPatchProblemReview() {

        ProblemReview problemReview = testObjectsFactory.createProblemFilmReview();

        ProblemReviewPatchDTO patch = new ProblemReviewPatchDTO();
        patch.setProblemType(ProblemType.FILTHY_LANGUAGE);

        ProblemReviewReadDTO read = problemReviewService.patchProblemReview(problemReview.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        problemReview = problemReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(problemReview).isEqualToIgnoringGivenFields(read, "registeredUser",
                "moderator", "reviewFilm", "reviewRole");
        Assert.assertEquals(problemReview.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(problemReview.getReviewFilm().getId(), read.getReviewFilmId());
    }

    @Test
    public void testPatchProblemReviewEmptyPatch() {

        ProblemReview problemReview = testObjectsFactory.createProblemFilmReview();

        ProblemReviewPatchDTO patch = new ProblemReviewPatchDTO();
        ProblemReviewReadDTO read = problemReviewService.patchProblemReview(problemReview.getId(), patch);

        Assert.assertNotNull(read.getProblemType());

        ProblemReview problemReviewAfterUpdate = problemReviewRepository.findById(read.getId()).get();

        Assert.assertNotNull(problemReviewAfterUpdate.getProblemType());

        Assertions.assertThat(problemReview).isEqualToIgnoringGivenFields(problemReviewAfterUpdate, "registeredUser",
                "moderator", "reviewFilm", "reviewRole");
        Assert.assertEquals(problemReview.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(problemReview.getReviewFilm().getId(), read.getReviewFilmId());
    }

    @Test
    public void testUpdateProblemReview() {

        ProblemReview problemReview = testObjectsFactory.createProblemFilmReview();

        ProblemReviewPutDTO put = new ProblemReviewPutDTO();
        put.setProblemType(ProblemType.FILTHY_LANGUAGE);
        ProblemReviewReadDTO read = problemReviewService.updateProblemReview(problemReview.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        problemReview = problemReviewRepository.findById(read.getId()).get();
        Assertions.assertThat(problemReview).isEqualToIgnoringGivenFields(read, "registeredUser",
                "moderator", "reviewFilm", "reviewRole");
        Assert.assertEquals(problemReview.getRegisteredUser().getId(), read.getRegisteredUserId());
        Assert.assertEquals(problemReview.getReviewFilm().getId(), read.getReviewFilmId());
    }

    @Test
    public void testDeleteProblemReview() {

        ProblemReview problemReview = testObjectsFactory.createProblemFilmReview();

        problemReviewService.deleteProblemReview(problemReview.getId());
        Assert.assertFalse(problemReviewRepository.existsById(problemReview.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteProblemReviewNotFound() {

        problemReviewService.deleteProblemReview(UUID.randomUUID());
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveProblemReviewValidation() {

        ProblemReview problemReview = new ProblemReview();
        problemReviewRepository.save(problemReview);
    }
}

