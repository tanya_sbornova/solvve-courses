package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.Person;
import com.solvve.dto.person.PersonCreateDTO;
import com.solvve.dto.person.PersonPatchDTO;
import com.solvve.dto.person.PersonPutDTO;
import com.solvve.dto.person.PersonReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.PersonRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.util.UUID;

public class PersonServiceTest extends BaseTest {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PersonService personService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetPerson() {

        Person person = testObjectsFactory.createPerson();

        PersonReadDTO readDTO = personService.getPerson(person.getId());

        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(person);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetPersonWrongId() {

        personService.getPerson(UUID.randomUUID());
    }

    @Test
    public void testCreatePerson() {

        PersonCreateDTO create = testObjectsFactory.createPersonCreateDTO();

        PersonReadDTO read = personService.createPerson(create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);

        Assert.assertNotNull(read.getId());

        Person person = personRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(person);
    }

    @Test
    public void testPatchPerson() {

        Person person = testObjectsFactory.createPerson();

        PersonPatchDTO patch = testObjectsFactory.createPersonPatchDTO();

        PersonReadDTO read = personService.patchPerson(person.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        person = personRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(person);
    }

    @Test
    public void testUpdatePerson() {

        Person person = testObjectsFactory.createPerson();

        PersonPutDTO put = testObjectsFactory.createPersonPutDTO();

        PersonReadDTO read = personService.updatePerson(person.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        person = personRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(person);
    }

    @Test
    public void testPatchPersonEmptyPatch() {

        Person person = testObjectsFactory.createPerson();

        PersonPatchDTO patch = new PersonPatchDTO();
        PersonReadDTO read = personService.patchPerson(person.getId(), patch);

        Assert.assertNotNull(read.getName());
        Assert.assertNotNull(read.getDateBirth());
        Assert.assertNotNull(read.getPlaceBirth());

        Person personAfterUpdate = personRepository.findById(read.getId()).get();

        Assert.assertNotNull(personAfterUpdate.getName());
        Assert.assertNotNull(personAfterUpdate.getDateBirth());
        Assert.assertNotNull(personAfterUpdate.getPlaceBirth());

        Assertions.assertThat(person).isEqualToComparingFieldByField(personAfterUpdate);
    }

    @Test
    public void testDeletePerson() {

        Person person = testObjectsFactory.createPerson();
        person = personRepository.save(person);

        personService.deletePerson(person.getId());
        Assert.assertFalse(personRepository.existsById(person.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeletePersonNotFound() {

        personService.deletePerson(UUID.randomUUID());
    }

    @Test(expected = TransactionSystemException.class)
    public void testSavePersonValidation() {

        Person person = new Person();
        personRepository.save(person);
    }
}