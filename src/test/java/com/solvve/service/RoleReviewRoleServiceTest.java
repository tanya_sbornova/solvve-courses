package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.RegisteredUser;
import com.solvve.domain.ReviewStatus;
import com.solvve.domain.Role;
import com.solvve.domain.ReviewRole;
import com.solvve.dto.reviewrole.ReviewRoleCreateDTO;
import com.solvve.dto.reviewrole.ReviewRolePatchDTO;
import com.solvve.dto.reviewrole.ReviewRolePutDTO;
import com.solvve.dto.reviewrole.ReviewRoleReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.RoleRepository;
import com.solvve.repository.ReviewRoleRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class RoleReviewRoleServiceTest extends BaseTest {

    @Autowired
    private ReviewRoleRepository reviewRoleRepository;

    @Autowired
    private RoleReviewRoleService roleReviewRoleService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void getRoleReviewRoles() {

        Role role0 = testObjectsFactory.createRole();
        ReviewRole reviewRole1 = testObjectsFactory.createReviewRole(role0);
        Role role1 = reviewRole1.getRole();

        List<UUID> reviewRolesId = transactionTemplate.execute(status -> {
            Role role = roleRepository.findById(role1.getId()).get();
            return role.getReviewRoles().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });

        List<ReviewRoleReadDTO> reviewRolesReadDTO = roleReviewRoleService.getRoleReviewRoles(role1.getId());

        Assertions.assertThat(reviewRolesReadDTO).extracting("id").
                containsExactlyInAnyOrder(reviewRolesId.toArray());
    }

    @Test
    public void getRoleReviewsRoleOfCurrentRegisteredUser() {

        Role role = testObjectsFactory.createRole();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        RegisteredUser registeredUser2 = testObjectsFactory.createRegisteredUser();
        testObjectsFactory.createReviewRole(ReviewStatus.NEED_TO_MODERATE, false,
                registeredUser, role);
        testObjectsFactory.createReviewRole(ReviewStatus.FIXED, false,
                registeredUser, role);
        testObjectsFactory.createReviewRole(ReviewStatus.MODERATED, true,
                registeredUser2, role);

        List<UUID> reviewRolesId = transactionTemplate.execute(status -> {
            Role role1 = roleRepository.findById(role.getId()).get();
            return role1.getReviewRoles().stream()
                    .filter(rv -> rv.getRegisteredUser().getId().equals(registeredUser.getId()))
                    .map(x -> x.getId()).collect(Collectors.toList());
        });

        List<ReviewRoleReadDTO> reviewRolesReadDTO = roleReviewRoleService
                .getRoleReviewsRoleOfCurrentRegisteredUser(registeredUser.getId(), role.getId());

        Assertions.assertThat(reviewRolesReadDTO).extracting("id").
                containsExactlyInAnyOrder(reviewRolesId.toArray());
    }

    @Test
    public void getRoleReviewRole() {

        Role role1 = testObjectsFactory.createRole();
        ReviewRole reviewRole1 = testObjectsFactory.createReviewRole(role1);
        Role role = reviewRole1.getRole();

        ReviewRole reviewRole = transactionTemplate.execute(status -> {
            return roleRepository.findById(role.getId()).get().getReviewRoles().get(0);
        });

        ReviewRoleReadDTO reviewRoleReadDTO = roleReviewRoleService.
                    getRoleReviewRole(role.getId(), reviewRole.getId());
        Assertions.assertThat(reviewRoleReadDTO).isEqualToIgnoringGivenFields(reviewRole,
                    "roleId", "registeredUserId");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRoleReviewRoleWrongId() {
        Role role = testObjectsFactory.createRole();
        roleReviewRoleService.getRoleReviewRole(role.getId(), UUID.randomUUID());
    }

    @Test
    public void testCreateRoleReviewRole() {

        ReviewRoleCreateDTO create = testObjectsFactory.createReviewRoleCreateDTO();
        Role role = testObjectsFactory.createRole();

        ReviewRoleReadDTO read = roleReviewRoleService.createRoleReviewRole(role.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ReviewRole reviewRole = reviewRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(reviewRole, "roleId", "registeredUserId");
        Assert.assertEquals(read.getRoleId(), reviewRole.getRole().getId());
        Assert.assertEquals(read.getRegisteredUserId(), reviewRole.getRegisteredUser().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateRoleReviewRoleWithWrongUser() {

        ReviewRoleCreateDTO create = testObjectsFactory.createReviewRoleWithWrongUser();
        Role role = testObjectsFactory.createRole();

        ReviewRoleReadDTO read = roleReviewRoleService.createRoleReviewRole(role.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ReviewRole reviewRole = reviewRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(reviewRole).isEqualToIgnoringGivenFields(read, "role", "registeredUser");
        Assert.assertEquals(read.getRoleId(), reviewRole.getRole().getId());
        Assert.assertEquals(read.getRegisteredUserId(), reviewRole.getRegisteredUser().getId());
    }

    @Test
    public void testPatchRoleReviewRole() {

        Role role1 = testObjectsFactory.createRole();
        ReviewRolePatchDTO patch = new ReviewRolePatchDTO();
        patch.setText("The best role");
        patch.setStatus(ReviewStatus.MODERATED);

        ReviewRole reviewRole1 = transactionTemplate.execute(status -> {
            Role role = roleRepository.findById(role1.getId()).get();
            return roleRepository.findById(role.getId()).get().getReviewRoles().get(0);
        });

        ReviewRoleReadDTO read = roleReviewRoleService.patchRoleReviewRole(role1.getId(),
                    reviewRole1.getId(), patch);

        inTransaction(()-> {
            Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
            ReviewRole reviewRole = reviewRoleRepository.findById(reviewRole1.getId()).get();
            Assertions.assertThat(reviewRole).isEqualToIgnoringGivenFields(read,
                    "registeredUser", "role");
            Assert.assertEquals(read.getRoleId(), reviewRole.getRole().getId());
            Assert.assertEquals(read.getRegisteredUserId(), reviewRole.getRegisteredUser().getId());
        });
    }

    @Test
    public void testUpdateRoleReviewRole() {

        Role role0 = testObjectsFactory.createRole();
        ReviewRole reviewRole1 = testObjectsFactory.createReviewRole(role0);
        ReviewRolePutDTO put = new ReviewRolePutDTO();
        put.setText("The best");
        put.setStatus(ReviewStatus.FIXED);

        Role role1 = reviewRole1.getRole();

        ReviewRole reviewRole2 = transactionTemplate.execute(status -> {
            Role role = roleRepository.findById(role1.getId()).get();
            return roleRepository.findById(role.getId()).get().getReviewRoles().get(0);
        });

        ReviewRoleReadDTO read = roleReviewRoleService.updateRoleReviewRole(role1.getId(),
                    reviewRole2.getId(), put);

        inTransaction(()-> {
            Assertions.assertThat(put).isEqualToComparingFieldByField(read);
            ReviewRole reviewRole = reviewRoleRepository.findById(read.getId()).get();
            Assertions.assertThat(reviewRole).isEqualToIgnoringGivenFields(read,
                    "registeredUser", "role", "createdAt", "updatedAt");
            Assert.assertEquals(read.getRoleId(), reviewRole.getRole().getId());
            Assert.assertEquals(read.getRegisteredUserId(), reviewRole.getRegisteredUser().getId());
        });
    }

    @Test
    public void testDeleteRoleReviewRole() {

        Role role = testObjectsFactory.createRole();
        UUID reviewRoleUUID = transactionTemplate.execute(status-> {
            ReviewRole reviewRole = roleRepository.findById(role.getId()).get().getReviewRoles().get(0);
            return reviewRole.getId();
        });
        roleReviewRoleService.deleteRoleReviewRole(role.getId(), reviewRoleUUID);
        Assert.assertFalse(reviewRoleRepository.existsById(reviewRoleUUID));
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status->{
            runnable.run();
        });
    }
}
