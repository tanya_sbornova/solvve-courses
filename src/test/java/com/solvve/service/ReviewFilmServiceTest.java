package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.RegisteredUser;
import com.solvve.domain.ReviewFilm;
import com.solvve.domain.ReviewStatus;
import com.solvve.dto.reviewfilm.ReviewFilmFilter;
import com.solvve.dto.reviewfilm.ReviewFilmPatchDTO;
import com.solvve.dto.reviewfilm.ReviewFilmPutDTO;
import com.solvve.dto.reviewfilm.ReviewFilmReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ReviewFilmRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.TransactionSystemException;

import java.util.Arrays;
import java.util.UUID;

public class ReviewFilmServiceTest extends BaseTest {

    @Autowired
    private ReviewFilmRepository reviewFilmRepository;

    @Autowired
    private ReviewFilmService reviewFilmService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetReviewFilm() {

        Film film = testObjectsFactory.createFilm();
        ReviewFilm reviewFilm = testObjectsFactory.createReviewFilm(film);

        ReviewFilmReadDTO readDTO = reviewFilmService.getReviewFilm(reviewFilm.getId());

        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(reviewFilm, "filmId", "registeredUserId");
        Assert.assertEquals(readDTO.getFilmId(), reviewFilm.getFilm().getId());
        Assert.assertEquals(readDTO.getRegisteredUserId(), reviewFilm.getRegisteredUser().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetReviewFilmWrongId() {

        reviewFilmService.getReviewFilm(UUID.randomUUID());
    }

    @Test
    public void testPatchReviewFilm() {

        Film film = testObjectsFactory.createFilm();
        ReviewFilm reviewFilm = testObjectsFactory.createReviewFilm(film);

        ReviewFilmPatchDTO patch = testObjectsFactory.createReviewFilmPatchDTO();
        ReviewFilmReadDTO read = reviewFilmService.patchReviewFilm(reviewFilm.getId(), patch);

        Assertions.assertThat(patch).isEqualToIgnoringGivenFields(read);

        reviewFilm = reviewFilmRepository.findById(read.getId()).get();
        Assertions.assertThat(reviewFilm).isEqualToIgnoringGivenFields(read, "film", "registeredUser");
        Assert.assertEquals(read.getFilmId(), reviewFilm.getFilm().getId());
        Assert.assertEquals(read.getRegisteredUserId(), reviewFilm.getRegisteredUser().getId());
    }

    @Test
    public void testPatchReviewFilmEmptyPatch() {

        Film film = testObjectsFactory.createFilm();
        ReviewFilm reviewFilm = testObjectsFactory.createReviewFilm(film);

        ReviewFilmPatchDTO patch = new ReviewFilmPatchDTO();
        ReviewFilmReadDTO read = reviewFilmService.patchReviewFilm(reviewFilm.getId(), patch);

        Assert.assertNotNull(read.getText());
        Assert.assertNotNull(read.getIsSpoiler());
        Assert.assertNotNull(read.getStatus());

        ReviewFilm reviewFilmAfterUpdate = reviewFilmRepository.findById(read.getId()).get();

        Assert.assertNotNull(reviewFilmAfterUpdate.getText());
        Assert.assertNotNull(reviewFilmAfterUpdate.getIsSpoiler());
        Assert.assertNotNull(reviewFilmAfterUpdate.getStatus());

        Assertions.assertThat(reviewFilm).isEqualToIgnoringGivenFields(reviewFilmAfterUpdate,
                "film", "registeredUser");
        Assertions.assertThat(reviewFilm.getFilm()).isEqualToIgnoringGivenFields(reviewFilmAfterUpdate.getFilm(),
                "ratingFilms", "reviewFilms", "crewMembers", "genres", "companies", "averageRating", "updatedAt");
        Assertions.assertThat(reviewFilm.getRegisteredUser()).
                isEqualToIgnoringGivenFields(reviewFilmAfterUpdate.getRegisteredUser(),
                        "reviewFilmLikeDislikes", "reviewRoleLikeDislikes", "newsLikeDislikes", "problemReviews",
                        "misprintInfoFilms", "misprintInfoRoles", "misprintInfoNews");
    }

    @Test
    public void testPutReviewFilm() {

        Film film = testObjectsFactory.createFilm();
        ReviewFilm reviewFilm = testObjectsFactory.createReviewFilm(film);

        ReviewFilmPutDTO put = testObjectsFactory.createReviewFilmPutDTO();
        ReviewFilmReadDTO read = reviewFilmService.updateReviewFilm(reviewFilm.getId(), put);

        Assertions.assertThat(put).isEqualToIgnoringGivenFields(read);

        reviewFilm = reviewFilmRepository.findById(read.getId()).get();
        Assertions.assertThat(reviewFilm).isEqualToIgnoringGivenFields(read, "film", "registeredUser");
        Assert.assertEquals(read.getFilmId(), reviewFilm.getFilm().getId());
        Assert.assertEquals(read.getRegisteredUserId(), reviewFilm.getRegisteredUser().getId());
    }

    @Test
    public void testDeleteReviewFilm() {

        Film film = testObjectsFactory.createFilm();
        ReviewFilm reviewFilm = testObjectsFactory.createReviewFilm(film);

        reviewFilmService.deleteReviewFilm(reviewFilm.getId());
        Assert.assertFalse(reviewFilmRepository.existsById(reviewFilm.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteReviewFilmNotFound() {

        reviewFilmService.deleteReviewFilm(UUID.randomUUID());
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveReviewFilmValidation() {

        ReviewFilm reviewFilm = new ReviewFilm();
        reviewFilmRepository.save(reviewFilm);
    }

    @Test
    public void testGetReviewFilmsWithEmptyFilterWithPagingAndSorting() throws InterruptedException {
        Film film = testObjectsFactory.createFlatFilm();
        ReviewFilm rf1 = testObjectsFactory.createReviewFilm(film);
        Thread.sleep(500);
        ReviewFilm rf2 = testObjectsFactory.createReviewFilm(film);

        ReviewFilmFilter filter = new ReviewFilmFilter();
        PageRequest pageRequest = PageRequest.of(0, 2, Sort.by(Sort.Direction.DESC, "createdAt"));
        Assertions.assertThat(reviewFilmService.getReviewFilms(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(rf2.getId(), rf1.getId()));
    }

    @Test
    public void testGetReviewFilmsWithAllFiltersWithPagingAndSorting() {
        Film film1 = testObjectsFactory.createFlatFilm();
        RegisteredUser registeredUser1 = testObjectsFactory.createRegisteredUser();
        ReviewFilm rf1 = testObjectsFactory.createReviewFilm(ReviewStatus.FIXED, false,
                registeredUser1, film1);
        reviewFilmRepository.save(rf1);

        Film film2 = testObjectsFactory.createFlatFilm();
        RegisteredUser registeredUser2 = testObjectsFactory.createRegisteredUser();
        ReviewFilm rf2 = testObjectsFactory.createReviewFilm(ReviewStatus.CANCELLED, true,
                registeredUser2, film2);
        reviewFilmRepository.save(rf2);

        ReviewFilmFilter filter = new ReviewFilmFilter();
        filter.setStatus(ReviewStatus.FIXED);
        filter.setIsSpoiler(false);
        filter.setRegisteredUserId(registeredUser1.getId());
        filter.setFilmId(film1.getId());
        PageRequest pageRequest = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "status"));

        Assertions.assertThat(reviewFilmService.getReviewFilms(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(rf1.getId()));
    }

    @Test
    public void testGetReviewFilmsWithFilterByStatusWithPagingAndSorting() throws InterruptedException {
        Film film1 = testObjectsFactory.createFlatFilm();
        RegisteredUser registeredUser1 = testObjectsFactory.createRegisteredUser();
        ReviewFilm rf1 = testObjectsFactory.createReviewFilm(ReviewStatus.FIXED, false,
                registeredUser1, film1);
        reviewFilmRepository.save(rf1);
        Thread.sleep(500);
        Film film2 = testObjectsFactory.createFlatFilm();
        RegisteredUser registeredUser2 = testObjectsFactory.createRegisteredUser();
        ReviewFilm rf2 = testObjectsFactory.createReviewFilm(ReviewStatus.FIXED, true,
                registeredUser2, film2);
        reviewFilmRepository.save(rf2);

        ReviewFilmFilter filter = new ReviewFilmFilter();
        filter.setStatus(ReviewStatus.FIXED);
        PageRequest pageRequest = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "createdAt"));

        Assertions.assertThat(reviewFilmService.getReviewFilms(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(rf1.getId(), rf2.getId()));
    }
}

