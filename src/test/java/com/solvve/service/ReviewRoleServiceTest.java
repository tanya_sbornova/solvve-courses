package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.RegisteredUser;
import com.solvve.domain.ReviewRole;
import com.solvve.domain.ReviewStatus;
import com.solvve.domain.Role;
import com.solvve.dto.reviewrole.ReviewRoleFilter;
import com.solvve.dto.reviewrole.ReviewRolePatchDTO;
import com.solvve.dto.reviewrole.ReviewRolePutDTO;
import com.solvve.dto.reviewrole.ReviewRoleReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ReviewRoleRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.TransactionSystemException;

import java.util.Arrays;
import java.util.UUID;

public class ReviewRoleServiceTest extends BaseTest {

    @Autowired
    private ReviewRoleRepository reviewRoleRepository;

    @Autowired
    private ReviewRoleService reviewRoleService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetReviewRole() {

        Role role = testObjectsFactory.createRole();
        ReviewRole reviewRole = testObjectsFactory.createReviewRole(role);

        ReviewRoleReadDTO readDTO = reviewRoleService.getReviewRole(reviewRole.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(reviewRole, "roleId", "registeredUserId");
        Assertions.assertThat(readDTO.getRoleId()).isEqualToComparingFieldByField(reviewRole.getRole().getId());
        Assert.assertEquals(readDTO.getRoleId(), reviewRole.getRole().getId());
        Assert.assertEquals(readDTO.getRegisteredUserId(), reviewRole.getRegisteredUser().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetReviewRoleWrongId() {

        reviewRoleService.getReviewRole(UUID.randomUUID());
    }

    @Test
    public void testPatchReviewRole() {

        Role role = testObjectsFactory.createRole();
        ReviewRole reviewRole = testObjectsFactory.createReviewRole(role);

        ReviewRolePatchDTO patch = testObjectsFactory.createReviewRolePatchDTO();

        ReviewRoleReadDTO read = reviewRoleService.patchReviewRole(reviewRole.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        reviewRole = reviewRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(reviewRole, "roleId", "registeredUserId");
        Assert.assertEquals(read.getRoleId(), reviewRole.getRole().getId());
        Assert.assertEquals(read.getRegisteredUserId(), reviewRole.getRegisteredUser().getId());
    }

    @Test
    public void testPatchReviewRoleEmptyPatch() {

        Role role = testObjectsFactory.createRole();
        ReviewRole reviewRole = testObjectsFactory.createReviewRole(role);

        ReviewRolePatchDTO patch = new ReviewRolePatchDTO();
        ReviewRoleReadDTO read = reviewRoleService.patchReviewRole(reviewRole.getId(), patch);

        Assert.assertNotNull(read.getText());
        Assert.assertNotNull(read.getIsSpoiler());
        Assert.assertNotNull(read.getStatus());

        ReviewRole reviewRoleAfterUpdate = reviewRoleRepository.findById(read.getId()).get();

        Assert.assertNotNull(reviewRoleAfterUpdate.getText());
        Assert.assertNotNull(reviewRoleAfterUpdate.getIsSpoiler());
        Assert.assertNotNull(reviewRoleAfterUpdate.getStatus());

        Assertions.assertThat(reviewRole).isEqualToIgnoringGivenFields(reviewRoleAfterUpdate,
                "role", "registeredUser");
        Assertions.assertThat(reviewRole.getRole()).
                isEqualToIgnoringGivenFields(reviewRoleAfterUpdate.getRole(),
                        "ratingRoles", "reviewRoles", "actor", "film");
        Assertions.assertThat(reviewRole.getRegisteredUser()).
                isEqualToIgnoringGivenFields(reviewRoleAfterUpdate.getRegisteredUser(),
                        "reviewFilmLikeDislikes", "reviewRoleLikeDislikes", "newsLikeDislikes", "problemReviews",
                        "misprintInfoFilms", "misprintInfoRoles", "misprintInfoNews");
    }

    @Test
    public void testPutReviewRole() {

        Role role = testObjectsFactory.createRole();
        ReviewRole reviewRole = testObjectsFactory.createReviewRole(role);

        ReviewRolePutDTO put = testObjectsFactory.createReviewRolePutDTO();

        ReviewRoleReadDTO read = reviewRoleService.updateReviewRole(reviewRole.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        reviewRole = reviewRoleRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(reviewRole, "roleId", "registeredUserId");
        Assert.assertEquals(read.getRoleId(), reviewRole.getRole().getId());
        Assert.assertEquals(read.getRegisteredUserId(), reviewRole.getRegisteredUser().getId());
    }

    @Test
    public void testDeleteReviewRole() {

        Role role = testObjectsFactory.createRole();
        ReviewRole reviewRole = testObjectsFactory.createReviewRole(role);

        reviewRoleService.deleteReviewRole(reviewRole.getId());
        Assert.assertFalse(reviewRoleRepository.existsById(reviewRole.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteReviewRoleNotFound() {

        reviewRoleService.deleteReviewRole(UUID.randomUUID());
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveReviewRoleValidation() {

        ReviewRole reviewRole = new ReviewRole();
        reviewRoleRepository.save(reviewRole);
    }

    @Test
    public void testGetReviewRolesWithEmptyFilterWithPagingAndSorting() throws InterruptedException {
        Role role = testObjectsFactory.createFlatRole();
        ReviewRole rf1 = testObjectsFactory.createReviewRole(role);
        Thread.sleep(500);
        ReviewRole rf2 = testObjectsFactory.createReviewRole(role);

        ReviewRoleFilter filter = new ReviewRoleFilter();
        PageRequest pageRequest = PageRequest.of(0, 2, Sort.by(Sort.Direction.DESC, "createdAt"));

        Assertions.assertThat(reviewRoleService.getReviewRoles(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(rf2.getId(), rf1.getId()));
    }

    @Test
    public void testGetReviewRolesWithAllFiltersWithPagingAndSorting() {
        Role role1 = testObjectsFactory.createFlatRole();
        RegisteredUser registeredUser1 = testObjectsFactory.createRegisteredUser();
        ReviewRole rf1 = testObjectsFactory.createReviewRole(ReviewStatus.FIXED, false,
                registeredUser1, role1);
        reviewRoleRepository.save(rf1);

        Role role2 = testObjectsFactory.createFlatRole();
        RegisteredUser registeredUser2 = testObjectsFactory.createRegisteredUser();
        ReviewRole rf2 = testObjectsFactory.createReviewRole(ReviewStatus.CANCELLED, true,
                registeredUser2, role2);
        reviewRoleRepository.save(rf2);

        ReviewRoleFilter filter = new ReviewRoleFilter();
        filter.setStatus(ReviewStatus.FIXED);
        filter.setIsSpoiler(false);
        filter.setRegisteredUserId(registeredUser1.getId());
        filter.setRoleId(role1.getId());
        PageRequest pageRequest = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "status"));

        Assertions.assertThat(reviewRoleService.getReviewRoles(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(rf1.getId()));
    }

    @Test
    public void testGetReviewRolesWithFilterByStatusWithPagingAndSorting() throws InterruptedException {
        Role role1 = testObjectsFactory.createFlatRole();
        RegisteredUser registeredUser1 = testObjectsFactory.createRegisteredUser();
        ReviewRole rf1 = testObjectsFactory.createReviewRole(ReviewStatus.FIXED, false,
                registeredUser1, role1);
        reviewRoleRepository.save(rf1);
        Thread.sleep(500);
        Role role2 = testObjectsFactory.createFlatRole();
        RegisteredUser registeredUser2 = testObjectsFactory.createRegisteredUser();
        ReviewRole rf2 = testObjectsFactory.createReviewRole(ReviewStatus.FIXED, true,
                registeredUser2, role2);
        reviewRoleRepository.save(rf2);

        ReviewRoleFilter filter = new ReviewRoleFilter();
        filter.setStatus(ReviewStatus.FIXED);
        PageRequest pageRequest = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "createdAt"));

        Assertions.assertThat(reviewRoleService.getReviewRoles(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(rf1.getId(), rf2.getId()));
    }
}

