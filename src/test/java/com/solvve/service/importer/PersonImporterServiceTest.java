package com.solvve.service.importer;

import com.solvve.BaseTest;
import com.solvve.client.themoviedb.TheMovieDbClient;
import com.solvve.client.themoviedb.dto.PersonReadDTO;
import com.solvve.domain.Person;
import com.solvve.exception.ImportAlreadyPerformedException;
import com.solvve.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

public class PersonImporterServiceTest extends BaseTest {

    @MockBean
    private TheMovieDbClient movieDbClient;

    @Autowired
    private PersonImporterService personImporterService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testPersonImporter() throws ImportAlreadyPerformedException {

        String personExternalId = "id4";

        PersonReadDTO read = testObjectsFactory.generatePersonReadDTO();
        read.setBirthday(convertStringToLocalDate("1984-11-22"));
        read.setDeathday(null);
        read.setGender("2");

        Mockito.when(movieDbClient.getPerson(personExternalId, null)).thenReturn(read);

        Person person = personImporterService.importPersonIfNeeded(personExternalId);

        Assert.assertEquals(read.getName(), person.getName());
        Assert.assertEquals(read.getBiography(), person.getBiography());
        Assert.assertEquals(read.getBirthday(), person.getDateBirth());
        Assert.assertEquals(read.getPlaceOfBirth(), person.getPlaceBirth());
    }

    @Test
    public void testImportPersonAlreadyExist() throws ImportAlreadyPerformedException {

        String personExternalId = "id5";

        PersonReadDTO read = testObjectsFactory.generatePersonReadDTO();
        read.setBirthday(null);
        read.setDeathday(null);
        read.setGender("2");

        Mockito.when(movieDbClient.getPerson(personExternalId, null)).thenReturn(read);

        UUID personId = personImporterService.importPersonIfNeeded(personExternalId).getId();

        Mockito.verify(movieDbClient).getPerson(personExternalId, null);

        UUID personIdDuplicate = personImporterService.importPersonIfNeeded(personExternalId).getId();

        Assert.assertEquals(personId, personIdDuplicate);
    }

    private LocalDate convertStringToLocalDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(date, formatter);
    }
}
