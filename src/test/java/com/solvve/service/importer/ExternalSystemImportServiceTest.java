package com.solvve.service.importer;

import com.solvve.BaseTest;
import com.solvve.domain.ExternalSystemImport;
import com.solvve.domain.Film;
import com.solvve.domain.ImportedEntityType;
import com.solvve.exception.ImportAlreadyPerformedException;
import com.solvve.repository.ExternalSystemImportRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class ExternalSystemImportServiceTest extends BaseTest {

    @Autowired
    private ExternalSystemImportRepository externalSystemImportRepository;

    @Autowired
    private ExternalSystemImportService externalSystemImportService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testValidateNotImportedExternalSystem() throws ImportAlreadyPerformedException {
        externalSystemImportService.validateNotImported(Film.class, "some-id");
    }

    @Test
    public void testExceptionWhenAlreadyImported() {

        ExternalSystemImport esi = testObjectsFactory.generateFlatEntityWithoutId(ExternalSystemImport.class);
        esi.setEntityType(ImportedEntityType.FILM);
        esi = externalSystemImportRepository.save(esi);
        String idInExternalSystem = esi.getIdInExternalSystem();

        ImportAlreadyPerformedException ex = Assertions.catchThrowableOfType(
                () -> externalSystemImportService.validateNotImported(Film.class, idInExternalSystem),
                ImportAlreadyPerformedException.class);
        Assertions.assertThat(ex.getExternalSystemImport()).isEqualToComparingFieldByField(esi);
    }

    @Test
    public void testNoExceptionWhenAlreadyImportedButDifferentEntityType() throws ImportAlreadyPerformedException {

        ExternalSystemImport esi = testObjectsFactory.generateFlatEntityWithoutId(ExternalSystemImport.class);
        esi.setEntityType(ImportedEntityType.PERSON);
        esi = externalSystemImportRepository.save(esi);
        String idInExternalSystem = esi.getIdInExternalSystem();

        externalSystemImportService.validateNotImported(Film.class, idInExternalSystem);
    }

    @Test
    public void createExternalSystemImport() {

        Film film = testObjectsFactory.createFlatFilm();
        String idInExternalSystem = "id1";
        UUID importId = externalSystemImportService.createExternalSystemImport(film, idInExternalSystem);
        Assert.assertNotNull(importId);
        ExternalSystemImport esi = externalSystemImportRepository.findById(importId).get();
        Assert.assertEquals(idInExternalSystem, esi.getIdInExternalSystem());
        Assert.assertEquals(ImportedEntityType.FILM, esi.getEntityType());
        Assert.assertEquals(film.getId(), esi.getEntityId());
    }
}
