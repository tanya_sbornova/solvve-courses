package com.solvve.service.importer;

import com.solvve.BaseTest;
import com.solvve.client.themoviedb.TheMovieDbClient;
import com.solvve.client.themoviedb.dto.*;
import com.solvve.domain.*;
import com.solvve.exception.ImportAlreadyPerformedException;
import com.solvve.exception.ImportedEntityAlreadyExistException;
import com.solvve.repository.*;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class MovieImporterServiceTest extends BaseTest {

    @MockBean
    private TheMovieDbClient movieDbClient;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private MovieImporterService movieImporterService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testMovieImporterWithCrew() throws ImportedEntityAlreadyExistException,
            ImportAlreadyPerformedException {

        String movieExternalId = "id1";
        String personExternalId = "id11";

        MovieReadDTO read = testObjectsFactory.generateObject(MovieReadDTO.class);
        read.setReleaseDate(LocalDate.of(1999, 7, 1));
        read.setStatus(FilmStatus.RELEASED.toString());
        MovieCreditsDTO readCredits = testObjectsFactory.flatGenerateObject(MovieCreditsDTO.class);
        CrewReadDTO crew = testObjectsFactory.flatGenerateObject(CrewReadDTO.class);
        crew.setJob("Sound Re-Recording Mixer");
        crew.setDepartment("Sound");
        crew.setId(personExternalId);
        CrewReadDTO crew2 = testObjectsFactory.flatGenerateObject(CrewReadDTO.class);
        crew2.setJob("Stunt Coordinator");
        crew.setDepartment("Crew");
        crew2.setId(personExternalId);
        CrewReadDTO crew3 = testObjectsFactory.flatGenerateObject(CrewReadDTO.class);
        crew3.setJob("Co-Executive Producer");
        crew.setDepartment("Production");
        crew3.setId(personExternalId);
        readCredits.setCrew(List.of(crew, crew2, crew3));

        PersonReadDTO personReadDTO = testObjectsFactory.generatePersonReadDTO();
        personReadDTO.setBirthday(convertStringToLocalDate("2000-12-11"));
        personReadDTO.setDeathday(null);
        personReadDTO.setGender("1");

        Mockito.when(movieDbClient.getMovie(movieExternalId, null)).thenReturn(read);
        Mockito.when(movieDbClient.getPerson(personExternalId, null)).thenReturn(personReadDTO);
        Mockito.when(movieDbClient.getCreditsOfMovie(movieExternalId, null)).thenReturn(readCredits);

        UUID movieId = movieImporterService.importMovie(movieExternalId);

        inTransaction(() -> {
            Film film = filmRepository.findById(movieId).get();
            Assert.assertEquals(read.getTitle(), film.getTitle());
            Assert.assertEquals(read.getOverview(), film.getDescription());
            Assert.assertEquals(readCredits.getCrew().size(), film.getCrewMembers().size());

            List<String> personsName = transactionTemplate.execute(status -> {
                return film.getCrewMembers().stream().
                        map(x -> x.getPerson().getName()).collect(Collectors.toList());
            });

            Assertions.assertThat(film.getCrewMembers()).extracting("CrewMemberType").
                    containsExactlyInAnyOrder(convertStringToCrewMemberType(crew.getJob()),
                            convertStringToCrewMemberType(crew2.getJob()),
                            convertStringToCrewMemberType(crew3.getJob()));
            Assertions.assertThat(personsName).contains(personReadDTO.getName());
        });
    }

    @Test
    public void testMovieImporterWithCast() throws ImportedEntityAlreadyExistException,
            ImportAlreadyPerformedException {

        String movieExternalId = "id22";
        String personExternalId = "id33";
        String personExternalId2 = "id44";

        MovieReadDTO read = testObjectsFactory.generateObject(MovieReadDTO.class);
        read.setReleaseDate(LocalDate.of(1999, 7, 1));
        read.setStatus(FilmStatus.RELEASED.toString());
        MovieCreditsDTO readCredits = testObjectsFactory.flatGenerateObject(MovieCreditsDTO.class);
        CastReadDTO cast = testObjectsFactory.flatGenerateObject(CastReadDTO.class);
        cast.setId(personExternalId);
        CastReadDTO cast2 = testObjectsFactory.flatGenerateObject(CastReadDTO.class);
        cast2.setId(personExternalId2);
        readCredits.setCast(List.of(cast, cast2));

        PersonReadDTO personReadDTO = testObjectsFactory.generatePersonReadDTO();
        personReadDTO.setBirthday(convertStringToLocalDate("1984-11-22"));
        personReadDTO.setDeathday(null);
        personReadDTO.setGender("1");

        PersonReadDTO personReadDTO2 = testObjectsFactory.generatePersonReadDTO();
        personReadDTO2.setBirthday(convertStringToLocalDate("1984-11-22"));
        personReadDTO2.setDeathday(null);
        personReadDTO2.setGender("1");

        Mockito.when(movieDbClient.getMovie(movieExternalId, null)).thenReturn(read);
        Mockito.when(movieDbClient.getPerson(personExternalId, null)).thenReturn(personReadDTO);
        Mockito.when(movieDbClient.getPerson(personExternalId2, null)).thenReturn(personReadDTO2);
        Mockito.when(movieDbClient.getCreditsOfMovie(movieExternalId, null)).thenReturn(readCredits);

        UUID movieId = movieImporterService.importMovie(movieExternalId);

        inTransaction(() -> {
            Film film = filmRepository.findById(movieId).get();
            Assert.assertEquals(read.getTitle(), film.getTitle());
            Assert.assertEquals(read.getOverview(), film.getDescription());

            long actorCount = actorRepository.count();
            Assert.assertEquals(readCredits.getCast().size(), actorCount);

            Actor actor = actorRepository.findByPersonName(personReadDTO.getName());
            Assert.assertNotNull(actor.getId());

            Actor actors2 = actorRepository.findByPersonName(personReadDTO2.getName());
            Assert.assertNotNull(actors2.getId());

            Role role = roleRepository.findRoleByFilmIdAndNameRole(film.getId(), cast.getCharacter());
            Assert.assertNotNull(role.getId());

            Role role2 = roleRepository.findRoleByFilmIdAndNameRole(film.getId(), cast2.getCharacter());
            Assert.assertNotNull(role2.getId());
        });
    }

    @Test
    public void testMovieImporterAlreadyExist() {

        String movieExternalId = "id2";

        Film existingFilm = testObjectsFactory.createFlatFilm();
        existingFilm = filmRepository.save(existingFilm);

        MovieReadDTO read = testObjectsFactory.generateObject(MovieReadDTO.class);
        read.setTitle(existingFilm.getTitle());
        Mockito.when(movieDbClient.getMovie(movieExternalId, null)).thenReturn(read);

        ImportedEntityAlreadyExistException ex = Assertions.catchThrowableOfType(
                () -> movieImporterService.importMovie(movieExternalId), ImportedEntityAlreadyExistException.class);
        Assert.assertEquals(Film.class, ex.getEntityClass());
        Assert.assertEquals(existingFilm.getId(), ex.getEntityId());
    }

    @Test
    public void testNoCallToClientOnDuplicateImport() throws ImportedEntityAlreadyExistException,
            ImportAlreadyPerformedException {

        String movieExternalId = "id3";

        MovieReadDTO read = testObjectsFactory.generateObject(MovieReadDTO.class);
        read.setReleaseDate(LocalDate.of(1999, 7, 1));
        read.setStatus(FilmStatus.RELEASED.toString());
        MovieCreditsDTO readCredits = testObjectsFactory.flatGenerateObject(MovieCreditsDTO.class);

        Mockito.when(movieDbClient.getMovie(movieExternalId, null)).thenReturn(read);
        Mockito.when(movieDbClient.getCreditsOfMovie(movieExternalId, null)).thenReturn(readCredits);

        movieImporterService.importMovie(movieExternalId);
        Mockito.verify(movieDbClient).getMovie(movieExternalId, null);
        Mockito.reset(movieDbClient);

        Assertions.assertThatThrownBy(() -> movieImporterService
                .importMovie(movieExternalId)).isInstanceOf(ImportAlreadyPerformedException.class);

        Mockito.verifyNoInteractions(movieDbClient);
    }

    @Test
    public void testImportCreditsOfMovie() {

        String movieExternalId = "id6";

        Film film = testObjectsFactory.createFlatFilm();

        Person person = testObjectsFactory.createPerson();
        person.setDateBirth(convertStringToLocalDate("2000-12-11"));
        person.setSex(Sex.MALE);
        personRepository.save(person);

        String personExternalId = person.getId().toString();

        MovieCreditsDTO read = testObjectsFactory.flatGenerateObject(MovieCreditsDTO.class);
        CrewReadDTO crew = testObjectsFactory.flatGenerateObject(CrewReadDTO.class);
        crew.setJob("Sound Re-Recording Mixer");
        crew.setDepartment("Sound");
        crew.setId(personExternalId);
        CrewReadDTO crew2 = testObjectsFactory.flatGenerateObject(CrewReadDTO.class);
        crew2.setJob("Stunt Coordinator");
        crew2.setDepartment("Crew");
        crew2.setId(personExternalId);
        CrewReadDTO crew3 = testObjectsFactory.flatGenerateObject(CrewReadDTO.class);
        crew3.setJob("Co-Executive Producer");
        crew3.setDepartment("Production");
        crew3.setId(personExternalId);
        read.setCrew(List.of(crew, crew2, crew3));

        PersonReadDTO personReadDTO = testObjectsFactory.generatePersonReadDTO();
        personReadDTO.setBirthday(convertStringToLocalDate("2000-12-11"));
        personReadDTO.setDeathday(null);
        personReadDTO.setGender("2");

        Mockito.when(movieDbClient.getCreditsOfMovie(movieExternalId, null)).thenReturn(read);
        Mockito.when(movieDbClient.getPerson(personExternalId, null)).thenReturn(personReadDTO);

        Set<CrewMember> crewMembers = movieImporterService.importCreditsOfMovieIfNeeded(movieExternalId, film);

        Assert.assertEquals(read.getCrew().size(), crewMembers.size());

        inTransaction(() -> {

            List<String> personsName = transactionTemplate.execute(status -> {
                return crewMembers.stream().
                        map(x -> x.getPerson().getName()).collect(Collectors.toList());
            });

            Assertions.assertThat(crewMembers).extracting("CrewMemberType").
                    containsExactlyInAnyOrder(convertStringToCrewMemberType(crew.getJob()),
                            convertStringToCrewMemberType(crew2.getJob()),
                            convertStringToCrewMemberType(crew3.getJob()));
            Assertions.assertThat(personsName).contains(personReadDTO.getName());
        });
    }

    @Test
    public void testMovieImporterWithCrewAlreadyExist()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {

        CrewMember crew = testObjectsFactory.flatGenerateObject(CrewMember.class);
        crew.setCrewMemberType(CrewMemberType.CASTING);
        Person person = testObjectsFactory.createPerson();
        person.setName("Abra Edelman");
        person = personRepository.save(person);
        crew.setPerson(person);
        CrewMember crewMember = crewMemberRepository.save(crew);
        Department department = testObjectsFactory.flatGenerateObject(Department.class);
        department.setDepartmentType(DepartmentType.PRODUCTION);
        departmentRepository.save(department);
        CrewMember crew2 = testObjectsFactory.flatGenerateObject(CrewMember.class);
        crew2.setCrewMemberType(CrewMemberType.FIRST_ASSISTANT_EDITOR);
        person = testObjectsFactory.createPerson();
        person.setName("Caroline Ross");
        person = personRepository.save(person);
        crew2.setPerson(person);
        CrewMember crewMember2 = crewMemberRepository.save(crew2);
        department = testObjectsFactory.flatGenerateObject(Department.class);
        department.setDepartmentType(DepartmentType.EDITING);
        departmentRepository.save(department);
        CrewMember crew3 = testObjectsFactory.flatGenerateObject(CrewMember.class);
        crew3.setCrewMemberType(CrewMemberType.CO_EXECUTIVE_PRODUCER);
        crew3.setPerson(testObjectsFactory.createPerson());
        crewMemberRepository.save(crew3);
        department = testObjectsFactory.flatGenerateObject(Department.class);
        department.setDepartmentType(DepartmentType.PRODUCTION);
        departmentRepository.save(department);
        String movieExternalId = "280";
        String personExternalId = "id11";
        PersonReadDTO personReadDTO = testObjectsFactory.generatePersonReadDTO();
        personReadDTO.setName("Abra Edelman");
        personReadDTO.setBirthday(convertStringToLocalDate("2000-12-11"));
        personReadDTO.setDeathday(null);
        personReadDTO.setGender("1");

        String personExternalId2 = "id12";
        PersonReadDTO personReadDTO2 = testObjectsFactory.generatePersonReadDTO();
        personReadDTO.setName("Caroline Ross");
        personReadDTO.setBirthday(convertStringToLocalDate("2000-12-11"));
        personReadDTO.setDeathday(null);
        personReadDTO.setGender("1");

        MovieReadDTO readMovie = testObjectsFactory.generateObject(MovieReadDTO.class);
        readMovie.setReleaseDate(LocalDate.of(1999, 7, 1));
        readMovie.setStatus(FilmStatus.NOT_RELEASED.toString());
        MovieCreditsDTO readCredits = testObjectsFactory.flatGenerateObject(MovieCreditsDTO.class);
        CrewReadDTO crewCredits = testObjectsFactory.flatGenerateObject(CrewReadDTO.class);
        crewCredits.setJob("Casting");
        crewCredits.setName("Abra Edelman");
        crewCredits.setId(personExternalId);
        CrewReadDTO crewCredits2 = testObjectsFactory.flatGenerateObject(CrewReadDTO.class);
        crewCredits2.setJob("First assistant editor");
        crewCredits2.setName("Caroline Ross");
        crewCredits2.setId(personExternalId);
        CrewReadDTO crewCredits3 = testObjectsFactory.flatGenerateObject(CrewReadDTO.class);
        crewCredits3.setJob("Co-Executive Producer");
        crewCredits3.setId(personExternalId);
        CrewReadDTO crewCredits4 = testObjectsFactory.flatGenerateObject(CrewReadDTO.class);
        crewCredits4.setJob("No-define");
        crewCredits4.setId(personExternalId);
        readCredits.setCrew(List.of(crewCredits, crewCredits2, crewCredits3, crewCredits4));
        readMovie.setId(movieExternalId);
        readMovie.setTitle("Terminator 2: Judgment Day");

        Mockito.when(movieDbClient.getMovie(movieExternalId, null)).thenReturn(readMovie);
        Mockito.when(movieDbClient.getCreditsOfMovie(movieExternalId, null)).thenReturn(readCredits);
        Mockito.when(movieDbClient.getPerson(personExternalId, null)).thenReturn(personReadDTO);
        Mockito.when(movieDbClient.getPerson(personExternalId2, null)).thenReturn(personReadDTO2);

        movieImporterService.importMovie(movieExternalId);

        inTransaction(() -> {
            CrewMember crewAfter = crewMemberRepository.findById(crewMember.getId()).get();
            Assertions.assertThat(crewAfter.getFilms()).extracting("title").contains("Terminator 2: Judgment Day");
            CrewMember crew2After = crewMemberRepository.findById(crewMember2.getId()).get();
            Assertions.assertThat(crew2After.getFilms()).extracting("title").contains("Terminator 2: Judgment Day");
        });
    }

    @Test
    public void testMovieImporterWithCastAlreadyExist()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {

        Film film = testObjectsFactory.createFlatFilm();
        film.setTitle("The Terminator");
        film = filmRepository.save(film);

        Person person = testObjectsFactory.createPerson();
        person.setName("Linda Hamilton");
        person = personRepository.save(person);
        Actor actor = testObjectsFactory.flatGenerateObject(Actor.class);
        actor.setPerson(person);
        actor = actorRepository.save(actor);
        Role role = testObjectsFactory.flatGenerateObject(Role.class);
        role.setText("Sarah Connor");
        role.setFilm(film);
        role.setActor(actor);
        roleRepository.save(role);
        Actor actorSave = actorRepository.save(actor);

        Role existingRole = roleRepository.findRoleByFilmIdAndNameRole(film.getId(), "Sarah Connor");

        person = testObjectsFactory.createPerson();
        person.setName("Arnold Schwarzenegger");
        person = personRepository.save(person);
        Actor actor2 = testObjectsFactory.flatGenerateObject(Actor.class);
        actor2.setPerson(person);
        actor2 = actorRepository.save(actor2);
        Role role2 = testObjectsFactory.flatGenerateObject(Role.class);
        role2.setText("The Terminator");
        role2.setFilm(film);
        role2.setActor(actor2);
        roleRepository.save(role2);
        Actor actor2Save = actorRepository.save(actor2);

        Role existingRole2 = roleRepository.findRoleByFilmIdAndNameRole(film.getId(), "The Terminator");

        String movieExternalId = "id555";
        String personExternalId = "id11";
        PersonReadDTO personReadDTO = testObjectsFactory.generatePersonReadDTO();
        personReadDTO.setName("Linda Hamilton");
        personReadDTO.setBirthday(convertStringToLocalDate("2000-12-11"));
        personReadDTO.setBirthday(null);
        personReadDTO.setGender("1");

        String personExternalId2 = "id12";
        PersonReadDTO personReadDTO2 = testObjectsFactory.generatePersonReadDTO();
        personReadDTO2.setName("Arnold Schwarzenegger");
        personReadDTO2.setBirthday(convertStringToLocalDate("2000-12-11"));
        personReadDTO2.setBirthday(null);
        personReadDTO2.setGender("2");

        MovieReadDTO readMovie = testObjectsFactory.generateObject(MovieReadDTO.class);
        readMovie.setReleaseDate(LocalDate.of(1999, 7, 1));
        readMovie.setStatus(FilmStatus.RELEASED.toString());
        MovieCreditsDTO readCredits = testObjectsFactory.flatGenerateObject(MovieCreditsDTO.class);
        CastReadDTO castCredits = testObjectsFactory.flatGenerateObject(CastReadDTO.class);
        castCredits.setName("Linda Hamilton");
        castCredits.setCharacter("Sarah Connor");
        castCredits.setId(personExternalId);
        CastReadDTO castCredits2 = testObjectsFactory.flatGenerateObject(CastReadDTO.class);
        castCredits2.setName("Arnold Schwarzenegger");
        castCredits2.setCharacter("The Terminator");
        castCredits2.setId(personExternalId2);
        CastReadDTO castCredits3 = testObjectsFactory.flatGenerateObject(CastReadDTO.class);
        castCredits3.setId(personExternalId);
        readCredits.setCast(List.of(castCredits, castCredits2, castCredits3));
        readMovie.setId(movieExternalId);
        readMovie.setTitle("Terminator 2: Judgment Day");

        Mockito.when(movieDbClient.getMovie(movieExternalId, null)).thenReturn(readMovie);
        Mockito.when(movieDbClient.getCreditsOfMovie(movieExternalId, null)).thenReturn(readCredits);
        Mockito.when(movieDbClient.getPerson(personExternalId, null)).thenReturn(personReadDTO);
        Mockito.when(movieDbClient.getPerson(personExternalId2, null)).thenReturn(personReadDTO2);

        movieImporterService.importMovie(movieExternalId);

        inTransaction(() -> {
            Actor actorAfter = actorRepository.findById(actorSave.getId()).get();
            Assertions.assertThat(actorAfter.getRoles()).extracting("text").contains("Sarah Connor");

            Actor actor2After = actorRepository.findById(actor2Save.getId()).get();

            Assertions.assertThat(actor2After.getRoles()).extracting("text").contains("The Terminator");
        });
    }

    @Test
    public void testImportActorsOfMovieWithRoleAlreadyExist() {

        Film film = testObjectsFactory.createFlatFilm();
        film.setTitle("Terminator 2: Judgment Day");
        film = filmRepository.save(film);

        Person person = testObjectsFactory.createPerson();
        person.setName("Linda Hamilton");
        person = personRepository.save(person);
        Actor actor = testObjectsFactory.flatGenerateObject(Actor.class);
        actor.setPerson(person);
        actor = actorRepository.save(actor);
        Role role = testObjectsFactory.flatGenerateObject(Role.class);
        role.setText("Sarah Connor");
        role.setFilm(film);
        role.setActor(actor);
        roleRepository.save(role);
        Actor actorSave = actorRepository.save(actor);

        Role existingRole = roleRepository.findRoleByFilmIdAndNameRole(film.getId(), "Sarah Connor");

        person = testObjectsFactory.createPerson();
        person.setName("Arnold Schwarzenegger");
        person = personRepository.save(person);
        Actor actor2 = testObjectsFactory.flatGenerateObject(Actor.class);
        actor2.setPerson(person);
        actor2 = actorRepository.save(actor2);
        Role role2 = testObjectsFactory.flatGenerateObject(Role.class);
        role2.setText("The Terminator");
        role2.setFilm(film);
        role2.setActor(actor2);
        roleRepository.save(role2);
        Actor actor2Save = actorRepository.save(actor2);

        Role existingRole2 = roleRepository.findRoleByFilmIdAndNameRole(film.getId(), "The Terminator");

        String movieExternalId = film.getId().toString();
        String personExternalId = "id11";
        PersonReadDTO personReadDTO = testObjectsFactory.generatePersonReadDTO();
        personReadDTO.setName("Linda Hamilton");
        personReadDTO.setBirthday(convertStringToLocalDate("2000-12-11"));
        personReadDTO.setGender("1");

        String personExternalId2 = "id12";
        PersonReadDTO personReadDTO2 = testObjectsFactory.generatePersonReadDTO();
        personReadDTO2.setName("Arnold Schwarzenegger");
        personReadDTO2.setBirthday(convertStringToLocalDate("2000-12-11"));
        personReadDTO2.setGender("2");

        CastReadDTO castCredits = testObjectsFactory.flatGenerateObject(CastReadDTO.class);
        castCredits.setName("Linda Hamilton");
        castCredits.setCharacter("Sarah Connor");
        castCredits.setId(personExternalId);
        CastReadDTO castCredits2 = testObjectsFactory.flatGenerateObject(CastReadDTO.class);
        castCredits2.setName("Arnold Schwarzenegger");
        castCredits2.setCharacter("The Terminator");
        castCredits2.setId(personExternalId2);
        CastReadDTO castCredits3 = testObjectsFactory.flatGenerateObject(CastReadDTO.class);
        castCredits3.setId(personExternalId);
        List<CastReadDTO> castToImport = List.of(castCredits, castCredits2, castCredits3);

        Mockito.when(movieDbClient.getPerson(personExternalId, null)).thenReturn(personReadDTO);
        Mockito.when(movieDbClient.getPerson(personExternalId2, null)).thenReturn(personReadDTO2);

        movieImporterService.importActorsOfMovieIfNeeded(castToImport, movieExternalId, film);

        inTransaction(() -> {
            Actor actorAfter = actorRepository.findById(actorSave.getId()).get();
            Assertions.assertThat(actorAfter.getRoles()).extracting("text").contains("Sarah Connor");
            Actor actor2After = actorRepository.findById(actor2Save.getId()).get();

            Assertions.assertThat(actor2After.getRoles()).extracting("text").contains("The Terminator");
        });
    }

    @Test
    public void testImportCompaniesOfMovie() {

        String movieExternalId = "id6";

        Film film = testObjectsFactory.createFlatFilm();

        ProductionCompanyReadDTO companyReadDTO = testObjectsFactory
                .flatGenerateObject(ProductionCompanyReadDTO.class);
        companyReadDTO.setOriginCountry("RU");
        ProductionCompanyReadDTO companyReadDTO2 = testObjectsFactory
                .flatGenerateObject(ProductionCompanyReadDTO.class);
        companyReadDTO2.setOriginCountry("UA");
        ProductionCompanyReadDTO companyReadDTO3 = testObjectsFactory
                .flatGenerateObject(ProductionCompanyReadDTO.class);
        companyReadDTO3.setOriginCountry("US");
        ProductionCompanyReadDTO companyReadDTO4 = testObjectsFactory
                .flatGenerateObject(ProductionCompanyReadDTO.class);
        companyReadDTO4.setOriginCountry("GB");
        List<ProductionCompanyReadDTO> companiesToImport = List.of(companyReadDTO,
                companyReadDTO2, companyReadDTO3, companyReadDTO4);

        Set<Company> companies = movieImporterService.importCompaniesOfMovieIfNeeded(companiesToImport,
                movieExternalId, film);

        Assert.assertEquals(companiesToImport.size(), companies.size());

        inTransaction(() -> {


            Assertions.assertThat(companies).extracting("countryName").
                    containsExactlyInAnyOrder(Country.valueOf(companyReadDTO.getOriginCountry()),
                            Country.valueOf(companyReadDTO2.getOriginCountry()),
                            Country.valueOf(companyReadDTO3.getOriginCountry()),
                            Country.valueOf(companyReadDTO4.getOriginCountry()));
            Assertions.assertThat(companies).extracting("name").
                    containsExactlyInAnyOrder(companyReadDTO.getName(),
                            companyReadDTO2.getName(),
                            companyReadDTO3.getName(),
                            companyReadDTO4.getName());
            for(Company company :companies) {
                Assertions.assertThat(company.getFilms()).extracting("id").
                        containsExactlyInAnyOrder(film.getId());
            }
        });
    }

    @Test
    public void testImportCompanyAlreadyExist() {

        String movieExternalId = "id6";

        Film film = testObjectsFactory.createFlatFilm();

        Company company = testObjectsFactory.flatGenerateObject(Company.class);
        company.setName("Company1");
        company.setCountryName(Country.RU);
        companyRepository.save(company);
        Company company2 = testObjectsFactory.flatGenerateObject(Company.class);
        company2.setName("Company2");
        company2.setCountryName(Country.UA);
        companyRepository.save(company2);
        Company company3 = testObjectsFactory.flatGenerateObject(Company.class);
        company3.setName("Company3");
        company3.setCountryName(Country.US);
        companyRepository.save(company3);
        Company company4 = testObjectsFactory.flatGenerateObject(Company.class);
        company4.setName("Company4");
        company4.setCountryName(Country.GB);
        companyRepository.save(company4);

        ProductionCompanyReadDTO companyReadDTO = testObjectsFactory
                .flatGenerateObject(ProductionCompanyReadDTO.class);
        companyReadDTO.setName("Company1");
        companyReadDTO.setOriginCountry("RU");
        ProductionCompanyReadDTO companyReadDTO2 = testObjectsFactory
                .flatGenerateObject(ProductionCompanyReadDTO.class);
        companyReadDTO2.setName("Company2");
        companyReadDTO2.setOriginCountry("UA");
        ProductionCompanyReadDTO companyReadDTO3 = testObjectsFactory
                .flatGenerateObject(ProductionCompanyReadDTO.class);
        companyReadDTO3.setName("Company3");
        companyReadDTO3.setOriginCountry("US");
        ProductionCompanyReadDTO companyReadDTO4 = testObjectsFactory
                .flatGenerateObject(ProductionCompanyReadDTO.class);
        companyReadDTO4.setName("Company4");
        companyReadDTO4.setOriginCountry("GB");
        List<ProductionCompanyReadDTO> companiesToImport = List.of(companyReadDTO,
                companyReadDTO2, companyReadDTO3, companyReadDTO4);

        Set<Company> companies = movieImporterService.importCompaniesOfMovieIfNeeded(companiesToImport,
                movieExternalId, film);

        Assert.assertEquals(companiesToImport.size(), companies.size());

        inTransaction(() -> {

            Assertions.assertThat(companies).extracting("countryName").
                    containsExactlyInAnyOrder(Country.valueOf(companyReadDTO.getOriginCountry()),
                            Country.valueOf(companyReadDTO2.getOriginCountry()),
                            Country.valueOf(companyReadDTO3.getOriginCountry()),
                            Country.valueOf(companyReadDTO4.getOriginCountry()));
            Assertions.assertThat(companies).extracting("name").
                    containsExactlyInAnyOrder(companyReadDTO.getName(),
                            companyReadDTO2.getName(),
                            companyReadDTO3.getName(),
                            companyReadDTO4.getName());
            for(Company comp :companies) {
                Assertions.assertThat(comp.getFilms()).extracting("id").
                        containsExactlyInAnyOrder(film.getId());
            }
        });
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }

    private CrewMemberType convertStringToCrewMemberType(String job) {
        return CrewMemberType.valueOf(job
                .replace(' ', '_').replace('-', '_').toUpperCase());
    }

    private  LocalDate convertStringToLocalDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(date, formatter);
    }
}

