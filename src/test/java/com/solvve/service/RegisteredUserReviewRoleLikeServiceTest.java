package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.ReviewRoleLikeDislike;
import com.solvve.domain.RegisteredUser;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikeCreateDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePatchDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePutDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikeReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ReviewRoleLikeDislikeRepository;
import com.solvve.repository.RegisteredUserRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class RegisteredUserReviewRoleLikeServiceTest extends BaseTest {

    @Autowired
    private ReviewRoleLikeDislikeRepository reviewRoleLikeDislikeRepository;

    @Autowired
    private RegisteredUserReviewRoleLikeService registeredUserReviewRoleLikeService;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void getRegisteredUserReviewRoleLikes() {

        ReviewRoleLikeDislike reviewRoleLikeDislike1 = testObjectsFactory.createReviewRoleLikeDislike();
        RegisteredUser registeredUser = reviewRoleLikeDislike1.getRegisteredUser();

        List<UUID> reviewRoleLikeDislikesId = transactionTemplate.execute(status -> {
            RegisteredUser registeredUser1 = registeredUserRepository.findById(registeredUser.getId()).get();
            return registeredUser1.getReviewRoleLikeDislikes().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });

        List<ReviewRoleLikeDislikeReadDTO> reviewRoleLikeDislikesReadDTO = registeredUserReviewRoleLikeService.
                getRegisteredUserReviewRoleLikes(registeredUser.getId());
        Assertions.assertThat(reviewRoleLikeDislikesReadDTO).extracting("id").
                containsExactlyInAnyOrder(reviewRoleLikeDislikesId.toArray());
    }

    @Test
    public void getRegisteredUserReviewRoleLike() {

        ReviewRoleLikeDislike reviewRoleLikeDislike = testObjectsFactory.createReviewRoleLikeDislike();
        RegisteredUser registeredUser = reviewRoleLikeDislike.getRegisteredUser();

        List<UUID> reviewRoleLikeDislikesId = transactionTemplate.execute(status -> {
            RegisteredUser registeredUser1 = registeredUserRepository.findById(registeredUser.getId()).get();
            return registeredUser1.getReviewRoleLikeDislikes().stream().
                    map(x -> x.getId()).collect(Collectors.toList());
        });

        ReviewRoleLikeDislikeReadDTO reviewRoleLikeDislikeReadDTO = registeredUserReviewRoleLikeService.
                getRegisteredUserReviewRoleLike(registeredUser.getId(),
                        reviewRoleLikeDislikesId.get(0));

        Assertions.assertThat(reviewRoleLikeDislikeReadDTO).isEqualToIgnoringGivenFields(reviewRoleLikeDislike,
                "reviewRoleId", "registeredUserId");
        Assert.assertEquals(reviewRoleLikeDislike.getReviewRole().getId(),
                reviewRoleLikeDislikeReadDTO.getReviewRoleId());
        Assert.assertEquals(reviewRoleLikeDislike.getRegisteredUser().getId(),
                reviewRoleLikeDislikeReadDTO.getRegisteredUserId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRegisteredUserReviewRoleWrongId() {
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();
        registeredUserReviewRoleLikeService.getRegisteredUserReviewRoleLike(registeredUser.getId(), UUID.randomUUID());
    }

    @Test
    public void testCreateRegisteredUserReviewRole() {

        ReviewRoleLikeDislikeCreateDTO create = testObjectsFactory.createReviewRoleLikeDislikeCreateDTO();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        ReviewRoleLikeDislikeReadDTO read = registeredUserReviewRoleLikeService.
                createRegisteredUserReviewRoleLike(registeredUser.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ReviewRoleLikeDislike reviewRoleLikeDislike = reviewRoleLikeDislikeRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(reviewRoleLikeDislike,
                "reviewRoleId", "registeredUserId");
        Assert.assertEquals(read.getReviewRoleId(), reviewRoleLikeDislike.getReviewRole().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateRegisteredUserReviewRoleWithWrongUser() {

        ReviewRoleLikeDislikeCreateDTO create = testObjectsFactory.
                createReviewRoleLikeDislikeWithWrongReviewRole();
        RegisteredUser registeredUser = testObjectsFactory.createRegisteredUser();

        ReviewRoleLikeDislikeReadDTO read = registeredUserReviewRoleLikeService.
                createRegisteredUserReviewRoleLike(registeredUser.getId(), create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ReviewRoleLikeDislike reviewReviewRole = reviewRoleLikeDislikeRepository.findById(read.getId()).get();
        Assertions.assertThat(reviewReviewRole).isEqualToIgnoringGivenFields(read,
                "reviewRole", "registeredUser");
    }

    @Test
    public void testPatchRegisteredUserReviewRole() {

        ReviewRoleLikeDislike reviewRoleLikeDislike1 = testObjectsFactory.createReviewRoleLikeDislike();
        ReviewRoleLikeDislikePatchDTO patch = new ReviewRoleLikeDislikePatchDTO();
        patch.setIsLike(true);

        RegisteredUser registeredUser1 = reviewRoleLikeDislike1.getRegisteredUser();
        ReviewRoleLikeDislike reviewRoleLikeDislike = transactionTemplate.execute(status -> {
            return registeredUserRepository.findById(registeredUser1.getId()).get().
                    getReviewRoleLikeDislikes().get(0);
        });

        ReviewRoleLikeDislikeReadDTO read = registeredUserReviewRoleLikeService.
                patchRegisteredUserReviewRoleLike(registeredUser1.getId(),
                        reviewRoleLikeDislike.getId(), patch);

        inTransaction(()-> {
            Assertions.assertThat(patch).isEqualToComparingFieldByField(read);
            ReviewRoleLikeDislike reviewRoleLikeDislike2 = reviewRoleLikeDislikeRepository.
                    findById(reviewRoleLikeDislike1.getId()).get();
            Assertions.assertThat(reviewRoleLikeDislike2).isEqualToIgnoringGivenFields(read,
                    "registeredUser", "reviewRole", "createdAt", "updatedAt");
        });
    }

    @Test
    public void testUpdateRegisteredUserReviewRole() {

        ReviewRoleLikeDislike reviewRole1 = testObjectsFactory.createReviewRoleLikeDislike();
        ReviewRoleLikeDislikePutDTO put = new ReviewRoleLikeDislikePutDTO();
        put.setIsLike(true);

        RegisteredUser registeredUser1 = reviewRole1.getRegisteredUser();
        ReviewRoleLikeDislike reviewRole = transactionTemplate.execute(status -> {
            return registeredUserRepository.findById(registeredUser1.getId()).get().
                    getReviewRoleLikeDislikes().get(0);
        });

        ReviewRoleLikeDislikeReadDTO read = registeredUserReviewRoleLikeService.
                updateRegisteredUserReviewRoleLike(registeredUser1.getId(),
                        reviewRole.getId(), put);

        inTransaction(()-> {
            Assertions.assertThat(put).isEqualToComparingFieldByField(read);
            ReviewRoleLikeDislike reviewRoleLikeDislike = reviewRoleLikeDislikeRepository.findById(read.getId()).get();
            Assertions.assertThat(reviewRoleLikeDislike).isEqualToIgnoringGivenFields(read,
                    "registeredUser", "reviewRole", "createdAt", "updatedAt");
        });
    }

    @Test
    public void testDeleteRegisteredUserReviewRole() {

        ReviewRoleLikeDislike roleLikeDislike = testObjectsFactory.createReviewRoleLikeDislike();
        RegisteredUser registeredUser = roleLikeDislike.getRegisteredUser();
        UUID reviewRoleLikeDislikeUUID = roleLikeDislike.getId();

        registeredUserReviewRoleLikeService.deleteRegisteredUserReviewRoleLike(registeredUser.getId(),
                reviewRoleLikeDislikeUUID);
        Assert.assertFalse(reviewRoleLikeDislikeRepository.existsById(reviewRoleLikeDislikeUUID));
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }
}
