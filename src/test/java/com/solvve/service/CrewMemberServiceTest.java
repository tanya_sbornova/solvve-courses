package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.Department;
import com.solvve.domain.Film;
import com.solvve.exception.LinkDuplicatedException;
import com.solvve.repository.DepartmentRepository;
import com.solvve.repository.FilmRepository;
import com.solvve.util.TestObjectsFactory;
import com.solvve.domain.CrewMember;
import com.solvve.dto.crewmember.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.CrewMemberRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.*;
import java.util.stream.Collectors;

public class CrewMemberServiceTest extends BaseTest {

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private CrewMemberService crewMemberService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void getCrewMembers() {

        List<CrewMember> crewMembers = new ArrayList<>();
        crewMemberRepository.findAll().forEach(crewMembers::add);

        List<UUID> crewMembersId = crewMembers.stream().
                map(CrewMember::getId).collect(Collectors.toList());

        List<CrewMemberReadDTO> crewMembersReadDTO = crewMemberService.getCrewMembers();

        Assertions.assertThat(crewMembersReadDTO).extracting("id").
                containsExactlyInAnyOrder(crewMembersId.toArray());
    }

    @Test
    public void testGetCrewMember() {

        CrewMember crewMember = testObjectsFactory.createCrewMember();

        CrewMemberReadDTO readDTO = crewMemberService.getCrewMember(crewMember.getId());

        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(crewMember,
                "personId", "departmentId");
        Assert.assertEquals(readDTO.getPersonId(), crewMember.getPerson().getId());
    }

    @Test
    public void testGetSimpleCrewMember() {

        CrewMember crewMember = testObjectsFactory.createCrewMember();

        CrewMemberReadDTO readDTO = crewMemberService.getCrewMember(crewMember.getId());

        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(crewMember,
                "personId", "departmentId");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetCrewMemberWrongId() {
        crewMemberService.getCrewMember(UUID.randomUUID());
    }

    @Test
    public void getDepartmentCrewMembers() {

        Department department = testObjectsFactory.createDepartment();

        Set<UUID> rolesId = transactionTemplate.execute(status -> {
            Department department1 = departmentRepository.findById(department.getId()).get();
            return department1.getCrewMembers().stream().
                    map(x -> x.getId()).collect(Collectors.toSet());
        });

        List<CrewMemberReadDTO> crewMembersReadDTO = crewMemberService.getDepartmentCrewMembers(department.getId());

        Assertions.assertThat(crewMembersReadDTO).extracting("id").
                containsExactlyInAnyOrder(rolesId.toArray());
    }

    @Test
    public void testCreateCrewMember() {

        CrewMemberCreateDTO create = testObjectsFactory.createCrewMemberCreateDTO();

        CrewMemberReadDTO read = crewMemberService.createCrewMember(create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        CrewMember crewMember = crewMemberRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(crewMember, "personId", "departmentId");
    }

    @Test
    public void testAddCrewMemberToFilm() {

        Film film = testObjectsFactory.createFlatFilm();
        List<CrewMember> crewMembers = List.of(testObjectsFactory.createCrewMember());
        UUID crewMemberId = crewMembers.get(0).getId();

        List<CrewMemberReadDTO> crewMembersReadDTO = crewMemberService.addCrewMemberToFilm(film.getId(), crewMemberId);

        CrewMemberReadDTO expectedRead = new CrewMemberReadDTO();
        expectedRead.setId(crewMemberId);
        expectedRead.setCrewMemberType(crewMembers.get(0).getCrewMemberType());
        expectedRead.setPersonId(crewMembers.get(0).getPerson().getId());
        expectedRead.setDepartmentId(crewMembers.get(0).getDepartment().getId());
        expectedRead.setCreatedAt(crewMembers.get(0).getCreatedAt());
        expectedRead.setUpdatedAt(crewMembers.get(0).getUpdatedAt());
        Assertions.assertThat(crewMembersReadDTO).containsExactlyInAnyOrder(expectedRead);

        inTransaction(() -> {
            Film filmAfterSave = filmRepository.findById(film.getId()).get();
            Assertions.assertThat(filmAfterSave.getCrewMembers()).extracting(CrewMember::getId)
                    .containsExactlyInAnyOrder(crewMemberId);
        });
    }

    @Test
    public void testAddCrewMemberToDepartment() {

            Department department = testObjectsFactory.createDepartment();
            CrewMember crewMember = testObjectsFactory.createCrewMember();
            UUID crewMemberId = crewMember.getId();

            List<CrewMemberReadDTO> crewMembersReadDTO = crewMemberService
                    .addCrewMemberToDepartment(department.getId(), crewMemberId);

            CrewMemberReadDTO expectedRead = new CrewMemberReadDTO();
            expectedRead.setId(crewMemberId);
            expectedRead.setCrewMemberType(crewMember.getCrewMemberType());
            expectedRead.setPersonId(crewMember.getPerson().getId());
            expectedRead.setDepartmentId(department.getId());
            expectedRead.setCreatedAt(crewMember.getCreatedAt());
            expectedRead.setUpdatedAt(crewMember.getUpdatedAt());
            Assertions.assertThat(crewMembersReadDTO).containsExactlyInAnyOrder(expectedRead);

            inTransaction(() -> {
                Department departmentAfterSave = departmentRepository.findById(department.getId()).get();
                Assertions.assertThat(departmentAfterSave.getCrewMembers()).extracting(CrewMember::getId)
                        .containsExactlyInAnyOrder(crewMemberId);
            });
    }

    @Test
    public void testDuplicatedCrewMember() {

        Film film = testObjectsFactory.createFlatFilm();
        List<CrewMember> crewMembers = List.of(testObjectsFactory.createCrewMember());
        UUID crewMemberId = crewMembers.get(0).getId();

        crewMemberService.addCrewMemberToFilm(film.getId(), crewMemberId);
        Assertions.assertThatThrownBy(() -> {
            crewMemberService.addCrewMemberToFilm(film.getId(), crewMemberId);
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongFilmId() {

        UUID wrongFilmId = UUID.randomUUID();
        List<CrewMember> crewMembers = List.of(testObjectsFactory.createCrewMember(),
                testObjectsFactory.createCrewMember());
        UUID crewMemberId = crewMembers.get(0).getId();

        crewMemberService.addCrewMemberToFilm(wrongFilmId, crewMemberId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongCrewMemberId() {

        Film film = testObjectsFactory.createFlatFilm();
        UUID wrongCrewMemberId = UUID.randomUUID();
        crewMemberService.addCrewMemberToFilm(film.getId(), wrongCrewMemberId);
    }

    @Test
    public void testPatchCrewMember() {

        CrewMember crewMember = testObjectsFactory.createCrewMember();
        CrewMemberPatchDTO patch = testObjectsFactory.createCrewMemberPatchDTO();
        CrewMemberReadDTO read = crewMemberService.patchCrewMember(crewMember.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        crewMember = crewMemberRepository.findById(read.getId()).get();
        Assertions.assertThat(crewMember).isEqualToIgnoringGivenFields(read,
                "person", "department", "films");
        Assert.assertEquals(read.getPersonId(), crewMember.getPerson().getId());
    }

    @Test
    public void testPatchCrewMemberEmptyPatch() {

        CrewMember crewMember = testObjectsFactory.createCrewMember();
        CrewMemberPatchDTO patch = new CrewMemberPatchDTO();
        CrewMemberReadDTO read = crewMemberService.patchCrewMember(crewMember.getId(), patch);

        Assert.assertNotNull(read.getCrewMemberType());

        CrewMember crewMemberAfterUpdate = crewMemberRepository.findById(read.getId()).get();

        Assert.assertNotNull(crewMemberAfterUpdate.getCrewMemberType());
    }

    @Test
    public void testUpdateCrewMember() {

        CrewMember crewMember = testObjectsFactory.createCrewMember();
        CrewMemberPutDTO put = testObjectsFactory.createCrewMemberPutDTO();
        CrewMemberReadDTO read = crewMemberService.updateCrewMember(crewMember.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        crewMember = crewMemberRepository.findById(read.getId()).get();
        Assertions.assertThat(crewMember).isEqualToIgnoringGivenFields(read,
                "person", "department", "films");
        Assert.assertEquals(read.getPersonId(), crewMember.getPerson().getId());
    }

    @Test
    public void testDeleteCrewMember() {

        CrewMember crewMember = testObjectsFactory.createCrewMember();

        crewMemberService.deleteCrewMember(crewMember.getId());
        Assert.assertFalse(crewMemberRepository.existsById(crewMember.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteCrewMemberNotFound() {

        crewMemberService.deleteCrewMember(UUID.randomUUID());
    }

    @Test
    public void testRemoveCrewMemberFromFilm() {

        Film film = testObjectsFactory.createFlatFilm();
        List<CrewMember> crewMembers = List.of(testObjectsFactory.createCrewMember(),
                testObjectsFactory.createCrewMember());
        UUID crewMemberId = crewMembers.get(0).getId();
        crewMemberService.addCrewMemberToFilm(film.getId(), crewMemberId);

        List<CrewMemberReadDTO> remainingCrewMembers = crewMemberService
                .removeCrewMemberFromFilm(film.getId(), crewMemberId);
        Assert.assertTrue(remainingCrewMembers.isEmpty());

        inTransaction(() -> {
            Film filmAfterRemove = filmRepository.findById(film.getId()).get();
            Assertions.assertThat(filmAfterRemove.getCrewMembers().isEmpty());
        });
    }

    @Test
    public void testRemoveCrewMemberFromDepartment() {

        Department department = testObjectsFactory.createDepartment();
        List<CrewMember> crewMembers = List.of(testObjectsFactory.createCrewMember(),
                testObjectsFactory.createCrewMember());
        UUID crewMemberId = crewMembers.get(0).getId();
        crewMemberService.addCrewMemberToDepartment(department.getId(), crewMemberId);

        List<CrewMemberReadDTO> remainingCrewMembers = crewMemberService
                .removeCrewMemberFromDepartment(department.getId(), crewMemberId);
        Assert.assertTrue(remainingCrewMembers.isEmpty());

        inTransaction(() -> {
            Department departmentAfterRemove = departmentRepository.findById(department.getId()).get();
            Assertions.assertThat(departmentAfterRemove.getCrewMembers().isEmpty());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedCrewMember() {

        Film film = testObjectsFactory.createFlatFilm();
        List<CrewMember> crewMembers = List.of(testObjectsFactory.createCrewMember(),
                testObjectsFactory.createCrewMember());
        UUID crewMemberId = crewMembers.get(0).getId();

        crewMemberService.removeCrewMemberFromFilm(film.getId(), crewMemberId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedCrewMember() {

        Film film = testObjectsFactory.createFlatFilm();
        crewMemberService.removeCrewMemberFromFilm(film.getId(), UUID.randomUUID());
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveCrewMemberValidation() {

        CrewMember crewMember = new CrewMember();
        crewMemberRepository.save(crewMember);
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }
}
