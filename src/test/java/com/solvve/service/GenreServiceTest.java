package com.solvve.service;

import com.solvve.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.FilmGenre;
import com.solvve.domain.Genre;
import com.solvve.dto.genre.GenreCreateDTO;
import com.solvve.dto.genre.GenrePatchDTO;
import com.solvve.dto.genre.GenrePutDTO;
import com.solvve.dto.genre.GenreReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.LinkDuplicatedException;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.GenreRepository;
import com.solvve.util.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class GenreServiceTest extends BaseTest {

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private GenreService genreService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testGetGenre() {

        Genre genre = testObjectsFactory.createGenre(FilmGenre.DRAMA, new Film());

        GenreReadDTO readDTO = genreService.getGenre(genre.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(genre);
    }

    @Test
    public void getGenres() {

        List<Genre> genres = new ArrayList<>();
        genreRepository.findAll().forEach(genres::add);

        List<UUID> genresId = genres.stream().
                map(x -> x.getId()).collect(Collectors.toList());

        List<GenreReadDTO> genresReadDTO = genreService.getGenres();

        Assertions.assertThat(genresReadDTO).extracting("id").
                containsExactlyInAnyOrder(genresId.toArray());
    }

    @Test
    public void getFilmGenres() {

        Film film = testObjectsFactory.createFilm();

        Set<UUID> genresId = transactionTemplate.execute(status -> {
            Film film1 = filmRepository.findById(film.getId()).get();
            return film1.getGenres().stream().
                    map(x -> x.getId()).collect(Collectors.toSet());
        });

        List<GenreReadDTO> genresReadDTO = genreService.getFilmGenres(film.getId());

        Assertions.assertThat(genresReadDTO).extracting("id").
                containsExactlyInAnyOrder(genresId.toArray());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetGenreWrongId() {

        genreService.getGenre(UUID.randomUUID());
    }

    @Test
    public void testCreateGenre() {

        GenreCreateDTO create = new GenreCreateDTO();
        create.setName(FilmGenre.DRAMA);

        GenreReadDTO read = genreService.createGenre(create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Genre genre = genreRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(genre);
    }

    @Test
    public void testAddGenreToFilm() {

        Film film = testObjectsFactory.createFlatFilm();
        List<Genre> genres = new ArrayList<>();
        genreRepository.findAll().forEach(genres::add);
        UUID genreId = genres.get(0).getId();

        List<GenreReadDTO> genresReadDTO = genreService.addGenreToFilm(film.getId(), genreId);

        GenreReadDTO expectedRead = new GenreReadDTO();
        expectedRead.setId(genreId);
        expectedRead.setName(genres.get(0).getName());
        expectedRead.setCreatedAt(genres.get(0).getCreatedAt());
        expectedRead.setUpdatedAt(genres.get(0).getUpdatedAt());
        Assertions.assertThat(genresReadDTO).containsExactlyInAnyOrder(expectedRead);

        inTransaction(() -> {
            Film filmAfterSave = filmRepository.findById(film.getId()).get();
            Assertions.assertThat(filmAfterSave.getGenres()).extracting(Genre::getId)
                    .containsExactlyInAnyOrder(genreId);
        });
    }

    @Test
    public void testDuplicatedGenre() {

        Film film = testObjectsFactory.createFlatFilm();
        List<Genre> genres = new ArrayList<>();
        genreRepository.findAll().forEach(genres::add);
        UUID genreId = genres.get(0).getId();

        genreService.addGenreToFilm(film.getId(), genreId);
        Assertions.assertThatThrownBy(() -> {
            genreService.addGenreToFilm(film.getId(), genreId);
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongFilmId() {

        UUID wrongFilmId = UUID.randomUUID();
        List<Genre> genres = new ArrayList<>();
        genreRepository.findAll().forEach(genres::add);
        UUID genreId = genres.get(0).getId();

        genreService.addGenreToFilm(wrongFilmId, genreId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongGenreId() {

        Film film = testObjectsFactory.createFlatFilm();
        UUID wrongGenreId = UUID.randomUUID();
        genreService.addGenreToFilm(film.getId(), wrongGenreId);
    }

    @Test
    public void testPatchGenre() {

        Genre genre = testObjectsFactory.createGenre(FilmGenre.DRAMA, new Film());

        GenrePatchDTO patch = new GenrePatchDTO();
        patch.setName(FilmGenre.DRAMA);

        GenreReadDTO read = genreService.patchGenre(genre.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        genre = genreRepository.findById(read.getId()).get();
        Assertions.assertThat(genre).isEqualToIgnoringGivenFields(read, "films");
    }

    @Test
    public void testPatchGenreEmptyPatch() {

        Genre genre = testObjectsFactory.createGenre(FilmGenre.DRAMA, new Film());

        GenrePatchDTO patch = new GenrePatchDTO();
        GenreReadDTO read = genreService.patchGenre(genre.getId(), patch);

        Assert.assertNotNull(read.getName());

        Genre genreAfterUpdate = genreRepository.findById(read.getId()).get();

        Assert.assertNotNull(genreAfterUpdate.getName());

        Assertions.assertThat(genre).isEqualToIgnoringGivenFields(genreAfterUpdate, "films");
    }

    @Test
    public void testUpdateGenre() {

        Genre genre = testObjectsFactory.createGenre(FilmGenre.DRAMA, new Film());

        GenrePutDTO put = new GenrePutDTO();
        put.setName(FilmGenre.DRAMA);

        GenreReadDTO read = genreService.updateGenre(genre.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        genre = genreRepository.findById(read.getId()).get();
        Assertions.assertThat(genre).isEqualToIgnoringGivenFields(read, "films");
    }

    @Test
    public void testDeleteGenre() {

        Genre genre = testObjectsFactory.createGenre(FilmGenre.DRAMA, new Film());

        genreService.deleteGenre(genre.getId());
        Assert.assertFalse(genreRepository.existsById(genre.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteGenreNotFound() {

        genreService.deleteGenre(UUID.randomUUID());
    }

    @Test
    public void testRemoveGenreFromFilm() {

        Film film = testObjectsFactory.createFlatFilm();
        List<Genre> genres = new ArrayList<>();
        genreRepository.findAll().forEach(genres::add);
        UUID genreId = genres.get(0).getId();
        genreService.addGenreToFilm(film.getId(), genreId);

        List<GenreReadDTO> remainingGenres = genreService.removeGenreFromFilm(film.getId(), genreId);
        Assert.assertTrue(remainingGenres.isEmpty());

        inTransaction(() -> {
            Film filmAfterRemove = filmRepository.findById(film.getId()).get();
            Assertions.assertThat(filmAfterRemove.getGenres().isEmpty());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedGenre() {

        Film film = testObjectsFactory.createFlatFilm();
        List<Genre> genres = new ArrayList<>();
        genreRepository.findAll().forEach(genres::add);
        UUID genreId = genres.get(0).getId();

        genreService.removeGenreFromFilm(film.getId(), genreId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedGenre() {

        Film film = testObjectsFactory.createFlatFilm();
        genreService.removeGenreFromFilm(film.getId(), UUID.randomUUID());
    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveGenreValidation() {

        Genre genre = new Genre();
        genreRepository.save(genre);
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.executeWithoutResult(status -> {
            runnable.run();
        });
    }
}


