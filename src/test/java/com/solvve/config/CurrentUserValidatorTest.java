package com.solvve.config;

import com.solvve.BaseTest;
import com.solvve.domain.RegisteredUser;
import com.solvve.domain.UserStatus;
import com.solvve.repository.RegisteredUserRepository;
import com.solvve.security.AuthenticationResolver;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;

public class CurrentUserValidatorTest extends BaseTest {

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private CurrentUserValidator currentUserValidator;

    @MockBean
    private AuthenticationResolver authenticationResolver;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void testIsCurrentUser() {

        RegisteredUser user = new RegisteredUser();
        user.setEmail("test@gmail.com");
        user.setEncodedPassword(passwordEncoder.encode("pass123"));
        user.setUserStatus(UserStatus.ACTIVE);
        user = registeredUserRepository.save(user);

        Authentication authentication = new TestingAuthenticationToken(user.getEmail(), null);
        Mockito.when(authenticationResolver.getCurrentAuthentication()).thenReturn(authentication);
        Assert.assertTrue(currentUserValidator.isCurrentUser(user.getId()));
    }

    @Test
    public void testIsDifferentUser() {

        RegisteredUser user1 = new RegisteredUser();
        user1.setEmail("test@gmail.com");
        user1.setEncodedPassword(passwordEncoder.encode("pass123"));
        user1.setUserStatus(UserStatus.ACTIVE);
        user1 = registeredUserRepository.save(user1);

        RegisteredUser user2 = new RegisteredUser();
        user2.setEmail("test2@gmail.com");
        user2.setEncodedPassword(passwordEncoder.encode("pass1234"));
        user2.setUserStatus(UserStatus.ACTIVE);
        user2 = registeredUserRepository.save(user2);

        Authentication authentication = new TestingAuthenticationToken(user1.getEmail(), null);
        Mockito.when(authenticationResolver.getCurrentAuthentication()).thenReturn(authentication);
        Assert.assertFalse(currentUserValidator.isCurrentUser(user2.getId()));
    }
}
