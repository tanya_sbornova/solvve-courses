package com.solvve.util;

import com.solvve.domain.MisprintStatus;
import com.solvve.dto.actor.ActorCreateDTO;
import com.solvve.dto.actor.ActorInLeaderBoardReadDTO;
import com.solvve.dto.actor.ActorReadDTO;
import com.solvve.dto.actor.ActorReadExtendedDTO;
import com.solvve.dto.admin.AdminCreateDTO;
import com.solvve.dto.admin.AdminReadDTO;
import com.solvve.dto.company.CompanyCreateDTO;
import com.solvve.dto.company.CompanyReadDTO;
import com.solvve.dto.contentmanager.ContentManagerCreateDTO;
import com.solvve.dto.contentmanager.ContentManagerReadDTO;
import com.solvve.dto.crewmember.CrewMemberCreateDTO;
import com.solvve.dto.crewmember.CrewMemberPatchDTO;
import com.solvve.dto.crewmember.CrewMemberPutDTO;
import com.solvve.dto.crewmember.CrewMemberReadDTO;
import com.solvve.dto.department.DepartmentCreateDTO;
import com.solvve.dto.department.DepartmentReadDTO;
import com.solvve.dto.film.FilmInLeaderBoardReadDTO;
import com.solvve.dto.film.FilmReadDTO;
import com.solvve.dto.film.FilmReadExtendedDTO;
import com.solvve.dto.genre.GenreReadDTO;
import com.solvve.dto.misprintinfofilm.MisprintInfoFilmCreateDTO;
import com.solvve.dto.misprintinfofilm.MisprintInfoFilmReadDTO;
import com.solvve.dto.misprintinfofix.MisprintInfoFixReadDTO;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsCreateDTO;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsReadDTO;
import com.solvve.dto.misprintinforole.MisprintInfoRoleCreateDTO;
import com.solvve.dto.misprintinforole.MisprintInfoRoleReadDTO;
import com.solvve.dto.moderator.ModeratorCreateDTO;
import com.solvve.dto.moderator.ModeratorReadDTO;
import com.solvve.dto.news.NewsCreateDTO;
import com.solvve.dto.news.NewsPatchDTO;
import com.solvve.dto.news.NewsPutDTO;
import com.solvve.dto.news.NewsReadDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikeCreateDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikePatchDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikePutDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikeReadDTO;
import com.solvve.dto.person.PersonCreateDTO;
import com.solvve.dto.person.PersonPatchDTO;
import com.solvve.dto.person.PersonPutDTO;
import com.solvve.dto.person.PersonReadDTO;
import com.solvve.dto.problemreview.ProblemReviewCreateDTO;
import com.solvve.dto.problemreview.ProblemReviewPatchDTO;
import com.solvve.dto.problemreview.ProblemReviewPutDTO;
import com.solvve.dto.problemreview.ProblemReviewReadDTO;
import com.solvve.dto.ratingfilm.RatingFilmCreateDTO;
import com.solvve.dto.ratingfilm.RatingFilmPatchDTO;
import com.solvve.dto.ratingfilm.RatingFilmPutDTO;
import com.solvve.dto.ratingfilm.RatingFilmReadDTO;
import com.solvve.dto.ratingrole.RatingRoleCreateDTO;
import com.solvve.dto.ratingrole.RatingRolePatchDTO;
import com.solvve.dto.ratingrole.RatingRolePutDTO;
import com.solvve.dto.ratingrole.RatingRoleReadDTO;
import com.solvve.dto.registereduser.RegisteredUserCreateDTO;
import com.solvve.dto.registereduser.RegisteredUserReadDTO;
import com.solvve.dto.reviewfilm.ReviewFilmCreateDTO;
import com.solvve.dto.reviewfilm.ReviewFilmPatchDTO;
import com.solvve.dto.reviewfilm.ReviewFilmPutDTO;
import com.solvve.dto.reviewfilm.ReviewFilmReadDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeCreateDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePatchDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePutDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeReadDTO;
import com.solvve.dto.reviewrole.ReviewRoleCreateDTO;
import com.solvve.dto.reviewrole.ReviewRolePatchDTO;
import com.solvve.dto.reviewrole.ReviewRolePutDTO;
import com.solvve.dto.reviewrole.ReviewRoleReadDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikeCreateDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePatchDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePutDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikeReadDTO;
import com.solvve.dto.role.RolePatchDTO;
import com.solvve.dto.role.RolePutDTO;
import com.solvve.dto.role.RoleReadDTO;
import org.bitbucket.brunneng.br.Configuration;
import org.bitbucket.brunneng.br.RandomObjectGenerator;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

@Component
public class TestObjectsFactoryController {

    public CompanyCreateDTO createCompanyCreateDTO() {
        return generateObject(CompanyCreateDTO.class);
    }

    public CompanyReadDTO createCompanyReadDTO() {
        return generateObject(CompanyReadDTO.class);
    }

    public CrewMemberReadDTO createCrewMemberReadDTO() {
        return generateObject(CrewMemberReadDTO.class);
    }

    public CrewMemberPatchDTO createCrewMemberPatchDTO() {
        return generateObject(CrewMemberPatchDTO.class);
    }

    public CrewMemberPutDTO createCrewMemberPutDTO() {
        return generateObject(CrewMemberPutDTO.class);
    }

    public CrewMemberCreateDTO createCrewMemberCreateDTO() {
        return generateObject(CrewMemberCreateDTO.class);
    }

    public ActorCreateDTO createActorCreateDTO() {
        return generateObject(ActorCreateDTO.class);
    }

    public DepartmentCreateDTO createDepartmentCreateDTO() {
        return generateObject(DepartmentCreateDTO.class);
    }

    public DepartmentReadDTO createDepartmentReadDTO() {
        return generateObject(DepartmentReadDTO.class);
    }

    public PersonCreateDTO createPersonCreateDTO() {
        return generateObject(PersonCreateDTO.class);
    }

    public PersonReadDTO createPersonReadDTO() {
        return generateObject(PersonReadDTO.class);
    }

    public PersonPatchDTO createPersonPatchDTO() {
        return generateObject(PersonPatchDTO.class);
    }

    public PersonPutDTO createPersonPutDTO() {
        return generateObject(PersonPutDTO.class);
    }

    public RoleReadDTO createRoleReadDTO() {
        return generateObject(RoleReadDTO.class);
    }

    public RolePatchDTO createRolePatchDTO() {
        return generateObject(RolePatchDTO.class);
    }

    public RolePutDTO createRolePutDTO() {
        return generateObject(RolePutDTO.class);
    }

    public ActorReadDTO createActorReadDTO() {
        return generateObject(ActorReadDTO.class);
    }

    public ActorReadExtendedDTO createActorReadExtendedDTO() {
        return generateObject(ActorReadExtendedDTO.class);
    }

    public RatingFilmCreateDTO createRatingFilmCreateDTO() {
        return generateObject(RatingFilmCreateDTO.class);
    }

    public RatingFilmReadDTO createRatingFilmReadDTO() {
        return generateObject(RatingFilmReadDTO.class);
    }

    public RatingFilmPatchDTO createRatingFilmPatchDTO() {
        return generateObject(RatingFilmPatchDTO.class);
    }

    public RatingFilmPutDTO createRatingFilmPutDTO() {
        return generateObject(RatingFilmPutDTO.class);
    }

    public FilmReadDTO createFilmReadDTO() {
        return generateObject(FilmReadDTO.class);
    }

    public FilmReadExtendedDTO createFilmReadExtendedDTO() {

        return generateObject(FilmReadExtendedDTO.class);
    }

    public RatingRoleCreateDTO createRatingRoleCreateDTO() {
        return generateObject(RatingRoleCreateDTO.class);
    }

    public RatingRoleReadDTO createRatingRoleReadDTO() {
        return generateObject(RatingRoleReadDTO.class);
    }

    public RatingRolePatchDTO createRatingRolePatchDTO() {
        return generateObject(RatingRolePatchDTO.class);
    }

    public RatingRolePutDTO createRatingRolePutDTO() {
        return generateObject(RatingRolePutDTO.class);
    }

    public ReviewFilmCreateDTO createReviewFilmCreateDTO() {
        return generateObject(ReviewFilmCreateDTO.class);
    }

    public ReviewFilmReadDTO createReviewFilmReadDTO() {
        return generateObject(ReviewFilmReadDTO.class);
    }

    public ReviewFilmPatchDTO createReviewFilmPatchDTO() {
        return generateObject(ReviewFilmPatchDTO.class);
    }

    public ReviewFilmPutDTO createReviewFilmPutDTO() {
        return generateObject(ReviewFilmPutDTO.class);
    }

    public ReviewRoleCreateDTO createReviewRoleCreateDTO() {
        return generateObject(ReviewRoleCreateDTO.class);
    }

    public ReviewRoleReadDTO createReviewRoleReadDTO() {
        return generateObject(ReviewRoleReadDTO.class);
    }

    public ReviewRolePatchDTO createReviewRolePatchDTO() {
        return generateObject(ReviewRolePatchDTO.class);
    }

    public ReviewRolePutDTO createReviewRolePutDTO() {
        return generateObject(ReviewRolePutDTO.class);
    }

    public RegisteredUserReadDTO createRegisteredUserReadDTO() {
        return generateObject(RegisteredUserReadDTO.class);
    }

    public NewsCreateDTO createNewsCreateDTO() {
        return generateObject(NewsCreateDTO.class);
    }

    public NewsReadDTO createNewsReadDTO() {
        return generateObject(NewsReadDTO.class);
    }

    public NewsPutDTO createNewsPutDTO() {
        return generateObject(NewsPutDTO.class);
    }

    public NewsPatchDTO createNewsPatchDTO() {
        return generateObject(NewsPatchDTO.class);
    }


    public NewsLikeDislikeCreateDTO createNewsLikeDislikeCreateDTO() {
        return generateObject(NewsLikeDislikeCreateDTO.class);
    }

    public NewsLikeDislikeReadDTO createNewsLikeDislikeReadDTO() {
        return generateObject(NewsLikeDislikeReadDTO.class);
    }

    public NewsLikeDislikePutDTO createNewsLikeDislikePutDTO() {
        return generateObject(NewsLikeDislikePutDTO.class);
    }

    public NewsLikeDislikePatchDTO createNewsLikeDislikePatchDTO() {
        return generateObject(NewsLikeDislikePatchDTO.class);
    }

    public ReviewFilmLikeDislikeReadDTO createReviewFilmLikeDislikeReadDTO() {
        return generateObject(ReviewFilmLikeDislikeReadDTO.class);
    }

    public ReviewFilmLikeDislikeCreateDTO createReviewFilmLikeDislikeCreateDTO() {
        return generateObject(ReviewFilmLikeDislikeCreateDTO.class);
    }

    public ReviewFilmLikeDislikePutDTO createReviewFilmLikeDislikePutDTO() {
        return generateObject(ReviewFilmLikeDislikePutDTO.class);
    }

    public ReviewFilmLikeDislikePatchDTO createReviewFilmLikeDislikePatchDTO() {
        return generateObject(ReviewFilmLikeDislikePatchDTO.class);
    }

    public ReviewRoleLikeDislikeCreateDTO createReviewRoleLikeDislikeCreateDTO() {
        return generateObject(ReviewRoleLikeDislikeCreateDTO.class);
    }

    public ReviewRoleLikeDislikeReadDTO createReviewRoleLikeDislikeReadDTO() {
        return generateObject(ReviewRoleLikeDislikeReadDTO.class);
    }

    public ReviewRoleLikeDislikePutDTO createReviewRoleLikeDislikePutDTO() {
        return generateObject(ReviewRoleLikeDislikePutDTO.class);
    }

    public ReviewRoleLikeDislikePatchDTO createReviewRoleLikeDislikePatchDTO() {
        return generateObject(ReviewRoleLikeDislikePatchDTO.class);
    }

    public ProblemReviewReadDTO createProblemFilmReviewReadDTO() {
        return generateObject(ProblemReviewReadDTO.class);
    }

    public ProblemReviewCreateDTO createProblemFilmReviewCreateDTO() {
        return generateObject(ProblemReviewCreateDTO.class);
    }

    public ProblemReviewReadDTO createProblemReviewReadDTO() {
        return generateObject(ProblemReviewReadDTO.class);
    }

    public ProblemReviewPatchDTO createProblemReviewPatchDTO() {
        return generateObject(ProblemReviewPatchDTO.class);
    }

    public ProblemReviewPutDTO createProblemReviewPutDTO() {
        return generateObject(ProblemReviewPutDTO.class);
    }

    public MisprintInfoFilmReadDTO createMisprintInfoFilmReadDTO() {
        return generateObject(MisprintInfoFilmReadDTO.class);
    }

    public MisprintInfoNewsReadDTO createMisprintInfoNewsReadDTO() {
        return generateObject(MisprintInfoNewsReadDTO.class);
    }

    public MisprintInfoFilmCreateDTO createMisprintInfoFilmCreateDTO() {
        return generateObject(MisprintInfoFilmCreateDTO.class);
    }

    public MisprintInfoNewsCreateDTO createMisprintInfoNewsCreateDTO() {
        return generateObject(MisprintInfoNewsCreateDTO.class);
    }

    public MisprintInfoRoleCreateDTO createMisprintInfoRoleCreateDTO() {
        return generateObject(MisprintInfoRoleCreateDTO.class);
    }

    public MisprintInfoRoleReadDTO createMisprintInfoRoleReadDTO() {
        return generateObject(MisprintInfoRoleReadDTO.class);
    }

    public MisprintInfoFixReadDTO createMPInfoFilmReadDTOWithParam(UUID filmID,
                                                                   Integer startIndex, Integer endIndex,
                                                                   String text,
                                                                   UUID registeredUserIdID,
                                                                   UUID contentManagerIdID) {
        MisprintInfoFixReadDTO misprintInfo = new MisprintInfoFixReadDTO();
        misprintInfo.setId(UUID.randomUUID());
        misprintInfo.setStartIndex(startIndex);
        misprintInfo.setEndIndex(endIndex);
        misprintInfo.setText(text);
        misprintInfo.setStatus(MisprintStatus.FIXED);
        misprintInfo.setContentId(filmID);
        misprintInfo.setRegisteredUserId(registeredUserIdID);
        misprintInfo.setContentManagerId(contentManagerIdID);
        return misprintInfo;
    }

    public MisprintInfoFixReadDTO createMPInfoNewsReadDTOWithParam(UUID newsID,
                                                                   Integer startIndex, Integer endIndex,
                                                                   String text,
                                                                   UUID registeredUserIdID,
                                                                   UUID contentManagerIdID) {
        MisprintInfoFixReadDTO misprintInfo = new MisprintInfoFixReadDTO();
        misprintInfo.setId(UUID.randomUUID());
        misprintInfo.setStartIndex(startIndex);
        misprintInfo.setEndIndex(endIndex);
        misprintInfo.setText(text);
        misprintInfo.setStatus(MisprintStatus.FIXED);
        misprintInfo.setContentId(newsID);
        misprintInfo.setRegisteredUserId(registeredUserIdID);
        misprintInfo.setContentManagerId(contentManagerIdID);
        return misprintInfo;
    }

    public AdminCreateDTO createAdminCreateDTO() {
        AdminCreateDTO create = generateObject(AdminCreateDTO.class);
        create.setStartAt(LocalDateTime.of(2020, 1, 10, 11, 0, 0)
                .toInstant(ZoneOffset.UTC));
        create.setFinishAt(create.getStartAt().plus(30, ChronoUnit.MINUTES));
        return create;
    }

    public AdminReadDTO createAdminReadDTO() {
        return generateObject(AdminReadDTO.class);
    }

    public ContentManagerCreateDTO createContentManagerCreateDTO() {
        return generateObject(ContentManagerCreateDTO.class);
    }

    public ContentManagerReadDTO createContentManagerReadDTO() {
        return generateObject(ContentManagerReadDTO.class);
    }

    public ModeratorCreateDTO createModeratorCreateDTO() {
        return generateObject(ModeratorCreateDTO.class);
    }

    public ModeratorReadDTO createModeratorReadDTO() {
        return generateObject(ModeratorReadDTO.class);
    }

    public RegisteredUserCreateDTO createRegisteredUserCreateDTO() {
        return generateObject(RegisteredUserCreateDTO.class);
    }

    public GenreReadDTO createGenreReadDTO() {
        return generateObject(GenreReadDTO.class);
    }

    public FilmInLeaderBoardReadDTO createFilmInLeaderBoardReadDTO() {
        return generateObject(FilmInLeaderBoardReadDTO.class);
    }

    public ActorInLeaderBoardReadDTO createActorInLeaderBoardReadDTO() {
        return generateObject(ActorInLeaderBoardReadDTO.class);
    }

    public <T> T generateObject(Class<T> objectClass) {
        return generator.generateRandomObject(objectClass);
    }

    private RandomObjectGenerator generator;

    {
        Configuration c = new Configuration();
        Configuration.Bean bean = c.beanOfClass(RegisteredUserCreateDTO.class);
        Configuration.Bean.Property p = bean.property("trustLevel");
        p.setMinValue(1);
        p.setMaxValue(5);
        Configuration.Bean bean2 = c.beanOfClass(RatingFilmCreateDTO.class);
        Configuration.Bean.Property p2 = bean2.property("rating");
        p2.setMinValue(0);
        p2.setMaxValue(10);
        Configuration.Bean bean3 = c.beanOfClass(RatingRoleCreateDTO.class);
        Configuration.Bean.Property p3 = bean3.property("rating");
        p3.setMinValue(0);
        p3.setMaxValue(10);
        generator = new RandomObjectGenerator(c);
    }

    private Instant createInstant(int year, int month, int day) {
        return LocalDateTime.of(year, month, day, 0, 0).toInstant(ZoneOffset.UTC);
    }

}
