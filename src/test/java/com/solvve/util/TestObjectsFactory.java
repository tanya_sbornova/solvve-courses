package com.solvve.util;

import com.solvve.client.themoviedb.dto.PersonReadDTO;
import com.solvve.domain.*;
import com.solvve.dto.actor.ActorCreateDTO;
import com.solvve.dto.company.CompanyCreateDTO;
import com.solvve.dto.crewmember.CrewMemberCreateDTO;
import com.solvve.dto.crewmember.CrewMemberPatchDTO;
import com.solvve.dto.crewmember.CrewMemberPutDTO;
import com.solvve.dto.department.DepartmentCreateDTO;
import com.solvve.dto.film.FilmCreateDTO;
import com.solvve.dto.misprintinfofilm.MisprintInfoFilmCreateDTO;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsCreateDTO;
import com.solvve.dto.misprintinforole.MisprintInfoRoleCreateDTO;
import com.solvve.dto.news.NewsCreateDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikeCreateDTO;
import com.solvve.dto.person.PersonCreateDTO;
import com.solvve.dto.person.PersonPatchDTO;
import com.solvve.dto.person.PersonPutDTO;
import com.solvve.dto.problemreview.ProblemReviewCreateDTO;
import com.solvve.dto.problemreview.ProblemReviewPatchDTO;
import com.solvve.dto.problemreview.ProblemReviewPutDTO;
import com.solvve.dto.ratingfilm.RatingFilmCreateDTO;
import com.solvve.dto.ratingrole.RatingRoleCreateDTO;
import com.solvve.dto.registereduser.RegisteredUserCreateDTO;
import com.solvve.dto.registereduser.RegisteredUserPatchDTO;
import com.solvve.dto.reviewfilm.ReviewFilmCreateDTO;
import com.solvve.dto.reviewfilm.ReviewFilmPatchDTO;
import com.solvve.dto.reviewfilm.ReviewFilmPutDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeCreateDTO;
import com.solvve.dto.reviewrole.ReviewRoleCreateDTO;
import com.solvve.dto.reviewrole.ReviewRolePatchDTO;
import com.solvve.dto.reviewrole.ReviewRolePutDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikeCreateDTO;
import com.solvve.dto.role.RolePatchDTO;
import com.solvve.dto.role.RolePutDTO;
import com.solvve.repository.*;
import org.bitbucket.brunneng.br.Configuration;
import org.bitbucket.brunneng.br.RandomObjectGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Period;
import java.util.*;

@Component
public class TestObjectsFactory {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ProblemReviewRepository problemReviewRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private RatingFilmRepository ratingFilmRepository;

    @Autowired
    private RatingRoleRepository ratingRoleRepository;

    @Autowired
    private ReviewFilmRepository reviewFilmRepository;

    @Autowired
    private ReviewRoleRepository reviewRoleRepository;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private NewsLikeDislikeRepository newsLikeDislikeRepository;

    @Autowired
    private ReviewFilmLikeDislikeRepository reviewFilmLikeDislikeRepository;

    @Autowired
    private ReviewRoleLikeDislikeRepository reviewRoleLikeDislikeRepository;

    @Autowired
    private MisprintInfoFilmRepository misprintInfoFilmRepository;

    @Autowired
    private MisprintInfoNewsRepository misprintInfoNewsRepository;

    @Autowired
    private MisprintInfoRoleRepository misprintInfoRoleRepository;

    @Autowired
    private MisprintInfoCommonRepository misprintInfoCommonRepository;

    public Admin createAdmin() {
        Admin admin = generateEntityWithoutId(Admin.class);
        return adminRepository.save(admin);
    }

    public Person createPerson() {
        Person person = generateEntityWithoutId(Person.class);
        return personRepository.save(person);
    }

    public Actor createActor(Person person) {
        Actor actor = generateFlatEntityWithoutId(Actor.class);
        actor.setPerson(person);
        actor.setAverageRating(null);
        actor.setAverageRatingByFilm(null);
        return actorRepository.save(actor);
    }

    public Actor createActorWithRating(Person person) {
        Actor actor = generateFlatEntityWithoutId(Actor.class);
        actor.setPerson(person);
        actor.setAverageRating(0.0);
        actor.setAverageRatingByFilm(0.0);
        return actorRepository.save(actor);
    }

    public Genre createGenre(Film film) {
        Genre genre = generateFlatEntityWithoutId(Genre.class);
        genre.setFilms(Set.of(film));
        return genreRepository.save(genre);
    }

    public Genre createGenre(Film film, FilmGenre filmGenre) {
        Genre genre = generateFlatEntityWithoutId(Genre.class);
        genre.setName(filmGenre);
        genre.setFilms(Set.of(film));
        return genreRepository.save(genre);
    }

    public Company createCompany() {
        Company company = generateFlatEntityWithoutId(Company.class);
        company.setFilms(Set.of(createFilm()));
        return companyRepository.save(company);
    }

    public Company createCompany(Film film) {
        Company company = generateFlatEntityWithoutId(Company.class);
        company.setFilms(Set.of(film));
        return companyRepository.save(company);
    }

    public Department createDepartment() {
        Department department = generateFlatEntityWithoutId(Department.class);
        return departmentRepository.save(department);
    }

    public Film createFilm() {
        Film film = generateFlatEntityWithoutId(Film.class);
        film = filmRepository.save(film);
        film.setCrewMembers(Set.of(createCrewMember(film)));
        film.setGenres(Set.of(createGenre(film)));
        film.setCompanies(Set.of(createCompany(film)));
        film.setReviewFilms(List.of(createReviewFilm(film)));
        film.setRatingFilms(List.of(createRatingFilm(film)));
        film.setAverageRating(null);
        film.setForecastRating(null);
        return filmRepository.save(film);
    }

    public Film createFilm(FilmGenre genre) {
        Film film = generateFlatEntityWithoutId(Film.class);
        film = filmRepository.save(film);
        film.setCrewMembers(Set.of(createCrewMember(film)));
        film.setGenres(Set.of(createGenre(film, genre)));
        film.setCompanies(Set.of(createCompany(film)));
        film.setAverageRating(null);
        film.setForecastRating(null);
        return filmRepository.save(film);
    }

    public Film createFlatFilm() {
        Film film = generateFlatEntityWithoutId(Film.class);
        film.setAverageRating(null);
        film.setForecastRating(null);
        return filmRepository.save(film);
    }

    public Film createFlatFilmWithRating() {
        Film film = generateFlatEntityWithoutId(Film.class);
        film.setAverageRating(0.0);
        film.setForecastRating(0.0);
        return filmRepository.save(film);
    }

    public CrewMember createCrewMember() {
        CrewMember crewMember = generateFlatEntityWithoutId(CrewMember.class);
        crewMember.setPerson(createPerson());
        crewMember.setDepartment(createDepartment());
        crewMember.setFilms(Set.of(createFilm()));
        return crewMemberRepository.save(crewMember);
    }

    public CrewMember createCrewMember(Film film) {
        CrewMember crewMember = generateFlatEntityWithoutId(CrewMember.class);
        crewMember.setPerson(createPerson());
        crewMember.setDepartment(createDepartment());
        crewMember.setFilms(Set.of(film));
        return crewMemberRepository.save(crewMember);
    }

    public CrewMember createCrewMember(Person person, CrewMemberType crewMemberType) {
        CrewMember crewMember = generateFlatEntityWithoutId(CrewMember.class);
        crewMember.setPerson(person);
        crewMember.setDepartment(createDepartment());
        crewMember.setCrewMemberType(crewMemberType);
        crewMember.setFilms(Set.of(createFilm()));
        return crewMemberRepository.save(crewMember);
    }

    public Role createFlatRole() {
        Role role = generateFlatEntityWithoutId(Role.class);
        role.setFilm(createFilm());
        role.setActor(createActor(createPerson()));
        role = roleRepository.save(role);
        return role;
    }

    public Role createRole() {
        Role role = generateFlatEntityWithoutId(Role.class);
        role.setFilm(createFilm());
        role.setActor(createActor(createPerson()));
        role = roleRepository.save(role);
        role.setRatingRoles(List.of(createRatingRole(role)));
        role.setReviewRoles(List.of(createReviewRole(role)));
        return roleRepository.save(role);
    }

    public Role createRole(String text, Actor actor) {
        Role role = generateFlatEntityWithoutId(Role.class);
        role.setText(text);
        role.setFilm(createFlatFilm());
        role.setActor(actor);
        return roleRepository.save(role);
    }

    public Role createRole(Actor actor, Film film) {
        Role role = generateFlatEntityWithoutId(Role.class);
        role.setFilm(film);
        role.setActor(actor);
        role = roleRepository.save(role);
        role.setRatingRoles(List.of(createRatingRole(role)));
        role.setReviewRoles(List.of(createReviewRole(role)));
        return roleRepository.save(role);
    }

    public RatingRole createRatingRole(Role role) {
        RatingRole ratingRole = generateFlatEntityWithoutId(RatingRole.class);
        ratingRole.setRole(role);
        ratingRole.setRegisteredUser(createRegisteredUser());
        return ratingRoleRepository.save(ratingRole);
    }

    public RatingRole createRatingRole(Role role, Double rating) {
        RatingRole ratingRole = generateFlatEntityWithoutId(RatingRole.class);
        ratingRole.setRating(rating);
        ratingRole.setRole(role);
        ratingRole.setRegisteredUser(createRegisteredUser());
        return ratingRoleRepository.save(ratingRole);
    }

    public ReviewRole createReviewRole(Role role) {
        ReviewRole reviewRole = generateFlatEntityWithoutId(ReviewRole.class);
        reviewRole.setRole(role);
        reviewRole.setRegisteredUser(createRegisteredUser());
        return reviewRoleRepository.save(reviewRole);
    }

    public RatingFilm createRatingFilm(Film film) {
        RatingFilm ratingFilm = generateFlatEntityWithoutId(RatingFilm.class);
        ratingFilm.setFilm(film);
        ratingFilm.setRegisteredUser(createRegisteredUser());
        return ratingFilmRepository.save(ratingFilm);
    }

    public RatingFilm createRatingFilm(Film film, Double rating) {
        RatingFilm ratingFilm = generateFlatEntityWithoutId(RatingFilm.class);
        ratingFilm.setRating(rating);
        ratingFilm.setFilm(film);
        ratingFilm.setRegisteredUser(createRegisteredUser());
        return ratingFilmRepository.save(ratingFilm);
    }

    public ReviewFilm createReviewFilm(Film film) {
        ReviewFilm reviewFilm = generateFlatEntityWithoutId(ReviewFilm.class);
        reviewFilm.setFilm(film);
        reviewFilm.setRegisteredUser(createRegisteredUser());
        reviewFilm.setStatus(ReviewStatus.FIXED);
        return reviewFilmRepository.save(reviewFilm);
    }

    public ReviewFilm createReviewFilm(ReviewStatus status, Boolean isSpoiler, RegisteredUser regUser, Film film) {
        ReviewFilm rf = generateFlatEntityWithoutId(ReviewFilm.class);
        rf.setStatus(status);
        rf.setIsSpoiler(isSpoiler);
        rf.setRegisteredUser(regUser);
        rf.setFilm(film);
        reviewFilmRepository.save(rf);
        return rf;
    }

    public ReviewRole createReviewRole(ReviewStatus status, Boolean isSpoiler, RegisteredUser regUser, Role role) {
        ReviewRole rr = generateFlatEntityWithoutId(ReviewRole.class);
        rr.setStatus(status);
        rr.setIsSpoiler(isSpoiler);
        rr.setRegisteredUser(regUser);
        rr.setRole(role);
        reviewRoleRepository.save(rr);
        return rr;
    }

    public ReviewFilmLikeDislike createReviewFilmLikeDislike() {
        ReviewFilmLikeDislike reviewFilmLikeDislike = generateFlatEntityWithoutId(ReviewFilmLikeDislike.class);
        reviewFilmLikeDislike.setReviewFilm(createReviewFilm(createFilm()));
        reviewFilmLikeDislike.setRegisteredUser(createRegisteredUser());
        return reviewFilmLikeDislikeRepository.save(reviewFilmLikeDislike);
    }

    public ReviewRoleLikeDislike createReviewRoleLikeDislike() {
        ReviewRoleLikeDislike reviewRoleLikeDislike = generateFlatEntityWithoutId(ReviewRoleLikeDislike.class);
        reviewRoleLikeDislike.setReviewRole(createReviewRole(createRole()));
        reviewRoleLikeDislike.setRegisteredUser(createRegisteredUser());
        return reviewRoleLikeDislikeRepository.save(reviewRoleLikeDislike);
    }

    public News createFilmNews(Film film) {
        News news = generateFlatEntityWithoutId(News.class);
        news.setText("Brad Pitt rev_als he turned down 'The Matrix'");
        news.setNewsType(NewsType.FILM);
        news.setNewsObjectId(film.getId());
        news.setCntLike(0);
        news.setCntDislike(0);
        return newsRepository.save(news);
    }

    public News createNews(Role role) {
        News news = generateFlatEntityWithoutId(News.class);
        news.setText("Brad Pitt reveals he turned down 'The Matrix'");
        news.setNewsType(NewsType.ROLE);
        news.setNewsObjectId(role.getId());
        news.setCntLike(0);
        news.setCntLike(0);
        return newsRepository.save(news);
    }

    public NewsLikeDislike createNewsLikeDislike() {
        NewsLikeDislike newsLikeDislike = generateFlatEntityWithoutId(NewsLikeDislike.class);
        newsLikeDislike.setNews(createFilmNews(createFlatFilm()));
        newsLikeDislike.setRegisteredUser(createRegisteredUser());
        return newsLikeDislikeRepository.save(newsLikeDislike);
    }

    public NewsLikeDislike createNewsLikeDislike(News news, boolean isLike) {
        NewsLikeDislike newsLikeDislike = generateFlatEntityWithoutId(NewsLikeDislike.class);
        newsLikeDislike.setNews(news);
        newsLikeDislike.setIsLike(isLike);
        newsLikeDislike.setRegisteredUser(createRegisteredUser());
        return newsLikeDislikeRepository.save(newsLikeDislike);
    }

    public ProblemReview createProblemFilmReview() {
        ProblemReview problemReview = generateFlatEntityWithoutId(ProblemReview.class);
        problemReview.setReviewFilm(createReviewFilm(createFilm()));
        problemReview.setRegisteredUser(createRegisteredUser());
        return problemReviewRepository.save(problemReview);
    }

    public ProblemReview createProblemRoleReview() {
        ProblemReview problemReview = generateFlatEntityWithoutId(ProblemReview.class);
        problemReview.setReviewRole(createReviewRole(createRole()));
        problemReview.setRegisteredUser(createRegisteredUser());
        return problemReviewRepository.save(problemReview);
    }

    public MisprintInfoFilm createMisprintInfoFilm() {
        MisprintInfoFilm misprintInfo = generateFlatEntityWithoutId(MisprintInfoFilm.class);
        misprintInfo.setContent(createFilm());
        misprintInfo.setRegisteredUser(createRegisteredUser());
        misprintInfo.setObject(MisprintObject.FILM);
        return misprintInfoFilmRepository.save(misprintInfo);
    }

    public MisprintInfoNews createMisprintInfoNews() {
        MisprintInfoNews misprintInfo = generateFlatEntityWithoutId(MisprintInfoNews.class);
        misprintInfo.setContent(createFilmNews(createFlatFilm()));
        misprintInfo.setRegisteredUser(createRegisteredUser());
        misprintInfo.setObject(MisprintObject.NEWS);
        return misprintInfoNewsRepository.save(misprintInfo);
    }

    public MisprintInfoRole createMisprintInfoRole() {
        MisprintInfoRole misprintInfo = generateFlatEntityWithoutId(MisprintInfoRole.class);
        misprintInfo.setContent(createRole());
        misprintInfo.setRegisteredUser(createRegisteredUser());
        misprintInfo.setObject(MisprintObject.ROLE);
        return misprintInfoRoleRepository.save(misprintInfo);
    }

    public RegisteredUser createRegisteredUser() {
        RegisteredUser registeredUser = generateFlatEntityWithoutId(RegisteredUser.class);
        return registeredUserRepository.save(registeredUser);
    }

    public ContentManager createContentManager() {
        ContentManager contentManager = generateEntityWithoutId(ContentManager.class);
        return contentManagerRepository.save(contentManager);
    }

    public Moderator createModerator() {
        Moderator moderator = generateEntityWithoutId(Moderator.class);
        return moderatorRepository.save(moderator);
    }

    public CompanyCreateDTO createCompanyCreateDTO() {

        CompanyCreateDTO createDTO = generateObject(CompanyCreateDTO.class);
        return createDTO;
    }

    public CrewMemberCreateDTO createCrewMemberCreateDTO() {

        CrewMemberCreateDTO createDTO = generateObject(CrewMemberCreateDTO.class);
        createDTO.setPersonId(createPerson().getId());
        createDTO.setDepartmentId(createDepartment().getId());
        return createDTO;
    }

    public CrewMemberPatchDTO createCrewMemberPatchDTO() {

        return generateObject(CrewMemberPatchDTO.class);
    }

    public CrewMemberPutDTO createCrewMemberPutDTO() {

        return generateObject(CrewMemberPutDTO.class);
    }

    public ActorCreateDTO createActorCreateDTO() {

        ActorCreateDTO actor = generateObject(ActorCreateDTO.class);
        actor.setPersonId(createPerson().getId());
        actor.setAverageRating(null);
        actor.setAverageRatingByFilm(null);
        return actor;
    }

    public DepartmentCreateDTO createDepartmentCreateDTO() {

        DepartmentCreateDTO department = generateObject(DepartmentCreateDTO.class);
        return department;
    }

    public PersonCreateDTO createPersonCreateDTO() {

        return generateObject(PersonCreateDTO.class);
    }

    public PersonPatchDTO createPersonPatchDTO() {
        return generateObject(PersonPatchDTO.class);
    }

    public PersonPutDTO createPersonPutDTO() {
        return generateObject(PersonPutDTO.class);
    }

    public RolePatchDTO createRolePatchDTO() {
        return generateObject(RolePatchDTO.class);
    }

    public RolePutDTO createRolePutDTO() {
        return generateObject(RolePutDTO.class);
    }

    public RatingFilmCreateDTO createRatingFilmCreateDTO() {
        RatingFilmCreateDTO create = generateObject(RatingFilmCreateDTO.class);
        create.setRegisteredUserId(createRegisteredUser().getId());
        return create;
    }

    public RatingRoleCreateDTO createRatingRoleCreateDTO() {
        RatingRoleCreateDTO create = generateObject(RatingRoleCreateDTO.class);
        create.setRegisteredUserId(createRegisteredUser().getId());
        return create;
    }

    public ReviewFilmCreateDTO createReviewFilmCreateDTO() {
        ReviewFilmCreateDTO create = generateObject(ReviewFilmCreateDTO.class);
        create.setRegisteredUserId(createRegisteredUser().getId());
        return create;
    }

    public ReviewFilmCreateDTO createReviewFilmWithWrongUser() {
        ReviewFilmCreateDTO create = generateObject(ReviewFilmCreateDTO.class);
        create.setRegisteredUserId(UUID.randomUUID());
        return create;
    }

    public RatingFilmCreateDTO createRatingFilmWithWrongUser() {
        RatingFilmCreateDTO create = generateObject(RatingFilmCreateDTO.class);
        create.setRegisteredUserId(UUID.randomUUID());
        return create;
    }

    public ReviewRoleCreateDTO createReviewRoleWithWrongUser() {
        ReviewRoleCreateDTO create = generateObject(ReviewRoleCreateDTO.class);
        create.setRegisteredUserId(UUID.randomUUID());
        return create;
    }

    public RatingRoleCreateDTO createRatingRoleWithWrongUser() {
        RatingRoleCreateDTO create = generateObject(RatingRoleCreateDTO.class);
        create.setRegisteredUserId(UUID.randomUUID());
        return create;
    }

    public RegisteredUserPatchDTO createRegisteredUserPatchDTO() {
        RegisteredUserPatchDTO patch = generateObject(RegisteredUserPatchDTO.class);
        patch.setEncodedPassword(null);
        patch.setProfileName(null);
        patch.setTrustLevel(null);
        patch.setUserStatus(null);
        return patch;
    }

    public ReviewFilmPatchDTO createReviewFilmPatchDTO() {
        return generateObject(ReviewFilmPatchDTO.class);
    }

    public ReviewFilmPutDTO createReviewFilmPutDTO() {
        return generateObject(ReviewFilmPutDTO.class);
    }

    public ReviewRoleCreateDTO createReviewRoleCreateDTO() {
        ReviewRoleCreateDTO create = generateObject(ReviewRoleCreateDTO.class);
        create.setRegisteredUserId(createRegisteredUser().getId());
        return create;
    }

    public ReviewRolePatchDTO createReviewRolePatchDTO() {
        return generateObject(ReviewRolePatchDTO.class);
    }

    public ReviewRolePutDTO createReviewRolePutDTO() {
        return generateObject(ReviewRolePutDTO.class);
    }

    public NewsLikeDislikeCreateDTO createNewsLikeDislikeWithWrongNews() {
        NewsLikeDislikeCreateDTO create = generateObject(NewsLikeDislikeCreateDTO.class);
        create.setNewsId(UUID.randomUUID());
        return create;
    }

    public ReviewFilmLikeDislikeCreateDTO createReviewFilmLikeDislikeWithWrongReviewFilm() {
        ReviewFilmLikeDislikeCreateDTO create = generateObject(ReviewFilmLikeDislikeCreateDTO.class);
        create.setReviewFilmId(UUID.randomUUID());
        return create;
    }

    public ReviewRoleLikeDislikeCreateDTO createReviewRoleLikeDislikeWithWrongReviewRole() {
        ReviewRoleLikeDislikeCreateDTO create = generateObject(ReviewRoleLikeDislikeCreateDTO.class);
        create.setReviewRoleId(UUID.randomUUID());
        return create;
    }


    public NewsLikeDislikeCreateDTO createNewsLikeDislikeCreateDTO() {
        NewsLikeDislikeCreateDTO create = generateObject(NewsLikeDislikeCreateDTO.class);
        create.setNewsId(createFilmNews(createFlatFilm()).getId());
        return create;
    }

    public ReviewFilmLikeDislikeCreateDTO createReviewFilmLikeDislikeCreateDTO() {
        ReviewFilmLikeDislikeCreateDTO create = generateObject(ReviewFilmLikeDislikeCreateDTO.class);
        create.setReviewFilmId(createReviewFilm(createFilm()).getId());
        return create;
    }

    public ReviewRoleLikeDislikeCreateDTO createReviewRoleLikeDislikeCreateDTO() {
        ReviewRoleLikeDislikeCreateDTO create = generateObject(ReviewRoleLikeDislikeCreateDTO.class);
        create.setReviewRoleId(createReviewRole(createRole()).getId());
        return create;
    }

    public ProblemReviewCreateDTO createProblemFilmReviewCreateDTO() {
        ProblemReviewCreateDTO create = generateObject(ProblemReviewCreateDTO.class);
        create.setReviewFilmId(createReviewFilm(createFilm()).getId());
        return create;
    }

    public ProblemReviewCreateDTO createProblemReviewFilmCreateDTO() {
        ProblemReviewCreateDTO create = generateObject(ProblemReviewCreateDTO.class);
        create.setReviewFilmId(createReviewFilm(createFlatFilm()).getId());
        create.setReviewRoleId(null);
        return create;
    }

    public ProblemReviewCreateDTO createProblemReviewRoleCreateDTO() {
        ProblemReviewCreateDTO create = generateObject(ProblemReviewCreateDTO.class);
        create.setReviewFilmId(null);
        create.setReviewRoleId(createReviewRole(createRole()).getId());
        return create;
    }

    public ProblemReviewPatchDTO createProblemReviewPatchDTO() {
        ProblemReviewPatchDTO create = generateObject(ProblemReviewPatchDTO.class);
        return create;
    }

    public ProblemReviewPutDTO createProblemReviewPutDTO() {
        ProblemReviewPutDTO create = generateObject(ProblemReviewPutDTO.class);
        return create;
    }

    public ProblemReviewCreateDTO createProblemReviewWithWrongReviewFilm() {
        ProblemReviewCreateDTO create = generateObject(ProblemReviewCreateDTO.class);
        create.setReviewFilmId(UUID.randomUUID());
        return create;
    }

    public MisprintInfoFilm createMpInfoFilmWithParam(Film film,
                                                      Integer startIndex, Integer endIndex,
                                                      String text,
                                                      String incorrectText) {
        MisprintInfoFilm misprintInfo = new MisprintInfoFilm();
        misprintInfo.setStatus(MisprintStatus.NEED_TO_FIX);
        misprintInfo.setObject(MisprintObject.FILM);
        misprintInfo.setStartIndex(startIndex);
        misprintInfo.setEndIndex(endIndex);
        misprintInfo.setText(text);
        misprintInfo.setIncorrectText(incorrectText);
        misprintInfo.setContent(film);
        misprintInfo.setRegisteredUser(createRegisteredUser());
        return misprintInfoFilmRepository.save(misprintInfo);
    }

    public MisprintInfoFilm createMisprintInfoFilmWrongStatus() {
        MisprintInfoFilm misprintInfo = new MisprintInfoFilm();
        misprintInfo.setStatus(MisprintStatus.FIXED);
        misprintInfo.setObject(MisprintObject.FILM);
        misprintInfo.setStartIndex(10);
        misprintInfo.setEndIndex(19);
        misprintInfo.setText("a Virtual");
        misprintInfo.setContent(createFilm());
        misprintInfo.setRegisteredUser(createRegisteredUser());
        return misprintInfoFilmRepository.save(misprintInfo);
    }

    public MisprintInfoNews createMpInfoNewsWithParam(News news,
                                                      Integer startIndex, Integer endIndex,
                                                      String text,
                                                      String incorrectText) {
        MisprintInfoNews misprintInfo = new MisprintInfoNews();
        misprintInfo.setStatus(MisprintStatus.NEED_TO_FIX);
        misprintInfo.setObject(MisprintObject.NEWS);
        misprintInfo.setStartIndex(startIndex);
        misprintInfo.setEndIndex(endIndex);
        misprintInfo.setText(text);
        misprintInfo.setIncorrectText(incorrectText);
        misprintInfo.setContent(news);
        misprintInfo.setRegisteredUser(createRegisteredUser());
        return misprintInfoNewsRepository.save(misprintInfo);
    }

    public MisprintInfoNews createMisprintInfoNewsWrongStatus() {
        MisprintInfoNews misprintInfo = new MisprintInfoNews();
        misprintInfo.setStatus(MisprintStatus.FIXED);
        misprintInfo.setObject(MisprintObject.NEWS);
        misprintInfo.setStartIndex(10);
        misprintInfo.setEndIndex(16);
        misprintInfo.setText("reveal");
        misprintInfo.setContent(createFilmNews(createFlatFilm()));
        misprintInfo.setRegisteredUser(createRegisteredUser());
        return misprintInfoNewsRepository.save(misprintInfo);
    }

    public MisprintInfoRole createMpInfoRoleWithParam(Role role,
                                                      Integer startIndex, Integer endIndex,
                                                      String text,
                                                      String incorrectText) {
        MisprintInfoRole misprintInfo = new MisprintInfoRole();
        misprintInfo.setStatus(MisprintStatus.NEED_TO_FIX);
        misprintInfo.setObject(MisprintObject.ROLE);
        misprintInfo.setStartIndex(startIndex);
        misprintInfo.setEndIndex(endIndex);
        misprintInfo.setText(text);
        misprintInfo.setIncorrectText(incorrectText);
        misprintInfo.setContent(role);
        misprintInfo.setRegisteredUser(createRegisteredUser());
        return misprintInfoRoleRepository.save(misprintInfo);
    }

    public MisprintInfoRole createMisprintInfoRoleWrongStatus() {
        MisprintInfoRole misprintInfo = new MisprintInfoRole();
        misprintInfo.setStatus(MisprintStatus.FIXED);
        misprintInfo.setObject(MisprintObject.ROLE);
        misprintInfo.setStartIndex(1);
        misprintInfo.setEndIndex(1);
        misprintInfo.setText("e");
        misprintInfo.setContent(createRole());
        misprintInfo.setRegisteredUser(createRegisteredUser());
        return misprintInfoRoleRepository.save(misprintInfo);
    }

    public MisprintInfoCommon createMpInfoCommonWithParam(UUID objectId,
                                                          MisprintObject objectType,
                                                          Integer startIndex, Integer endIndex,
                                                          String text,
                                                          String incorrectText) {
        MisprintInfoCommon misprintInfo = new MisprintInfoCommon();
        misprintInfo.setStatus(MisprintStatus.NEED_TO_FIX);
        misprintInfo.setObjectType(objectType);
        misprintInfo.setStartIndex(startIndex);
        misprintInfo.setEndIndex(endIndex);
        misprintInfo.setText(text);
        misprintInfo.setIncorrectText(incorrectText);
        misprintInfo.setObjectId(objectId);
        return misprintInfoCommonRepository.save(misprintInfo);
    }

    public MisprintInfoCommon createMisprintInfoCommonWrongStatus() {
        MisprintInfoCommon misprintInfo = new MisprintInfoCommon();
        misprintInfo.setStatus(MisprintStatus.FIXED);
        misprintInfo.setObjectType(MisprintObject.FILM);
        misprintInfo.setStartIndex(10);
        misprintInfo.setEndIndex(19);
        misprintInfo.setText("a Virtual");
        misprintInfo.setObjectId(createFilm().getId());
        return misprintInfoCommonRepository.save(misprintInfo);
    }

    public MisprintInfoCommon createMisprintInfoCommonWrongObject() {
        MisprintInfoCommon misprintInfo = new MisprintInfoCommon();
        misprintInfo.setStatus(MisprintStatus.NEED_TO_FIX);
        misprintInfo.setObjectType(MisprintObject.ARTICLE);
        misprintInfo.setStartIndex(10);
        misprintInfo.setEndIndex(19);
        misprintInfo.setText("a Virtual");
        misprintInfo.setObjectId(createFilm().getId());
        return misprintInfoCommonRepository.save(misprintInfo);
    }

    public MisprintInfoFilmCreateDTO createMisprintInfoFilmCreateDTO() {
        MisprintInfoFilmCreateDTO create = generateObject(MisprintInfoFilmCreateDTO.class);
        create.setObject(MisprintObject.FILM);
        create.setContentId(createFilm().getId());
        return create;
    }

    public MisprintInfoNewsCreateDTO createMisprintInfoNewsCreateDTO() {
        MisprintInfoNewsCreateDTO create = generateObject(MisprintInfoNewsCreateDTO.class);
        create.setObject(MisprintObject.NEWS);
        create.setContentId(createFilmNews(createFlatFilm()).getId());
        return create;
    }

    public MisprintInfoRoleCreateDTO createMisprintInfoRoleCreateDTO() {
        MisprintInfoRoleCreateDTO create = generateObject(MisprintInfoRoleCreateDTO.class);
        create.setObject(MisprintObject.ROLE);
        create.setContentId(createRole().getId());
        return create;
    }

    public MisprintInfoFilmCreateDTO createMisprintInfoFilmWithWrongFilm() {
        MisprintInfoFilmCreateDTO create = generateObject(MisprintInfoFilmCreateDTO.class);
        create.setObject(MisprintObject.FILM);
        create.setContentId(UUID.randomUUID());
        return create;
    }

    public MisprintInfoRoleCreateDTO createMisprintInfoRoleWithWrongRole() {
        MisprintInfoRoleCreateDTO create = generateObject(MisprintInfoRoleCreateDTO.class);
        create.setObject(MisprintObject.ROLE);
        create.setContentId(UUID.randomUUID());
        return create;
    }

    public MisprintInfoNewsCreateDTO createMisprintInfoNewsWithWrongNews() {
        MisprintInfoNewsCreateDTO create = generateObject(MisprintInfoNewsCreateDTO.class);
        create.setObject(MisprintObject.NEWS);
        create.setContentId(UUID.randomUUID());
        return create;
    }

    public Genre createGenre(FilmGenre name, Film film) {
        Genre genre = new Genre();
        genre.setName(name);
        Set<Film> films = new HashSet<>();
        films.add(film);
        genre.setFilms(films);
        return genreRepository.save(genre);
    }

    public PersonReadDTO generatePersonReadDTO() {
        PersonReadDTO read = generateObject(PersonReadDTO.class);
        return read;
    }

    public <T extends AbstractEntity> T generateFlatEntityWithoutId(Class<T> entityClass) {
        T entity = flatGenerator.generateRandomObject(entityClass);
        entity.setId(null);
        return entity;
    }

    public <T> T generateObject(Class<T> objectClass) {
        return generator.generateRandomObject(objectClass);
    }

    public <T> T flatGenerateObject(Class<T> objectClass) {
        return flatGenerator.generateRandomObject(objectClass);
    }

    private <T extends AbstractEntity> T generateEntityWithoutId(Class<T> entityClass) {
        T entity = generator.generateRandomObject(entityClass);
        entity.setId(null);
        return entity;
    }

    private RandomObjectGenerator generator;

    private RandomObjectGenerator flatGenerator;

    {
        Configuration c = new Configuration();
        c.setFlatMode(true);
        Configuration.Bean bean = c.beanOfClass(Film.class);
        Configuration.Bean.Property сp = bean.property("datePrime");
        сp.setMaxValue(LocalDate.now().minus(Period.ofDays(1)));
        Configuration.Bean.Property сp2 = bean.property("averageRating");
        сp2.setMaxValue(10.0);
        Configuration.Bean.Property сp3 = bean.property("forecastRating");
        сp3.setMaxValue(10.0);
        Configuration.Bean beanс2 = c.beanOfClass(Actor.class);
        Configuration.Bean.Property сp4 = beanс2.property("averageRating");
        сp4.setMaxValue(10.0);
        Configuration.Bean.Property сp5 = beanс2.property("averageRatingByFilm");
        сp5.setMaxValue(10.0);
        Configuration.Bean beanc3 = c.beanOfClass(RatingFilm.class);
        Configuration.Bean.Property cp6 = beanc3.property("rating");
        cp6.setMinValue(0.0);
        cp6.setMaxValue(10.0);
        Configuration.Bean beanc4 = c.beanOfClass(RatingRole.class);
        Configuration.Bean.Property cp7 = beanc4.property("rating");
        cp7.setMinValue(0.0);
        cp7.setMaxValue(10.0);
        Configuration.Bean beanc5 = c.beanOfClass(News.class);
        Configuration.Bean.Property cp8 = beanc5.property("cntLike");
        cp8.setMinValue(0);
        Configuration.Bean.Property cp9 = beanc5.property("cntDislike");
        cp9.setMinValue(0);
        Configuration.Bean beanc6 = c.beanOfClass(NewsCreateDTO.class);
        Configuration.Bean.Property cp10 = beanc6.property("cntLike");
        cp10.setMinValue(0);
        Configuration.Bean.Property cp11 = beanc6.property("cntDislike");
        cp11.setMinValue(0);
        flatGenerator = new RandomObjectGenerator(c);

        Configuration cc = new Configuration();
        Configuration.Bean bean1 = cc.beanOfClass(Person.class);
        Configuration.Bean.Property p1 = bean1.property("dateBirth");
        p1.setMaxValue(LocalDate.now().minus(Period.ofDays(1)));
        Configuration.Bean.Property p2 = bean1.property("dateDeath");
        p2.setMaxValue(LocalDate.now().minus(Period.ofDays(1)));

        Configuration.Bean bean2 = cc.beanOfClass(PersonCreateDTO.class);
        Configuration.Bean.Property p3 = bean2.property("dateBirth");
        p3.setMaxValue(LocalDate.now().minus(Period.ofDays(1)));
        Configuration.Bean.Property p4 = bean2.property("dateDeath");
        p4.setMaxValue(LocalDate.now().minus(Period.ofDays(1)));

        Configuration.Bean bean3 = cc.beanOfClass(Film.class);
        Configuration.Bean.Property p5 = bean3.property("datePrime");
        p5.setMaxValue(LocalDate.now().minus(Period.ofDays(1)));
        Configuration.Bean.Property p51 = bean3.property("averageRating");
        p51.setMaxValue(10.0);
        Configuration.Bean.Property p52 = bean3.property("forecastRating");
        p52.setMaxValue(10.0);

        Configuration.Bean bean4 = cc.beanOfClass(FilmCreateDTO.class);
        Configuration.Bean.Property p6 = bean4.property("datePrime");
        p6.setMaxValue(LocalDate.now().minus(Period.ofDays(1)));

        Configuration.Bean bean5 = cc.beanOfClass(RegisteredUser.class);
        Configuration.Bean.Property p7 = bean5.property("trustLevel");
        p7.setMinValue(1);
        p7.setMaxValue(5);

        Configuration.Bean bean6 = cc.beanOfClass(RegisteredUserCreateDTO.class);
        Configuration.Bean.Property p8 = bean6.property("trustLevel");
        p8.setMinValue(1);
        p8.setMaxValue(5);

        Configuration.Bean bean7 = cc.beanOfClass(Actor.class);
        Configuration.Bean.Property p9 = bean7.property("averageRating");
        p9.setMaxValue(10.0);
        Configuration.Bean.Property p92 = bean7.property("averageRatingByFilm");
        p92.setMaxValue(10.0);

        Configuration.Bean bean8 = cc.beanOfClass(ActorCreateDTO.class);
        Configuration.Bean.Property p10 = bean8.property("averageRating");
        p10.setMaxValue(10.0);
        Configuration.Bean.Property p11 = bean8.property("averageRatingByFilm");
        p11.setMaxValue(10.0);

        Configuration.Bean bean9 = cc.beanOfClass(RatingFilm.class);
        Configuration.Bean.Property p12 = bean9.property("rating");
        p12.setMinValue(0.0);
        p12.setMaxValue(10.0);

        Configuration.Bean bean10 = cc.beanOfClass(RatingFilmCreateDTO.class);
        Configuration.Bean.Property p13 = bean10.property("rating");
        p13.setMinValue(0.0);
        p13.setMaxValue(10.0);

        Configuration.Bean bean11 = cc.beanOfClass(RatingRole.class);
        Configuration.Bean.Property p14 = bean11.property("rating");
        p14.setMinValue(0.0);
        p14.setMaxValue(10.0);

        Configuration.Bean bean12 = cc.beanOfClass(RatingRoleCreateDTO.class);
        Configuration.Bean.Property p15 = bean12.property("rating");
        p15.setMinValue(0.0);
        p15.setMaxValue(10.0);

        Configuration.Bean bean13 = cc.beanOfClass(News.class);
        Configuration.Bean.Property p16 = bean13.property("cntLike");
        p16.setMinValue(0);
        Configuration.Bean.Property p17 = bean13.property("cntDislike");
        p17.setMinValue(0);

        Configuration.Bean bean14 = cc.beanOfClass(NewsCreateDTO.class);
        Configuration.Bean.Property p18 = bean14.property("cntLike");
        p18.setMinValue(0);
        Configuration.Bean.Property p19 = bean14.property("cntDislike");
        p19.setMinValue(0);

        generator = new RandomObjectGenerator(cc);
    }
}
