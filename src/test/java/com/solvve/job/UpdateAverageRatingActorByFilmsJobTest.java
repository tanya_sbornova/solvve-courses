package com.solvve.job;

import com.solvve.BaseTest;
import com.solvve.domain.*;
import com.solvve.repository.ActorRepository;
import com.solvve.repository.RoleRepository;
import com.solvve.service.ActorService;
import com.solvve.util.TestObjectsFactory;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.List;
import java.util.UUID;

@Slf4j
@Import(UpdateAverageRatingActorByFilmsJobTest.ScheduledTestConfig.class)
public class UpdateAverageRatingActorByFilmsJobTest extends BaseTest {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private RoleRepository roleRepository;

    @SpyBean
    private ActorService actorService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private UpdateAverageRatingActorByFilmsJob updateAverageRatingActorByFilmsJob;

    @EnableScheduling
    static class ScheduledTestConfig {
    }

    @Test
    public void testUpdateAverageRatingByFilms() {

        Film film = testObjectsFactory.createFlatFilm();
        testObjectsFactory.createRatingFilm(film, 7.0);
        testObjectsFactory.createRatingFilm(film, 8.0);
        testObjectsFactory.createRatingFilm(film, 9.0);
        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        Role role1 = testObjectsFactory.createRole(actor, film);
        roleRepository.save(role1);
        Film film2 = testObjectsFactory.createFlatFilm();
        testObjectsFactory.createRatingFilm(film, 6.0);
        testObjectsFactory.createRatingFilm(film, 7.0);
        testObjectsFactory.createRatingFilm(film, 8.0);
        Role role2 = testObjectsFactory.createRole(actor, film2);
        roleRepository.save(role2);

        updateAverageRatingActorByFilmsJob.updateAverageRatingOfActorByFilms();

        actor = actorRepository.findById(actor.getId()).get();

        Assert.assertEquals(7.5, actor.getAverageRatingByFilm(), Double.MIN_NORMAL);
    }

    @Test
    public void testActorsUpdatedIndependently() {

        Film film = testObjectsFactory.createFlatFilm();
        testObjectsFactory.createRatingFilm(film, 7.0);
        testObjectsFactory.createRatingFilm(film, 8.0);
        testObjectsFactory.createRatingFilm(film, 9.0);
        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        Role role1 = testObjectsFactory.createRole(actor, film);
        roleRepository.save(role1);
        Film film2 = testObjectsFactory.createFlatFilm();
        testObjectsFactory.createRatingFilm(film, 6.0);
        testObjectsFactory.createRatingFilm(film, 7.0);
        testObjectsFactory.createRatingFilm(film, 8.0);
        Role role2 = testObjectsFactory.createRole(actor, film2);
        roleRepository.save(role2);

        UUID[] failedId = new UUID[1];
        Mockito.doAnswer(invocationOnMock -> {
            if (failedId[0] == null) {
                failedId[0] = invocationOnMock.getArgument(0);
                throw new RuntimeException();
            }
            return invocationOnMock.callRealMethod();
        }).when(actorService).updateAverageRatingActorOfRole(Mockito.any());

        updateAverageRatingActorByFilmsJob.updateAverageRatingOfActorByFilms();

        for (Actor f : actorRepository.findAll()) {
            if (f.getId().equals(failedId[0])) {
                Assert.assertNull(f.getAverageRatingByFilm());
            } else {
                Assert.assertNotNull(f.getAverageRatingByFilm());
            }
        }
    }
}

