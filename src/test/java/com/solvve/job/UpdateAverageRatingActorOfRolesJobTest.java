package com.solvve.job;

import com.solvve.BaseTest;
import com.solvve.domain.Actor;
import com.solvve.domain.Person;
import com.solvve.domain.RatingRole;
import com.solvve.domain.Role;
import com.solvve.repository.ActorRepository;
import com.solvve.repository.RoleRepository;
import com.solvve.service.ActorService;
import com.solvve.util.TestObjectsFactory;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.List;
import java.util.UUID;

@Slf4j
@Import(UpdateAverageRatingActorOfRolesJobTest.ScheduledTestConfig.class)
public class UpdateAverageRatingActorOfRolesJobTest extends BaseTest {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private RoleRepository roleRepository;

    @SpyBean
    private ActorService actorService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private UpdateAverageRatingActorOfRolesJob updateAverageRatingActorOfRolesJob;

    @EnableScheduling
    static class ScheduledTestConfig {
    }

    @Test
    public void testUpdateAverageRatingOfActors() {
        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        Role role1 = testObjectsFactory.createRole("Neo", actor);
        RatingRole ratingRole1 = testObjectsFactory.createRatingRole(role1, 8.0);
        role1.setRatingRoles(List.of(ratingRole1));
        roleRepository.save(role1);
        Role role2 = testObjectsFactory.createRole("Kevin Lomax", actor);
        RatingRole ratingRole2 = testObjectsFactory.createRatingRole(role2, 8.5);
        role2.setRatingRoles(List.of(ratingRole2));
        roleRepository.save(role2);

        updateAverageRatingActorOfRolesJob.updateAverageRatingOfActors();

        actor = actorRepository.findById(actor.getId()).get();

        Assert.assertEquals(8.25, actor.getAverageRating(), Double.MIN_NORMAL);
    }

    @Test
    public void testActorsUpdatedIndependently() {
        Person person = testObjectsFactory.createPerson();
        Actor actor = testObjectsFactory.createActor(person);
        Role role1 = testObjectsFactory.createRole("Neo", actor);
        RatingRole ratingRole1 = testObjectsFactory.createRatingRole(role1, 8.0);
        role1.setRatingRoles(List.of(ratingRole1));
        roleRepository.save(role1);
        Role role2 = testObjectsFactory.createRole("Kevin Lomax", actor);
        RatingRole ratingRole2 = testObjectsFactory.createRatingRole(role2, 8.5);
        role2.setRatingRoles(List.of(ratingRole2));
        roleRepository.save(role2);

        Actor actor2 = testObjectsFactory.createActor(person);
        role1 = testObjectsFactory.createRole( "Neo", actor2);
        ratingRole1 = testObjectsFactory.createRatingRole(role1, 8.0);
        role1.setRatingRoles(List.of(ratingRole1));
        roleRepository.save(role1);
        role2 = testObjectsFactory.createRole( "Kevin Lomax", actor2);
        ratingRole2 = testObjectsFactory.createRatingRole(role2, 8.5);
        role2.setRatingRoles(List.of(ratingRole2));
        roleRepository.save(role2);

        UUID[] failedId = new UUID[1];
        Mockito.doAnswer(invocationOnMock -> {
            if (failedId[0] == null) {
                failedId[0] = invocationOnMock.getArgument(0);
                throw new RuntimeException();
            }
            return invocationOnMock.callRealMethod();
        }).when(actorService).updateAverageRatingActorOfRole(Mockito.any());

        updateAverageRatingActorOfRolesJob.updateAverageRatingOfActors();

        for (Actor f : actorRepository.findAll()) {
            if (f.getId().equals(failedId[0])) {
                Assert.assertNull(f.getAverageRating());
            } else {
                Assert.assertNotNull(f.getAverageRating());
            }
        }
    }
}
