package com.solvve.job;

import com.solvve.BaseTest;
import com.solvve.domain.Film;
import com.solvve.repository.FilmRepository;
import com.solvve.service.FilmService;
import com.solvve.util.TestObjectsFactory;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.UUID;

@Slf4j
@Import(UpdateAverageRatingOfFilmsJobTest.ScheduledTestConfig.class)
public class UpdateAverageRatingOfFilmsJobTest extends BaseTest {

    @Autowired
    private FilmRepository filmRepository;

    @SpyBean
    private FilmService filmService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private UpdateAverageRatingOfFilmsJob updateAverageRatingOfFilmsJob;

    @EnableScheduling
    static class ScheduledTestConfig {
    }

    @Test
    public void testUpdateAverageRatingOfFilms() {
        Film film = testObjectsFactory.createFlatFilm();
        testObjectsFactory.createRatingFilm(film, 7.0);
        testObjectsFactory.createRatingFilm(film, 8.0);
        testObjectsFactory.createRatingFilm(film, 9.0);

        updateAverageRatingOfFilmsJob.updateAverageRatingOfFilms();

        film = filmRepository.findById(film.getId()).get();

        Assert.assertEquals(8.0, film.getAverageRating(), Double.MIN_NORMAL);
    }

    @Test
    public void testFilmsUpdatedIndependently() {
        Film film = testObjectsFactory.createFlatFilm();
        testObjectsFactory.createRatingFilm(film, 7.0);
        testObjectsFactory.createRatingFilm(film, 8.0);
        testObjectsFactory.createRatingFilm(film, 9.0);
        Film film2 = testObjectsFactory.createFlatFilm();
        testObjectsFactory.createRatingFilm(film2, 7.0);
        testObjectsFactory.createRatingFilm(film2, 8.0);
        testObjectsFactory.createRatingFilm(film2, 9.0);

        UUID[] failedId = new UUID[1];
        Mockito.doAnswer(invocationOnMock -> {
            if (failedId[0] == null) {
                failedId[0] = invocationOnMock.getArgument(0);
                throw new RuntimeException();
            }
            return invocationOnMock.callRealMethod();
        }).when(filmService).updateAverageRatingOfFilm(Mockito.any());

        updateAverageRatingOfFilmsJob.updateAverageRatingOfFilms();

        for (Film f : filmRepository.findAll()) {
            if (f.getId().equals(failedId[0])) {
                Assert.assertNull(f.getAverageRating());
            }
            else {
                Assert.assertNotNull(f.getAverageRating());
            }
        }
    }
}
