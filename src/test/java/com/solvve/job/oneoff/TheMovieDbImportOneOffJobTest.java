package com.solvve.job.oneoff;

import com.solvve.BaseTest;
import com.solvve.client.themoviedb.TheMovieDbClient;
import com.solvve.client.themoviedb.dto.MovieReadShortDTO;
import com.solvve.client.themoviedb.dto.MoviesPageDTO;
import com.solvve.exception.ImportAlreadyPerformedException;
import com.solvve.exception.ImportedEntityAlreadyExistException;
import com.solvve.service.importer.MovieImporterService;
import com.solvve.util.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

public class TheMovieDbImportOneOffJobTest extends BaseTest {

    @Autowired
    private TheMovieDbImportOneOffJob job;

    @MockBean
    private TheMovieDbClient client;

    @MockBean
    private MovieImporterService movieImporterService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testDoImport() throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        MoviesPageDTO page = generatePageWith2Results();
        Mockito.when(client.getTopRatedMovies()).thenReturn(page);

        job.doImport();

        for(MovieReadShortDTO m: page.getResults()) {
            Mockito.verify(movieImporterService).importMovie(m.getId());
        }
    }

    @Test
    public void testDoImportNoExceptionIfGetPageFailed() {
        Mockito.when(client.getTopRatedMovies()).thenThrow(RuntimeException.class);

        job.doImport();

        Mockito.verifyNoInteractions(movieImporterService);
    }

    @Test
    public void testDoImportFirstFailedAndSecondSuccess() throws ImportedEntityAlreadyExistException,
            ImportAlreadyPerformedException {
        MoviesPageDTO page = generatePageWith2Results();
        Mockito.when(client.getTopRatedMovies()).thenReturn(page);
        Mockito.when(movieImporterService.importMovie(page.getResults().get(0).getId()))
                .thenThrow(RuntimeException.class);

        job.doImport();

        for(MovieReadShortDTO m: page.getResults()) {
            Mockito.verify(movieImporterService).importMovie(m.getId());
        }
    }

    private MoviesPageDTO generatePageWith2Results() {
        MoviesPageDTO page = testObjectsFactory.generateObject(MoviesPageDTO.class);
        page.getResults().add(testObjectsFactory.generateObject(MovieReadShortDTO.class));
        Assert.assertEquals(2, page.getResults().size());
        return page;
    }
}

