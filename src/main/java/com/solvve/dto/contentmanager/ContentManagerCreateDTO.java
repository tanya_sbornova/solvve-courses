package com.solvve.dto.contentmanager;

import com.solvve.domain.Sex;
import com.solvve.domain.UserRoleType;
import com.solvve.domain.UserStatus;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class ContentManagerCreateDTO {

    @NotNull
    private String email;

    @NotNull
    private String encodedPassword;

    private UserRoleType userRole;

    @NotNull
    private UserStatus userStatus;

    private LocalDate dateBirth;
    private String country;
    private Sex sex;
}
