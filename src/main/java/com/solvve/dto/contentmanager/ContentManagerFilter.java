package com.solvve.dto.contentmanager;

import com.solvve.domain.UserStatus;
import lombok.Data;

@Data
public class ContentManagerFilter {

    private String email;
    private UserStatus userStatus;
}
