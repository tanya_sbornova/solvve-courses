package com.solvve.dto.contentmanager;

import com.solvve.domain.UserRoleType;
import com.solvve.domain.UserStatus;
import lombok.Data;

@Data
public class ContentManagerPutDTO {

    private UserRoleType userRole;
    private UserStatus userStatus;
}
