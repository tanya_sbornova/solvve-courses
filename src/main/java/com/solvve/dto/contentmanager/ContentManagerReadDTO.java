package com.solvve.dto.contentmanager;

import com.solvve.domain.Sex;
import com.solvve.domain.UserRoleType;
import com.solvve.domain.UserStatus;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Data
@EntityListeners(AuditingEntityListener.class)
public class ContentManagerReadDTO {

    private UUID id;
    private String email;
    private String encodedPassword;
    private UserRoleType userRole;
    private UserStatus userStatus;
    private LocalDate dateBirth;
    private String country;
    private Sex sex;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
