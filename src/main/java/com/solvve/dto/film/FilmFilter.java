package com.solvve.dto.film;

import com.solvve.domain.FilmGenre;
import com.solvve.domain.FilmStatus;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
public class FilmFilter {

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate datePrimeFrom;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate datePrimeTo;

    private Integer budgetFrom;
    private Integer budgetTo;
    private FilmStatus status;
    private FilmGenre genre;
}

