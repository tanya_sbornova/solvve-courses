package com.solvve.dto.film;

import com.solvve.domain.FilmStatus;
import lombok.Data;

import java.time.LocalDate;

@Data
public class FilmPatchDTO {

    private String title;
    private String text;
    private LocalDate datePrime;
    private Integer budget;
    private FilmStatus status;
}
