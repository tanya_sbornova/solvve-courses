package com.solvve.dto.film;

import com.solvve.domain.FilmStatus;
import com.solvve.dto.crewmember.CrewMemberReadDTO;
import com.solvve.dto.genre.GenreReadDTO;
import com.solvve.dto.ratingfilm.RatingFilmReadDTO;
import com.solvve.dto.reviewfilm.ReviewFilmReadDTO;
import lombok.Data;

import java.time.LocalDate;
import java.util.*;

@Data
public class FilmReadExtendedDTO {

    private UUID id;
    private String title;
    private String text;
    private String description;
    private LocalDate datePrime;
    private Integer budget;
    private FilmStatus status;
    private Double averageRating;
    private Double forecastRating;
    private List<CrewMemberReadDTO> crewMembers = new ArrayList<>();
    private List<RatingFilmReadDTO> ratingFilms = new ArrayList<>();
    private List<ReviewFilmReadDTO> reviewFilms = new ArrayList<>();
    private Set<GenreReadDTO> genres = new HashSet<>();
}
