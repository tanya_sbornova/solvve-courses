package com.solvve.dto.film;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class FilmInLeaderBoardReadDTO {

    private UUID id;
    private String text;
    private Double averageRating;
    private Long minRatingsCount;
    private Long maxRatingsCount;
}
