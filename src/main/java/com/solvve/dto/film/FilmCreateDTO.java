package com.solvve.dto.film;

import com.solvve.domain.FilmStatus;
import java.time.LocalDate;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class FilmCreateDTO {

    private String title;

    @NotNull
    private String text;

    private String description;

    private LocalDate datePrime;
    private Integer budget;
    private FilmStatus status;
    private Double averageRating;
    private Double forecastRating;
}


