package com.solvve.dto.film;

import com.solvve.domain.FilmStatus;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Data
@EntityListeners(AuditingEntityListener.class)
public class FilmReadDTO {

    private UUID id;
    private String title;
    private String text;
    private String description;
    private LocalDate datePrime;
    private Integer budget;
    private FilmStatus status;
    private Double averageRating;
    private Double forecastRating;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
