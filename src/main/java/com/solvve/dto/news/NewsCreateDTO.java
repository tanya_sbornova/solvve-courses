package com.solvve.dto.news;

import com.solvve.domain.NewsType;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class NewsCreateDTO {

    private UUID newsObjectId;
    private String title;

    @NotNull
    private String text;

    @NotNull
    private NewsType newsType;

    @NotNull
    @Min(value = 0, message = "Cnt like should not be less than 0")
    private Integer cntLike;

    @NotNull
    @Min(value = 0, message = "Cnt dislike should not be less than 0")
    private Integer cntDislike;
}
