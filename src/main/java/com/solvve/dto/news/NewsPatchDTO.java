package com.solvve.dto.news;

import lombok.Data;

@Data
public class NewsPatchDTO {

    private String title;

    private String text;

}
