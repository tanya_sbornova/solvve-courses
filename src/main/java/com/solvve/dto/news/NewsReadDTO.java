package com.solvve.dto.news;

import com.solvve.domain.NewsType;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import java.time.Instant;
import java.util.UUID;

@Data
@EntityListeners(AuditingEntityListener.class)
public class NewsReadDTO {

    private UUID id;
    private UUID newsObjectId;
    private String title;

    private String text;
    private NewsType newsType;

    private Integer cntLike;
    private Integer cntDislike;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
