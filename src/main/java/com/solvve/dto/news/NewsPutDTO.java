package com.solvve.dto.news;

import lombok.Data;

@Data
public class NewsPutDTO {

    private String title;

    private String text;
}
