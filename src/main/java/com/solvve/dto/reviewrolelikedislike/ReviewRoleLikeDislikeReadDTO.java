package com.solvve.dto.reviewrolelikedislike;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import java.time.Instant;
import java.util.UUID;

@Data
@EntityListeners(AuditingEntityListener.class)
public class ReviewRoleLikeDislikeReadDTO {

    private UUID id;
    private Boolean isLike;

    private UUID registeredUserId;
    private UUID reviewRoleId;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
