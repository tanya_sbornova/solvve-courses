package com.solvve.dto.reviewrolelikedislike;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class ReviewRoleLikeDislikeCreateDTO {

    @NotNull
    private Boolean isLike;

    @NotNull
    private UUID reviewRoleId;
}
