package com.solvve.dto.reviewrolelikedislike;

import lombok.Data;

@Data
public class ReviewRoleLikeDislikePatchDTO {

    private Boolean isLike;
}
