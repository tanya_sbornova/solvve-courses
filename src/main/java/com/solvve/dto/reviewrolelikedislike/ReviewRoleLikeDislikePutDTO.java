package com.solvve.dto.reviewrolelikedislike;

import lombok.Data;

@Data
public class ReviewRoleLikeDislikePutDTO {

    private Boolean isLike;
}
