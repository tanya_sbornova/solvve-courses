package com.solvve.dto.problemreview;

import com.solvve.domain.ProblemType;
import lombok.Data;

@Data
public class ProblemReviewPatchDTO {

    private ProblemType problemType;
}
