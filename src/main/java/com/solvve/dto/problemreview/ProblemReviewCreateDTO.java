package com.solvve.dto.problemreview;

import com.solvve.domain.ProblemType;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class ProblemReviewCreateDTO {

    @NotNull
    private ProblemType problemType;

    private UUID reviewFilmId;
    private UUID reviewRoleId;
}
