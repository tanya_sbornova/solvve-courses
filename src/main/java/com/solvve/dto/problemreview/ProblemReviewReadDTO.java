package com.solvve.dto.problemreview;

import com.solvve.domain.ProblemType;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import java.time.Instant;
import java.util.UUID;

@Data
@EntityListeners(AuditingEntityListener.class)
public class ProblemReviewReadDTO {

    private UUID id;
    private ProblemType problemType;

    private UUID registeredUserId;
    private UUID reviewFilmId;
    private UUID reviewRoleId;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;

}
