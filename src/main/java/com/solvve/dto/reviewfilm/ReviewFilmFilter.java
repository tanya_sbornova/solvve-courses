package com.solvve.dto.reviewfilm;

import com.solvve.domain.ReviewStatus;
import lombok.Data;

import java.util.UUID;

@Data
public class ReviewFilmFilter {

    private ReviewStatus status;
    private Boolean isSpoiler;
    private UUID registeredUserId;
    private UUID filmId;
}
