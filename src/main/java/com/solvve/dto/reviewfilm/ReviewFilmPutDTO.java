package com.solvve.dto.reviewfilm;

import com.solvve.domain.ReviewStatus;
import lombok.Data;

@Data
public class ReviewFilmPutDTO {

    private String text;
    private ReviewStatus status;
}