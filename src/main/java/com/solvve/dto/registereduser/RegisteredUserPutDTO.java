package com.solvve.dto.registereduser;

import com.solvve.domain.UserRoleType;
import com.solvve.domain.UserStatus;
import lombok.Data;

@Data
public class RegisteredUserPutDTO {

    private String profileName;
    private Integer trustLevel;
    private UserRoleType userRole;
    private UserStatus userStatus;
}

