package com.solvve.dto.registereduser;

import com.solvve.domain.FilmGenre;
import com.solvve.domain.Sex;
import com.solvve.domain.UserRoleType;
import com.solvve.domain.UserStatus;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class RegisteredUserCreateDTO {

    @NotNull
    private String email;

    @NotNull
    private String encodedPassword;

    @NotNull
    private UserRoleType userRole;

    @NotNull
    private UserStatus userStatus;

    @NotNull
    @Min(value = 1, message = "Trust level should not be less than 1")
    @Max(value = 5, message = "Trust level should not be greater than 5")
    private Integer trustLevel;

    private String name;
    private LocalDate dateBirth;
    private String country;
    private Sex sex;

    private String profileName;
    private FilmGenre favouriteGenre;
    private Double ratingReviews;
    private Double ratingActivity;
}

