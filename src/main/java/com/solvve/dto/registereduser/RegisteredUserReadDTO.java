package com.solvve.dto.registereduser;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

import com.solvve.domain.FilmGenre;
import com.solvve.domain.Sex;
import com.solvve.domain.UserRoleType;
import com.solvve.domain.UserStatus;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.EntityListeners;

@Data
@EntityListeners(AuditingEntityListener.class)
public class RegisteredUserReadDTO {

    private UUID id;
    private String email;
    private String encodedPassword;
    private UserRoleType userRole;
    private UserStatus userStatus;
    private Integer trustLevel;
    private String name;
    private LocalDate dateBirth;
    private String country;
    private Sex sex;

    private String profileName;
    private FilmGenre favouriteGenre;
    private Double ratingReviews;
    private Double ratingActivity;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
