package com.solvve.dto.registereduser;

import com.solvve.domain.UserStatus;
import lombok.Data;

@Data
public class RegisteredUserPatchDTO {

    private String encodedPassword;
    private String profileName;
    private UserStatus userStatus;
    private Integer trustLevel;
}
