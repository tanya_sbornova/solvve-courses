package com.solvve.dto.crewmember;

import com.solvve.domain.CrewMemberType;
import lombok.Data;

@Data
public class CrewMemberPutDTO {

    private CrewMemberType crewMemberType;
}
