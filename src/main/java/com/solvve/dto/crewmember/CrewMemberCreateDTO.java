package com.solvve.dto.crewmember;

import com.solvve.domain.CrewMemberType;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class CrewMemberCreateDTO {

    @NotNull
    private CrewMemberType crewMemberType;

    @NotNull
    private UUID personId;

    @NotNull
    private UUID departmentId;
}
