package com.solvve.dto.crewmember;

import com.solvve.domain.CrewMemberType;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import java.time.Instant;
import java.util.UUID;

@Data
@EntityListeners(AuditingEntityListener.class)
public class CrewMemberReadDTO {

    private UUID id;
    private CrewMemberType crewMemberType;
    private UUID personId;
    private UUID departmentId;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
