package com.solvve.dto.department;

import com.solvve.domain.DepartmentType;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DepartmentCreateDTO {

    @NotNull
    private DepartmentType departmentType;
}
