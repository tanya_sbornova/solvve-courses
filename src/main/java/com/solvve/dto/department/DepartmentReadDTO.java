package com.solvve.dto.department;

import com.solvve.domain.DepartmentType;
import lombok.Data;

import java.util.UUID;

@Data
public class DepartmentReadDTO {

    private UUID id;
    private DepartmentType departmentType;
}
