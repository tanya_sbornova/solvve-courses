package com.solvve.dto.misprintinfonews;

import com.solvve.domain.MisprintObject;
import com.solvve.domain.MisprintStatus;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Data
public class MisprintInfoNewsCreateDTO {

    private Integer startIndex;
    private Integer endIndex;
    private String text;

    @NotNull
    private String incorrectText;


    private MisprintStatus status;
    private MisprintObject object;

    private Instant fixedDate;
    private UUID contentId;
}
