package com.solvve.dto.person;

import java.time.LocalDate;

import com.solvve.domain.Country;
import com.solvve.domain.Sex;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PersonCreateDTO {

    @NotNull
    private String name;
    private LocalDate dateBirth;
    private LocalDate dateDeath;
    private String placeBirth;
    private Country country;
    private String education;
    private Sex sex;
    private String biography;

}
