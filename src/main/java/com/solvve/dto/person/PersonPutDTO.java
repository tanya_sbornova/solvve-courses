package com.solvve.dto.person;

import com.solvve.domain.Country;
import lombok.Data;

@Data
public class PersonPutDTO {

    private Country country;
}
