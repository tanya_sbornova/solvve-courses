package com.solvve.dto.person;

import java.time.Instant;
import java.time.LocalDate;
import java.util.*;

import com.solvve.domain.Country;
import com.solvve.domain.Sex;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;

@Data
@EntityListeners(AuditingEntityListener.class)
public class PersonReadDTO {

    private UUID id;
    private String name;
    private LocalDate dateBirth;
    private LocalDate dateDeath;
    private String placeBirth;
    private Country country;
    private String education;
    private Sex sex;
    private String biography;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
