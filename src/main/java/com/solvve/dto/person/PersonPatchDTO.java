package com.solvve.dto.person;

import com.solvve.domain.Country;
import lombok.Data;

@Data
public class PersonPatchDTO {

    private Country country;
}
