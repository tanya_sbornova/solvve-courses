package com.solvve.dto.genre;

import com.solvve.domain.FilmGenre;
import lombok.Data;

@Data
public class GenrePutDTO {

    private FilmGenre name;
}

