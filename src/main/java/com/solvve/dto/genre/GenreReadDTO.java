package com.solvve.dto.genre;

import com.solvve.domain.FilmGenre;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import java.time.Instant;
import java.util.UUID;

@Data
@EntityListeners(AuditingEntityListener.class)
public class GenreReadDTO {

    private UUID id;
    private FilmGenre name;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
