package com.solvve.dto.genre;

import com.solvve.domain.FilmGenre;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class GenreCreateDTO {

    @NotNull
    private FilmGenre name;
}
