package com.solvve.dto.ratingfilm;

import lombok.Data;

@Data
public class RatingFilmPutDTO {

    private Double rating;
}
