package com.solvve.dto.ratingfilm;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class RatingFilmCreateDTO {

    @NotNull
    @Min(value = 0, message = "Rating should not be less than 0")
    @Max(value = 10, message = "Rating level should not be greater than 10")
    private Double rating;

    @NotNull
    private UUID registeredUserId;
}
