package com.solvve.dto.ratingfilm;

import lombok.Data;

@Data
public class RatingFilmPatchDTO {

    private Double rating;
}
