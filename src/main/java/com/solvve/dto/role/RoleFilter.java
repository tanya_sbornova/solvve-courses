package com.solvve.dto.role;

import lombok.Data;
import java.util.UUID;

@Data
public class RoleFilter {

    private UUID personId;
    private UUID filmId;
}
