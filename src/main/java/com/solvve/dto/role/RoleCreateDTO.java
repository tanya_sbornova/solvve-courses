package com.solvve.dto.role;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class RoleCreateDTO {

    @NotNull
     private String text;

    private UUID actorId;

    @NotNull
    private UUID filmId;
}
