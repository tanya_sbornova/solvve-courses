package com.solvve.dto.role;

import lombok.Data;

@Data
public class RolePutDTO {

    private String text;
}
