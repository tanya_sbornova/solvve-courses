package com.solvve.dto.role;

import com.solvve.dto.actor.ActorReadDTO;
import com.solvve.dto.film.FilmReadDTO;
import lombok.Data;

import java.util.UUID;

@Data
public class RoleReadExtendedDTO {

    private UUID id;

    private String text;
    private ActorReadDTO actor;
    private FilmReadDTO film;
}
