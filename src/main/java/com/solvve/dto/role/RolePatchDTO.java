package com.solvve.dto.role;

import lombok.Data;

@Data
public class RolePatchDTO {

    private String text;
}
