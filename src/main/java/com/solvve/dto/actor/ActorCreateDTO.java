package com.solvve.dto.actor;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class ActorCreateDTO {

    @NotNull
    private UUID personId;
    private Double averageRating;
    private Double averageRatingByFilm;
}
