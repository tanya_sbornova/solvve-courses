package com.solvve.dto.actor;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class ActorInLeaderBoardReadDTO {

    private UUID id;
    private String name;
    private Double averageRating;
}
