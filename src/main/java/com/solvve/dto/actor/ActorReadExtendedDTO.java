package com.solvve.dto.actor;

import com.solvve.dto.person.PersonReadDTO;
import com.solvve.dto.role.RoleReadDTO;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class ActorReadExtendedDTO {

    private UUID id;
    private List<RoleReadDTO> roles = new ArrayList<>();
    private PersonReadDTO person;
    private Double averageRating;
    private Double averageRatingByFilm;
}
