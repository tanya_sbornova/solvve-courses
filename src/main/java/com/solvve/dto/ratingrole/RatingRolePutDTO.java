package com.solvve.dto.ratingrole;

import lombok.Data;

@Data
public class RatingRolePutDTO {

    private Double rating;
}