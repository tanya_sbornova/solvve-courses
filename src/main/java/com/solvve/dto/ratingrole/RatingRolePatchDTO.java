package com.solvve.dto.ratingrole;

import lombok.Data;

@Data
public class RatingRolePatchDTO {

    private Double rating;
}
