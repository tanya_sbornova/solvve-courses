package com.solvve.dto.misprintinforole;

import com.solvve.domain.MisprintObject;
import com.solvve.domain.MisprintStatus;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.Instant;
import java.util.UUID;

@Data
public class MisprintInfoRoleReadDTO {

    private UUID id;
    private Integer startIndex;
    private Integer endIndex;
    private String text;
    private String incorrectText;
    private MisprintStatus status;
    private MisprintObject object;
    private Instant fixedDate;
    private UUID contentManagerId;
    private UUID contentId;
    private UUID registeredUserId;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
