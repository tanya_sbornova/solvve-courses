package com.solvve.dto.misprintinforole;

import com.solvve.domain.MisprintObject;
import com.solvve.domain.MisprintStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class MisprintInfoRoleCreateDTO {

    private Integer startIndex;
    private Integer endIndex;
    private String text;
    private String incorrectText;
    private MisprintStatus status;
    private MisprintObject object;
    private Instant fixedDate;
    private UUID contentId;
}
