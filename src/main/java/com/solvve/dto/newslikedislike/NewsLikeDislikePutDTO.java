package com.solvve.dto.newslikedislike;

import lombok.Data;

@Data
public class NewsLikeDislikePutDTO {

    private Boolean isLike;
}
