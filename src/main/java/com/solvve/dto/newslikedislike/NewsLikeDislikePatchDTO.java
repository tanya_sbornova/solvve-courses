package com.solvve.dto.newslikedislike;

import lombok.Data;

@Data
public class NewsLikeDislikePatchDTO {

    private Boolean isLike;
}
