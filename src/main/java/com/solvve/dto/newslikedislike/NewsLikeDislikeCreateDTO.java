package com.solvve.dto.newslikedislike;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class NewsLikeDislikeCreateDTO {

    @NotNull
    private Boolean isLike;

    @NotNull
    private UUID newsId;
}
