package com.solvve.dto.misprintinfofix;

import lombok.Data;
import java.util.UUID;

@Data
public class MisprintInfoFixFilter {

    private UUID contentId;
    private Integer startIndex;
    private Integer endIndex;
    private String incorrectText;
}
