package com.solvve.dto.misprintinfofix;

import lombok.Data;

import java.util.UUID;

@Data
public class ContentFixFilter {

    UUID misprintInfoId;
}
