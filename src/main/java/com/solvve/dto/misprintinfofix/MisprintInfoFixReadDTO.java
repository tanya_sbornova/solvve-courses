package com.solvve.dto.misprintinfofix;

import com.solvve.domain.MisprintObject;
import com.solvve.domain.MisprintStatus;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.Instant;
import java.util.UUID;

@Data
public class MisprintInfoFixReadDTO {

    private UUID id;
    private Integer startIndex;
    private Integer endIndex;
    private String text;
    private String incorrectText;
    private MisprintStatus status;
    private MisprintObject object;
    private Instant fixedDate;
    private UUID contentId;
    private UUID registeredUserId;
    private UUID contentManagerId;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}

