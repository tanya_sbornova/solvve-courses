package com.solvve.dto.misprintinfofix;

import com.solvve.domain.MisprintStatus;
import lombok.Data;

import java.time.Instant;

@Data
public class MisprintInfoFixDTO {

    private String text;
    private MisprintStatus status;
    private Instant fixedDate;
}
