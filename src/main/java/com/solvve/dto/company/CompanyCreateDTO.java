package com.solvve.dto.company;

import com.solvve.domain.Country;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CompanyCreateDTO {

    private Country countryName;

    @NotNull
    private String name;

}
