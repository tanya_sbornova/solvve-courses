package com.solvve.dto.admin;

import com.solvve.domain.Sex;
import com.solvve.domain.UserRoleType;
import com.solvve.domain.UserStatus;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDate;

@Data
public class AdminCreateDTO {

    @NotNull
    private String email;

    @NotNull
    private String encodedPassword;

    @NotNull
    private UserRoleType userRole;

    @NotNull
    private UserStatus userStatus;

    private LocalDate dateBirth;
    private String country;
    private Sex sex;

    private Instant startAt;
    private Instant finishAt;
}
