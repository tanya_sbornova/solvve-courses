package com.solvve.dto.admin;

import com.solvve.domain.UserRoleType;
import com.solvve.domain.UserStatus;
import lombok.Data;

@Data
public class AdminPatchDTO {

    private String encodedPassword;
    private UserRoleType userRole;
    private UserStatus userStatus;
}
