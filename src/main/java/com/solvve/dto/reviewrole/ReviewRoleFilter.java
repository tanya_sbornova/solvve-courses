package com.solvve.dto.reviewrole;

import com.solvve.domain.ReviewStatus;
import lombok.Data;

import java.util.UUID;

@Data
public class ReviewRoleFilter {

    private ReviewStatus status;
    private Boolean isSpoiler;
    private UUID registeredUserId;
    private UUID roleId;
}
