package com.solvve.dto.reviewrole;

import com.solvve.domain.ReviewStatus;
import lombok.Data;

@Data
public class ReviewRolePutDTO {

    private String text;
    private ReviewStatus status;
}
