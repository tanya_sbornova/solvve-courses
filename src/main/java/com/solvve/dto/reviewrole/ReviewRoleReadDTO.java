package com.solvve.dto.reviewrole;

import com.solvve.domain.ReviewStatus;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import java.time.Instant;
import java.util.UUID;

@Data
@EntityListeners(AuditingEntityListener.class)
public class ReviewRoleReadDTO {

    private UUID id;
    private String text;
    private ReviewStatus status;
    private Boolean isSpoiler;
    private UUID roleId;
    private UUID registeredUserId;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
