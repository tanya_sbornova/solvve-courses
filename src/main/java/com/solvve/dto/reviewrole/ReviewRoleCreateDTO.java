package com.solvve.dto.reviewrole;

import com.solvve.domain.ReviewStatus;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class ReviewRoleCreateDTO {

    @NotNull
    private String text;

    @NotNull
    private ReviewStatus status;

    @NotNull
    private Boolean isSpoiler;

    @NotNull
    private UUID registeredUserId;
}
