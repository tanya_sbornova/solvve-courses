package com.solvve.dto.moderator;

import com.solvve.domain.UserRoleType;
import com.solvve.domain.UserStatus;
import lombok.Data;

@Data
public class ModeratorPutDTO {

    private UserStatus userStatus;
    private UserRoleType userRole;
}

