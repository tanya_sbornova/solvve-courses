package com.solvve.dto.moderator;

import com.solvve.domain.UserRoleType;
import com.solvve.domain.UserStatus;
import lombok.Data;

@Data
public class ModeratorPatchDTO {

    private String encodedPassword;
    private UserStatus userStatus;
    private UserRoleType userRole;
}
