package com.solvve.dto.moderator;

import com.solvve.domain.UserStatus;
import lombok.Data;

@Data
public class ModeratorFilter {

    private String email;
    private UserStatus userStatus;
}
