package com.solvve.dto.moderator;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

import com.solvve.domain.Sex;
import com.solvve.domain.UserRoleType;
import com.solvve.domain.UserStatus;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;

@Data
@EntityListeners(AuditingEntityListener.class)
public class ModeratorReadDTO {

    private UUID id;
    private String email;
    private String encodedPassword;
    private UserRoleType userRole;
    private UserStatus userStatus;
    private LocalDate dateBirth;
    private String country;
    private Sex sex;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;
}
