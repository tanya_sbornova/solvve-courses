package com.solvve.dto.reviewfilmlikedislike;

import lombok.Data;

@Data
public class ReviewFilmLikeDislikePutDTO {

    private Boolean isLike;
}
