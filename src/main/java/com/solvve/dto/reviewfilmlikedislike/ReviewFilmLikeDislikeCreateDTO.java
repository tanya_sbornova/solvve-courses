package com.solvve.dto.reviewfilmlikedislike;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class ReviewFilmLikeDislikeCreateDTO {

    @NotNull
    private Boolean isLike;

    @NotNull
    private UUID reviewFilmId;
}
