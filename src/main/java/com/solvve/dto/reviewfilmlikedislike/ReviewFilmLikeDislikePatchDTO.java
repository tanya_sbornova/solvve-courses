package com.solvve.dto.reviewfilmlikedislike;

import lombok.Data;

@Data
public class ReviewFilmLikeDislikePatchDTO {

    private Boolean isLike;
}
