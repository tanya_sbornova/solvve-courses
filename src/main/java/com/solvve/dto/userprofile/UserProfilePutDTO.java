package com.solvve.dto.userprofile;

import com.solvve.domain.FilmGenre;
import lombok.Data;

@Data
public class UserProfilePutDTO {

    private Double ratingReviews;
    private Double ratingActivity;
    private FilmGenre favouriteGenre;
}
