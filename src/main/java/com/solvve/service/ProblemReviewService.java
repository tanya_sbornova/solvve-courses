package com.solvve.service;

import com.solvve.domain.ProblemReview;
import com.solvve.dto.problemreview.ProblemReviewCreateDTO;
import com.solvve.dto.problemreview.ProblemReviewPatchDTO;
import com.solvve.dto.problemreview.ProblemReviewPutDTO;
import com.solvve.dto.problemreview.ProblemReviewReadDTO;
import com.solvve.repository.ProblemReviewRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class ProblemReviewService {

    @Autowired
    private ProblemReviewRepository problemReviewRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public ProblemReviewReadDTO getProblemReview(UUID id) {

        ProblemReview problemReview = repositoryHelper.getEntityRequired(ProblemReview.class, id);
        return translationService.translate(problemReview, ProblemReviewReadDTO.class);
    }

    public List<ProblemReviewReadDTO> getProblemReviews() {
        List<ProblemReview> prvs = new ArrayList<>();
        problemReviewRepository.findAll().forEach(prvs::add);
        return translationService.translateList(prvs, ProblemReviewReadDTO.class);
    }

    public ProblemReviewReadDTO createProblemReview(ProblemReviewCreateDTO create) {

        ProblemReview problemReview = translationService.translate(create, ProblemReview.class);
        problemReview = problemReviewRepository.save(problemReview);
        return translationService.translate(problemReview, ProblemReviewReadDTO.class);
    }

    public ProblemReviewReadDTO patchProblemReview(UUID id, ProblemReviewPatchDTO patch) {

        ProblemReview problemReview = repositoryHelper.getEntityRequired(ProblemReview.class, id);
        translationService.map(patch, problemReview);
        problemReview = problemReviewRepository.save(problemReview);
        return translationService.translate(problemReview, ProblemReviewReadDTO.class);
    }

    public ProblemReviewReadDTO updateProblemReview(UUID id, ProblemReviewPutDTO put) {

        ProblemReview problemReview = repositoryHelper.getEntityRequired(ProblemReview.class, id);
        translationService.map(put, problemReview);
        problemReview = problemReviewRepository.save(problemReview);
        return translationService.translate(problemReview, ProblemReviewReadDTO.class);
    }

    public void deleteProblemReview(UUID id) {

        problemReviewRepository.delete(repositoryHelper.getEntityRequired(ProblemReview.class, id));
    }
}
