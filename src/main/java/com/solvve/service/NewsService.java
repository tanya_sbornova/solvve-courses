package com.solvve.service;

import com.solvve.domain.News;
import com.solvve.dto.news.NewsCreateDTO;
import com.solvve.dto.news.NewsPatchDTO;
import com.solvve.dto.news.NewsPutDTO;
import com.solvve.dto.news.NewsReadDTO;
import com.solvve.repository.NewsLikeDislikeRepository;
import com.solvve.repository.NewsRepository;
import com.solvve.repository.RepositoryHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@Service
public class NewsService {

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private NewsLikeDislikeRepository newsLikeDislikeRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public NewsReadDTO getNews(UUID id) {

        News news = repositoryHelper.getEntityRequired(News.class, id);
        return translationService.translate(news, NewsReadDTO.class);
    }

    public NewsReadDTO createNews(NewsCreateDTO create) {

        News news = translationService.translate(create, News.class);
        news.setCntLike(0);
        news.setCntDislike(0);
        news = newsRepository.save(news);

        return translationService.translate(news, NewsReadDTO.class);
    }

    public NewsReadDTO patchNews(UUID id, NewsPatchDTO patch) {

        News news = repositoryHelper.getEntityRequired(News.class, id);
        translationService.map(patch, news);
        news = newsRepository.save(news);
        return translationService.translate(news, NewsReadDTO.class);
    }

    public NewsReadDTO updateNews(UUID id, NewsPutDTO put) {

        News news = repositoryHelper.getEntityRequired(News.class, id);
        translationService.map(put, news);
        news = newsRepository.save(news);
        return translationService.translate(news, NewsReadDTO.class);
    }

    public void deleteNews(UUID id) {
        newsRepository.delete(repositoryHelper.getEntityRequired(News.class, id));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateCntLikeDislike(UUID newsId) {
        Integer cntLike = newsLikeDislikeRepository.calcCntOfNewsLike(newsId);
        Integer cntDislike = newsLikeDislikeRepository.calcCntOfNewsDisLike(newsId);
        News news = repositoryHelper.getEntityRequired(News.class, newsId);
        log.info("Setting new cnt likes of news: {}. Old value: {}, new value: {}", newsId,
                news.getCntLike(), cntLike);
        log.info("Setting new cnt dislikes of news: {}. Old value: {}, new value: {}", newsId,
                news.getCntDislike(), cntDislike);
        if (cntLike != null) {
            news.setCntLike(cntLike);
        }
        if (cntDislike != null) {
            news.setCntDislike(cntDislike);
        }
        newsRepository.save(news);
    }
}


