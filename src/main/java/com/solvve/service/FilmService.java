package com.solvve.service;

import com.solvve.domain.Film;
import com.solvve.domain.PageResult;
import com.solvve.domain.ReviewFilm;
import com.solvve.domain.ReviewStatus;
import com.solvve.dto.film.*;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.RatingFilmRepository;
import com.solvve.repository.RepositoryHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class FilmService {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private RatingFilmRepository ratingFilmRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public FilmReadDTO getFilm(UUID id) {

        Film film = repositoryHelper.getEntityRequired(Film.class, id);
        return translationService.translate(film, FilmReadDTO.class);
    }

    public PageResult<FilmReadDTO> getFilms(FilmFilter filter, Pageable pageable) {
        Page<Film> films = filmRepository.findByFilter(filter, pageable);
        PageResult<FilmReadDTO> pageResult = translationService.toPageResult(films, FilmReadDTO.class);
        return pageResult;
    }

    @Transactional(readOnly = true)
    public FilmReadExtendedDTO getFilmExtended(UUID id) {
        Film film = repositoryHelper.getEntityRequired(Film.class, id);
        List<ReviewFilm> reviewFilms = film.getReviewFilms()
                .stream()
                .filter(rf -> rf.getStatus() == ReviewStatus.MODERATED
                        || rf.getStatus() == ReviewStatus.FIXED)
                .collect(Collectors.toList());
        film.setReviewFilms(reviewFilms);
        return translationService.translate(film, FilmReadExtendedDTO.class);
    }

    public FilmReadDTO createFilm(FilmCreateDTO create) {

        Film film = translationService.translate(create, Film.class);
        film.setAverageRating(null);
        film.setForecastRating(null);
        film = filmRepository.save(film);
        return translationService.translate(film, FilmReadDTO.class);
    }

    public FilmReadDTO patchFilm(UUID id, FilmPatchDTO patch) {

        Film film = repositoryHelper.getEntityRequired(Film.class, id);
        translationService.map(patch, film);
        film = filmRepository.save(film);
        return translationService.translate(film, FilmReadDTO.class);
    }

    public FilmReadDTO updateFilm(UUID id, FilmPutDTO put) {

        Film film = repositoryHelper.getEntityRequired(Film.class, id);
        translationService.map(put, film);
        film = filmRepository.save(film);
        return translationService.translate(film, FilmReadDTO.class);
    }

    public void deleteFilm(UUID id) {
        filmRepository.delete(repositoryHelper.getEntityRequired(Film.class, id));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAverageRatingOfFilm(UUID filmId) {
        Double averageRating = ratingFilmRepository.calcAverageRatingOfFilm(filmId);
        Film film = repositoryHelper.getEntityRequired(Film.class, filmId);
        log.info("Setting new average rating of film: {}. Old value: {}, new value: {}", filmId,
                film.getAverageRating(), averageRating);
        film.setAverageRating(averageRating);
        filmRepository.save(film);
    }

    public List<FilmInLeaderBoardReadDTO> getFilmsLeaderBoard() {
        return filmRepository.getFilmsLeaderBoard();
    }
}
