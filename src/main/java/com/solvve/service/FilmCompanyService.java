package com.solvve.service;

import com.solvve.domain.Company;
import com.solvve.domain.Film;
import com.solvve.dto.company.CompanyReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.CompanyRepository;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class FilmCompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Transactional(readOnly = true)
    public List<CompanyReadDTO> getFilmCompanies(UUID filmId) {

        List<CompanyReadDTO> companiesReadDTO = new ArrayList<>();
        repositoryHelper.getEntityRequired(Film.class, filmId).getCompanies()
                .forEach(company -> {
                    companiesReadDTO.add(translationService.translate(company, CompanyReadDTO.class));
                });
        return companiesReadDTO;
    }

    public CompanyReadDTO getFilmCompany(UUID filmId, UUID companyId) {
        repositoryHelper.getEntityRequired(Film.class, filmId);
        repositoryHelper.getEntityRequired(Company.class, companyId);
        Company company = getFilmCompanyRequired(filmId, companyId);
        return translationService.translate(company, CompanyReadDTO.class);
    }

    private Company getFilmCompanyRequired(UUID filmId, UUID companyId) {

        Company company = transactionTemplate.execute(status -> {
            Film film = getFilmRequired(filmId);
            getCompanyRequired(companyId);
            Set<Company> companys = film.getCompanies();
            return companys.stream().filter(cm -> cm.getId()
                    .equals(companyId)).findAny().orElseThrow(() ->
                    new EntityNotFoundException(Company.class, companyId, Film.class, filmId));
        });
        return company;
    }

    private Film getFilmRequired(UUID id) {

        return filmRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException(Film.class, id));
    }

    private Company getCompanyRequired(UUID id) {

        return companyRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException(Company.class, id));
    }
}