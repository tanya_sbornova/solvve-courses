package com.solvve.service;

import com.solvve.domain.RegisteredUser;
import com.solvve.domain.UserRoleType;
import com.solvve.domain.UserStatus;
import com.solvve.dto.registereduser.RegisteredUserCreateDTO;
import com.solvve.dto.registereduser.RegisteredUserPatchDTO;
import com.solvve.dto.registereduser.RegisteredUserPutDTO;
import com.solvve.dto.registereduser.RegisteredUserReadDTO;
import com.solvve.repository.RegisteredUserRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.UUID;

@Service
public class RegisteredUserService {

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TranslationService translationService;

    public RegisteredUserReadDTO getRegisteredUser(UUID id) {

        RegisteredUser registeredUser = repositoryHelper.getEntityRequired(RegisteredUser.class, id);
        return translationService.translate(registeredUser, RegisteredUserReadDTO.class);
    }

    public RegisteredUserReadDTO createRegisteredUser(RegisteredUserCreateDTO create) {

        create.setEncodedPassword(passwordEncoder.encode(create.getEncodedPassword()));
        create.setUserRole(UserRoleType.REGISTERED_USER);
        create.setUserStatus(UserStatus.ACTIVE);
        create.setTrustLevel(3);
        RegisteredUser registeredUser = translationService.translate(create, RegisteredUser.class);
        registeredUser = registeredUserRepository.save(registeredUser);
        return translationService.translate(registeredUser, RegisteredUserReadDTO.class);
    }

    public RegisteredUserReadDTO patchRegisteredUser(UUID id, RegisteredUserPatchDTO patch) {

        RegisteredUser registeredUser = repositoryHelper.getEntityRequired(RegisteredUser.class, id);
        if (patch.getEncodedPassword() != null) {
            patch.setEncodedPassword(passwordEncoder.encode(patch.getEncodedPassword()));
        }
        translationService.map(patch, registeredUser);
        registeredUser = registeredUserRepository.save(registeredUser);
        return translationService.translate(registeredUser, RegisteredUserReadDTO.class);
    }

    public RegisteredUserReadDTO updateRegisteredUser(UUID id, RegisteredUserPutDTO put) {

        RegisteredUser registeredUser = repositoryHelper.getEntityRequired(RegisteredUser.class, id);
        translationService.map(put, registeredUser);
        registeredUser = registeredUserRepository.save(registeredUser);
        return translationService.translate(registeredUser, RegisteredUserReadDTO.class);
    }

    public void deleteRegisteredUser(UUID id) {
        registeredUserRepository.delete(repositoryHelper.getEntityRequired(RegisteredUser.class, id));
    }
}



