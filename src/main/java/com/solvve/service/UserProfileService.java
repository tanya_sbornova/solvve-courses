package com.solvve.service;

import com.solvve.domain.UserProfile;
import com.solvve.dto.userprofile.UserProfileCreateDTO;
import com.solvve.dto.userprofile.UserProfilePatchDTO;
import com.solvve.dto.userprofile.UserProfilePutDTO;
import com.solvve.dto.userprofile.UserProfileReadDTO;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserProfileService {

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public UserProfileReadDTO getUserProfile(UUID id) {

        UserProfile userProfile = repositoryHelper.getEntityRequired(UserProfile.class, id);
        return translationService.translate(userProfile, UserProfileReadDTO.class);
    }

    public UserProfileReadDTO createUserProfile(UserProfileCreateDTO create) {

        UserProfile userProfile = translationService.translate(create, UserProfile.class);
        userProfile = userProfileRepository.save(userProfile);

        return translationService.translate(userProfile, UserProfileReadDTO.class);
    }

    public UserProfileReadDTO patchUserProfile(UUID id, UserProfilePatchDTO patch) {

        UserProfile userProfile = repositoryHelper.getEntityRequired(UserProfile.class, id);
        translationService.map(patch, userProfile);
        userProfile = userProfileRepository.save(userProfile);
        return translationService.translate(userProfile, UserProfileReadDTO.class);
    }

    public UserProfileReadDTO updateUserProfile(UUID id, UserProfilePutDTO put) {

        UserProfile userProfile = repositoryHelper.getEntityRequired(UserProfile.class, id);
        translationService.map(put, userProfile);
        userProfile = userProfileRepository.save(userProfile);
        return translationService.translate(userProfile, UserProfileReadDTO.class);
    }

    public void deleteUserProfile(UUID id) {

        userProfileRepository.delete(repositoryHelper.getEntityRequired(UserProfile.class, id));
    }
}
