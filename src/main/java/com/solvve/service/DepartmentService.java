package com.solvve.service;

import com.solvve.domain.Department;
import com.solvve.dto.department.*;
import com.solvve.repository.DepartmentRepository;
import com.solvve.repository.RepositoryHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private TranslationService translationService;

    public DepartmentReadDTO getDepartment(UUID id) {

        Department department = repositoryHelper.getEntityRequired(Department.class, id);
        return translationService.translate(department, DepartmentReadDTO.class);
    }

    public DepartmentReadDTO createDepartment(DepartmentCreateDTO create) {
        Department department = translationService.translate(create, Department.class);
        department = departmentRepository.save(department);
        return translationService.translate(department, DepartmentReadDTO.class);
    }

    public void deleteDepartment(UUID id) {
        departmentRepository.delete(repositoryHelper.getEntityRequired(Department.class, id));
    }

}
