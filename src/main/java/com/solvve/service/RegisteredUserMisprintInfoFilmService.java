package com.solvve.service;

import com.solvve.domain.RegisteredUser;
import com.solvve.domain.MisprintInfoFilm;
import com.solvve.dto.misprintinfofilm.MisprintInfoFilmCreateDTO;
import com.solvve.dto.misprintinfofilm.MisprintInfoFilmReadDTO;
import com.solvve.repository.MisprintInfoFilmRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class RegisteredUserMisprintInfoFilmService {

    @Autowired
    private MisprintInfoFilmRepository misprintInfoFilmRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Transactional(readOnly = true)
    public List<MisprintInfoFilmReadDTO> getRegisteredUserMisprintInfoFilms(UUID registeredUserId) {

        List<MisprintInfoFilmReadDTO> misprintInfoFilmsReadDTO = new ArrayList<>();
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId).getMisprintInfoFilms()
                .forEach(misprintInfoFilm -> {
                    misprintInfoFilmsReadDTO.add(translationService.translate(misprintInfoFilm,
                            MisprintInfoFilmReadDTO.class));
                });
        return misprintInfoFilmsReadDTO;
    }

    public MisprintInfoFilmReadDTO getRegisteredUserMisprintInfoFilm(UUID registeredUserId,
                                                                     UUID misprintInfoFilmId) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                MisprintInfoFilm.class, registeredUserId, misprintInfoFilmId);
        return translationService.translate(repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                MisprintInfoFilm.class,
                registeredUserId, misprintInfoFilmId), MisprintInfoFilmReadDTO.class);
    }

    public MisprintInfoFilmReadDTO createRegisteredUserMisprintInfoFilm(UUID filmId,
                                                                        MisprintInfoFilmCreateDTO
                                                                                misprintInfoFilmCreate) {

        MisprintInfoFilm misprintInfoFilm = translationService.translate(misprintInfoFilmCreate,
                MisprintInfoFilm.class);
        misprintInfoFilm.setRegisteredUser(repositoryHelper.getReferenceIfExists(RegisteredUser.class, filmId));
        misprintInfoFilm = misprintInfoFilmRepository.save(misprintInfoFilm);
        return translationService.translate(misprintInfoFilm, MisprintInfoFilmReadDTO.class);
    }

    public void deleteRegisteredUserMisprintInfoFilm(UUID registeredUserId, UUID misprintInfoFilmId) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        misprintInfoFilmRepository.delete(repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                MisprintInfoFilm.class, registeredUserId, misprintInfoFilmId));
    }
}


