package com.solvve.service;

import com.solvve.domain.Film;
import com.solvve.domain.RatingFilm;
import com.solvve.dto.ratingfilm.RatingFilmCreateDTO;
import com.solvve.dto.ratingfilm.RatingFilmPatchDTO;
import com.solvve.dto.ratingfilm.RatingFilmPutDTO;
import com.solvve.dto.ratingfilm.RatingFilmReadDTO;
import com.solvve.repository.RatingFilmRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class FilmRatingFilmService {

    @Autowired
    private RatingFilmRepository ratingFilmRepository;

    @Autowired
    private RatingFilmService ratingFilmService;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Transactional(readOnly = true)
    public List<RatingFilmReadDTO> getFilmRatingFilms(UUID filmId) {
        List<RatingFilmReadDTO> ratingFilmsReadDTO = new ArrayList<>();
        repositoryHelper.getEntityRequired(Film.class, filmId).getRatingFilms()
                .forEach(ratingFilm -> {
                    ratingFilmsReadDTO.add(translationService.translate(ratingFilm, RatingFilmReadDTO.class));
                });
        return ratingFilmsReadDTO;
    }

    @Transactional(readOnly = true)
    public RatingFilmReadDTO getFilmRatingFilm(UUID filmId, UUID ratingFilmId)  {
        repositoryHelper.getEntityRequired(Film.class, filmId);
        repositoryHelper.getEntityRequired(RatingFilm.class, ratingFilmId);
        return translationService.translate(repositoryHelper.getEntityAssociationRequired(Film.class, RatingFilm.class,
                filmId, ratingFilmId), RatingFilmReadDTO.class);
    }

    public RatingFilmReadDTO createFilmRatingFilm(UUID filmId, RatingFilmCreateDTO ratingFilmCreate) {

        RatingFilm ratingFilm = translationService.translate(ratingFilmCreate, RatingFilm.class);
        Film film = repositoryHelper.getReferenceIfExists(Film.class, filmId);
        ratingFilm.setFilm(film);
        ratingFilm = ratingFilmRepository.save(ratingFilm);
        return translationService.translate(ratingFilm, RatingFilmReadDTO.class);
    }

    public RatingFilmReadDTO patchFilmRatingFilm(UUID filmId, UUID ratingFilmId, RatingFilmPatchDTO patch) {
        repositoryHelper.getEntityRequired(Film.class, filmId);
        repositoryHelper.getEntityAssociationRequired(Film.class, RatingFilm.class,
                filmId, ratingFilmId);
        return  ratingFilmService.patchRatingFilm(ratingFilmId, patch);
    }

    public RatingFilmReadDTO updateFilmRatingFilm(UUID filmId, UUID ratingFilmId, RatingFilmPutDTO put) {
        repositoryHelper.getEntityRequired(Film.class, filmId);
        repositoryHelper.getEntityAssociationRequired(Film.class, RatingFilm.class,
                filmId, ratingFilmId);
        return  ratingFilmService.updateRatingFilm(ratingFilmId, put);
    }

    public void deleteFilmRatingFilm(UUID filmId, UUID ratingFilmId) {
        repositoryHelper.getEntityRequired(Film.class, filmId);
        ratingFilmRepository.delete(repositoryHelper.getEntityAssociationRequired(Film.class, RatingFilm.class,
                filmId, ratingFilmId));
    }
}
