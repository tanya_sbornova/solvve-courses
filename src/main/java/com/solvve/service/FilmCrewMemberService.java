package com.solvve.service;

import com.solvve.domain.CrewMember;
import com.solvve.domain.Film;
import com.solvve.dto.crewmember.CrewMemberPatchDTO;
import com.solvve.dto.crewmember.CrewMemberPutDTO;
import com.solvve.dto.crewmember.CrewMemberReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.CrewMemberRepository;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class FilmCrewMemberService {

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Autowired
    private CrewMemberService crewMemberService;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Transactional(readOnly = true)
    public List<CrewMemberReadDTO> getFilmCrewMembers(UUID filmId) {

        List<CrewMemberReadDTO> crewMembersReadDTO = new ArrayList<>();
        repositoryHelper.getEntityRequired(Film.class, filmId).getCrewMembers()
                .forEach(crewMember -> {
                    crewMembersReadDTO.add(translationService.translate(crewMember, CrewMemberReadDTO.class));
                });
        return crewMembersReadDTO;
    }

    public CrewMemberReadDTO getFilmCrewMember(UUID filmId, UUID crewMemberId) {
        repositoryHelper.getEntityRequired(Film.class, filmId);
        repositoryHelper.getEntityRequired(CrewMember.class, crewMemberId);
        CrewMember crewMember = getFilmCrewMemberRequired(filmId, crewMemberId);
        return translationService.translate(crewMember, CrewMemberReadDTO.class);
    }

    public CrewMemberReadDTO patchFilmCrewMember(UUID filmId, UUID crewMemberId, CrewMemberPatchDTO patch) {
        repositoryHelper.getEntityRequired(Film.class, filmId);
        repositoryHelper.getEntityRequired(CrewMember.class, crewMemberId);
        return crewMemberService.patchCrewMember(crewMemberId, patch);
    }

    public CrewMemberReadDTO updateFilmCrewMember(UUID filmId, UUID crewMemberId, CrewMemberPutDTO put) {
        repositoryHelper.getEntityRequired(Film.class, filmId);
        repositoryHelper.getEntityRequired(CrewMember.class, crewMemberId);
        return crewMemberService.updateCrewMember(crewMemberId, put);
    }

    private CrewMember getFilmCrewMemberRequired(UUID filmId, UUID crewMemberId) {

        CrewMember crewMember = transactionTemplate.execute(status -> {
            Film film = getFilmRequired(filmId);
            getCrewMemberRequired(crewMemberId);
            Set<CrewMember> crewMembers = film.getCrewMembers();
            return crewMembers.stream().filter(cm -> cm.getId()
                    .equals(crewMemberId)).findAny().orElseThrow(() ->
                    new EntityNotFoundException(CrewMember.class, crewMemberId, Film.class, filmId));
        });
        return crewMember;
    }

    private Film getFilmRequired(UUID id) {

        return filmRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException(Film.class, id));
    }

    private CrewMember getCrewMemberRequired(UUID id) {

        return crewMemberRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException(CrewMember.class, id));
    }
}
