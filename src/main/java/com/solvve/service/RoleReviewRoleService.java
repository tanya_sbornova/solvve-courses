package com.solvve.service;

import com.solvve.domain.ReviewRole;
import com.solvve.domain.Role;
import com.solvve.dto.reviewrole.ReviewRoleCreateDTO;
import com.solvve.dto.reviewrole.ReviewRolePatchDTO;
import com.solvve.dto.reviewrole.ReviewRolePutDTO;
import com.solvve.dto.reviewrole.ReviewRoleReadDTO;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.ReviewRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class RoleReviewRoleService {

    @Autowired
    private ReviewRoleRepository reviewRoleRepository;

    @Autowired
    private ReviewRoleService reviewRoleService;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Transactional(readOnly = true)
    public List<ReviewRoleReadDTO> getRoleReviewRoles(UUID roleId) {
        List<ReviewRoleReadDTO> reviewRolesReadDTO = new ArrayList<>();
        repositoryHelper.getEntityRequired(Role.class, roleId).getReviewRoles()
                .forEach(reviewRole -> {
                    reviewRolesReadDTO.add(translationService.translate(reviewRole,
                            ReviewRoleReadDTO.class));
                });
        return  reviewRolesReadDTO;
    }

    @Transactional(readOnly = true)
    public List<ReviewRoleReadDTO> getRoleReviewsRoleOfCurrentRegisteredUser(UUID registeredUserId,
                                                                             UUID roleId) {
        List<ReviewRoleReadDTO> reviewRolesReadDTO = new ArrayList<>();
        repositoryHelper.getEntityRequired(Role.class, roleId).getReviewRoles()
                .stream()
                .filter(rv -> rv.getRegisteredUser().getId().equals(registeredUserId))
                .forEach(reviewRole -> {
                    reviewRolesReadDTO.add(translationService.translate(reviewRole, ReviewRoleReadDTO.class));
                });
        return reviewRolesReadDTO;
    }

    public ReviewRoleReadDTO getRoleReviewRole(UUID roleId, UUID reviewRoleId) {
        repositoryHelper.getEntityRequired(Role.class, roleId);
        repositoryHelper.getEntityRequired(ReviewRole.class, reviewRoleId);
        return translationService.translate(repositoryHelper.getEntityAssociationRequired(Role.class, ReviewRole.class,
                roleId, reviewRoleId), ReviewRoleReadDTO.class);
    }

    public ReviewRoleReadDTO createRoleReviewRole(UUID roleId, ReviewRoleCreateDTO reviewRoleCreate) {

        ReviewRole reviewRole = translationService.translate(reviewRoleCreate, ReviewRole.class);
        reviewRole.setRole(repositoryHelper.getReferenceIfExists(Role.class, roleId));
        reviewRole = reviewRoleRepository.save(reviewRole);
        return translationService.translate(reviewRole, ReviewRoleReadDTO.class);
    }

    public ReviewRoleReadDTO patchRoleReviewRole(UUID roleId, UUID reviewRoleId, ReviewRolePatchDTO patch) {
        repositoryHelper.getEntityRequired(Role.class, roleId);
        repositoryHelper.getEntityAssociationRequired(Role.class, ReviewRole.class,
                roleId, reviewRoleId);
        return reviewRoleService.patchReviewRole(reviewRoleId, patch);
    }

    public ReviewRoleReadDTO updateRoleReviewRole(UUID roleId, UUID reviewRoleId, ReviewRolePutDTO put) {
        repositoryHelper.getEntityRequired(Role.class, roleId);
        repositoryHelper.getEntityAssociationRequired(Role.class, ReviewRole.class,
                roleId, reviewRoleId);
        return reviewRoleService.updateReviewRole(reviewRoleId, put);
    }

    public void deleteRoleReviewRole(UUID roleId, UUID reviewRoleId) {
        repositoryHelper.getEntityRequired(Role.class, roleId);
        reviewRoleRepository.delete(repositoryHelper.getEntityAssociationRequired(Role.class, ReviewRole.class,
                roleId, reviewRoleId));
    }
}


