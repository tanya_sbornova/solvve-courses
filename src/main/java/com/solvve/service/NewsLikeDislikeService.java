package com.solvve.service;

import com.solvve.domain.NewsLikeDislike;
import com.solvve.dto.newslikedislike.NewsLikeDislikePatchDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikePutDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikeReadDTO;
import com.solvve.repository.NewsLikeDislikeRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.UUID;

@Service
public class NewsLikeDislikeService {

    @Autowired
    private NewsLikeDislikeRepository newsLikeDislikeRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public NewsLikeDislikeReadDTO getNewsLikeDislike(UUID id) {

        NewsLikeDislike newsLikeDislike = repositoryHelper.getEntityRequired(NewsLikeDislike.class, id);
        return translationService.translate(newsLikeDislike, NewsLikeDislikeReadDTO.class);
    }

    public NewsLikeDislikeReadDTO patchNewsLikeDislike(UUID id, NewsLikeDislikePatchDTO patch) {

        NewsLikeDislike newsLikeDislike = repositoryHelper.getEntityRequired(NewsLikeDislike.class, id);
        translationService.map(patch, newsLikeDislike);
        newsLikeDislike = newsLikeDislikeRepository.save(newsLikeDislike);
        return translationService.translate(newsLikeDislike, NewsLikeDislikeReadDTO.class);
    }

    public NewsLikeDislikeReadDTO updateNewsLikeDislike(UUID id, NewsLikeDislikePutDTO put) {

        NewsLikeDislike newsLikeDislike = repositoryHelper.getEntityRequired(NewsLikeDislike.class, id);
        translationService.map(put, newsLikeDislike);
        newsLikeDislike = newsLikeDislikeRepository.save(newsLikeDislike);
        return translationService.translate(newsLikeDislike, NewsLikeDislikeReadDTO.class);
    }

    public void deleteNewsLikeDislike(UUID id) {

        newsLikeDislikeRepository.delete(repositoryHelper.getEntityRequired(NewsLikeDislike.class, id));
    }
}


