package com.solvve.service.importer;

import com.solvve.client.themoviedb.TheMovieDbClient;
import com.solvve.client.themoviedb.dto.*;
import com.solvve.domain.*;
import com.solvve.exception.ImportAlreadyPerformedException;
import com.solvve.exception.ImportedEntityAlreadyExistException;
import com.solvve.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Slf4j
@Service
public class MovieImporterService {

    @Autowired
    private TheMovieDbClient movieDbClient;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ExternalSystemImportService externalSystemImportService;

    @Autowired
    private PersonImporterService personImporterService;

    @Transactional
    public UUID importMovie(String movieExternalId) throws ImportedEntityAlreadyExistException,
            ImportAlreadyPerformedException {

        log.info("Importing movie with external id={}", movieExternalId);
        externalSystemImportService.validateNotImported(Film.class, movieExternalId);
        MovieReadDTO read = movieDbClient.getMovie(movieExternalId, null);
        Film existingFilm = filmRepository.findByTitle(read.getTitle());
        if (existingFilm != null) {
            throw new ImportedEntityAlreadyExistException(Film.class, existingFilm.getId(),
                    "Movie witn name = " + read.getTitle() + " already exists");
        }
        Film film = new Film();
        film.setTitle(read.getTitle());
        film.setText(read.getTitle());
        film.setDatePrime(read.getReleaseDate());
        film.setBudget(read.getBudget());
        film.setStatus(convertStringToFilmStatus(read.getStatus()));
        film = filmRepository.save(film);
        film.setDescription(read.getOverview());

        Set<CrewMember> crewMembers = importCreditsOfMovieIfNeeded(movieExternalId, film);
        film.setCrewMembers(crewMembers);
        Set<Company> companies = importCompaniesOfMovieIfNeeded(read.getProductionCompanies(), movieExternalId, film);
        film.setCompanies(companies);
        film = filmRepository.save(film);

        externalSystemImportService.createExternalSystemImport(film, movieExternalId);
        log.info("Movie with external id={} imported", movieExternalId);
        return film.getId();
    }

    @Transactional
    public Set<Company> importCompaniesOfMovieIfNeeded(List<ProductionCompanyReadDTO> companiesToImport,
                                                       String movieExternalId, Film film) {
        int successfullyImported = 0;
        int successfullyUpdated = 0;
        int skipped = 0;
        int failed = 0;
        Company company;
        Set<Company> companiesResult = new HashSet<>();
        for (ProductionCompanyReadDTO companyRead : companiesToImport) {
            try {
                Company existingCompany = companyRepository
                        .findByNameAndCountryName(companyRead.getName(),
                                convertStringToCountryType(companyRead.getOriginCountry()));
                if (existingCompany != null) {
                    log.info("Company with name = " + existingCompany.getName()
                            + " and origin country = " + existingCompany.getCountryName() + " already exists,"
                            + " it will be updated");
                    company = existingCompany;
                    successfullyUpdated++;
                } else {
                    company = createCompany(companyRead, film);
                    log.info("Company witn name = " + company.getName()
                            + " and origin country = " + company.getCountryName() + " imported");
                    successfullyImported++;
                }
                Set<Film> films = company.getFilms();
                if (!films.contains(film)) {
                    films.add(film);
                    company.setFilms(films);
                    company = companyRepository.save(company);
                }
                companiesResult.add(company);
            } catch (IllegalArgumentException e) {
                log.info("Can't import company with name ={} for movie id={}, title={}, incorrect Country code, "
                        + "it will be skipped", companyRead.getName(), movieExternalId, film.getTitle());
                skipped++;
            } catch (Exception e) {
                log.warn("Failed to import company with job ={} for movie id={}, title={} : {}",
                        companyRead.getName(), movieExternalId, film.getTitle(), e);
                failed++;
            }
        }
        log.info("Total companies to import: {}, successfully imported: {}, successfully updated: {},"
                        + " skipped: {}, failed: {} for movie id={}",
                companiesToImport.size(), successfullyImported, successfullyUpdated,
                skipped, failed, movieExternalId);
        return companiesResult;
    }

    @Transactional
    public Set<CrewMember> importCreditsOfMovieIfNeeded(String movieExternalId, Film film) {

        log.info("Importing crew members and actors of film with id={}, title={}", film.getId(), film.getTitle());
        MovieCreditsDTO read = movieDbClient.getCreditsOfMovie(movieExternalId, null);

        if (CollectionUtils.isNotEmpty(read.getCast())) {
            log.info("Before read.getCast()");
            List<CastReadDTO> castToImport = read.getCast();
            log.info("Before read.getCast()  size" + castToImport.size());
            importActorsOfMovieIfNeeded(read.getCast(), movieExternalId, film);
        }

        if (CollectionUtils.isNotEmpty(read.getCrew())) {
            return importCrewMembersOfMovieIfNeeded(read.getCrew(), movieExternalId, film);
        }
        return null;
    }

    @Transactional
    public Set<CrewMember> importCrewMembersOfMovieIfNeeded(List<CrewReadDTO> crewMembersToImport,
                                                            String movieExternalId, Film film) {

        int successfullyImported = 0;
        int successfullyUpdated = 0;
        int skipped = 0;
        int failed = 0;
        CrewMember cm;
        Department dep;
        Set<CrewMember> crewMembersResult = new HashSet<>();
        for (CrewReadDTO cr : crewMembersToImport) {
            dep = null;
            try {
                DepartmentType depType = convertStringToDepartmentType(cr.getDepartment());
                Department existingDepartment = departmentRepository.findByDepartmentType(depType);
                if (existingDepartment != null) {
                    log.info("Department with type = " + existingDepartment.getDepartmentType()
                            + " already exists, it will be updated");
                    dep = existingDepartment;
                    successfullyUpdated++;
                } else {
                    dep = createDepartment(cr);
                    log.info("Department with type = " + dep.getDepartmentType()
                            + " imported");
                    successfullyImported++;
                }
            } catch (IllegalArgumentException e) {
                log.info("Can't import department with type = {} for movie id={}, title={}, incorrect DepartmentType, "
                        + "it will be skipped", cr.getDepartment(), movieExternalId, film.getTitle());
                skipped++;
            } catch (Exception e) {
                log.warn("Failed to import department with type ={} for movie id={}, title={} : {}",
                        cr.getDepartment(), movieExternalId, film.getTitle(), e);
                failed++;
            }
            try {
                CrewMemberType cmType = convertStringToCrewMemberType(cr.getJob());
                CrewMember existingCrewMember = crewMemberRepository
                        .findByPersonNameAndCrewMemberType(cr.getName(), cmType);
                if (existingCrewMember != null) {
                    log.info("CrewMember with name = " + existingCrewMember.getPerson().getName()
                            + " and type = " + existingCrewMember.getCrewMemberType() + " already exists,"
                            + " it will be updated");
                    cm = existingCrewMember;
                    cm.setDepartment(dep);
                    successfullyUpdated++;
                } else {
                    cm = createCrewMember(cmType, dep, cr);
                    log.info("CrewMember witn name = " + cm.getPerson().getName()
                            + " and type = " + cm.getCrewMemberType() + " imported");
                    successfullyImported++;
                }
                Set<Film> films = cm.getFilms();
                if (!films.contains(film)) {
                    films.add(film);
                    cm.setFilms(films);
                    cm = crewMemberRepository.save(cm);
                    crewMembersResult.add(cm);
                }
            } catch (IllegalArgumentException e) {
                log.info("Can't import crew with job ={} for movie id={}, title={}, incorrect CrewMemberType, "
                        + "it will be skipped", cr.getJob(), movieExternalId, film.getTitle());
                skipped++;
            } catch (Exception e) {
                log.warn("Failed to import crew with job ={} for movie id={}, title={} : {}",
                        cr.getJob(), movieExternalId, film.getTitle(), e);
                failed++;
            }
        }
        log.info("Total crew members to import: {}, successfully imported: {}, successfully updated: {},"
                        + " skipped: {}, failed: {} for movie id={}",
                crewMembersToImport.size(), successfullyImported, successfullyUpdated,
                skipped, failed, movieExternalId);
        return crewMembersResult;
    }

    @Transactional
    public void importActorsOfMovieIfNeeded(List<CastReadDTO> castToImport,
                                            String movieExternalId, Film film) {
        int successfullyImported = 0;
        int successfullyUpdated = 0;
        int skipped = 0;
        int failed = 0;
        Actor actor;
        Role role;
        for (CastReadDTO cast : castToImport) {
            try {
                Actor existingActor = actorRepository.findByPersonName(cast.getName());
                if (existingActor != null) {
                    log.info("Actor witn name = " + existingActor.getPerson().getName()
                            + " already exists, it will be updated");
                    actor = existingActor;
                    successfullyUpdated++;
                } else {
                    actor = new Actor();
                    Person person = personImporterService.importPersonIfNeeded(cast.getId());
                    actor.setPerson(person);
                    actor = actorRepository.save(actor);
                    log.info("Actor witn name = " + actor.getPerson().getName()
                            + " imported");
                    successfullyImported++;
                }
                log.info("Role name = " + cast.getCharacter());
                Role existingRole = roleRepository.findRoleByFilmIdAndNameRole(film.getId(), cast.getCharacter());

                if (existingRole != null) {
                    log.info("Role " + existingRole.getText()
                            + " already exists, it will be updated");
                    role = existingRole;
                    role.setActor(actor);
                    role.setFilm(film);
                    roleRepository.save(role);
                } else {
                    role = new Role();
                    role.setText(cast.getCharacter());
                    role.setActor(actor);
                    role.setFilm(film);
                    role = roleRepository.save(role);
                    log.info("Role " + role.getText() + " imported");
                }
            } catch (Exception e) {
                log.warn("Failed to import actor for movie id={}, title={} : {}",
                        movieExternalId, film.getTitle(), e);
                failed++;
            }
        }
        log.info("Total actors to import: {}, successfully imported: {}, successfully updated: {},"
                        + " skipped: {}, failed: {} for movie id={}",
                castToImport.size(), successfullyImported, successfullyUpdated,
                skipped, failed, movieExternalId);
    }

    private CrewMember createCrewMember(CrewMemberType cmType, Department department, CrewReadDTO cr)
            throws ImportAlreadyPerformedException {
        CrewMember cm = new CrewMember();
        cm.setCrewMemberType(cmType);
        cm.setDepartment(department);
        Person person = personImporterService.importPersonIfNeeded(cr.getId());
        cm.setPerson(person);
        return crewMemberRepository.save(cm);
    }

    private Department createDepartment(CrewReadDTO cr) {
        Department department;
        department = new Department();
        department.setDepartmentType(convertStringToDepartmentType(cr.getDepartment()));
        return departmentRepository.save(department);
    }

    private Company createCompany(ProductionCompanyReadDTO read, Film film) {
        Company company = new Company();
        company.setCountryName(convertStringToCountryType(read.getOriginCountry()));
        company.setName(read.getName());
        Set<Film> films = new HashSet<>();
        films.add(film);
        company.setFilms(films);
        return companyRepository.save(company);
    }

    private CrewMemberType convertStringToCrewMemberType(String crew) {
        return CrewMemberType.valueOf(crew
                .replace(' ', '_').replace('-', '_').toUpperCase());
    }

    private DepartmentType convertStringToDepartmentType(String department) {
        return DepartmentType.valueOf(department
                .replace(' ', '_').replace('-', '_').toUpperCase());
    }

    private FilmStatus convertStringToFilmStatus(String status) {
        return FilmStatus.valueOf(status
                .replace(' ', '_').replace('-', '_').toUpperCase());
    }

    private Country convertStringToCountryType(String country) {
        return Country.valueOf(country);
    }
}



