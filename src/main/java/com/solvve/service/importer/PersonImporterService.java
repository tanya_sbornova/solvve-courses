package com.solvve.service.importer;

import com.solvve.client.themoviedb.TheMovieDbClient;
import com.solvve.client.themoviedb.dto.PersonReadDTO;
import com.solvve.domain.Person;
import com.solvve.domain.Sex;
import com.solvve.exception.ImportAlreadyPerformedException;
import com.solvve.repository.PersonRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@Service
public class PersonImporterService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private TheMovieDbClient movieDbClient;

    @Autowired
    private ExternalSystemImportService externalSystemImportService;

    @Transactional
    public Person importPersonIfNeeded(String personExternalId) throws ImportAlreadyPerformedException {

        log.info("Importing person with external id={}", personExternalId);

        UUID existingPersonId = externalSystemImportService.getImportedEntityId(Person.class, personExternalId);
        if (existingPersonId != null) {
            Person person = personRepository.findById(existingPersonId).get();
            if (person != null) {
                log.info("Person witn  external id = " + personExternalId + " already exists");
                return person;
            }
        }

        UUID personId = importPerson(personExternalId);
        Person person = personRepository.findById(personId).get();

        log.info("Person with external id={} imported", personExternalId);

        return person;
    }

    private UUID importPerson(String personExternalId) {

        PersonReadDTO read = movieDbClient.getPerson(personExternalId, null);
        Person existingPerson = personRepository.findByName(read.getName());
        if (existingPerson != null) {
            log.info("Person witn name = " + read.getName() + " already exists");
            return existingPerson.getId();
        }
        Person person = new Person();
        person.setName(read.getName());
        if (read.getGender().equals("1")) {
            person.setSex(Sex.FEMALE);
        } else if (read.getGender().equals("2")) {
            person.setSex(Sex.MALE);
        }
        person.setPlaceBirth(read.getPlaceOfBirth());
        person.setDateBirth(read.getBirthday());
        person.setDateDeath(read.getDeathday());
        person.setBiography(read.getBiography());

        person = personRepository.save(person);
        externalSystemImportService.createExternalSystemImport(person, personExternalId);
        return person.getId();
    }
}