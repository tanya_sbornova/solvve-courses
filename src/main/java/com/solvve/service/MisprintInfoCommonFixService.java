package com.solvve.service;

import com.solvve.domain.*;
import com.solvve.dto.misprintinfofix.MisprintInfoFixDTO;
import com.solvve.dto.misprintinfofix.MisprintInfoFixReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.EntityWrongStatusException;
import com.solvve.repository.MisprintInfoCommonRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Supplier;

@Service
public class MisprintInfoCommonFixService {

    @Autowired
    private MisprintInfoCommonRepository mpFixRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Transactional
    public List<MisprintInfoFixReadDTO> fixContent(UUID contentManagerId,
                                                   UUID id,
                                                   MisprintInfoFixDTO fixDTO) {
        MisprintInfoCommon misprintInfo = repositoryHelper.getEntityRequired(MisprintInfoCommon.class, id);

        checkMisprintInfo(misprintInfo.getId(), misprintInfo.getStatus());

        boolean resultUpdate;
        Content content;
        switch (misprintInfo.getObjectType()) {
          case FILM:
              Film film = repositoryHelper.getEntityRequired(Film.class, misprintInfo.getObjectId());
              resultUpdate = updateContent(film::getText, film::setText,
                      new HandleContent()::handleFilm, misprintInfo, fixDTO);
              content = film;
              break;
          case ROLE:
              Role role = repositoryHelper.getEntityRequired(Role.class, misprintInfo.getObjectId());
              resultUpdate = updateContent(role::getText, role::setText,
                      new HandleContent()::handleRole, misprintInfo, fixDTO);
              content = role;
              break;
          case NEWS:
              News news = repositoryHelper.getEntityRequired(News.class, misprintInfo.getObjectId());
              resultUpdate = updateContent(news::getText, news::setText,
                      new HandleContent()::handleNews, misprintInfo, fixDTO);
              content = news;
              break;
          default:
              throw new EntityNotFoundException("Unknown content type "
                      + misprintInfo.getObjectType() + " in misprintInfo " + misprintInfo.getId());
        }
        if (!resultUpdate) {
            return cancelMisprintInfo(misprintInfo);
        }
        repositoryHelper.saveEntity(content);

        return updateSimilarMisprints(misprintInfo, fixDTO, contentManagerId);
    }

    private List<MisprintInfoFixReadDTO> updateSimilarMisprints(MisprintInfoCommon misprintInfo,
                                                                MisprintInfoFixDTO fixDTO,
                                                                UUID contentManagerId) {

        UUID objectId = misprintInfo.getObjectId();
        String replacement = (fixDTO.getText() != null) ? fixDTO.getText() : misprintInfo.getText();
        Instant fixDate = Instant.now();

        ContentManager contentManager = repositoryHelper.getReferenceIfExists(ContentManager.class,
                contentManagerId);

        List<MisprintInfoFixReadDTO> similarMisprints = new ArrayList<>();
        mpFixRepository.getMisprintsInfoCommon(objectId, misprintInfo.getIncorrectText(),
                misprintInfo.getStartIndex(), misprintInfo.getEndIndex())
                .forEach(mpInfo -> {
                    mpInfo.setStatus(MisprintStatus.FIXED);
                    mpInfo.setFixedDate(fixDate);
                    mpInfo.setText(replacement);
                    mpInfo.setContentManager(contentManager);
                    similarMisprints.add(translationService.translate(mpFixRepository.save(mpInfo),
                            MisprintInfoFixReadDTO.class));
                });
        return similarMisprints;
    }

    private List<MisprintInfoFixReadDTO> cancelMisprintInfo(MisprintInfoCommon misprintInfo) {
        misprintInfo.setStatus(MisprintStatus.CANCELLED);
        misprintInfo.setFixedDate(Instant.now());
        misprintInfo = mpFixRepository.save(misprintInfo);
        return List.of(translationService.translate(misprintInfo, MisprintInfoFixReadDTO.class));
    }

    private <T> boolean updateContent(Supplier<T> getter, Consumer<T> setter,
                                      TriFunction<T, MisprintInfoCommon, MisprintInfoFixDTO, T> handleMethod,
                                      MisprintInfoCommon misprintInfo,
                                      MisprintInfoFixDTO fixDTO) {

        T resultHandle = handleMethod.apply(getter.get(), misprintInfo, fixDTO);
        if (resultHandle != null) {
            setter.accept(resultHandle);
            return true;
        }
        return false;
    }

    static class HandleContent {

        String handleCommon(String text, MisprintInfoCommon misprintInfo, MisprintInfoFixDTO fixDTO) {

            int startIndex = misprintInfo.getStartIndex();
            int endIndex = misprintInfo.getEndIndex();
            String replacement = (fixDTO.getText() != null) ? fixDTO.getText() : misprintInfo.getText();

            if (text.substring(startIndex, endIndex).contains(misprintInfo.getIncorrectText())) {
                return prepareText(text, replacement, startIndex, endIndex);
            }
            return null;
        }

        String handleFilm(String text, MisprintInfoCommon misprintInfo, MisprintInfoFixDTO fixDTO) {
            return handleCommon(text, misprintInfo, fixDTO);
        }

        String handleNews(String text, MisprintInfoCommon misprintInfo, MisprintInfoFixDTO fixDTO) {
            return handleCommon(text, misprintInfo, fixDTO);
        }

        String handleRole(String text, MisprintInfoCommon misprintInfo, MisprintInfoFixDTO fixDTO) {
            return handleCommon(text, misprintInfo, fixDTO);
        }

        private String prepareText(String description, String replacement, Integer startIndex, Integer endIndex) {
            StringBuilder builder = new StringBuilder(description);
            builder.replace(startIndex, endIndex, replacement);
            return builder.toString();
        }
    }

    private void checkMisprintInfo(UUID id, MisprintStatus status) {
        if (status != MisprintStatus.NEED_TO_FIX) {
            throw new EntityWrongStatusException(MisprintInfoFilm.class, id, status, MisprintStatus.NEED_TO_FIX);
        }
    }

    interface TriFunction<T, U, V, R> {
        R apply(T t, U u, V v);
    }
}
