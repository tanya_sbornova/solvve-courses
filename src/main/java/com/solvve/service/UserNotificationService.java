package com.solvve.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class UserNotificationService {

    public void notifyOnReviewStatusChangedToModerated(UUID reviewId) {
        log.info("reviewId {}", reviewId);
    }
}
