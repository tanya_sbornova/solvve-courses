package com.solvve.service;

import com.solvve.domain.Actor;
import com.solvve.domain.PageResult;
import com.solvve.domain.Role;
import com.solvve.dto.role.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.LinkDuplicatedException;
import com.solvve.repository.ActorRepository;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public RoleReadDTO getRole(UUID id) {

        Role role = repositoryHelper.getEntityRequired(Role.class, id);
        return translationService.translate(role, RoleReadDTO.class);
    }

    public PageResult<RoleReadDTO> getRoles(RoleFilter filter, Pageable pageable) {
        Page<Role> roles = roleRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(roles, RoleReadDTO.class);
    }

    @Transactional(readOnly = true)
    public RoleReadExtendedDTO getRoleExtended(UUID id) {
        Role role = repositoryHelper.getEntityRequired(Role.class, id);
        return translationService.translate(role, RoleReadExtendedDTO.class);
    }

    @Transactional(readOnly = true)
    public List<RoleReadDTO> getActorRoles(UUID actorId) {
        List<Role> roles = repositoryHelper.getEntityRequired(Actor.class, actorId).getRoles();
        return translationService.translateList(roles, RoleReadDTO.class);
    }

    public RoleReadDTO createRole(RoleCreateDTO create) {

        Role role = translationService.translate(create, Role.class);
        role = roleRepository.save(role);
        return translationService.translate(role, RoleReadDTO.class);
    }

    @Transactional
    public List<RoleReadDTO> addRoleToActor(UUID actorId, UUID id) {

        Actor actor = repositoryHelper.getEntityRequired(Actor.class, actorId);
        Role role = repositoryHelper.getEntityRequired(Role.class, id);
        if (actor.getRoles().stream().anyMatch(r -> r.getId().equals(id))) {
            throw new LinkDuplicatedException(String.format("Actor %s already has role %s", actorId, id));
        }
        role.setActor(actor);
        role = roleRepository.save(role);
        actor.getRoles().add(role);
        actor = actorRepository.save(actor);
        return translationService.translateList(actor.getRoles(), RoleReadDTO.class);
    }

    public RoleReadDTO patchRole(UUID id, RolePatchDTO patch) {

        Role role = repositoryHelper.getEntityRequired(Role.class, id);
        translationService.map(patch, role);
        role = roleRepository.save(role);
        return translationService.translate(role, RoleReadDTO.class);
    }

    public RoleReadDTO updateRole(UUID id, RolePutDTO put) {

        Role role = repositoryHelper.getEntityRequired(Role.class, id);
        translationService.map(put, role);
        role = roleRepository.save(role);
        return translationService.translate(role, RoleReadDTO.class);
    }

    public void deleteRole(UUID id) {

        Role role = repositoryHelper.getEntityRequired(Role.class, id);
        roleRepository.delete(role);
    }

    @Transactional
    public List<RoleReadDTO> removeRoleFromActor(UUID actorId, UUID id) {

        Actor actor = repositoryHelper.getEntityRequired(Actor.class, actorId);

        boolean removed = actor.getRoles().removeIf(r -> r.getId().equals(id));
        if (!removed) {
            throw new EntityNotFoundException("Actor " + actorId + " has no role " + id);
        }
        actor = actorRepository.save(actor);
        return translationService.translateList(actor.getRoles(), RoleReadDTO.class);
    }
}
