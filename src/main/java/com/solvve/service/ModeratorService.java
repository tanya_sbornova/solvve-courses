package com.solvve.service;

import com.solvve.domain.Moderator;
import com.solvve.domain.PageResult;
import com.solvve.domain.UserStatus;
import com.solvve.dto.moderator.*;
import com.solvve.repository.ModeratorRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ModeratorService {

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TranslationService translationService;

    public ModeratorReadDTO getModerator(UUID id) {

        Moderator moderator = repositoryHelper.getEntityRequired(Moderator.class, id);
        return translationService.translate(moderator, ModeratorReadDTO.class);
    }

    public PageResult<ModeratorReadDTO> getModerators(ModeratorFilter filter, Pageable pageable) {
        Page<Moderator> moderators = moderatorRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(moderators, ModeratorReadDTO.class);
    }

    public ModeratorReadDTO createModerator(ModeratorCreateDTO create) {

        create.setEncodedPassword(passwordEncoder.encode(create.getEncodedPassword()));
        create.setUserRole(null);
        create.setUserStatus(UserStatus.NEW);
        Moderator moderator = translationService.translate(create, Moderator.class);
        moderator = moderatorRepository.save(moderator);

        return translationService.translate(moderator, ModeratorReadDTO.class);
    }

    public ModeratorReadDTO patchModerator(UUID id, ModeratorPatchDTO patch) {

        Moderator moderator = repositoryHelper.getEntityRequired(Moderator.class, id);
        if (patch.getEncodedPassword() != null) {
            patch.setEncodedPassword(passwordEncoder.encode(patch.getEncodedPassword()));
        }
        translationService.map(patch, moderator);
        moderator = moderatorRepository.save(moderator);
        return translationService.translate(moderator, ModeratorReadDTO.class);
    }

    public ModeratorReadDTO updateModerator(UUID id, ModeratorPutDTO put) {

        Moderator moderator = repositoryHelper.getEntityRequired(Moderator.class, id);
        translationService.map(put, moderator);
        moderator = moderatorRepository.save(moderator);
        return translationService.translate(moderator, ModeratorReadDTO.class);
    }

    public void deleteModerator(UUID id) {
        moderatorRepository.delete(repositoryHelper.getEntityRequired(Moderator.class, id));
    }
}


