package com.solvve.service;

import com.solvve.domain.RegisteredUser;
import com.solvve.domain.ReviewFilmLikeDislike;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeCreateDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePatchDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePutDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeReadDTO;
import com.solvve.repository.ReviewFilmLikeDislikeRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class RegisteredUserReviewFilmLikeService {

    @Autowired
    private ReviewFilmLikeDislikeRepository reviewFilmLikeDislikeRepository;

    @Autowired
    private ReviewFilmLikeDislikeService reviewFilmLikeDislikeService;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Transactional(readOnly = true)
    public List<ReviewFilmLikeDislikeReadDTO> getRegisteredUserReviewFilmLikes(UUID registeredUserId) {
        List<ReviewFilmLikeDislikeReadDTO> reviewFilmLikesReadDTO = new ArrayList<>();
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId).getReviewFilmLikeDislikes()
                .forEach(reviewFilmLike -> {
                    reviewFilmLikesReadDTO.add(translationService.translate(reviewFilmLike,
                            ReviewFilmLikeDislikeReadDTO.class));
                });
        return reviewFilmLikesReadDTO;
    }

    public ReviewFilmLikeDislikeReadDTO getRegisteredUserReviewFilmLike(UUID registeredUserId,
                                                                        UUID reviewFilmLikeDislikeId) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        repositoryHelper.getEntityRequired(ReviewFilmLikeDislike.class, reviewFilmLikeDislikeId);
        return translationService.translate(repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                ReviewFilmLikeDislike.class,
                registeredUserId, reviewFilmLikeDislikeId), ReviewFilmLikeDislikeReadDTO.class);
    }

    public ReviewFilmLikeDislikeReadDTO createRegisteredUserReviewFilmLike(UUID registeredUserId,
            ReviewFilmLikeDislikeCreateDTO reviewFilmLikeDislikeCreate) {

        ReviewFilmLikeDislike reviewFilmLikeDislike = translationService.translate(reviewFilmLikeDislikeCreate,
                ReviewFilmLikeDislike.class);
        reviewFilmLikeDislike.setRegisteredUser(repositoryHelper.getReferenceIfExists(RegisteredUser.class,
                registeredUserId));
        reviewFilmLikeDislike = reviewFilmLikeDislikeRepository.save(reviewFilmLikeDislike);
        return translationService.translate(reviewFilmLikeDislike, ReviewFilmLikeDislikeReadDTO.class);
    }

    public ReviewFilmLikeDislikeReadDTO patchRegisteredUserReviewFilmLike(UUID registeredUserId,
                                                                          UUID reviewFilmLikeDislikeId,
                                                                          ReviewFilmLikeDislikePatchDTO patch) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                ReviewFilmLikeDislike.class, registeredUserId, reviewFilmLikeDislikeId);
        return reviewFilmLikeDislikeService.patchReviewFilmLikeDislike(reviewFilmLikeDislikeId, patch);
    }

    public ReviewFilmLikeDislikeReadDTO updateRegisteredUserReviewFilmLike(UUID registeredUserId,
                                                                           UUID reviewFilmLikeDislikeId,
                                                                           ReviewFilmLikeDislikePutDTO put) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                ReviewFilmLikeDislike.class, registeredUserId, reviewFilmLikeDislikeId);
        return reviewFilmLikeDislikeService.updateReviewFilmLikeDislike(reviewFilmLikeDislikeId, put);
    }

    public void deleteRegisteredUserReviewFilmLike(UUID registeredUserId, UUID reviewFilmLikeDislikeId) {

        reviewFilmLikeDislikeRepository.delete(repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                ReviewFilmLikeDislike.class, registeredUserId, reviewFilmLikeDislikeId));
    }
}


