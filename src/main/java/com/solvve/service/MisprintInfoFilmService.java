package com.solvve.service;

import com.solvve.domain.MisprintInfoFilm;
import com.solvve.dto.misprintinfofilm.MisprintInfoFilmReadDTO;
import com.solvve.repository.MisprintInfoFilmRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class MisprintInfoFilmService {

    @Autowired
    private MisprintInfoFilmRepository misprintInfoFilmRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public MisprintInfoFilmReadDTO getMisprintInfoFilm(UUID id) {

        MisprintInfoFilm misprintInfo = repositoryHelper.getEntityRequired(MisprintInfoFilm.class, id);
        return translationService.translate(misprintInfo, MisprintInfoFilmReadDTO.class);
    }

    public void deleteMisprintInfoFilm(UUID id) {
        misprintInfoFilmRepository.delete(repositoryHelper.getEntityRequired(MisprintInfoFilm.class, id));
    }
}
