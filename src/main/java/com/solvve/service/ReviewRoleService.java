package com.solvve.service;

import com.solvve.domain.PageResult;
import com.solvve.domain.ReviewRole;
import com.solvve.dto.reviewrole.ReviewRoleFilter;
import com.solvve.dto.reviewrole.ReviewRolePatchDTO;
import com.solvve.dto.reviewrole.ReviewRolePutDTO;
import com.solvve.dto.reviewrole.ReviewRoleReadDTO;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.ReviewRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ReviewRoleService {

    @Autowired
    private ReviewRoleRepository reviewRoleRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public ReviewRoleReadDTO getReviewRole(UUID id) {

        ReviewRole reviewRole = repositoryHelper.getEntityRequired(ReviewRole.class, id);
        return translationService.translate(reviewRole, ReviewRoleReadDTO.class);
    }

    public PageResult<ReviewRoleReadDTO> getReviewRoles(ReviewRoleFilter filter, Pageable pageable) {
        Page<ReviewRole> reviewRoles = reviewRoleRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(reviewRoles, ReviewRoleReadDTO.class);
    }

    public ReviewRoleReadDTO patchReviewRole(UUID id, ReviewRolePatchDTO patch) {

        ReviewRole reviewRole = repositoryHelper.getEntityRequired(ReviewRole.class, id);
        translationService.map(patch, reviewRole);
        reviewRole = reviewRoleRepository.save(reviewRole);
        return translationService.translate(reviewRole, ReviewRoleReadDTO.class);
    }

    public ReviewRoleReadDTO updateReviewRole(UUID id, ReviewRolePutDTO put) {

        ReviewRole reviewRole = repositoryHelper.getEntityRequired(ReviewRole.class, id);
        translationService.map(put, reviewRole);
        reviewRole = reviewRoleRepository.save(reviewRole);
        return translationService.translate(reviewRole, ReviewRoleReadDTO.class);
    }

    public void deleteReviewRole(UUID id) {
        reviewRoleRepository.delete(repositoryHelper.getEntityRequired(ReviewRole.class, id));
    }
}


