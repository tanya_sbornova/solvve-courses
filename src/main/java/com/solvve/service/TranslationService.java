package com.solvve.service;

import com.solvve.domain.*;
import com.solvve.dto.actor.ActorCreateDTO;
import com.solvve.dto.actor.ActorReadDTO;
import com.solvve.dto.admin.AdminPatchDTO;
import com.solvve.dto.contentmanager.ContentManagerPatchDTO;
import com.solvve.dto.crewmember.CrewMemberCreateDTO;
import com.solvve.dto.crewmember.CrewMemberPatchDTO;
import com.solvve.dto.crewmember.CrewMemberReadDTO;
import com.solvve.dto.film.FilmPatchDTO;
import com.solvve.dto.genre.GenrePatchDTO;
import com.solvve.dto.misprintinfofilm.MisprintInfoFilmCreateDTO;
import com.solvve.dto.misprintinfofilm.MisprintInfoFilmReadDTO;
import com.solvve.dto.misprintinfofix.MisprintInfoFixReadDTO;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsCreateDTO;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsReadDTO;
import com.solvve.dto.misprintinforole.MisprintInfoRoleCreateDTO;
import com.solvve.dto.misprintinforole.MisprintInfoRoleReadDTO;
import com.solvve.dto.moderator.ModeratorPatchDTO;
import com.solvve.dto.news.NewsPatchDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikeCreateDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikePatchDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikeReadDTO;
import com.solvve.dto.person.PersonPatchDTO;
import com.solvve.dto.problemreview.ProblemReviewCreateDTO;
import com.solvve.dto.problemreview.ProblemReviewPatchDTO;
import com.solvve.dto.problemreview.ProblemReviewReadDTO;
import com.solvve.dto.ratingfilm.RatingFilmCreateDTO;
import com.solvve.dto.ratingfilm.RatingFilmPatchDTO;
import com.solvve.dto.ratingfilm.RatingFilmReadDTO;
import com.solvve.dto.ratingrole.RatingRoleCreateDTO;
import com.solvve.dto.ratingrole.RatingRolePatchDTO;
import com.solvve.dto.ratingrole.RatingRoleReadDTO;
import com.solvve.dto.registereduser.RegisteredUserPatchDTO;
import com.solvve.dto.reviewfilm.ReviewFilmCreateDTO;
import com.solvve.dto.reviewfilm.ReviewFilmPatchDTO;
import com.solvve.dto.reviewfilm.ReviewFilmReadDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeCreateDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePatchDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeReadDTO;
import com.solvve.dto.reviewrole.ReviewRoleCreateDTO;
import com.solvve.dto.reviewrole.ReviewRolePatchDTO;
import com.solvve.dto.reviewrole.ReviewRoleReadDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikeCreateDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePatchDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikeReadDTO;
import com.solvve.dto.role.RoleCreateDTO;
import com.solvve.dto.role.RolePatchDTO;
import com.solvve.dto.role.RoleReadDTO;
import com.solvve.dto.userprofile.UserProfilePatchDTO;
import com.solvve.repository.RepositoryHelper;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.brunneng.ot.Configuration;
import org.bitbucket.brunneng.ot.ObjectTranslator;
import org.bitbucket.brunneng.ot.exceptions.TranslationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TranslationService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    public ObjectTranslator objectTranslator = new ObjectTranslator(createConfiguration());

    public <T> T translate(Object srcObject, Class<T> targetClass) {
        try {
            return objectTranslator.translate(srcObject, targetClass);
        } catch (TranslationException e) {
            log.warn(e.getMessage());
            throw (RuntimeException) e.getCause();
        }
    }

    public <T> void map(Object srcObject, Object destObject) {
        try {
            objectTranslator.mapBean(srcObject, destObject);
        } catch (TranslationException e) {
            log.warn(e.getMessage());
            throw (RuntimeException) e.getCause();
        }
    }

    public <T> List<T> translateList(List<?> objects, Class<T> targetClass) {
        return objects.stream().map(o -> translate(o, targetClass)).collect(Collectors.toList());
    }

    public <T extends AbstractEntity> List<T> convertSetToList(Set<T> objects) {

        List<T> listOfObjects = objects.stream().collect(Collectors.toList());
        Comparator<T> idSorter = (a, b) -> a.getCreatedAt().compareTo(b.getCreatedAt());
        Collections.sort(listOfObjects, idSorter);
        return listOfObjects;
    }

    public <E, T> PageResult<T> toPageResult(Page<E> page, Class<T> dtoType) {
        PageResult<T> res = new PageResult<>();
        res.setPage(page.getNumber());
        res.setPageSize(page.getSize());
        res.setTotalPages(page.getTotalPages());
        res.setTotalElements(page.getTotalElements());
        res.setData(page.getContent().stream().map(e -> translate(e, dtoType)).collect(Collectors.toList()));
        return res;
    }

    private Configuration createConfiguration() {
        Configuration c = new Configuration();
        configureForAbstractEntity(c);
        configureForActor(c);
        configureForAdmin(c);
        configureForContentManager(c);
        configureForCrewMember(c);
        configureForFilm(c);
        configureForGenre(c);
        configureForNewsLikeDislike(c);
        configureForMisprintInfoFilm(c);
        configureForMisprintInfoNews(c);
        configureForMisprintInfoRole(c);
        configureForModerator(c);
        configureForNews(c);
        configureForNewsLikeDislike(c);
        configureForPerson(c);
        configureForProblemReview(c);
        configureForRatingFilm(c);
        configureForRatingRole(c);
        configureForRegisteredUser(c);
        configureForReviewFilm(c);
        configureForReviewFilmLikeDislike(c);
        configureForReviewRole(c);
        configureForReviewRoleLikeDislike(c);
        configureForRole(c);
        configureForUserProfile(c);
        configureForMisprintInfoCommon(c);
        return c;
    }

    private void configureForAbstractEntity(Configuration c) {
        c.beanOfClass(AbstractEntity.class).setIdentifierProperty("id");
        c.beanOfClass(AbstractEntity.class).setBeanFinder(
                (beanClass, id) -> repositoryHelper.getReferenceIfExists(beanClass, (UUID) id));
    }

    private void configureForActor(Configuration c) {
        Configuration.Translation t = c.beanOfClass(Actor.class).translationTo(ActorReadDTO.class);
        t.srcProperty("person.id").translatesTo("personId");
        Configuration.Translation tcreate = c.beanOfClass(ActorCreateDTO.class).translationTo(Actor.class);
        tcreate.srcProperty("personId").translatesTo("person.id");
    }

    private void configureForAdmin(Configuration c) {
        c.beanOfClass(AdminPatchDTO.class).translationTo(Admin.class).mapOnlyNotNullProperties();
    }

    private void configureForContentManager(Configuration c) {
        c.beanOfClass(ContentManagerPatchDTO.class).translationTo(ContentManager.class).mapOnlyNotNullProperties();
    }

    private void configureForCrewMember(Configuration c) {
        Configuration.Translation t = c.beanOfClass(CrewMember.class).translationTo(CrewMemberReadDTO.class);
        t.srcProperty("person.id").translatesTo("personId");
        t.srcProperty("department.id").translatesTo("departmentId");
        Configuration.Translation tcreate = c.beanOfClass(CrewMemberCreateDTO.class).translationTo(CrewMember.class);
        tcreate.srcProperty("personId").translatesTo("person.id");
        tcreate.srcProperty("departmentId").translatesTo("department.id");
        c.beanOfClass(CrewMemberPatchDTO.class).translationTo(CrewMember.class).mapOnlyNotNullProperties();
    }

    private void configureForFilm(Configuration c) {
        c.beanOfClass(FilmPatchDTO.class).translationTo(Film.class).mapOnlyNotNullProperties();
    }

    private void configureForGenre(Configuration c) {
        c.beanOfClass(GenrePatchDTO.class).translationTo(Genre.class).mapOnlyNotNullProperties();
    }

    private void configureForMisprintInfoCommon(Configuration c) {
        Configuration.Translation t = c.beanOfClass(MisprintInfoCommon.class)
                .translationTo(MisprintInfoFilmReadDTO.class);
        t.srcProperty("contentManager.id").translatesTo("contentManagerId");
        Configuration.Translation tr = c.beanOfClass(MisprintInfoCommon.class)
                .translationTo(MisprintInfoFixReadDTO.class);
        tr.srcProperty("contentManager.id").translatesTo("contentManagerId");
    }

    private void configureForMisprintInfoFilm(Configuration c) {
        Configuration.Translation t = c.beanOfClass(MisprintInfoFilm.class)
                .translationTo(MisprintInfoFilmReadDTO.class);
        t.srcProperty("contentManager.id").translatesTo("contentManagerId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");
        t.srcProperty("content.id").translatesTo("contentId");
        Configuration.Translation tr = c.beanOfClass(MisprintInfoFilm.class)
                .translationTo(MisprintInfoFixReadDTO.class);
        tr.srcProperty("contentManager.id").translatesTo("contentManagerId");
        tr.srcProperty("registeredUser.id").translatesTo("registeredUserId");
        tr.srcProperty("content.id").translatesTo("contentId");
        Configuration.Translation tcreate = c.beanOfClass(MisprintInfoFilmCreateDTO.class)
                .translationTo(MisprintInfoFilm.class);
        tcreate.srcProperty("contentId").translatesTo("content.id");
    }

    private void configureForMisprintInfoNews(Configuration c) {
        Configuration.Translation t = c.beanOfClass(MisprintInfoNews.class)
                .translationTo(MisprintInfoNewsReadDTO.class);
        t.srcProperty("contentManager.id").translatesTo("contentManagerId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");
        t.srcProperty("content.id").translatesTo("contentId");
        Configuration.Translation tr = c.beanOfClass(MisprintInfoNews.class)
                .translationTo(MisprintInfoFixReadDTO.class);
        tr.srcProperty("contentManager.id").translatesTo("contentManagerId");
        tr.srcProperty("registeredUser.id").translatesTo("registeredUserId");
        tr.srcProperty("content.id").translatesTo("contentId");
        Configuration.Translation tcreate = c.beanOfClass(MisprintInfoNewsCreateDTO.class)
                .translationTo(MisprintInfoNews.class);
        tcreate.srcProperty("contentId").translatesTo("content.id");
    }

    private void configureForMisprintInfoRole(Configuration c) {
        Configuration.Translation t = c.beanOfClass(MisprintInfoRole.class)
                .translationTo(MisprintInfoRoleReadDTO.class);
        t.srcProperty("contentManager.id").translatesTo("contentManagerId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");
        t.srcProperty("content.id").translatesTo("contentId");
        Configuration.Translation tr = c.beanOfClass(MisprintInfoRole.class)
                .translationTo(MisprintInfoFixReadDTO.class);
        tr.srcProperty("contentManager.id").translatesTo("contentManagerId");
        tr.srcProperty("registeredUser.id").translatesTo("registeredUserId");
        tr.srcProperty("content.id").translatesTo("contentId");
        Configuration.Translation tcreate = c.beanOfClass(MisprintInfoRoleCreateDTO.class)
                .translationTo(MisprintInfoRole.class);
        tcreate.srcProperty("contentId").translatesTo("content.id");
    }

    private void configureForModerator(Configuration c) {
        c.beanOfClass(ModeratorPatchDTO.class).translationTo(Moderator.class).mapOnlyNotNullProperties();
    }

    private void configureForNewsLikeDislike(Configuration c) {
        Configuration.Translation t = c.beanOfClass(NewsLikeDislike.class).translationTo(NewsLikeDislikeReadDTO.class);
        t.srcProperty("news.id").translatesTo("newsId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");
        Configuration.Translation tcreate = c.beanOfClass(NewsLikeDislikeCreateDTO.class)
                .translationTo(NewsLikeDislike.class);
        tcreate.srcProperty("newsId").translatesTo("news.id");
        c.beanOfClass(NewsLikeDislikePatchDTO.class).translationTo(NewsLikeDislike.class).mapOnlyNotNullProperties();
    }

    private void configureForNews(Configuration c) {
        c.beanOfClass(NewsPatchDTO.class).translationTo(News.class).mapOnlyNotNullProperties();
    }

    private void configureForPerson(Configuration c) {
        c.beanOfClass(PersonPatchDTO.class).translationTo(Person.class).mapOnlyNotNullProperties();
    }

    private void configureForProblemReview(Configuration c) {
        Configuration.Translation t = c.beanOfClass(ProblemReview.class).translationTo(ProblemReviewReadDTO.class);
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");
        t.srcProperty("reviewFilm.id").translatesTo("reviewFilmId");
        t.srcProperty("reviewRole.id").translatesTo("reviewRoleId");
        Configuration.Translation tcreate = c.beanOfClass(ProblemReviewCreateDTO.class)
                .translationTo(ProblemReview.class);
        tcreate.srcProperty("reviewFilmId").translatesTo("reviewFilm.id");
        tcreate.srcProperty("reviewRoleId").translatesTo("reviewRole.id");
        c.beanOfClass(ProblemReviewPatchDTO.class).translationTo(ProblemReview.class).mapOnlyNotNullProperties();
    }

    private void configureForRatingFilm(Configuration c) {
        Configuration.Translation t = c.beanOfClass(RatingFilm.class).translationTo(RatingFilmReadDTO.class);
        t.srcProperty("film.id").translatesTo("filmId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");
        Configuration.Translation tcreate = c.beanOfClass(RatingFilmCreateDTO.class).translationTo(RatingFilm.class);
        tcreate.srcProperty("registeredUserId").translatesTo("registeredUser.id");
        c.beanOfClass(RatingFilmPatchDTO.class).translationTo(RatingFilm.class).mapOnlyNotNullProperties();
    }

    private void configureForRatingRole(Configuration c) {
        Configuration.Translation t = c.beanOfClass(RatingRole.class).translationTo(RatingRoleReadDTO.class);
        t.srcProperty("role.id").translatesTo("roleId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");
        Configuration.Translation tcreate = c.beanOfClass(RatingRoleCreateDTO.class).translationTo(RatingRole.class);
        tcreate.srcProperty("registeredUserId").translatesTo("registeredUser.id");
        c.beanOfClass(RatingRolePatchDTO.class).translationTo(RatingRole.class).mapOnlyNotNullProperties();
    }

    private void configureForRegisteredUser(Configuration c) {
        c.beanOfClass(RegisteredUserPatchDTO.class).translationTo(RegisteredUser.class).mapOnlyNotNullProperties();
    }

    private void configureForReviewFilm(Configuration c) {
        Configuration.Translation t = c.beanOfClass(ReviewFilm.class).translationTo(ReviewFilmReadDTO.class);
        t.srcProperty("film.id").translatesTo("filmId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");
        Configuration.Translation tcreate = c.beanOfClass(ReviewFilmCreateDTO.class).translationTo(ReviewFilm.class);
        tcreate.srcProperty("registeredUserId").translatesTo("registeredUser.id");
        c.beanOfClass(ReviewFilmPatchDTO.class).translationTo(ReviewFilm.class).mapOnlyNotNullProperties();
    }

    private void configureForReviewFilmLikeDislike(Configuration c) {
        Configuration.Translation t = c.beanOfClass(ReviewFilmLikeDislike.class)
                .translationTo(ReviewFilmLikeDislikeReadDTO.class);
        t.srcProperty("reviewFilm.id").translatesTo("reviewFilmId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");
        Configuration.Translation tcreate = c.beanOfClass(ReviewFilmLikeDislikeCreateDTO.class)
                .translationTo(ReviewFilmLikeDislike.class);
        tcreate.srcProperty("reviewFilmId").translatesTo("reviewFilm.id");
        c.beanOfClass(ReviewFilmLikeDislikePatchDTO.class).translationTo(ReviewFilmLikeDislike.class)
                .mapOnlyNotNullProperties();
    }

    private void configureForReviewRole(Configuration c) {
        Configuration.Translation t = c.beanOfClass(ReviewRole.class).translationTo(ReviewRoleReadDTO.class);
        t.srcProperty("role.id").translatesTo("roleId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");
        Configuration.Translation tcreate = c.beanOfClass(ReviewRoleCreateDTO.class).translationTo(ReviewRole.class);
        tcreate.srcProperty("registeredUserId").translatesTo("registeredUser.id");
        c.beanOfClass(ReviewRolePatchDTO.class).translationTo(ReviewRole.class).mapOnlyNotNullProperties();
    }

    private void configureForReviewRoleLikeDislike(Configuration c) {
        Configuration.Translation t = c.beanOfClass(ReviewRoleLikeDislike.class)
                .translationTo(ReviewRoleLikeDislikeReadDTO.class);
        t.srcProperty("reviewRole.id").translatesTo("reviewRoleId");
        t.srcProperty("registeredUser.id").translatesTo("registeredUserId");
        Configuration.Translation tcreate = c.beanOfClass(ReviewRoleLikeDislikeCreateDTO.class)
                .translationTo(ReviewRoleLikeDislike.class);
        tcreate.srcProperty("reviewRoleId").translatesTo("reviewRole.id");
        c.beanOfClass(ReviewRoleLikeDislikePatchDTO.class).translationTo(ReviewRoleLikeDislike.class)
                .mapOnlyNotNullProperties();
    }

    private void configureForRole(Configuration c) {
        Configuration.Translation t = c.beanOfClass(Role.class).translationTo(RoleReadDTO.class);
        t.srcProperty("actor.id").translatesTo("actorId");
        t.srcProperty("film.id").translatesTo("filmId");
        Configuration.Translation tcreate = c.beanOfClass(RoleCreateDTO.class).translationTo(Role.class);
        tcreate.srcProperty("actorId").translatesTo("actor.id");
        tcreate.srcProperty("filmId").translatesTo("film.id");
        c.beanOfClass(RolePatchDTO.class).translationTo(Role.class).mapOnlyNotNullProperties();
    }

    private void configureForUserProfile(Configuration c) {
        c.beanOfClass(UserProfilePatchDTO.class).translationTo(UserProfile.class).mapOnlyNotNullProperties();
    }
}
