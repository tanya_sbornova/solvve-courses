package com.solvve.service;

import com.solvve.domain.ReviewRoleLikeDislike;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePatchDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePutDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikeReadDTO;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.ReviewRoleLikeDislikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ReviewRoleLikeDislikeService {

    @Autowired
    private ReviewRoleLikeDislikeRepository reviewRoleLikeDislikeRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public ReviewRoleLikeDislikeReadDTO getReviewRoleLikeDislike(UUID id) {

        ReviewRoleLikeDislike reviewRoleLikeDislike =
                repositoryHelper.getEntityRequired(ReviewRoleLikeDislike.class, id);
        return translationService.translate(reviewRoleLikeDislike, ReviewRoleLikeDislikeReadDTO.class);
    }

    public ReviewRoleLikeDislikeReadDTO patchReviewRoleLikeDislike(UUID id, ReviewRoleLikeDislikePatchDTO patch) {

        ReviewRoleLikeDislike reviewRoleLikeDislike =
                repositoryHelper.getEntityRequired(ReviewRoleLikeDislike.class, id);
        translationService.map(patch, reviewRoleLikeDislike);
        reviewRoleLikeDislike = reviewRoleLikeDislikeRepository.save(reviewRoleLikeDislike);
        return translationService.translate(reviewRoleLikeDislike, ReviewRoleLikeDislikeReadDTO.class);
    }

    public ReviewRoleLikeDislikeReadDTO updateReviewRoleLikeDislike(UUID id, ReviewRoleLikeDislikePutDTO put) {

        ReviewRoleLikeDislike reviewRoleLikeDislike =
                repositoryHelper.getEntityRequired(ReviewRoleLikeDislike.class, id);
        translationService.map(put, reviewRoleLikeDislike);
        reviewRoleLikeDislike = reviewRoleLikeDislikeRepository.save(reviewRoleLikeDislike);
        return translationService.translate(reviewRoleLikeDislike, ReviewRoleLikeDislikeReadDTO.class);
    }

    public void deleteReviewRoleLikeDislike(UUID id) {
        reviewRoleLikeDislikeRepository.delete(repositoryHelper.getEntityRequired(ReviewRoleLikeDislike.class, id));
    }
}

