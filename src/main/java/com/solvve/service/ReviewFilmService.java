package com.solvve.service;

import com.solvve.domain.PageResult;
import com.solvve.domain.ProblemReview;
import com.solvve.domain.ReviewFilm;
import com.solvve.dto.reviewfilm.ReviewFilmFilter;
import com.solvve.dto.reviewfilm.ReviewFilmPatchDTO;
import com.solvve.dto.reviewfilm.ReviewFilmPutDTO;
import com.solvve.dto.reviewfilm.ReviewFilmReadDTO;
import com.solvve.repository.ProblemReviewRepository;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.ReviewFilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class ReviewFilmService {

    @Autowired
    private ReviewFilmRepository reviewFilmRepository;

    @Autowired
    private ProblemReviewRepository problemReviewRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public ReviewFilmReadDTO getReviewFilm(UUID id) {

        ReviewFilm reviewFilm = repositoryHelper.getEntityRequired(ReviewFilm.class, id);
        return translationService.translate(reviewFilm, ReviewFilmReadDTO.class);
    }

    public PageResult<ReviewFilmReadDTO> getReviewFilms(ReviewFilmFilter filter, Pageable pageable) {
        Page<ReviewFilm> reviewFilms = reviewFilmRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(reviewFilms, ReviewFilmReadDTO.class);
    }

    public ReviewFilmReadDTO patchReviewFilm(UUID id, ReviewFilmPatchDTO patch) {

        ReviewFilm reviewFilm = repositoryHelper.getEntityRequired(ReviewFilm.class, id);
        translationService.map(patch, reviewFilm);
        reviewFilm = reviewFilmRepository.save(reviewFilm);
        return translationService.translate(reviewFilm, ReviewFilmReadDTO.class);
    }

    public ReviewFilmReadDTO updateReviewFilm(UUID id, ReviewFilmPutDTO put) {

        ReviewFilm reviewFilm = repositoryHelper.getEntityRequired(ReviewFilm.class, id);
        translationService.map(put, reviewFilm);
        reviewFilm = reviewFilmRepository.save(reviewFilm);
        return translationService.translate(reviewFilm, ReviewFilmReadDTO.class);
    }

    @Transactional
    public void deleteReviewFilm(UUID id) {

        ReviewFilm reviewFilm = repositoryHelper.getEntityRequired(ReviewFilm.class, id);
        List<ProblemReview> problemReviews = problemReviewRepository
                .findProblemReviewsByReviewFilmId(reviewFilm.getId());
        problemReviews
                .forEach(problemReview -> {
                    problemReviewRepository.delete(problemReview);
                });
        reviewFilmRepository.delete(repositoryHelper.getEntityRequired(ReviewFilm.class, id));
    }
}

