package com.solvve.service;

import com.solvve.domain.RatingRole;
import com.solvve.dto.ratingrole.RatingRolePatchDTO;
import com.solvve.dto.ratingrole.RatingRolePutDTO;
import com.solvve.dto.ratingrole.RatingRoleReadDTO;
import com.solvve.repository.RatingRoleRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RatingRoleService {

    @Autowired
    private RatingRoleRepository ratingRoleRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public RatingRoleReadDTO getRatingRole(UUID id) {

        RatingRole ratingRole = repositoryHelper.getEntityRequired(RatingRole.class, id);
        return translationService.translate(ratingRole, RatingRoleReadDTO.class);
    }

    public RatingRoleReadDTO patchRatingRole(UUID id, RatingRolePatchDTO patch) {

        RatingRole ratingRole = repositoryHelper.getEntityRequired(RatingRole.class, id);
        translationService.map(patch, ratingRole);
        ratingRole = ratingRoleRepository.save(ratingRole);
        return translationService.translate(ratingRole, RatingRoleReadDTO.class);
    }

    public RatingRoleReadDTO updateRatingRole(UUID id, RatingRolePutDTO put) {

        RatingRole ratingRole = repositoryHelper.getEntityRequired(RatingRole.class, id);
        translationService.map(put, ratingRole);
        ratingRole = ratingRoleRepository.save(ratingRole);
        return translationService.translate(ratingRole, RatingRoleReadDTO.class);
    }

    public void deleteRatingRole(UUID id) {

        ratingRoleRepository.delete(repositoryHelper.getEntityRequired(RatingRole.class, id));
    }
}
