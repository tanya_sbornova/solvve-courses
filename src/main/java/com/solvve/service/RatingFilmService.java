package com.solvve.service;

import com.solvve.domain.RatingFilm;
import com.solvve.dto.ratingfilm.RatingFilmPatchDTO;
import com.solvve.dto.ratingfilm.RatingFilmPutDTO;
import com.solvve.dto.ratingfilm.RatingFilmReadDTO;
import com.solvve.repository.RatingFilmRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.UUID;

@Service
public class RatingFilmService {

    @Autowired
    private RatingFilmRepository ratingFilmRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public RatingFilmReadDTO getRatingFilm(UUID id) {

        RatingFilm ratingFilm = repositoryHelper.getEntityRequired(RatingFilm.class, id);
        return translationService.translate(ratingFilm, RatingFilmReadDTO.class);
    }

    public RatingFilmReadDTO patchRatingFilm(UUID id, RatingFilmPatchDTO patch) {

        RatingFilm ratingFilm = repositoryHelper.getEntityRequired(RatingFilm.class, id);
        translationService.map(patch, ratingFilm);
        ratingFilm = ratingFilmRepository.save(ratingFilm);
        return translationService.translate(ratingFilm, RatingFilmReadDTO.class);
    }

    public RatingFilmReadDTO updateRatingFilm(UUID id, RatingFilmPutDTO put) {

        RatingFilm ratingFilm = repositoryHelper.getEntityRequired(RatingFilm.class, id);
        translationService.map(put, ratingFilm);
        ratingFilm = ratingFilmRepository.save(ratingFilm);
        return translationService.translate(ratingFilm, RatingFilmReadDTO.class);
    }

    public void deleteRatingFilm(UUID id) {

        ratingFilmRepository.delete(repositoryHelper.getEntityRequired(RatingFilm.class, id));

    }
}
