package com.solvve.service;

import com.solvve.domain.Admin;
import com.solvve.domain.UserRoleType;
import com.solvve.dto.admin.AdminCreateDTO;
import com.solvve.dto.admin.AdminPatchDTO;
import com.solvve.dto.admin.AdminPutDTO;
import com.solvve.dto.admin.AdminReadDTO;
import com.solvve.repository.AdminRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.UUID;

@Service
public class AdminService {

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TranslationService translationService;

    public AdminReadDTO getAdmin(UUID id) {

        Admin admin = repositoryHelper.getEntityRequired(Admin.class, id);
        return translationService.translate(admin, AdminReadDTO.class);
    }

    public AdminReadDTO createAdmin(AdminCreateDTO create) {

        create.setEncodedPassword(passwordEncoder.encode(create.getEncodedPassword()));
        create.setUserRole(UserRoleType.ADMIN);
        Admin admin = translationService.translate(create, Admin.class);
        admin = adminRepository.save(admin);

        return translationService.translate(admin, AdminReadDTO.class);
    }

    public AdminReadDTO patchAdmin(UUID id, AdminPatchDTO patch) {

        Admin admin = repositoryHelper.getEntityRequired(Admin.class, id);
        if (patch.getEncodedPassword() != null) {
            patch.setEncodedPassword(passwordEncoder.encode(patch.getEncodedPassword()));
        }
        translationService.map(patch, admin);
        admin = adminRepository.save(admin);
        return translationService.translate(admin, AdminReadDTO.class);
    }

    public AdminReadDTO updateAdmin(UUID id, AdminPutDTO put) {

        Admin admin = repositoryHelper.getEntityRequired(Admin.class, id);
        translationService.map(put, admin);
        admin = adminRepository.save(admin);
        return translationService.translate(admin, AdminReadDTO.class);
    }

    public void deleteAdmin(UUID id) {

        adminRepository.delete(repositoryHelper.getEntityRequired(Admin.class, id));
    }
}

