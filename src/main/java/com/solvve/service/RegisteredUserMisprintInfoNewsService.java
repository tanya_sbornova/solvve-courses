package com.solvve.service;

import com.solvve.domain.MisprintStatus;
import com.solvve.domain.RegisteredUser;
import com.solvve.domain.MisprintInfoNews;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsCreateDTO;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsReadDTO;
import com.solvve.repository.MisprintInfoNewsRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class RegisteredUserMisprintInfoNewsService {

    @Autowired
    private MisprintInfoNewsRepository misprintInfoNewsRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Transactional(readOnly = true)
    public List<MisprintInfoNewsReadDTO> getRegisteredUserMisprintInfoNews(UUID registeredUserId) {
        List<MisprintInfoNewsReadDTO> misprintInfoNewsReadDTO = new ArrayList<>();
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId).getMisprintInfoNews()
                .forEach(misprintInfoNews -> {
                    misprintInfoNewsReadDTO.add(translationService.translate(misprintInfoNews,
                            MisprintInfoNewsReadDTO.class));
                });
        return misprintInfoNewsReadDTO;
    }

    public MisprintInfoNewsReadDTO getRegisteredUserMisprintInfoNews(UUID registeredUserId,
                                                                     UUID misprintInfoNewsId) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                MisprintInfoNews.class, registeredUserId, misprintInfoNewsId);
        return translationService.translate(repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                MisprintInfoNews.class,
                registeredUserId, misprintInfoNewsId), MisprintInfoNewsReadDTO.class);
    }

    public MisprintInfoNewsReadDTO createRegisteredUserMisprintInfoNews(UUID roleId,
                                                                        MisprintInfoNewsCreateDTO
                                                                                misprintInfoNewsCreate) {

        MisprintInfoNews misprintInfoNews = translationService.translate(misprintInfoNewsCreate,
                MisprintInfoNews.class);
        misprintInfoNews.setRegisteredUser(repositoryHelper.getReferenceIfExists(RegisteredUser.class,
                roleId));
        misprintInfoNews.setStatus(MisprintStatus.NEED_TO_FIX);
        misprintInfoNews = misprintInfoNewsRepository.save(misprintInfoNews);
        return translationService.translate(misprintInfoNews, MisprintInfoNewsReadDTO.class);
    }

    public void deleteRegisteredUserMisprintInfoNews(UUID registeredUserId, UUID misprintInfoNewsId) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        misprintInfoNewsRepository.delete(repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                MisprintInfoNews.class, registeredUserId, misprintInfoNewsId));
    }
}

