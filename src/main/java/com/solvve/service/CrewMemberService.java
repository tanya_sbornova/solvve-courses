package com.solvve.service;

import com.solvve.domain.CrewMember;
import com.solvve.domain.Department;
import com.solvve.domain.Film;
import com.solvve.dto.crewmember.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.LinkDuplicatedException;
import com.solvve.repository.CrewMemberRepository;
import com.solvve.repository.DepartmentRepository;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class CrewMemberService {

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private TranslationService translationService;

    public CrewMemberReadDTO getCrewMember(UUID id) {

        CrewMember crewMember = repositoryHelper.getEntityRequired(CrewMember.class, id);
        return translationService.translate(crewMember, CrewMemberReadDTO.class);
    }

    public List<CrewMemberReadDTO> getCrewMembers() {
        List<CrewMember> crewMembers = new ArrayList<>();
        crewMemberRepository.findAll().forEach(crewMembers::add);
        return translationService.translateList(crewMembers, CrewMemberReadDTO.class);
    }

    @Transactional(readOnly = true)
    public List<CrewMemberReadDTO> getDepartmentCrewMembers(UUID actorId) {
        List<CrewMember> crewMembers = new ArrayList<>();
        crewMembers = repositoryHelper.getEntityRequired(Department.class, actorId).getCrewMembers();
        return translationService.translateList(crewMembers, CrewMemberReadDTO.class);
    }

    public CrewMemberReadDTO createCrewMember(CrewMemberCreateDTO create) {

        CrewMember crewMember = translationService.translate(create, CrewMember.class);
        crewMember = crewMemberRepository.save(crewMember);

        return translationService.translate(crewMember, CrewMemberReadDTO.class);
    }

    @Transactional
    public List<CrewMemberReadDTO> addCrewMemberToFilm(UUID filmId, UUID id) {

        Film film = repositoryHelper.getEntityRequired(Film.class, filmId);
        CrewMember crewMember = repositoryHelper.getEntityRequired(CrewMember.class, id);
        if (film.getCrewMembers().stream().anyMatch(gnr -> gnr.getId().equals(id))) {
            throw new LinkDuplicatedException(String.format("Film %s already has crewMember %s", filmId, id));
        }
        film.getCrewMembers().add(crewMember);
        film = filmRepository.save(film);
        List<CrewMember> listGenres = translationService.convertSetToList(film.getCrewMembers());
        return translationService.translateList(listGenres, CrewMemberReadDTO.class);
    }

    @Transactional
    public List<CrewMemberReadDTO> addCrewMemberToDepartment(UUID departmentId, UUID id) {

        Department department = repositoryHelper.getEntityRequired(Department.class, departmentId);
        CrewMember crewMember = repositoryHelper.getEntityRequired(CrewMember.class, id);
        if (department.getCrewMembers().stream().anyMatch(r -> r.getId().equals(id))) {
            throw new LinkDuplicatedException(String.format("Department %s already has crewMember %s",
                    departmentId, id));
        }
        crewMember.setDepartment(department);
        crewMember = crewMemberRepository.save(crewMember);
        department.getCrewMembers().add(crewMember);
        department = departmentRepository.save(department);
        return translationService.translateList(department.getCrewMembers(), CrewMemberReadDTO.class);
    }

    public CrewMemberReadDTO patchCrewMember(UUID id, CrewMemberPatchDTO patch) {

        CrewMember crewMember = repositoryHelper.getEntityRequired(CrewMember.class, id);
        translationService.map(patch, crewMember);
        crewMember = crewMemberRepository.save(crewMember);
        return translationService.translate(crewMember, CrewMemberReadDTO.class);
    }

    public CrewMemberReadDTO updateCrewMember(UUID id, CrewMemberPutDTO put) {
        CrewMember crewMember = repositoryHelper.getEntityRequired(CrewMember.class, id);
        translationService.map(put, crewMember);
        crewMember = crewMemberRepository.save(crewMember);
        return translationService.translate(crewMember, CrewMemberReadDTO.class);
    }

    public void deleteCrewMember(UUID id) {
        crewMemberRepository.delete(repositoryHelper.getEntityRequired(CrewMember.class, id));
    }

    @Transactional
    public List<CrewMemberReadDTO> removeCrewMemberFromFilm(UUID filmId, UUID id) {

        Film film = repositoryHelper.getEntityRequired(Film.class, filmId);

        boolean removed = film.getCrewMembers().removeIf(gnr -> gnr.getId().equals(id));
        if (!removed) {
            throw new EntityNotFoundException("Film " + filmId + " has no crewMember " + id);
        }
        film = filmRepository.save(film);
        List<CrewMember> listGenres = translationService.convertSetToList(film.getCrewMembers());
        return translationService.translateList(listGenres, CrewMemberReadDTO.class);
    }

    @Transactional
    public List<CrewMemberReadDTO> removeCrewMemberFromDepartment(UUID departmentId, UUID id) {

        Department department = repositoryHelper.getEntityRequired(Department.class, departmentId);

        boolean removed = department.getCrewMembers().removeIf(cr -> cr.getId().equals(id));
        if (!removed) {
            throw new EntityNotFoundException("Department " + departmentId + " has no crewMember " + id);
        }
        department = departmentRepository.save(department);
        return translationService.translateList(department.getCrewMembers(), CrewMemberReadDTO.class);
    }

}
