package com.solvve.service;

import com.solvve.domain.ReviewFilmLikeDislike;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePatchDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePutDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeReadDTO;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.ReviewFilmLikeDislikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.UUID;

@Service
public class ReviewFilmLikeDislikeService {

    @Autowired
    private ReviewFilmLikeDislikeRepository reviewFilmLikeDislikeRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public ReviewFilmLikeDislikeReadDTO getReviewFilmLikeDislike(UUID id) {

        ReviewFilmLikeDislike reviewFilmLikeDislike =
                repositoryHelper.getEntityRequired(ReviewFilmLikeDislike.class, id);
        return translationService.translate(reviewFilmLikeDislike, ReviewFilmLikeDislikeReadDTO.class);
    }

    public ReviewFilmLikeDislikeReadDTO patchReviewFilmLikeDislike(UUID id, ReviewFilmLikeDislikePatchDTO patch) {

        ReviewFilmLikeDislike reviewFilmLikeDislike =
                repositoryHelper.getEntityRequired(ReviewFilmLikeDislike.class, id);
        translationService.map(patch, reviewFilmLikeDislike);
        reviewFilmLikeDislike = reviewFilmLikeDislikeRepository.save(reviewFilmLikeDislike);
        return translationService.translate(reviewFilmLikeDislike, ReviewFilmLikeDislikeReadDTO.class);
    }

    public ReviewFilmLikeDislikeReadDTO updateReviewFilmLikeDislike(UUID id, ReviewFilmLikeDislikePutDTO put) {

        ReviewFilmLikeDislike reviewFilmLikeDislike =
                repositoryHelper.getEntityRequired(ReviewFilmLikeDislike.class, id);
        translationService.map(put, reviewFilmLikeDislike);
        reviewFilmLikeDislike = reviewFilmLikeDislikeRepository.save(reviewFilmLikeDislike);
        return translationService.translate(reviewFilmLikeDislike, ReviewFilmLikeDislikeReadDTO.class);
    }

    public void deleteReviewFilmLikeDislike(UUID id) {

        reviewFilmLikeDislikeRepository.delete(repositoryHelper.getEntityRequired(ReviewFilmLikeDislike.class, id));
    }
}
