package com.solvve.service;

import com.solvve.domain.MisprintInfoRole;
import com.solvve.dto.misprintinforole.MisprintInfoRoleReadDTO;
import com.solvve.repository.MisprintInfoRoleRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class MisprintInfoRoleService {

    @Autowired
    private MisprintInfoRoleRepository misprintInfoRoleRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public MisprintInfoRoleReadDTO getMisprintInfoRole(UUID id) {

        MisprintInfoRole misprintInfo = repositoryHelper.getEntityRequired(MisprintInfoRole.class, id);
        return translationService.translate(misprintInfo, MisprintInfoRoleReadDTO.class);
    }

    public void deleteMisprintInfoRole(UUID id) {
        misprintInfoRoleRepository.delete(repositoryHelper.getEntityRequired(MisprintInfoRole.class, id));
    }
}

