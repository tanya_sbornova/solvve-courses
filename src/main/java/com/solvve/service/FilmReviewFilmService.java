package com.solvve.service;

import com.solvve.domain.*;
import com.solvve.dto.reviewfilm.ReviewFilmCreateDTO;
import com.solvve.dto.reviewfilm.ReviewFilmPatchDTO;
import com.solvve.dto.reviewfilm.ReviewFilmPutDTO;
import com.solvve.dto.reviewfilm.ReviewFilmReadDTO;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.ReviewFilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class FilmReviewFilmService {

    @Autowired
    private ReviewFilmRepository reviewFilmRepository;

    @Autowired
    private ReviewFilmService reviewFilmService;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Transactional(readOnly = true)
    public List<ReviewFilmReadDTO> getFilmReviewsFilm(UUID filmId) {
        List<ReviewFilmReadDTO> reviewFilmsReadDTO = new ArrayList<>();
        repositoryHelper.getEntityRequired(Film.class, filmId).getReviewFilms()
                .forEach(reviewFilm -> {
                    reviewFilmsReadDTO.add(translationService.translate(reviewFilm, ReviewFilmReadDTO.class));
                });
        return reviewFilmsReadDTO;
    }

    @Transactional(readOnly = true)
    public List<ReviewFilmReadDTO> getFilmReviewsFilmOfCurrentRegisteredUser(UUID registeredUserId,
                                                                             UUID filmId) {
        List<ReviewFilmReadDTO> reviewFilmsReadDTO = new ArrayList<>();
        repositoryHelper.getEntityRequired(Film.class, filmId).getReviewFilms()
                .stream()
                .filter(rv -> rv.getRegisteredUser().getId().equals(registeredUserId))
                .forEach(reviewFilm -> {
                    reviewFilmsReadDTO.add(translationService.translate(reviewFilm, ReviewFilmReadDTO.class));
                });
        return reviewFilmsReadDTO;
    }

    public ReviewFilmReadDTO getFilmReviewFilm(UUID filmId, UUID reviewFilmId) {
        repositoryHelper.getEntityRequired(Film.class, filmId);
        repositoryHelper.getEntityRequired(ReviewFilm.class, reviewFilmId);
        return translationService.translate(repositoryHelper.getEntityAssociationRequired(Film.class, ReviewFilm.class,
                filmId, reviewFilmId), ReviewFilmReadDTO.class);
    }

    public ReviewFilmReadDTO createFilmReviewFilm(UUID filmId, ReviewFilmCreateDTO create) {

        RegisteredUser regUser = repositoryHelper.getEntityRequired(RegisteredUser.class,
                create.getRegisteredUserId());
        if (regUser.getTrustLevel() >= 4) {
            create.setStatus(ReviewStatus.MODERATED);
        } else {
            create.setStatus(ReviewStatus.NEED_TO_MODERATE);
        }
        ReviewFilm reviewFilm = translationService.translate(create, ReviewFilm.class);
        reviewFilm.setFilm(repositoryHelper.getReferenceIfExists(Film.class, filmId));
        reviewFilm = reviewFilmRepository.save(reviewFilm);
        return translationService.translate(reviewFilm, ReviewFilmReadDTO.class);
    }

    public ReviewFilmReadDTO patchFilmReviewFilm(UUID filmId, UUID reviewFilmId, ReviewFilmPatchDTO patch) {
        repositoryHelper.getEntityRequired(Film.class, filmId);
        repositoryHelper.getEntityAssociationRequired(Film.class, ReviewFilm.class,
                filmId, reviewFilmId);
        return reviewFilmService.patchReviewFilm(reviewFilmId, patch);
    }

    public ReviewFilmReadDTO updateFilmReviewFilm(UUID filmId, UUID reviewFilmId, ReviewFilmPutDTO put) {
        repositoryHelper.getEntityRequired(Film.class, filmId);
        repositoryHelper.getEntityAssociationRequired(Film.class, ReviewFilm.class,
                filmId, reviewFilmId);
        return reviewFilmService.updateReviewFilm(reviewFilmId, put);
    }

    public void deleteFilmReviewFilm(UUID filmId, UUID reviewFilmId) {
        repositoryHelper.getEntityRequired(Film.class, filmId);
        reviewFilmRepository.delete(repositoryHelper.getEntityAssociationRequired(Film.class, ReviewFilm.class,
                filmId, reviewFilmId));
    }
}
