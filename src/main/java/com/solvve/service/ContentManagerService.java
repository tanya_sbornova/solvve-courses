package com.solvve.service;

import com.solvve.domain.ContentManager;
import com.solvve.domain.PageResult;
import com.solvve.domain.UserStatus;
import com.solvve.dto.contentmanager.*;
import com.solvve.repository.ContentManagerRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ContentManagerService {

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TranslationService translationService;

    public ContentManagerReadDTO getContentManager(UUID id) {

        ContentManager contentManager = repositoryHelper.getEntityRequired(ContentManager.class, id);

        return translationService.translate(contentManager, ContentManagerReadDTO.class);
    }

    public PageResult<ContentManagerReadDTO> getContentManagers(ContentManagerFilter filter, Pageable pageable) {
        Page<ContentManager> cms = contentManagerRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(cms, ContentManagerReadDTO.class);
    }

    public ContentManagerReadDTO createContentManager(ContentManagerCreateDTO create) {

        create.setEncodedPassword(passwordEncoder.encode(create.getEncodedPassword()));
        create.setUserRole(null);
        create.setUserStatus(UserStatus.NEW);
        ContentManager contentManager = translationService.translate(create, ContentManager.class);
        contentManager = contentManagerRepository.save(contentManager);

        return translationService.translate(contentManager, ContentManagerReadDTO.class);
    }

    public ContentManagerReadDTO patchContentManager(UUID id, ContentManagerPatchDTO patch) {

        ContentManager contentManager = repositoryHelper.getEntityRequired(ContentManager.class, id);
        if (patch.getEncodedPassword() != null) {
            patch.setEncodedPassword(passwordEncoder.encode(patch.getEncodedPassword()));
        }
        translationService.map(patch, contentManager);
        contentManager = contentManagerRepository.save(contentManager);
        return translationService.translate(contentManager, ContentManagerReadDTO.class);
    }

    public ContentManagerReadDTO updateContentManager(UUID id, ContentManagerPutDTO put) {

        ContentManager contentManager = repositoryHelper.getEntityRequired(ContentManager.class, id);
        translationService.map(put, contentManager);
        contentManager = contentManagerRepository.save(contentManager);
        return translationService.translate(contentManager, ContentManagerReadDTO.class);
    }

    public void deleteContentManager(UUID id) {

        contentManagerRepository.delete(repositoryHelper.getEntityRequired(ContentManager.class, id));
    }
}


