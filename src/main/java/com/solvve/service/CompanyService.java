package com.solvve.service;

import com.solvve.domain.Company;
import com.solvve.domain.Film;
import com.solvve.dto.company.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.LinkDuplicatedException;
import com.solvve.repository.CompanyRepository;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private TranslationService translationService;

    public CompanyReadDTO getCompany(UUID id) {

        Company company = repositoryHelper.getEntityRequired(Company.class, id);
        return translationService.translate(company, CompanyReadDTO.class);
    }

    public List<CompanyReadDTO> getCompanies() {
        List<Company> companys = new ArrayList<>();
        companyRepository.findAll().forEach(companys::add);
        return translationService.translateList(companys, CompanyReadDTO.class);
    }

    public CompanyReadDTO createCompany(CompanyCreateDTO create) {

        Company company = translationService.translate(create, Company.class);
        company = companyRepository.save(company);

        return translationService.translate(company, CompanyReadDTO.class);
    }

    @Transactional
    public List<CompanyReadDTO> addCompanyToFilm(UUID filmId, UUID id) {

        Film film = repositoryHelper.getEntityRequired(Film.class, filmId);
        Company company = repositoryHelper.getEntityRequired(Company.class, id);
        if (film.getCompanies().stream().anyMatch(gnr -> gnr.getId().equals(id))) {
            throw new LinkDuplicatedException(String.format("Film %s already has company %s", filmId, id));
        }
        film.getCompanies().add(company);
        film = filmRepository.save(film);
        List<Company> listGenres = translationService.convertSetToList(film.getCompanies());
        return translationService.translateList(listGenres, CompanyReadDTO.class);
    }

    public void deleteCompany(UUID id) {
        companyRepository.delete(repositoryHelper.getEntityRequired(Company.class, id));
    }

    @Transactional
    public List<CompanyReadDTO> removeCompanyFromFilm(UUID filmId, UUID id) {

        Film film = repositoryHelper.getEntityRequired(Film.class, filmId);

        boolean removed = film.getCompanies().removeIf(gnr -> gnr.getId().equals(id));
        if (!removed) {
            throw new EntityNotFoundException("Film " + filmId + " has no company " + id);
        }
        film = filmRepository.save(film);
        List<Company> listGenres = translationService.convertSetToList(film.getCompanies());
        return translationService.translateList(listGenres, CompanyReadDTO.class);
    }
}
