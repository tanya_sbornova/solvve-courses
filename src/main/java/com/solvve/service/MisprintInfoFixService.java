package com.solvve.service;

import com.solvve.domain.*;
import com.solvve.dto.misprintinfofix.ContentFixFilter;
import com.solvve.dto.misprintinfofix.MisprintInfoFixDTO;
import com.solvve.dto.misprintinfofix.MisprintInfoFixFilter;
import com.solvve.dto.misprintinfofix.MisprintInfoFixReadDTO;
import com.solvve.exception.EntityWrongStatusException;
import com.solvve.repository.MisprintInfoFixRepositoryCustomImpl;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class MisprintInfoFixService {

    @Autowired
    private MisprintInfoFixRepositoryCustomImpl mpFixRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Transactional
    public List<MisprintInfoFixReadDTO> fixContent(UUID contentManagerId,
                                                   UUID id,
                                                   MisprintInfoFixDTO fixDTO) {
        MisprintInfo misprintInfo = repositoryHelper.getEntityRequired(MisprintInfo.class, id);

        checkMisprintInfo(misprintInfo.getId(), misprintInfo.getStatus());

        ContentFixFilter contentFixFilter = createContentFilter(id);
        Content content = mpFixRepository.findContentByMisprintInfoId(contentFixFilter, misprintInfo.getObject(),
                Pageable.unpaged());

        String replacement = (fixDTO.getText() != null) ? fixDTO.getText() : misprintInfo.getText();
        String correctText = prepareText(content.getText(), replacement,
                misprintInfo.getStartIndex(), misprintInfo.getEndIndex());

        if (!updateContent(content, misprintInfo.getStartIndex(), misprintInfo.getEndIndex(),
                misprintInfo.getIncorrectText(), correctText)) {
            misprintInfo.setStatus(MisprintStatus.CANCELLED);
            misprintInfo.setFixedDate(Instant.now());
            repositoryHelper.saveEntity(misprintInfo);
            return List.of(translationService.translate(misprintInfo, MisprintInfoFixReadDTO.class));
        }
        return updateSimilarMisprints(misprintInfo, content, replacement, contentManagerId);
    }

    private List<MisprintInfoFixReadDTO> updateSimilarMisprints(MisprintInfo misprintInfo,
                                                                Content content,
                                                                String text,
                                                                UUID contentManagerId) {
        List<MisprintInfoFixReadDTO> similarMisprints = new ArrayList<>();
        Instant fixDate = Instant.now();

        MisprintInfoFixFilter fixFilter = createMisprintInfoFilter(content.getId(),
                misprintInfo.getStartIndex(),
                misprintInfo.getEndIndex(),
                misprintInfo.getIncorrectText());
        MisprintObject typeContent = misprintInfo.getObject();

        ContentManager contentManager = repositoryHelper.getReferenceIfExists(ContentManager.class,
                contentManagerId);

        mpFixRepository.findByFilterMisprintsInfo(fixFilter, typeContent, Pageable.unpaged())
                .forEach(mpInfo -> {
                    mpInfo.setStatus(MisprintStatus.FIXED);
                    mpInfo.setFixedDate(fixDate);
                    mpInfo.setText(text);
                    mpInfo.setContentManager(contentManager);
                    repositoryHelper.saveEntity(mpInfo);
                    similarMisprints.add(translationService.translate(mpInfo, MisprintInfoFixReadDTO.class));
                });
        return similarMisprints;
    }

    private boolean updateContent(Content content, Integer startIndex, Integer endIndex,
                                  String inCorrectText, String correctText) {
        String text = content.getText();
        if (text.substring(startIndex, endIndex).contains(inCorrectText)) {
            content.setText(correctText);
            repositoryHelper.saveEntity(content);
            return true;
        }
        return false;
    }

    private String prepareText(String description, String replacement, Integer startIndex, Integer endIndex) {
        StringBuilder builder = new StringBuilder(description);
        builder.replace(startIndex, endIndex, replacement);
        return builder.toString();
    }

    private MisprintInfoFixFilter createMisprintInfoFilter(UUID contentId,
                                                           Integer startIndex,
                                                           Integer endIndex,
                                                           String incorrectText) {
        MisprintInfoFixFilter filter = new MisprintInfoFixFilter();
        filter.setContentId(contentId);
        filter.setStartIndex(startIndex);
        filter.setEndIndex(endIndex);
        filter.setIncorrectText(incorrectText);
        return filter;
    }

    private ContentFixFilter createContentFilter(UUID misprintInfoId) {
        ContentFixFilter filter = new ContentFixFilter();
        filter.setMisprintInfoId(misprintInfoId);
        return filter;
    }

    private void checkMisprintInfo(UUID id, MisprintStatus status) {
        if (status != MisprintStatus.NEED_TO_FIX) {
            throw new EntityWrongStatusException(MisprintInfoFilm.class, id, status, MisprintStatus.NEED_TO_FIX);
        }
    }
}


