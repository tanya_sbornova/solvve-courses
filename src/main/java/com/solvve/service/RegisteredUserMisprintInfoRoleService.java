package com.solvve.service;

import com.solvve.domain.RegisteredUser;
import com.solvve.domain.MisprintInfoRole;
import com.solvve.dto.misprintinforole.MisprintInfoRoleCreateDTO;
import com.solvve.dto.misprintinforole.MisprintInfoRoleReadDTO;
import com.solvve.repository.MisprintInfoRoleRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class RegisteredUserMisprintInfoRoleService {

    @Autowired
    private MisprintInfoRoleRepository misprintInfoRoleRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Transactional(readOnly = true)
    public List<MisprintInfoRoleReadDTO> getRegisteredUserMisprintInfoRoles(UUID registeredUserId) {
        List<MisprintInfoRoleReadDTO> misprintInfoRoleReadDTO = new ArrayList<>();
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId).getMisprintInfoRoles()
                .forEach(misprintInfoRole -> {
                    misprintInfoRoleReadDTO.add(translationService.translate(misprintInfoRole,
                            MisprintInfoRoleReadDTO.class));
                });
        return misprintInfoRoleReadDTO;
    }

    public MisprintInfoRoleReadDTO getRegisteredUserMisprintInfoRole(UUID registeredUserId,
                                                                     UUID misprintInfoRoleId) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                MisprintInfoRole.class, registeredUserId, misprintInfoRoleId);
        return translationService.translate(repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                MisprintInfoRole.class,
                registeredUserId, misprintInfoRoleId), MisprintInfoRoleReadDTO.class);
    }

    public MisprintInfoRoleReadDTO createRegisteredUserMisprintInfoRole(UUID roleId,
                                                                        MisprintInfoRoleCreateDTO
                                                                                misprintInfoRoleCreate) {

        MisprintInfoRole misprintInfoRole = translationService.translate(misprintInfoRoleCreate,
                MisprintInfoRole.class);
        misprintInfoRole.setRegisteredUser(repositoryHelper.getReferenceIfExists(RegisteredUser.class, roleId));
        misprintInfoRole = misprintInfoRoleRepository.save(misprintInfoRole);
        return translationService.translate(misprintInfoRole, MisprintInfoRoleReadDTO.class);
    }

    public void deleteRegisteredUserMisprintInfoRole(UUID registeredUserId, UUID misprintInfoRoleId) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        misprintInfoRoleRepository.delete(repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                MisprintInfoRole.class, registeredUserId, misprintInfoRoleId));
    }
}



