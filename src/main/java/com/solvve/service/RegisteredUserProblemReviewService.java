package com.solvve.service;

import com.solvve.domain.RegisteredUser;
import com.solvve.domain.ProblemReview;
import com.solvve.dto.problemreview.ProblemReviewCreateDTO;
import com.solvve.dto.problemreview.ProblemReviewPatchDTO;
import com.solvve.dto.problemreview.ProblemReviewPutDTO;
import com.solvve.dto.problemreview.ProblemReviewReadDTO;
import com.solvve.repository.ProblemReviewRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class RegisteredUserProblemReviewService {

    @Autowired
    private ProblemReviewRepository problemReviewRepository;

    @Autowired
    private ProblemReviewService problemReviewService;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Transactional(readOnly = true)
    public List<ProblemReviewReadDTO> getRegisteredUserProblemReviews(UUID registeredUserId) {
        List<ProblemReviewReadDTO> misprintInfoFilmsReadDTO = new ArrayList<>();
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId).getProblemReviews()
                .forEach(misprintInfoFilm -> {
                    misprintInfoFilmsReadDTO.add(translationService.translate(misprintInfoFilm,
                            ProblemReviewReadDTO.class));
                });
        return misprintInfoFilmsReadDTO;
    }

    public ProblemReviewReadDTO getRegisteredUserProblemReview(UUID registeredUserId, UUID problemReviewDislikeId) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                ProblemReview.class, registeredUserId, problemReviewDislikeId);
        return translationService.translate(repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                ProblemReview.class,
                registeredUserId, problemReviewDislikeId), ProblemReviewReadDTO.class);
    }

    public ProblemReviewReadDTO createRegisteredUserProblemReview(UUID registeredUserId,
                                                                  ProblemReviewCreateDTO problemReviewDislikeCreate) {

        ProblemReview problemReviewDislike = translationService.translate(problemReviewDislikeCreate,
                ProblemReview.class);
        problemReviewDislike.setRegisteredUser(repositoryHelper.getReferenceIfExists(RegisteredUser.class,
                registeredUserId));
        problemReviewDislike = problemReviewRepository.save(problemReviewDislike);
        return translationService.translate(problemReviewDislike, ProblemReviewReadDTO.class);
    }

    public ProblemReviewReadDTO patchRegisteredUserProblemReview(UUID registeredUserId, UUID problemReviewDislikeId,
                                                                 ProblemReviewPatchDTO patch) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                ProblemReview.class, registeredUserId, problemReviewDislikeId);
        return problemReviewService.patchProblemReview(problemReviewDislikeId, patch);
    }

    public ProblemReviewReadDTO updateRegisteredUserProblemReview(UUID registeredUserId, UUID problemReviewDislikeId,
                                                                  ProblemReviewPutDTO put) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                ProblemReview.class, registeredUserId, problemReviewDislikeId);
        return problemReviewService.updateProblemReview(problemReviewDislikeId, put);
    }

    public void deleteRegisteredUserProblemReview(UUID registeredUserId, UUID problemReviewDislikeId) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        problemReviewRepository.delete(repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                ProblemReview.class, registeredUserId, problemReviewDislikeId));
    }
}

