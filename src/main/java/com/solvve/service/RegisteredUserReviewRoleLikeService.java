package com.solvve.service;

import com.solvve.domain.RegisteredUser;
import com.solvve.domain.ReviewRoleLikeDislike;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikeCreateDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePatchDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePutDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikeReadDTO;
import com.solvve.repository.ReviewRoleLikeDislikeRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class RegisteredUserReviewRoleLikeService {

    @Autowired
    private ReviewRoleLikeDislikeRepository reviewRoleLikeDislikeRepository;

    @Autowired
    private ReviewRoleLikeDislikeService reviewRoleLikeDislikeService;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Transactional(readOnly = true)
    public List<ReviewRoleLikeDislikeReadDTO> getRegisteredUserReviewRoleLikes(UUID registeredUserId) {
        List<ReviewRoleLikeDislikeReadDTO> reviewRoleLikesReadDTO = new ArrayList<>();
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId).getReviewRoleLikeDislikes()
                .forEach(reviewRoleLike -> {
                    reviewRoleLikesReadDTO.add(translationService.translate(reviewRoleLike,
                            ReviewRoleLikeDislikeReadDTO.class));
                });
        return reviewRoleLikesReadDTO;
    }

    public ReviewRoleLikeDislikeReadDTO getRegisteredUserReviewRoleLike(UUID registeredUserId,
                                                                        UUID reviewRoleLikeDislikeId) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        repositoryHelper.getEntityRequired(ReviewRoleLikeDislike.class, reviewRoleLikeDislikeId);
        return translationService.translate(repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                ReviewRoleLikeDislike.class,
                registeredUserId, reviewRoleLikeDislikeId), ReviewRoleLikeDislikeReadDTO.class);
    }

    public ReviewRoleLikeDislikeReadDTO createRegisteredUserReviewRoleLike(UUID registeredUserId,
                                                                           ReviewRoleLikeDislikeCreateDTO
                                                                                   reviewRoleLikeDislikeCreate) {

        ReviewRoleLikeDislike reviewRoleLikeDislike = translationService.translate(reviewRoleLikeDislikeCreate,
                ReviewRoleLikeDislike.class);
        reviewRoleLikeDislike.setRegisteredUser(repositoryHelper.getReferenceIfExists(RegisteredUser.class,
                registeredUserId));
        reviewRoleLikeDislike = reviewRoleLikeDislikeRepository.save(reviewRoleLikeDislike);
        return translationService.translate(reviewRoleLikeDislike, ReviewRoleLikeDislikeReadDTO.class);
    }

    public ReviewRoleLikeDislikeReadDTO patchRegisteredUserReviewRoleLike(UUID registeredUserId,
                                                                          UUID reviewRoleLikeDislikeId,
                                                                          ReviewRoleLikeDislikePatchDTO patch) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                ReviewRoleLikeDislike.class, registeredUserId, reviewRoleLikeDislikeId);
        return reviewRoleLikeDislikeService.patchReviewRoleLikeDislike(reviewRoleLikeDislikeId, patch);
    }

    public ReviewRoleLikeDislikeReadDTO updateRegisteredUserReviewRoleLike(UUID registeredUserId,
                                                                           UUID reviewRoleLikeDislikeId,
                                                                           ReviewRoleLikeDislikePutDTO put) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                ReviewRoleLikeDislike.class, registeredUserId, reviewRoleLikeDislikeId);
        return reviewRoleLikeDislikeService.updateReviewRoleLikeDislike(reviewRoleLikeDislikeId, put);
    }

    public void deleteRegisteredUserReviewRoleLike(UUID registeredUserId, UUID reviewRoleLikeDislikeId) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        reviewRoleLikeDislikeRepository.delete(repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                ReviewRoleLikeDislike.class, registeredUserId, reviewRoleLikeDislikeId));
    }
}


