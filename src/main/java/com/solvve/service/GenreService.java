package com.solvve.service;

import com.solvve.domain.Film;
import com.solvve.domain.Genre;
import com.solvve.dto.genre.GenreCreateDTO;
import com.solvve.dto.genre.GenrePatchDTO;
import com.solvve.dto.genre.GenrePutDTO;
import com.solvve.dto.genre.GenreReadDTO;

import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.LinkDuplicatedException;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.GenreRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class GenreService {

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public GenreReadDTO getGenre(UUID id) {

        Genre genre = repositoryHelper.getEntityRequired(Genre.class, id);
        return translationService.translate(genre, GenreReadDTO.class);
    }

    public List<GenreReadDTO> getGenres() {
        List<Genre> genres = new ArrayList<>();
        genreRepository.findAll().forEach(genres::add);
        return translationService.translateList(genres, GenreReadDTO.class);
    }

    @Transactional(readOnly = true)
    public List<GenreReadDTO> getFilmGenres(UUID filmId) {
        Set<Genre> setGenres = repositoryHelper.getEntityRequired(Film.class, filmId).getGenres();
        List<Genre> listGenres = translationService.convertSetToList(setGenres);
        return translationService.translateList(listGenres, GenreReadDTO.class);
    }

    public GenreReadDTO createGenre(GenreCreateDTO create) {

        Genre genre = translationService.translate(create, Genre.class);
        genre = genreRepository.save(genre);

        return translationService.translate(genre, GenreReadDTO.class);
    }

    @Transactional
    public List<GenreReadDTO> addGenreToFilm(UUID filmId, UUID id) {

        Film film = repositoryHelper.getEntityRequired(Film.class, filmId);
        Genre genre = repositoryHelper.getEntityRequired(Genre.class, id);
        if (film.getGenres().stream().anyMatch(gnr -> gnr.getId().equals(id))) {
            throw new LinkDuplicatedException(String.format("Film %s already has genre %s", filmId, id));
        }
        film.getGenres().add(genre);
        film = filmRepository.save(film);
        List<Genre> genres = translationService.convertSetToList(film.getGenres());
        return translationService.translateList(genres, GenreReadDTO.class);
    }

    public GenreReadDTO patchGenre(UUID id, GenrePatchDTO patch) {

        Genre genre = repositoryHelper.getEntityRequired(Genre.class, id);
        translationService.map(patch, genre);
        genre = genreRepository.save(genre);
        return translationService.translate(genre, GenreReadDTO.class);
    }

    public GenreReadDTO updateGenre(UUID id, GenrePutDTO put) {

        Genre genre = repositoryHelper.getEntityRequired(Genre.class, id);
        translationService.map(put, genre);
        genre = genreRepository.save(genre);
        return translationService.translate(genre, GenreReadDTO.class);
    }

    public void deleteGenre(UUID id) {

        genreRepository.delete(repositoryHelper.getEntityRequired(Genre.class, id));
    }

    @Transactional
    public List<GenreReadDTO> removeGenreFromFilm(UUID filmId, UUID id) {

        Film film = repositoryHelper.getEntityRequired(Film.class, filmId);

        boolean removed = film.getGenres().removeIf(gnr -> gnr.getId().equals(id));
        if (!removed) {
            throw new EntityNotFoundException("Film " + filmId + " has no genre " + id);
        }
        film = filmRepository.save(film);
        List<Genre> genres = translationService.convertSetToList(film.getGenres());
        return translationService.translateList(genres, GenreReadDTO.class);
    }
}
