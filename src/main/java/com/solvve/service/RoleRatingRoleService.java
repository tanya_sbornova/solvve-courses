package com.solvve.service;

import com.solvve.domain.RatingRole;
import com.solvve.domain.Role;
import com.solvve.dto.ratingrole.RatingRoleCreateDTO;
import com.solvve.dto.ratingrole.RatingRolePatchDTO;
import com.solvve.dto.ratingrole.RatingRolePutDTO;
import com.solvve.dto.ratingrole.RatingRoleReadDTO;
import com.solvve.repository.RatingRoleRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class RoleRatingRoleService {

    @Autowired
    private RatingRoleRepository ratingRoleRepository;

    @Autowired
    private RatingRoleService ratingRoleService;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Transactional(readOnly = true)
    public List<RatingRoleReadDTO> getRoleRatingRoles(UUID roleId) {
        List<RatingRoleReadDTO> ratingRolesReadDTO = new ArrayList<>();
        repositoryHelper.getEntityRequired(Role.class, roleId).getRatingRoles()
                .forEach(ratingRole -> {
                    ratingRolesReadDTO.add(translationService.translate(ratingRole,
                            RatingRoleReadDTO.class));
                });
        return  ratingRolesReadDTO;
    }

    public RatingRoleReadDTO getRoleRatingRole(UUID roleId, UUID ratingRoleId) {
        repositoryHelper.getEntityRequired(Role.class, roleId);
        repositoryHelper.getEntityRequired(RatingRole.class, ratingRoleId);
        return translationService.translate(repositoryHelper.getEntityAssociationRequired(Role.class, RatingRole.class,
                roleId, ratingRoleId), RatingRoleReadDTO.class);
    }

    public RatingRoleReadDTO createRoleRatingRole(UUID roleId, RatingRoleCreateDTO ratingRoleCreate) {

        RatingRole ratingRole = translationService.translate(ratingRoleCreate, RatingRole.class);
        ratingRole.setRole(repositoryHelper.getReferenceIfExists(Role.class, roleId));
        ratingRole = ratingRoleRepository.save(ratingRole);
        return translationService.translate(ratingRole, RatingRoleReadDTO.class);
    }

    public RatingRoleReadDTO patchRoleRatingRole(UUID roleId, UUID ratingRoleId, RatingRolePatchDTO patch) {
        repositoryHelper.getEntityRequired(Role.class, roleId);
        repositoryHelper.getEntityAssociationRequired(Role.class, RatingRole.class,
                roleId, ratingRoleId);
        return ratingRoleService.patchRatingRole(ratingRoleId, patch);
    }

    public RatingRoleReadDTO updateRoleRatingRole(UUID roleId, UUID ratingRoleId, RatingRolePutDTO put) {
        repositoryHelper.getEntityRequired(Role.class, roleId);
        repositoryHelper.getEntityAssociationRequired(Role.class, RatingRole.class,
                roleId, ratingRoleId);
        return ratingRoleService.updateRatingRole(ratingRoleId, put);
    }

    public void deleteRoleRatingRole(UUID roleId, UUID ratingRoleId) {
        repositoryHelper.getEntityRequired(Role.class, roleId);
        ratingRoleRepository.delete(repositoryHelper.getEntityAssociationRequired(Role.class, RatingRole.class,
                roleId, ratingRoleId));
    }
}

