package com.solvve.service;

import com.solvve.domain.Actor;
import com.solvve.domain.Person;
import com.solvve.dto.actor.*;
import com.solvve.repository.ActorRepository;
import com.solvve.repository.RatingFilmRepository;
import com.solvve.repository.RatingRoleRepository;
import com.solvve.repository.RepositoryHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Slf4j
@Service
public class ActorService {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private RatingFilmRepository ratingFilmRepository;

    @Autowired
    private RatingRoleRepository ratingRoleRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private TranslationService translationService;

    public ActorReadDTO getActor(UUID id) {

        Actor actor = repositoryHelper.getEntityRequired(Actor.class, id);
        return translationService.translate(actor, ActorReadDTO.class);
    }

    @Transactional(readOnly = true)
    public ActorReadExtendedDTO getActorExtended(UUID id) {

        Actor actor = repositoryHelper.getEntityRequired(Actor.class, id);
        return translationService.translate(actor, ActorReadExtendedDTO.class);
    }

    public ActorReadDTO getPersonActor(UUID personId, UUID actorId)  {
        repositoryHelper.getEntityRequired(Person.class, personId);
        repositoryHelper.getEntityRequired(Actor.class, actorId);
        return translationService.translate(repositoryHelper.getEntityAssociationRequired(Person.class, Actor.class,
                personId, actorId), ActorReadDTO.class);
    }

    public ActorReadDTO createActor(ActorCreateDTO create) {
        Actor actor = translationService.translate(create, Actor.class);
        actor.setAverageRatingByFilm(null);
        actor.setAverageRating(null);
        actor = actorRepository.save(actor);
        return translationService.translate(actor, ActorReadDTO.class);
    }

    public ActorReadDTO createPersonActor(UUID personId, ActorCreateDTO actorsCreate) {
        repositoryHelper.getEntityRequired(Person.class, personId);
        actorsCreate.setPersonId(personId);
        Actor actor = translationService.translate(actorsCreate, Actor.class);
        actor = actorRepository.save(actor);
        return translationService.translate(actor, ActorReadDTO.class);
    }

    public void deleteActor(UUID id) {
        actorRepository.delete(repositoryHelper.getEntityRequired(Actor.class, id));
    }

    public void deletePersonActor(UUID personId, UUID actorId) {
        repositoryHelper.getEntityRequired(Person.class, personId);
        actorRepository.delete(repositoryHelper.getEntityAssociationRequired(Person.class, Actor.class,
                personId, actorId));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAverageRatingActorOfRole(UUID actorId) {
        Actor actor = repositoryHelper.getEntityRequired(Actor.class, actorId);
        Double averageRating = ratingRoleRepository.calcAverageRatingActorOfRole(actor.getId());
        log.info("Setting new average rating of actor: {}. Old value: {}, new value: {}", actorId,
                actor.getAverageRating(), averageRating);
        actor.setAverageRating(averageRating);
        actorRepository.save(actor);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAverageRatingActorByFilm(UUID actorId) {
        Actor actor = repositoryHelper.getEntityRequired(Actor.class, actorId);
        Double averageRating = ratingFilmRepository.calcAverageRatingActorByFilm(actor.getId());
        log.info("Setting new average rating of actor by film: {}. Old value: {}, new value: {}", actorId,
                actor.getAverageRatingByFilm(), averageRating);
        actor.setAverageRatingByFilm(averageRating);
        actorRepository.save(actor);
    }

    public List<ActorInLeaderBoardReadDTO> getActorsLeaderBoard() {
        return actorRepository.getActorsLeaderBoard();
    }
}

