package com.solvve.service;

import com.solvve.domain.MisprintInfoNews;
import com.solvve.domain.MisprintStatus;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsReadDTO;
import com.solvve.repository.MisprintInfoNewsRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class MisprintInfoNewsService {

    @Autowired
    private MisprintInfoNewsRepository misprintInfoNewsRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public MisprintInfoNewsReadDTO getMisprintInfoNews(UUID id) {

        MisprintInfoNews misprintInfo = repositoryHelper.getEntityRequired(MisprintInfoNews.class, id);
        return translationService.translate(misprintInfo, MisprintInfoNewsReadDTO.class);
    }

    public List<MisprintInfoNewsReadDTO> getMisprintsInfoNewsNeedToFix() {

        List<MisprintInfoNews> misprintsInfoNews = misprintInfoNewsRepository.findByStatus(MisprintStatus.NEED_TO_FIX);
        return translationService.translateList(misprintsInfoNews, MisprintInfoNewsReadDTO.class);
    }

    public void deleteMisprintInfoNews(UUID id) {
        misprintInfoNewsRepository.delete(repositoryHelper.getEntityRequired(MisprintInfoNews.class, id));
    }
}

