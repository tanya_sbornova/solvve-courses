package com.solvve.service;

import com.solvve.domain.News;
import com.solvve.domain.NewsLikeDislike;
import com.solvve.domain.RegisteredUser;
import com.solvve.dto.newslikedislike.NewsLikeDislikeCreateDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikePatchDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikePutDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikeReadDTO;
import com.solvve.repository.NewsLikeDislikeRepository;
import com.solvve.repository.NewsRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class RegisteredUserNewsLikeService {

    @Autowired
    private NewsLikeDislikeRepository newsLikeDislikeRepository;

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private NewsLikeDislikeService newsLikeDislikeService;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Transactional(readOnly = true)
    public List<NewsLikeDislikeReadDTO> getRegisteredUserNewsLikes(UUID registeredUserId) {

        List<NewsLikeDislikeReadDTO> newsLikeDislikesReadDTO = new ArrayList<>();
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId)
                .getNewsLikeDislikes()
                .forEach(newsLikeDislike -> {
                    newsLikeDislikesReadDTO.add(translationService.translate(newsLikeDislike,
                            NewsLikeDislikeReadDTO.class));
                });
        return newsLikeDislikesReadDTO;
    }

    public NewsLikeDislikeReadDTO getRegisteredUserNewsLike(UUID registeredUserId, UUID newsLikeDislikeId) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                NewsLikeDislike.class, registeredUserId, newsLikeDislikeId);
        return translationService.translate(repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                NewsLikeDislike.class,
                registeredUserId, newsLikeDislikeId), NewsLikeDislikeReadDTO.class);
    }

    @Transactional
    public NewsLikeDislikeReadDTO createRegisteredUserNewsLike(UUID registeredUserId,
                                                               NewsLikeDislikeCreateDTO newsLikeDislikeCreate) {

        NewsLikeDislike newsLikeDislike = translationService.translate(newsLikeDislikeCreate, NewsLikeDislike.class);
        newsLikeDislike.setRegisteredUser(repositoryHelper.getReferenceIfExists(RegisteredUser.class,
                registeredUserId));
        newsLikeDislike = newsLikeDislikeRepository.save(newsLikeDislike);
        UUID newsId = newsLikeDislikeCreate.getNewsId();
        //News news = newsRepository.findById(newsId).get();
        News news = repositoryHelper.getEntityRequired(News.class, newsId);
        if (newsLikeDislikeCreate.getIsLike()) {
            int cntLike = news.getCntLike();
            news.setCntLike(++cntLike);
        } else {
            int cntDisLike = news.getCntDislike();
            news.setCntDislike(++cntDisLike);
        }
        newsRepository.save(news);
        return translationService.translate(newsLikeDislike, NewsLikeDislikeReadDTO.class);
    }

    public NewsLikeDislikeReadDTO patchRegisteredUserNewsLike(UUID registeredUserId, UUID newsLikeDislikeId,
                                                              NewsLikeDislikePatchDTO patch) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                NewsLikeDislike.class, registeredUserId, newsLikeDislikeId);
        return newsLikeDislikeService.patchNewsLikeDislike(newsLikeDislikeId, patch);
    }

    public NewsLikeDislikeReadDTO updateRegisteredUserNewsLike(UUID registeredUserId, UUID newsLikeDislikeId,
                                                               NewsLikeDislikePutDTO put) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                NewsLikeDislike.class, registeredUserId, newsLikeDislikeId);
        return newsLikeDislikeService.updateNewsLikeDislike(newsLikeDislikeId, put);
    }

    @Transactional
    public void deleteRegisteredUserNewsLike(UUID registeredUserId, UUID newsLikeDislikeId) {
        repositoryHelper.getEntityRequired(RegisteredUser.class, registeredUserId);
        NewsLikeDislike newsLikeDislike = repositoryHelper.getEntityRequired(NewsLikeDislike.class, newsLikeDislikeId);
        News news = repositoryHelper.getEntityRequired(News.class, newsLikeDislike.getNews().getId());
        if (newsLikeDislike.getIsLike() && news.getCntLike() > 0) {
            news.setCntLike(news.getCntLike() - 1);
        } else if (news.getCntDislike() > 0) {
            news.setCntDislike(news.getCntDislike() - 1);
        }
        newsRepository.save(news);
        newsLikeDislikeRepository.delete(repositoryHelper.getEntityAssociationRequired(RegisteredUser.class,
                NewsLikeDislike.class, registeredUserId, newsLikeDislikeId));
    }
}
