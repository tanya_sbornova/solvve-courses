package com.solvve.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Slf4j
@Service
public class DynamicPropertyExampleService {

    @Value("${dynamic.property.value}")
    private String dynamicPropertyValue;

    @PostConstruct
    void init() {
        log.info("Hello {}!", dynamicPropertyValue);
    }
}
