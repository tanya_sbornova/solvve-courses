package com.solvve.security;

import com.solvve.domain.UserAccount;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class UserDetailsImpl extends org.springframework.security.core.userdetails.User {

    private UUID id;

    public UserDetailsImpl(UserAccount user) {

        super(user.getEmail(), user.getEncodedPassword(),
                List.of(new SimpleGrantedAuthority(user.getUserRole().toString())));

        id = user.getId();
    }
}