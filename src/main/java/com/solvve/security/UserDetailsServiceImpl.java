package com.solvve.security;

import com.solvve.domain.Admin;
import com.solvve.domain.ContentManager;
import com.solvve.domain.Moderator;
import com.solvve.domain.RegisteredUser;
import com.solvve.repository.AdminRepository;
import com.solvve.repository.ContentManagerRepository;
import com.solvve.repository.ModeratorRepository;
import com.solvve.repository.RegisteredUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private ContentManagerRepository contentManagerRepository;

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Admin admin = adminRepository.findByEmail(username);
        if (admin != null) {
            return new UserDetailsImpl(admin);
        }
        ContentManager contentManager = contentManagerRepository.findByEmail(username);
        if (contentManager != null) {
            return new UserDetailsImpl(contentManager);
        }
        Moderator moderator = moderatorRepository.findByEmail(username);
        if (moderator != null) {
            return new UserDetailsImpl(moderator);
        }
        RegisteredUser user = registeredUserRepository.findByEmail(username);
        if (user != null) {
            return new UserDetailsImpl(user);
        }
        throw new UsernameNotFoundException("User " + username + " is not found!");
    }
}
