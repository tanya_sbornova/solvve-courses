package com.solvve.controller;

import com.solvve.controller.security.ContentManager;
import com.solvve.controller.security.ContentManagerOrRegisteredUser;
import com.solvve.dto.person.*;
import com.solvve.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/persons")
public class PersonController {

    @Autowired
    private PersonService personService;

    @ContentManagerOrRegisteredUser
    @GetMapping("/{id}")
    public PersonReadDTO getPerson(@PathVariable UUID id) {
        return personService.getPerson(id);
    }

    @ContentManager
    @PostMapping
    public PersonReadDTO createPerson(@RequestBody @Valid PersonCreateDTO createDTO) {
        return personService.createPerson(createDTO);
    }

    @ContentManager
    @PatchMapping("/{id}")
    public PersonReadDTO patchAdmin(@PathVariable UUID id, @RequestBody PersonPatchDTO patch) {
        return personService.patchPerson(id, patch);
    }

    @ContentManager
    @PutMapping("/{id}")
    public PersonReadDTO updateAdmin(@PathVariable UUID id, @RequestBody PersonPutDTO put) {
        return personService.updatePerson(id, put);
    }

    @ContentManager
    @DeleteMapping("/{id}")
    public void deletePerson(@PathVariable UUID id) {
        personService.deletePerson(id);
    }
}
