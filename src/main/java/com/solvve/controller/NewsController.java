package com.solvve.controller;

import com.solvve.controller.security.ContentManager;
import com.solvve.controller.security.ContentManagerOrRegisteredUser;
import com.solvve.dto.news.NewsCreateDTO;
import com.solvve.dto.news.NewsPatchDTO;
import com.solvve.dto.news.NewsPutDTO;
import com.solvve.dto.news.NewsReadDTO;
import com.solvve.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class NewsController {

    @Autowired
    private NewsService newsService;

    @GetMapping("/unregistered-user/news/{id}")
    public NewsReadDTO getNewsForUnregisteredUser(@PathVariable UUID id) {
        return newsService.getNews(id);
    }

    @ContentManagerOrRegisteredUser
    @GetMapping("/news/{id}")
    public NewsReadDTO getNews(@PathVariable UUID id) {
        return newsService.getNews(id);
    }

    @ContentManager
    @PostMapping("/news")
    public NewsReadDTO createNews(@RequestBody @Valid NewsCreateDTO createDTO) {
        return newsService.createNews(createDTO);
    }

    @ContentManager
    @PatchMapping("/news/{id}")
    public NewsReadDTO patchNews(@PathVariable UUID id, @RequestBody NewsPatchDTO patch) {
        return newsService.patchNews(id, patch);
    }

    @ContentManager
    @PutMapping("/news/{id}")
    public NewsReadDTO updateNews(@PathVariable UUID id, @RequestBody NewsPutDTO put) {
        return newsService.updateNews(id, put);
    }

    @ContentManager
    @DeleteMapping("/news/{id}")
    public void deleteNews(@PathVariable UUID id) {
        newsService.deleteNews(id);
    }
}



