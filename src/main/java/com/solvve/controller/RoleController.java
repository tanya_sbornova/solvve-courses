package com.solvve.controller;

import com.solvve.controller.documentation.ApiPageable;
import com.solvve.controller.security.ContentManager;
import com.solvve.controller.security.ContentManagerOrRegisteredUser;
import com.solvve.domain.PageResult;
import com.solvve.dto.role.*;
import com.solvve.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @ContentManagerOrRegisteredUser
    @GetMapping("/roles/{id}")
    public RoleReadDTO getRole(@PathVariable UUID id) {
        return roleService.getRole(id);
    }

    @ContentManagerOrRegisteredUser
    @GetMapping("/roles/{id}/extended")
    public RoleReadExtendedDTO getRoleExtended(@PathVariable UUID id) {
        return roleService.getRoleExtended(id);
    }

    @ContentManagerOrRegisteredUser
    @ApiPageable
    @PostMapping("/roles-filter")
    public PageResult<RoleReadDTO> getRolesFilter(@RequestBody RoleFilter filter, @ApiIgnore Pageable pageable) {
        return roleService.getRoles(filter, pageable);
    }

    @ContentManagerOrRegisteredUser
    @ApiPageable
    @GetMapping("/roles")
    public PageResult<RoleReadDTO> getRoles(RoleFilter filter, @ApiIgnore Pageable pageable) {
        return roleService.getRoles(filter, pageable);
    }

    @ContentManagerOrRegisteredUser
    @GetMapping("/actor/{actorId}/roles")
    public List<RoleReadDTO> getActorRoles(@PathVariable UUID actorId) {
        return roleService.getActorRoles(actorId);
    }

    @ContentManager
    @PostMapping("/roles")
    public RoleReadDTO createRole(@RequestBody @Valid RoleCreateDTO createDTO) {
        return roleService.createRole(createDTO);
    }

    @ContentManager
    @PostMapping("/actor/{actorId}/roles/{id}")
    public List<RoleReadDTO> addRoleToActor(@PathVariable UUID actorId, @PathVariable UUID id) {
        return roleService.addRoleToActor(actorId, id);
    }

    @ContentManager
    @PatchMapping("/roles/{id}")
    public RoleReadDTO patchRole(@PathVariable UUID id, @RequestBody RolePatchDTO patch) {
        return roleService.patchRole(id, patch);
    }

    @ContentManager
    @PutMapping("/roles/{id}")
    public RoleReadDTO updateRole(@PathVariable UUID id, @RequestBody RolePutDTO put) {
        return roleService.updateRole(id, put);
    }

    @ContentManager
    @DeleteMapping("/roles/{id}")
    public void deleteRole(@PathVariable UUID id) {
        roleService.deleteRole(id);
    }

    @ContentManager
    @DeleteMapping("/actor/{actorId}/roles/{id}")
    public List<RoleReadDTO> removeRoleFromActor(@PathVariable UUID actorId, @PathVariable UUID id) {
        return roleService.removeRoleFromActor(actorId, id);
    }

}
