package com.solvve.controller;

import com.solvve.controller.security.ContentManager;
import com.solvve.controller.security.ContentManagerOrRegisteredUser;
import com.solvve.dto.crewmember.CrewMemberPatchDTO;
import com.solvve.dto.crewmember.CrewMemberPutDTO;
import com.solvve.dto.crewmember.CrewMemberReadDTO;
import com.solvve.service.FilmCrewMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/film/{filmId}/crew-members")
public class FilmCrewMemberController {

    @Autowired
    private FilmCrewMemberService filmCrewMemberService;

    @ContentManagerOrRegisteredUser
    @GetMapping
    public List<CrewMemberReadDTO> getFilmCrewMembers(@PathVariable UUID filmId) {
        return filmCrewMemberService.getFilmCrewMembers(filmId);
    }

    @ContentManagerOrRegisteredUser
    @GetMapping("/{crewMemberId}")
    public CrewMemberReadDTO getFilmCrewMember(@PathVariable UUID filmId,
                                                 @PathVariable UUID crewMemberId) {
        return filmCrewMemberService.getFilmCrewMember(filmId, crewMemberId);
    }

    @ContentManager
    @PatchMapping("/{crewMemberId}")
    public CrewMemberReadDTO patchFilmCrewMember(@PathVariable UUID filmId,
                                                   @PathVariable UUID crewMemberId,
                                                   @RequestBody CrewMemberPatchDTO patch) {
        return filmCrewMemberService.patchFilmCrewMember(filmId, crewMemberId, patch);
    }

    @ContentManager
    @PutMapping("/{crewMemberId}")
    public CrewMemberReadDTO updateFilmCrewMember(@PathVariable UUID filmId,
                                                    @PathVariable UUID crewMemberId,
                                                    @RequestBody CrewMemberPutDTO put) {
        return filmCrewMemberService.updateFilmCrewMember(filmId, crewMemberId, put);
    }
}
