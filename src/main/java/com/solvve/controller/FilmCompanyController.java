package com.solvve.controller;

import com.solvve.controller.security.ContentManagerOrRegisteredUser;
import com.solvve.dto.company.CompanyReadDTO;
import com.solvve.service.FilmCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/film/{filmId}/companies")
public class FilmCompanyController {

    @Autowired
    private FilmCompanyService filmCompanyService;

    @ContentManagerOrRegisteredUser
    @GetMapping
    public List<CompanyReadDTO> getFilmCompanies(@PathVariable UUID filmId) {
        return filmCompanyService.getFilmCompanies(filmId);
    }

    @ContentManagerOrRegisteredUser
    @GetMapping("/{companyId}")
    public CompanyReadDTO getFilmCompany(@PathVariable UUID filmId,
                                               @PathVariable UUID companyId) {
        return filmCompanyService.getFilmCompany(filmId, companyId);
    }
}
