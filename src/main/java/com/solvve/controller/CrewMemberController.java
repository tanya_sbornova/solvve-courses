package com.solvve.controller;

import com.solvve.controller.security.ContentManager;
import com.solvve.controller.security.ContentManagerOrRegisteredUser;
import com.solvve.dto.crewmember.*;
import com.solvve.service.CrewMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class CrewMemberController {

    @Autowired
    private CrewMemberService crewMemberService;

    @ContentManagerOrRegisteredUser
    @GetMapping("/crew-members/{id}")
    public CrewMemberReadDTO getCrewMember(@PathVariable UUID id) {
        return crewMemberService.getCrewMember(id);
    }

    @ContentManagerOrRegisteredUser
    @GetMapping("/crew-members")
    public List<CrewMemberReadDTO> getCrewMembers() {
        return crewMemberService.getCrewMembers();
    }

    @ContentManagerOrRegisteredUser
    @GetMapping("/department/{departmentId}/crewMembers")
    public List<CrewMemberReadDTO> getDepartmentCrewMembers(@PathVariable UUID departmentId) {
        return crewMemberService.getDepartmentCrewMembers(departmentId);
    }

    @ContentManager
    @PostMapping("/crew-members")
    public CrewMemberReadDTO createCrewMember(@RequestBody @Valid CrewMemberCreateDTO createDTO) {
        return crewMemberService.createCrewMember(createDTO);
    }

    @ContentManager
    @PostMapping("/film/{filmId}/crew-members/{id}")
    public List<CrewMemberReadDTO> addCrewMemberToFilm(@PathVariable UUID filmId, @PathVariable UUID id) {
        return crewMemberService.addCrewMemberToFilm(filmId, id);
    }

    @ContentManager
    @PostMapping("/department/{departmentId}/crew-members/{id}")
    public List<CrewMemberReadDTO> addCrewMemberToDepartment(@PathVariable UUID departmentId,
                                                             @PathVariable UUID id) {
        return crewMemberService.addCrewMemberToDepartment(departmentId, id);
    }

    @ContentManager
    @PatchMapping("/crew-members/{id}")
    public CrewMemberReadDTO patchCrewMember(@PathVariable UUID id,
                                             @RequestBody CrewMemberPatchDTO patch) {
        return crewMemberService.patchCrewMember(id, patch);
    }

    @ContentManager
    @PutMapping("/crew-members/{id}")
    public CrewMemberReadDTO updateCrewMember(@PathVariable UUID id, @RequestBody CrewMemberPutDTO put) {
        return crewMemberService.updateCrewMember(id, put);
    }

    @ContentManager
    @DeleteMapping("/crew-members/{id}")
    public void deleteCrewMember(@PathVariable UUID id) {
        crewMemberService.deleteCrewMember(id);
    }

    @ContentManager
    @DeleteMapping("/film/{filmId}/crew-members/{id}")
    public List<CrewMemberReadDTO> removeCrewMemberFromFilm(@PathVariable UUID filmId, @PathVariable UUID id) {
        return crewMemberService.removeCrewMemberFromFilm(filmId, id);
    }

    @ContentManager
    @DeleteMapping("/department/{departmentId}/crew-members/{id}")
    public List<CrewMemberReadDTO> removeCrewMemberFromDepartment(@PathVariable UUID departmentId,
                                                                  @PathVariable UUID id) {
        return crewMemberService.removeCrewMemberFromDepartment(departmentId, id);
    }
}
