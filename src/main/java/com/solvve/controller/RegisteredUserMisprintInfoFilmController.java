package com.solvve.controller;

import com.solvve.controller.security.CurrentRegisteredUser;
import com.solvve.dto.misprintinfofilm.MisprintInfoFilmCreateDTO;
import com.solvve.dto.misprintinfofilm.MisprintInfoFilmReadDTO;
import com.solvve.service.RegisteredUserMisprintInfoFilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/registered-user/{registeredUserId}/misprint-info-films")
public class RegisteredUserMisprintInfoFilmController  {

    @Autowired
    private RegisteredUserMisprintInfoFilmService registeredUserMisprintInfoFilmService;

    @CurrentRegisteredUser
    @GetMapping
    public List<MisprintInfoFilmReadDTO> getRegisteredUserMisprintInfoFilms(@PathVariable UUID registeredUserId) {
        return registeredUserMisprintInfoFilmService.getRegisteredUserMisprintInfoFilms(registeredUserId);
    }

    @CurrentRegisteredUser
    @GetMapping("/{misprintInfoFilmId}")
    public MisprintInfoFilmReadDTO getRegisteredUserMisprintInfoFilm(@PathVariable UUID registeredUserId,
                                                               @PathVariable UUID misprintInfoFilmId) {
        return registeredUserMisprintInfoFilmService.getRegisteredUserMisprintInfoFilm(registeredUserId,
                misprintInfoFilmId);
    }

    @CurrentRegisteredUser
    @PostMapping
    public MisprintInfoFilmReadDTO createMisprintInfoFilm(@PathVariable UUID registeredUserId,
                                                    @RequestBody MisprintInfoFilmCreateDTO createDTO) {
        return registeredUserMisprintInfoFilmService.createRegisteredUserMisprintInfoFilm(registeredUserId, createDTO);
    }

    @CurrentRegisteredUser
    @DeleteMapping("/{misprintInfoFilmId}")
    public void deleteRegisteredUserMisprintInfoFilm(@PathVariable UUID registeredUserId,
                                                     @PathVariable UUID misprintInfoFilmId) {
        registeredUserMisprintInfoFilmService.deleteRegisteredUserMisprintInfoFilm(registeredUserId,
                misprintInfoFilmId);
    }
}