package com.solvve.controller;

import com.solvve.controller.security.CurrentRegisteredUser;
import com.solvve.controller.security.ModeratorOrRegisteredUser;
import com.solvve.controller.security.RegisteredUser;
import com.solvve.dto.reviewfilm.ReviewFilmCreateDTO;
import com.solvve.dto.reviewfilm.ReviewFilmPatchDTO;
import com.solvve.dto.reviewfilm.ReviewFilmPutDTO;
import com.solvve.dto.reviewfilm.ReviewFilmReadDTO;
import com.solvve.service.FilmReviewFilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class FilmReviewFilmController {

    @Autowired
    private FilmReviewFilmService filmReviewFilmService;

    @ModeratorOrRegisteredUser
    @GetMapping("/film/{filmId}/review-films")
    public List<ReviewFilmReadDTO> getFilmReviewsFilms(@PathVariable UUID filmId) {
        return filmReviewFilmService.getFilmReviewsFilm(filmId);
    }

    @CurrentRegisteredUser
    @GetMapping("/registered-user/{registeredUserId}/film/{filmId}/review-films")
    public List<ReviewFilmReadDTO> getFilmReviewsFilmOfCurrentRegisteredUser(@PathVariable UUID registeredUserId,
                                                                      @PathVariable UUID filmId) {
        return filmReviewFilmService.getFilmReviewsFilmOfCurrentRegisteredUser(registeredUserId, filmId);
    }

    @ModeratorOrRegisteredUser
    @GetMapping("/film/{filmId}/review-films/{reviewFilmId}")
    public ReviewFilmReadDTO getFilmReviewFilm(@PathVariable UUID filmId,
                                               @PathVariable UUID reviewFilmId) {
        return filmReviewFilmService.getFilmReviewFilm(filmId, reviewFilmId);
    }

    @RegisteredUser
    @PostMapping("/film/{filmId}/review-films")
    public ReviewFilmReadDTO createFilmReviewFilm(@PathVariable UUID filmId,
                                              @RequestBody @Valid ReviewFilmCreateDTO createDTO) {
        return filmReviewFilmService.createFilmReviewFilm(filmId, createDTO);
    }

    @ModeratorOrRegisteredUser
    @PatchMapping("/film/{filmId}/review-films/{reviewFilmId}")
    public ReviewFilmReadDTO patchFilmReviewFilm(@PathVariable UUID filmId,
                                                 @PathVariable UUID reviewFilmId,
                                                 @RequestBody ReviewFilmPatchDTO patch) {
        return filmReviewFilmService.patchFilmReviewFilm(filmId, reviewFilmId, patch);
    }

    @ModeratorOrRegisteredUser
    @PutMapping("/film/{filmId}/review-films/{reviewFilmId}")
    public ReviewFilmReadDTO updateFilmReviewFilm(@PathVariable UUID filmId,
                                                  @PathVariable UUID reviewFilmId,
                                                  @RequestBody ReviewFilmPutDTO put) {
        return filmReviewFilmService.updateFilmReviewFilm(filmId, reviewFilmId, put);
    }

    @ModeratorOrRegisteredUser
    @DeleteMapping("/film/{filmId}/review-films/{reviewFilmId}")
    public void deleteFilmReviewFilm(@PathVariable UUID filmId, @PathVariable UUID reviewFilmId) {
        filmReviewFilmService.deleteFilmReviewFilm(filmId, reviewFilmId);
    }
}
