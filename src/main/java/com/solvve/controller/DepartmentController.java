package com.solvve.controller;

import com.solvve.controller.security.ContentManager;
import com.solvve.controller.security.ContentManagerOrRegisteredUser;
import com.solvve.dto.department.*;
import com.solvve.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @ContentManagerOrRegisteredUser
    @GetMapping("/departments/{id}")
    public DepartmentReadDTO getDepartment(@PathVariable UUID id) {
        return departmentService.getDepartment(id);
    }

    @ContentManager
    @PostMapping("/departments")
    public DepartmentReadDTO createDepartment(@RequestBody @Valid DepartmentCreateDTO createDTO) {
        return departmentService.createDepartment(createDTO);
    }

    @ContentManager
    @DeleteMapping("/departments/{id}")
    public void deleteDepartment(@PathVariable UUID id) {
        departmentService.deleteDepartment(id);
    }
}
