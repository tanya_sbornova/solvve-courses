package com.solvve.controller;

import com.solvve.controller.security.ModeratorOrRegisteredUser;
import com.solvve.controller.security.RegisteredUser;
import com.solvve.dto.ratingfilm.RatingFilmCreateDTO;
import com.solvve.dto.ratingfilm.RatingFilmPatchDTO;
import com.solvve.dto.ratingfilm.RatingFilmPutDTO;
import com.solvve.dto.ratingfilm.RatingFilmReadDTO;
import com.solvve.service.FilmRatingFilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/film/{filmId}/rating-films")
public class FilmRatingFilmController {

    @Autowired
    private FilmRatingFilmService filmRatingFilmService;

    @ModeratorOrRegisteredUser
    @GetMapping
    public List<RatingFilmReadDTO> getFilmRatingFilms(@PathVariable UUID filmId) {
        return filmRatingFilmService.getFilmRatingFilms(filmId);
    }

    @ModeratorOrRegisteredUser
    @GetMapping("/{ratingFilmId}")
    public RatingFilmReadDTO getFilmRatingFilm(@PathVariable UUID filmId,
                                               @PathVariable UUID ratingFilmId) {
        return filmRatingFilmService.getFilmRatingFilm(filmId, ratingFilmId);
    }

    @RegisteredUser
    @PostMapping
    public RatingFilmReadDTO createFilmRatingFilm(@PathVariable UUID filmId,
                                                  @RequestBody @Valid RatingFilmCreateDTO createDTO) {
        return filmRatingFilmService.createFilmRatingFilm(filmId, createDTO);
    }

    @RegisteredUser
    @PatchMapping("/{ratingFilmId}")
    public RatingFilmReadDTO patchFilmRatingFilm(@PathVariable UUID filmId,
                                                 @PathVariable UUID ratingFilmId,
                                                 @RequestBody RatingFilmPatchDTO patch) {
        return filmRatingFilmService.patchFilmRatingFilm(filmId, ratingFilmId, patch);
    }

    @RegisteredUser
    @PutMapping("/{ratingFilmId}")
    public RatingFilmReadDTO updateFilmRatingFilm(@PathVariable UUID filmId,
                                                  @PathVariable UUID ratingFilmId,
                                                  @RequestBody RatingFilmPutDTO put) {
        return filmRatingFilmService.updateFilmRatingFilm(filmId, ratingFilmId, put);
    }

    @RegisteredUser
    @DeleteMapping("/{ratingFilmId}")
    public void deleteFilmRatingFilm(@PathVariable UUID filmId, @PathVariable UUID ratingFilmId) {
        filmRatingFilmService.deleteFilmRatingFilm(filmId, ratingFilmId);
    }
}