package com.solvve.controller;

import com.solvve.controller.documentation.ApiPageable;
import com.solvve.controller.security.Admin;
import com.solvve.domain.PageResult;
import com.solvve.dto.contentmanager.*;
import com.solvve.service.ContentManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/content-managers")
public class ContentManagerController {

    @Autowired
    private ContentManagerService contentManagerService;

    @Admin
    @GetMapping("/{id}")
    public ContentManagerReadDTO getContentManager(@PathVariable UUID id) {
        return contentManagerService.getContentManager(id);
    }

    @Admin
    @ApiPageable
    @GetMapping
    public PageResult<ContentManagerReadDTO> getContentManagers(ContentManagerFilter filter,
                                                                @ApiIgnore Pageable pageable) {
        return contentManagerService.getContentManagers(filter, pageable);
    }

    @PostMapping
    public ContentManagerReadDTO createContentManager(@RequestBody @Valid ContentManagerCreateDTO createDTO) {
        return contentManagerService.createContentManager(createDTO);
    }

    @Admin
    @PatchMapping("/{id}")
    public ContentManagerReadDTO patchContentManager(@PathVariable UUID id,
                                                     @RequestBody ContentManagerPatchDTO patch) {
        return contentManagerService.patchContentManager(id, patch);
    }

    @Admin
    @PutMapping("/{id}")
    public ContentManagerReadDTO updateContentManager(@PathVariable UUID id,
                                                      @RequestBody ContentManagerPutDTO put) {
        return contentManagerService.updateContentManager(id, put);
    }

    @Admin
    @DeleteMapping("/{id}")
    public void deleteContentManager(@PathVariable UUID id) {
        contentManagerService.deleteContentManager(id);
    }
}

