package com.solvve.controller;

import com.solvve.controller.security.ContentManager;
import com.solvve.controller.security.ContentManagerOrRegisteredUser;
import com.solvve.dto.genre.GenreCreateDTO;
import com.solvve.dto.genre.GenrePatchDTO;
import com.solvve.dto.genre.GenrePutDTO;
import com.solvve.dto.genre.GenreReadDTO;
import com.solvve.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class GenreController {

    @Autowired
    private GenreService genreService;

    @ContentManagerOrRegisteredUser
    @GetMapping("/genres/{id}")
    public GenreReadDTO getGenre(@PathVariable UUID id) {
        return genreService.getGenre(id);
    }

    @ContentManagerOrRegisteredUser
    @GetMapping("/genres")
    public List<GenreReadDTO> getGenres() {
        return genreService.getGenres();
    }

    @ContentManagerOrRegisteredUser
    @GetMapping("/film/{filmId}/genres")
    public List<GenreReadDTO> getFilmGenres(@PathVariable UUID filmId) {
        return genreService.getFilmGenres(filmId);
    }

    @ContentManager
    @PostMapping("/genres")
    public GenreReadDTO createGenre(@RequestBody @Valid GenreCreateDTO createDTO) {
        return genreService.createGenre(createDTO);
    }

    @ContentManager
    @PostMapping("/film/{filmId}/genres/{id}")
    public List<GenreReadDTO> addGenreToFilm(@PathVariable UUID filmId, @PathVariable UUID id) {
        return genreService.addGenreToFilm(filmId, id);
    }

    @ContentManager
    @PatchMapping("/genres/{id}")
    public GenreReadDTO patchGenre(@PathVariable UUID id, @RequestBody GenrePatchDTO patch) {
        return genreService.patchGenre(id, patch);
    }

    @ContentManager
    @PutMapping("/genres/{id}")
    public GenreReadDTO updateGenre(@PathVariable UUID id, @RequestBody GenrePutDTO put) {
        return genreService.updateGenre(id, put);
    }

    @ContentManager
    @DeleteMapping("/genres/{id}")
    public void deleteGenre(@PathVariable UUID id) {
        genreService.deleteGenre(id);
    }

    @ContentManager
    @DeleteMapping("/film/{filmId}/genres/{id}")
    public List<GenreReadDTO> removeGenreFromFilm(@PathVariable UUID filmId, @PathVariable UUID id) {
        return genreService.removeGenreFromFilm(filmId, id);
    }
}