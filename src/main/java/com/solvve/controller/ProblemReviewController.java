package com.solvve.controller;

import com.solvve.controller.security.Moderator;
import com.solvve.controller.security.RegisteredUser;
import com.solvve.dto.problemreview.ProblemReviewCreateDTO;
import com.solvve.dto.problemreview.ProblemReviewPatchDTO;
import com.solvve.dto.problemreview.ProblemReviewPutDTO;
import com.solvve.dto.problemreview.ProblemReviewReadDTO;
import com.solvve.service.ProblemReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/problem-reviews")
public class ProblemReviewController {

    @Autowired
    private ProblemReviewService problemReviewService;

    @Moderator
    @GetMapping("/{id}")
    public ProblemReviewReadDTO getProblemReview(@PathVariable UUID id) {
        return problemReviewService.getProblemReview(id);
    }

    @Moderator
    @GetMapping
    public List<ProblemReviewReadDTO> getProblemReviews() {
        return problemReviewService.getProblemReviews();
    }

    @RegisteredUser
    @PostMapping
    public ProblemReviewReadDTO createProblemReview(@RequestBody @Valid ProblemReviewCreateDTO createDTO) {
        return problemReviewService.createProblemReview(createDTO);
    }

    @Moderator
    @PatchMapping("/{id}")
    public ProblemReviewReadDTO patchProblemReview(@PathVariable UUID id,
                                                   @RequestBody ProblemReviewPatchDTO patch) {
        return problemReviewService.patchProblemReview(id, patch);
    }

    @Moderator
    @PutMapping("/{id}")
    public ProblemReviewReadDTO updateProblemReview(@PathVariable UUID id,
                                                    @RequestBody ProblemReviewPutDTO put) {
        return problemReviewService.updateProblemReview(id, put);
    }

    @Moderator
    @DeleteMapping("/{id}")
    public void deleteProblemReview(@PathVariable UUID id) {
        problemReviewService.deleteProblemReview(id);
    }
}

