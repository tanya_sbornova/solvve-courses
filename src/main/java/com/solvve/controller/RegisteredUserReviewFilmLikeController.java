package com.solvve.controller;

import com.solvve.controller.security.CurrentRegisteredUser;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeCreateDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePatchDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePutDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeReadDTO;
import com.solvve.service.RegisteredUserReviewFilmLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/registered-user/{registeredUserId}/review-film-like-dislike")
public class RegisteredUserReviewFilmLikeController {

    @Autowired
    private RegisteredUserReviewFilmLikeService registeredUserReviewFilmLikeService;

    @CurrentRegisteredUser
    @GetMapping
    public List<ReviewFilmLikeDislikeReadDTO> getRegisteredUserReviewFilmLikes(@PathVariable UUID registeredUserId) {
        return registeredUserReviewFilmLikeService.getRegisteredUserReviewFilmLikes(registeredUserId);
    }

    @CurrentRegisteredUser
    @GetMapping("/{reviewFilmLikeDislikeId}")
    public ReviewFilmLikeDislikeReadDTO getRegisteredUserReviewFilmLike(@PathVariable UUID registeredUserId,
                                                                        @PathVariable UUID reviewFilmLikeDislikeId) {
        return registeredUserReviewFilmLikeService.getRegisteredUserReviewFilmLike(registeredUserId,
                reviewFilmLikeDislikeId);
    }

    @CurrentRegisteredUser
    @PostMapping
    public ReviewFilmLikeDislikeReadDTO
            createRegisteredUserReviewFilmLike(@PathVariable UUID registeredUserId,
                                       @RequestBody @Valid ReviewFilmLikeDislikeCreateDTO
                                               createDTO) {
        return registeredUserReviewFilmLikeService.createRegisteredUserReviewFilmLike(registeredUserId, createDTO);
    }

    @CurrentRegisteredUser
    @PatchMapping("/{reviewFilmLikeDislikeId}")
    public ReviewFilmLikeDislikeReadDTO
            patchRegisteredUserReviewFilmLike(@PathVariable UUID registeredUserId,
                                      @PathVariable UUID reviewFilmLikeDislikeId,
                                      @RequestBody ReviewFilmLikeDislikePatchDTO patch) {
        return registeredUserReviewFilmLikeService.patchRegisteredUserReviewFilmLike(registeredUserId,
                reviewFilmLikeDislikeId, patch);
    }

    @CurrentRegisteredUser
    @PutMapping("/{reviewFilmLikeDislikeId}")
    public ReviewFilmLikeDislikeReadDTO
            updateRegisteredUserReviewFilmLike(@PathVariable UUID registeredUserId,
                                       @PathVariable UUID reviewFilmLikeDislikeId,
                                       @RequestBody ReviewFilmLikeDislikePutDTO put) {
        return registeredUserReviewFilmLikeService.updateRegisteredUserReviewFilmLike(registeredUserId,
                reviewFilmLikeDislikeId, put);
    }

    @CurrentRegisteredUser
    @DeleteMapping("/{reviewFilmLikeDislikeId}")
    public void deleteRegisteredUserReviewFilmLike(@PathVariable UUID registeredUserId,
                                                   @PathVariable UUID reviewFilmLikeDislikeId) {
        registeredUserReviewFilmLikeService.deleteRegisteredUserReviewFilmLike(registeredUserId,
                reviewFilmLikeDislikeId);
    }
}



