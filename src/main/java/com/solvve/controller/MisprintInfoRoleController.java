package com.solvve.controller;

import com.solvve.controller.security.ContentManager;
import com.solvve.dto.misprintinforole.MisprintInfoRoleReadDTO;
import com.solvve.service.MisprintInfoRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/misprint-info-roles")
public class MisprintInfoRoleController {

    @Autowired
    private MisprintInfoRoleService misprintInfoService;

    @ContentManager
    @GetMapping("/{id}")
    public MisprintInfoRoleReadDTO getMisprintInfoRole(@PathVariable UUID id) {
        return misprintInfoService.getMisprintInfoRole(id);
    }

    @ContentManager
    @DeleteMapping("/{id}")
    public void deleteMisprintInfoRole(@PathVariable UUID id) {
        misprintInfoService.deleteMisprintInfoRole(id);
    }
}


