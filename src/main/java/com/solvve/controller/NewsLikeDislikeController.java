package com.solvve.controller;

import com.solvve.controller.security.ContentManagerOrRegisteredUser;
import com.solvve.controller.security.RegisteredUser;
import com.solvve.dto.newslikedislike.NewsLikeDislikePatchDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikePutDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikeReadDTO;
import com.solvve.service.NewsLikeDislikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/news-like-dislikes")
public class NewsLikeDislikeController {

    @Autowired
    private NewsLikeDislikeService newsLikeDislikeService;

    @ContentManagerOrRegisteredUser
    @GetMapping("/{id}")
    public NewsLikeDislikeReadDTO getNewsLikeDislike(@PathVariable UUID id) {
        return newsLikeDislikeService.getNewsLikeDislike(id);
    }


    @RegisteredUser
    @PatchMapping("/{id}")
    public NewsLikeDislikeReadDTO patchNewsLikeDislike(@PathVariable UUID id,
                                                       @RequestBody NewsLikeDislikePatchDTO patch) {
        return newsLikeDislikeService.patchNewsLikeDislike(id, patch);
    }

    @RegisteredUser
    @PutMapping("/{id}")
    public NewsLikeDislikeReadDTO updateNewsLikeDislike(@PathVariable UUID id,
                                                        @RequestBody NewsLikeDislikePutDTO put) {
        return newsLikeDislikeService.updateNewsLikeDislike(id, put);
    }

    @RegisteredUser
    @DeleteMapping("/{id}")
    public void deleteNewsLikeDislike(@PathVariable UUID id) {
        newsLikeDislikeService.deleteNewsLikeDislike(id);
    }

}

