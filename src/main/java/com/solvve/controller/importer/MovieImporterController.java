package com.solvve.controller.importer;

import com.solvve.controller.security.ContentManager;
import com.solvve.exception.ImportAlreadyPerformedException;
import com.solvve.exception.ImportedEntityAlreadyExistException;
import com.solvve.service.importer.MovieImporterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/movies/import")
public class MovieImporterController {

    @Autowired
    private MovieImporterService movieImporterService;

    @ContentManager
    @PostMapping("/{movieExternalId}")
    public UUID getModerator(@PathVariable String movieExternalId)
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {
        return movieImporterService.importMovie(movieExternalId);
    }
}
