package com.solvve.controller;

import com.solvve.controller.security.ContentManager;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsReadDTO;
import com.solvve.service.MisprintInfoNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/misprint-info-news")
public class MisprintInfoNewsController {

    @Autowired
    private MisprintInfoNewsService misprintInfoService;

    @ContentManager
    @GetMapping("/{id}")
    public MisprintInfoNewsReadDTO getMisprintInfoNews(@PathVariable UUID id) {
        return misprintInfoService.getMisprintInfoNews(id);
    }

    @ContentManager
    @GetMapping("/need-to-fix")
    public List<MisprintInfoNewsReadDTO> getMisprintsInfoNews() {
        return misprintInfoService.getMisprintsInfoNewsNeedToFix();
    }

    @ContentManager
    @DeleteMapping("/{id}")
    public void deleteMisprintInfoNews(@PathVariable UUID id) {
        misprintInfoService.deleteMisprintInfoNews(id);
    }
}
