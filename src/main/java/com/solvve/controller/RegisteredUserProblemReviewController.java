package com.solvve.controller;

import com.solvve.controller.security.CurrentRegisteredUser;
import com.solvve.dto.problemreview.ProblemReviewCreateDTO;
import com.solvve.dto.problemreview.ProblemReviewPatchDTO;
import com.solvve.dto.problemreview.ProblemReviewPutDTO;
import com.solvve.dto.problemreview.ProblemReviewReadDTO;
import com.solvve.service.RegisteredUserProblemReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/registered-user/{registeredUserId}/problem-reviews")
public class RegisteredUserProblemReviewController  {

    @Autowired
    private RegisteredUserProblemReviewService registeredUserProblemReviewService;

    @CurrentRegisteredUser
    @GetMapping
    public List<ProblemReviewReadDTO> getRegisteredUserProblemReviews(@PathVariable UUID registeredUserId) {
        return registeredUserProblemReviewService.getRegisteredUserProblemReviews(registeredUserId);
    }

    @CurrentRegisteredUser
    @GetMapping("/{problemReviewId}")
    public ProblemReviewReadDTO getRegisteredUserProblemReview(@PathVariable UUID registeredUserId,
                                                               @PathVariable UUID problemReviewId) {
        return registeredUserProblemReviewService.getRegisteredUserProblemReview(registeredUserId, problemReviewId);
    }

    @CurrentRegisteredUser
    @PostMapping
    public ProblemReviewReadDTO createRegisteredUserProblemReview(@PathVariable UUID registeredUserId,
                                                    @RequestBody @Valid ProblemReviewCreateDTO createDTO) {
        return registeredUserProblemReviewService.createRegisteredUserProblemReview(registeredUserId, createDTO);
    }

    @CurrentRegisteredUser
    @PatchMapping("/{problemReviewId}")
    public ProblemReviewReadDTO patchRegisteredUserProblemReview(@PathVariable UUID registeredUserId,
                                                                 @PathVariable UUID problemReviewId,
                                                                 @RequestBody ProblemReviewPatchDTO patch) {
        return registeredUserProblemReviewService.patchRegisteredUserProblemReview(registeredUserId,
                problemReviewId, patch);
    }

    @CurrentRegisteredUser
    @PutMapping("/{problemReviewId}")
    public ProblemReviewReadDTO updateRegisteredUserProblemReview(@PathVariable UUID registeredUserId,
                                                         @PathVariable UUID problemReviewId,
                                                         @RequestBody ProblemReviewPutDTO put) {
        return registeredUserProblemReviewService.updateRegisteredUserProblemReview(registeredUserId,
                problemReviewId, put);
    }

    @CurrentRegisteredUser
    @DeleteMapping("/{problemReviewId}")
    public void deleteRegisteredUserProblemReview(@PathVariable UUID registeredUserId,
                                                  @PathVariable UUID problemReviewId) {
        registeredUserProblemReviewService.deleteRegisteredUserProblemReview(registeredUserId, problemReviewId);
    }
}

