package com.solvve.controller;

import com.solvve.controller.security.Admin;
import com.solvve.controller.security.Moderator;
import com.solvve.controller.security.RegisteredUser;
import com.solvve.dto.registereduser.RegisteredUserCreateDTO;
import com.solvve.dto.registereduser.RegisteredUserPatchDTO;
import com.solvve.dto.registereduser.RegisteredUserPutDTO;
import com.solvve.dto.registereduser.RegisteredUserReadDTO;
import com.solvve.service.RegisteredUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class RegisteredUserController {


    @Autowired
    private RegisteredUserService registeredUserService;

    @Admin
    @GetMapping("/registered-users/{id}")
    public RegisteredUserReadDTO getRegisteredUser(@PathVariable UUID id) {
        return registeredUserService.getRegisteredUser(id);
    }

    @PostMapping("/registered-users")
    public RegisteredUserReadDTO createRegisteredUser(@RequestBody @Valid RegisteredUserCreateDTO createDTO) {
        return registeredUserService.createRegisteredUser(createDTO);
    }

    @Admin
    @PatchMapping("/admin/registered-users/{registeredUserId}")
    public RegisteredUserReadDTO patchRegisteredUserByAdmin(@PathVariable UUID registeredUserId,
                                                            @RequestBody RegisteredUserPatchDTO patch) {
        return registeredUserService.patchRegisteredUser(registeredUserId, patch);
    }

    @Moderator
    @PatchMapping("/moderator/registered-users/{registeredUserId}")
    public RegisteredUserReadDTO patchRegisteredUserByModerator(@PathVariable UUID registeredUserId,
                                                                @RequestBody RegisteredUserPatchDTO patch) {
        return registeredUserService.patchRegisteredUser(registeredUserId, patch);
    }

    @RegisteredUser
    @PatchMapping("/registered-users/{registeredUserId}")
    public RegisteredUserReadDTO patchRegisteredUser(@PathVariable UUID registeredUserId,
                                                     @RequestBody RegisteredUserPatchDTO patch) {
        return registeredUserService.patchRegisteredUser(registeredUserId, patch);
    }

    @Moderator
    @PutMapping("/moderator/registered-users/{registeredUserId}")
    public RegisteredUserReadDTO updateRegisteredUserByModerator(@PathVariable UUID registeredUserId,
                                                                 @RequestBody RegisteredUserPutDTO put) {
        return registeredUserService.updateRegisteredUser(registeredUserId, put);
    }

    @RegisteredUser
    @PutMapping("/registered-users/{registeredUserId}")
    public RegisteredUserReadDTO updateRegisteredUser(@PathVariable UUID registeredUserId,
                                                      @RequestBody RegisteredUserPutDTO put) {
        return registeredUserService.updateRegisteredUser(registeredUserId, put);
    }

    @Admin
    @DeleteMapping("/registered-users/{id}")
    public void deleteRegisteredUser(@PathVariable UUID id) {
        registeredUserService.deleteRegisteredUser(id);
    }
}


