package com.solvve.controller;

import com.solvve.controller.security.CurrentRegisteredUser;
import com.solvve.controller.security.RegisteredUser;
import com.solvve.dto.newslikedislike.NewsLikeDislikeCreateDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikePatchDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikePutDTO;
import com.solvve.dto.newslikedislike.NewsLikeDislikeReadDTO;
import com.solvve.service.RegisteredUserNewsLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/registered-user/{registeredUserId}/news-like-dislike")
public class RegisteredUserNewsLikeController {

    @Autowired
    private RegisteredUserNewsLikeService registeredUserNewsLikeService;

    @CurrentRegisteredUser
    @GetMapping
    public List<NewsLikeDislikeReadDTO> getRegisteredUserNews(@PathVariable UUID registeredUserId) {
        return registeredUserNewsLikeService.getRegisteredUserNewsLikes(registeredUserId);
    }

    @RegisteredUser
    @GetMapping("/{newsLikeDislikeId}")
    public NewsLikeDislikeReadDTO getRegisteredUserNews(@PathVariable UUID registeredUserId,
                                                        @PathVariable UUID newsLikeDislikeId) {
        return registeredUserNewsLikeService.getRegisteredUserNewsLike(registeredUserId, newsLikeDislikeId);
    }

    @RegisteredUser
    @PostMapping
    public NewsLikeDislikeReadDTO createNewsLikeDislike(@PathVariable UUID registeredUserId,
                                                        @RequestBody @Valid NewsLikeDislikeCreateDTO createDTO) {
        return registeredUserNewsLikeService.createRegisteredUserNewsLike(registeredUserId, createDTO);
    }

    @CurrentRegisteredUser
    @PatchMapping("/{newsLikeDislikeId}")
    public NewsLikeDislikeReadDTO patchRegisteredUserNews(@PathVariable UUID registeredUserId,
                                                          @PathVariable UUID newsLikeDislikeId,
                                                          @RequestBody NewsLikeDislikePatchDTO patch) {
        return registeredUserNewsLikeService.patchRegisteredUserNewsLike(registeredUserId, newsLikeDislikeId, patch);
    }

    @RegisteredUser
    @PutMapping("/{newsLikeDislikeId}")
    public NewsLikeDislikeReadDTO updateRegisteredUserNews(@PathVariable UUID registeredUserId,
                                                           @PathVariable UUID newsLikeDislikeId,
                                                           @RequestBody NewsLikeDislikePutDTO put) {
        return registeredUserNewsLikeService.updateRegisteredUserNewsLike(registeredUserId, newsLikeDislikeId, put);
    }

    @CurrentRegisteredUser
    @DeleteMapping("/{newsLikeDislikeId}")
    public void deleteRegisteredUserNews(@PathVariable UUID registeredUserId, @PathVariable UUID newsLikeDislikeId) {
        registeredUserNewsLikeService.deleteRegisteredUserNewsLike(registeredUserId, newsLikeDislikeId);
    }
}

