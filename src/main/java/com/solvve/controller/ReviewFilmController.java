package com.solvve.controller;

import com.solvve.controller.documentation.ApiPageable;
import com.solvve.controller.security.Moderator;
import com.solvve.domain.PageResult;
import com.solvve.dto.reviewfilm.ReviewFilmFilter;
import com.solvve.dto.reviewfilm.ReviewFilmPatchDTO;
import com.solvve.dto.reviewfilm.ReviewFilmPutDTO;
import com.solvve.dto.reviewfilm.ReviewFilmReadDTO;
import com.solvve.service.ReviewFilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class ReviewFilmController {

    @Autowired
    private ReviewFilmService reviewFilmService;

    @Moderator
    @GetMapping("/review-films/{id}")
    public ReviewFilmReadDTO getReviewFilm(@PathVariable UUID id) {
        return reviewFilmService.getReviewFilm(id);
    }

    @Moderator
    @ApiPageable
    @GetMapping("/review-films")
    public PageResult<ReviewFilmReadDTO> getReviewFilms(ReviewFilmFilter filter,
                                                        @ApiIgnore Pageable pageable) {
        return reviewFilmService.getReviewFilms(filter, pageable);
    }

    @Moderator
    @PatchMapping("/review-films/{id}")
    public ReviewFilmReadDTO patchReviewFilm(@PathVariable UUID id, @RequestBody ReviewFilmPatchDTO patch) {
        return reviewFilmService.patchReviewFilm(id, patch);
    }

    @Moderator
    @PutMapping("/review-films/{id}")
    public ReviewFilmReadDTO updateReviewFilm(@PathVariable UUID id, @RequestBody ReviewFilmPutDTO put) {
        return reviewFilmService.updateReviewFilm(id, put);
    }

    @Moderator
    @DeleteMapping("/review-films/{id}")
    public void deleteReviewFilm(@PathVariable UUID id) {
        reviewFilmService.deleteReviewFilm(id);
    }
}

