package com.solvve.controller;

import com.solvve.controller.security.ContentManager;
import com.solvve.dto.misprintinfofix.MisprintInfoFixDTO;
import com.solvve.dto.misprintinfofix.MisprintInfoFixReadDTO;
import com.solvve.service.MisprintInfoFixService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/content-manager/{contentManagerId}/misprints-info")
public class MisprintInfoFixController {

    @Autowired
    private MisprintInfoFixService mpFixService;

    @ContentManager
    @PostMapping("/{misprintInfoId}/fix")
    public List<MisprintInfoFixReadDTO> fixMisprintInfoNews(@PathVariable UUID contentManagerId,
                                                            @PathVariable UUID misprintInfoId,
                                                            @RequestBody MisprintInfoFixDTO post) {
        return mpFixService.fixContent(contentManagerId, misprintInfoId, post);
    }
}
