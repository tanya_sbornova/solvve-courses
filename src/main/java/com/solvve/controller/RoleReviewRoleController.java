package com.solvve.controller;

import com.solvve.controller.security.CurrentRegisteredUser;
import com.solvve.controller.security.ModeratorOrRegisteredUser;
import com.solvve.controller.security.RegisteredUser;
import com.solvve.dto.reviewrole.ReviewRoleCreateDTO;
import com.solvve.dto.reviewrole.ReviewRolePatchDTO;
import com.solvve.dto.reviewrole.ReviewRolePutDTO;
import com.solvve.dto.reviewrole.ReviewRoleReadDTO;
import com.solvve.service.RoleReviewRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class RoleReviewRoleController {

    @Autowired
    private RoleReviewRoleService roleReviewRoleService;

    @ModeratorOrRegisteredUser
    @GetMapping("/role/{roleId}/review-roles")
    public List<ReviewRoleReadDTO> getRoleReviewRoles(@PathVariable UUID roleId) {
        return roleReviewRoleService.getRoleReviewRoles(roleId);
    }

    @CurrentRegisteredUser
    @GetMapping("/registered-user/{registeredUserId}/role/{roleId}/review-roles")
    public List<ReviewRoleReadDTO> getRoleReviewsRoleOfCurrentRegisteredUser(@PathVariable UUID registeredUserId,
                                                                             @PathVariable UUID roleId) {
        return roleReviewRoleService.getRoleReviewsRoleOfCurrentRegisteredUser(registeredUserId, roleId);
    }

    @ModeratorOrRegisteredUser
    @GetMapping("/role/{roleId}/review-roles/{reviewRoleId}")
    public ReviewRoleReadDTO getRoleReviewRole(@PathVariable UUID roleId,
                                               @PathVariable UUID reviewRoleId) {
        return roleReviewRoleService.getRoleReviewRole(roleId, reviewRoleId);
    }

    @RegisteredUser
    @PostMapping("/role/{roleId}/review-roles")
    public ReviewRoleReadDTO createReviewRole(@PathVariable UUID roleId,
                                              @RequestBody @Valid ReviewRoleCreateDTO createDTO) {
        return roleReviewRoleService.createRoleReviewRole(roleId, createDTO);
    }

    @ModeratorOrRegisteredUser
    @PatchMapping("/role/{roleId}/review-roles/{reviewRoleId}")
    public ReviewRoleReadDTO patchRoleReviewRole(@PathVariable UUID roleId,
                                                 @PathVariable UUID reviewRoleId,
                                                 @RequestBody ReviewRolePatchDTO patch) {
        return roleReviewRoleService.patchRoleReviewRole(roleId, reviewRoleId, patch);
    }

    @ModeratorOrRegisteredUser
    @PutMapping("/role/{roleId}/review-roles/{reviewRoleId}")
    public ReviewRoleReadDTO updateRoleReviewRole(@PathVariable UUID roleId,
                                                  @PathVariable UUID reviewRoleId,
                                                  @RequestBody ReviewRolePutDTO put) {
        return roleReviewRoleService.updateRoleReviewRole(roleId, reviewRoleId, put);
    }

    @ModeratorOrRegisteredUser
    @DeleteMapping("/role/{roleId}/review-roles/{reviewRoleId}")
    public void deleteRoleRatingRole(@PathVariable UUID roleId, @PathVariable UUID reviewRoleId) {
        roleReviewRoleService.deleteRoleReviewRole(roleId, reviewRoleId);
    }
}
