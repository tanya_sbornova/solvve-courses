package com.solvve.controller;

import com.solvve.controller.security.ContentManager;
import com.solvve.dto.misprintinfofilm.MisprintInfoFilmReadDTO;
import com.solvve.service.MisprintInfoFilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/misprint-info-films")
public class MisprintInfoFilmController {

    @Autowired
    private MisprintInfoFilmService misprintInfoService;

    @ContentManager
    @GetMapping("/{id}")
    public MisprintInfoFilmReadDTO getMisprintInfoFilm(@PathVariable UUID id) {
        return misprintInfoService.getMisprintInfoFilm(id);
    }

    @ContentManager
    @DeleteMapping("/{id}")
    public void deleteMisprintInfoFilm(@PathVariable UUID id) {
        misprintInfoService.deleteMisprintInfoFilm(id);
    }
}

