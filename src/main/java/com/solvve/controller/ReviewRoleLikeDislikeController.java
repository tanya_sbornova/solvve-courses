package com.solvve.controller;

import com.solvve.controller.security.ContentManagerOrRegisteredUser;
import com.solvve.controller.security.RegisteredUser;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePatchDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePutDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikeReadDTO;
import com.solvve.service.ReviewRoleLikeDislikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/review-role-like-dislikes")
public class ReviewRoleLikeDislikeController {

    @Autowired
    private ReviewRoleLikeDislikeService reviewRoleLikeDislikeService;

    @ContentManagerOrRegisteredUser
    @GetMapping("/{id}")
    public ReviewRoleLikeDislikeReadDTO getReviewRoleLikeDislike(@PathVariable UUID id) {
        return reviewRoleLikeDislikeService.getReviewRoleLikeDislike(id);
    }

    @RegisteredUser
    @PatchMapping("/{id}")
    public ReviewRoleLikeDislikeReadDTO patchReviewRoleLikeDislike(@PathVariable UUID id,
                                                             @RequestBody ReviewRoleLikeDislikePatchDTO patch) {
        return reviewRoleLikeDislikeService.patchReviewRoleLikeDislike(id, patch);
    }

    @RegisteredUser
    @PutMapping("/{id}")
    public ReviewRoleLikeDislikeReadDTO updateReviewRoleLikeDislike(@PathVariable UUID id,
                                                                 @RequestBody ReviewRoleLikeDislikePutDTO put) {
        return reviewRoleLikeDislikeService.updateReviewRoleLikeDislike(id, put);
    }

    @RegisteredUser
    @DeleteMapping("/{id}")
    public void deleteReviewRoleLikeDislike(@PathVariable UUID id) {
        reviewRoleLikeDislikeService.deleteReviewRoleLikeDislike(id);
    }

}

