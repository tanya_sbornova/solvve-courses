package com.solvve.controller;

import com.solvve.controller.security.ContentManagerOrRegisteredUser;
import com.solvve.controller.security.RegisteredUser;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePatchDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikePutDTO;
import com.solvve.dto.reviewfilmlikedislike.ReviewFilmLikeDislikeReadDTO;
import com.solvve.service.ReviewFilmLikeDislikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/review-film-like-dislikes")
public class ReviewFilmLikeDislikeController {

    @Autowired
    private ReviewFilmLikeDislikeService reviewFilmLikeDislikeService;

    @ContentManagerOrRegisteredUser
    @GetMapping("/{id}")
    public ReviewFilmLikeDislikeReadDTO getReviewFilmLikeDislike(@PathVariable UUID id) {
        return reviewFilmLikeDislikeService.getReviewFilmLikeDislike(id);
    }

    @RegisteredUser
    @PatchMapping("/{id}")
    public ReviewFilmLikeDislikeReadDTO patchReviewFilmLikeDislike(@PathVariable UUID id,
                                                         @RequestBody ReviewFilmLikeDislikePatchDTO patch) {
        return reviewFilmLikeDislikeService.patchReviewFilmLikeDislike(id, patch);
    }

    @RegisteredUser
    @PutMapping("/{id}")
    public ReviewFilmLikeDislikeReadDTO updateReviewFilmLikeDislike(@PathVariable UUID id,
                                                        @RequestBody ReviewFilmLikeDislikePutDTO put) {
        return reviewFilmLikeDislikeService.updateReviewFilmLikeDislike(id, put);
    }

    @RegisteredUser
    @DeleteMapping("/{id}")
    public void deleteReviewFilmLikeDislike(@PathVariable UUID id) {
        reviewFilmLikeDislikeService.deleteReviewFilmLikeDislike(id);
    }

}

