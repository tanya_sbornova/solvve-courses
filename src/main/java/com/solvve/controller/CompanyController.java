package com.solvve.controller;

import com.solvve.controller.security.ContentManager;
import com.solvve.controller.security.ContentManagerOrRegisteredUser;
import com.solvve.dto.company.*;
import com.solvve.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @ContentManagerOrRegisteredUser
    @GetMapping("/companies/{id}")
    public CompanyReadDTO getCompany(@PathVariable UUID id) {
        return companyService.getCompany(id);
    }

    @ContentManagerOrRegisteredUser
    @GetMapping("/companies")
    public List<CompanyReadDTO> getCompanies() {
        return companyService.getCompanies();
    }

    @ContentManager
    @PostMapping("/companies")
    public CompanyReadDTO createCompany(@RequestBody @Valid CompanyCreateDTO createDTO) {
        return companyService.createCompany(createDTO);
    }

    @ContentManager
    @PostMapping("/film/{filmId}/companies/{id}")
    public List<CompanyReadDTO> addCompanyToFilm(@PathVariable UUID filmId, @PathVariable UUID id) {
        return companyService.addCompanyToFilm(filmId, id);
    }

    @ContentManager
    @DeleteMapping("/companies/{id}")
    public void deleteCompany(@PathVariable UUID id) {
        companyService.deleteCompany(id);
    }

    @ContentManager
    @DeleteMapping("/film/{filmId}/companies/{id}")
    public List<CompanyReadDTO> removeCompanyFromFilm(@PathVariable UUID filmId, @PathVariable UUID id) {
        return companyService.removeCompanyFromFilm(filmId, id);
    }
}

