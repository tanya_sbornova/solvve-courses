package com.solvve.controller;

import com.solvve.controller.security.ContentManager;
import com.solvve.controller.security.ContentManagerOrRegisteredUser;
import com.solvve.dto.actor.*;
import com.solvve.service.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class ActorController {

    @Autowired
    private ActorService actorService;

    @ContentManagerOrRegisteredUser
    @GetMapping("/actors/{id}")
    public ActorReadDTO getActor(@PathVariable UUID id) {
        return actorService.getActor(id);
    }

    @GetMapping("/unregistered-user/actors/{id}/extended")
    public ActorReadExtendedDTO getActorExtendedForUnregisteredUser(@PathVariable UUID id) {
        return actorService.getActorExtended(id);
    }

    @ContentManagerOrRegisteredUser
    @GetMapping("/actors/{id}/extended")
    public ActorReadExtendedDTO getActorExtended(@PathVariable UUID id) {
        return actorService.getActorExtended(id);
    }

    @ContentManagerOrRegisteredUser
    @GetMapping("/actors/leader-board")
    public List<ActorInLeaderBoardReadDTO> getFilmsLeaderBoard() {
        return actorService.getActorsLeaderBoard();
    }

    @ContentManagerOrRegisteredUser
    @GetMapping("/person/{personId}/actors/{actorId}")
    public ActorReadDTO getPersonActor(@PathVariable UUID personId,
                                       @PathVariable UUID actorId) throws Throwable {
        return actorService.getPersonActor(personId, actorId);
    }

    @ContentManager
    @PostMapping("/actors")
    public ActorReadDTO createActor(@RequestBody @Valid ActorCreateDTO createDTO) {
        return actorService.createActor(createDTO);
    }

    @ContentManager
    @PostMapping("/person/{personId}/actors")
    public ActorReadDTO createPersonActor(@PathVariable UUID personId,
                                          @RequestBody ActorCreateDTO createDTO) {
        return actorService.createPersonActor(personId, createDTO);
    }

    @ContentManager
    @DeleteMapping("/actors/{id}")
    public void deleteActor(@PathVariable UUID id) {
        actorService.deleteActor(id);
    }
}
