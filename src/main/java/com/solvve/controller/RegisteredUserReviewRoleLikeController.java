package com.solvve.controller;

import com.solvve.controller.security.CurrentRegisteredUser;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikeCreateDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePatchDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikePutDTO;
import com.solvve.dto.reviewrolelikedislike.ReviewRoleLikeDislikeReadDTO;
import com.solvve.service.RegisteredUserReviewRoleLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/registered-user/{registeredUserId}/review-role-like-dislike")
public class RegisteredUserReviewRoleLikeController {

    @Autowired
    private RegisteredUserReviewRoleLikeService registeredUserReviewRoleLikeService;

    @CurrentRegisteredUser
    @GetMapping
    public List<ReviewRoleLikeDislikeReadDTO> getRegisteredUserReviewRoleLikes(@PathVariable UUID registeredUserId) {
        return registeredUserReviewRoleLikeService.getRegisteredUserReviewRoleLikes(registeredUserId);
    }

    @CurrentRegisteredUser
    @GetMapping("/{reviewRoleLikeDislikeId}")
    public ReviewRoleLikeDislikeReadDTO getRegisteredUserReviewRoleLike(@PathVariable UUID registeredUserId,
                                                                        @PathVariable UUID reviewRoleLikeDislikeId) {
        return registeredUserReviewRoleLikeService.getRegisteredUserReviewRoleLike(registeredUserId,
                reviewRoleLikeDislikeId);
    }

    @CurrentRegisteredUser
    @PostMapping
    public ReviewRoleLikeDislikeReadDTO createRegisteredUserReviewRoleLike(@PathVariable UUID registeredUserId,
                                                                    @RequestBody @Valid ReviewRoleLikeDislikeCreateDTO
                                                                    createDTO) {
        return registeredUserReviewRoleLikeService.createRegisteredUserReviewRoleLike(registeredUserId, createDTO);
    }

    @CurrentRegisteredUser
    @PatchMapping("/{reviewRoleLikeDislikeId}")
    public ReviewRoleLikeDislikeReadDTO patchRegisteredUserReviewRoleLike(@PathVariable UUID registeredUserId,
                                                                @PathVariable UUID reviewRoleLikeDislikeId,
                                                                @RequestBody ReviewRoleLikeDislikePatchDTO patch) {
        return registeredUserReviewRoleLikeService.patchRegisteredUserReviewRoleLike(registeredUserId,
                reviewRoleLikeDislikeId, patch);
    }

    @CurrentRegisteredUser
    @PutMapping("/{reviewRoleLikeDislikeId}")
    public ReviewRoleLikeDislikeReadDTO updateRegisteredUserReviewRoleLike(@PathVariable UUID registeredUserId,
                                                                 @PathVariable UUID reviewRoleLikeDislikeId,
                                                                 @RequestBody ReviewRoleLikeDislikePutDTO put) {
        return registeredUserReviewRoleLikeService.updateRegisteredUserReviewRoleLike(registeredUserId,
                reviewRoleLikeDislikeId, put);
    }

    @CurrentRegisteredUser
    @DeleteMapping("/{reviewRoleLikeDislikeId}")
    public void deleteRegisteredUserReviewRoleLike(@PathVariable UUID registeredUserId,
                                         @PathVariable UUID reviewRoleLikeDislikeId) {
        registeredUserReviewRoleLikeService.deleteRegisteredUserReviewRoleLike(registeredUserId,
                reviewRoleLikeDislikeId);
    }
}
