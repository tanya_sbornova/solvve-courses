package com.solvve.controller;

import com.solvve.controller.security.RegisteredUser;
import com.solvve.dto.ratingfilm.RatingFilmPatchDTO;
import com.solvve.dto.ratingfilm.RatingFilmPutDTO;
import com.solvve.dto.ratingfilm.RatingFilmReadDTO;
import com.solvve.service.RatingFilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/rating-films")
public class RatingFilmController {

    @Autowired
    private RatingFilmService ratingFilmService;

    @RegisteredUser
    @GetMapping("/{id}")
    public RatingFilmReadDTO getRatingFilm(@PathVariable UUID id) {
        return ratingFilmService.getRatingFilm(id);
    }

    @RegisteredUser
    @PatchMapping("/{id}")
    public RatingFilmReadDTO patchRatingFilm(@PathVariable UUID id,
                                             @RequestBody RatingFilmPatchDTO patch) {
        return ratingFilmService.patchRatingFilm(id, patch);
    }

    @RegisteredUser
    @PutMapping("/{id}")
    public RatingFilmReadDTO updateRatingFilm(@PathVariable UUID id, @RequestBody RatingFilmPutDTO put) {
        return ratingFilmService.updateRatingFilm(id, put);
    }

    @RegisteredUser
    @DeleteMapping("/{id}")
    public void deleteRatingFilm(@PathVariable UUID id) {
        ratingFilmService.deleteRatingFilm(id);
    }
}

