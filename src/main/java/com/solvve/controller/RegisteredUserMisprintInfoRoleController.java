package com.solvve.controller;

import com.solvve.dto.misprintinforole.MisprintInfoRoleCreateDTO;
import com.solvve.dto.misprintinforole.MisprintInfoRoleReadDTO;
import com.solvve.service.RegisteredUserMisprintInfoRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/registered-user/{registeredUserId}/misprint-info-roles")
public class RegisteredUserMisprintInfoRoleController  {

    @Autowired
    private RegisteredUserMisprintInfoRoleService registeredUserMisprintInfoRoleService;

    @GetMapping
    public List<MisprintInfoRoleReadDTO> getRegisteredUserMisprintInfoRoles(@PathVariable UUID registeredUserId) {
        return registeredUserMisprintInfoRoleService.getRegisteredUserMisprintInfoRoles(registeredUserId);
    }

    @GetMapping("/{misprintInfoRoleId}")
    public MisprintInfoRoleReadDTO getRegisteredUserMisprintInfoRole(@PathVariable UUID registeredUserId,
                                                                     @PathVariable UUID misprintInfoRoleId) {
        return registeredUserMisprintInfoRoleService.getRegisteredUserMisprintInfoRole(registeredUserId,
                misprintInfoRoleId);
    }

    @PostMapping
    public MisprintInfoRoleReadDTO createMisprintInfoRole(@PathVariable UUID registeredUserId,
                                                          @RequestBody MisprintInfoRoleCreateDTO createDTO) {
        return registeredUserMisprintInfoRoleService.createRegisteredUserMisprintInfoRole(registeredUserId,
                createDTO);
    }

    @DeleteMapping("/{misprintInfoRoleId}")
    public void deleteRegisteredUserMisprintInfoRole(@PathVariable UUID registeredUserId,
                                                     @PathVariable UUID misprintInfoRoleId) {
        registeredUserMisprintInfoRoleService.deleteRegisteredUserMisprintInfoRole(registeredUserId,
                misprintInfoRoleId);
    }
}