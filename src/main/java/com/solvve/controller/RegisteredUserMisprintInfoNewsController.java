package com.solvve.controller;

import com.solvve.controller.security.CurrentRegisteredUser;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsCreateDTO;
import com.solvve.dto.misprintinfonews.MisprintInfoNewsReadDTO;
import com.solvve.service.RegisteredUserMisprintInfoNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/registered-user/{registeredUserId}/misprint-info-news")
public class RegisteredUserMisprintInfoNewsController  {

    @Autowired
    private RegisteredUserMisprintInfoNewsService registeredUserMisprintInfoNewsService;

    @CurrentRegisteredUser
    @GetMapping
    public List<MisprintInfoNewsReadDTO> getRegisteredUserMisprintInfoNews(@PathVariable UUID registeredUserId) {
        return registeredUserMisprintInfoNewsService.getRegisteredUserMisprintInfoNews(registeredUserId);
    }

    @CurrentRegisteredUser
    @GetMapping("/{misprintInfoNewsId}")
    public MisprintInfoNewsReadDTO getRegisteredUserMisprintInfoNews(@PathVariable UUID registeredUserId,
                                                                     @PathVariable UUID misprintInfoNewsId) {
        return registeredUserMisprintInfoNewsService.getRegisteredUserMisprintInfoNews(registeredUserId,
                misprintInfoNewsId);
    }

    @CurrentRegisteredUser
    @PostMapping
    public MisprintInfoNewsReadDTO createMisprintInfoNews(@PathVariable UUID registeredUserId,
                                                          @RequestBody MisprintInfoNewsCreateDTO createDTO) {
        return registeredUserMisprintInfoNewsService.createRegisteredUserMisprintInfoNews(registeredUserId,
                createDTO);
    }

    @CurrentRegisteredUser
    @DeleteMapping("/{misprintInfoNewsId}")
    public void deleteRegisteredUserNews(@PathVariable UUID registeredUserId, @PathVariable UUID misprintInfoNewsId) {
        registeredUserMisprintInfoNewsService.deleteRegisteredUserMisprintInfoNews(registeredUserId,
                misprintInfoNewsId);
    }
}
