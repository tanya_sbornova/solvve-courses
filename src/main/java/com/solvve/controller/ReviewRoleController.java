package com.solvve.controller;

import com.solvve.controller.documentation.ApiPageable;
import com.solvve.controller.security.Moderator;
import com.solvve.controller.security.ModeratorOrRegisteredUser;
import com.solvve.domain.PageResult;
import com.solvve.dto.reviewrole.ReviewRoleFilter;
import com.solvve.dto.reviewrole.ReviewRolePatchDTO;
import com.solvve.dto.reviewrole.ReviewRolePutDTO;
import com.solvve.dto.reviewrole.ReviewRoleReadDTO;
import com.solvve.service.ReviewRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class ReviewRoleController {

    @Autowired
    private ReviewRoleService reviewRoleService;

    @ModeratorOrRegisteredUser
    @GetMapping("/review-roles/{id}")
    public ReviewRoleReadDTO getReviewRole(@PathVariable UUID id) {
        return reviewRoleService.getReviewRole(id);
    }

    @Moderator
    @ApiPageable
    @GetMapping("/review-roles")
    public PageResult<ReviewRoleReadDTO> getReviewRoles(ReviewRoleFilter filter,
                                                        @ApiIgnore Pageable pageable) {
        return reviewRoleService.getReviewRoles(filter, pageable);
    }

    @Moderator
    @PatchMapping("/review-roles/{id}")
    public ReviewRoleReadDTO patchReviewRole(@PathVariable UUID id, @RequestBody ReviewRolePatchDTO patch) {
        return reviewRoleService.patchReviewRole(id, patch);
    }

    @Moderator
    @PutMapping("/review-roles/{id}")
    public ReviewRoleReadDTO updateReviewRole(@PathVariable UUID id, @RequestBody ReviewRolePutDTO put) {
        return reviewRoleService.updateReviewRole(id, put);
    }

    @Moderator
    @DeleteMapping("/review-roles/{id}")
    public void deleteReviewRole(@PathVariable UUID id) {
        reviewRoleService.deleteReviewRole(id);
    }
}
