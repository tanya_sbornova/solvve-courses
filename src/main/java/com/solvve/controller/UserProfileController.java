package com.solvve.controller;

import com.solvve.controller.security.Moderator;
import com.solvve.controller.security.ModeratorOrRegisteredUser;
import com.solvve.dto.userprofile.UserProfileCreateDTO;
import com.solvve.dto.userprofile.UserProfilePatchDTO;
import com.solvve.dto.userprofile.UserProfilePutDTO;
import com.solvve.dto.userprofile.UserProfileReadDTO;
import com.solvve.service.UserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/user-profiles")
public class UserProfileController {

    @Autowired
    private UserProfileService userProfileService;

    @ModeratorOrRegisteredUser
    @GetMapping("/{id}")
    public UserProfileReadDTO getUserProfile(@PathVariable UUID id) {
        return userProfileService.getUserProfile(id);
    }

    @Moderator
    @PostMapping
    public UserProfileReadDTO createUserProfile(@RequestBody UserProfileCreateDTO createDTO) {
        return userProfileService.createUserProfile(createDTO);
    }

    @ModeratorOrRegisteredUser
    @PatchMapping("/{id}")
    public UserProfileReadDTO patchUserProfile(@PathVariable UUID id, @RequestBody UserProfilePatchDTO patch) {
        return userProfileService.patchUserProfile(id, patch);
    }

    @ModeratorOrRegisteredUser
    @PutMapping("/{id}")
    public UserProfileReadDTO updateUserProfile(@PathVariable UUID id, @RequestBody UserProfilePutDTO put) {
        return userProfileService.updateUserProfile(id, put);
    }

    @Moderator
    @DeleteMapping("/{id}")
    public void deleteUserProfile(@PathVariable UUID id) {
        userProfileService.deleteUserProfile(id);
    }
}

