package com.solvve.controller;

import com.solvve.controller.security.ModeratorOrRegisteredUser;
import com.solvve.controller.security.RegisteredUser;
import com.solvve.dto.ratingrole.RatingRoleCreateDTO;
import com.solvve.dto.ratingrole.RatingRolePatchDTO;
import com.solvve.dto.ratingrole.RatingRolePutDTO;
import com.solvve.dto.ratingrole.RatingRoleReadDTO;
import com.solvve.service.RoleRatingRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/role/{roleId}/rating-roles")
public class RoleRatingRoleController {

    @Autowired
    private RoleRatingRoleService roleRatingRoleService;

    @ModeratorOrRegisteredUser
    @GetMapping
    public List<RatingRoleReadDTO> getRoleRatingRoles(@PathVariable UUID roleId) {
        return roleRatingRoleService.getRoleRatingRoles(roleId);
    }

    @ModeratorOrRegisteredUser
    @GetMapping("/{ratingRoleId}")
    public RatingRoleReadDTO getRoleRatingRole(@PathVariable UUID roleId,
                                               @PathVariable UUID ratingRoleId) {
        return roleRatingRoleService.getRoleRatingRole(roleId, ratingRoleId);
    }

    @RegisteredUser
    @PostMapping
    public RatingRoleReadDTO createRoleRatingRole(@PathVariable UUID roleId,
                                                  @RequestBody @Valid RatingRoleCreateDTO createDTO) {
        return roleRatingRoleService.createRoleRatingRole(roleId, createDTO);
    }

    @RegisteredUser
    @PatchMapping("/{ratingRoleId}")
    public RatingRoleReadDTO patchRoleRatingRole(@PathVariable UUID roleId,
                                                 @PathVariable UUID ratingRoleId,
                                                 @RequestBody RatingRolePatchDTO patch) {
        return roleRatingRoleService.patchRoleRatingRole(roleId, ratingRoleId, patch);
    }

    @RegisteredUser
    @PutMapping("/{ratingRoleId}")
    public RatingRoleReadDTO updateRoleRatingRole(@PathVariable UUID roleId,
                                                  @PathVariable UUID ratingRoleId,
                                                  @RequestBody RatingRolePutDTO put) {
        return roleRatingRoleService.updateRoleRatingRole(roleId, ratingRoleId, put);
    }

    @RegisteredUser
    @DeleteMapping("/{ratingRoleId}")
    public void deleteRoleRatingRole(@PathVariable UUID roleId, @PathVariable UUID ratingRoleId) {
        roleRatingRoleService.deleteRoleRatingRole(roleId, ratingRoleId);
    }
}
