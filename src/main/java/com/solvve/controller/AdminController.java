package com.solvve.controller;

import com.solvve.controller.security.Admin;
import com.solvve.controller.validation.ControllerValidationUtil;
import com.solvve.dto.admin.AdminCreateDTO;
import com.solvve.dto.admin.AdminPatchDTO;
import com.solvve.dto.admin.AdminPutDTO;
import com.solvve.dto.admin.AdminReadDTO;
import com.solvve.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/admins")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @GetMapping("/{id}")
    public AdminReadDTO getAdmin(@PathVariable UUID id) {
        return adminService.getAdmin(id);
    }

    @Admin
    @PostMapping
    public AdminReadDTO createAdmin(@RequestBody @Valid AdminCreateDTO createDTO) {
        ControllerValidationUtil.validateLessThan(createDTO.getStartAt(), createDTO.getFinishAt(),
                "startAt", "finishAt");
        return adminService.createAdmin(createDTO);
    }

    @Admin
    @PatchMapping("/{id}")
    public AdminReadDTO patchAdmin(@PathVariable UUID id, @RequestBody AdminPatchDTO patch) {
        return adminService.patchAdmin(id, patch);
    }

    @Admin
    @PutMapping("/{id}")
    public AdminReadDTO updateAdmin(@PathVariable UUID id, @RequestBody AdminPutDTO put) {
        return adminService.updateAdmin(id, put);
    }

    @Admin
    @DeleteMapping("/{id}")
    public void deleteAdmin(@PathVariable UUID id) {
        adminService.deleteAdmin(id);
    }
}
