package com.solvve.controller;

import com.solvve.controller.documentation.ApiPageable;
import com.solvve.controller.security.Admin;
import com.solvve.domain.PageResult;
import com.solvve.dto.moderator.*;
import com.solvve.service.ModeratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/moderators")
public class ModeratorController {


    @Autowired
    private ModeratorService moderatorService;

    @Admin
    @GetMapping("/{id}")
    public ModeratorReadDTO getModerator(@PathVariable UUID id) {
        return moderatorService.getModerator(id);
    }

    @Admin
    @ApiPageable
    @GetMapping
    public PageResult<ModeratorReadDTO> getModerators(ModeratorFilter filter, @ApiIgnore Pageable pageable) {
        return moderatorService.getModerators(filter, pageable);
    }

    @PostMapping
    public ModeratorReadDTO createModerator(@RequestBody @Valid ModeratorCreateDTO createDTO) {
        return moderatorService.createModerator(createDTO);
    }

    @Admin
    @PatchMapping("/{id}")
    public ModeratorReadDTO patchCrewMember(@PathVariable UUID id, @RequestBody ModeratorPatchDTO patch) {
        return moderatorService.patchModerator(id, patch);
    }

    @Admin
    @PutMapping("/{id}")
    public ModeratorReadDTO updateCrewMember(@PathVariable UUID id, @RequestBody ModeratorPutDTO put) {
        return moderatorService.updateModerator(id, put);
    }

    @Admin
    @DeleteMapping("/{id}")
    public void deleteModerator(@PathVariable UUID id) {
        moderatorService.deleteModerator(id);
    }
}
