package com.solvve.controller;

import com.solvve.controller.security.RegisteredUser;
import com.solvve.dto.ratingrole.RatingRolePatchDTO;
import com.solvve.dto.ratingrole.RatingRolePutDTO;
import com.solvve.dto.ratingrole.RatingRoleReadDTO;
import com.solvve.service.RatingRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/rating-roles")
public class RatingRoleController {

    @Autowired
    private RatingRoleService ratingRoleService;

    @RegisteredUser
    @GetMapping("/{id}")
    public RatingRoleReadDTO getRatingRole(@PathVariable UUID id) {
        return ratingRoleService.getRatingRole(id);
    }

    @RegisteredUser
    @PatchMapping("/{id}")
    public RatingRoleReadDTO patchRatingRole(@PathVariable UUID id, @RequestBody RatingRolePatchDTO patch) {
        return ratingRoleService.patchRatingRole(id, patch);
    }

    @RegisteredUser
    @PutMapping("/{id}")
    public RatingRoleReadDTO updateRatingRole(@PathVariable UUID id, @RequestBody RatingRolePutDTO put) {
        return ratingRoleService.updateRatingRole(id, put);
    }

    @RegisteredUser
    @DeleteMapping("/{id}")
    public void deleteRatingRole(@PathVariable UUID id) {
        ratingRoleService.deleteRatingRole(id);
    }
}
