package com.solvve.controller;

import com.solvve.controller.documentation.ApiPageable;
import com.solvve.controller.security.ContentManager;
import com.solvve.controller.security.ContentManagerOrRegisteredUser;
import com.solvve.domain.PageResult;
import com.solvve.dto.film.*;
import com.solvve.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class FilmController {

    @Autowired
    private FilmService filmService;

    @ContentManagerOrRegisteredUser
    @GetMapping("/films/{id}")
    public FilmReadDTO getFilm(@PathVariable UUID id) {
        return filmService.getFilm(id);
    }

    @ContentManagerOrRegisteredUser
    @ApiPageable
    @GetMapping("/films")
    public PageResult<FilmReadDTO> getFilms(FilmFilter filter, @ApiIgnore Pageable pageable) {
        return filmService.getFilms(filter, pageable);
    }

    @GetMapping("/unregistered-user/films/{id}/extended")
    public FilmReadExtendedDTO getFilmExtendedForUnregisteredUser(@PathVariable UUID id) {
        return filmService.getFilmExtended(id);
    }

    @ContentManagerOrRegisteredUser
    @GetMapping("/films/{id}/extended")
    public FilmReadExtendedDTO getFilmExtended(@PathVariable UUID id) {
        return filmService.getFilmExtended(id);
    }

    @ContentManagerOrRegisteredUser
    @GetMapping("/films/leader-board")
    public List<FilmInLeaderBoardReadDTO> getFilmsLeaderBoard() {
        return filmService.getFilmsLeaderBoard();
    }

    @ContentManager
    @PostMapping("/films")
    public FilmReadDTO createFilm(@RequestBody FilmCreateDTO createDTO) {
        return filmService.createFilm(createDTO);
    }

    @ContentManager
    @PatchMapping("/films/{id}")
    public FilmReadDTO patchFilm(@PathVariable UUID id, @RequestBody FilmPatchDTO patch) {
        return filmService.patchFilm(id, patch);
    }

    @ContentManager
    @PutMapping("/films/{id}")
    public FilmReadDTO updateFilm(@PathVariable UUID id, @RequestBody FilmPutDTO put) {
        return filmService.updateFilm(id, put);
    }

    @ContentManager
    @DeleteMapping("/films/{id}")
    public void deleteFilm(@PathVariable UUID id) {
        filmService.deleteFilm(id);
    }
}
