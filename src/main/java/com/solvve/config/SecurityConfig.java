package com.solvve.config;

import com.solvve.security.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers("/health").anonymous()
                .antMatchers(HttpMethod.POST, "/api/v1/content-managers").anonymous()
                .antMatchers(HttpMethod.POST, "/api/v1/moderators").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/unregistered-user/news/{id}").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/unregistered-user/films/{id}/extended").anonymous()
                .antMatchers(HttpMethod.GET, "/api/v1/unregistered-user/actors/{id}/extended").anonymous()
                .antMatchers("/api/v1/registered-users").anonymous()
                .antMatchers("/v2/api-docs").anonymous()
                .antMatchers("/swagger-ui.html", "/webjars/springfox-swagger-ui/**",
                        "/swagger-resources/**", "/", "/csrf").anonymous()
                .anyRequest().authenticated()
                .and().httpBasic()
                .and().csrf().disable();

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER);
        http.authenticationProvider(authProvider());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

}
