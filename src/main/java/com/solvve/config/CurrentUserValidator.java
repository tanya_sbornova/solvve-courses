package com.solvve.config;

import com.solvve.repository.RegisteredUserRepository;
import com.solvve.security.AuthenticationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class CurrentUserValidator {

    @Autowired
    private AuthenticationResolver authenticationResolver;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    public boolean isCurrentUser(UUID userId) {
        Authentication authentication = authenticationResolver.getCurrentAuthentication();
        return registeredUserRepository.existsByIdAndEmail(userId, authentication.getName());
    }
}
