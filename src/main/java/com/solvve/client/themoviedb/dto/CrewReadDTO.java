package com.solvve.client.themoviedb.dto;

import lombok.Data;

@Data
public class CrewReadDTO {

    private String crewId;
    private String department;
    private String job;
    private String gender;
    private String id;
    private String name;
}
