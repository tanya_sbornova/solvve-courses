package com.solvve.client.themoviedb.dto;

import lombok.Data;

import java.util.List;

@Data
public class MovieCreditsDTO {

    private String id;
    private List<CastReadDTO> cast;
    private List<CrewReadDTO> crew;
}
