package com.solvve.client.themoviedb.dto;

import lombok.Data;

@Data
public class CastReadDTO {

    private String castId;
    private String character;
    private String gender;
    private String id;
    private String name;
}
