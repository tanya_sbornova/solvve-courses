package com.solvve.client.themoviedb.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import lombok.Data;

import java.time.LocalDate;

@Data
public class PersonReadDTO {

    private String id;
    private String name;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate birthday;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate deathday;

    private String placeOfBirth;
    private String biography;
    private String gender;
}
