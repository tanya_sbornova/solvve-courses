package com.solvve.client.themoviedb.dto;

import lombok.Data;

@Data
public class DepartmentReadDTO {

    private String departmentId;
    private String name;
}
