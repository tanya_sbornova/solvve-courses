package com.solvve.repository;

import com.solvve.domain.NewsLikeDislike;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface NewsLikeDislikeRepository extends CrudRepository<NewsLikeDislike, UUID> {

    @Query("select sum(case when nld.isLike = true then 1 else 0 end) from"
            + " NewsLikeDislike nld where nld.news.id = :newsId")
    Integer calcCntOfNewsLike(UUID newsId);

    @Query("select count(nld) from NewsLikeDislike nld where nld.news.id = :newsId and nld.isLike = false")
    Integer calcCntOfNewsDisLike(UUID newsId);
}
