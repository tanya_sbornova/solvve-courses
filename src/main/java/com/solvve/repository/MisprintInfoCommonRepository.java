package com.solvve.repository;

import com.solvve.domain.MisprintInfoCommon;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface MisprintInfoCommonRepository extends CrudRepository<MisprintInfoCommon, UUID> {

    @Query("select mp from MisprintInfoCommon mp where mp.objectId = :objectId and mp.status = 'NEED_TO_FIX'"
            + "and (mp.incorrectText LIKE CONCAT('%',:incorrectText,'%') "
            + "or (mp.startIndex >= :startIndex and mp.endIndex <= :endIndex "
            + "and :incorrectText LIKE CONCAT('%',mp.incorrectText,'%')))")
    List<MisprintInfoCommon> getMisprintsInfoCommon(UUID objectId, String incorrectText,
                                                         Integer startIndex,
                                                         Integer endIndex);
}
