package com.solvve.repository;

import com.solvve.domain.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface RoleRepository extends CrudRepository<Role, UUID>, RoleRepositoryCustom {

    @Query("select r from Role r where r.actor.person.id = :personId and r.film.id = :filmId")
    List<Role> findRolesForPersonAndForFilm(UUID personId, UUID filmId);

    @Query("select r from Role r where r.actor.id = :actorId and r.film.id = :filmId")
    List<Role> findRolesForActorAndForFilm(UUID actorId, UUID filmId);

    List<Role> findByActorIdAndFilmId(UUID actor, UUID film);

    @Query("select r from Role r where r.film.id = :filmId and r.text = :nameRole")
    Role findRoleByFilmIdAndNameRole(UUID filmId, String nameRole);

    @Query("select r from Role r where r.text = :nameRole")
    Role findRoleByNameRole(String nameRole);
}
