package com.solvve.repository;

import com.solvve.domain.Role;
import com.solvve.dto.role.RoleFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class RoleRepositoryCustomImpl implements RoleRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<Role> findByFilter(RoleFilter filter, Pageable pageable) {

        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select r from Role r where 1=1 ");
        qb.append("and r.actor.person.id = :v", filter.getPersonId());
        qb.append("and r.film.id = :v", filter.getFilmId());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}
