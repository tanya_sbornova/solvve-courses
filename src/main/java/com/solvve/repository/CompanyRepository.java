package com.solvve.repository;

import com.solvve.domain.Company;
import com.solvve.domain.Country;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CompanyRepository extends CrudRepository<Company, UUID> {

    Company findByNameAndCountryName(String name, Country countryName);
}
