package com.solvve.repository;

import com.solvve.domain.Content;
import com.solvve.domain.MisprintInfo;
import com.solvve.domain.MisprintObject;
import com.solvve.dto.misprintinfofix.ContentFixFilter;
import com.solvve.dto.misprintinfofix.MisprintInfoFixFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MisprintInfoFixRepositoryCustom {
    Page<MisprintInfo> findByFilterMisprintsInfo(MisprintInfoFixFilter filter,
                                                 MisprintObject typeContent, Pageable pageable);

    Content findContentByMisprintInfoId(ContentFixFilter filter, MisprintObject typeContent, Pageable pageable);
}
