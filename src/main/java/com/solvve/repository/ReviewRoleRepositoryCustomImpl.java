package com.solvve.repository;

import com.solvve.domain.ReviewRole;
import com.solvve.dto.reviewrole.ReviewRoleFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ReviewRoleRepositoryCustomImpl implements ReviewRoleRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<ReviewRole> findByFilter(ReviewRoleFilter filter, Pageable pageable) {

        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select rr from ReviewRole rr ");
        qb.append(" where 1 = 1 ");
        qb.append("and rr.status = :v", filter.getStatus());
        qb.append("and rr.isSpoiler = :v", filter.getIsSpoiler());
        qb.append("and rr.registeredUser.id = :v", filter.getRegisteredUserId());
        qb.append("and rr.role.id = :v", filter.getRoleId());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}
