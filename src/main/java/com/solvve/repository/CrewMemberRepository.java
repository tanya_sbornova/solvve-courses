package com.solvve.repository;

import com.solvve.domain.CrewMember;
import com.solvve.domain.CrewMemberType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CrewMemberRepository extends CrudRepository<CrewMember, UUID> {

    @Query("select cr from CrewMember cr where cr.crewMemberType = :crewMemberType and cr.person.id = :personId")
    List<CrewMember> findCrewMembersForPerson(CrewMemberType crewMemberType, UUID personId);

    List<CrewMember> findByCrewMemberType(CrewMemberType crewMemberType);

    List<CrewMember> findByPersonIdAndCrewMemberType(UUID person, CrewMemberType crewMemberType);

    CrewMember findByPersonNameAndCrewMemberType(String personName, CrewMemberType crewMemberType);
}
