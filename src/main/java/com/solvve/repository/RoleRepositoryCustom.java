package com.solvve.repository;

import com.solvve.domain.Role;
import com.solvve.dto.role.RoleFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RoleRepositoryCustom {
    Page<Role> findByFilter(RoleFilter filter, Pageable pageable);
}
