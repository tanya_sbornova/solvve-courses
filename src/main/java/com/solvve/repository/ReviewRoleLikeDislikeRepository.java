package com.solvve.repository;

import com.solvve.domain.ReviewRoleLikeDislike;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ReviewRoleLikeDislikeRepository extends CrudRepository<ReviewRoleLikeDislike, UUID> {

    ReviewRoleLikeDislike findByRegisteredUserIdAndId(UUID registeredUserId, UUID id);
}
