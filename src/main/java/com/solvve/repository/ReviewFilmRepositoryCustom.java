package com.solvve.repository;

import com.solvve.domain.ReviewFilm;
import com.solvve.dto.reviewfilm.ReviewFilmFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ReviewFilmRepositoryCustom {
    Page<ReviewFilm> findByFilter(ReviewFilmFilter filter, Pageable pageable);
}
