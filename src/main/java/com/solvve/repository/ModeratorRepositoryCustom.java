package com.solvve.repository;

import com.solvve.domain.Moderator;
import com.solvve.dto.moderator.ModeratorFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ModeratorRepositoryCustom {
    Page<Moderator> findByFilter(ModeratorFilter filter, Pageable pageable);
}
