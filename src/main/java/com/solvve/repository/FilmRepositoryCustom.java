package com.solvve.repository;

import com.solvve.domain.Film;
import com.solvve.dto.film.FilmFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface FilmRepositoryCustom {
    Page<Film> findByFilter(FilmFilter filter, Pageable pageable);
}
