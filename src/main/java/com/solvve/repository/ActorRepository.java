package com.solvve.repository;

import com.solvve.domain.Actor;
import com.solvve.dto.actor.ActorInLeaderBoardReadDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@Repository
public interface ActorRepository extends CrudRepository<Actor, UUID> {

    @Query("select act from Actor act where act.person.id = :personId")
    List<Actor> findActorsForPerson(UUID personId);

    List<Actor> findByPersonId(UUID person);

    @Query("select act.id from Actor act")
    Stream<UUID> getIdsOfActors();

    @Query("select new com.solvve.dto.actor.ActorInLeaderBoardReadDTO(ac.id, ac.person.name, ac.averageRating)"
            + "from Actor ac where ac.averageRating is not null order by ac.averageRating desc")
    List<ActorInLeaderBoardReadDTO> getActorsLeaderBoard();

    @Query("select act from Actor act where act.person.name = :personName")
    Actor findByPersonName(String personName);
}