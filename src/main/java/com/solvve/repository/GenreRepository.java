package com.solvve.repository;

import com.solvve.domain.FilmGenre;
import com.solvve.domain.Genre;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface GenreRepository extends CrudRepository<Genre, UUID> {

    Genre findByName(FilmGenre name);
}