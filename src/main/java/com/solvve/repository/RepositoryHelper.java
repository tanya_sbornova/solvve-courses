package com.solvve.repository;

import com.solvve.exception.EntityNotFoundException;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.UUID;

@Component
public class RepositoryHelper {

    @PersistenceContext
    private EntityManager entityManager;

    public void saveEntity(Object object) {
        entityManager.merge(object);
    }

    public <E> E getEntityRequired(Class<E> entityClass, UUID id) {
        E entityFound = entityManager.find(entityClass, id);
        if (entityFound == null) {
            throw new EntityNotFoundException(entityClass, id);
        }
        return entityFound;
    }

    public <T, E> E getEntityAssociationRequired(Class<T> associationClass, Class<E> entityClass,
                                                 UUID associationId, UUID entityId) {
        validateExistsAssociation(associationClass, entityClass, associationId, entityId);
        return entityManager.find(entityClass, entityId);
    }

    public <E> E getReferenceIfExists(Class<E> entityClass, UUID id) {
        validateExists(entityClass, id);
        return entityManager.getReference(entityClass, id);
    }

    private <E> void validateExists(Class<E> entityClass, UUID id) {
        Query query = entityManager.createQuery(
                "select count(e) from " + entityClass.getSimpleName() + " e where e.id = :id");
        query.setParameter("id", id);
        boolean exists = ((Number) query.getSingleResult()).intValue() > 0;
        if (!exists) {
            throw new EntityNotFoundException(entityClass, id);
        }
    }

    private <T, E> void validateExistsAssociation(Class<T> associationClass, Class<E> entityClass,
                                                  UUID associationId, UUID entityId) {
        Query query = entityManager.createQuery(
                "select count(e) from " + entityClass.getSimpleName() + " e where e.id = :id and e."
                        + associationClass.getSimpleName().substring(0, 1).toLowerCase()
                        + associationClass.getSimpleName().substring(1) + ".id = :aid");
        query.setParameter("id", entityId);
        query.setParameter("aid", associationId);
        boolean exists = ((Number) query.getSingleResult()).intValue() > 0;
        if (!exists) {
            throw new EntityNotFoundException(entityClass, entityId, associationClass, associationId);
        }
    }
}
