package com.solvve.repository;

import com.solvve.domain.MisprintInfoFilm;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
import java.util.stream.Stream;

@Repository
public interface MisprintInfoFilmRepository extends CrudRepository<MisprintInfoFilm, UUID> {

    @Query("select mp from MisprintInfoFilm mp where mp.content.id = :filmId and "
            + "mp.startIndex >= :startIndex and mp.endIndex <= :endIndex")
    Stream<MisprintInfoFilm> getIdsOfMisprintInfoFilms(UUID filmId, Integer startIndex, Integer endIndex);
}
