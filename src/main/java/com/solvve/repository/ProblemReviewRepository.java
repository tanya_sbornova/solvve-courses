package com.solvve.repository;

import com.solvve.domain.ProblemReview;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ProblemReviewRepository extends CrudRepository<ProblemReview, UUID> {

    @Query("select pr from ProblemReview pr where pr.reviewFilm.id = :reviewFilmId")
    List<ProblemReview> findProblemReviewsByReviewFilmId(UUID reviewFilmId);

    @Query("select pr from ProblemReview pr where pr.reviewRole.id = :reviewRoleId")
    List<ProblemReview> findProblemReviewsByReviewRoleId(UUID reviewRoleId);
}

