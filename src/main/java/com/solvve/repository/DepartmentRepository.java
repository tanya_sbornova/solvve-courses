package com.solvve.repository;

import com.solvve.domain.Department;
import com.solvve.domain.DepartmentType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, UUID> {

    Department findByDepartmentType(DepartmentType departmentType);
}
