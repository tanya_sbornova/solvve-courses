package com.solvve.repository;

import com.solvve.domain.ContentManager;
import com.solvve.dto.contentmanager.ContentManagerFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ContentManagerRepositoryCustom {
    Page<ContentManager> findByFilter(ContentManagerFilter filter, Pageable pageable);
}
