package com.solvve.repository;

import com.solvve.domain.ContentManager;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ContentManagerRepository extends CrudRepository<ContentManager, UUID>,
        ContentManagerRepositoryCustom {

    ContentManager findByEmail(String email);
}
