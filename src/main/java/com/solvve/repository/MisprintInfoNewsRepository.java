package com.solvve.repository;

import com.solvve.domain.MisprintInfoNews;
import com.solvve.domain.MisprintStatus;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@Repository
public interface MisprintInfoNewsRepository extends CrudRepository<MisprintInfoNews, UUID> {

    @Query("select mp.id from MisprintInfoNews mp where mp.content.id = :newsId and "
            + "mp.startIndex >= :startIndex and mp.endIndex <= :endIndex and mp.status = 'NEED_TO_FIX'")
    Stream<UUID> getIdsOfMisprintInfoNews(UUID newsId, Integer startIndex, Integer endIndex);

    List<MisprintInfoNews> findByStatus(MisprintStatus status);
}

