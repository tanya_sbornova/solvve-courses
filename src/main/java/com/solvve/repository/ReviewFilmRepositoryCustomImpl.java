package com.solvve.repository;

import com.solvve.domain.ReviewFilm;
import com.solvve.dto.reviewfilm.ReviewFilmFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ReviewFilmRepositoryCustomImpl implements ReviewFilmRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<ReviewFilm> findByFilter(ReviewFilmFilter filter, Pageable pageable) {

        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select rf from ReviewFilm rf ");
        qb.append(" where 1 = 1 ");
        qb.append("and rf.status = :v", filter.getStatus());
        qb.append("and rf.isSpoiler = :v", filter.getIsSpoiler());
        qb.append("and rf.registeredUser.id = :v", filter.getRegisteredUserId());
        qb.append("and rf.film.id = :v", filter.getFilmId());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}
