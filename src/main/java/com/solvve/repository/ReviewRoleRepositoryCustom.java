package com.solvve.repository;

import com.solvve.domain.ReviewRole;
import com.solvve.dto.reviewrole.ReviewRoleFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ReviewRoleRepositoryCustom {
    Page<ReviewRole> findByFilter(ReviewRoleFilter filter, Pageable pageable);
}
