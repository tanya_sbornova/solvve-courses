package com.solvve.repository;

import com.solvve.domain.MisprintInfoRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MisprintInfoRoleRepository extends CrudRepository<MisprintInfoRole, UUID> {

}