package com.solvve.repository;

import com.solvve.domain.RatingFilm;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RatingFilmRepository extends CrudRepository<RatingFilm, UUID> {

    RatingFilm findByFilmIdAndId(UUID film, UUID id);

    @Query("select avg(rf.rating) from RatingFilm rf where rf.film.id = :filmId")
    Double calcAverageRatingOfFilm(UUID filmId);

    @Query("select avg(rf.rating) from RatingFilm rf join Role r on rf.film.id = r.film where r.actor.id = :actorId")
    Double calcAverageRatingActorByFilm(UUID actorId);
}
