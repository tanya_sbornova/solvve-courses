package com.solvve.repository;

import com.solvve.domain.Film;
import com.solvve.dto.film.FilmFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


public class FilmRepositoryCustomImpl implements FilmRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<Film> findByFilter(FilmFilter filter, Pageable pageable) {

        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select f from Film f ");
        if (filter.getGenre() != null) {
            qb.append(" join f.genres g where g.name in :v", filter.getGenre());
        } else {
            qb.append(" where 1 = 1 ");
        }
        qb.append("and f.datePrime >= :v", filter.getDatePrimeFrom());
        qb.append("and f.datePrime < :v", filter.getDatePrimeTo());
        qb.append("and f.budget >= :v", filter.getBudgetFrom());
        qb.append("and f.budget < :v", filter.getBudgetTo());
        qb.append("and f.status = :v", filter.getStatus());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}
