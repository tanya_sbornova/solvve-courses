package com.solvve.repository;

import com.solvve.domain.Content;
import com.solvve.domain.MisprintInfo;
import com.solvve.domain.MisprintObject;
import com.solvve.dto.misprintinfofix.ContentFixFilter;
import com.solvve.dto.misprintinfofix.MisprintInfoFixFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.*;

@Repository
public class MisprintInfoFixRepositoryCustomImpl implements MisprintInfoFixRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<MisprintInfo> findByFilterMisprintsInfo(MisprintInfoFixFilter filter,
                                                        MisprintObject typeContent,
                                                        Pageable pageable) {

        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select mp from ");
        String mpTypeContent = "MisprintInfo" + typeContent.toString().substring(0, 1).toUpperCase()
                + typeContent.toString().substring(1).toLowerCase();
        qb.append(mpTypeContent);
        qb.append(" mp where 1 = 1 and mp.status = 'NEED_TO_FIX'");

        qb.append(" and mp.content.id = :v", filter.getContentId());
        if (filter.getIncorrectText() != null && (filter.getStartIndex() == null || filter.getEndIndex() == null)) {
            qb.appendLike(" and mp.incorrectText LIKE :v ", filter.getIncorrectText());
        }
        if (filter.getIncorrectText() != null && filter.getStartIndex() != null && filter.getEndIndex() != null) {
            qb.append(" and mp.startIndex >= :v", filter.getStartIndex());
            qb.append(" and mp.endIndex <= :v", filter.getEndIndex());
            qb.append(" and CONCAT('\"',:v,'\"')", filter.getIncorrectText());
            qb.append("LIKE CONCAT('%',mp.incorrectText,'%')");
        }
        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }

    @Override
    public Content findContentByMisprintInfoId(ContentFixFilter filter, MisprintObject typeContent,
                                               Pageable pageable) {

        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select content from ");
        String mpTypeContent = "MisprintInfo" + typeContent.toString().substring(0, 1).toUpperCase()
                + typeContent.toString().substring(1).toLowerCase();
        qb.append(mpTypeContent);
        qb.append(" mp where 1 = 1 and mp.status = 'NEED_TO_FIX'");

        qb.append(" and mp.id = :v", filter.getMisprintInfoId());

        return (Content) SpringQueryBuilderUtils.loadPage(qb, pageable, "id").get().findFirst().orElse(null);
    }
}
