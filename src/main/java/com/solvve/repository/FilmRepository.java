package com.solvve.repository;

import com.solvve.domain.Film;
import com.solvve.dto.film.FilmInLeaderBoardReadDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@Repository
public interface FilmRepository extends CrudRepository<Film, UUID>, FilmRepositoryCustom {

    @Query("select f.id from Film f")
    Stream<UUID> getIdsOfFilms();

    @Query("select new com.solvve.dto.film.FilmInLeaderBoardReadDTO(f.id, f.text, f.averageRating,"
            + " (select count(rf) from RatingFilm rf where rf.film.id = f.id and rf.rating < 3.0), "
            + " (select count(rf) from RatingFilm rf where rf.film.id = f.id and rf.rating >  8.0)) "
            + "from Film f where f.averageRating is not null order by f.averageRating desc")
    List<FilmInLeaderBoardReadDTO> getFilmsLeaderBoard();

    Film findByTitle(String title);
}

