package com.solvve.repository;

import com.solvve.domain.ContentManager;
import com.solvve.dto.contentmanager.ContentManagerFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ContentManagerRepositoryCustomImpl implements  ContentManagerRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<ContentManager> findByFilter(ContentManagerFilter filter, Pageable pageable) {

        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select cm from ContentManager cm where 1=1 ");
        qb.append("and cm.email = :v", filter.getEmail());
        qb.append("and cm.userStatus = :v", filter.getUserStatus());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}
