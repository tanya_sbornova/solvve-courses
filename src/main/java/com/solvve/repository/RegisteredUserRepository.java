package com.solvve.repository;

import com.solvve.domain.RegisteredUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RegisteredUserRepository extends CrudRepository<RegisteredUser, UUID> {

    RegisteredUser findByEmail(String email);

    boolean existsByIdAndEmail(UUID id, String email);
}
