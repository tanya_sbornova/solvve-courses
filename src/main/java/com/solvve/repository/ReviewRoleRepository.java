package com.solvve.repository;

import com.solvve.domain.ReviewRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ReviewRoleRepository extends CrudRepository<ReviewRole, UUID>, ReviewRoleRepositoryCustom {

    ReviewRole findByRoleIdAndId(UUID role, UUID id);
}
