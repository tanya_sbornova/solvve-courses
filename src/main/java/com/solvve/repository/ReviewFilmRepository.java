package com.solvve.repository;

import com.solvve.domain.ReviewFilm;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ReviewFilmRepository extends CrudRepository<ReviewFilm, UUID>, ReviewFilmRepositoryCustom {

}
