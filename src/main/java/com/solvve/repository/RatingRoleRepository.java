package com.solvve.repository;

import com.solvve.domain.RatingRole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RatingRoleRepository extends CrudRepository<RatingRole, UUID> {

    RatingRole findByRoleIdAndId(UUID role, UUID id);

    @Query("select avg(rr.rating) from RatingRole rr where rr.role.actor.id = :actorId")
    Double calcAverageRatingActorOfRole(UUID actorId);
}
