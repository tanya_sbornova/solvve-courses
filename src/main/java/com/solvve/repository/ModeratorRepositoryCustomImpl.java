package com.solvve.repository;

import com.solvve.domain.Moderator;
import com.solvve.dto.moderator.ModeratorFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ModeratorRepositoryCustomImpl implements ModeratorRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<Moderator> findByFilter(ModeratorFilter filter, Pageable pageable) {

        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select m from Moderator m where 1=1 ");
        qb.append("and m.email = :v", filter.getEmail());
        qb.append("and m.userStatus = :v", filter.getUserStatus());

        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}
