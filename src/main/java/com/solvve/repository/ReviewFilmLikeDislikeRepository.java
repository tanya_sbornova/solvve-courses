package com.solvve.repository;

import com.solvve.domain.ReviewFilmLikeDislike;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ReviewFilmLikeDislikeRepository extends CrudRepository<ReviewFilmLikeDislike, UUID> {

    ReviewFilmLikeDislike findByRegisteredUserIdAndId(UUID registeredUserId, UUID id);
}
