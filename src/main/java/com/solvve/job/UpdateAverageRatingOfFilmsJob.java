package com.solvve.job;

import com.solvve.repository.FilmRepository;
import com.solvve.service.FilmService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class UpdateAverageRatingOfFilmsJob {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private FilmService filmService;

    @Scheduled(cron = "${update.average.rating.of.films.job.cron}")
    @Transactional(readOnly = true)
    public void updateAverageRatingOfFilms() {
        log.info("Job started");
        filmRepository.getIdsOfFilms().forEach(filmId -> {
            try {
                filmService.updateAverageRatingOfFilm(filmId);
            }
            catch (Exception e) {
                log.error("Failed to update average rating for film: {}", filmId, e);
            }
        });
        log.info("Job finished");
    }
}
