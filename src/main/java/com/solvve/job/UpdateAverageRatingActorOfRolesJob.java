package com.solvve.job;

import com.solvve.repository.ActorRepository;
import com.solvve.service.ActorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class UpdateAverageRatingActorOfRolesJob {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private ActorService actorService;

    @Scheduled(cron = "${update.average.rating.of.actors.job.cron}")
    @Transactional(readOnly = true)
    public void updateAverageRatingOfActors() {
        log.info("Job started");
        actorRepository.getIdsOfActors().forEach(actorId -> {
            try {
                actorService.updateAverageRatingActorOfRole(actorId);
            }
            catch (Exception e) {
                log.error("Failed to update average rating for actor: {}", actorId, e);
            }
        });
        log.info("Job finished");
    }
}
