package com.solvve.exception;

import com.solvve.domain.ExternalSystemImport;
import lombok.Getter;

@Getter
public class ImportAlreadyPerformedException extends Exception {

    private ExternalSystemImport externalSystemImport;

    public ImportAlreadyPerformedException(ExternalSystemImport esi) {
        super(String.format("Already performed import of %s with id=%s and id in external system = %s",
                esi.getEntityType(), esi.getEntityId(), esi.getIdInExternalSystem()));
        this.externalSystemImport = esi;
    }
}
