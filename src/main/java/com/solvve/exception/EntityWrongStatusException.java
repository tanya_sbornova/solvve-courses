package com.solvve.exception;

import com.solvve.domain.MisprintStatus;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class EntityWrongStatusException extends RuntimeException {
    public EntityWrongStatusException(Class entityClass, UUID id, MisprintStatus misprintStatus,
                                       MisprintStatus misprintRequiredStatus) {
        super(String.format("Entity %s with id=%s can't be fixed because of it's status %s! "
                        + misprintRequiredStatus + " required",
                entityClass.getSimpleName(), id, misprintStatus));
    }
}
