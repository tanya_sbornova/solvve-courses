package com.solvve.exception.handler;

import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Object> handleException(Exception ex) {

        ResponseStatus status = AnnotatedElementUtils.findMergedAnnotation(ex.getClass(),
                ResponseStatus.class);
        HttpStatus httpStatus = status != null ? status.code() : HttpStatus.INTERNAL_SERVER_ERROR;

        ErrorInfo errorInfo = new ErrorInfo(httpStatus, ex.getClass(), ex.getMessage());
        return new ResponseEntity<>(errorInfo, new HttpHeaders(), httpStatus);
    }

    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    public final ResponseEntity<Object> handleExceptionTypeMismatch(MethodArgumentTypeMismatchException ex) {

        String error =
                ex.getName() + " should be of type " + ex.getRequiredType().getName();

        HttpStatus httpstatus = HttpStatus.BAD_REQUEST;

        ErrorInfo errorInfo = new ErrorInfo(httpstatus, ex.getClass(), error);
        return new ResponseEntity<>(errorInfo, new HttpHeaders(), httpstatus);

    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public final ResponseEntity<Object> handleExceptionValidationFailed(MethodArgumentNotValidException ex) {

        HttpStatus httpstatus = HttpStatus.BAD_REQUEST;

        ErrorInfo errorInfo = new ErrorInfo(httpstatus, ex.getClass(), ex.getMessage());
        return new ResponseEntity<>(errorInfo, new HttpHeaders(), httpstatus);

    }

    @ExceptionHandler(value = AccessDeniedException.class)
    public final ResponseEntity<Object> handleExceptionValidationFailed(AccessDeniedException ex) {

        HttpStatus httpstatus = HttpStatus.FORBIDDEN;

        ErrorInfo errorInfo = new ErrorInfo(httpstatus, ex.getClass(), ex.getMessage());
        return new ResponseEntity<>(errorInfo, new HttpHeaders(), httpstatus);

    }
}
