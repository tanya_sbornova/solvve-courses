package com.solvve.exception;

import com.solvve.domain.MisprintStatus;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class EntityUnprocessableException extends RuntimeException {
    public EntityUnprocessableException(Class entityClass, UUID id, MisprintStatus misprintStatus) {
        super(String.format("Entity %s with id=%s can't be fixed because of it's status %s! NEED_TO_FIX required",
                entityClass.getSimpleName(), id, misprintStatus));
    }

}
