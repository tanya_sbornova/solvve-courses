package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Review extends AbstractEntity {

    @NotNull
    @Lob
    protected String text;

    @NotNull
    @Enumerated(EnumType.STRING)
    protected ReviewStatus status;

    @NotNull
    protected Boolean isSpoiler;
}
