package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@Entity
public class NewsLikeDislike extends AbstractEntity {

    @NotNull
    protected Boolean isLike;

    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private RegisteredUser registeredUser;

    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private News news;
}
