package com.solvve.domain;

public enum DepartmentType {

    ART,
    CAMERA,
    COSTUME_AND_MAKE_UP,
    CREW,
    DIRECTING,
    EDITING,
    LIGHTING,
    PRODUCTION,
    SOUND,
    WRITING
}
