package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;

@Setter
@Getter
@Entity
public class ReviewFilm extends Review {

    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private Film film;

    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private RegisteredUser registeredUser;
}
