package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
public class Role extends Content {

    @ManyToOne
    private Actor actor;

    @NotNull
    @ManyToOne
    private Film film;

    @OneToMany(mappedBy = "role", cascade = CascadeType.PERSIST)
    private List<RatingRole> ratingRoles = new ArrayList<>();

    @OneToMany(mappedBy = "role", cascade = CascadeType.PERSIST)
    private List<ReviewRole> reviewRoles = new ArrayList<>();
}
