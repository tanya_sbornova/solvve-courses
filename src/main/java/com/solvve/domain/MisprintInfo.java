package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.time.Instant;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "MP_TYPE")
public abstract class MisprintInfo extends AbstractEntity {

    protected Integer startIndex;
    protected Integer endIndex;

    protected String text;

    @Enumerated(EnumType.STRING)
    protected MisprintStatus status;

    @Enumerated(EnumType.STRING)
    protected MisprintObject object;

    protected String incorrectText;

    protected Instant fixedDate;

    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    protected RegisteredUser registeredUser;

    @ManyToOne
    protected ContentManager contentManager;

}
