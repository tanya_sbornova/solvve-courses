package com.solvve.domain;

public enum ReviewStatus {
    NEED_TO_MODERATE,
    MODERATED,
    FIXED,
    CANCELLED
}
