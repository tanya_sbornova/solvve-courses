package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Past;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Setter
@Getter
@Entity
public class Film extends Content {

    private String title;

    @Lob
    protected String description;

    @Past
    private LocalDate datePrime;

    private Integer budget;

    @Enumerated(EnumType.STRING)
    private FilmStatus status;

    @Max(value = 10)
    private Double averageRating;

    @Max(value = 10)
    private Double forecastRating;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "film_crew_member",
            joinColumns = {@JoinColumn(name = "film_id")},
            inverseJoinColumns = {@JoinColumn(name = "crew_member_id")}
    )
    Set<CrewMember> crewMembers = new HashSet<>();

    @OneToMany(mappedBy = "film", cascade = CascadeType.PERSIST)
    private List<RatingFilm> ratingFilms = new ArrayList<>();

    @OneToMany(mappedBy = "film", cascade = CascadeType.PERSIST)
    private List<ReviewFilm> reviewFilms = new ArrayList<>();

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "film_company",
            joinColumns = {@JoinColumn(name = "film_id")},
            inverseJoinColumns = {@JoinColumn(name = "company_id")}
    )
    Set<Company> companies = new HashSet<>();

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "film_genre",
            joinColumns = {@JoinColumn(name = "film_id")},
            inverseJoinColumns = {@JoinColumn(name = "genre_id")}
    )
    Set<Genre> genres = new HashSet<>();
}
