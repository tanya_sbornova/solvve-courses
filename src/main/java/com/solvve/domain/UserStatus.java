package com.solvve.domain;

public enum UserStatus {
    NEW,
    ACTIVE,
    BLOCKED
}
