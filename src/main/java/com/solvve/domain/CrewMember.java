package com.solvve.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class CrewMember extends AbstractEntity {

    @NotNull
    @Enumerated(EnumType.STRING)
    private CrewMemberType crewMemberType;

    @NotNull
    @ManyToOne
    private Person person;

    @ManyToOne
    private Department department;

    @ManyToMany(mappedBy = "crewMembers")
    private Set<Film> films = new HashSet<>();
}
