package com.solvve.domain;

public enum MisprintStatus {
    NEED_TO_FIX,
    FIXED,
    CANCELLED
}
