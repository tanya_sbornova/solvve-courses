package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Setter
@Getter
@Entity
@DiscriminatorValue("FILM")
public class MisprintInfoFilm extends MisprintInfo {

    @ManyToOne
    @JoinColumn(name = "film_id", nullable = false, updatable = false)
    private Film content;
}
