package com.solvve.domain;

public enum ProblemType {
    SPOILER,
    SPAM,
    FILTHY_LANGUAGE
}
