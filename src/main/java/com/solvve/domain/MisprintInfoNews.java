package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Setter
@Getter
@Entity
@DiscriminatorValue("NEWS")
public class MisprintInfoNews extends MisprintInfo {

    @ManyToOne
    @JoinColumn(name = "news_id", nullable = false, updatable = false)
    private News content;
}
