package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Setter
@Getter
@Entity
public class MisprintInfoCommon extends AbstractEntity {

    private Integer startIndex;
    private Integer endIndex;

    private String text;

    @Enumerated(EnumType.STRING)
    private MisprintStatus status;

    @Enumerated(EnumType.STRING)
    private MisprintObject objectType;

    private UUID objectId;

    private String incorrectText;

    private Instant fixedDate;

    @ManyToOne
    private ContentManager contentManager;
}
