package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;

@Setter
@Getter
@Entity
public class Person extends AbstractEntity {

    @NotNull
    private String name;

    @Past
    private LocalDate dateBirth;

    @Past
    private LocalDate dateDeath;

    private String placeBirth;

    @Enumerated(EnumType.STRING)
    private Country country;
    private String education;

    @Lob
    private String biography;

    @Enumerated(EnumType.STRING)
    private Sex sex;

}
