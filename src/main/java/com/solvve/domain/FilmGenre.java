package com.solvve.domain;

public enum FilmGenre {
    ACTION,
    COMEDY,
    DRAMA,
    FAMILY,
    ROMANCE,
    SCI_FI,
    THRILLER
}