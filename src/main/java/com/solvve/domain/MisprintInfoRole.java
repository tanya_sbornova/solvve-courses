package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Setter
@Getter
@Entity
@DiscriminatorValue("ROLE")
public class MisprintInfoRole extends MisprintInfo {

    @ManyToOne
    @JoinColumn(name = "role_id", nullable = false, updatable = false)
    private Role content;
}
