package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;

@Setter
@Getter
@Entity
public class UserProfile extends AbstractEntity {

    private Double ratingReviews;
    private Double ratingActivity;

    @Enumerated(EnumType.STRING)
    private FilmGenre favouriteGenre;
}
