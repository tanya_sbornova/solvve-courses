package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Setter
@Getter
@Entity
public class News extends Content {


    private String title;

    @NotNull
    @Min(value = 0, message = "Cnt like should not be less than 0")
    private Integer cntLike;

    @NotNull
    @Min(value = 0, message = "Cnt dislike should not be less than 0")
    private Integer cntDislike;

    @NotNull
    @Enumerated(EnumType.STRING)
    private NewsType newsType;

    private UUID newsObjectId;
}
