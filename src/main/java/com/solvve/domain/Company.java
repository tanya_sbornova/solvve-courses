package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@Entity
public class Company extends AbstractEntity {

    @Enumerated(EnumType.STRING)
    private Country countryName;

    @NotNull
    private String name;

    @ManyToMany(mappedBy = "companies")
    private Set<Film> films = new HashSet<>();
}
