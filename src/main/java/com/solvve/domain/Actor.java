package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
public class Actor extends AbstractEntity {

    @OneToMany(mappedBy = "actor", fetch = FetchType.LAZY)
    private List<Role> roles = new ArrayList<>();

    @NotNull
    @ManyToOne
    private Person person;

    @Max(value = 10, message = "Average actor's rating by film should not be greater than 10")
    private Double averageRating;

    @Max(value = 10, message = "Average actor's rating by film should not be greater than 10")
    private Double averageRatingByFilm;
}