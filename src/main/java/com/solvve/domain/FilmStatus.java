package com.solvve.domain;

public enum FilmStatus {
    RELEASED,
    NOT_RELEASED
}

