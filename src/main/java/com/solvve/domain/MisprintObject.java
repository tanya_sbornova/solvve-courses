package com.solvve.domain;

public enum MisprintObject {
    FILM,
    ROLE,
    NEWS,
    ARTICLE
}

