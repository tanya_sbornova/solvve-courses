package com.solvve.domain;

public enum UserRoleType {
    ADMIN,
    CONTENT_MANAGER,
    MODERATOR,
    REGISTERED_USER,
    BLOCKED
}
