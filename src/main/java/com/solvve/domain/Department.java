package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
public class Department extends AbstractEntity {

    @NotNull
    @Enumerated(EnumType.STRING)
    private DepartmentType departmentType;

    @OneToMany(mappedBy = "department", cascade = CascadeType.PERSIST)
    private List<CrewMember> crewMembers = new ArrayList<>();
}
