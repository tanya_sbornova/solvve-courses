package com.solvve.domain;

public enum ImportedEntityType {
    ACTOR,
    CREW_MEMBER,
    FILM,
    PERSON,
    ROLE,
    COMPANY,
    DEPARTMENT
}
