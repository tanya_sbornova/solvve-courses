package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class UserAccount extends AbstractEntity {

    @NotNull
    protected String email;

    @NotNull
    protected String encodedPassword;

    @Enumerated(EnumType.STRING)
    protected UserRoleType userRole;

    @NotNull
    @Enumerated(EnumType.STRING)
    protected UserStatus userStatus;

    protected LocalDate dateBirth;
    protected String country;

    @Enumerated(EnumType.STRING)
    protected Sex sex;
}
