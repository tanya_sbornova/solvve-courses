package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@Entity
public class ReviewRole extends Review {

    @NotNull
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private Role role;

    @NotNull
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private RegisteredUser registeredUser;
}
