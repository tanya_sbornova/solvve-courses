package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@Entity
public class ReviewRoleLikeDislike extends AbstractEntity {

    @NotNull
    private Boolean isLike;

    @NotNull
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private RegisteredUser registeredUser;

    @NotNull
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private ReviewRole reviewRole;
}
