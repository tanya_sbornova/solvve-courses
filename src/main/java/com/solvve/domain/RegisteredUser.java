package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
public class RegisteredUser extends  UserAccount {

    private Integer trustLevel;
    private String name;
    private LocalDate dateBirth;

    private String profileName;

    @Enumerated(EnumType.STRING)
    private FilmGenre favouriteGenre;
    private Double ratingReviews;
    private Double ratingActivity;

    @OneToMany(mappedBy = "registeredUser", cascade = CascadeType.PERSIST)
    private List<ReviewFilmLikeDislike> reviewFilmLikeDislikes = new ArrayList<>();

    @OneToMany(mappedBy = "registeredUser", cascade = CascadeType.PERSIST)
    private List<ReviewRoleLikeDislike> reviewRoleLikeDislikes = new ArrayList<>();

    @OneToMany(mappedBy = "registeredUser", cascade = CascadeType.PERSIST)
    private List<NewsLikeDislike> newsLikeDislikes = new ArrayList<>();

    @OneToMany(mappedBy = "registeredUser", cascade = CascadeType.PERSIST)
    private List<MisprintInfoFilm> misprintInfoFilms = new ArrayList<>();

    @OneToMany(mappedBy = "registeredUser", cascade = CascadeType.PERSIST)
    private List<MisprintInfoRole> misprintInfoRoles = new ArrayList<>();

    @OneToMany(mappedBy = "registeredUser", cascade = CascadeType.PERSIST)
    private List<MisprintInfoNews> misprintInfoNews = new ArrayList<>();

    @OneToMany(mappedBy = "registeredUser", cascade = CascadeType.PERSIST)
    private List<ProblemReview> problemReviews = new ArrayList<>();
}
