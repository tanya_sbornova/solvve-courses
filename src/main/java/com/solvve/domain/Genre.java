package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@Entity
public class Genre extends AbstractEntity {

    @NotNull
    @Enumerated(EnumType.STRING)
    private FilmGenre name;

    @ManyToMany(mappedBy = "genres")
    private Set<Film> films = new HashSet<>();
}
