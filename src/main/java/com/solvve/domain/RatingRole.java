package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@Entity
public class RatingRole extends AbstractEntity {

    @NotNull
    @Min(value = 0, message = "Rating should not be less than 0")
    @Max(value = 10, message = "Rating level should not be greater than 10")
    private Double rating;

    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private Role role;

    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private RegisteredUser registeredUser;
}
