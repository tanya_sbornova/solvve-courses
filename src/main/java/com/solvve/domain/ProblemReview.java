package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@Entity
public class ProblemReview extends AbstractEntity {

    @NotNull
    @Enumerated(EnumType.STRING)
    private ProblemType problemType;

    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private RegisteredUser registeredUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(
            name = "problem_film",
            joinColumns =
            @JoinColumn(name = "problem_id"),
            inverseJoinColumns =
            @JoinColumn(nullable = false)
    )
    private ReviewFilm reviewFilm;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(
            name = "problem_role",
            joinColumns =
            @JoinColumn(name = "problem_id"),
            inverseJoinColumns =
            @JoinColumn(nullable = false)
    )
    private ReviewRole reviewRole;
}